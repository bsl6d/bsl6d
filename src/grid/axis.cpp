/*****************************************************************/
/*

\brief Define the axis of a single dimension

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   17.02.2022

*/
/*****************************************************************/

#include <axis.hpp>
namespace bsl6d {

Axis::Axis(Type axisType, double length, size_t Npoints, double minBound)
    : m_axisType{axisType}
    , m_length{length}
    , m_minBound{minBound}
    , m_points{Npoints} {
  switch (axisType) {
    case Type::L: m_dxv = this->L() / this->N(); break;
    case Type::N: m_dxv = this->L() / (this->N() - 1); break;
    default:
      throw std::invalid_argument(
          "Unknown axis type in class Axis Constructor.");
  }
}

Axis Axis::partition(size_t low, size_t up) const {
  Axis axis{};
  axis.m_axisType = this->type();
  axis.m_minBound = this->operator()(low - this->low());
  axis.m_dxv      = this->delta();
  axis.m_lowIdx   = low;
  axis.m_points   = up - low + 1;
  if (axis.low() < this->low()) {
    throw std::out_of_range("Lower index to small in axis constructor.");
  } else if (axis.up() > this->up()) {
    throw std::out_of_range("Upper index to large in axis constructor.");
  }

  switch (axis.type()) {
    case Axis::Type::L:
      axis.m_length =
          this->operator()(up) - this->operator()(low) + this->delta();
      break;
    case Axis::Type::N:
      axis.m_length = this->operator()(up) - this->operator()(low);
      break;
    default:
      throw std::invalid_argument(
          "Unknown axis type for partitioning an axis.");
  }
  return axis;
}

std::string axis_type_to_string(Axis::Type const &type) {
  switch (type) {
    case (Axis::Type::L): return "L";
    case (Axis::Type::N): return "N";
    default:
      throw std::runtime_error(
          "Axis::Type given to axis_type_to_string unknown");
  }
}

Axis::Type string_to_axis_type(std::string const &type) {
  if (type == "L") {
    return Axis::Type::L;
  } else if (type == "N" or type == "C") {
    return Axis::Type::N;
  } else {
    throw std::runtime_error("Axis Type given to string_to_axis_type unknown");
  }
}

bool operator==(Axis const &a, Axis const &b) {
  double Ldiff{};
  if (a.L() != 0.) Ldiff = std::abs((a.L() - b.L()) / a.L());
  if ((Ldiff < 1e-15) && (a.min() == b.min()) && (a.low() == b.low()) &&
      (a.N() == b.N()) && (a.type() == b.type())) {
    return true;
  } else {
    return false;
  }
};

bool operator!=(Axis const &a, Axis const &b) {
  if (a == b) {
    return false;
  } else {
    return true;
  }
};

} /* namespace bsl6d */

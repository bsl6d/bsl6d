/*****************************************************************/
/*

\brief Grid defined for 6d hypercube

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.09.2021

*/
/*****************************************************************/

#include "grid.hpp"

namespace bsl6d {

namespace Impl {
static std::vector<AxisType> xAxes{};
static std::vector<AxisType> vAxes{};
static std::vector<AxisType> xvAxes{};

}  // namespace Impl

const std::vector<AxisType> &xAxes() { return Impl::xAxes; }
const std::vector<AxisType> &vAxes() { return Impl::vAxes; }
const std::vector<AxisType> &xvAxes() { return Impl::xvAxes; }

/*****************************************************************/
/*

\brief Grid functions

*/
/*****************************************************************/
template <size_t xDim, size_t vDim>
Grid<xDim, vDim>::Grid(std::array<Axis, xDim + vDim> const &_axes,
                       std::array<AxisType, xDim + vDim> const &_xvAxes,
                       std::array<AxisType, xDim> const &_xAxes,
                       std::array<AxisType, vDim> const &_vAxes,
                       bool rotatingVelocityGrid)
    : xvAxes{_xvAxes}
    , xAxes{_xAxes}
    , vAxes{_vAxes}
    , axes{_axes}
    , m_rotatingVelocityGrid{rotatingVelocityGrid} {};

std::string axis_to_string(AxisType const &axis) {
  switch (axis) {
    case (AxisType::x1): return "x1";
    case (AxisType::x2): return "x2";
    case (AxisType::x3): return "x3";
    case (AxisType::v1): return "v1";
    case (AxisType::v2): return "v2";
    case (AxisType::v3): return "v3";
    default: throw std::runtime_error("Unknown axis given to axis_to_string()");
  };
};

AxisType string_to_axis(std::string const &axis) {
  if (axis == "x1") {
    return AxisType::x1;
  } else if (axis == "x2") {
    return AxisType::x2;
  } else if (axis == "x3") {
    return AxisType::x3;
  } else if (axis == "v1") {
    return AxisType::v1;
  } else if (axis == "v2") {
    return AxisType::v2;
  } else if (axis == "v3") {
    return AxisType::v3;
  } else {
    throw std::runtime_error("Unknown axis given to axis_to_string()");
  }
}

template <size_t xDim, size_t vDim>
Grid<xDim, 0> Grid<xDim, vDim>::create_xSpace() const {
  std::array<Axis, xDim> axesX{};
  for (auto a : xAxes) axesX[static_cast<int>(a)] = axes[static_cast<int>(a)];

  return Grid<xDim, 0>{axesX, xAxes, xAxes, {}, false};
}

template <size_t xDim, size_t vDim>
Grid<xDim, vDim> Grid<xDim, vDim>::partition(
    std::array<size_t, xDim + vDim> low,
    std::array<size_t, xDim + vDim> up) const {
  std::array<Axis, xDim + vDim> _axes{};
  for (auto axis : xvAxes) {
    _axes[axis] = this->axes[axis].partition(low[axis], up[axis]);
  }

  return Grid<xDim, vDim>{_axes, xvAxes, xAxes, vAxes, m_rotatingVelocityGrid};
}

// template class Grid<0,3>;

Kokkos::View<double ******> create_grid_compatible_view(
    Grid<3, 3> const &grid,
    std::string const &label) {
  return create_grid_compatible_view(grid, label, grid.xvAxes);
}
Kokkos::View<double ***> create_grid_compatible_view(Grid<3, 0> const &grid,
                                                     std::string const &label) {
  return Kokkos::View<double ***>{label, grid(AxisType::x1).N(),
                                  grid(AxisType::x2).N(),
                                  grid(AxisType::x3).N()};
}

template <size_t xDim, size_t vDim>
Grid<xDim, vDim>::Grid(GridDistributionConfig const &config) {
  GridDistributionConfig::ConfigGrid configGrid{config.configGrid};
  if constexpr (xDim + vDim == 6) {
    if (configGrid.axes.size() < 6) {
      std::vector<std::string> axes{"x1", "x2", "x3", "v1", "v2", "v3"};
      for (auto axis : axes) {
        auto axisFound =
            std::find(configGrid.axes.begin(), configGrid.axes.end(), axis);
        // Axis does not exist in initial data
        if (axisFound == configGrid.axes.end()) {
          configGrid.axes.push_back(axis);
          configGrid.xvMax.push_back(1.0);
          configGrid.xvMin.push_back(0.0);
          configGrid.NxvPoints.push_back(1);
          configGrid.position.push_back("L");
        }
      }
    }
    std::array<Axis, xDim + vDim> axes{};
    std::array<AxisType, xDim + vDim> xvAxes{AxisType::x1, AxisType::x2,
                                             AxisType::x3, AxisType::v1,
                                             AxisType::v2, AxisType::v3};

    std::array<AxisType, xDim> xAxes{AxisType::x1, AxisType::x2, AxisType::x3};

    std::array<AxisType, vDim> vAxes{AxisType::v1, AxisType::v2, AxisType::v3};
    for (size_t i = 0; i < configGrid.axes.size(); i++) {
      axes[static_cast<int>(string_to_axis(configGrid.axes[i]))] =
          Axis{string_to_axis_type(configGrid.position[i]),
               configGrid.xvMax[i] - configGrid.xvMin[i],
               configGrid.NxvPoints[i], configGrid.xvMin[i]};
    }
    Grid<xDim, vDim> grid{axes, xvAxes, xAxes, vAxes,
                          configGrid.rotateGyroFreq};

    Impl::xAxes  = std::vector<AxisType>{};
    Impl::vAxes  = std::vector<AxisType>{};
    Impl::xvAxes = std::vector<AxisType>{};
    for (auto axis : grid.xAxes) {
      if (grid(axis).N() > 1) {
        Impl::xAxes.push_back(axis);
        Impl::xvAxes.push_back(axis);
      }
    }
    for (auto axis : grid.vAxes) {
      if (grid(axis).N() > 1) {
        Impl::vAxes.push_back(axis);
        Impl::xvAxes.push_back(axis);
      }
    }
    *this = std::move(grid);
  } else {
    throw std::runtime_error(
        "Only get_3d3v_grid(GridDistributionConfig const&) for xDim=3 and "
        "vDim=3 is implemented");
  }
}

template class Grid<3, 3>;
template class Grid<3, 0>;

} /* namespace bsl6d */

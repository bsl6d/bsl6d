#include <string>

#include <gtest/gtest.h>

#include <config.hpp>
#include "../grid.hpp"

struct DefaultGridConf {
  std::array<double, 6> length{1.5, 2.0, 4.0, 5.0, 1.0, 1.0};
  std::array<size_t, 6> N{10, 12, 15, 17, 1, 1};
  std::array<double, 6> min{0.0, 0.0, -2.0, -2.5, 0.0, 0.0};
  std::array<std::string, 6> name{"x1", "x2", "v1", "v2", "x3", "v3"};
};

TEST(GridGrid, Init3d3v) {
  nlohmann::json expectedGrid = R"({
      "grid": {
        "axes": ["x1","x2","x3","v1","v2","v3"],
        "xvMax": [2.2, 2.6, 1.0, 0.9, 0.7, 1.0],
        "xvMin": [0.0, 0.0, 0.0,-0.9,-0.7, 0.0],
        "NxvPoints": [22, 26, 1, 19, 15, 1],
        "position": ["L", "L", "L", "N", "N", "L"],
        "rotateGyroFreq": false
      }
    })"_json;

  {
    nlohmann::json configFile = R"({
    "gridDistribution": {
      "grid": {
        "axes": ["x1","x2","v1","v2"],
        "xvMax": [2.2, 2.6, 0.9, 0.7],
        "xvMin": [0.0, 0.0,-0.9,-0.7],
        "NxvPoints": [22, 26, 19, 15],
        "position": ["L", "L", "N", "N"],
        "rotateGyroFreq": false
      },
      "parallel": {}
    }
    })"_json;
    bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{configFile}};

    std::vector<bsl6d::AxisType> xAxes{bsl6d::AxisType::x1,
                                       bsl6d::AxisType::x2};
    EXPECT_EQ(bsl6d::xAxes(), xAxes);
    std::vector<bsl6d::AxisType> vAxes{bsl6d::AxisType::v1,
                                       bsl6d::AxisType::v2};
    EXPECT_EQ(bsl6d::vAxes(), vAxes);
    std::vector<bsl6d::AxisType> xvAxes{
        bsl6d::AxisType::x1, bsl6d::AxisType::x2, bsl6d::AxisType::v1,
        bsl6d::AxisType::v2};
    EXPECT_EQ(bsl6d::xvAxes(), xvAxes);
    for (auto axis : grid.xvAxes) {
      EXPECT_EQ(grid(axis).L(),
                expectedGrid["grid"]["xvMax"]
                        .get<std::vector<double>>()[static_cast<size_t>(axis)] -
                    expectedGrid["grid"]["xvMin"]
                        .get<std::vector<double>>()[static_cast<size_t>(axis)])
          << "Axis: " + axis_to_string(axis);
      EXPECT_EQ(grid(axis).N(),
                expectedGrid["grid"]["NxvPoints"][static_cast<size_t>(axis)])
          << "Axis: " + axis_to_string(axis);
      EXPECT_EQ(grid(axis).min(),
                expectedGrid["grid"]["xvMin"][static_cast<size_t>(axis)])
          << "Axis: " + axis_to_string(axis);
    }

    EXPECT_DOUBLE_EQ(grid.cell_vol_x(), 0.01);
    EXPECT_DOUBLE_EQ(grid.cell_vol_v(), 0.01);
    EXPECT_DOUBLE_EQ(grid.volume_normalized_cell_vol_x(),
                     grid.cell_vol_x() / grid(bsl6d::AxisType::x1).L() /
                         grid(bsl6d::AxisType::x2).L() /
                         grid(bsl6d::AxisType::x3).L());
  }
}

/*****************************************************************/
TEST(GridGrid, Creation3d0v) {
  bsl6d::Grid<3, 3> grid3d3v{bsl6d::GridDistributionConfig{}};
  bsl6d::Grid<3, 0> grid3d0v{grid3d3v.create_xSpace()};

  for (auto axis : grid3d0v.xvAxes) {
    EXPECT_EQ(grid3d0v.xAxes[static_cast<size_t>(axis)], axis);
    EXPECT_EQ(grid3d0v(axis).L(), grid3d3v(axis).L())
        << "Axis: " + axis_to_string(axis);
    EXPECT_EQ(grid3d0v(axis).N(), grid3d3v(axis).N())
        << "Axis: " + axis_to_string(axis);
    EXPECT_EQ(grid3d0v(axis).min(), grid3d3v(axis).min())
        << "Axis: " + axis_to_string(axis);
  }
}

/*****************************************************************/
TEST(GridGrid, EqualityOperator) {
  bsl6d::Grid<3, 3> gridDefault{bsl6d::GridDistributionConfig{}};
  nlohmann::json configFile = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [2.2, 2.6, 2.8, 1.8, 1.5, 1.6],
      "xvMin": [2.2, 2.6, 2.8,-1.8,-1.5,-1.6],
      "NxvPoints": [22, 26, 28, 19, 15, 17],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;

  bsl6d::Grid<3, 3> gridConf{bsl6d::GridDistributionConfig{configFile}};

  EXPECT_EQ(gridDefault == gridConf, false);
  EXPECT_EQ(gridDefault == gridDefault, true);

  EXPECT_EQ(gridConf != gridConf, false);
  EXPECT_EQ(gridDefault != gridConf, true);
}

/*****************************************************************/
TEST(GridGrid, RuleOfFive) {
  bsl6d::Grid<3, 3> gridDefault{bsl6d::GridDistributionConfig{}};
  nlohmann::json configFile = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [2.2, 2.6, 2.8, 1.8, 1.5, 1.6],
      "xvMin": [2.2, 2.6, 2.8,-1.8,-1.5,-1.6],
      "NxvPoints": [22, 26, 28, 19, 15, 17],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;

  bsl6d::Grid<3, 3> gridConf{bsl6d::GridDistributionConfig{configFile}};

  EXPECT_THROW(gridDefault = gridConf, std::runtime_error);
  bsl6d::Grid<3, 3> gridDefault2{};
  EXPECT_NO_THROW(gridDefault2 = gridDefault);
  EXPECT_EQ(gridDefault, gridDefault2);

  EXPECT_THROW(gridDefault = std::move(gridConf), std::runtime_error);
  EXPECT_NO_THROW(gridDefault = std::move(gridDefault2));

  bsl6d::Grid<3, 3> gridEmpty{};
  EXPECT_NO_THROW(gridEmpty = gridDefault);
  EXPECT_EQ(gridEmpty, gridDefault);
}

TEST(GridGrid, Partition) {
  bsl6d::Grid<3, 3> gridDefault{bsl6d::GridDistributionConfig{}};
  std::array<size_t, 6> low{0, 1, 2, 3, 4, 5};
  std::array<size_t, 6> up{9, 9, 8, 7, 6, 5};

  bsl6d::Grid<3, 3> gridPartition{gridDefault.partition(low, up)};
  for (auto axis : gridPartition.xvAxes) {
    EXPECT_EQ(gridPartition(axis).low(), low[static_cast<size_t>(axis)]);
    EXPECT_EQ(gridPartition(axis).up(), up[static_cast<size_t>(axis)]);
  }
}

int parallel_kokkos_reduction(
    Kokkos::MDRangePolicy<Kokkos::Rank<6>> mdRangePol) {
  int sum{};
  Kokkos::parallel_reduce(
      "3dIterationRange", mdRangePol,
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3, int &lsum) { lsum += 1; },
      sum);
  return sum;
}

int parallel_kokkos_reduction(
    Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePol) {
  int sum{};
  Kokkos::parallel_reduce(
      "3dIterationRange", mdRangePol,
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, int &lsum) {
        lsum += 1;
      },
      sum);
  return sum;
}

TEST(GridGrid, MDRangePolicy) {
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};

  Kokkos::MDRangePolicy<Kokkos::Rank<6>> rngPol6D{grid.mdRangePolicy()};
  Kokkos::MDRangePolicy<Kokkos::Rank<3>> rngPol3D{
      grid.create_xSpace().mdRangePolicy()};

  size_t sum3d{1};
  for (auto axis : grid.xAxes) { sum3d *= grid(axis).N(); }
  size_t sum3d3v{sum3d};
  for (auto axis : grid.vAxes) { sum3d3v *= grid(axis).N(); }

  ASSERT_EQ(sum3d3v, parallel_kokkos_reduction(rngPol6D));
  ASSERT_EQ(sum3d, parallel_kokkos_reduction(rngPol3D));
}

TEST(GridGrid, CreateCompatibleView) {
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  Kokkos::View<double ******> view6d{
      create_grid_compatible_view(grid, "TestView6D")};
  for (auto axis : grid.xvAxes) {
    EXPECT_EQ(view6d.extent(static_cast<size_t>(axis)), grid(axis).N());
  }
  Kokkos::View<double ***> view3d{
      create_grid_compatible_view(grid.create_xSpace(), "TestView3D")};
  for (auto axis : grid.xAxes) {
    EXPECT_EQ(view6d.extent(static_cast<size_t>(axis)), grid(axis).N());
  }
  std::array<bsl6d::AxisType, 2> axes{bsl6d::AxisType::v1, bsl6d::AxisType::x3};
  Kokkos::View<double **> view2d{
      create_grid_compatible_view(grid, "TestView2D", axes)};
  for (auto axis : axes) {
    EXPECT_EQ(view2d.extent(3 - static_cast<size_t>(axis)), grid(axis).N());
  }
}

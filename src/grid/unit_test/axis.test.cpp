#include <string>

#include <gtest/gtest.h>

#include <config.hpp>
#include "../axis.hpp"

TEST(GridAxis, LeftDof) {
  double L{2.2}, min{-0.1};
  size_t N{22};
  bsl6d::Axis axis{bsl6d::Axis::Type::L, L, N, min};
  EXPECT_EQ(axis.type(), bsl6d::Axis::Type::L);

  EXPECT_DOUBLE_EQ(axis.L(), L);
  EXPECT_DOUBLE_EQ(axis.min(), axis(0));
  EXPECT_DOUBLE_EQ(axis.max(), axis(axis.up() + 1));
  EXPECT_DOUBLE_EQ(axis.max() - axis.min(), axis.L());
  EXPECT_DOUBLE_EQ(axis.delta(), axis(2) - axis(1));

  EXPECT_EQ(axis.low(), 0);
  EXPECT_EQ(axis.N(), N);
  EXPECT_EQ(axis.up(), axis.low() + axis.N() - 1);
}

TEST(GridAxis, NodeDof) {
  double L{3.6}, min{-2.1};
  size_t N{19};
  bsl6d::Axis axis{bsl6d::Axis::Type::N, L, N, min};
  EXPECT_EQ(axis.type(), bsl6d::Axis::Type::N);

  EXPECT_DOUBLE_EQ(axis.L(), L);
  EXPECT_DOUBLE_EQ(axis.min(), axis(0));
  EXPECT_DOUBLE_EQ(axis.max(), axis(axis.up()));
  EXPECT_DOUBLE_EQ(axis.max() - axis.min(), axis.L());
  EXPECT_DOUBLE_EQ(axis.delta(), axis(2) - axis(1));

  EXPECT_EQ(axis.N(), N);
  EXPECT_EQ(axis.low(), 0);
  EXPECT_EQ(axis.up(), axis.N() + axis.low() - 1);
  EXPECT_DOUBLE_EQ(axis(axis.up()), axis.max());
}

TEST(GridAxis, PartitionLeft) {
  size_t idxLow{4}, idxUp{10};
  bsl6d::Axis axisBaseL{bsl6d::Axis::Type::L, 2.2, 22, 0};
  bsl6d::Axis axis{axisBaseL.partition(idxLow, idxUp)};
  EXPECT_EQ(axis.low(), idxLow);
  EXPECT_EQ(axis.up(), idxUp);
  EXPECT_EQ(axis.type(), axisBaseL.type());
  EXPECT_EQ(axis.delta(), axisBaseL.delta());
  EXPECT_EQ(axis.N(), axis.up() - axis.low() + 1);
  EXPECT_EQ(axis.min(), axisBaseL(idxLow));
  EXPECT_EQ(axis.max(), axisBaseL(idxUp + 1));
  for (size_t i = axis.low(); i <= axis.up(); i++)
    EXPECT_EQ(axis(i - axis.low()), axisBaseL(i));
  EXPECT_EQ(axis.partition(axis.low(), axis.up()), axis);

  EXPECT_THROW(axis.partition(3, 5), std::out_of_range);
  EXPECT_THROW(axis.partition(9, 11), std::out_of_range);
}

TEST(GridAxis, PartitionNode) {
  size_t idxLow{4}, idxUp{10};
  bsl6d::Axis axisBaseN{bsl6d::Axis::Type::N, 2.2, 23, 0};
  bsl6d::Axis axis{axisBaseN.partition(idxLow, idxUp)};
  EXPECT_EQ(axis.low(), idxLow);
  EXPECT_EQ(axis.up(), idxUp);
  EXPECT_EQ(axis.type(), axisBaseN.type());
  EXPECT_EQ(axis.delta(), axisBaseN.delta());
  EXPECT_EQ(axis.N(), idxUp - idxLow + 1);
  EXPECT_EQ(axis.min(), axisBaseN(idxLow));
  EXPECT_EQ(axis.max(), axisBaseN(idxUp));
  for (size_t i = axis.low(); i <= axis.up(); i++)
    EXPECT_EQ(axis(i - axis.low()), axisBaseN(i));
  EXPECT_EQ(axis.partition(axis.low(), axis.up()), axis);

  EXPECT_THROW(axis.partition(3, 5), std::out_of_range);
  EXPECT_THROW(axis.partition(9, 11), std::out_of_range);
}

TEST(GridAxis, EqualityOperator) {
  bsl6d::Axis axisX{bsl6d::Axis::Type::L, 2, 10, 0};
  bsl6d::Axis axisV{bsl6d::Axis::Type::N, 2, 11, -1};
  EXPECT_EQ(axisX == axisV, false);
  EXPECT_EQ(axisX == axisX, true);

  bsl6d::Axis axisEmpty{};
  EXPECT_EQ(axisEmpty == axisEmpty, true);
  EXPECT_EQ(axisX == axisEmpty, false);

  EXPECT_EQ(axisX != axisX, false);
  EXPECT_EQ(axisX != axisV, true);
}

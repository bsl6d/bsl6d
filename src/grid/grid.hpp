/*****************************************************************/
/*

\brief Grid defined for 6d hypercube

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.09.2021

*/
/*****************************************************************/
#pragma once

#include <array>

#include <Kokkos_Core.hpp>

#include <axis.hpp>
#include <config.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief Class to define a 6d tensor grid in phase space that is spanned
       by orthogonal axis using bsl6d::Axis

*/
/*****************************************************************/
// If the AxisType is in the global bsl6d scope it could be accessed
// without specifying the enum type.
enum AxisType { x1, x2, x3, v1, v2, v3 };
const std::vector<AxisType> &xAxes();
const std::vector<AxisType> &vAxes();
const std::vector<AxisType> &xvAxes();
template <size_t xDim, size_t vDim>
class Grid {
  public:
  enum Direction { x1, x2, x3, v1, v2, v3 };
  Grid() = default;
  Grid(GridDistributionConfig const &);
  Grid(std::array<Axis, xDim + vDim> const &axes,
       std::array<AxisType, xDim + vDim> const &xvAxes,
       std::array<AxisType, xDim> const &xAxes,
       std::array<AxisType, vDim> const &vAxes,
       bool rotatingVelocityGrid);

  Grid(Grid<xDim, vDim> const &grid) = default;
  Grid<xDim, vDim> &operator=(Grid<xDim, vDim> const &grid) {
    this->m_rotatingVelocityGrid = grid.m_rotatingVelocityGrid;
    if (*this == grid || *this == Grid<xDim, vDim>{}) {
      this->axes   = grid.axes;
      this->xvAxes = grid.xvAxes;
      this->xAxes  = grid.xAxes;
      this->vAxes  = grid.vAxes;
      return *this;
    } else {
      throw std::runtime_error(
          "Grids are not identical in copy assignment of grid");
    }
  }

  Grid(Grid<xDim, vDim> &&grid) = default;
  Grid<xDim, vDim> &operator=(Grid<xDim, vDim> &&grid) {
    this->m_rotatingVelocityGrid = std::move(grid.m_rotatingVelocityGrid);
    if (*this == grid || *this == Grid<xDim, vDim>{}) {
      this->axes   = std::move(grid.axes);
      this->xvAxes = std::move(grid.xvAxes);
      this->xAxes  = std::move(grid.xAxes);
      this->vAxes  = std::move(grid.vAxes);
      return *this;
    } else {
      throw std::runtime_error(
          "Grids are not identical in move assignment of grid");
    }
  }

  // To be depricated
  KOKKOS_INLINE_FUNCTION
  Axis operator()(AxisType const axis) const {
    return axes[static_cast<int>(axis)];
  }
  KOKKOS_INLINE_FUNCTION
  Axis operator()(Direction const axis) const { return axes[axis]; }
  bool rotating_velocity_grid() { return m_rotatingVelocityGrid; }

  KOKKOS_INLINE_FUNCTION
  double cell_vol_x() const {
    double volx{1};
    for (auto axis : xAxes) volx *= axes[static_cast<size_t>(axis)].delta();

    return volx;
  };

  KOKKOS_INLINE_FUNCTION
  double cell_vol_v() const {
    double volv{1};
    for (auto axis : vAxes) volv *= axes[static_cast<size_t>(axis)].delta();

    return volv;
  };

  KOKKOS_INLINE_FUNCTION
  double cell_vol_xv() const {
    double volxv{1};
    for (auto axis : xvAxes) volxv *= axes[static_cast<size_t>(axis)].delta();

    return volxv;
  };

  KOKKOS_INLINE_FUNCTION
  double spatial_domain_volume() const {
    double volx{1};
    for (auto axis : xAxes) volx *= axes[static_cast<size_t>(axis)].L();

    return volx;
  }

  KOKKOS_INLINE_FUNCTION
  double volume_normalized_cell_vol_x() const {
    double volx{1};
    for (auto axis : xAxes) volx *= 1. / axes[static_cast<size_t>(axis)].N();

    return volx;
  };

  template <typename KokkosExecutionSpace = Kokkos::DefaultExecutionSpace>
  Kokkos::MDRangePolicy<Kokkos::Rank<xDim + vDim>, KokkosExecutionSpace>
      mdRangePolicy() const {
    Kokkos::Array<int, xDim + vDim> start{};
    Kokkos::Array<int, xDim + vDim> stop{};
    for (size_t i = 0; i < xDim + vDim; i++) {
      start[i] = 0;
      stop[i]  = axes[i].N();
    }
    return Kokkos::MDRangePolicy<Kokkos::Rank<xDim + vDim>,
                                 KokkosExecutionSpace>{start, stop};
  }

  Grid<xDim, 0> create_xSpace() const;
  Grid<xDim, vDim> partition(std::array<size_t, xDim + vDim> low,
                             std::array<size_t, xDim + vDim> up) const;

  // To be solved in #issue 34
  // To be replaced or depricated.
  // Restrict these to 6D and 3D arrays containing
  // x1-v3
  std::array<AxisType, xDim + vDim> xvAxes{};
  // x1-x3
  std::array<AxisType, xDim> xAxes{};
  // v1-v3
  std::array<AxisType, vDim> vAxes{};

  private:
  std::array<Axis, xDim + vDim> axes{};
  bool m_rotatingVelocityGrid{};
};

template <size_t S>
auto create_grid_compatible_view(Grid<3, 3> const &grid,
                                 std::string const &label,
                                 std::array<AxisType, S> const &axes) {
  if constexpr (S == 1) {
    size_t N = grid(axes[0]).N();
    return Kokkos::View<double *>{label, N};
  } else if constexpr (S == 2) {
    std::array<size_t, S> N{};
    for (size_t i = 0; i < S; i++) N[i] = grid(axes[i]).N();
    return Kokkos::View<double **>{label, N[0], N[1]};
  } else if constexpr (S == 3) {
    std::array<size_t, S> N{};
    for (size_t i = 0; i < S; i++) N[i] = grid(axes[i]).N();
    return Kokkos::View<double ***>{label, N[0], N[1], N[2]};
  } else if constexpr (S == 4) {
    std::array<size_t, S> N{};
    for (size_t i = 0; i < S; i++) N[i] = grid(axes[i]).N();
    return Kokkos::View<double ****>{label, N[0], N[1], N[2], N[3]};
  } else if constexpr (S == 5) {
    std::array<size_t, S> N{};
    for (size_t i = 0; i < S; i++) N[i] = grid(axes[i]).N();
    return Kokkos::View<double *****>{label, N[0], N[1], N[2], N[3], N[4]};
  } else if constexpr (S == 6) {
    std::array<size_t, S> N{};
    for (size_t i = 0; i < S; i++) N[i] = grid(axes[i]).N();
    return Kokkos::View<double ******>{label, N[0], N[1], N[2],
                                       N[3],  N[4], N[5]};
  }
  throw(
      "[create_grid_compatible_view]: The rank of the View has to be "
      "lower than or equal to 6");
}
Kokkos::View<double ******> create_grid_compatible_view(
    Grid<3, 3> const &grid,
    std::string const &label);
Kokkos::View<double ***> create_grid_compatible_view(Grid<3, 0> const &grid,
                                                     std::string const &label);

template <typename KokkosExecutionSpace = Kokkos::DefaultExecutionSpace,
          size_t S>
Kokkos::MDRangePolicy<Kokkos::Rank<S>, KokkosExecutionSpace>
    create_mdRangePolicy(Grid<3, 3> const &grid,
                         std::array<AxisType, S> const &axes) {
  Kokkos::Array<size_t, S> start{};
  Kokkos::Array<size_t, S> stop{};
  for (size_t i = 0; i < S; i++) {
    start[i] = 0;
    stop[i]  = grid(axes[i]).N();
  }
  return Kokkos::MDRangePolicy<Kokkos::Rank<S>, KokkosExecutionSpace>{start,
                                                                      stop};
}

template <typename KokkosExecutionSpace = Kokkos::DefaultExecutionSpace>
Kokkos::MDRangePolicy<Kokkos::Rank<6>, KokkosExecutionSpace>
    create_mdRangePolicy(Grid<3, 3> const &grid) {
  return create_mdRangePolicy<KokkosExecutionSpace, 6>(
      grid, std::array<AxisType, 6>{AxisType::x1, AxisType::x2, AxisType::x3,
                                    AxisType::v1, AxisType::v2, AxisType::v3});
}

AxisType string_to_axis(std::string const &axis);
std::string axis_to_string(AxisType const &axis);

/*****************************************************************/
/*

\brief == : True if xvAxes-Array and all Axes within array are equal
       != : True if not ==

*/
/*****************************************************************/
template <size_t xDim, size_t yDim>
bool operator==(Grid<xDim, yDim> const &a, Grid<xDim, yDim> const &b) {
  bool equal{true};
  if (a.xvAxes == b.xvAxes) {
    for (auto axis : a.xvAxes) {
      if (a(axis) != b(axis)) equal = false;
    }
    return equal;
  } else {
    return false;
  }
}

template <size_t xDim, size_t yDim>
bool operator!=(Grid<xDim, yDim> const &a, Grid<xDim, yDim> const &b) {
  if (a == b) {
    return false;
  } else {
    return true;
  }
}

} /* namespace bsl6d */

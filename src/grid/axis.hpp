#pragma once

#include <Kokkos_Core.hpp>

#include <config.hpp>

namespace bsl6d {

struct t_configAxis {
  double length{2.};
  double min{-1.};
  size_t N{10};
};

/**
 * @class Axis
 * @brief Represents a one-dimensional axis for equidistant finite difference
 * grids
 *
 * @details A single axis manages data for a 1-D finite difference grid.
 *          The axis is defined by its length, the minimum value, the number of
 *          cells (degrees of freedom) and the type of the axis.
 *
 *          The computational domain of the axis is given by \f$L = min - max\f$
 *
 * Two independent definitions for axes are available depending on where in
 * the cell a degree of freedom is positioned.
 *
 * @ref Axis::Type L:
 * The degree of freedom is located in the left end of a cell. The interval of
 * the cell is a closed interval at the lower boundary and an open interval on
 * the upper boundary.
 *
 * @image html BSL6DCellAxis.png
 *
 * @ref Axis::Type N:
 * The degree of freedom is located on the cell nodes or on the boundareis of
 * the cell. Therefore N-1 cells in an axis have N degrees of freedom.
 *
 * @image html BSL6DNodeAxis.png
 *
 */
class Axis {
  public:
  // ToDo Rename to position
  enum class Type  //! Defines the location of the degree of freedom within a
                   //! cell
  {
    L,
    N
  };

  Axis() = default;
  Axis(Type type, double length, size_t N, double minBound);

  /**
   * @brief Create a subset of the @ref Axis
   *
   * @param low : Lower idx the new axis
   * @param up : Upper idx the new axis
   */
  Axis partition(size_t low, size_t up) const;

  KOKKOS_INLINE_FUNCTION
  double L()  //! Length of the domain
      const {
    return m_length;
  };
  KOKKOS_INLINE_FUNCTION
  double
      min()  //! Lower inverval boundary of the domain represented by the axis
      const {
    return m_minBound;
  };
  KOKKOS_INLINE_FUNCTION
  double
      max()  //! Upper interval boundary of the domain represented by the axis
      const {
    return (m_minBound + L());
  };
  KOKKOS_INLINE_FUNCTION
  double delta()  //! Width of a cell
      const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  size_t N()  //! Number of cells in the axis
      const {
    return m_points;
  };
  KOKKOS_INLINE_FUNCTION
  size_t low()  //! Index of the upper degree of freedom with resprect to the
                //! global index
      const {
    return m_lowIdx;
  };
  KOKKOS_INLINE_FUNCTION
  size_t up()  //! Index of the upper degree of freedom with resprect to the
               //! global index
      const {
    return m_lowIdx + m_points - 1;
  };
  KOKKOS_INLINE_FUNCTION
  Type type()  //! Position of the degree of freedom within a cell
      const {
    return m_axisType;
  };

  /**
   * @brief Calculate the position of the given index in the interval
   *        [@ref Axis::min, @ref Axis::max]
   *
   * @param idx : idx\f$\in[0,\f$ @ref Axis::N \f$]\f$
   */
  KOKKOS_INLINE_FUNCTION
  double operator()(int idx) const { return double(idx) * delta() + min(); }

  private:
  Type m_axisType;

  double m_length{};
  double m_minBound{};
  double m_dxv{};
  size_t m_lowIdx{};
  size_t m_points{};
};

std::string axis_type_to_string(Axis::Type const &type);
Axis::Type string_to_axis_type(std::string const &type);

/*****************************************************************/
/*

\brief == : True if L,low,N,type are equal.
       != : True if not ==

*/
/*****************************************************************/
bool operator==(Axis const &a, Axis const &b);
bool operator!=(Axis const &a, Axis const &b);

} /* namespace bsl6d */

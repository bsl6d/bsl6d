/*****************************************************************/
/*

\brief Halo communication datastructure

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#pragma once

#include <boundaryHypercube.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <mpiTopology.hpp>

#include <Kokkos_Core.hpp>

#include <mpi.h>

namespace bsl6d {

class HaloComm {
  public:
  HaloComm() = default;
  HaloComm(Grid<3, 3> const grid,
           std::map<bsl6d::AxisType, size_t> boundaryWidth);
  HaloComm(Grid<3, 0> const grid,
           std::map<bsl6d::AxisType, size_t> boundaryWidth);
  BoundaryHypercube exchange(DistributionFunction const &f6d,
                             AxisType const &axis) const;
  void exchange(bsl6d::ScalarField const &scalarField,
                bsl6d::AxisType const &axis) const;

  private:
#if BSL6D_ENABLE_CUDA_AWARE_MPI
  void cuda_aware_mpi_exchange(AxisType const &axis,
                               int const &lowNeighbourRank,
                               int const &upNeighbourRank,
                               MPI_Comm const &comm) const;
#else
  void non_cuda_aware_mpi_exchange(AxisType const &axis,
                                   int const &lowNeighbourRank,
                                   int const &upNeighbourRank,
                                   MPI_Comm const &comm) const;
#endif

  BoundaryHypercube m_sendBoundary{}, m_recvBoundary{};
};

}  // namespace bsl6d

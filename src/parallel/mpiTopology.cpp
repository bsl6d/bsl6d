/*****************************************************************/
/*

\brief Initialize MPI cartesian Topology and Setup

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#include <algorithm>
#include <vector>

#include <mpi_helper.hpp>

#include "mpiTopology.hpp"

namespace bsl6d {

namespace Impl {

MPICommHandler::MPICommHandler(MPICommHandler &&other) {
  m_comm       = other.m_comm;
  other.m_comm = MPI_COMM_NULL;
};

MPICommHandler::~MPICommHandler() {
  if (m_comm != MPI_COMM_NULL) MPI_Comm_free(&m_comm);
};

}  // namespace Impl

template <size_t dim>
CartTopology<dim>::CartTopology(GridDistributionConfig const &config) {
  std::vector<int> Nprocs{1, 1, 1, 1, 1, 1};
  std::vector<bool> periodicity{true, true, true, true, true, true};

  for (size_t iAxis = 0; iAxis < config.configGrid.NxvPoints.size(); iAxis++) {
    Nprocs[static_cast<size_t>(string_to_axis(config.configGrid.axes[iAxis]))] =
        config.configMPICartTopology.Nprocs[iAxis];
    periodicity[static_cast<size_t>(
        string_to_axis(config.configGrid.axes[iAxis]))] =
        config.configMPICartTopology.periodicity[iAxis];
  }
  std::copy_n(periodicity.begin(), dim, m_periodicity.begin());
  std::copy_n(Nprocs.begin(), dim, m_cartDimSizes.begin());
  // Periodicity in all directions is necessary for now
  if (std::find(m_periodicity.begin(), m_periodicity.end(), false) !=
      m_periodicity.end()) {
    throw std::invalid_argument(
        "Currently only periodic boundaries are "
        "supported for class CartTopology<>");
  }

  MPI_Comm baseComm{};
  CHECK_MPI(MPI_Comm_dup(MPI_COMM_WORLD, &baseComm));
  MPI_Comm cartComm{};
  CHECK_MPI(MPI_Cart_create(baseComm, dim, m_cartDimSizes.begin(),
                            m_periodicity.begin(), false, &cartComm));
  m_comm =
      std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(baseComm));
  m_cartComm =
      std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(cartComm));

  set_harddrive_writer();
}

// When created with this constructor, the new instance of CartTopology
// takes responsibility for destroying the MPI_Comm
template <size_t dim>
CartTopology<dim>::CartTopology(MPI_Comm comm,
                                GridDistributionConfig const &config) {
  std::vector<int> Nprocs{1, 1, 1, 1, 1, 1};
  std::vector<bool> periodicity{true, true, true, true, true, true};

  for (size_t iAxis = 0; iAxis < config.configGrid.NxvPoints.size(); iAxis++) {
    Nprocs[static_cast<size_t>(string_to_axis(config.configGrid.axes[iAxis]))] =
        config.configMPICartTopology.Nprocs[iAxis];
    periodicity[static_cast<size_t>(
        string_to_axis(config.configGrid.axes[iAxis]))] =
        config.configMPICartTopology.periodicity[iAxis];
  }
  std::copy_n(periodicity.begin(), dim, m_periodicity.begin());
  std::copy_n(Nprocs.begin(), dim, m_cartDimSizes.begin());
  for (size_t i = 0; i < m_periodicity.size(); i++) m_periodicity[i] = true;

  MPI_Comm cartComm{};
  CHECK_MPI(MPI_Cart_create(comm, dim, m_cartDimSizes.begin(),
                            m_periodicity.begin(), false, &cartComm));
  m_comm = std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(comm));
  m_cartComm =
      std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(cartComm));

  set_harddrive_writer();
}

// When created with this constructor, the new instance of CartTopology
// takes responsibility for destroying the MPI_Comm
template <size_t dim>
CartTopology<dim>::CartTopology(MPI_Comm cartComm) {
  m_cartComm =
      std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(cartComm));

  // For some reason this is now needed. Still don't know why
  std::array<int, dim> dimensions;
  std::array<int, dim> periods;
  std::array<int, dim> coords;
  MPI_Cart_get(cartComm, dim, dimensions.begin(), periods.begin(),
               coords.begin());
  for (size_t i = 0; i < dim; i++) {
    m_cartDimSizes[i] = dimensions[i];
    m_periodicity[i]  = periods[i];
  }

  // Create the non-topology communicator
  MPI_Group myGroup{};
  MPI_Comm comm{};
  CHECK_MPI(MPI_Comm_group(cartComm, &myGroup));
  CHECK_MPI(MPI_Comm_create(cartComm, myGroup, &comm));
  MPI_Group_free(&myGroup);

  m_comm = std::make_shared<Impl::MPICommHandler>(Impl::MPICommHandler(comm));

  set_harddrive_writer();
}

template <size_t dim>
int CartTopology<dim>::size() const {
  int numProcesses{};
  CHECK_MPI(MPI_Comm_size(cart_comm(), &numProcesses));
  return numProcesses;
}

template <size_t dim>
int CartTopology<dim>::my_cart_rank() const {
  int myCartRank{};
  CHECK_MPI(MPI_Comm_rank(cart_comm(), &myCartRank));
  return myCartRank;
}

template <size_t dim>
int CartTopology<dim>::my_low_neighbor(AxisType const axis) const {
  int lowNeighbor;
  int _myCartRank;
  CHECK_MPI(MPI_Cart_shift(cart_comm(), static_cast<size_t>(axis), -1,
                           &_myCartRank, &lowNeighbor));
  return lowNeighbor;
}

template <size_t dim>
int CartTopology<dim>::my_up_neighbor(AxisType const axis) const {
  int upNeighbor;
  int _myCartRank;
  CHECK_MPI(MPI_Cart_shift(cart_comm(), static_cast<size_t>(axis), 1,
                           &_myCartRank, &upNeighbor));
  return upNeighbor;
}

template <size_t dim>
std::array<int, dim> CartTopology<dim>::my_cart_coord() const {
  std::array<int, dim> cartRanks{};
  CHECK_MPI(
      MPI_Cart_coords(cart_comm(), my_cart_rank(), dim, cartRanks.begin()));
  return cartRanks;
}

template <size_t dim>
std::array<int, dim> CartTopology<dim>::cart_coord(int cartRank) const {
  if (cartRank < 0 || size() <= cartRank)
    throw std::invalid_argument(
        "cartRank given to cart_coord of Topology "
        "does not exist.");
  std::array<int, dim> cartRanks{};
  CHECK_MPI(MPI_Cart_coords(cart_comm(), cartRank, dim, cartRanks.begin()));
  return cartRanks;
}

template <size_t dim>
CartTopology<dim> CartTopology<dim>::create_single_proc_topology() const {
  MPI_Group group{};
  CHECK_MPI(MPI_Comm_group(MPI_COMM_WORLD, &group));
  MPI_Group singleProcGroup{};
  int myRank{my_cart_rank()};
  CHECK_MPI(MPI_Group_incl(group, 1, &myRank, &singleProcGroup));
  MPI_Group_free(&group);

  MPI_Comm singleProcComm{};
  CHECK_MPI(MPI_Comm_create(MPI_COMM_WORLD, singleProcGroup, &singleProcComm));
  MPI_Group_free(&singleProcGroup);

  nlohmann::json configFile{};
  GridDistributionConfig config{};
  for (auto periodic : m_periodicity) {
    if (not periodic) {
      throw std::runtime_error(
          "Single Process topology is only defined for periodic boundaries. "
          "But one dimension is not periodic!");
    }
  }
  return CartTopology<dim>{singleProcComm, config};
};

template <size_t dim>
void CartTopology<dim>::set_harddrive_writer() {
  int size{};
  CHECK_MPI(MPI_Comm_size(cart_comm(), &size));
  if (size > 1) {
    MPI_Group myGroup{};
    MPI_Group worldGroup{};
    CHECK_MPI(MPI_Comm_group(cart_comm(), &myGroup));
    CHECK_MPI(MPI_Comm_group(MPI_COMM_WORLD, &worldGroup));
    CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &size));

    std::vector<int> ranksWorld(size);
    for (int i = 0; i < size; i++) ranksWorld[i] = i;
    std::vector<int> ranksComm(size);
    CHECK_MPI(MPI_Group_translate_ranks(worldGroup, size, ranksWorld.data(),
                                        myGroup, ranksComm.data()));
    MPI_Group_free(&myGroup);
    MPI_Group_free(&worldGroup);
    auto found = std::find(ranksWorld.begin(), ranksWorld.end(), 0);
    size_t idx = found - ranksWorld.begin();
    if (ranksComm[idx] != MPI_UNDEFINED) m_harddriveWriter = true;
  } else {
    int worldRank{};
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);
    if (worldRank == 0) m_harddriveWriter = true;
  }
}

template class CartTopology<1>;
template class CartTopology<2>;
template class CartTopology<3>;
template class CartTopology<4>;
template class CartTopology<5>;
template class CartTopology<6>;

} /* namespace bsl6d */

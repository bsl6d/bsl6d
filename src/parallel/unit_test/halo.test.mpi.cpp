/*****************************************************************/
/*

\brief Testing of Halo structure

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.02.2022

*/
/*****************************************************************/
#include <gtest/gtest.h>

#include <boundaryHypercube.hpp>
#include <distributionFunction.hpp>
#include <mpiTopology.hpp>

#include "../halo.hpp"

nlohmann::json configFileHalo = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 2.0, 3.0, 4.0, 5.0, 6.0],
      "xvMin": [ 0.0,  0.0,  0.0,-4.0,-5.0,-6.0],
      "NxvPoints": [9, 12, 15, 18, 21, 24],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
})"_json;

class ParallelHalo : public ::testing::TestWithParam<bsl6d::AxisType> {
  public:
  bsl6d::CartTopology<6> topology{};
  // Need multiple of 3 to distribute between all MPI processes.
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{configFileHalo}};
  bsl6d::GridDistribution<3, 3> gridDistribution{};
  bsl6d::DistributionFunction f6d{};
  bsl6d::ScalarField scalarField{};

  std::map<bsl6d::AxisType, size_t> boundaryWidth{};

  bsl6d::AxisType const commAxis{GetParam()};

  ParallelHalo() {
    for (auto axis : bsl6d::xvAxes()) {
      boundaryWidth[axis] = static_cast<size_t>(axis) + 1;
    }
    int nProcs{};
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
    switch (commAxis) {
      case bsl6d::AxisType::x1:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{nProcs, 1, 1, 1, 1, 1};
        break;
      case bsl6d::AxisType::x2:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{1, nProcs, 1, 1, 1, 1};
        break;
      case bsl6d::AxisType::x3:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{1, 1, nProcs, 1, 1, 1};
        break;
      case bsl6d::AxisType::v1:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{1, 1, 1, nProcs, 1, 1};
        break;
      case bsl6d::AxisType::v2:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{1, 1, 1, 1, nProcs, 1};
        break;
      case bsl6d::AxisType::v3:
        configFileHalo["gridDistribution"]["parallel"]["Nprocs"] =
            std::vector<int>{1, 1, 1, 1, 1, nProcs};
        break;
    }
    topology =
        bsl6d::CartTopology<6>{bsl6d::GridDistributionConfig{configFileHalo}};
    gridDistribution = bsl6d::GridDistribution<3, 3>{grid, topology};
    f6d              = bsl6d::DistributionFunction{gridDistribution,
                                      bsl6d::DistributionFunctionConfig{}};
    Kokkos::deep_copy(f6d.device(), topology.my_cart_rank());
    scalarField = bsl6d::ScalarField{
        gridDistribution.create_xGridDistribution(), "scalarField"};
    scalarField.set_halo_width(commAxis, boundaryWidth[commAxis]);
    Kokkos::deep_copy(
        scalarField.device(),
        scalarField.gridDistribution().cart_topology().my_cart_rank());
  }

  double LInfDiff(Kokkos::View<double ******>::HostMirror const &boundaryView,
                  double res) {
    double err{};
    Kokkos::parallel_reduce(
        "test_halo_communication",
        Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>{
            0, boundaryView.size()},
        [=](size_t const i, double &lerr) {
          double *data_ptr = boundaryView.data();
          lerr             = std::max(lerr, std::abs(data_ptr[i] - res));
        },
        Kokkos::Max<double>(err));
    return err;
  }
};

TEST_P(ParallelHalo, Exchange) {
  bsl6d::HaloComm haloComm{gridDistribution.my_local_grid(), boundaryWidth};
  bsl6d::BoundaryHypercube boundary{haloComm.exchange(f6d, commAxis)};

  Kokkos::deep_copy(boundary.h_low(commAxis), boundary.low(commAxis));
  Kokkos::deep_copy(boundary.h_up(commAxis), boundary.up(commAxis));

  EXPECT_EQ(
      LInfDiff(boundary.h_low(commAxis), topology.my_low_neighbor(commAxis)), 0)
      << "Lower Boundary Exchange Failed with proc size"
      << topology.cart_dim_size(commAxis) << " and on axis "
      << bsl6d::axis_to_string(commAxis);
  EXPECT_EQ(
      LInfDiff(boundary.h_up(commAxis), topology.my_up_neighbor(commAxis)), 0)
      << "Upper Boundary Exchange Failed with proc size"
      << topology.cart_dim_size(commAxis) << " and on axis "
      << bsl6d::axis_to_string(commAxis);
}

TEST_P(ParallelHalo, ExchangeSingleProc) {
  bsl6d::HaloComm haloComm{gridDistribution.my_local_grid(), boundaryWidth};
  bsl6d::AxisType singleProcAxis =
      static_cast<bsl6d::AxisType>((static_cast<size_t>(commAxis) + 1) %
                                   static_cast<size_t>(bsl6d::AxisType::v3));
  bsl6d::BoundaryHypercube boundary{haloComm.exchange(f6d, singleProcAxis)};

  Kokkos::deep_copy(boundary.h_low(singleProcAxis),
                    boundary.low(singleProcAxis));
  Kokkos::deep_copy(boundary.h_up(singleProcAxis), boundary.up(singleProcAxis));

  EXPECT_EQ(LInfDiff(boundary.h_low(singleProcAxis),
                     topology.my_low_neighbor(singleProcAxis)),
            0)
      << "Lower Boundary Exchange Failed.";
  EXPECT_EQ(LInfDiff(boundary.h_up(singleProcAxis),
                     topology.my_up_neighbor(singleProcAxis)),
            0)
      << "Upper Boundary Exchange Failed.";
}

TEST_P(ParallelHalo, ErrorNoNextNeighbourCommunication) {
  std::map<bsl6d::AxisType, size_t> toLargeboundaryWidth{};
  for (auto axis : bsl6d::xvAxes()) {
    toLargeboundaryWidth[axis] = grid(axis).N() + 1;
  }
  bsl6d::HaloComm haloComm{grid, toLargeboundaryWidth};
  EXPECT_THROW(haloComm.exchange(f6d, commAxis), std::overflow_error);
}

TEST_P(ParallelHalo, ScalarField) {
  if (bsl6d::xAxes().end() !=
      std::find(bsl6d::xAxes().begin(), bsl6d::xAxes().end(), commAxis)) {
    bsl6d::HaloComm haloComm{
        gridDistribution.create_xGridDistribution().mpi_local_grid(),
        boundaryWidth};
    haloComm.exchange(scalarField, commAxis);
    Kokkos::deep_copy(scalarField.host(), scalarField.device());
    std::string outputString{"Boundary Exchange Failed with proc rank " +
                             std::to_string(topology.my_cart_rank()) +
                             " and on axis " + bsl6d::axis_to_string(commAxis)};
    std::map<bsl6d::AxisType, int> leftIdx{{bsl6d::AxisType::x1, 0},
                                           {bsl6d::AxisType::x2, 0},
                                           {bsl6d::AxisType::x3, 0}};
    std::map<bsl6d::AxisType, int> rightIdx{
        {bsl6d::AxisType::x1, scalarField.grid()(bsl6d::AxisType::x1).N() - 1},
        {bsl6d::AxisType::x2, scalarField.grid()(bsl6d::AxisType::x2).N() - 1},
        {bsl6d::AxisType::x3, scalarField.grid()(bsl6d::AxisType::x3).N() - 1}};
    leftIdx[commAxis]  = -1;
    rightIdx[commAxis] = -boundaryWidth[commAxis];
    EXPECT_DOUBLE_EQ(
        scalarField.host()(leftIdx[bsl6d::AxisType::x1],
                           leftIdx[bsl6d::AxisType::x2],
                           leftIdx[bsl6d::AxisType::x3]),
        scalarField.gridDistribution().cart_topology().my_low_neighbor(
            commAxis))
        << "Lower " << outputString;
    EXPECT_DOUBLE_EQ(
        scalarField.host()(rightIdx[bsl6d::AxisType::x1],
                           rightIdx[bsl6d::AxisType::x2],
                           rightIdx[bsl6d::AxisType::x3]),
        scalarField.gridDistribution().cart_topology().my_low_neighbor(
            commAxis))
        << "Lower " << outputString;
    leftIdx  = {{bsl6d::AxisType::x1, 0},
                {bsl6d::AxisType::x2, 0},
                {bsl6d::AxisType::x3, 0}};
    rightIdx = {
        {bsl6d::AxisType::x1, scalarField.grid()(bsl6d::AxisType::x1).N() - 1},
        {bsl6d::AxisType::x2, scalarField.grid()(bsl6d::AxisType::x2).N() - 1},
        {bsl6d::AxisType::x3, scalarField.grid()(bsl6d::AxisType::x3).N() - 1}};
    leftIdx[commAxis] = scalarField.grid()(commAxis).N();
    rightIdx[commAxis] =
        scalarField.grid()(commAxis).N() - 1 + boundaryWidth[commAxis];
    EXPECT_DOUBLE_EQ(
        scalarField.host()(leftIdx[bsl6d::AxisType::x1],
                           leftIdx[bsl6d::AxisType::x2],
                           leftIdx[bsl6d::AxisType::x3]),
        scalarField.gridDistribution().cart_topology().my_up_neighbor(commAxis))
        << "Upper " << outputString;
    EXPECT_DOUBLE_EQ(
        scalarField.host()(rightIdx[bsl6d::AxisType::x1],
                           rightIdx[bsl6d::AxisType::x2],
                           rightIdx[bsl6d::AxisType::x3]),
        scalarField.gridDistribution().cart_topology().my_up_neighbor(commAxis))
        << "Upper " << outputString;
  }
}

INSTANTIATE_TEST_SUITE_P(
    TestHaloExchangeAllDim,
    ParallelHalo,
    ::testing::Values(bsl6d::AxisType::x1,
                      bsl6d::AxisType::x2,
                      bsl6d::AxisType::x3,
                      bsl6d::AxisType::v1,
                      bsl6d::AxisType::v2,
                      bsl6d::AxisType::v3),
    [](const ::testing::TestParamInfo<ParallelHalo::ParamType> &info) {
      std::string name{"Axis_" + bsl6d::axis_to_string(info.param)};
      return name;
    });
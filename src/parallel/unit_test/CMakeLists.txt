bsl6d_add_mpitest(NAME  Parallel.Topology
                  FILES topology.test.mpi.cpp
                  PROCESSES 16)

bsl6d_add_mpitest(NAME  Parallel.GridDistribution
                  FILES gridDistribution.test.mpi.cpp
                  PROCESSES 48)

bsl6d_add_mpitest(NAME  Parallel.Halo
                  FILES halo.test.mpi.cpp
                  PROCESSES 3)

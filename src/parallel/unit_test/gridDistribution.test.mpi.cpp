/*****************************************************************/
/*

\brief Testing of initialization of MPI strucutre

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.02.2022

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <mpiTopology.hpp>

#include "../gridDistribution.hpp"

nlohmann::json configFileCartTopoMPI = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [10.0, 12.0, 14.0, 2.0, 2.5, 3.0],
      "xvMin": [ 0.0,  0.0,  0.0,-2.0,-2.5,-3.0],
      "NxvPoints": [10, 12, 14, 15, 17, 19],
      "position": ["L","L","L","N","N","N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [2, 2, 2, 3, 2, 1]
    }
  }
})"_json;

TEST(ParallelGridDistribution, PartitionedGrid) {
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileCartTopoMPI}};
  bsl6d::Grid<3, 3> grid{
      bsl6d::Grid<3, 3>{bsl6d::GridDistributionConfig{}}.partition(
          {2, 4, 6, 1, 1, 0}, {3, 7, 11, 6, 4, 5})};

  bsl6d::GridDistribution<3, 3> gridDistribution{};
  bsl6d::Grid<3, 3> gridEmpty{};
  EXPECT_EQ(gridDistribution.my_local_grid(), gridEmpty);
  EXPECT_NO_FATAL_FAILURE(gridDistribution.cart_topology());
  EXPECT_NO_FATAL_FAILURE(gridDistribution.create_xGridDistribution());
  EXPECT_NO_FATAL_FAILURE(gridDistribution.mpi_global_grid());
  gridDistribution = bsl6d::GridDistribution<3, 3>{grid, topology};

  switch (topology.my_cart_rank()) {
    case 0:
      EXPECT_EQ(grid.partition({2, 4, 6, 1, 1, 0}, {2, 5, 8, 2, 2, 5}),
                gridDistribution.my_local_grid())
          << "Rank: " << topology.my_cart_rank() << "\n";
      EXPECT_EQ(grid.partition({3, 6, 9, 5, 3, 0}, {3, 7, 11, 6, 4, 5}),
                gridDistribution.mpi_local_grid(47))
          << "Rank: 47\n";
      break;
    case 11:
      EXPECT_EQ(grid.partition({2, 4, 9, 5, 3, 0}, {2, 5, 11, 6, 4, 5}),
                gridDistribution.my_local_grid())
          << "Rank: " << topology.my_cart_rank();
      EXPECT_EQ(grid.partition({2, 4, 6, 1, 1, 0}, {2, 5, 8, 2, 2, 5}),
                gridDistribution.mpi_local_grid(0))
          << "Rank: 0\n";
      break;
    case 47:
      EXPECT_EQ(grid.partition({3, 6, 9, 5, 3, 0}, {3, 7, 11, 6, 4, 5}),
                gridDistribution.my_local_grid())
          << "Rank: " << topology.my_cart_rank();
      EXPECT_EQ(grid.partition({2, 4, 9, 5, 3, 0}, {2, 5, 11, 6, 4, 5}),
                gridDistribution.mpi_local_grid(11))
          << "Rank: 11\n";
      break;
  }

  bsl6d::CartTopology<3> topologyX{
      topology.split<3>({true, true, true, false, false, false})};
  bsl6d::GridDistribution<3, 0> gridDistX{
      gridDistribution.create_xGridDistribution()};
  switch (topologyX.my_cart_rank()) {
    case 0:
      EXPECT_EQ(grid.partition({2, 4, 6, 1, 1, 0}, {2, 5, 8, 1, 1, 0})
                    .create_xSpace(),
                gridDistX.my_local_grid())
          << "RankX: " << topologyX.my_cart_rank() << "\n"
          << "RankGlobal: " << topology.my_cart_rank() << "\n";
      break;
    case 3:
      EXPECT_EQ(grid.partition({2, 6, 9, 1, 1, 0}, {2, 7, 11, 1, 1, 0})
                    .create_xSpace(),
                gridDistX.my_local_grid())
          << "RankX: " << topologyX.my_cart_rank() << "\n"
          << "RankGlobal: " << topology.my_cart_rank() << "\n";
      break;
    case 7:
      EXPECT_EQ(grid.partition({3, 6, 9, 1, 1, 0}, {3, 7, 11, 1, 1, 0})
                    .create_xSpace(),
                gridDistX.my_local_grid())
          << "RankX: " << topologyX.my_cart_rank() << "\n"
          << "RankGlobal: " << topology.my_cart_rank() << "\n";
      break;
  }
}

TEST(ParallelGridDistribution, PartitionedGridDistribution) {
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileCartTopoMPI}};
  bsl6d::Grid<3, 3> partitionedGrid{
      bsl6d::Grid<3, 3>{bsl6d::GridDistributionConfig{}}.partition(
          {2, 3, 4, 3, 2, 1}, {9, 10, 11, 11, 9, 10})};

  bsl6d::GridDistribution<3, 3> partitionedGridDistribution{partitionedGrid,
                                                            topology};

  ASSERT_EQ(partitionedGridDistribution.mpi_global_grid(), partitionedGrid);
}

TEST(ParallelGridDistribution, GridForSingleProc) {
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileCartTopoMPI}};

  bsl6d::CartTopology<6> oneProcTopo{topology.create_single_proc_topology()};

  EXPECT_EQ(oneProcTopo.size(), 1);

  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};

  bsl6d::GridDistribution<3, 3> gridDistribution{grid, oneProcTopo};

  EXPECT_EQ(grid, gridDistribution.mpi_local_grid());
  EXPECT_EQ(grid, gridDistribution.mpi_global_grid());
}

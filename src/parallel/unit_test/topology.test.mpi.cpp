/*****************************************************************/
/*

\brief Testing of initialization of MPI strucutre

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.02.2022

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include "mpiTopology.hpp"

nlohmann::json configFileCartTopoMPI = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [10.0, 12.0, 14.0, 2.0, 2.5, 3.0],
      "xvMin": [ 0.0,  0.0,  0.0,-2.0,-2.5,-3.0],
      "NxvPoints": [10, 12, 14, 15, 17, 19],
      "position": ["L","L","L","N","N","N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 4, 1, 2, 1, 2]
    }
  }
})"_json;

class ParallelCartTopology : public ::testing::Test {
  public:
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileCartTopoMPI}};
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
};

TEST_F(ParallelCartTopology, SetupCartTop) {
  EXPECT_EQ(topology.size(), 16);

  for (auto axis : bsl6d::xvAxes())
    EXPECT_EQ(topology.cart_dim_size(axis),
              configFileCartTopoMPI["gridDistribution"]["parallel"]["Nprocs"]
                  .get<std::vector<int>>()[static_cast<size_t>(axis)]);

  std::array<std::array<int, 6>, 4> myRankResult{
      std::array<int, 6>{0, 0, 0, 0, 0, 0},
      std::array<int, 6>{0, 1, 0, 0, 0, 1},
      std::array<int, 6>{0, 2, 0, 1, 0, 1},
      std::array<int, 6>{0, 3, 0, 1, 0, 1}};
  switch (topology.my_cart_rank()) {
    case 0:
      EXPECT_EQ(topology.my_cart_coord(), myRankResult[0]);
      EXPECT_EQ(topology.cart_coord(5), myRankResult[1]);
      break;
    case 5:
      EXPECT_EQ(topology.my_cart_coord(), myRankResult[1]);
      EXPECT_EQ(topology.cart_coord(11), myRankResult[2]);
      break;
    case 11:
      EXPECT_EQ(topology.my_cart_coord(), myRankResult[2]);
      EXPECT_EQ(topology.cart_coord(15), myRankResult[3]);
      break;
    case 15:
      EXPECT_EQ(topology.my_cart_coord(), myRankResult[3]);
      EXPECT_EQ(topology.cart_coord(0), myRankResult[0]);
      break;
  }

  int compRes{};
  MPI_Comm_compare(topology.comm(), topology.cart_comm(), &compRes);
  EXPECT_EQ(compRes, MPI_CONGRUENT);
}

TEST_F(ParallelCartTopology, CartProcNeighbors) {
  std::array<std::array<int, 6>, 3> lowNeighborResults{
      std::array<int, 6>{0, 12, 0, 2, 0, 1},
      std::array<int, 6>{7, 3, 7, 5, 7, 6},
      std::array<int, 6>{15, 11, 15, 13, 15, 14}};

  switch (topology.my_cart_rank()) {
    case 0:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_low_neighbor(axis),
                  lowNeighborResults[0][static_cast<size_t>(axis)]);
      break;
    case 7:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_low_neighbor(axis),
                  lowNeighborResults[1][static_cast<size_t>(axis)]);
      break;
    case 15:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_low_neighbor(axis),
                  lowNeighborResults[2][static_cast<size_t>(axis)]);
      break;
  }

  std::array<std::array<int, 6>, 3> upNeighborResults{
      std::array<int, 6>{0, 4, 0, 2, 0, 1},
      std::array<int, 6>{7, 11, 7, 5, 7, 6},
      std::array<int, 6>{15, 3, 15, 13, 15, 14}};

  switch (topology.my_cart_rank()) {
    case 0:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_up_neighbor(axis),
                  upNeighborResults[0][static_cast<size_t>(axis)]);
      break;
    case 7:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_up_neighbor(axis),
                  upNeighborResults[1][static_cast<size_t>(axis)]);
      break;
    case 15:
      for (auto axis : bsl6d::xvAxes())
        EXPECT_EQ(topology.my_up_neighbor(axis),
                  upNeighborResults[2][static_cast<size_t>(axis)]);
      break;
  }
}

TEST_F(ParallelCartTopology, SplittingCartComm) {
  std::array<std::array<int, 3>, 4> myRankResultX{
      std::array<int, 3>{0, 0, 0}, std::array<int, 3>{0, 1, 0},
      std::array<int, 3>{0, 2, 0}, std::array<int, 3>{0, 3, 0}};

  bsl6d::CartTopology<3> topologyX{
      topology.split<3>({true, true, true, false, false, false})};
  switch (topologyX.my_cart_rank()) {
    case 0:
      EXPECT_EQ(topologyX.my_cart_coord(), myRankResultX[0])
          << "Proc Rank" << topologyX.my_cart_rank();
      break;
    case 1:
      EXPECT_EQ(topologyX.my_cart_coord(), myRankResultX[1])
          << "Proc Rank" << topologyX.my_cart_rank();
      break;
    case 2:
      EXPECT_EQ(topologyX.my_cart_coord(), myRankResultX[2])
          << "Proc Rank" << topologyX.my_cart_rank();
      break;
    case 3:
      EXPECT_EQ(topologyX.my_cart_coord(), myRankResultX[3])
          << "Proc Rank" << topologyX.my_cart_rank();
      break;
  }

  std::array<std::array<int, 3>, 4> myRankResultV{
      std::array<int, 3>{0, 0, 0}, std::array<int, 3>{0, 0, 1},
      std::array<int, 3>{1, 0, 0}, std::array<int, 3>{1, 0, 1}};

  bsl6d::CartTopology<3> topologyV{
      topology.split<3>({false, false, false, true, true, true})};
  switch (topologyV.my_cart_rank()) {
    case 0:
      EXPECT_EQ(topologyV.my_cart_coord(), myRankResultV[0])
          << "Proc Rank" << topologyV.my_cart_rank();
      break;
    case 1:
      EXPECT_EQ(topologyV.my_cart_coord(), myRankResultV[1])
          << "Proc Rank" << topologyV.my_cart_rank();
      break;
    case 2:
      EXPECT_EQ(topologyV.my_cart_coord(), myRankResultV[2])
          << "Proc Rank" << topologyV.my_cart_rank();
      break;
    case 3:
      EXPECT_EQ(topologyV.my_cart_coord(), myRankResultV[3])
          << "Proc Rank" << topologyV.my_cart_rank();
      break;
  }
}

TEST_F(ParallelCartTopology, OutputCommunicator) {
  EXPECT_EQ(true, topology.harddrive_write_communicator());

  bsl6d::CartTopology<3> topologyX{
      topology.split<3>({true, true, true, false, false, false})};
  int myRankX{};
  MPI_Comm_rank(topologyX.cart_comm(), &myRankX);
  int myRankW{};
  MPI_Comm_rank(MPI_COMM_WORLD, &myRankW);

  if (myRankW == 0) {
    EXPECT_EQ(true, topologyX.harddrive_write_communicator());
  }
}

void equal_topologies(bsl6d::CartTopology<6> a, bsl6d::CartTopology<6> b) {
  int comparisonResult{};
  bsl6d::CHECK_MPI(MPI_Comm_compare(a.comm(), b.comm(), &comparisonResult));
  EXPECT_EQ(comparisonResult, MPI_IDENT);
  bsl6d::CHECK_MPI(
      MPI_Comm_compare(a.cart_comm(), b.cart_comm(), &comparisonResult));
  EXPECT_EQ(comparisonResult, MPI_IDENT);
  for (auto axis : bsl6d::xvAxes()) {
    EXPECT_EQ(a.cart_dim_size(axis), b.cart_dim_size(axis));
  }
  EXPECT_EQ(a.harddrive_write_communicator(), b.harddrive_write_communicator());
};

TEST_F(ParallelCartTopology, RuleOf5) {
  bsl6d::CartTopology<6> copyConstructor{topology};
  equal_topologies(copyConstructor, topology);
  bsl6d::CartTopology<6> moveConstructor{std::move(copyConstructor)};
  EXPECT_EQ(copyConstructor.comm(), MPI_COMM_NULL);
  EXPECT_EQ(copyConstructor.cart_comm(), MPI_COMM_NULL);
  equal_topologies(moveConstructor, topology);
  bsl6d::CartTopology<6> moveAssignment{};
  moveAssignment = std::move(moveConstructor);
  EXPECT_EQ(moveConstructor.comm(), MPI_COMM_NULL);
  equal_topologies(moveAssignment, topology);
  bsl6d::CartTopology<6> copyAssignment{
      bsl6d::GridDistributionConfig{configFileCartTopoMPI}};
  MPI_Comm commReleasedAfterCopy = copyAssignment.comm();
  copyAssignment                 = moveAssignment;
  EXPECT_NE(copyAssignment.comm(), commReleasedAfterCopy);
  equal_topologies(copyAssignment, topology);
  // EXPECT_ANY_THROW(bsl6d::CHECK_MPI(MPI_Comm_free(&commReleasedAfterCopy)));
}
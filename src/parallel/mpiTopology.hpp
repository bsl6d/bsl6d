/*****************************************************************/
/*

\brief Initialize MPI cartesian Topology and Setup

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#pragma once

#include <memory>

#include <mpi.h>
#include <Kokkos_Core.hpp>

// Depending on process topology the grid and topology coordinates
// may not fit to together! This has to be solved at some point.
#include <grid.hpp>
#include <mpi_helper.hpp>

namespace bsl6d {

namespace Impl {
class MPICommHandler {
  public:
  MPICommHandler() = delete;
  MPICommHandler(MPI_Comm comm) : m_comm(comm) {
    if (comm == MPI_COMM_NULL) {
      throw std::runtime_error(
          "Cannot instantiate MPICommHandler with an MPI_COMM_NULL valued "
          "communicator");
    }
  }
  MPICommHandler(const MPICommHandler &)            = delete;
  MPICommHandler &operator=(const MPICommHandler &) = delete;
  MPICommHandler(MPICommHandler &&);
  MPICommHandler &operator=(MPICommHandler &&) = delete;
  ~MPICommHandler();

  MPI_Comm comm() const { return m_comm; }

  private:
  MPI_Comm m_comm;
};
}  // namespace Impl

/**
 * @class CartTopology
 * @brief Manages a Cartesian topology for parallel processing using MPI.
 *
 * This class handles the creation, management, and querying of Cartesian
 * topologies within an MPI context. It provides methods to access and
 * manipulate MPI communicators and their associated topological information.
 *
 *  @param config The configuration for the grid distribution.
 *
 *  @throws std::invalid_argument if any axis is not periodic. Currently, only
 * periodic boundaries are supported.
 *
 *
 * \b Constructor \b 1: Create from configuration struct
 * @code
 * GridDistributionConfig config;
 * CartTopology<3> topology(config);
 * @endcode
 *
 *  `GridDistributionConfig` contains a substruct `ConfigMPICartTopology`
 *
 * @code
 *   struct ConfigMPICartTopology {
 *   std::vector<int> Nprocs{};
 *   std::vector<bool> periodicity{};
 * };
 * @endcode
 *
 * The configuration parameter are parsed from the configuration file:
 * - Nprocs: The number of MPI processes used to split one resepctive dimension
 * (default: `{1,1,1,1,1,1}`)
 * - periodicity: A vector indicating whether each dimension is periodic
 * (default: `{true, true, true, true, true, true}`)
 *
 * Creates topology with new MPI communicator according to parameter in
 * configuration struct based on `MPI_Comm_World`
 *
 * \b Constructor \b 2: Pass existing MPI communicator and configuration
 * Returs topology with new MPI communicator according to
 * parameter in configuration struct
 *
 * \b Constructor \b 3: Pass existing MPI communicator
 * Returns topology with given communicator
 * @tparam dim The number of dimensions in the Cartesian topology.
 *
 * @todo What are MPI ressources that could leak?
 *  - Groups
 *  - Communicators
 *  - ... everything that contains a `create` in a function call?
 */
template <size_t dim>
class CartTopology {
  public:
  CartTopology() = default;

  CartTopology(GridDistributionConfig const &config);

  MPI_Comm cart_comm() const {
    if (m_cartComm == nullptr) return MPI_COMM_NULL;
    return m_cartComm.get()->comm();
  }
  MPI_Comm comm() const {
    if (m_cartComm == nullptr) return MPI_COMM_NULL;
    return m_comm.get()->comm();
  }

  int size() const;
  // This will not give correct result if dim != 6
  // e.g. 1D communicator for axes x1 would SegFault
  // e.g. 2D communicator for axes x1,x3 would SegFault
  // The array m_cartDimSizes has to be replaced by a map to work correctly
  // Substitute
  // int mpi_processes_in(...) const;
  int cart_dim_size(AxisType const axis) const {
    return m_cartDimSizes.at(static_cast<size_t>(axis));
  }
  // This will not give correct result if dim != 6
  // e.g. 1D communicator for axes x1 would SegFault
  // e.g. 2D communicator for axes x1,x3 would SegFault
  // The array m_periodicity has to be replaced by a map to work correctly
  // Substitute
  // bool is_periodic(...) const;
  bool periodic(AxisType const axis) const {
    return m_periodicity.at(static_cast<size_t>(axis));
  }
  // Substitute
  int my_cart_rank() const;

  std::array<int, dim> my_cart_coord() const;
  std::array<int, dim> cart_coord(int cartRank) const;
  /**
   * @brief Get the rank of the neighboring process in the negative direction
   * along the specified axis.
   *
   * @param axis The axis along which to find the neighboring process.
   * @return The rank of the neighboring process.
   */
  int my_low_neighbor(AxisType const axis) const;

  /**
   * @brief Get the rank of the neighboring process in the positive direction
   * along the specified axis.
   *
   * @param axis The axis along which to find the neighboring process.
   * @return The rank of the neighboring process.
   */
  int my_up_neighbor(AxisType const axis) const;

  /**
   * @brief Split the Cartesian communicator into a new communicator with a
   * subset of dimensions.
   *
   * @tparam subDim The number of dimensions in the new communicator.
   * @param remainDims An array specifying the dimensions to remain in the new
   * communicator.
   * @return A new Cartesian communicator with the specified subset of
   * dimensions.
   */

  // Check remainDims return dereferenced pointer to sub cart comm
  template <size_t subDim>
  CartTopology<subDim> split(std::array<int, dim> const remainDims) const {
    MPI_Comm commSplit{};
    CHECK_MPI(MPI_Cart_sub(cart_comm(), remainDims.begin(), &commSplit));
    return CartTopology<subDim>{commSplit};
  }
  /**
   * @brief Create a Cartesian topology for a single process.
   *
   * @return A Cartesian topology with a single process.
   */
  CartTopology<dim> create_single_proc_topology() const;

  bool harddrive_write_communicator() const { return m_harddriveWriter; };

  private:
  CartTopology(MPI_Comm _cartComm);
  CartTopology(MPI_Comm _baseComm, GridDistributionConfig const &config);
  void set_harddrive_writer();
  template <size_t subDim>
  friend class CartTopology;

  std::array<int, dim> m_cartDimSizes{};
  std::array<int, dim> m_periodicity{};
  // These two communicators are the resources managed by this class based on
  // RAII The communicators are identical except that the first one has topology
  // information attached
  //  which the second one does not have.
  std::shared_ptr<Impl::MPICommHandler> m_cartComm;
  std::shared_ptr<Impl::MPICommHandler> m_comm;
  // If the 6D communicator is split into multiple equivalent 3D
  // communicators only one of the 3D shall output its data.
  bool m_harddriveWriter{false};
};

}  // namespace bsl6d
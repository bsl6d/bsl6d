/*****************************************************************/
/*

\brief Mapping between local grids and process topology

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#pragma once

#include <grid.hpp>
#include <mpiTopology.hpp>

#include <mpi.h>

namespace bsl6d {
/**
 * @class GridDistribution
 * @brief Distributes a multi-dimensional grid over a Cartesian topology for
 * parallel processing.
 *
 * This class manages the distribution of a grid across multiple processors in a
 * Cartesian topology. It provides methods to access the global and local
 * partitions of the grid and to interact with the underlying Cartesian
 * topology.
 *
 *
 * @tparam xDim The number of dimensions in the x-direction.
 * @tparam vDim The number of dimensions in the v-direction.
 */
template <size_t xDim, size_t vDim>
class GridDistribution {
  public:
  GridDistribution() = default;
  GridDistribution(GridDistributionConfig const &);
  GridDistribution(Grid<xDim, vDim> const &_grid,
                   CartTopology<xDim + vDim> const &_topology);

  CartTopology<xDim + vDim> cart_topology() const { return topology; }
  KOKKOS_INLINE_FUNCTION
  Grid<xDim, vDim> mpi_global_grid() const { return globalGrid; };
  // To depricate
  KOKKOS_INLINE_FUNCTION
  Grid<xDim, vDim> my_local_grid() const { return myLocalGrid; };
  KOKKOS_INLINE_FUNCTION
  Grid<xDim, vDim> mpi_local_grid() const { return myLocalGrid; };
  Grid<xDim, vDim> mpi_local_grid(int const rank) const {
    if (globalGrid != Grid<xDim, vDim>{}) {
      return globalGrid.partition(rank_to_low_idx(rank), rank_to_up_idx(rank));
    } else {
      return Grid<xDim, vDim>{};
    }
  };

  GridDistribution<xDim, 0> create_xGridDistribution() const;

  private:
  Grid<xDim, vDim> globalGrid{};
  // Creating local grid on the fly becomes a long trace of
  // function calls needed to be used with KOKKOS_INLINE_FUNCTION.
  // To omit this we store a local copy of the processor local grid
  Grid<xDim, vDim> myLocalGrid{};
  CartTopology<xDim + vDim> topology{};
  std::array<size_t, xDim + vDim> NperMpiRank{};
  std::array<size_t, xDim + vDim> rank_to_low_idx(int const rank) const;
  std::array<size_t, xDim + vDim> rank_to_up_idx(int const rank) const;
};

}  // namespace bsl6d

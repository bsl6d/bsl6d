/*****************************************************************/
/*

\brief MPI helper to check for errors in MPI library

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#pragma once

#include <iostream>
#include <stdexcept>
#include <string>

#include <mpi.h>

namespace bsl6d {

template <typename T>
auto CHECK_MPI(T const &status) {
  if (status != MPI_SUCCESS) {
    T errClass{MPI_SUCCESS};
    MPI_Error_class(status, &errClass);
    std::string err{};
    err.resize(MPI_MAX_ERROR_STRING);
    int errLength{};
    MPI_Error_string(errClass, err.data(), &errLength);
    std::cerr << "ERROR found by bsl6d::CHECK_MPI\n";
    std::cerr << err << std::endl;
    MPI_Abort(MPI_COMM_WORLD, status);
  }
  return status;
}

}  // namespace bsl6d

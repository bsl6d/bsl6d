/*****************************************************************/
/*

\brief Mapping between local grids and process topology

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#include <gridDistribution.hpp>

namespace bsl6d {
template <size_t xDim, size_t vDim>
GridDistribution<xDim, vDim>::GridDistribution(
    GridDistributionConfig const &config) {
  if constexpr (xDim + vDim != 6) {
    throw std::runtime_error(
        "GridDistribution constructor based on GridDistributionConfig can only "
        "be used for xDim=vDim=3 it was called with xDim=" +
        std::to_string(xDim) + " and vDim=" + std::to_string(vDim));
  }
  if (config.configGrid.axes.size() < 6) {
    throw std::runtime_error(
        "GridDistribution constructor based on GridDistributionConfig can only "
        "be used for 6D configuration. The configuration only provided " +
        std::to_string(config.configGrid.axes.size()) + " axes.");
  }
  *this = GridDistribution<xDim, vDim>{Grid<xDim, vDim>{config},
                                       CartTopology<xDim + vDim>{config}};
}

template <size_t xDim, size_t vDim>
GridDistribution<xDim, vDim>::GridDistribution(
    Grid<xDim, vDim> const &_grid,
    CartTopology<xDim + vDim> const &_topology)
    : globalGrid{_grid}
    , topology{_topology} {
  for (auto axis : globalGrid.xvAxes) {
    if (globalGrid(axis).N() % topology.cart_dim_size(axis) > 0)
      throw std::invalid_argument(
          "The Number of grid points in a certain dimension "
          "has to be an integer multiple of processes in the "
          "cartesian process grid.");

    NperMpiRank[static_cast<size_t>(axis)] =
        globalGrid(axis).N() / topology.cart_dim_size(axis);
  }
  myLocalGrid = mpi_local_grid(topology.my_cart_rank());
};

template <size_t xDim, size_t vDim>
std::array<size_t, xDim + vDim> GridDistribution<xDim, vDim>::rank_to_low_idx(
    int const rank) const {
  std::array<size_t, xDim + vDim> low{};
  for (auto axis : globalGrid.xvAxes) {
    low[static_cast<size_t>(axis)] =
        topology.cart_coord(rank)[static_cast<size_t>(axis)] *
            NperMpiRank[static_cast<size_t>(axis)] +
        globalGrid(axis).low();
  }
  return low;
}

template <size_t xDim, size_t vDim>
std::array<size_t, xDim + vDim> GridDistribution<xDim, vDim>::rank_to_up_idx(
    int const rank) const {
  std::array<size_t, xDim + vDim> up{};
  std::array<size_t, xDim + vDim> low{rank_to_low_idx(rank)};
  for (auto axis : globalGrid.xvAxes) {
    up[static_cast<size_t>(axis)] = low[static_cast<size_t>(axis)] +
                                    NperMpiRank[static_cast<size_t>(axis)] - 1;
  }
  return up;
}

template <size_t xDim, size_t vDim>
GridDistribution<xDim, 0>
    GridDistribution<xDim, vDim>::create_xGridDistribution() const {
  if (globalGrid != Grid<xDim, vDim>{}) {
    std::array<int, xDim + vDim> keepXDim{};
    for (size_t i = 0; i < xDim; i++) keepXDim[i] = true;
    for (size_t i = xDim; i < xDim + vDim; i++) keepXDim[i] = false;
    CartTopology<xDim> topologyX{topology.template split<xDim>(keepXDim)};

    Grid<xDim, 0> gridX{globalGrid.create_xSpace()};

    return GridDistribution<xDim, 0>{gridX, topologyX};
  } else {
    return GridDistribution<xDim, 0>{};
  }
}

template class GridDistribution<3, 3>;
template class GridDistribution<3, 0>;
// template class GridDistribution<0,3>;

} /* namespace bsl6d */

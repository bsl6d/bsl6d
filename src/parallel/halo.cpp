/*****************************************************************/
/*

\brief Halo communication datastructure

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.03.2022

*/
/*****************************************************************/

#include "halo.hpp"

#include <iostream>

#include <mpi_helper.hpp>

namespace bsl6d {

namespace Impl {

void copy_distributionFunction_to_send_buffer(
    DistributionFunction const &f6d,
    BoundaryHypercube const &sendBoundary,
    AxisType const &axis) {
  size_t axisIdx{static_cast<size_t>(axis)};

  Kokkos::MDRangePolicy<Kokkos::Rank<6>> mdRangePol{
      {0, 0, 0, 0, 0, 0},
      {sendBoundary.low(axis).extent(0), sendBoundary.low(axis).extent(1),
       sendBoundary.low(axis).extent(2), sendBoundary.low(axis).extent(3),
       sendBoundary.low(axis).extent(4), sendBoundary.low(axis).extent(5)}};
  std::array<int, 6> readOffset{0, 0, 0, 0, 0, 0};
  readOffset[axisIdx] =
      f6d.grid()(axis).N() - sendBoundary.up(axis).extent(axisIdx);
  Kokkos::parallel_for(
      "copy_to_sendbuffer_distributionFunction" + axis_to_string(axis),
      mdRangePol,
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        sendBoundary.low(axis)(ix1, ix2, ix3, iv1, iv2, iv3) =
            f6d(ix1, ix2, ix3, iv1, iv2, iv3);
        sendBoundary.up(axis)(ix1, ix2, ix3, iv1, iv2, iv3) =
            f6d(readOffset[0] + ix1, readOffset[1] + ix2, readOffset[2] + ix3,
                readOffset[3] + iv1, readOffset[4] + iv2, readOffset[5] + iv3);
      });
}

void copy_scalarField_to_send_buffer(ScalarField const &scalarField,
                                     BoundaryHypercube const &sendBoundary,
                                     AxisType const &axis) {
  size_t axisIdx{static_cast<size_t>(axis)};

  Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
      {0, 0, 0},
      {sendBoundary.low(axis).extent(0), sendBoundary.low(axis).extent(1),
       sendBoundary.low(axis).extent(2)}};
  std::array<int, 6> readOffset{0, 0, 0, 0, 0, 0};
  readOffset[axisIdx] =
      scalarField.grid()(axis).N() - sendBoundary.up(axis).extent(axisIdx);
  Kokkos::parallel_for(
      "copy_to_sendbuffer_from_scalarField" + axis_to_string(axis),
      mdRangePolicy, KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        sendBoundary.low(axis)(ix1, ix2, ix3, 0, 0, 0) =
            scalarField(ix1, ix2, ix3);
        sendBoundary.up(axis)(ix1, ix2, ix3, 0, 0, 0) = scalarField(
            readOffset[0] + ix1, readOffset[1] + ix2, readOffset[2] + ix3);
      });
}

void copy_receive_buffer_to_scalarField(ScalarField const &scalarField,
                                        BoundaryHypercube const &recvBoundary,
                                        AxisType const &axis) {
  size_t axisIdx{static_cast<size_t>(axis)};
  Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
      {0, 0, 0},
      {recvBoundary.low(axis).extent(0), recvBoundary.low(axis).extent(1),
       recvBoundary.low(axis).extent(2)}};
  std::pair<std::array<int, 6>, std::array<int, 6>> writeOffset{
      {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
  writeOffset.first[axisIdx]  = -recvBoundary.low(axis).extent(axisIdx);
  writeOffset.second[axisIdx] = scalarField.grid()(axis).N();
  Kokkos::parallel_for(
      "copy_from_recvbuffer_to_scalarField" + axis_to_string(axis),
      mdRangePolicy, KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        scalarField(writeOffset.first[0] + ix1, writeOffset.first[1] + ix2,
                    writeOffset.first[2] + ix3) =
            recvBoundary.low(axis)(ix1, ix2, ix3, 0, 0, 0);
        scalarField(writeOffset.second[0] + ix1, writeOffset.second[1] + ix2,
                    writeOffset.second[2] + ix3) =
            recvBoundary.up(axis)(ix1, ix2, ix3, 0, 0, 0);
      });
}

}  // namespace Impl

HaloComm::HaloComm(Grid<3, 3> const grid,
                   std::map<bsl6d::AxisType, size_t> boundaryWidth) {
  std::map<bsl6d::AxisType, size_t> Nxv{};
  for (auto axis : grid.xvAxes) Nxv[axis] = grid(axis).N();
  m_sendBoundary = BoundaryHypercube{Nxv, boundaryWidth};
  m_recvBoundary = BoundaryHypercube{Nxv, boundaryWidth};
}
HaloComm::HaloComm(Grid<3, 0> const grid,
                   std::map<bsl6d::AxisType, size_t> boundaryWidth) {
  // BoundaryHypercube has been designed for 6 dimensions only.
  // The missing three dimensions are filled with 1 which removes any
  // effect of the these dimensions
  std::map<bsl6d::AxisType, size_t> Nxv;
  std::array<AxisType, 3> vAxes{AxisType::v1, AxisType::v2, AxisType::v3};
  for (auto axis : grid.xAxes) Nxv[axis] = grid(axis).N();
  for (auto axis : vAxes) {
    boundaryWidth[axis] = 1;
    Nxv[axis]           = 1;
  }
  m_sendBoundary = BoundaryHypercube{Nxv, boundaryWidth};
  m_recvBoundary = BoundaryHypercube{Nxv, boundaryWidth};
}

BoundaryHypercube HaloComm::exchange(DistributionFunction const &f6d,
                                     AxisType const &axis) const {
  size_t axisIdx{static_cast<size_t>(axis)};

  if (m_sendBoundary.low(axis).extent(axisIdx) > f6d.grid()(axis).N() ||
      m_sendBoundary.up(axis).extent(axisIdx) > f6d.grid()(axis).N()) {
    throw std::overflow_error(
        "Lower boundary or upper boundary exceeds next neighbour "
        "communication for axis " +
        bsl6d::axis_to_string(axis) +
        ". Reduce boundary width for interpolation or "
        "distribution.");
  }
  Impl::copy_distributionFunction_to_send_buffer(f6d, m_sendBoundary, axis);
  if (f6d.gridDistribution().cart_topology().cart_dim_size(axis) > 1) {
    Kokkos::fence("Start MPI Exchange");
    Kokkos::Profiling::pushRegion("HaloMPIExchangeDistributionFunction" +
                                  axis_to_string(axis));
#if BSL6D_ENABLE_CUDA_AWARE_MPI
    cuda_aware_mpi_exchange(
        axis, f6d.gridDistribution().cart_topology().my_low_neighbor(axis),
        f6d.gridDistribution().cart_topology().my_up_neighbor(axis),
        f6d.gridDistribution().cart_topology().cart_comm());
#else
    non_cuda_aware_mpi_exchange(
        axis, f6d.gridDistribution().cart_topology().my_low_neighbor(axis),
        f6d.gridDistribution().cart_topology().my_up_neighbor(axis),
        f6d.gridDistribution().cart_topology().cart_comm());
#endif
    Kokkos::Profiling::popRegion();
  } else {
    Kokkos::Profiling::pushRegion("HaloNonMPIExchangeDistributionFunction" +
                                  axis_to_string(axis));
    Kokkos::deep_copy(m_recvBoundary.low(axis), m_sendBoundary.up(axis));
    Kokkos::deep_copy(m_recvBoundary.up(axis), m_sendBoundary.low(axis));
    Kokkos::Profiling::popRegion();
  }

  // We need to define which axis is stored in the boundary
  // for the reciever of the call to exchange().
  // This is only a shallow copy and should be cheap
  BoundaryHypercube recvBoundary{m_recvBoundary};
  recvBoundary.set_my_axis(axis);
  return recvBoundary;
}

void HaloComm::exchange(bsl6d::ScalarField const &scalarField,
                        bsl6d::AxisType const &axis) const {
  size_t axisIdx{static_cast<size_t>(axis)};

  Kokkos::Profiling::pushRegion("HaloMPIExchangeScalarField" +
                                axis_to_string(axis));
  if (m_sendBoundary.low(axis).extent(axisIdx) > scalarField.grid()(axis).N() ||
      m_sendBoundary.up(axis).extent(axisIdx) > scalarField.grid()(axis).N()) {
    throw std::overflow_error(
        "Lower boundary or upper boundary exceeds next neighbour "
        "communication for axis " +
        bsl6d::axis_to_string(axis) +
        ". Reduce boundary width for interpolation or "
        "distribution.");
  }

  Impl::copy_scalarField_to_send_buffer(scalarField, m_sendBoundary, axis);
  if (scalarField.gridDistribution().cart_topology().cart_dim_size(axis) > 1) {
    Kokkos::fence("Start MPI Exchange");
    Kokkos::Profiling::pushRegion("HaloMPIExchangeScalarField" +
                                  axis_to_string(axis));
#if BSL6D_ENABLE_CUDA_AWARE_MPI
    cuda_aware_mpi_exchange(
        axis,
        scalarField.gridDistribution().cart_topology().my_low_neighbor(axis),
        scalarField.gridDistribution().cart_topology().my_up_neighbor(axis),
        scalarField.gridDistribution().cart_topology().cart_comm());
#else
    non_cuda_aware_mpi_exchange(
        axis,
        scalarField.gridDistribution().cart_topology().my_low_neighbor(axis),
        scalarField.gridDistribution().cart_topology().my_up_neighbor(axis),
        scalarField.gridDistribution().cart_topology().cart_comm());
#endif
    Kokkos::Profiling::popRegion();
  } else {
    Kokkos::Profiling::pushRegion("HaloNonMPIExchangeScalarField" +
                                  axis_to_string(axis));
    Kokkos::deep_copy(m_recvBoundary.low(axis), m_sendBoundary.up(axis));
    Kokkos::deep_copy(m_recvBoundary.up(axis), m_sendBoundary.low(axis));
    Kokkos::Profiling::popRegion();
  }
  Impl::copy_receive_buffer_to_scalarField(scalarField, m_recvBoundary, axis);

  Kokkos::Profiling::popRegion();
};

#if BSL6D_ENABLE_CUDA_AWARE_MPI
void HaloComm::cuda_aware_mpi_exchange(AxisType const &axis,
                                       int const &lowNeighbourRank,
                                       int const &upNeighbourRank,
                                       MPI_Comm const &comm) const {
  MPI_Status lowSendStat{};
  int mpiSendTag{0};
  CHECK_MPI(MPI_Sendrecv(
      m_sendBoundary.low(axis).data(), m_sendBoundary.low(axis).size(),
      MPI_DOUBLE, lowNeighbourRank, mpiSendTag, m_recvBoundary.up(axis).data(),
      m_recvBoundary.up(axis).size(), MPI_DOUBLE, upNeighbourRank, mpiSendTag,
      comm, &lowSendStat));
  CHECK_MPI(lowSendStat.MPI_ERROR);
  MPI_Status upSendStat{};
  CHECK_MPI(MPI_Sendrecv(
      m_sendBoundary.up(axis).data(), m_sendBoundary.up(axis).size(),
      MPI_DOUBLE, upNeighbourRank, mpiSendTag, m_recvBoundary.low(axis).data(),
      m_recvBoundary.low(axis).size(), MPI_DOUBLE, lowNeighbourRank, mpiSendTag,
      comm, &upSendStat));
  CHECK_MPI(upSendStat.MPI_ERROR);
}
#else
void HaloComm::non_cuda_aware_mpi_exchange(AxisType const &axis,
                                           int const &lowNeighbourRank,
                                           int const &upNeighbourRank,
                                           MPI_Comm const &comm) const {
  // This is only a shallow copy.
  // Setting the axis is a non-const operation but required to
  // actually access the data via h_low and h_up memberfunctions of
  // BoundaryHypercube
  BoundaryHypercube sendBoundary{m_sendBoundary};
  BoundaryHypercube recvBoundary{m_recvBoundary};
  sendBoundary.set_my_axis(axis);
  recvBoundary.set_my_axis(axis);

  Kokkos::deep_copy(sendBoundary.h_low(axis), sendBoundary.low(axis));
  Kokkos::deep_copy(sendBoundary.h_up(axis), sendBoundary.up(axis));
  int mpiSendTag{0};
  /*Send lower boundary*/
  MPI_Status lowSendStat{};
  CHECK_MPI(MPI_Sendrecv(
      sendBoundary.h_low(axis).data(), sendBoundary.low(axis).size(),
      MPI_DOUBLE, lowNeighbourRank, mpiSendTag, recvBoundary.h_up(axis).data(),
      recvBoundary.up(axis).size(), MPI_DOUBLE, upNeighbourRank, mpiSendTag,
      comm, &lowSendStat));
  CHECK_MPI(lowSendStat.MPI_ERROR);
  /*Send upper boundary*/
  MPI_Status upSendStat{};
  CHECK_MPI(MPI_Sendrecv(
      sendBoundary.h_up(axis).data(), sendBoundary.up(axis).size(), MPI_DOUBLE,
      upNeighbourRank, mpiSendTag, recvBoundary.h_low(axis).data(),
      recvBoundary.low(axis).size(), MPI_DOUBLE, lowNeighbourRank, mpiSendTag,
      comm, &upSendStat));
  CHECK_MPI(upSendStat.MPI_ERROR);
  Kokkos::deep_copy(recvBoundary.low(axis), recvBoundary.h_low(axis));
  Kokkos::deep_copy(recvBoundary.up(axis), recvBoundary.h_up(axis));
}
#endif

}  // namespace bsl6d
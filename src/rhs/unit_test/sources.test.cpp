/*****************************************************************/
/*
\Background electric field solver implementation

\author Mario Raeth
\department NMPP
\email  Mario .Raeth@ipp.mpg.de
\date   15.12.2023

*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <config.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <io_init.hpp>
#include <mathNorms.hpp>
#include <mpiTopology.hpp>
#include <quasiNeutral.hpp>

#include "../sources.hpp"

class RHSSources : public ::testing::Test {
  public:
  static nlohmann::json create_config_file_source_v() {
    nlohmann::json configFile = R"({      
      "distributionFunction": {
        "q": 1.0,
        "m": 1.0
      },
      "simulationTime": {
        "dt": 0.1,
        "T": 1.0,
        "restart": false
      },
      "rightHandSide": {
        "kappaT": 0.1,
        "kappaN": 0.1,
        "gradientType": "boussinesq"
      },
      "gridDistribution": {
        "grid":{
          "axes": ["x1","x2","x3","v1","v2","v3"],
          "xvMax": [1.5, 2.0, 3.0, 4.0, 4.0, 4.0],
          "xvMin": [0.0, 0.0, 0.0,-4.0,-4.0,-4.0],
          "NxvPoints": [2, 4, 6, 9, 11, 13],
          "position": ["L", "L", "L", "N", "N", "N"],
          "rotateGyroFreq": true
          }
      },
      "initializationDistributionFunction":{
        "initFunction": "planewave_dens_perturbation",
        "alpha": 1.0,
        "mk": [1.0,0.0,0.0],
        "temperature": 1.0
      },
      "fieldSolver":{
        "quasiNeutral": {},
        "backgroundField": {
          "Bx3": 1.0
        }
      }
    })"_json;
    return configFile;
  }
  static nlohmann::json create_config_file_profile() {
    nlohmann::json configFile = R"({      
      "distributionFunction": {
        "q": 1.0,
        "m": 1.0
      },
      "characteristic": {
        "Bx3": 0.0
      },
      "simulationTime": {
        "dt": 0.5,
        "T": 30.0,
        "restart": false
      },
      "rightHandSide": {
        "kappaT": 0.0,
        "kappaN": 0.0,
        "gradientType": "nonlinear"
      },
      "gridDistribution": {
        "grid":{
          "axes": ["v1","v2"],
          "xvMax": [6.0,6.0],
          "xvMin": [-6.0,-6.0],
          "NxvPoints": [17,17],
          "position": ["N", "N"],
          "rotateGyroFreq": false
        }
      },
      "fieldSolver": {
        "quasiNeutral": {
          "substractFluxSurfaceAverage": true
        }
      },
      "initializationDistributionFunction":{
        "initFunction": "planewave_dens_perturbation",
        "alpha": 0.01,
        "mk": [1.0,0.0,0.0],
        "temperature": 1.0
      }  
    })"_json;
    return configFile;
  }
};

TEST_F(RHSSources, sourceV) {
  auto [simulationTime, f6d] = bsl6d::initialize(create_config_file_source_v());
  bsl6d::Sources source{bsl6d::RightHandSideConfig(bsl6d::config_file())};
  bsl6d::QuasiNeutral solver{bsl6d::FieldSolverConfig{bsl6d::config_file()}};

  bsl6d::MaxwellFields maxwellFields = solver(f6d, simulationTime, 0.1);

  // Change to `integral f6d dxdv` from `integral |f6d| dxdv`.
  // which is the acutal mass since we do not have a positive
  // definit distribution function
  double mass = bsl6d::L1_norm(f6d);

  source(f6d, maxwellFields, simulationTime.current_T(), simulationTime.dt());

  bsl6d::Impl::is_positive_definite(f6d);
  EXPECT_DOUBLE_EQ(bsl6d::L1_norm(f6d) - mass, 0.0);
}

TEST_F(RHSSources, profiles) {
  auto [simulationTime, f6d] = bsl6d::initialize(create_config_file_profile());

  bsl6d::Sources source{bsl6d::RightHandSideConfig(bsl6d::config_file())};
  bsl6d::QuasiNeutral solver{bsl6d::FieldSolverConfig{bsl6d::config_file()}};

  bsl6d::MaxwellFields maxwellFields = solver(f6d, simulationTime);

  while (simulationTime.continue_advection()) {
    source(f6d, maxwellFields, simulationTime.current_T(), simulationTime.dt());
    simulationTime.advance_current_T_by_dt();
  }

  bsl6d::Impl::is_positive_definite(f6d);
  EXPECT_NEAR(
      bsl6d::L1_norm(f6d) /
          f6d.gridDistribution().mpi_global_grid().spatial_domain_volume(),
      1.0, 0.1);
}

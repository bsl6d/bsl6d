/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
       by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2023

*/
/*****************************************************************/

#pragma once

#include <utility>

#include <Kokkos_Core.hpp>

#include <baseFieldSolver.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <gridDistribution.hpp>
#include <scalarField.hpp>
#include <time.hpp>
#include <vectorField.hpp>

namespace bsl6d {

class Sources {
  public:
  Sources() = default;
  Sources(RightHandSideConfig const &config);

  void operator()(DistributionFunction f6d,
                  MaxwellFields maxwellFields,
                  double t0,
                  double dt);

  void boussinesqGradient(DistributionFunction f6d,
                          MaxwellFields maxwellFields,
                          double t0,
                          double dt);

  void initProfile(DistributionFunction f6d,
                   MaxwellFields maxwellFields,
                   double t0,
                   double dt);

  private:
  GridDistribution<3, 3> m_gridDistribution{};

  RightHandSideConfig::Config m_config{};
  DistributionInitializationConfig::Config m_initConfig{};
};

} /* namespace bsl6d */

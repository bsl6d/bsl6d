/*****************************************************************/
/*
\brief Base structure to define to define 3D scalar field

\author Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth.mpg.de
\date   10.02.2022

*/
/*****************************************************************/

#include <iostream>
#include <random>

#include <distributionFunction.hpp>
#include <initializationFunctions.hpp>
#include <io_init.hpp>
#include <openPMDInterface.hpp>
#include <sources.hpp>
#include <time.hpp>

namespace bsl6d {

Sources::Sources(RightHandSideConfig const &config) : m_config{config.config} {
  m_initConfig = config_file()["initializationDistributionFunction"]
                     .get<DistributionInitializationConfig::Config>();
}

void Sources::boussinesqGradient(DistributionFunction f6d,
                                 MaxwellFields maxwellFields,
                                 double t0,
                                 double dt) {
  bsl6d::Grid<3, 3> grid{f6d.gridDistribution().my_local_grid()};

  double kappaN = m_config.kappaN;
  double kappaT = m_config.kappaT;

  double temperature = m_initConfig.temperature;
  double mass        = f6d.m();

  Kokkos::parallel_for(
      "Init density distribution", grid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        std::array<size_t, 6> xv{ix1, ix2, ix3, iv1, iv2, iv3};

        double fac = 1.0;

        double sourcev = 0.0;

        for (auto axis : grid.vAxes) {
          if (grid(axis).N() != 1) {
            fac *= Impl::gauss(grid(axis)(xv[static_cast<size_t>(axis)]), 0.0,
                               temperature);
            sourcev += 0.5 * mass * grid(axis)(xv[static_cast<size_t>(axis)]) *
                           grid(axis)(xv[static_cast<size_t>(axis)]) -
                       0.5 * temperature;
          }
        }
        f6d(ix1, ix2, ix3, iv1, iv2, iv3) +=
            dt * kappaT * sourcev * fac *
            maxwellFields.E(AxisType::x2)(ix1, ix2, ix3);

        f6d(ix1, ix2, ix3, iv1, iv2, iv3) +=
            dt * kappaN * fac * maxwellFields.E(AxisType::x2)(ix1, ix2, ix3);
      });
}

void Sources::initProfile(DistributionFunction f6d,
                          MaxwellFields maxwellFields,
                          double t0,
                          double dt) {
  double initTime = m_config.rampTime;
  double sigma    = m_config.rampSTD;

  if (t0 < initTime) {
    double kx0 =
        2 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
    bsl6d::Grid<3, 3> grid{f6d.gridDistribution().my_local_grid()};

    std::mt19937 generator{
        static_cast<unsigned int>(f6d.gridDistribution()
                                      .create_xGridDistribution()
                                      .cart_topology()
                                      .my_cart_rank())};
    Kokkos::View<double ***> perturbation{
        "RandValues", grid(bsl6d::AxisType::x1).N(),
        grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()};
    Kokkos::View<double ***>::HostMirror h_perturbation{
        Kokkos::create_mirror_view(perturbation)};

    // mt19937.operator() is not const and cannot be used in a Lambda
    for (size_t i0 = 0; i0 < grid(bsl6d::AxisType::x1).N(); i0++)
      for (size_t i1 = 0; i1 < grid(bsl6d::AxisType::x2).N(); i1++)
        for (size_t i2 = 0; i2 < grid(bsl6d::AxisType::x3).N(); i2++) {
          h_perturbation(i0, i1, i2) =
              (double)(generator() - (double)generator.max() / 2.) * 2. /
              generator.max();
        }

    double kappaN = m_config.kappaN;
    double kappaT = m_config.kappaT;
    double alpha  = m_initConfig.alpha;

    Kokkos::deep_copy(perturbation, h_perturbation);
    Kokkos::parallel_for(
        "InitKernelGradient", grid.mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1,
                      size_t iv2, size_t iv3) {
          std::array<size_t, 6> xv{ix1, ix2, ix3, iv1, iv2, iv3};

          double x1 = grid(bsl6d::AxisType::x1)(ix1);
          double v1 = grid(bsl6d::AxisType::v1)(iv1);
          double v2 = grid(bsl6d::AxisType::v2)(iv2);

          double n =
              1 + kappaN * sin(kx0 * (x1 + v2 * cos(t0) - v1 * sin(t0))) +
              alpha / (0.5 * initTime / dt) * perturbation(ix1, ix2, ix3);
          double t = 1 + kappaT * sin(kx0 * (x1 + v2 * cos(t0) - v1 * sin(t0)));

          double fac = n;

          for (auto axis : grid.vAxes)
            if (grid(axis).N() != 1)
              fac *= Impl::gauss(grid(axis)(xv[static_cast<size_t>(axis)]), 0.0,
                                 t);

          f6d(ix1, ix2, ix3, iv1, iv2, iv3) +=
              fac *
              exp(-(t0 - 0.5 * initTime) * (t0 - 0.5 * initTime) /
                  (2 * sigma * sigma)) /
              sqrt(2 * M_PI) / sigma * dt;
        });
  }
}

void Sources::operator()(DistributionFunction f6d,
                         MaxwellFields maxwellFields,
                         double t0,
                         double dt) {
  // Initialization
  if (m_config.gradientType == "nonlinear") {
    initProfile(f6d, maxwellFields, t0, dt);
  }

  // Continous Sources
  if (m_config.gradientType == "boussinesq") {
    boussinesqGradient(f6d, maxwellFields, t0, dt);
  }
}

} /* namespace bsl6d */

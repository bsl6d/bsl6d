FILE(GLOB_RECURSE BSL6D_SOURCES *.cpp)
FILE(GLOB_RECURSE BSL6D_UTESTS *.test.cpp) #Normal unit tests
FILE(GLOB_RECURSE BSL6D_UMPITESTS *.test.mpi.cpp) #Unit tests with MPI_NUM_RANKS>1
FILE(GLOB_RECURSE BSL6D_PTESTS *.test.perf.cpp) #Performance tests
FILE(GLOB_RECURSE BSL6D_ITESTS *.test.integration.cpp) # Integration tests

LIST(FILTER BSL6D_SOURCES EXCLUDE REGEX "(\\.test\\.)")

#*****************************************************************#
IF(CMAKE_BUILD_TYPE MATCHES "Release")
  ADD_COMPILE_OPTIONS(${BSL6D_CXX_FLAGS})
ELSEIF(CMAKE_BUILD_TYPE MATCHES "Debug")
  ADD_COMPILE_OPTIONS(${BSL6D_CXX_DEBUG_FLAGS})
ENDIF()

ADD_LIBRARY(bsl6d ${BSL6D_SOURCES})
TARGET_LINK_LIBRARIES(bsl6d PUBLIC
                        Kokkos::kokkos
                        MPI::MPI_CXX
                        hdf5::hdf5
                        openPMD::openPMD
                        nlohmann_json::nlohmann_json
                        cmake_git_version_tracking)
if(BSL6D_HEFFTE_SET_BY_FETCH_CONTENT)
  target_link_libraries(bsl6d PUBLIC
                          Heffte)
else()
  target_link_libraries(bsl6d PUBLIC
                          Heffte::Heffte)
endif()

IF(likwid_FOUND)
        TARGET_LINK_LIBRARIES(bsl6d PRIVATE
                               OpenMP::OpenMP_CXX
                               bsl6d::likwid)
ENDIF()
IF(rocprofiler_FOUND)
        TARGET_LINK_LIBRARIES(bsl6d PRIVATE
                               bsl6d::roctracer 
                               bsl6d::roctx)
ENDIF()
IF(BSL6D_ENABLE_CUDA_AWARE_MPI)
  TARGET_COMPILE_DEFINITIONS(bsl6d
                                PUBLIC
                                -DBSL6D_ENABLE_CUDA_AWARE_MPI=1)
ELSE()
  TARGET_COMPILE_DEFINITIONS(bsl6d
                                PUBLIC
                                -DBSL6D_ENABLE_CUDA_AWARE_MPI=0)
ENDIF()

# Search for include files
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/advection")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/characteristics")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/datastructures")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/field_solvers")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/grid")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/interpolator")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/int_test")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/io/data")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/io/user")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/operators")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/parallel")
INCLUDE_DIRECTORIES("${CMAKE_SOURCE_DIR}/src/rhs")

IF(BSL6D_ENABLE_PROFILING)
  ADD_SUBDIRECTORY(interpolator/perf_test)
ENDIF()

IF(BSL6D_ENABLE_TESTS)
  ADD_LIBRARY(bsl6d_gtest_main 
                bsl6d.test.main.cpp)
  TARGET_LINK_LIBRARIES(bsl6d_gtest_main PUBLIC
                          MPI::MPI_CXX
                          Kokkos::kokkos
                          GTest::gtest)
  ADD_SUBDIRECTORY(io/data/unit_test)
  ADD_SUBDIRECTORY(operators/unit_test)
  ADD_SUBDIRECTORY(parallel/unit_test)
  ADD_SUBDIRECTORY(int_test)

  ADD_EXECUTABLE(bsl6d_unit_tests 
                    ${BSL6D_UTESTS})
  TARGET_LINK_LIBRARIES(bsl6d_unit_tests
                          bsl6d_gtest_main
                          bsl6d)
  GTEST_DISCOVER_TESTS(bsl6d_unit_tests 
                          DISCOVERY_MODE PRE_TEST)
  
  ADD_CUSTOM_COMMAND(TARGET bsl6d_unit_tests POST_BUILD
                      COMMAND ln -sfn ${CMAKE_SOURCE_DIR}/src/io/user/bsl6dConfig.json
                                  $<TARGET_FILE_DIR:bsl6d_unit_tests>/bsl6dConfig.json)

  ADD_CUSTOM_COMMAND(TARGET bsl6d_unit_tests POST_BUILD
          COMMAND ln -sfn ${CMAKE_SOURCE_DIR}/src/io/data/unit_test/000000_TestopenPMDIO.h5
          $<TARGET_FILE_DIR:bsl6d_unit_tests>/000000_TestopenPMDIO.h5)
  ADD_CUSTOM_COMMAND(TARGET bsl6d_unit_tests POST_BUILD
          COMMAND ln -sfn ${CMAKE_SOURCE_DIR}/src/io/data/unit_test/000001_TestopenPMDIO.h5
          $<TARGET_FILE_DIR:bsl6d_unit_tests>/000001_TestopenPMDIO.h5)
ENDIF()

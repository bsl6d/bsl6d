/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
        by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#include <time.hpp>

namespace bsl6d {

SimulationTime::SimulationTime(SimulationTimeConfig const &conf,
                               size_t startIteration,
                               double startTime,
                               double gridPhase)
    : m_dt{conf.config.dt}
    , m_currentT{startTime}
    , m_finalT{conf.config.T + m_currentT}
    , m_N{startIteration}
    , m_currentPhase{gridPhase}
    , m_Nmax{startIteration +
             static_cast<size_t>(std::lround(conf.config.T / dt()))}
    , m_gyroFrequency{conf.config.gyroFreq} {

      };

SimulationTime::SimulationTime(SimulationTimeConfig const &conf,
                               size_t startIteration)
    : SimulationTime(conf,
                     startIteration,
                     startIteration * conf.config.dt,
                     startIteration * conf.config.dt) {}

void SimulationTime::advance_current_T_by_dt() {
  m_N++;
  m_currentT += dt();
  m_currentPhase = std::fmod(m_currentPhase + m_dt * m_gyroFrequency, 2 * M_PI);
}

bool SimulationTime::continue_advection() const {
  if (m_N < m_Nmax) {
    return true;
  } else {
    return false;
  }
}

} /* namespace bsl6d */

#include <gtest/gtest.h>

#include <advectorArakawa.hpp>
#include <config.hpp>
#include <initializationFunctions.hpp>
#include <integrator.hpp>
#include <mathNorms.hpp>
#include <poisson.hpp>
#include <quasiNeutral.hpp>
#include <time.hpp>

class AdvectionArakawa : public ::testing::Test {
  public:
  AdvectionArakawa() {
    // Testing the convergence of the ArakawaScheme this configuration
    // can be used.
    config.configGrid.axes      = {"x1", "x2", "x3", "v1", "v2", "v3"};
    config.configGrid.NxvPoints = {100, 90, 4, 7, 5, 3};
    config.configGrid.position  = {"L", "L", "L", "N", "N", "N"};
    config.configGrid.xvMax     = {2 * M_PI, 2 * M_PI, 1.0, 1.0, 1.0, 1.0};
    config.configGrid.xvMin     = {0.0, 0.0, 0.0, -1.0, -1.0, -1.0};
  };
  bsl6d::GridDistributionConfig config{};

  // The analytical expressions 1 and 2 may only depend on x1 and x2.
  // If we add a dependence on x3,v1,v2,v3 we can not copy data from
  // the initialized distributionFunction f into the scalarField g
  // and expect the same results.
  bsl6d::DistributionFunction analytical1() {
    bsl6d::DistributionFunction f{config, bsl6d::DistributionFunctionConfig{}};
    double k0x1{};
    double k0x2{};
    k0x1 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
    k0x2 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();
    auto analyticalExpression = KOKKOS_LAMBDA(double x1, double x2, double x3,
                                              double v1, double v2, double v3)
                                    ->double {
      x1 *= k0x1;
      x2 *= k0x2;
      return std::sin(2.0 * x1) + std::cos(x2);
    };
    bsl6d::Impl::fill(f, analyticalExpression);
    return f;
  }

  bsl6d::DistributionFunction analytical2() {
    bsl6d::DistributionFunction f{config, bsl6d::DistributionFunctionConfig{}};
    double k0x1{};
    double k0x2{};
    k0x1 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
    k0x2 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();

    auto analyticalExpression = KOKKOS_LAMBDA(double x1, double x2, double x3,
                                              double v1, double v2, double v3)
                                    ->double {
      x1 *= k0x1;
      x2 *= k0x2;
      return std::cos(x1) - std::sin(3.0 * x2);
    };
    bsl6d::Impl::fill(f, analyticalExpression);
    return f;
  }

  bsl6d::DistributionFunction analytical3() {
    bsl6d::DistributionFunction f{config, bsl6d::DistributionFunctionConfig{}};
    double k0x1{};
    double k0x2{};
    k0x1 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
    k0x2 = 2 * M_PI /
           f.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();

    auto analyticalExpression = KOKKOS_LAMBDA(double x1, double x2, double x3,
                                              double v1, double v2, double v3)
                                    ->double {
      x1 *= k0x1;
      x2 *= k0x2;
      return std::cos(2.0 * x1) * std::sin(x2) * x3 - x3 * x3;
    };
    bsl6d::Impl::fill(f, analyticalExpression);
    return f;
  }

  // {expr1,{expr2,expr3}}
  Kokkos::View<double ******> poisson_bracket_in_poisson_bracket(
      bsl6d::DistributionFunction const &f1,
      bsl6d::DistributionFunction const &f2,
      bsl6d::DistributionFunction const &f3) {
    bsl6d::Arakawa arakawa{};
    bsl6d::ScalarField g{f3.gridDistribution().create_xGridDistribution(), ""};

    auto fxSpace = Kokkos::subview(f3.device(), Kokkos::ALL(), Kokkos::ALL(),
                                   Kokkos::ALL(), 0, 0, 0);
    Kokkos::deep_copy(g.device(), fxSpace);

    auto poissonBracket = arakawa(f2, g);
    fxSpace = Kokkos::subview(poissonBracket, Kokkos::ALL(), Kokkos::ALL(),
                              Kokkos::ALL(), 0, 0, 0);
    Kokkos::deep_copy(g.device(), fxSpace);

    poissonBracket = arakawa(f1, g);

    return poissonBracket;
  }
};

TEST_F(AdvectionArakawa, IterationPattern) {
  bsl6d::DistributionFunction f{analytical1()};
  bsl6d::ScalarField g{f.gridDistribution().create_xGridDistribution(), ""};
  auto fxSpace{Kokkos::subview(f.device(), Kokkos::ALL(), Kokkos::ALL(),
                               Kokkos::ALL(), 0, 0, 0)};
  Kokkos::deep_copy(g.device(), fxSpace);

  bsl6d::Arakawa arakawa{};
  Kokkos::View<double ******> poissonBracket{};
  poissonBracket = arakawa(f, g);

  Kokkos::View<double ******>::HostMirror h_poissonBracket{
      Kokkos::create_mirror_view(poissonBracket)};
  Kokkos::deep_copy(h_poissonBracket, poissonBracket);

  double error{};
  Kokkos::parallel_reduce(
      "All subviews contain the same value",
      f.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double &l_err) {
        l_err =
            std::max(std::abs(h_poissonBracket(ix1, ix2, 0, 0, 0, 0) -
                              h_poissonBracket(ix1, ix2, ix3, iv1, iv2, iv3)),
                     l_err);
      },
      Kokkos::Max<double>(error));
  EXPECT_EQ(error, 0.0);
}

// {f,f} = 0
TEST_F(AdvectionArakawa, PoissonBracketZeroSameElements) {
  bsl6d::DistributionFunction f{analytical1()};
  bsl6d::ScalarField g{f.gridDistribution().create_xGridDistribution(), ""};
  auto fxSpace{Kokkos::subview(f.device(), Kokkos::ALL(), Kokkos::ALL(),
                               Kokkos::ALL(), 0, 0, 0)};
  Kokkos::deep_copy(g.device(), fxSpace);

  bsl6d::Arakawa arakawa{};
  Kokkos::View<double ******> poissonBracket{};
  poissonBracket = arakawa(f, g);

  EXPECT_LT(bsl6d::Impl::LInf_norm(poissonBracket), 1.0e-12);
};

// Antisymmetry: {f,g} = -{g,f}
TEST_F(AdvectionArakawa, PoissonBracketAntisymmetry) {
  bsl6d::DistributionFunction f2{analytical2()};
  bsl6d::ScalarField g2{f2.gridDistribution().create_xGridDistribution(), ""};
  auto fx2{Kokkos::subview(f2.device(), Kokkos::ALL(), Kokkos::ALL(),
                           Kokkos::ALL(), 0, 0, 0)};
  Kokkos::deep_copy(g2.device(), fx2);
  bsl6d::DistributionFunction f1{analytical1()};

  bsl6d::Arakawa arakawa{};
  Kokkos::View<double ******> poissonBracket{};
  poissonBracket = arakawa(f1, g2);
  EXPECT_GT(bsl6d::Impl::LInf_norm(poissonBracket), 1.0);

  bsl6d::ScalarField g1{f1.gridDistribution().create_xGridDistribution(), ""};
  auto fx1{Kokkos::subview(f1.device(), Kokkos::ALL(), Kokkos::ALL(),
                           Kokkos::ALL(), 0, 0, 0)};
  Kokkos::deep_copy(g1.device(), fx1);

  Kokkos::View<double ******> poissonBracketSwaped{};
  poissonBracketSwaped = arakawa(f2, g1);
  EXPECT_GT(bsl6d::Impl::LInf_norm(poissonBracketSwaped), 1.0);

  Kokkos::View<double ******>::HostMirror h_poissonBracket,
      h_poissonBracketSwaped;
  h_poissonBracket = Kokkos::create_mirror_view(poissonBracket);
  Kokkos::deep_copy(h_poissonBracket, poissonBracket);
  h_poissonBracketSwaped = Kokkos::create_mirror_view(poissonBracketSwaped);
  Kokkos::deep_copy(h_poissonBracketSwaped, poissonBracketSwaped);

  double error{};
  Kokkos::parallel_reduce(
      "Skewsymmetry Error",
      f1.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double &l_err) {
        l_err = std::max(
            l_err,
            std::abs(h_poissonBracket(ix1, ix2, ix3, iv1, iv2, iv3) +
                     h_poissonBracketSwaped(ix1, ix2, ix3, iv1, iv2, iv3)));
      },
      Kokkos::Max<double>(error));
  EXPECT_LT(error, 1.0e-12);
}

// Jacobi-Identity: {f,{g,h}} + {g,{h,f}} + {h,{f,g}} = 0
TEST_F(AdvectionArakawa, PoissonBracketJacobiIdentity) {
  bsl6d::DistributionFunction f1{analytical1()};
  bsl6d::DistributionFunction f2{analytical2()};
  bsl6d::DistributionFunction f3{analytical3()};

  // {f,{g,h}}
  Kokkos::View<double ******> poissonBracket1 =
      poisson_bracket_in_poisson_bracket(f1, f2, f3);
  EXPECT_GT(bsl6d::Impl::LInf_norm(poissonBracket1), 1.0);

  // {g,{h,f}}
  Kokkos::View<double ******> poissonBracket2 =
      poisson_bracket_in_poisson_bracket(f2, f3, f1);
  EXPECT_GT(bsl6d::Impl::LInf_norm(poissonBracket2), 1.0);

  // {h,{f,g}}
  Kokkos::View<double ******> poissonBracket3 =
      poisson_bracket_in_poisson_bracket(f3, f1, f2);
  EXPECT_GT(bsl6d::Impl::LInf_norm(poissonBracket3), 1.0);

  Kokkos::View<double ******>::HostMirror h_poissonBracket1, h_poissonBracket2,
      h_poissonBracket3{};
  h_poissonBracket1 = Kokkos::create_mirror_view(poissonBracket1);
  Kokkos::deep_copy(h_poissonBracket1, poissonBracket1);
  h_poissonBracket2 = Kokkos::create_mirror_view(poissonBracket2);
  Kokkos::deep_copy(h_poissonBracket2, poissonBracket2);
  h_poissonBracket3 = Kokkos::create_mirror_view(poissonBracket3);
  Kokkos::deep_copy(h_poissonBracket3, poissonBracket3);

  double error{};
  Kokkos::parallel_reduce(
      "Jacobi Identity Error",
      f1.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double &l_err) {
        l_err = std::max(
            l_err, std::abs(h_poissonBracket1(ix1, ix2, ix3, iv1, iv2, iv3) +
                            h_poissonBracket2(ix1, ix2, ix3, iv1, iv2, iv3) +
                            h_poissonBracket3(ix1, ix2, ix3, iv1, iv2, iv3)));
      },
      Kokkos::Max<double>(error));
  // This is not preserved by the Arakawa poisson bracket
  EXPECT_LT(error, 1.0e-4);
}

// ToDo if tests are not trust worthy implement:
// Bilinearity: {af+bg,h} = a{f,h} + b{g,h}
// Leibniz's rule: {fg,h} = {f,h}g + f{g,h}

// Test analytical advection with Arakawa and leap frog
// Move test to characteristic test using backtracing to implement the solution
nlohmann::json create_config_file_leap_frog() {
  nlohmann::json configFile =
      R"({
      "distributionFunction": {
        "q": 1.0,
        "m": 1.0
      },
      "gridDistribution" : {
        "grid": {
          "axes": ["x1","x2"],
          "NxvPoints": [64, 64],
          "xvMin": [0.0, 0.0],
          "xvMax": [6.283185307179586, 6.283185307179586],
          "position": ["L", "L"],
          "rotateGyroFreq": true
        }
      },
      "fieldSolver": {
        "backgroundField": {
          "Bx3": 1.0, 
          "type": "cos",
          "alpha" : 0.1,
          "k" : [0.0,1.0,0.0]
        }
      },
      "initializationDistributionFunction":{
        "initFunction": "planewave_dens_perturbation",
        "alpha": 0.01,
        "mk": [1,0,0.0],
        "temperature": 1.0
      },
      "simulationTime": {
        "dt": 0.05,
        "T": 10.0,
        "restart": false
      },
      "integrator": [
            {"token": "solveMaxwellFields","t0Offset": 0.0,"dtRatio": 0.0},
            {"token": "advectXDrift","t0Offset": 0.0,"dtRatio": 1.0},
            {"token": "advanceTbydt","t0Offset": 0.0,"dtRatio": 0.0}
          ]
    })"_json;
  return configFile;
}

TEST(AdvectionArakawaIntegrator, LeapFrogConstantAdvection) {
  nlohmann::json configFile = create_config_file_leap_frog();

  auto [simTime, f6d] = bsl6d::initialize(configFile);
  bsl6d::MaxwellFields maxwellFields{};

  bsl6d::Integrator integrate{bsl6d::IntegratorConfig{bsl6d::config_file()},
                              f6d};

  while (simTime.continue_advection()) {
    integrate(f6d, maxwellFields, simTime, bsl6d::Integrator::Stage::loop);
  }

  Kokkos::deep_copy(f6d.host(), f6d.device());

  for (size_t ix1 = 0; ix1 < f6d.device().extent(0); ix1++) {
    for (size_t ix2 = 0; ix2 < f6d.device().extent(1); ix2++) {
      double x = f6d.mpi_local_grid()(bsl6d::AxisType::x1)(ix1);
      double y = f6d.mpi_local_grid()(bsl6d::AxisType::x2)(ix2);

      EXPECT_NEAR(f6d.host()(ix1, ix2, 0, 0, 0, 0), 1 + 0.01 * sin(x - sin(y)),
                  5e-8);
    }
  }
}

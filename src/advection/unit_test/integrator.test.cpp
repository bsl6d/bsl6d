#include <gtest/gtest.h>

#include "../integrator.hpp"

TEST(AdvectionIntegrator, CheckToken) {
  EXPECT_TRUE(bsl6d::Integrator::token_exists("advectX"));
  EXPECT_FALSE(bsl6d::Integrator::token_exists("unknownToken"));
}
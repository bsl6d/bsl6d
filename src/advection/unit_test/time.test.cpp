/*****************************************************************/
/*
\brief Testing the settings and functionalities of the lagrange
       interpolation of the bsl6d project using gtest

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.10.2021
*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <config.hpp>
#include <time.hpp>

TEST(Advection, Time) {
  nlohmann::json configFile = R"({
    "simulationTime": {
      "dt": 0.1,
      "T": 1.04,
      "restart": false
    }
  })"_json;
  bsl6d::SimulationTime time{bsl6d::SimulationTimeConfig{configFile}};

  ASSERT_DOUBLE_EQ(time.dt(), configFile["simulationTime"]["dt"]);
  ASSERT_DOUBLE_EQ(time.final_T(), configFile["simulationTime"]["T"]);
  ASSERT_EQ(time.current_step(), 0);
  ASSERT_DOUBLE_EQ(time.current_T(), 0.);
  ASSERT_EQ(time.continue_advection(), true);

  while (time.continue_advection()) {
    EXPECT_LT(time.current_T(), time.final_T());
    time.advance_current_T_by_dt();
  }
  EXPECT_EQ(time.current_step(), 10);
  EXPECT_DOUBLE_EQ(time.current_T(), 1.0);

  EXPECT_EQ(time.continue_advection(), false);

  bsl6d::SimulationTime time2{bsl6d::SimulationTimeConfig{configFile}, 5};
  EXPECT_EQ(time2.current_step(), 5);
  EXPECT_DOUBLE_EQ(time2.current_T(), 0.5);
};

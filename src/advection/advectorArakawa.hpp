#pragma once

#include <Kokkos_Core.hpp>

#include <baseFieldSolver.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <fdDerivativeStencils.hpp>
#include <gridDistribution.hpp>

namespace bsl6d {

// clang-format off
/**
 * @brief Implements the Arakawa Scheme using Poisson brackets 
 * \f$ \dfrac{\{g, f\}_1 + \{g,f\}_2}{2} \f$ to conserve the L2 norm of f.
 *
 * @details This operator implements 
 * @f[
 *  \{g, f\}_1 = (\partial_x g) (\partial_y f) - (\partial_y g)  (\partial_x f)\\
 *  \{g, f\}_2 = \partial_x \left(g (\partial_y f)\right) - \partial_y \left(g  (\partial_x f)\right)\\
 * @f]    
 * using a 7-point finite-difference stencil. 
 * The implemented Arakawa scheme conserves the L2 norm of the function f.
 * The dimensions included into the poisson bracket are hard coded to x and y.
 * The implementation assumes periodic boundary conditions.
 * 
 * @cite arakawa_computational_1966
 * 
 * @todo Long Term: Separate phyiscs from the poisson bracket as in Lagrange Interpolator and Characteristics objects;
 *       Lagrange Interpolator -> Mathematics -> Should only work with 2-D Views instead of DistributionFunction and ScalarField
 *       Characteristics -> Physics
 *       Then write test as for Characteristics and Poisson bracket separately
 */
// clang-format on
class Arakawa {
  public:
  Arakawa() = default;

  /**
   * @brief Calculate the Poisson bracket \f$\{\phi, f\}\f$
   * @param t0 Current time.
   * @param dt Time step.
   * @return Contains the Poisson bracket \f$\{\phi, f\}\f$.
   */
  Kokkos::View<double ******> operator()(DistributionFunction const &f6d,
                                         ScalarField const &scalarField) const;

  private:
  using TeamHandle = Kokkos::TeamPolicy<>::member_type;

  static constexpr size_t m_stencil_size{7};
  KOKKOS_INLINE_FUNCTION
  size_t stencil_index() { return (m_stencil_size - 3) / 2; }
  KOKKOS_INLINE_FUNCTION
  size_t halo_extent() const { return (m_stencil_size - 1) / 2; }
  KOKKOS_INLINE_FUNCTION
  size_t original_to_extended(size_t index) const {
    return index + halo_extent();
  }
  KOKKOS_INLINE_FUNCTION
  size_t extended_to_original(size_t index,
                              AxisType dim,
                              std::array<size_t, 6> N) const {
    return (index + N[dim] - halo_extent()) % N[dim];
  }
};

// clang-format off
/**
 * @brief Performs a single leap-frog time step for the distribution function
 *        based on the driftkinetic equation.
 *
 * @details A reconnection step is regularly performed to increase stability
 * - Standard update: \f$ f_{n+1} = f_{n-1} + 2 \Delta t \{f_n,\phi/B_0\} \f$
 * - Update with reconnection: \f$ f_{n+1} = f_{n-2} + \Delta t \{f_{n-1},\phi/B_0\} + 2 \Delta t \{f_{n},\phi/B_0\} \f$
 *
 * @cite hairer_geometric_2006
 *
 */
// clang-format on
class LeapFrog {
  public:
  LeapFrog() = default;

  LeapFrog(GridDistribution<3, 3> gridDistribution);

  void operator()(DistributionFunction &f6d,
                  MaxwellFields &maxwellFields,
                  double t0,
                  double dt);

  private:
  GridDistribution<3, 3> m_gridDistribution{};

  Arakawa m_arakawa{};

  Kokkos::View<double ******> m_f1{};
  static constexpr size_t m_recSteps{30};
};

} /* namespace bsl6d */

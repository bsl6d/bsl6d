#include <advector.hpp>
#include <advectorArakawa.hpp>
#include <baseFieldSolver.hpp>
#include <config.hpp>
#include <diagnostics.hpp>
#include <distributionFunction.hpp>
#include <sources.hpp>
#include <time.hpp>

namespace bsl6d {

class Integrator {
  public:
  Integrator() = default;
  Integrator(IntegratorConfig const &, DistributionFunction const &);

  enum class Stage { pre, loop, post };

  enum class Token {
    advectX,
    advectXAdj,
    advectXDrift,
    advectV,
    advectVAdj,
    solveMaxwellFields,
    diagnostic,
    rightHandSide,
    advanceTbydt,
    startLoop,
    endLoop
  };

  void operator()(DistributionFunction &f6d,
                  MaxwellFields &maxwellFields,
                  SimulationTime &simTime,
                  Stage currentStage = Stage::loop);

  static bool token_exists(std::string const &);
  static std::string token_to_string(Token const &);
  static Integrator::Token string_to_token(std::string const &);

  private:
  struct IntegratorStep {
    Token token{};
    double t0Offset{};
    double dtRatio{};
  };

  bool m_isFirstCall{true};

  std::map<Stage, std::list<IntegratorStep>> integrator{};

  std::map<AxisType, Advector> m_advector{};
  std::unique_ptr<LeapFrog> m_leapFrog{};
  std::shared_ptr<Characteristic> m_characteristicPtr{};
  std::unique_ptr<FieldSolver> m_fieldSolver{};
  std::unique_ptr<Sources> m_sources{};
  std::unique_ptr<Diagnostic> m_diagnostic{};
};

}  // namespace bsl6d

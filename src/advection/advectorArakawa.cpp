/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
        by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#include <advectorArakawa.hpp>
#include <fdDerivativeStencils.hpp>

namespace bsl6d {

Kokkos::View<double ******> Arakawa::operator()(
    DistributionFunction const &f6d,
    ScalarField const &scalarField) const {
  if (f6d.gridDistribution().cart_topology().cart_dim_size(
          bsl6d::AxisType::x1) > 1 or
      f6d.gridDistribution().cart_topology().cart_dim_size(
          bsl6d::AxisType::x2) > 1)
    throw std::runtime_error(
        "The Arakawa-Poisson Bracket does not work with a MPI parallel "
        "dimension in x1 and x2! The finite difference stencil requires "
        "halo exchange with which was not implemented yet.");

  double dx1{f6d.mpi_local_grid()(AxisType::x1).delta()};
  double dx2{f6d.mpi_local_grid()(AxisType::x2).delta()};

  std::array<size_t, 6> N{f6d.mpi_local_grid()(AxisType::x1).N(),
                          f6d.mpi_local_grid()(AxisType::x2).N(),
                          f6d.mpi_local_grid()(AxisType::x3).N(),
                          f6d.mpi_local_grid()(AxisType::v1).N(),
                          f6d.mpi_local_grid()(AxisType::v2).N(),
                          f6d.mpi_local_grid()(AxisType::v3).N()};

  Kokkos::View<double ******> arakawa = create_grid_compatible_view(
      f6d.gridDistribution().my_local_grid(), "arakawaView");
  using LocalView =
      Kokkos::View<double **,
                   Kokkos::DefaultExecutionSpace::scratch_memory_space,
                   Kokkos::MemoryTraits<Kokkos::Unmanaged>>;

  Kokkos::TeamPolicy<Kokkos::IndexType<size_t>> teamPolicy{
      static_cast<int>(N[AxisType::x3] * N[AxisType::v1] * N[AxisType::v2] *
                       N[AxisType::v3]),
      Kokkos::AUTO};

  size_t shmem_size = LocalView::shmem_size(N[AxisType::x1], N[AxisType::x2]);
  size_t shmem_size_ex = LocalView::shmem_size(
      N[AxisType::x1] + 2 * halo_extent(), N[AxisType::x2] + 2 * halo_extent());

  int scratch_level{1};

  teamPolicy.set_scratch_size(
      scratch_level, Kokkos::PerTeam(2 * shmem_size + 2 * shmem_size_ex));

  Kokkos::parallel_for(
      teamPolicy, KOKKOS_CLASS_LAMBDA(TeamHandle const &team) {
        size_t res{0};
        size_t Nx3Nv1{N[AxisType::x3] * N[AxisType::v1]};
        size_t Nx3Nv1Nv2{Nx3Nv1 * N[AxisType::v2]};
        size_t iv3 = team.league_rank() / Nx3Nv1Nv2;
        res        = team.league_rank() % Nx3Nv1Nv2;
        size_t iv2 = res / Nx3Nv1;
        res        = res % Nx3Nv1;
        size_t iv1 = res / (N[AxisType::x3]);
        res        = res % (N[AxisType::x3]);
        size_t ix3 = res;

        LocalView fdgdy{team.team_scratch(scratch_level), N[AxisType::x1],
                        N[AxisType::x2]};
        LocalView fdgdx{team.team_scratch(scratch_level), N[AxisType::x1],
                        N[AxisType::x2]};

        // Halo extended views for finite difference stencil
        LocalView f_ex{team.team_scratch(scratch_level),
                       N[AxisType::x1] + 2 * halo_extent(),
                       N[AxisType::x2] + 2 * halo_extent()};
        LocalView g_ex{team.team_scratch(scratch_level),
                       N[AxisType::x1] + 2 * halo_extent(),
                       N[AxisType::x2] + 2 * halo_extent()};

        auto ddx1 = [=](double *const f_ptr) {
          return FDDerivativeStencil7{}.deriveIdx(f_ptr, dx1, f_ex.stride(0));
        };

        auto ddx2 = [=](double *const f_ptr) {
          return FDDerivativeStencil7{}.deriveIdx(f_ptr, dx2, f_ex.stride(1));
        };

        auto range = Kokkos::TeamThreadMDRange<Kokkos::Rank<2>, TeamHandle>(
            team, N[AxisType::x1], N[AxisType::x2]);
        auto range_ex = Kokkos::TeamThreadMDRange<Kokkos::Rank<2>, TeamHandle>(
            team, g_ex.extent(0), g_ex.extent(1));

        Kokkos::parallel_for(range_ex, [=](const int iix1, const int iix2) {
          int ix1 = extended_to_original(iix1, AxisType::x1, N);
          int ix2 = extended_to_original(iix2, AxisType::x2, N);

          g_ex(iix1, iix2) = scalarField(ix1, ix2, ix3);
          f_ex(iix1, iix2) = f6d(ix1, ix2, ix3, iv1, iv2, iv3);
        });

        team.team_barrier();

        Kokkos::parallel_for(range, [=](const int ix1, const int ix2) {
          int iix1 = original_to_extended(ix1);
          int iix2 = original_to_extended(ix2);

          arakawa(ix1, ix2, ix3, iv1, iv2, iv3) =
              ddx1(&f_ex(iix1, iix2)) * ddx2(&g_ex(iix1, iix2)) -
              ddx1(&g_ex(iix1, iix2)) * ddx2(&f_ex(iix1, iix2));

          fdgdx(ix1, ix2) = f_ex(iix1, iix2) * ddx1(&g_ex(iix1, iix2));
          fdgdy(ix1, ix2) = f_ex(iix1, iix2) * ddx2(&g_ex(iix1, iix2));
        });

        team.team_barrier();

        Kokkos::parallel_for(range_ex, [=](const int iix1, const int iix2) {
          int ix1          = extended_to_original(iix1, AxisType::x1, N);
          int ix2          = extended_to_original(iix2, AxisType::x2, N);
          f_ex(iix1, iix2) = fdgdx(ix1, ix2);
          g_ex(iix1, iix2) = fdgdy(ix1, ix2);
        });
        team.team_barrier();

        Kokkos::parallel_for(range, [=](const int ix1, const int ix2) {
          int iix1 = original_to_extended(ix1);
          int iix2 = original_to_extended(ix2);
          arakawa(ix1, ix2, ix3, iv1, iv2, iv3) =
              (arakawa(ix1, ix2, ix3, iv1, iv2, iv3) + ddx1(&g_ex(iix1, iix2)) -
               ddx2(&f_ex(iix1, iix2))) /
              2.0;
        });
      });
  return arakawa;
}

LeapFrog::LeapFrog(GridDistribution<3, 3> gridDistribution)
    : m_gridDistribution{gridDistribution}
    , m_f1{create_grid_compatible_view(m_gridDistribution.my_local_grid(),
                                       "ftemp")} {}

void LeapFrog::operator()(DistributionFunction &f6d,
                          MaxwellFields &maxwellFields,
                          double t0,
                          double dt) {
  ScalarField scalarField{maxwellFields.phi.gridDistribution(),
                          "DriftKinetic: Phi/Bx3"};
  Kokkos::parallel_for(
      "Calculate phi/Bx3", scalarField.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        scalarField(ix1, ix2, ix3) =
            maxwellFields.phi(ix1, ix2, ix3) / maxwellFields.Bx3;
      });
  Kokkos::View<double ******> semiDiscrete{};
  semiDiscrete = m_arakawa(f6d, scalarField);

  if (t0 == 0.0) {
    Kokkos::parallel_for(
        "Leap Frog Initial", f6d.mpi_local_grid().mdRangePolicy(),
        KOKKOS_CLASS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1,
                            size_t iv2, size_t iv3) {
          m_f1(ix1, ix2, ix3, iv1, iv2, iv3) =
              f6d(ix1, ix2, ix3, iv1, iv2, iv3);

          f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
              f6d(ix1, ix2, ix3, iv1, iv2, iv3) +
              dt / 2.0 * semiDiscrete(ix1, ix2, ix3, iv1, iv2, iv3);

          f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
              m_f1(ix1, ix2, ix3, iv1, iv2, iv3) +
              dt * semiDiscrete(ix1, ix2, ix3, iv1, iv2, iv3);
        });
  } else {
    Kokkos::parallel_for(
        "Leap Frog step", f6d.mpi_local_grid().mdRangePolicy(),
        KOKKOS_CLASS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1,
                            size_t iv2, size_t iv3) {
          double temp = f6d(ix1, ix2, ix3, iv1, iv2, iv3);
          f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
              m_f1(ix1, ix2, ix3, iv1, iv2, iv3) +
              2.0 * dt * semiDiscrete(ix1, ix2, ix3, iv1, iv2, iv3);

          if (static_cast<int>(t0 / dt) % m_recSteps == 0) {
            m_f1(ix1, ix2, ix3, iv1, iv2, iv3) =
                m_f1(ix1, ix2, ix3, iv1, iv2, iv3) +
                dt * semiDiscrete(ix1, ix2, ix3, iv1, iv2, iv3);
          } else {
            m_f1(ix1, ix2, ix3, iv1, iv2, iv3) = temp;
          }
        });
  }
  f6d.require_velocity_moments_update();
}

} /* namespace bsl6d */

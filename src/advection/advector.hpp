/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
       by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#pragma once

#include <Kokkos_Core.hpp>

#include <characteristics.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <halo.hpp>
#include <lagrangeBuilder.hpp>

namespace bsl6d {

class Advector {
  public:
  Advector() = default;
  Advector(std::shared_ptr<Characteristic> _characteristic,
           CharacteristicVariants const _characteristicVariants,
           AxisType axis,
           LagrangeBuilder lagrangeBuilder);

  void operator()(DistributionFunction &f6d, double t0, double dt);

  private:
  std::shared_ptr<Base1dInterpolator> interpolator{};
  std::shared_ptr<Characteristic> characteristic;
  AxisType myAxis;
};

} /* namespace bsl6d */

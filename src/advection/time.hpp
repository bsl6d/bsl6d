/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
       by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#pragma once

#include <Kokkos_Core.hpp>

#include <config.hpp>

namespace bsl6d {

class SimulationTime {
  public:
  KOKKOS_INLINE_FUNCTION
  double dt() const { return m_dt; }
  KOKKOS_INLINE_FUNCTION
  double final_T() const { return m_finalT; }
  KOKKOS_INLINE_FUNCTION
  double current_T() const { return m_currentT; }
  KOKKOS_INLINE_FUNCTION
  double current_phase() const { return m_currentPhase; }
  KOKKOS_INLINE_FUNCTION
  size_t current_step() const { return m_N; }
  KOKKOS_INLINE_FUNCTION
  double gyro_frequency() const { return m_gyroFrequency; }

  void advance_current_T_by_dt();
  bool continue_advection() const;

  SimulationTime() = default;
  SimulationTime(SimulationTimeConfig const &conf,
                 size_t startIteration,
                 double startTime,
                 double gridPhase);
  SimulationTime(SimulationTimeConfig const &conf, size_t startIteration = 0);

  private:
  double m_dt{};
  double m_currentT{};
  double m_finalT{};
  size_t m_N{};
  double m_currentPhase{};
  size_t m_Nmax{};
  double m_gyroFrequency{};
};

} /* namespace bsl6d */

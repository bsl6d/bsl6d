#include <initializationFunctions.hpp>
#include <time.hpp>

#include "./integrator.hpp"

namespace bsl6d {

Integrator::Integrator(IntegratorConfig const &config,
                       DistributionFunction const &f6d) {
  Stage currentStage = Stage::pre;
  IntegratorStep parsedStep{};

  auto loopSpecified{
      std::find_if(std::begin(config.integratorSteps),
                   std::end(config.integratorSteps), [](auto step) {
                     return string_to_token(step.token) == Token::startLoop;
                   })};
  if (loopSpecified == std::end(config.integratorSteps)) {
    currentStage = Stage::loop;
  }

  for (auto step : config.integratorSteps) {
    parsedStep.token = string_to_token(step.token);
    if (parsedStep.token == Token::startLoop) {
      currentStage = Stage::loop;
      continue;
    }
    if (parsedStep.token == Token::endLoop) {
      currentStage = Stage::post;
      continue;
    }
    parsedStep.t0Offset = step.t0Offset;
    parsedStep.dtRatio  = step.dtRatio;
    integrator[currentStage].push_back(parsedStep);
  }
  auto [characteristicPtr, advector] = initialize_advector(f6d);
  m_characteristicPtr                = characteristicPtr;
  m_advector                         = advector;
  m_fieldSolver                      = initialize_fieldSolver();
  m_sources =
      std::make_unique<Sources>(Sources{RightHandSideConfig{config_file()}});

  // Only initialize diagnostic if it is actually part
  // of the integrator to omit creating the bsl6dDiagnostic
  // directory if diagnostics are acutally never called
  auto hasDiagnostic{std::find_if(
      std::begin(integrator[Stage::loop]), std::end(integrator[Stage::loop]),
      [](auto step) { return step.token == Token::diagnostic; })};
  if (hasDiagnostic != std::end(integrator[Stage::loop])) {
    m_diagnostic = std::make_unique<Diagnostic>(
        Diagnostic{DiagnosticConfig{config_file()},
                   f6d.gridDistribution().cart_topology()});
  }

  // Only initialize leapForg if it is actually part of the integrator
  // to omit unnecessary memory allocation of the LeapFrog integrator
  // if it is not used.
  auto hasLeapFrog{std::find_if(
      std::begin(integrator[Stage::loop]), std::end(integrator[Stage::loop]),
      [](auto step) { return step.token == Token::advectXDrift; })};
  if (hasLeapFrog != std::end(integrator[Stage::loop])) {
    m_leapFrog = std::make_unique<LeapFrog>(LeapFrog{f6d.gridDistribution()});
  }
};

void Integrator::operator()(DistributionFunction &f6d,
                            MaxwellFields &maxwellFields,
                            SimulationTime &simTime,
                            Stage currentStage) {
  auto hasMaxwellFieldSolver{std::find_if(
      std::begin(integrator[Stage::loop]), std::end(integrator[Stage::loop]),
      [](auto step) { return step.token == Token::solveMaxwellFields; })};
  if (m_isFirstCall and
      std::end(integrator[Stage::loop]) != hasMaxwellFieldSolver) {
    maxwellFields = m_fieldSolver->operator()(f6d, simTime);
    m_isFirstCall = false;
  }
  for (auto step : integrator[currentStage]) {
    switch (step.token) {
      case Token::advectX: {
        m_characteristicPtr->set_maxwellFields(maxwellFields);
        for (auto axis : xAxes()) {
          m_advector[axis](f6d,
                           simTime.current_T() + step.t0Offset * simTime.dt(),
                           simTime.dt() * step.dtRatio);
        }
        break;
      }
      case Token::advectXAdj: {
        m_characteristicPtr->set_maxwellFields(maxwellFields);
        for (auto axis = std::rbegin(xAxes()); axis != std::rend(xAxes());
             axis++) {
          m_advector[*axis](f6d,
                            simTime.current_T() + step.t0Offset * simTime.dt(),
                            simTime.dt() * step.dtRatio);
        }
        break;
      }
      case Token::advectXDrift: {
        m_leapFrog->operator()(
            f6d, maxwellFields,
            simTime.current_T() + step.t0Offset * simTime.dt(),
            simTime.dt() * step.dtRatio);
        if (f6d.gridDistribution().my_local_grid()(AxisType::x3).N() > 1)
          m_advector[AxisType::x3](
              f6d, simTime.current_T() + step.t0Offset * simTime.dt(),
              simTime.dt() * step.dtRatio);
        break;
      }
      case Token::advectV: {
        m_characteristicPtr->set_maxwellFields(maxwellFields);
        for (auto axis : vAxes()) {
          m_advector[axis](f6d,
                           simTime.current_T() + step.t0Offset * simTime.dt(),
                           simTime.dt() * step.dtRatio);
        }
        break;
      }
      case Token::advectVAdj: {
        m_characteristicPtr->set_maxwellFields(maxwellFields);
        for (auto axis = std::rbegin(vAxes()); axis != std::rend(vAxes());
             axis++) {
          m_advector[*axis](f6d,
                            simTime.current_T() + step.t0Offset * simTime.dt(),
                            simTime.dt() * step.dtRatio);
        }
        break;
      }
      case Token::solveMaxwellFields: {
        maxwellFields = m_fieldSolver->operator()(f6d, simTime);
        m_characteristicPtr->set_maxwellFields(maxwellFields);
        break;
      }
      case Token::diagnostic: {
        m_diagnostic->operator()(simTime, f6d, maxwellFields);
        break;
      }
      case Token::rightHandSide: {
        m_sources->operator()(
            f6d, maxwellFields,
            simTime.current_T() + step.t0Offset * simTime.dt(),
            simTime.dt() * step.dtRatio);
        break;
      }
      case Token::advanceTbydt: {
        simTime.advance_current_T_by_dt();
        break;
      }
      default:
        throw std::runtime_error(
            "BSL6D Integrator received a token, that is not implemented.");
    }
  }
}

std::map<std::string, Integrator::Token> parser{
    {"advectX", Integrator::Token::advectX},
    {"advectXAdj", Integrator::Token::advectXAdj},
    {"advectXDrift", Integrator::Token::advectXDrift},
    {"advectV", Integrator::Token::advectV},
    {"advectVAdj", Integrator::Token::advectVAdj},
    {"solveMaxwellFields", Integrator::Token::solveMaxwellFields},
    {"diagnostic", Integrator::Token::diagnostic},
    {"rightHandSide", Integrator::Token::rightHandSide},
    {"advanceTbydt", Integrator::Token::advanceTbydt},
    {"startLoop", Integrator::Token::startLoop},
    {"endLoop", Integrator::Token::endLoop}};

std::map<Integrator::Token, std::string> invertedParser{
    {Integrator::Token::advectX, "advectX"},
    {Integrator::Token::advectXAdj, "advectXAdj"},
    {Integrator::Token::advectXDrift, "advectXDrift"},
    {Integrator::Token::advectV, "advectV"},
    {Integrator::Token::advectVAdj, "advectVAdj"},
    {Integrator::Token::solveMaxwellFields, "solveMaxwellFields"},
    {Integrator::Token::diagnostic, "diagnostic"},
    {Integrator::Token::rightHandSide, "rightHandSide"},
    {Integrator::Token::advanceTbydt, "advanceTbydt"},
    {Integrator::Token::startLoop, "startLoop"},
    {Integrator::Token::endLoop, "endLoop"}};

bool Integrator::token_exists(std::string const &token) {
  auto found{std::find_if(
      parser.begin(), parser.end(),
      [=](std::pair<std::string, Integrator::Token> const &tokenPair) {
        return tokenPair.first == token;
      })};
  if (found != parser.end()) { return true; }
  return false;
}

std::string Integrator::token_to_string(Integrator::Token const &token) {
  return invertedParser.at(token);
}

Integrator::Token Integrator::string_to_token(std::string const &token) {
  return parser.at(token);
}

}  // namespace bsl6d

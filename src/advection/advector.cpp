/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
        by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#include <advector.hpp>

namespace bsl6d {

Advector::Advector(std::shared_ptr<Characteristic> _characteristic,
                   CharacteristicVariants const _characteristicVariants,
                   AxisType axis,
                   LagrangeBuilder lagrangeBuilder)
    : characteristic{_characteristic}
    , myAxis{axis} {
  lagrangeBuilder.setCharacteristic(_characteristicVariants);
  lagrangeBuilder.setAxis(axis);
  interpolator = lagrangeBuilder.getInterpolator();
};

/*****************************************************************/
/*

\brief Propagates distribution function by timestep

\param[inout] f6d - Distribution function object
\param[in] dt - time step

*/
/*****************************************************************/
void Advector::operator()(DistributionFunction &f6d, double t0, double dt) {
  characteristic->set_dt(dt);
  characteristic->set_simulationTime(t0);
  characteristic->set_distributionFunction(f6d);
  interpolator->interpolate(f6d);
}

} /* namespace bsl6d */

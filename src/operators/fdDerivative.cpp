#include <fdDerivative.hpp>

namespace bsl6d {
FDDerivative::FDDerivative(AxisType axis, Grid<3, 0> grid)
    : m_axis{axis}
    , m_grid{grid}
    , m_derivativeDirections{Kokkos::View<int***>{
          "FDDerivativeDirections", grid(AxisType::x1).N(),
          grid(AxisType::x2).N(), grid(AxisType::x3).N()}} {
  std::map<AxisType, size_t> boundaryWidth{};
  for (auto xAxis : grid.xAxes) { boundaryWidth[xAxis] = 0; }
  boundaryWidth[axis] = std::floor(m_stencil.width / 2.0);
  m_halo              = HaloComm{m_grid, boundaryWidth};
};

ScalarField FDDerivative::operator()(ScalarField const& input,
                                     std::string const resultLabel) const {
  ScalarField result{input.gridDistribution(), resultLabel};
  if (input.grid() != m_grid) {
    throw std::runtime_error(
        "input.grid() and m_grid do not match in FDDerivative!");
  }
  m_halo.exchange(input, m_axis);
  Kokkos::View<int***> derivativeDirections{m_derivativeDirections};
  AxisType axis{m_axis};
  FDDerivativeStencil9 stencil{m_stencil};
  size_t stride{input.device().stride(static_cast<size_t>(axis))};
  Kokkos::parallel_for(
      "ApplyFDStencil", input.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        result(ix1, ix2, ix3) =
            stencil.deriveIdx(&input(ix1, ix2, ix3), input.grid()(axis).delta(),
                              stride, derivativeDirections(ix1, ix2, ix3));
      });
  return result;
}

ScalarField FDDerivative::operator()(Inverse,
                                     ScalarField const& input,
                                     std::string const resultLabel) const {
  throw std::runtime_error(
      "The operator()(Inverse, ...) of class FDDerivative is not implemented!");
}
void FDDerivative::set_derivativeDirections(
    Kokkos::View<int***> const& derivativeDirections) {
  for (size_t i = 0; i < 3; i++) {
    if (derivativeDirections.extent(i) != m_derivativeDirections.extent(i)) {
      throw std::runtime_error(
          "Extents of Views do not match in FDDerivative for direction." +
          std::to_string(i));
    }
  }
  m_derivativeDirections = derivativeDirections;
}
void FDDerivative::prepare_input_argument_for_operator(ScalarField& input) {
  input.set_halo_width(m_axis, std::floor(m_stencil.width / 2.0));
}
}  // namespace bsl6d

#include <linearAlgebra.hpp>

#include "vectorAnalysis.hpp"

namespace bsl6d {

Gradient::Gradient(GridDistribution<3, 0> const &gridDistribX) {
  for (auto axis : gridDistribX.mpi_local_grid().xAxes)
    derivative[axis] = Derivative{axis, gridDistribX};
};

VectorField Gradient::operator()(ScalarField const &input,
                                 std::string const label) const {
  VectorField result{input.gridDistribution(), label};
  for (auto axis : input.grid().xAxes) {
    result.replace_scalarField(
        axis, derivative[axis](input, label + "_" + axis_to_string(axis)));
  }
  return result;
};

ScalarField Gradient::operator()(Inverse,
                                 VectorField const &input,
                                 std::string const label) const {
  throw std::runtime_error(
      "class Gradient has not implemented operator()(Inverse, ...).");
};

Divergence::Divergence(GridDistribution<3, 0> const &gridDistribX) {
  for (auto axis : gridDistribX.mpi_local_grid().xAxes)
    m_derivative[axis] = Derivative{axis, gridDistribX};
};

ScalarField Divergence::operator()(VectorField const &input,
                                   std::string const label) const {
  ScalarField res{input(AxisType::x1).gridDistribution(), label};
  Kokkos::deep_copy(res.device(), 0.0);
  for (auto axis : res.mpi_local_grid().xAxes) {
    res += m_derivative[axis](input(axis),
                              "d" + axis_to_string(axis) + input.label());
  }
  return res;
};

VectorField Divergence::operator()(Inverse,
                                   ScalarField const &input,
                                   std::string const label) const {
  throw std::runtime_error(
      "class Divergence has not implemented operator()(Inverse, ...).");
};

namespace Impl {

void multiply_ik2(Kokkos::View<std::complex<double> ***> const &outView,
                  FastFourierTransform3d const &fft) {
  Impl::FourierGrid fourierGrid{fft.fourier_grid()};
  Kokkos::parallel_for(
      "class Laplace; multiply_ik2()", fourierGrid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ik1, size_t ik2, size_t ik3) {
        double k2 =
            fourierGrid.k(AxisType::x1, ik1) *
                fourierGrid.k(AxisType::x1, ik1) +
            fourierGrid.k(AxisType::x2, ik2) *
                fourierGrid.k(AxisType::x2, ik2) +
            fourierGrid.k(AxisType::x3, ik3) * fourierGrid.k(AxisType::x3, ik3);
        Kokkos::complex<double> val =
            Kokkos::complex<double>{outView(ik1, ik2, ik3)} * (-k2);
        outView(ik1, ik2, ik3) = std::complex{val.real(), val.imag()};
      });
}

void divide_ik2(Kokkos::View<std::complex<double> ***> const &outView,
                FastFourierTransform3d const &fft) {
  Impl::FourierGrid fourierGrid{fft.fourier_grid()};
  Kokkos::parallel_for(
      "class Laplace; divide_ik2()", fourierGrid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ik1, size_t ik2, size_t ik3) {
        double k2 =
            fourierGrid.k(AxisType::x1, ik1) *
                fourierGrid.k(AxisType::x1, ik1) +
            fourierGrid.k(AxisType::x2, ik2) *
                fourierGrid.k(AxisType::x2, ik2) +
            fourierGrid.k(AxisType::x3, ik3) * fourierGrid.k(AxisType::x3, ik3);
        Kokkos::complex<double> val =
            Kokkos::complex<double>{outView(ik1, ik2, ik3)} / (-k2);
        outView(ik1, ik2, ik3) = std::complex{val.real(), val.imag()};
        // Zero constant at laplace problem.
        if (k2 == 0.0) { outView(0, 0, 0) = std::complex{0.0, 0.0}; }
      });
}

}  // namespace Impl

Laplace::Laplace(GridDistribution<3, 0> const &gridDistributionX)
    : m_fft{gridDistributionX} {};

ScalarField Laplace::operator()(ScalarField const &arg,
                                std::string const label) const {
  Kokkos::View<std::complex<double> ***> fftScalarFieldView{m_fft(arg, "FFT")};
  Impl::multiply_ik2(fftScalarFieldView, m_fft);
  return m_fft(Inverse{}, fftScalarFieldView, label);
}

ScalarField Laplace::operator()(Inverse,
                                ScalarField const &input,
                                std::string const label) const {
  Kokkos::View<std::complex<double> ***> fftScalarFieldView{
      m_fft(input, "FFT")};
  Impl::divide_ik2(fftScalarFieldView, m_fft);
  return m_fft(Inverse{}, fftScalarFieldView, label);
};

} /* namespace bsl6d */

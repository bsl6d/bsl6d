/*****************************************************************/
/*

\brief Abstract interface for all operators in bsl6d project

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.02.2021

*/
/*****************************************************************/

#pragma once
#include <string>

namespace bsl6d {

/*****************************************************************/
/*

\brief Abstract interface bsl6d operators

*/
/*****************************************************************/
struct Inverse {};  // Tag for inverse operator application
template <typename InFieldType, typename OutFieldType>
class Operator {
  public:
  Operator() = default;
  virtual ~Operator(){};
  virtual OutFieldType operator()(InFieldType const &input,
                                  std::string const resultLabel) const = 0;
  virtual InFieldType operator()(Inverse,
                                 OutFieldType const &input,
                                 std::string const resultLabel) const  = 0;

  virtual void prepare_input_argument_for_operator(InFieldType &input){};
};

} /* namespace bsl6d */

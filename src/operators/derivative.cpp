/*****************************************************************/
/*

\brief Implementation of derivative operator

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   10.02.2021

*/
/*****************************************************************/

#include <derivative.hpp>

namespace bsl6d {

namespace Impl {

void multiply_ik(Kokkos::View<std::complex<double>***> const& outView,
                 FastFourierTransform3d const& fft,
                 AxisType const& axis) {
  Impl::FourierGrid fourierGrid{fft.fourier_grid()};
  Kokkos::parallel_for(
      "Derivative multiply ik", fourierGrid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ik1, size_t ik2, size_t ik3) {
        size_t idx{
            std::array<size_t, 3>{ik1, ik2, ik3}[static_cast<size_t>(axis)]};
        Kokkos::complex<double> val =
            Kokkos::complex<double>{outView(ik1, ik2, ik3)} *
            Kokkos::complex<double>{0, fourierGrid.k(axis, idx)};
        outView(ik1, ik2, ik3) = std::complex{val.real(), val.imag()};
      });
}

template <AxisType axis>
void divide_ik(Kokkos::View<std::complex<double>***> const& outView,
               FastFourierTransform3d const& fft) {
  Impl::FourierGrid fourierGrid{fft.fourier_grid()};
  Kokkos::parallel_for(
      "Integrate divide ik", fourierGrid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ik1, size_t ik2, size_t ik3) {
        size_t idx{
            std::array<size_t, 3>{ik1, ik2, ik3}[static_cast<size_t>(axis)]};
        Kokkos::complex<double> val =
            Kokkos::complex{outView(ik1, ik2, ik3)} *
            Kokkos::complex{0.0, -1 / fourierGrid.k(axis, idx)};
        outView(ik1, ik2, ik3) = std::complex{val.real(), val.imag()};
        if (fourierGrid.k(axis, idx) == 0.0) {
          outView(ik1, ik2, ik3) = std::complex{0.0, 0.0};
        }
      });
}

}  // namespace Impl

Derivative::Derivative(AxisType _axis,
                       GridDistribution<3, 0> const& gridDistributionX)
    : axis{_axis}
    , fft{gridDistributionX} {};

ScalarField Derivative::operator()(ScalarField const& input,
                                   std::string const resultLabel) const {
  Kokkos::View<std::complex<double>***> out{};
  out = fft(input, input.device().label() + "FFT" + axis_to_string(axis));

  Impl::multiply_ik(out, fft, axis);

  ScalarField result = fft(Inverse{}, out, resultLabel);

  return result;
};

ScalarField Derivative::operator()(Inverse,
                                   ScalarField const& input,
                                   std::string const resultLabel) const {
  Kokkos::View<std::complex<double>***> out{};
  out = fft(input, input.device().label() + "FFT" + axis_to_string(axis));

  switch (axis) {
    case AxisType::x1: Impl::divide_ik<AxisType::x1>(out, fft); break;
    case AxisType::x2: Impl::divide_ik<AxisType::x2>(out, fft); break;
    case AxisType::x3: Impl::divide_ik<AxisType::x3>(out, fft); break;
    default:
      throw std::runtime_error(
          "Operator(Inverse,...) is not implemented for axis: " +
          axis_to_string(axis));
  }

  ScalarField result = fft(Inverse{}, out, resultLabel);

  return result;
};

}  // namespace bsl6d

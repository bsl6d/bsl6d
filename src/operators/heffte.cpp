/*****************************************************************/
/*

\brief Fast Fourier Transform on different Views using FFTW on CPU
       and cuFFTw on GPU

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.01.2022

*/
/*****************************************************************/

#include "heffte.hpp"

#include <string>
namespace bsl6d {

FastFourierTransform3d::FastFourierTransform3d(
    GridDistribution<3, 0> const &gridDistX) {
  m_grid.m_gridDistribution = gridDistX;
  m_grid.m_k0               = std::array<double, 3>{
      2 * M_PI / gridDistX.mpi_global_grid()(AxisType::x1).L(),
      2 * M_PI / gridDistX.mpi_global_grid()(AxisType::x2).L(),
      2 * M_PI / gridDistX.mpi_global_grid()(AxisType::x3).L()};
  CartTopology<3> topology{gridDistX.cart_topology()};
  std::array<int, 3> strideOrder{};

  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    strideOrder = std::array<int, 3>{2, 1, 0};
  } else {
    strideOrder = std::array<int, 3>{0, 1, 2};
  }
  heffte::box3d<> myXBox{
      {static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x1).low()),
       static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x2).low()),
       static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x3).low())},
      {static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x1).up()),
       static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x2).up()),
       static_cast<int>(gridDistX.mpi_local_grid()(AxisType::x3).up())},
      {strideOrder[0], strideOrder[1], strideOrder[2]}};

  heffte::box3d<> globalKBox{
      {0, 0, 0},
      {static_cast<int>(gridDistX.mpi_global_grid()(AxisType::x1).up()),
       static_cast<int>(gridDistX.mpi_global_grid()(AxisType::x2).up()),
       static_cast<int>(gridDistX.mpi_global_grid()(AxisType::x3).up())},
      {strideOrder[0], strideOrder[1], strideOrder[2]}};
  std::array<int, 3> procGrid{topology.cart_dim_size(AxisType::x1),
                              topology.cart_dim_size(AxisType::x2),
                              topology.cart_dim_size(AxisType::x3)};
  std::vector<heffte::box3d<>> kBoxes{
      heffte::split_world(globalKBox, procGrid)};
  heffte::box3d<> myKBox{kBoxes[topology.my_cart_rank()]};

  // heffte::box3d<> does not own a copy constructor and would have to be
  // defined as a shared_ptr member variable. Pointers are not accessible on GPU
  // in Kokkos-Lambdas. Therefore store all interesting information of k-box in
  // local std::arrays.
  for (size_t i = 0; i < 3; i++) {
    m_grid.m_lowK[i]  = myKBox.low[i];
    m_grid.m_upK[i]   = myKBox.high[i];
    m_grid.m_sizeK[i] = myKBox.size[i];
  }

  m_fft = std::make_shared<HeFFTeType>(myXBox, myKBox, topology.cart_comm());
};

Kokkos::View<std::complex<double> ***> FastFourierTransform3d::operator()(
    ScalarField const &fx,
    std::string const label) const {
  if (fx.grid() != m_grid.m_gridDistribution.mpi_local_grid())
    throw std::runtime_error(
        "FastFourierTransform3d was not created with the same grid as "
        "the transformed ScalarField owns.");
  Kokkos::View<std::complex<double> ***> fx_local{
      fx.device().label(), fx.grid()(AxisType::x1).N(),
      fx.grid()(AxisType::x2).N(), fx.grid()(AxisType::x3).N()};
  Kokkos::parallel_for(
      "Write to complex FFT", fx.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        fx_local(ix1, ix2, ix3) = std::complex<double>{fx(ix1, ix2, ix3), 0.0};
      });

  Kokkos::View<std::complex<double> ***> fk{
      label, m_grid.m_sizeK[0], m_grid.m_sizeK[1], m_grid.m_sizeK[2]};
  Kokkos::Profiling::pushRegion("HeFFTe_forward");
  m_fft->forward(fx_local.data(), fk.data());
  Kokkos::Profiling::popRegion();
  return fk;
}

ScalarField FastFourierTransform3d::operator()(
    Inverse,
    Kokkos::View<std::complex<double> ***> const &fk,
    std::string const label) const {
  Kokkos::View<std::complex<double> ***> fx_local{
      label, m_grid.m_gridDistribution.mpi_local_grid()(AxisType::x1).N(),
      m_grid.m_gridDistribution.mpi_local_grid()(AxisType::x2).N(),
      m_grid.m_gridDistribution.mpi_local_grid()(AxisType::x3).N()};
  Kokkos::Profiling::pushRegion("HeFFTe_backward");
  m_fft->backward(fk.data(), fx_local.data(), heffte::scale::full);
  Kokkos::Profiling::popRegion();

  ScalarField fx{m_grid.m_gridDistribution, label};
  Kokkos::parallel_for(
      "Write to complex FFT", fx.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        fx(ix1, ix2, ix3) = fx_local(ix1, ix2, ix3).real();
      });
  return fx;
}

} /* namespace bsl6d */

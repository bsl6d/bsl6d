/*****************************************************************/
/*

\brief Implementation of derative operator

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   10.02.2021

*/
/*****************************************************************/

#pragma once
#include <baseOperator.hpp>
#include <heffte.hpp>
#include <mpiTopology.hpp>
#include <scalarField.hpp>

namespace bsl6d {

class Derivative : public Operator<ScalarField, ScalarField> {
  public:
  Derivative() = default;
  Derivative(AxisType const _axis,
             GridDistribution<3, 0> const& gridDistributionX);

  ScalarField operator()(ScalarField const& arg,
                         std::string const resultLabel) const override;
  ScalarField operator()(Inverse,
                         ScalarField const& arg,
                         std::string const resultLabel) const override;

  private:
  AxisType axis{};
  FastFourierTransform3d fft{};
};
}  // namespace bsl6d

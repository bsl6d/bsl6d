/*****************************************************************/
/*

\brief Test FFT functionality on CPU or GPU

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.01.2022

*/
/*****************************************************************/
#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include <array>
#include <cmath>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <iostream>
#include <mpiTopology.hpp>

#include "../heffte.hpp"

nlohmann::json configFileHeFFTeMPI = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [10.0, 12.0, 14.0, 2.0, 2.5, 3.0],
      "xvMin": [ 0.0,  0.0,  0.0,-2.0,-2.5,-3.0],
      "NxvPoints": [10, 12, 14, 15, 17, 19],
      "position": ["L","L","L","N","N","N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [2,2,2,1,1,1]
    }
  }
})"_json;

class OperatorsHeFFTe : public ::testing::Test {
  public:
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::Grid<3, 0> gridX{grid.create_xSpace()};
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileHeFFTeMPI}};

  bsl6d::GridDistribution<3, 3> gridDistrib{grid, topology};

  /* Expected results in configuration */
  std::array<size_t, 3> localKBox{5, 6, 7};
  std::map<bsl6d::AxisType, double> k0{
      {bsl6d::AxisType::x1, 4.1887902047863905},
      {bsl6d::AxisType::x2, 3.141592653589793},
      {bsl6d::AxisType::x3, 2.0943951023931953}};
  std::array<double, 10> k1Coord{
      0.,           4.1887902,    8.37758041,   12.56637061, 16.75516082,
      -20.94395102, -16.75516082, -12.56637061, -8.37758041, -4.1887902};
  std::array<double, 12> k2Coord{0.,           3.14159265,   6.28318531,
                                 9.42477796,   12.56637061,  15.70796327,
                                 -18.84955592, -15.70796327, -12.56637061,
                                 -9.42477796,  -6.28318531,  -3.14159265};
  std::array<double, 14> k3Coord{
      0.,          2.0943951,   4.1887902,    6.28318531,   8.37758041,
      10.47197551, 12.56637061, -14.66076572, -12.56637061, -10.47197551,
      -8.37758041, -6.28318531, -4.1887902,   -2.0943951};

  OperatorsHeFFTe() = default;

  /* Functions to simplify Test and improve readability */
  /*
  \brief Init 3d-cos signal to be transformed
   */
  template <typename ViewType>
  ViewType init_global_x_space(std::array<size_t, 3> Nx) {
    ViewType fx{"RealData", Nx[0], Nx[1], Nx[2]};

    typedef typename ViewType::execution_space exec_space;
    Kokkos::parallel_for(
        "Init cos",
        Kokkos::MDRangePolicy<Kokkos::Rank<3>, exec_space>{
            {0, 0, 0}, {fx.extent(0), fx.extent(1), fx.extent(2)}},
        KOKKOS_LAMBDA(size_t i0, size_t i1, size_t i2) {
          constexpr double pi2{M_PI * 2};
          fx(i0, i1, i2) = std::cos(pi2 / fx.extent(2) * (double)i2) *
                           std::cos(pi2 / fx.extent(1) * (double)i1 * 2) * 2 *
                           std::cos(pi2 / fx.extent(0) * (double)i0 * 3) * 3;
        });

    return fx;
  }

  /*
  \brief Init Proc local distribution function from global distribution
         function
  */
  Kokkos::View<double ***> init_local_x_space(
      Kokkos::View<double ***> const &fx,
      bsl6d::Grid<3, 0> const &myGridX) {
    Kokkos::View<double ***> fx_local{
        "local_fx_data", myGridX(bsl6d::AxisType::x1).N(),
        myGridX(bsl6d::AxisType::x2).N(), myGridX(bsl6d::AxisType::x3).N()};

    Kokkos::parallel_for(
        "initialize input local",
        Kokkos::MDRangePolicy<Kokkos::Rank<3>>{
            {myGridX(bsl6d::AxisType::x1).low(),
             myGridX(bsl6d::AxisType::x2).low(),
             myGridX(bsl6d::AxisType::x3).low()},
            {myGridX(bsl6d::AxisType::x1).up() + 1,
             myGridX(bsl6d::AxisType::x2).up() + 1,
             myGridX(bsl6d::AxisType::x3).up() + 1}},
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
          fx_local(ix1 - myGridX(bsl6d::AxisType::x1).low(),
                   ix2 - myGridX(bsl6d::AxisType::x2).low(),
                   ix3 - myGridX(bsl6d::AxisType::x3).low()) =
              fx(ix1, ix2, ix3);
        });

    return fx_local;
  }

  /*
  \brief Init squared modulus of transformed signal (3d)
  */
  Kokkos::View<std::complex<double> ***, Kokkos::HostSpace> init_global_k_space(
      std::array<size_t, 3> Nx) {
    Kokkos::View<std::complex<double> ***, Kokkos::HostSpace> fk{};

    fk = Kokkos::View<std::complex<double> ***, Kokkos::HostSpace>{
        "CompareComplexData", Nx[0], Nx[1], Nx[2]};

    Kokkos::deep_copy(fk, {0, 0});
    fk(3, 2, 1) +=
        std::complex<double>{Nx[0] * 3. / 2. * Nx[1] * Nx[2] / 2., 0};

    fk(3, Nx[1] - 2, 1) += fk(3, 2, 1);
    fk(3, 2, Nx[2] - 1) += fk(3, 2, 1);
    fk(3, Nx[1] - 2, Nx[2] - 1) += fk(3, 2, 1);
    fk(Nx[0] - 3, 2, 1) += fk(3, 2, 1);
    fk(Nx[0] - 3, 2, Nx[2] - 1) += fk(3, 2, 1);
    fk(Nx[0] - 3, Nx[1] - 2, 1) += fk(3, 2, 1);
    fk(Nx[0] - 3, Nx[1] - 2, Nx[2] - 1) += fk(3, 2, 1);
    return fk;
  }

  /*
  \brief Compare Values within two Views (3d)
  */
  template <typename ViewTypeLeft, typename ViewTypeRight>
  void gtest_compare(ViewTypeLeft const &left,
                     bsl6d::FastFourierTransform3d const &fft,
                     ViewTypeRight const &right,
                     std::string const &label) {
    double err = 0;
    Kokkos::parallel_reduce(
        "Check Results of 3dFFT",
        fft.fourier_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
        [=](size_t i, size_t j, size_t k, double &lerr) {
          lerr = std::max(
              lerr,
              std::abs(
                  left(i, j, k) -
                  right(i + fft.fourier_grid().lowK(bsl6d::AxisType::x1),
                        j + fft.fourier_grid().lowK(bsl6d::AxisType::x2),
                        k + fft.fourier_grid().lowK(bsl6d::AxisType::x3))));
        },
        Kokkos::Max<double>(err));
    ASSERT_LE(err, 1e-11) << "||FFT(fx)||_max>1e-10 " << label;
  }

  /*
  \brief Compare Values within two Views (3d)
  */
  template <typename ViewTypeLeft, typename ViewTypeRight>
  void gtest_compare(ViewTypeLeft const &left,
                     bsl6d::Grid<3, 0> const &myGridX,
                     ViewTypeRight const &right,
                     std::string const &label) {
    double err = 0;
    Kokkos::parallel_reduce(
        "Check Results of 3dFFT",
        myGridX.mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
        [=](size_t i, size_t j, size_t k, double &lerr) {
          lerr = std::max(
              lerr, std::abs(left(i, j, k) -
                             right(myGridX(bsl6d::AxisType::x1).low() + i,
                                   myGridX(bsl6d::AxisType::x2).low() + j,
                                   myGridX(bsl6d::AxisType::x3).low() + k)));
        },
        Kokkos::Max<double>(err));
    ASSERT_LE(err, 1e-11) << "||FFT(fx)||_max>1e-10 in " << label;
  }
};

/*****************************************************************/
/*

\brief Test Configuration of distributed HeFFTe.

*/
/*****************************************************************/
TEST_F(OperatorsHeFFTe, Configuration) {
  /* Resulted in error on GPU therefore add this lines to test*/
  bsl6d::GridDistribution<3, 0> gridDistribX{
      gridDistrib.create_xGridDistribution()};
  bsl6d::Grid<3, 0> myGridX{gridDistribX.my_local_grid()};

  bsl6d::FastFourierTransform3d fft{gridDistribX};
  bsl6d::Impl::FourierGrid fourierGrid{fft.fourier_grid()};

  for (auto axis : gridX.xAxes)
    EXPECT_DOUBLE_EQ(k0[axis], fourierGrid.k0(axis))
        << "Rank: " << topology.my_cart_rank()
        << "; Axis: " << axis_to_string(axis);
  for (auto axis : gridX.xvAxes) {
    EXPECT_EQ(fourierGrid.sizeK(axis), localKBox[static_cast<size_t>(axis)]);
    EXPECT_EQ(fourierGrid.sizeX(axis), myGridX(axis).N());
  }

  for (size_t i = fourierGrid.lowK(bsl6d::AxisType::x1);
       i <= fourierGrid.upK(bsl6d::AxisType::x1); i++)
    EXPECT_NEAR(fourierGrid.k(bsl6d::AxisType::x1,
                              i - fourierGrid.lowK(bsl6d::AxisType::x1)),
                k1Coord[i], 1e-7)
        << "Idx: " << i;
  for (size_t i = fourierGrid.lowK(bsl6d::AxisType::x2);
       i <= fourierGrid.upK(bsl6d::AxisType::x2); i++)
    EXPECT_NEAR(fourierGrid.k(bsl6d::AxisType::x2,
                              i - fourierGrid.lowK(bsl6d::AxisType::x2)),
                k2Coord[i], 1e-7)
        << "Idx: " << i;
  for (size_t i = fourierGrid.lowK(bsl6d::AxisType::x3);
       i <= fourierGrid.upK(bsl6d::AxisType::x3); i++)
    EXPECT_NEAR(fourierGrid.k(bsl6d::AxisType::x3,
                              i - fourierGrid.lowK(bsl6d::AxisType::x3)),
                k3Coord[i], 1e-7)
        << "Idx: " << i;
}

/*****************************************************************/
/*

\brief Test Fourier Transform

*/
/*****************************************************************/
TEST_F(OperatorsHeFFTe, Transform) {
  /* Resulted in error on GPU therefore add this lines to test*/
  bsl6d::GridDistribution<3, 0> gridDistribX{
      gridDistrib.create_xGridDistribution()};
  bsl6d::Grid<3, 0> myGridX{gridDistribX.my_local_grid()};

  bsl6d::FastFourierTransform3d fft{gridDistribX};

  std::array<size_t, 3> globalXBox{gridX(bsl6d::AxisType::x1).N(),
                                   gridX(bsl6d::AxisType::x2).N(),
                                   gridX(bsl6d::AxisType::x3).N()};

  Kokkos::View<double ***> fx{
      init_global_x_space<Kokkos::View<double ***>>(globalXBox)};
  bsl6d::ScalarField fxRealSpace{gridDistribX, "RealSpace"};
  Kokkos::deep_copy(fxRealSpace.device(),
                    init_local_x_space(fx, fxRealSpace.grid()));

  /***************************************************************/
  // Transform x2k
  Kokkos::View<std::complex<double> ***> fk_local{
      fft(fxRealSpace, "FourierSpace")};
  EXPECT_EQ(fk_local.label(), "FourierSpace");
  Kokkos::View<std::complex<double> ***>::HostMirror h_fk_local{
      Kokkos::create_mirror_view(fk_local)};
  Kokkos::deep_copy(h_fk_local, fk_local);
  Kokkos::View<std::complex<double> ***, Kokkos::HostSpace> fk_ref{
      init_global_k_space(globalXBox)};

  gtest_compare(h_fk_local, fft, fk_ref, "compare forward trafo");

  /***************************************************************/
  // Transform k2x
  Kokkos::View<double ***, Kokkos::HostSpace> fx_ref{
      init_global_x_space<Kokkos::View<double ***, Kokkos::HostSpace>>(
          globalXBox)};
  Kokkos::deep_copy(fxRealSpace.device(), 0);
  fxRealSpace = fft(bsl6d::Inverse{}, fk_local, "RealSpace");
  EXPECT_EQ(fxRealSpace.device().label(), "RealSpace");
  Kokkos::deep_copy(fxRealSpace.host(), fxRealSpace.device());

  gtest_compare(fxRealSpace.host(), myGridX, fx_ref, "compare backward trafo");
}

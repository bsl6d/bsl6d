/*****************************************************************/
/*

\brief Testing the settings and functionalities of the lagrange
       interpolation of the bsl6d project using gtest (Stencil)

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.10.2021

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include "../fdDerivativeStencils.hpp"

template <typename StencilType>
void stencilApply(StencilType stencil,
                  double gridSpacing,
                  std::pair<int, int> derivativeDirection,
                  Kokkos::View<double**>& f,
                  Kokkos::View<double[2]>& fres) {
  size_t derivIdx = std::floor(stencil.width / 2.0);
  Kokkos::parallel_for(
      "Derivative StencilTest", 1, KOKKOS_LAMBDA(size_t i) {
        fres(0) = stencil.deriveIdx(&f(0, derivIdx), gridSpacing, f.stride(1),
                                    derivativeDirection.first);
        fres(1) = stencil.deriveIdx(&f(derivIdx, f.extent(1) - 1), gridSpacing,
                                    f.stride(0), derivativeDirection.second);
      });
}

template <typename StencilType>
void test_stencil_to_monomial(StencilType derivStencil) {
  std::pair<int, int> derivativeDirection{-1, 1};
  size_t domainWidth{};
  double gridSpacing{0.5};
  if (derivStencil.width % 2 == 0) {
    domainWidth = derivStencil.width + 1;
  } else {
    domainWidth = derivStencil.width;
  }
  Kokkos::View<double**> f{"IntrpStart", domainWidth, domainWidth};
  Kokkos::View<double[2]> fres{"IntrpRes"};

  Kokkos::View<double**>::HostMirror h_f{};
  Kokkos::View<double[2]>::HostMirror h_fres{};
  h_f    = Kokkos::create_mirror_view(f);
  h_fres = Kokkos::create_mirror_view(fres);

  Kokkos::deep_copy(h_f, 1.);
  for (size_t i = 0; i < f.extent(0); i++)
    for (size_t j = 0; j < f.extent(1); j++) {
      h_f(i, j) = std::pow((i + j), derivStencil.width - 1);
    }

  Kokkos::deep_copy(f, h_f);
  stencilApply(derivStencil, gridSpacing, derivativeDirection, f, fres);
  Kokkos::deep_copy(h_fres, fres);

  double derivedPointX = std::floor(f.extent(0) / 2.0);
  EXPECT_DOUBLE_EQ(h_fres(0),
                   (derivStencil.width - 1) *
                       std::pow(derivedPointX, derivStencil.width - 2) /
                       gridSpacing)
      << "Width: " << derivStencil.width;
  double derivedPointY = std::floor(f.extent(1) / 2.0) + (f.extent(0) - 1);
  EXPECT_DOUBLE_EQ(h_fres(1),
                   (derivStencil.width - 1) *
                       std::pow(derivedPointY, derivStencil.width - 2) /
                       gridSpacing)
      << "Width: " << derivStencil.width;
}

TEST(OperatorsFDDerivativeStencils, DeriveSinglePoint) {
  test_stencil_to_monomial<bsl6d::FDDerivativeStencil3>(
      bsl6d::FDDerivativeStencil3{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil4>(
      bsl6d::FDDerivativeStencil4{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil8>(
      bsl6d::FDDerivativeStencil8{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil5>(
      bsl6d::FDDerivativeStencil5{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil7>(
      bsl6d::FDDerivativeStencil7{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil9>(
      bsl6d::FDDerivativeStencil9{});

  test_stencil_to_monomial<bsl6d::FDDerivativeStencil11>(
      bsl6d::FDDerivativeStencil11{});
}

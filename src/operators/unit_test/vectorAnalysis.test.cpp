#include <array>
#include <cmath>
#include <iostream>

#include <gtest/gtest.h>
#include <Kokkos_Core.hpp>

#include <mathNorms.hpp>
#include <vectorAnalysis.hpp>

bsl6d::ScalarField get_scalarField() {
  bsl6d::GridDistribution<3, 3> gridDist{bsl6d::GridDistributionConfig{}};
  bsl6d::ScalarField s1{gridDist.create_xGridDistribution(), "ScalarField1"};

  double k0x1{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L()};
  double k0x2{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L()};
  double k0x3{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x3).L()};
  auto analytical = KOKKOS_LAMBDA(double x1, double x2, double x3) {
    return std::sin(k0x1 * x1) + std::cos(2.0 * k0x2 * x2) -
           std::sin(2.0 * k0x3 * x3);
  };
  bsl6d::Impl::fill(s1, analytical);
  return s1;
};

bsl6d::ScalarField get_laplace_scalarField() {
  bsl6d::GridDistribution<3, 3> gridDist{bsl6d::GridDistributionConfig{}};
  bsl6d::ScalarField s1{gridDist.create_xGridDistribution(), "ScalarField1"};

  double k0x1{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L()};
  double k0x2{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L()};
  double k0x3{2.0 * M_PI /
              s1.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x3).L()};
  auto analytical = KOKKOS_LAMBDA(double x1, double x2, double x3) {
    return -k0x1 * k0x1 * std::sin(k0x1 * x1) -
           4.0 * k0x2 * k0x2 * std::cos(2.0 * k0x2 * x2) +
           4.0 * k0x3 * k0x3 * std::sin(2.0 * k0x3 * x3);
  };
  bsl6d::Impl::fill(s1, analytical);
  return s1;
};

TEST(OperatorsVectorAnalysis, GradDivEqualLaplace) {
  bsl6d::ScalarField s1{get_scalarField()};

  bsl6d::Gradient grad{s1.gridDistribution()};
  bsl6d::Divergence div{s1.gridDistribution()};
  bsl6d::Laplace laplace{s1.gridDistribution()};

  EXPECT_LT(bsl6d::LInf_error(div(grad(s1, ""), ""), get_laplace_scalarField()),
            1.0e-12);
  EXPECT_LT(bsl6d::LInf_error(laplace(s1, ""), get_laplace_scalarField()),
            1.0e-12);
  EXPECT_LT(bsl6d::LInf_error(div(grad(s1, ""), ""), laplace(s1, "")), 1.0e-12);
}

TEST(OperatorVectorAnalysis, InvertLaplace) {
  bsl6d::ScalarField s1{get_laplace_scalarField()};

  bsl6d::Laplace laplace{s1.gridDistribution()};

  EXPECT_LT(
      bsl6d::LInf_error(laplace(bsl6d::Inverse{}, s1, ""), get_scalarField()),
      1.0e-12);
}

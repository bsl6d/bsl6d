/*****************************************************************/
/*

\brief Test FFT Derivative

\author Mario Raeth
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   21.01.2022

*/
/*****************************************************************/
#include <gtest/gtest.h>

#include <array>
#include <iostream>

#include <Kokkos_Core.hpp>

#include <config.hpp>
#include <grid.hpp>
#include <mathNorms.hpp>
#include <scalarField.hpp>

#include "../derivative.hpp"

class OperatorsDerivative
    : public ::testing::TestWithParam<std::tuple<bsl6d::AxisType, size_t>> {
  public:
  OperatorsDerivative() = default;

  bsl6d::GridDistribution<3, 3> const gridDistribution{
      bsl6d::GridDistributionConfig{}};
  bsl6d::AxisType axis{std::get<bsl6d::AxisType>(GetParam())};

  void cos_cos_cos_plus_sin_sin_sin(bsl6d::ScalarField const &f) {
    size_t mk{std::get<size_t>(GetParam())};
    double const k0x1{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x1).L()};
    double const k0x2{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x2).L()};
    double const k0x3{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x3).L()};
    auto cosCosCosPlusSinSinSin =
        KOKKOS_LAMBDA(double x1, double x2, double x3)->double {
      return std::cos(k0x1 * mk * x1) * std::cos(k0x2 * mk * x2) *
                 std::cos(k0x3 * mk * x3) +
             0.0 * std::sin(k0x1 * mk * x1) * std::sin(k0x2 * mk * x2) *
                 std::sin(k0x3 * mk * x3);
    };
    bsl6d::Impl::fill(f, cosCosCosPlusSinSinSin);
  }
  void derivative_cos_cos_cos_plus_sin_sin_sin(bsl6d::ScalarField const &f) {
    size_t mk{std::get<size_t>(GetParam())};
    double const k0x1{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x1).L()};
    double const k0x2{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x2).L()};
    double const k0x3{
        2 * M_PI / gridDistribution.mpi_global_grid()(bsl6d::AxisType::x3).L()};
    bsl6d::AxisType l_axis{axis};
    auto dcosCosCosPlusSinSinSin =
        KOKKOS_LAMBDA(double x1, double x2, double x3)->double {
      switch (l_axis) {
        case bsl6d::AxisType::x1:
          return k0x1 * mk *
                 (-std::sin(k0x1 * mk * x1) * std::cos(k0x2 * mk * x2) *
                      std::cos(k0x3 * mk * x3) +
                  0.0 * std::cos(k0x1 * mk * x1) * std::sin(k0x2 * mk * x2) *
                      std::sin(k0x3 * mk * x3));
        case bsl6d::AxisType::x2:
          return k0x2 * mk *
                 (std::cos(k0x1 * mk * x1) * -std::sin(k0x2 * mk * x2) *
                      std::cos(k0x3 * mk * x3) +
                  0.0 * std::sin(k0x1 * mk * x1) * std::cos(k0x2 * mk * x2) *
                      std::sin(k0x3 * mk * x3));
        case bsl6d::AxisType::x3:
          return k0x3 * mk *
                 (std::cos(k0x1 * mk * x1) * std::cos(k0x2 * mk * x2) *
                      -std::sin(k0x3 * mk * x3) +
                  0.0 * std::sin(k0x1 * mk * x1) * std::sin(k0x2 * mk * x2) *
                      std::cos(k0x3 * mk * x3));
      }
      return 0.0;
    };
    bsl6d::Impl::fill(f, dcosCosCosPlusSinSinSin);
  }
};

TEST_P(OperatorsDerivative, DerivativeOfCosine) {
  bsl6d::ScalarField f{gridDistribution.create_xGridDistribution(),
                       "TestDerivative"};
  cos_cos_cos_plus_sin_sin_sin(f);

  bsl6d::Derivative derivative{axis, f.gridDistribution()};
  bsl6d::ScalarField df{derivative(f, "TestDerivative")};

  bsl6d::ScalarField df_exact{f.gridDistribution(), "TestDerivative"};
  derivative_cos_cos_cos_plus_sin_sin_sin(df_exact);
  EXPECT_LT(bsl6d::LInf_error(df, df_exact), 1.0e-12);
}

TEST_P(OperatorsDerivative, IntegralOfCosine) {
  bsl6d::ScalarField f{gridDistribution.create_xGridDistribution(),
                       "TestIntegral"};
  derivative_cos_cos_cos_plus_sin_sin_sin(f);

  bsl6d::Derivative derivative{axis, f.gridDistribution()};
  bsl6d::ScalarField intf{derivative(bsl6d::Inverse{}, f, "TestIntegral")};

  bsl6d::ScalarField intf_exact{f.gridDistribution(), "TestIntegral"};
  cos_cos_cos_plus_sin_sin_sin(intf_exact);
  EXPECT_LT(bsl6d::LInf_error(intf, intf_exact), 1.0e-12);
}

INSTANTIATE_TEST_SUITE_P(
    OperatorsDerivative_allDimSomeModes,
    OperatorsDerivative,
    ::testing::Combine(::testing::Values(bsl6d::AxisType::x1,
                                         bsl6d::AxisType::x2,
                                         bsl6d::AxisType::x3),
                       ::testing::Values(1, 2, 3, 4)),
    [](const ::testing::TestParamInfo<OperatorsDerivative::ParamType> &info) {
      bsl6d::AxisType axis{std::get<bsl6d::AxisType>(info.param)};
      size_t mode{std::get<size_t>(info.param)};
      std::string name{"Axis" + bsl6d::axis_to_string(axis) + "FourierMode" +
                       std::to_string(mode)};
      return name;
    });

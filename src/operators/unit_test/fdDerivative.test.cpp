#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include "../fdDerivative.hpp"

void initialize_monomial(bsl6d::ScalarField &scalarField,
                         size_t monomialDegree,
                         bsl6d::AxisType axis) {
  /* First index in index range */
  for (int i = 0; i < 5; i++) {
    scalarField.host()(i, 0, 0) =
        std::pow(scalarField.grid()(axis)(i + 1), monomialDegree) + 1;
    scalarField.host()(scalarField.grid()(bsl6d::AxisType::x1).up() - i, 0, 0) =
        std::pow(scalarField.grid()(axis)(-i), monomialDegree) + 1;
  }

  /* Last index in index range */
  for (int i = 0; i < 5; i++) {
    scalarField.host()(scalarField.grid()(bsl6d::AxisType::x1).up() - i,
                       scalarField.grid()(bsl6d::AxisType::x2).up(),
                       scalarField.grid()(bsl6d::AxisType::x3).up()) =
        std::pow(scalarField.grid()(axis)(-i - 1), monomialDegree) + 1;
    scalarField.host()(i, scalarField.grid()(bsl6d::AxisType::x2).up(),
                       scalarField.grid()(bsl6d::AxisType::x3).up()) =
        std::pow(scalarField.grid()(axis)(i), monomialDegree) + 1;
  }

  Kokkos::deep_copy(scalarField.device(), scalarField.host());
};

TEST(OperatorsFDDerivative, Operator) {
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::CartTopology<6> topo{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 0> gridDistrib{
      bsl6d::GridDistribution<3, 3>{grid, topo}.create_xGridDistribution()};
  bsl6d::ScalarField scalarField{gridDistrib, "TestDerivative"};
  bsl6d::ScalarField result{gridDistrib, "Result"};
  bsl6d::AxisType axis{bsl6d::AxisType::x1};
  Kokkos::View<int ***> derivativeDirections{
      "FDDerivativeDirections", scalarField.grid()(bsl6d::AxisType::x1).N(),
      grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()};

  Kokkos::View<int ***>::HostMirror h_derivativeDirections{
      Kokkos::create_mirror_view(derivativeDirections)};
  Kokkos::deep_copy(h_derivativeDirections, 0);
  h_derivativeDirections(scalarField.grid()(bsl6d::AxisType::x1).up(),
                         grid(bsl6d::AxisType::x2).up(),
                         grid(bsl6d::AxisType::x3).up()) = -1;
  Kokkos::deep_copy(derivativeDirections, h_derivativeDirections);
  bsl6d::FDDerivative fdDerivative{axis, scalarField.grid()};
  fdDerivative.set_derivativeDirections(derivativeDirections);

  fdDerivative.prepare_input_argument_for_operator(scalarField);
  EXPECT_EQ(
      scalarField.device().extent(static_cast<size_t>(axis)) - grid(axis).N(),
      8);  // Compare halo width that needs to be created
  EXPECT_THROW(fdDerivative(bsl6d::Inverse{}, scalarField, "Result"),
               std::runtime_error);

  initialize_monomial(scalarField, 3, axis);

  EXPECT_NO_THROW(result = fdDerivative(scalarField, result.device().label()));

  Kokkos::deep_copy(result.host(), result.device());
  EXPECT_NEAR(result.host()(0, 0, 0), 3 * std::pow(grid(axis)(1), 2), 1e-15);
  EXPECT_NEAR(result.host()(grid(bsl6d::AxisType::x1).up(),
                            grid(bsl6d::AxisType::x2).up(),
                            grid(bsl6d::AxisType::x3).up()),
              3 * std::pow(grid(axis)(1), 2), 1e-15);
}

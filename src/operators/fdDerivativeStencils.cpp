/*****************************************************************/
/*

\brief Lagrange Stenicls that can be used for interpolation

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   12.01.2021

*/
/*****************************************************************/

#include <fdDerivativeStencils.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief FactoryMethod to return different stencil classes inside a
       variant.

*/
/*****************************************************************/
FDDerivativeStencilVariants create_finite_difference_stencil(
    size_t const width) {
  switch (width) {
    case (FDDerivativeStencil3::width):
      return FDDerivativeStencilVariants{FDDerivativeStencil3{}};
    default:
      throw std::invalid_argument("Not implemented Stencil width given: " +
                                  std::to_string(width));
  }
};

} /* namespace bsl6d */

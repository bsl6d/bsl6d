/*****************************************************************/
/*

\brief Lagrange Stenicls that can be used for interpolation

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   12.01.2021

\description
  To add a new stencil:
    1. Define the new stencil-class at the top of this file
    2. Add the new stencil class to the stencilVariants std::variant.
    3. Create the new stencil class with the SAME interface as the
       currently available stencil classes using TDD.
    4. Add the new stencil to the factory method.

*/
/*****************************************************************/

#pragma once

#include <variant>

#include <Kokkos_Core.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief Stencil classes that are available and variant to store any
       of the available classes.

*/
/*****************************************************************/
class FDDerivativeStencil3;
class FDDerivativeStencil4;
class FDDerivativeStencil8;

// Only use high order stencils to reduce compile time in
// lagrange interpolation
using FDDerivativeStencilVariants = std::
    variant<FDDerivativeStencil3, FDDerivativeStencil4, FDDerivativeStencil8>;

/*****************************************************************/
/*

\brief FactoryMethod to return different stencil classes inside a
       variant.
*/
/*****************************************************************/
FDDerivativeStencilVariants create_finite_difference_stencil(
    size_t const width);

/*****************************************************************/
/*

\brief Three point center finite-difference first-derivative stencil

*/
/*****************************************************************/
class FDDerivativeStencil3 {
  public:
  FDDerivativeStencil3() = default;

  static constexpr size_t width{3};

  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection = 0) const {
    return (-0.5 * f_ptr[0 - stride] + 0.5 * f_ptr[0 + stride]) / dx;
  };
};

class FDDerivativeStencil4 {
  public:
  FDDerivativeStencil4() = default;

  static constexpr size_t width{4};

  KOKKOS_INLINE_FUNCTION
  double deriveIdxForward(double const* f_ptr,
                          double const dx,
                          size_t const stride) const {
    return (-inv_3 * f_ptr[0 - stride] - inv_2 * f_ptr[0] + f_ptr[0 + stride] -
            inv_6 * f_ptr[0 + 2 * stride]) /
           dx;
  };
  KOKKOS_INLINE_FUNCTION
  double deriveIdxBackward(double const* f_ptr,
                           double const dx,
                           size_t const stride) const {
    return (inv_6 * f_ptr[0 - 2 * stride] - f_ptr[0 - stride] +
            inv_2 * f_ptr[0] + inv_3 * f_ptr[0 + stride]) /
           dx;
  };
  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection) const {
    double forward  = this->deriveIdxForward(f_ptr, dx, stride);
    double backward = this->deriveIdxBackward(f_ptr, dx, stride);

    return (derivativeDirection >= 0) ? forward : backward;
  }

  private:
  static constexpr double inv_2{1.0 / 2.0};
  static constexpr double inv_3{1.0 / 3.0};
  static constexpr double inv_6{1.0 / 6.0};
};

class FDDerivativeStencil5 {
  public:
  FDDerivativeStencil5() = default;

  static constexpr size_t width{5};

  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection = 0) const {
    return (inv_12 * f_ptr[0 - 2 * stride] - 2 * inv_3 * f_ptr[0 - stride] +
            2 * inv_3 * f_ptr[0 + stride] - inv_12 * f_ptr[0 + 2 * stride]) /
           dx;
  }

  private:
  static constexpr double inv_3{1.0 / 3.0};
  static constexpr double inv_12{1.0 / 12.0};
};

class FDDerivativeStencil7 {
  public:
  FDDerivativeStencil7() = default;

  static constexpr size_t width{7};

  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection = 0) const {
    return (-inv_60 * f_ptr[0 - 3 * stride] +
            3 * inv_20 * f_ptr[0 - 2 * stride] - 3 * inv_4 * f_ptr[0 - stride] +
            3 * inv_4 * f_ptr[0 + stride] - 3 * inv_20 * f_ptr[0 + 2 * stride] +
            inv_60 * f_ptr[0 + 3 * stride]) /
           dx;
  }

  private:
  static constexpr double inv_4{1.0 / 4.0};
  static constexpr double inv_20{1.0 / 20.0};
  static constexpr double inv_60{1.0 / 60.0};
};

class FDDerivativeStencil8 {
  public:
  FDDerivativeStencil8() = default;

  static constexpr size_t width{8};

  KOKKOS_INLINE_FUNCTION
  double deriveIdxForward(double const* f_ptr,
                          double const dx,
                          size_t const stride) const {
    return (-inv_105 * f_ptr[0 - 3 * stride] + inv_10 * f_ptr[0 - 2 * stride] -
            3.0 * inv_5 * f_ptr[0 - stride] - inv_4 * f_ptr[0] +
            f_ptr[0 + stride] - 3.0 * inv_10 * f_ptr[0 + 2 * stride] +
            inv_15 * f_ptr[0 + 3 * stride] - inv_140 * f_ptr[0 + 4 * stride]) /
           dx;
  };
  KOKKOS_INLINE_FUNCTION
  double deriveIdxBackward(double const* f_ptr,
                           double const dx,
                           size_t const stride) const {
    return (inv_140 * f_ptr[0 - 4 * stride] - inv_15 * f_ptr[0 - 3 * stride] +
            3 * inv_10 * f_ptr[0 - 2 * stride] - f_ptr[0 - stride] +
            inv_4 * f_ptr[0] + 3 * inv_5 * f_ptr[0 + stride] -
            inv_10 * f_ptr[0 + 2 * stride] + inv_105 * f_ptr[0 + 3 * stride]) /
           dx;
  };
  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection) const {
    double forward  = this->deriveIdxForward(f_ptr, dx, stride);
    double backward = this->deriveIdxBackward(f_ptr, dx, stride);

    return (derivativeDirection >= 0) ? forward : backward;
  }

  private:
  static constexpr double inv_4{1.0 / 4.0};
  static constexpr double inv_5{1.0 / 5.0};
  static constexpr double inv_10{1.0 / 10.0};
  static constexpr double inv_15{1.0 / 15.0};
  static constexpr double inv_105{1.0 / 105.0};
  static constexpr double inv_140{1.0 / 140.0};
};

class FDDerivativeStencil9 {
  public:
  FDDerivativeStencil9() = default;

  static constexpr size_t width{9};

  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection) const {
    return (inv_280 * f_ptr[0 - 4 * stride] -
            4 * inv_105 * f_ptr[0 - 3 * stride] +
            inv_5 * f_ptr[0 - 2 * stride] - 4 * inv_5 * f_ptr[0 - stride] +
            4 * inv_5 * f_ptr[0 + stride] - inv_5 * f_ptr[0 + 2 * stride] +
            4 * inv_105 * f_ptr[0 + 3 * stride] -
            inv_280 * f_ptr[0 + 4 * stride]) /
           dx;
  }

  private:
  static constexpr double inv_5{1.0 / 5.0};
  static constexpr double inv_105{1.0 / 105.0};
  static constexpr double inv_280{1.0 / 280.0};
};

class FDDerivativeStencil11 {
  public:
  FDDerivativeStencil11() = default;

  static constexpr size_t width{11};

  KOKKOS_INLINE_FUNCTION
  double deriveIdx(double const* f_ptr,
                   double const dx,
                   size_t const stride,
                   int derivativeDirection) const {
    return (-inv_1260 * f_ptr[0 - 5 * stride] +
            5 * inv_504 * f_ptr[0 - 4 * stride] -
            5 * inv_84 * f_ptr[0 - 3 * stride] +
            5 * inv_21 * f_ptr[0 - 2 * stride] - 5 * inv_6 * f_ptr[0 - stride] +
            5 * inv_6 * f_ptr[0 + stride] - 5 * inv_21 * f_ptr[0 + 2 * stride] +
            5 * inv_84 * f_ptr[0 + 3 * stride] -
            5 * inv_504 * f_ptr[0 + 4 * stride] +
            inv_1260 * f_ptr[0 + 5 * stride]) /
           dx;
  }

  private:
  static constexpr double inv_6{1.0 / 6.0};
  static constexpr double inv_21{1.0 / 21.0};
  static constexpr double inv_84{1.0 / 84.0};
  static constexpr double inv_504{1.0 / 504.0};
  static constexpr double inv_1260{1.0 / 1260.0};
};

} /* namespace bsl6d */

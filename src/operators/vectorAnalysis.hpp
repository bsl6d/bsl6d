#pragma once

#include <baseOperator.hpp>
#include <derivative.hpp>
#include <vectorField.hpp>

namespace bsl6d {

class Gradient : public Operator<ScalarField, VectorField> {
  public:
  Gradient() = default;
  Gradient(GridDistribution<3, 0> const &gridDistribX);

  VectorField operator()(ScalarField const &input,
                         std::string const label) const override;
  ScalarField operator()(Inverse,
                         VectorField const &input,
                         std::string const label) const override;

  private:
  std::array<Derivative, 3> derivative{};
};

class Divergence : public Operator<VectorField, ScalarField> {
  public:
  Divergence() = default;
  Divergence(GridDistribution<3, 0> const &gridDistribX);

  ScalarField operator()(VectorField const &input,
                         std::string const label) const override;
  VectorField operator()(Inverse,
                         ScalarField const &input,
                         std::string const label) const override;

  private:
  std::array<Derivative, 3> m_derivative{};
};

class Laplace : public Operator<ScalarField, ScalarField> {
  public:
  Laplace() = default;
  Laplace(GridDistribution<3, 0> const &gridDistributionX);

  ScalarField operator()(ScalarField const &input,
                         std::string const label) const override;
  ScalarField operator()(Inverse,
                         ScalarField const &input,
                         std::string const label) const override;

  private:
  FastFourierTransform3d m_fft{};
};

} /* namespace bsl6d */

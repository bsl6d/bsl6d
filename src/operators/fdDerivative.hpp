#pragma once
#include <baseOperator.hpp>
#include <fdDerivativeStencils.hpp>
#include <halo.hpp>
#include <mpiTopology.hpp>
#include <scalarField.hpp>

namespace bsl6d {

class FDDerivative : public Operator<ScalarField, ScalarField> {
  public:
  FDDerivative() = default;
  FDDerivative(AxisType axis, Grid<3, 0> grid);

  ScalarField operator()(ScalarField const& input,
                         std::string const resultLabel) const override;
  ScalarField operator()(Inverse,
                         ScalarField const& input,
                         std::string const resultLabel) const override;
  void prepare_input_argument_for_operator(ScalarField& input) override;
  void set_derivativeDirections(
      Kokkos::View<int***> const& derivativeDirections);

  private:
  AxisType m_axis{};
  FDDerivativeStencil9 m_stencil{};
  Grid<3, 0> m_grid{};
  HaloComm m_halo{};
  Kokkos::View<int***> m_derivativeDirections{};
};

}  // namespace bsl6d

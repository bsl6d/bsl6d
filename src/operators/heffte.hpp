/*****************************************************************/
/*

\brief Fast Fourier Transform on different Views using FFTW on CPU
       and cuFFTw if BSL6D_ENABLE_GPU is ON

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.01.2022

*/
/*****************************************************************/

#pragma once

#include <array>
#include <complex>
#include <memory>
#include <string>

#include <heffte.h>
#include <Kokkos_Core.hpp>

#include <baseOperator.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <scalarField.hpp>

namespace bsl6d {

namespace Impl {
struct FourierGrid {
  GridDistribution<3, 0> m_gridDistribution{};
  std::array<size_t, 3> m_lowK{}, m_upK{}, m_sizeK{};
  std::array<double, 3> m_k0{};
  size_t sizeX(AxisType const axis) const {
    return static_cast<size_t>(m_gridDistribution.mpi_local_grid()(axis).N());
  };
  KOKKOS_INLINE_FUNCTION
  size_t sizeK(AxisType const axis) const {
    return m_sizeK[static_cast<size_t>(axis)];
  };
  KOKKOS_INLINE_FUNCTION
  size_t lowK(AxisType const axis) const {
    return m_lowK[static_cast<size_t>(axis)];
  };
  KOKKOS_INLINE_FUNCTION
  size_t upK(AxisType const axis) const {
    return m_upK[static_cast<size_t>(axis)];
  };
  KOKKOS_INLINE_FUNCTION
  double k0(AxisType const axis) const {
    return m_k0[static_cast<size_t>(axis)];
  }
  KOKKOS_INLINE_FUNCTION
  double k(AxisType axis, size_t idx) const {
    return (lowK(axis) + idx) <
                   m_gridDistribution.mpi_global_grid()(axis).N() / 2
               ? (lowK(axis) + idx) * k0(axis)
               : -static_cast<int>(
                     m_gridDistribution.mpi_global_grid()(axis).N() -
                     (lowK(axis) + idx)) *
                     k0(axis);
  }
  template <typename KokkosExecutionSpace = Kokkos::DefaultExecutionSpace>
  Kokkos::MDRangePolicy<Kokkos::Rank<3>, KokkosExecutionSpace> mdRangePolicy() {
    Kokkos::Array<size_t, 3> start{};
    Kokkos::Array<size_t, 3> stop{};
    for (size_t i = 0; i < 3; i++) {
      start[i] = 0;
      stop[i]  = m_sizeK[i];
    }
    return Kokkos::MDRangePolicy<Kokkos::Rank<3>, KokkosExecutionSpace>{start,
                                                                        stop};
  }
};

}  // namespace Impl

#if defined(KOKKOS_ENABLE_CUDA)
using HeFFTeType = heffte::fft3d<heffte::backend::cufft>;
#elif defined(KOKKOS_ENABLE_HIP)
using HeFFTeType = heffte::fft3d<heffte::backend::rocfft>;
#else
using HeFFTeType = heffte::fft3d<heffte::backend::fftw>;
#endif

class FastFourierTransform3d
    : public Operator<ScalarField, Kokkos::View<std::complex<double> ***>> {
  public:
  FastFourierTransform3d() = default;
  FastFourierTransform3d(GridDistribution<3, 0> const &gridDistX);

  Kokkos::View<std::complex<double> ***> operator()(
      ScalarField const &fx,
      std::string const label) const override;
  ScalarField operator()(Inverse,
                         Kokkos::View<std::complex<double> ***> const &fk,
                         std::string const label) const override;

  Impl::FourierGrid fourier_grid() const { return m_grid; }

  private:
  std::shared_ptr<HeFFTeType> m_fft{};
  Impl::FourierGrid m_grid{};
};

} /* namespace bsl6d */

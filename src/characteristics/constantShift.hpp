/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#pragma once

#include <Kokkos_Core.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>

#include <baseCharacteristic.hpp>

namespace bsl6d {

class Constant0DShift : public Characteristic {
  public:
  Constant0DShift() = default;
  Constant0DShift(double _dxv);

  double update(AxisType axis) const override { return m_dxv; }

  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_dxv;
  }

  private:
  double m_dxv{};
};

class Constant5DShift : public Characteristic {
  public:
  Constant5DShift() = default;
  Constant5DShift(std::array<size_t, 6> const _Nxv,
                  size_t const seed     = 0,
                  double const boundary = 1.0);

  double update(AxisType axis) const override;

  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[0](i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[1](i0, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[2](i0, i1, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[3](i0, i1, i2, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[4](i0, i1, i2, i3, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts[5](i0, i1, i2, i3, i4);
  }

  Kokkos::View<double*****> device(AxisType axis) const {
    return m_shifts[static_cast<size_t>(axis)];
  }
  Kokkos::View<double*****>::HostMirror host(AxisType axis) const {
    return m_h_shifts[static_cast<size_t>(axis)];
  }

  private:
  Kokkos::View<double*> m_baseShift{};
  Kokkos::View<double*>::HostMirror m_h_baseShift{};

  using ShiftsViewType =
      Kokkos::View<double*****, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
  std::array<ShiftsViewType, 6> m_shifts{};
  std::array<ShiftsViewType::HostMirror, 6> m_h_shifts{};
};

class Constant6DShift : public Characteristic {
  public:
  Constant6DShift() = default;
  Constant6DShift(std::array<size_t, 6> const _Nxv,
                  size_t const seed     = 0,
                  double const boundary = 1.0);

  double update(AxisType axis) const override { return m_maxShift; };

  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return m_shifts(i0, i1, i2, i3, i4, i5);
  }

  Kokkos::View<double******> device() const { return m_shifts; }
  Kokkos::View<double******>::HostMirror host() const { return m_h_shifts; }

  private:
  Kokkos::View<double******> m_shifts{};
  Kokkos::View<double******>::HostMirror m_h_shifts{};
  double m_maxShift{};
};

}  // namespace bsl6d

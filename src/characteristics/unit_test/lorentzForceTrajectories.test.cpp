#include <gtest/gtest.h>

#include <advector.hpp>
#include <backtracer.hpp>
#include <characteristicHelpers.hpp>
#include <initializationFunctions.hpp>
#include <integrator.hpp>
#include <io_init.hpp>
#include <lagrangeBuilder.hpp>
#include "characteristicUnitTestHelpers.hpp"

#include "../lorentzForce.hpp"
#include "../lorentzForceRotatingGrid.hpp"

template <typename CharacteristicType>
class CharacteristicLorentzForceTrajectories : public ::testing::Test {
  public:
  static nlohmann::json create_config_file() {
    nlohmann::json configFile =
        R"({
        "simulationTime": {
          "restart": false
        },
        "gridDistribution": {
          "grid": {
            "axes": ["x1","x2","v1","v2"],
            "NxvPoints": [32, 32, 33, 33],
            "position": ["L", "L", "N", "N"]
          },
          "parallel": {}
        }, 
        "initializationDistributionFunction": {
          "initFunction": "planewave_dens_perturbation",
          "mk": [1.0, 1.0, 0.0],
          "xv0": [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
          "temperature": 1.0
        },
        "operators": {
          "interpolator": {
            "stencilWidth": [10,10,10,10,10,10],
            "teamSize": [0,0,0,0,0,0],
            "vectorSize": [0,0,0,0,0,0]
          }
        },
        "fieldSolver":{
          "backgroundField":{
            "Bx3": 1.0
          }
        },
        "integrator": 
          [
            {"token": "advectV", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
            {"token": "advectVAdj", "t0Offset": 0.5, "dtRatio": 0.5},
            {"token": "advanceTbydt", "t0Offset": 0.0, "dtRatio": 0.0}
          ]
      })"_json;
    std::vector<double> xvMax{3.0 * M_PI, 4.0 * M_PI, 9.0, 9.0};
    std::vector<double> xvMin{-xvMax[0], -xvMax[1], -xvMax[2], -xvMax[3]};
    configFile["gridDistribution"]["grid"]["xvMax"]           = xvMax;
    configFile["gridDistribution"]["grid"]["xvMin"]           = xvMin;
    configFile["simulationTime"]["dt"]                        = M_PI / 240.0;
    configFile["simulationTime"]["T"]                         = M_PI / 2.0;
    configFile["initializationDistributionFunction"]["alpha"] = 0.1;
    if (std::is_same_v<CharacteristicType, bsl6d::LorentzForceRotatingGrid>) {
      configFile["gridDistribution"]["grid"]["rotateGyroFreq"] = true;
    } else if (std::is_same_v<CharacteristicType, bsl6d::LorentzForce>) {
      configFile["gridDistribution"]["grid"]["rotateGyroFreq"] = false;
    }
    return configFile;
  }
  bsl6d::MaxwellFields maxwellFields{};

  CharacteristicLorentzForceTrajectories() = default;
};

using CharacteristicTypes =
    ::testing::Types<bsl6d::LorentzForce, bsl6d::LorentzForceRotatingGrid>;
TYPED_TEST_SUITE(CharacteristicLorentzForceTrajectories, CharacteristicTypes);

TYPED_TEST(CharacteristicLorentzForceTrajectories, ETimesBDrift) {
  auto [simTime, f6d] = bsl6d::initialize(this->create_config_file());

  double Ex{0.01};
  double Bx3{};
  double alpha{};
  double vx0{};
  double kx0{};
  double ky0{};
  Bx3 = bsl6d::config_file()["fieldSolver"]["backgroundField"]["Bx3"]
            .get<double>();
  alpha = bsl6d::config_file()["initializationDistributionFunction"]["alpha"]
              .get<double>();
  vx0 = bsl6d::config_file()["initializationDistributionFunction"]["xv0"][3]
            .get<double>();
  kx0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
  ky0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();
  auto const f0 = [&](double x1, double x2, double x3, double v1, double v2,
                      double v3) -> double {
    return (1.0 + alpha * std::sin(kx0 * x1 + ky0 * x2)) *
           bsl6d::Impl::gauss(v1, vx0, 1.0) * bsl6d::Impl::gauss(v2, 0.0, 1.0);
  };

  this->maxwellFields.E = bsl6d::VectorField{
      f6d.gridDistribution().create_xGridDistribution(), "E"};
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x1).device(), Ex);
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x2).device(), 0.0);
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x3).device(), 0.0);
  this->maxwellFields.Bx3 = Bx3;

  bsl6d::Integrator integrate{bsl6d::IntegratorConfig{bsl6d::config_file()},
                              f6d};

  while (simTime.continue_advection()) {
    integrate(f6d, this->maxwellFields, simTime);
  }

  double wc{this->maxwellFields.Bx3 * f6d.q() / f6d.m()};
  bsl6d::FullKineticVlasovBacktracer tracer{
      wc, Ex, f6d.mpi_local_grid().rotating_velocity_grid()};
  bsl6d::AnalyticalSolution ana{f6d.gridDistribution(), f0, tracer};
  // Both errors are at least two magnitudes smaller than the maximum
  // absolute change of f6d. This has been estimated in the LInf_error
  // function.
  EXPECT_NEAR(LInf_error(f6d, ana, simTime.current_T()), 0.0, 7.0e-4);
}

TYPED_TEST(CharacteristicLorentzForceTrajectories, Gyromotion) {
  auto [simTime, f6d] = bsl6d::initialize(this->create_config_file());

  double Bx3{};
  double alpha{};
  double vx0{};
  double kx0{};
  double ky0{};
  Bx3 = bsl6d::config_file()["fieldSolver"]["backgroundField"]["Bx3"]
            .get<double>();
  alpha = bsl6d::config_file()["initializationDistributionFunction"]["alpha"]
              .get<double>();
  vx0 = bsl6d::config_file()["initializationDistributionFunction"]["xv0"][3]
            .get<double>();
  kx0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
  ky0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();
  auto const f0 = [&](double x1, double x2, double x3, double v1, double v2,
                      double v3) -> double {
    return (1.0 + alpha * std::sin(kx0 * x1 + ky0 * x2)) *
           bsl6d::Impl::gauss(v1, vx0, 1.0) * bsl6d::Impl::gauss(v2, 0.0, 1.0);
  };

  this->maxwellFields.E = bsl6d::VectorField{
      f6d.gridDistribution().create_xGridDistribution(), "E"};
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x1).device(), 0.0);
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x2).device(), 0.0);
  Kokkos::deep_copy(this->maxwellFields.E(bsl6d::AxisType::x3).device(), 0.0);
  this->maxwellFields.Bx3 = Bx3;

  bsl6d::Integrator integrate{bsl6d::IntegratorConfig{bsl6d::config_file()},
                              f6d};

  while (simTime.continue_advection()) {
    integrate(f6d, this->maxwellFields, simTime);
  }

  double wc{this->maxwellFields.Bx3 * f6d.q() / f6d.m()};
  double Ex{0.0};
  bsl6d::FullKineticVlasovBacktracer tracer{
      wc, Ex, f6d.mpi_local_grid().rotating_velocity_grid()};
  bsl6d::AnalyticalSolution ana{f6d.gridDistribution(), f0, tracer};
  // Rotating gird has a much higher precision compared to the simple
  // Strang-Splitting approach. Both errors are at least two magnitudes
  // smaller than the maximum absolute change of f6d
  EXPECT_NEAR(LInf_error(f6d, ana, simTime.current_T()), 0.0, 5.0e-4);
}

#include <gtest/gtest.h>

#include <advector.hpp>
#include <backtracer.hpp>
#include <freeStreaming.hpp>
#include <grid.hpp>
#include <initializationFunctions.hpp>
#include <lagrangeBuilder.hpp>
#include "characteristicUnitTestHelpers.hpp"

TEST(CharacteristicFreeStreaming, Advection) {
  nlohmann::json configFile =
      R"({
        "gridDistribution": {
          "grid": {
            "axes": ["x1","x2","v1","v2"],
            "xvMax": [16.0,16.0,7.0,7.0],
            "xvMin": [0.0,0.0,-7.0,-7.0],
            "NxvPoints": [32, 32, 33, 33],
            "position": ["L", "L", "N", "N"],
            "rotateGyroFreq": false
          },
          "parallel": {
            "Nprocs": [1,1,1,1,1,1]
          }
        },
        "operators": {
          "interpolator": {
            "stencilWidth": [12,12,12,12,12,12],
            "teamSize": [0,0,0,0,0,0],
            "vectorSize": [0,0,0,0,0,0]
          }
        },
        "simulationTime": {
          "dt": 0.01,
          "T": 1.0,
          "restart": false
        },
        "distributionFunction": {
          "q": 1.0,
          "m": 1.0
        },
        "initializationDistributionFunction": {
          "initFunction": "planewave_dens_perturbation",
          "mk": [1.0, 1.0, 0.0],
          "sigma2": [1.0,1.0,1.0],
          "temperature": 1.0
        }
  })"_json;

  double vx0{1.0}, vy0{2.0}, alpha{0.01};
  configFile["initializationDistributionFunction"]["alpha"] = alpha;
  configFile["initializationDistributionFunction"]["xv0"] =
      std::vector<double>{0.0, 0.0, 0.0, vx0, vy0, 0.0};

  auto [simTime, f6d] = bsl6d::initialize(configFile);

  double kx0{};
  double ky0{};
  kx0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
  ky0 = 2.0 * M_PI /
        f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x2).L();
  auto const f0 = [&](double x1, double x2, double x3, double v1, double v2,
                      double v3) -> double {
    return (1 + alpha * std::sin(kx0 * x1 + ky0 * x2)) *
           bsl6d::Impl::gauss(v1, vx0, 1.0) * bsl6d::Impl::gauss(v2, vy0, 1.0);
  };

  std::shared_ptr<bsl6d::FreeStreaming> characteristicPtr{
      std::make_shared<bsl6d::FreeStreaming>(bsl6d::FreeStreaming{f6d})};

  bsl6d::LagrangeBuilder lagrangeBuilder{};
  lagrangeBuilder.setLagrangeConf(bsl6d::OperatorsConfig{configFile});
  lagrangeBuilder.set_gridDistribution(f6d.gridDistribution());

  std::map<bsl6d::AxisType, bsl6d::Advector> advector{};
  for (auto axis : bsl6d::xvAxes())
    advector[axis] = bsl6d::Advector{
        characteristicPtr, bsl6d::CharacteristicVariants{characteristicPtr},
        axis, lagrangeBuilder};

  while (simTime.continue_advection()) {
    for (auto axis : bsl6d::xvAxes())
      advector[axis](f6d, simTime.current_T(), simTime.dt());
    simTime.advance_current_T_by_dt();
  }

  bsl6d::AnalyticalSolution ana{f6d.gridDistribution(), f0,
                                bsl6d::FreeStreamingBacktracer{}};
  EXPECT_NEAR(LInf_error(f6d, ana, simTime.current_T()), 0.0, 1.0e-12);
}

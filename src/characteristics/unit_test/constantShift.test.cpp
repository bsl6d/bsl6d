/*****************************************************************/
/*

\brief Testing a characteristic for a constant shift parameter

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <constantShift.hpp>

/*****************************************************************/
/*

\brief Test Constant ShiftParameter

*/
/*****************************************************************/
TEST(CharacteristicConstant0DShift, Init) {
  bsl6d::Constant0DShift{};

  bsl6d::Constant0DShift shift_05{.5};

  EXPECT_EQ(shift_05.shift_x1(1, 2, 3, 4, 5, 6), .5);

  bsl6d::Constant0DShift shift_03{.3};

  EXPECT_EQ(shift_03.shift_x1(1, 2, 3, 4, 5, 6), .3);
}

template <typename T>
class CharacteristicRandomConstant : public ::testing::Test {
  public:
  CharacteristicRandomConstant() = default;
  using Constant0DShiftType      = T;
  T t{};
};

using ShiftTypes =
    ::testing::Types<bsl6d::Constant5DShift, bsl6d::Constant6DShift>;
TYPED_TEST_SUITE(CharacteristicRandomConstant, ShiftTypes);

TYPED_TEST(CharacteristicRandomConstant, Init) {
  TypeParam{};
  TypeParam randTest{{5, 6, 7, 8, 9, 10}};

  for (auto axis :
       {bsl6d::AxisType::x1, bsl6d::AxisType::x2, bsl6d::AxisType::x3,
        bsl6d::AxisType::v1, bsl6d::AxisType::v2, bsl6d::AxisType::v3}) {
    size_t size{};
    double* host_ptr{};
    if constexpr (std::is_same_v<TypeParam, bsl6d::Constant6DShift>) {
      auto view{randTest.host()};
      size     = view.size();
      host_ptr = view.data();
    } else if constexpr (std::is_same_v<TypeParam, bsl6d::Constant5DShift>) {
      auto view{randTest.host(axis)};
      size     = view.size();
      host_ptr = view.data();
    }
    double avg{0};
    Kokkos::parallel_reduce(
        "Test Rand Init",
        Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, size),
        [=](size_t i, double& sum) {
          EXPECT_GE(host_ptr[i], -1.);
          EXPECT_LE(host_ptr[i], 1.);
          sum += host_ptr[i];
        },
        avg);

    avg = avg / size;
    EXPECT_NEAR(avg, 0.0, .01);

    double var{0};
    Kokkos::parallel_reduce(
        "Test Rand Init",
        Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, size),
        [=](size_t i, double& var) {
          var += (avg - host_ptr[i]) * (avg - host_ptr[i]);
        },
        var);

    var = var / size;
    EXPECT_NEAR(var, 1. / 12., .005);
  }
}
#pragma once

#include <Kokkos_Core.hpp>

#include <grid.hpp>
/*
 Returns the shift of a Characteristic class for the given axis at
 the index idx.

 Example:
  shift(bsl6d::AxisType::x3,vpB,{1,2,3,4,5,6})
 equivalent to:
  vpB.shift_x3(1,2,3,4,5,6)
*/
template <typename CharacteristicType>
double shift(bsl6d::AxisType const &axis,
             CharacteristicType const &charac,
             std::array<size_t, 6> const &idx) {
  Kokkos::View<double[1]> res{"AdvResut"};
  Kokkos::View<double[1]>::HostMirror h_res{Kokkos::create_mirror_view(res)};
  Kokkos::parallel_for(
      "Calculate Shift", 1, KOKKOS_LAMBDA(size_t tmp) {
        if (axis == bsl6d::AxisType::x1) {
          res[0] =
              charac.shift_x1(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        } else if (axis == bsl6d::AxisType::x2) {
          res[0] =
              charac.shift_x2(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        } else if (axis == bsl6d::AxisType::x3) {
          res[0] =
              charac.shift_x3(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        } else if (axis == bsl6d::AxisType::v1) {
          res[0] =
              charac.shift_v1(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        } else if (axis == bsl6d::AxisType::v2) {
          res[0] =
              charac.shift_v2(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        } else if (axis == bsl6d::AxisType::v3) {
          res[0] =
              charac.shift_v3(idx[0], idx[1], idx[2], idx[3], idx[4], idx[5]);
        }
      });
  Kokkos::deep_copy(h_res, res);
  return h_res[0];
}

template <typename AnalyticalSolutionType>
double LInf_error(bsl6d::DistributionFunction const &sim,
                  AnalyticalSolutionType const &ana,
                  double const &t) {
  double err{};
  Kokkos::deep_copy(sim.host(), sim.device());
  Kokkos::parallel_reduce(
      "LInf Error",
      sim.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [&](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double &lerr) {
        lerr =
            std::max(lerr, std::abs(sim.host()(ix1, ix2, ix3, iv1, iv2, iv3) -
                                    ana(ix1, ix2, ix3, iv1, iv2, iv3, t)));
      },
      Kokkos::Max<double>(err));
  return err;
}

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 16:46:58 2022

@author: nilsch

Test values for shifts in all dimensions of the LorentzForceRotatingGrid 
Characteristic with a rotating grid implemented in lorentzForceRotatingGrid.hpp.
"""

import numpy as np

# General variables
dt= 0.1
q=-2
m=1.5
Bx3 = 3.
gyro_freq = q*Bx3/m
T=5.
dx12 = np.array((1.5/10.,2./12.))
dv12 = np.array((4./14.,5./16.))

# x-v-Space B dependent dimensions
dx3 = 3./14.
dv3 = 6./18.
x3min=0
v3min=-3.
Ex3 = 3.
print("x3:")
print(-dt*(dv3*6-3)/dx3)
print("v3")
print(-dt*(Ex3*q/m)/dv3)

# x-Space B dependent dimensions
d,offd=np.sin(dt*gyro_freq), 1-np.cos(dt*gyro_freq)
A_T = np.array([[d,-offd],[offd,d]])/gyro_freq
d,offd = np.cos(T*gyro_freq), np.sin(T*gyro_freq)
D_inv = np.array([[d,offd],[-offd,d]])

v12 = dv12*np.array((4.,5.)) - np.array((2.,2.5))
v12 = np.dot(D_inv,v12)
print("x1:")
print(- dt * v12[0]/dx12[0])
print("x2:")
print(- dt * v12[1]/dx12[1])
#v12 = np.dot(A_T,v12)
#print("x1:")
#print(-v12[0]/dx12[0])
#print("x2:")
#print(-v12[1]/dx12[1])

# v-Space B dependent dimensions
d,offd=np.sin(- dt*gyro_freq), 1-np.cos(- dt*gyro_freq)
A = np.array([[d,offd],[-offd,d]])/gyro_freq
d,offd = np.cos(T*gyro_freq), np.sin(T*gyro_freq)
D = np.array([[d,-offd],[offd,d]])

Exy = np.array((1.,2.))
Exy = np.dot(D,Exy)
#print("v1:")
#print(- dt * q/m * (Exy[0])/dv12[0])
#print("v2:")
#print(- dt * q/m * (Exy[1])/dv12[1])
Exy = np.dot(A,Exy)
print("v1:")
print(q/m*(Exy[0])/dv12[0])
print("v2:")
print(q/m*(Exy[1])/dv12[1])

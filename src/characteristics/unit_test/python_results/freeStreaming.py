#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 16:46:58 2022

@author: nilsch

Test values for shifts in all dimensions of the Vlasov-Poisson model
Characteristic with a implemented in lorentzForce.hpp.
"""

import numpy as np

# General variables
dt= 0.1

# x-Space
dx123 = np.array((1.5/10.,2./12.,3./14.))
dv123 = np.array((4./14.,5./16.,6./18.))
vmax123 = np.array((2.,2.5,3.))
v123 = dv123*np.array((4.,5.,6.)) - vmax123
print("x1:")
print(-dt*v123[0]/dx123[0], -dt*vmax123[0]/dx123[0])
print("x2:")
print(-dt*v123[1]/dx123[1], -dt*vmax123[1]/dx123[1])
print("x3:")
print(-dt*v123[2]/dx123[2], -dt*vmax123[2]/dx123[2])
#include <numeric>

#include <gtest/gtest.h>

#include <linearAlgebra.hpp>

#include "characteristicHelpers.hpp"

TEST(CharacteristicHelpers, MaxAbs) {
  Kokkos::View<double **> view{"View", 2, 2};
  Kokkos::View<double **>::HostMirror h_view{Kokkos::create_mirror_view(view)};
  h_view(0, 0) = 0.0;
  h_view(0, 1) = 2.0;
  h_view(1, 0) = 3.0;
  h_view(1, 1) = 4.0;
  Kokkos::deep_copy(view, h_view);
  EXPECT_DOUBLE_EQ(bsl6d::Impl::abs_max(view), 4.0);
  h_view(1, 0) = -6.0;
  Kokkos::deep_copy(view, h_view);
  EXPECT_DOUBLE_EQ(bsl6d::Impl::abs_max(view), 6.0);
}

template <typename IteratorLeft, typename IteratorRight>
double L1_error(IteratorLeft const &a, IteratorRight const &b) {
  auto absminus = [](double a, double b) -> double { return std::abs(a - b); };
  return std::inner_product(a.data.begin(), a.data.end(), b.data.begin(), 0.,
                            std::plus<>{}, absminus);
}

TEST(CharacteristicHelpers, RotationMatrix) {
  // Contains operator*(...) for Matrix-Matrix and Matrix-Vector Multiplication
  using namespace bsl6d;
  Vector v;
  Vector v_PiHalf;
  double res{};

  v        = {1.0, 0.0, 1.0};
  v_PiHalf = {0.0, -1.0, 1.0};
  EXPECT_EQ(bsl6d::Impl::RotationMatrix::R_x3_inv(0.0) * v, v);
  res =
      L1_error(bsl6d::Impl::RotationMatrix::R_x3_inv(M_PI / 2.0) * v, v_PiHalf);
  EXPECT_LT(res, 1.0e-15);
  EXPECT_EQ(bsl6d::Impl::RotationMatrix::R_x3(M_PI / 2.0) * v,
            bsl6d::Impl::RotationMatrix::R_x3_inv(-M_PI / 2.0) * v);

  v        = {1.0, 1.0, 0.0};
  v_PiHalf = {0.0, 1.0, -1.0};
  EXPECT_EQ(v, bsl6d::Impl::RotationMatrix::R_x2_inv(0.0) * v);
  res =
      L1_error(v_PiHalf, bsl6d::Impl::RotationMatrix::R_x2_inv(M_PI / 2.) * v);
  EXPECT_LT(res, 1.0e-15);
  EXPECT_EQ(bsl6d::Impl::RotationMatrix::R_x2(M_PI / 2.0) * v,
            bsl6d::Impl::RotationMatrix::R_x2_inv(-M_PI / 2.0) * v);

  v        = {1.0, 1.0, 0.0};
  v_PiHalf = {1.0, 0.0, -1.0};
  EXPECT_EQ(v, bsl6d::Impl::RotationMatrix::R_x1_inv(0.0) * v);
  res =
      L1_error(v_PiHalf, bsl6d::Impl::RotationMatrix::R_x1_inv(M_PI / 2.0) * v);
  EXPECT_LT(res, 1.0e-15);
  EXPECT_EQ(bsl6d::Impl::RotationMatrix::R_x2(M_PI / 2.0) * v,
            bsl6d::Impl::RotationMatrix::R_x2_inv(-M_PI / 2.0) * v);
}

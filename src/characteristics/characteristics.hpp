/*****************************************************************/
/*

\brief Collectiv header of all characteristics

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#pragma once

#include <memory>
#include <string>
#include <variant>

#include <constantShift.hpp>
#include <freeStreaming.hpp>
#include <lorentzForce.hpp>
#include <lorentzForceRotatingGrid.hpp>

namespace bsl6d {

using CharacteristicVariants =
    std::variant<std::shared_ptr<Constant0DShift>,
                 std::shared_ptr<Constant5DShift>,
                 // std::shared_ptr<Constant6DShift>,
                 std::shared_ptr<LorentzForce>,
                 std::shared_ptr<LorentzForceRotatingGrid>,
                 std::shared_ptr<FreeStreaming>>;

/*****************************************************************/
/*

\brief Builder class to build and return different characteristic classes
       This function has mainly testing purposes to test if different
       characteristics can be build correctly using class LagrangeBuilder.

*/
/*****************************************************************/
class CharacteristicBuilder {
  public:
  CharacteristicBuilder() = default;

  void set_shift(double const shift);
  void set_Nxv(std::array<size_t, 6> const _Nxv, size_t dim);

  CharacteristicVariants create_characteristic(
      CharacteristicId const characteristicId) const;

  private:
  CharacteristicVariants characteristicVariant{};
};

} /* namespace bsl6d */

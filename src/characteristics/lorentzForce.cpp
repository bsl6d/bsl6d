/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#include <characteristicHelpers.hpp>
#include <lorentzForce.hpp>

namespace bsl6d {

LorentzForce::LorentzForce(DistributionFunction f6d)
    : Characteristic(CharacteristicId::LorentzForce, f6d)
    , m_x1Shift{"LFShiftx1", f6d.mpi_local_grid()(AxisType::v1).N()}
    , m_x2Shift{"LFShiftx2", f6d.mpi_local_grid()(AxisType::v2).N()}
    , m_x3Shift{"LFShiftx3", f6d.mpi_local_grid()(AxisType::v3).N()}
    , m_v1Shift{create_grid_compatible_view(
          f6d.mpi_local_grid(),
          "LFShiftv1",
          std::array{AxisType::x1, AxisType::x2, AxisType::x3, AxisType::v2})}
    , m_v2Shift{create_grid_compatible_view(
          f6d.mpi_local_grid(),
          "LFShiftv2",
          std::array{AxisType::x1, AxisType::x2, AxisType::x3, AxisType::v1})}
    , m_v3Shift{
          create_grid_compatible_view(f6d.mpi_local_grid().create_xSpace(),
                                      "LFShiftv3")} {}

double LorentzForce::update(bsl6d::AxisType axis) const {
  Grid<3, 3> grid{_f6d.mpi_local_grid()};
  VectorField E{m_maxwellFields.E};
  double dt{_dt}, q{_f6d.q()}, m{_f6d.m()}, Bx3{m_maxwellFields.Bx3};

  switch (axis) {
    case AxisType::x1: {
      Kokkos::View<double*> shift{m_x1Shift};
      Kokkos::parallel_for(
          "UpdateVPx1", m_x1Shift.size(), KOKKOS_LAMBDA(size_t iv1) {
            shift(iv1) =
                -dt * grid(AxisType::v1)(iv1) / grid(AxisType::x1).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::x2: {
      Kokkos::View<double*> shift{m_x2Shift};
      Kokkos::parallel_for(
          "UpdateVPx2", m_x2Shift.size(), KOKKOS_LAMBDA(size_t iv2) {
            shift(iv2) =
                -dt * grid(AxisType::v2)(iv2) / grid(AxisType::x2).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::x3: {
      Kokkos::View<double*> shift{m_x3Shift};
      Kokkos::parallel_for(
          "UpdateVPx3", m_x3Shift.size(), KOKKOS_LAMBDA(size_t iv3) {
            shift(iv3) =
                -dt * grid(AxisType::v3)(iv3) / grid(AxisType::x3).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v1: {
      Kokkos::View<double****> shift{m_v1Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<4>> mdRangePolicy{create_mdRangePolicy(
          grid,
          std::array{AxisType::x1, AxisType::x2, AxisType::x3, AxisType::v2})};
      Kokkos::parallel_for(
          "UpdateVPv1", mdRangePolicy,
          KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv2) {
            shift(ix1, ix2, ix3, iv2) = -dt *
                                        (E(AxisType::x1)(ix1, ix2, ix3) +
                                         Bx3 * grid(AxisType::v2)(iv2)) *
                                        q / m / grid(AxisType::v1).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v2: {
      Kokkos::View<double****> shift{m_v2Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<4>> mdRangePolicy{create_mdRangePolicy(
          grid,
          std::array{AxisType::x1, AxisType::x2, AxisType::x3, AxisType::v1})};
      Kokkos::parallel_for(
          "UpdateVPv2", mdRangePolicy,
          KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1) {
            shift(ix1, ix2, ix3, iv1) = -dt *
                                        (E(AxisType::x2)(ix1, ix2, ix3) -
                                         Bx3 * grid(AxisType::v1)(iv1)) *
                                        q / m / grid(AxisType::v2).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v3: {
      Kokkos::View<double***> shift{m_v3Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{create_mdRangePolicy(
          grid, std::array{AxisType::x1, AxisType::x2, AxisType::x3})};
      Kokkos::parallel_for(
          "UpdateVPv3", mdRangePolicy,
          KOKKOS_LAMBDA(size_t i0, size_t i1, size_t i2) {
            shift(i0, i1, i2) = -dt * E(AxisType::x3)(i0, i1, i2) * q / m /
                                grid(AxisType::v3).delta();
          });
      return Impl::abs_max(shift);
    }
  }
  // Can not be reached just to remove compiler warning
  return 0;
}

} /* namespace bsl6d */

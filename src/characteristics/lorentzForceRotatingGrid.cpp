/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#include <characteristicHelpers.hpp>

#include <lorentzForceRotatingGrid.hpp>

namespace bsl6d {

LorentzForceRotatingGrid::LorentzForceRotatingGrid(
    DistributionFunction const &f6d)
    : Characteristic{CharacteristicId::LorentzForceRotatingGrid, f6d}
    , m_x1Shift{"LFRotGridShiftx1", f6d.mpi_local_grid()(AxisType::v1).N(),
                f6d.mpi_local_grid()(AxisType::v2).N()}
    , m_x2Shift{"LFRotGridShiftx2", f6d.mpi_local_grid()(AxisType::v1).N(),
                f6d.mpi_local_grid()(AxisType::v2).N()}
    , m_x3Shift{"LFRotGridShiftx3", f6d.mpi_local_grid()(AxisType::v3).N()}
    , m_v1Shift{create_grid_compatible_view(
          f6d.mpi_local_grid().create_xSpace(),
          "LFRotGridShiftv1")}
    , m_v2Shift{create_grid_compatible_view(
          f6d.mpi_local_grid().create_xSpace(),
          "LFRotGridShiftv2")}
    , m_v3Shift{
          create_grid_compatible_view(f6d.mpi_local_grid().create_xSpace(),
                                      "LFRotGridShiftv3")} {};

double LorentzForceRotatingGrid::update(bsl6d::AxisType axis) const {
  Grid<3, 3> grid{_f6d.mpi_local_grid()};
  VectorField E{m_maxwellFields.E};
  double const dt{_dt}, T{_simulationTime};
  double const q{_f6d.q()}, m{_f6d.m()};
  double const gf{gyro_freq()};
  switch (axis) {
    case AxisType::x1: {
      Kokkos::View<double **> shift{m_x1Shift};
      Kokkos::Array<size_t, 2> startIdx{0, 0};
      Kokkos::Array<size_t, 2> stopIdx{m_x1Shift.extent(0),
                                       m_x1Shift.extent(1)};
      Kokkos::MDRangePolicy<Kokkos::Rank<2>> mdRangePolicy{startIdx, stopIdx};
      Kokkos::parallel_for(
          "UpdateVPConstBx1", mdRangePolicy,
          KOKKOS_LAMBDA(size_t iv1, size_t iv2) {
            Vector const v{grid(AxisType::v1)(iv1), grid(AxisType::v2)(iv2),
                           0.0};
            Vector const ARv{
                Impl::RotationMatrix::integrated_R_x3_inv(T + dt, -dt, gf) * v};
            shift(iv1, iv2) = ARv(0) / grid(AxisType::x1).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::x2: {
      Kokkos::View<double **> shift{m_x2Shift};
      Kokkos::Array<size_t, 2> startIdx{0, 0};
      Kokkos::Array<size_t, 2> stopIdx{m_x2Shift.extent(0),
                                       m_x2Shift.extent(1)};
      Kokkos::MDRangePolicy<Kokkos::Rank<2>> mdRangePolicy{startIdx, stopIdx};
      Kokkos::parallel_for(
          "UpdateVPConstBx2", mdRangePolicy,
          KOKKOS_LAMBDA(size_t iv1, size_t iv2) {
            Vector const v{grid(AxisType::v1)(iv1), grid(AxisType::v2)(iv2),
                           0.0};
            Vector const ARv{
                Impl::RotationMatrix::integrated_R_x3_inv(T + dt, -dt, gf) * v};
            shift(iv1, iv2) = ARv(1) / grid(AxisType::x2).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::x3: {
      Kokkos::View<double *> shift{m_x3Shift};
      Kokkos::parallel_for(
          "UpdateVPConstBx3", m_x3Shift.size(), KOKKOS_LAMBDA(size_t iv3) {
            shift(iv3) =
                -dt * grid(AxisType::v3)(iv3) / grid(AxisType::x3).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v1: {
      Kokkos::View<double ***> shift{m_v1Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
          grid.create_xSpace().mdRangePolicy()};
      Kokkos::parallel_for(
          "UpdateVPConstBv1", mdRangePolicy,
          KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
            Vector const Exyz{E(AxisType::x1)(ix1, ix2, ix3),
                              E(AxisType::x2)(ix1, ix2, ix3), 0.0};
            Vector const ARE{
                Impl::RotationMatrix::integrated_R_x3(T + dt, -dt, gf) * Exyz};
            shift(ix1, ix2, ix3) = ARE(0) * q / m / grid(AxisType::v1).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v2: {
      Kokkos::View<double ***> shift{m_v2Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
          grid.create_xSpace().mdRangePolicy()};
      Kokkos::parallel_for(
          "UpdateVPConstBv2", mdRangePolicy,
          KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
            Vector const Exyz{E(AxisType::x1)(ix1, ix2, ix3),
                              E(AxisType::x2)(ix1, ix2, ix3), 0.0};
            Vector const ARE{
                Impl::RotationMatrix::integrated_R_x3(T + dt, -dt, gf) * Exyz};
            shift(ix1, ix2, ix3) = ARE(1) * q / m / grid(AxisType::v2).delta();
          });
      return Impl::abs_max(shift);
    }
    case AxisType::v3: {
      Kokkos::View<double ***> shift{m_v3Shift};
      Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
          grid.create_xSpace().mdRangePolicy()};
      Kokkos::parallel_for(
          "UpdateVPConstBv3", mdRangePolicy,
          KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
            shift(ix1, ix2, ix3) = -dt * E(bsl6d::AxisType::x3)(ix1, ix2, ix3) *
                                   q / m / grid(AxisType::v3).delta();
          });
      return Impl::abs_max(shift);
    }
  }
  // Can not be reached just to remove compiler warning
  return 0;
}
} /* namespace bsl6d */

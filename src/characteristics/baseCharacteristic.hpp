/*****************************************************************/
/*

\brief Base Characteristic Datatype

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#pragma once
#include <baseFieldSolver.hpp>
#include <distributionFunction.hpp>
#include <vectorField.hpp>

namespace bsl6d {

enum class CharacteristicId {
  Constant0DShift,
  Constant5DShift,
  Constant6DShift,
  FreeStreaming,
  LorentzForce,
  LorentzForceRotatingGrid
};

class Characteristic {
  public:
  Characteristic() = default;

  // It would be sufficient to initialize the Characteristic using the
  // gridDistribution instead of the full distribution function. Maybe adapt
  // the constructor to only utilize GridDistribution instead of
  // DistributionFunction
  Characteristic(CharacteristicId const charcId, DistributionFunction const f6d)
      : _f6d{f6d}
      , _id{charcId} {};
  KOKKOS_FUNCTION
  virtual ~Characteristic(){};
  CharacteristicId id() const { return _id; };

  /**
   * To achieve good performance (on CPU mostly) only Views should be called
   *  wrapped by shift_<dim>(). This function updates Views if it is called
   *  and returns maximum shift.
   */
  // Rename to calculate_shifts or update_shifts
  virtual double update(AxisType axis) const = 0;

  KOKKOS_INLINE_FUNCTION
  double gyro_freq() const {
    return _f6d.q() * m_maxwellFields.Bx3 / _f6d.m();
  };

  void set_dt(double const dt) { _dt = dt; };
  void set_maxwellFields(MaxwellFields const maxwellFields) {
    m_maxwellFields = maxwellFields;
  };
  void set_distributionFunction(DistributionFunction const f6d) { _f6d = f6d; };
  void set_simulationTime(double const T) { _simulationTime = T; };

  protected:
  double _dt{0};
  DistributionFunction _f6d{};
  MaxwellFields m_maxwellFields{};
  double _simulationTime{};

  private:
  CharacteristicId _id{};
};

} /* namespace bsl6d */

/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#pragma once

#include <Kokkos_Core.hpp>
#include <grid.hpp>

#include <baseCharacteristic.hpp>

namespace bsl6d {

class FreeStreaming : public Characteristic {
  public:
  FreeStreaming() = default;
  FreeStreaming(DistributionFunction f6d);

  double update(AxisType axis) const override;

  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return x1Shift(i3);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return x2Shift(i4);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return x3Shift(i5);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return 0;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return 0;
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const i0,
                  size_t const i1,
                  size_t const i2,
                  size_t const i3,
                  size_t const i4,
                  size_t const i5) const {
    return 0;
  }

  private:
  Kokkos::View<double*> x1Shift{};
  Kokkos::View<double*> x2Shift{};
  Kokkos::View<double*> x3Shift{};
};

}  // namespace bsl6d

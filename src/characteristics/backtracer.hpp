#include <functional>

#include <gridDistribution.hpp>

namespace bsl6d {

class AnalyticalSolution {
  public:
  using InitialConditionFunctor =
      std::function<double(double, double, double, double, double, double)>;
  using BacktracingFunctor = std::function<
      std::array<double,
                 6>(double, double, double, double, double, double, double)>;
  AnalyticalSolution() = delete;
  AnalyticalSolution(
      GridDistribution<3, 3> const &gridDist,
      AnalyticalSolution::InitialConditionFunctor const &initialCondition,
      AnalyticalSolution::BacktracingFunctor const &backtracer);
  double operator()(size_t ix1,
                    size_t ix2,
                    size_t ix3,
                    size_t iv1,
                    size_t iv2,
                    size_t iv3,
                    double t) const;

  private:
  GridDistribution<3, 3> m_gridDist{};
  Grid<3, 3> m_grid{m_gridDist.mpi_local_grid()};
  InitialConditionFunctor m_initialCondition{};
  BacktracingFunctor m_backtracer{};
};

class FullKineticVlasovBacktracer {
  public:
  FullKineticVlasovBacktracer() = delete;
  FullKineticVlasovBacktracer(double wc, double Ex, bool rotatingVelocityGrid);
  std::array<double, 6> operator()(double x1,
                                   double x2,
                                   double x3,
                                   double v1,
                                   double v2,
                                   double v3,
                                   double t) const;

  private:
  double m_wc{1.0};  // gyroFrequency
  double m_Ex{0.0};  // electric field
  bool m_rotatingVelocityGrid{};
};

class FreeStreamingBacktracer {
  public:
  FreeStreamingBacktracer() = default;
  std::array<double, 6> operator()(double x1,
                                   double x2,
                                   double x3,
                                   double v1,
                                   double v2,
                                   double v3,
                                   double t) const;
};

}  // namespace bsl6d
/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#include <characteristicHelpers.hpp>

#include <freeStreaming.hpp>

namespace bsl6d {

FreeStreaming::FreeStreaming(DistributionFunction const f6d)
    : Characteristic(CharacteristicId::FreeStreaming, f6d)
    , x1Shift{"x1Shift", f6d.mpi_local_grid()(AxisType::v1).N()}
    , x2Shift{"x2Shift", f6d.mpi_local_grid()(AxisType::v2).N()}
    , x3Shift{"x3Shift", f6d.mpi_local_grid()(AxisType::v3).N()} {}

double FreeStreaming::update(AxisType axis) const {
  Kokkos::View<double*> shift{};
  Grid<3, 3> grid{_f6d.mpi_local_grid()};
  double dt{_dt};
  switch (axis) {
    case AxisType::x1:
      shift = x1Shift;
      Kokkos::parallel_for(
          "UpdateFreeStreamingx1", x1Shift.size(), KOKKOS_LAMBDA(size_t i3) {
            shift(i3) =
                -dt * grid(AxisType::v1)(i3) / grid(AxisType::x1).delta();
          });
      return Impl::abs_max(shift);
    case AxisType::x2:
      shift = x2Shift;
      Kokkos::parallel_for(
          "UpdateFreeStreamingx2", x2Shift.size(), KOKKOS_LAMBDA(size_t i4) {
            shift(i4) =
                -dt * grid(AxisType::v2)(i4) / grid(AxisType::x2).delta();
          });
      return Impl::abs_max(shift);
    case AxisType::x3:
      shift = x3Shift;
      Kokkos::parallel_for(
          "UpdateFreeStreamingx3", x3Shift.size(), KOKKOS_LAMBDA(size_t i5) {
            shift(i5) =
                -dt * grid(AxisType::v3)(i5) / grid(AxisType::x3).delta();
          });
      return Impl::abs_max(shift);
    case AxisType::v1: return 0;
    case AxisType::v2: return 0;
    case AxisType::v3: return 0;
  }
  // Can not be reached just to remove compiler warning
  return 0;
}

} /* namespace bsl6d */

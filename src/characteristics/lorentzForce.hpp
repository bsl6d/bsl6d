/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#pragma once

#include <Kokkos_Core.hpp>
#include <grid.hpp>
#include <vectorField.hpp>

#include <baseCharacteristic.hpp>

namespace bsl6d {

class LorentzForce : public Characteristic {
  public:
  LorentzForce() = default;
  LorentzForce(DistributionFunction f6d);

  double update(AxisType axis) const override;

  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x1Shift(iv1);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x2Shift(iv2);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x3Shift(iv3);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v1Shift(ix1, ix2, ix3, iv2);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v2Shift(ix1, ix2, ix3, iv1);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v3Shift(ix1, ix2, ix3);
  }

  private:
  Kokkos::View<double*> m_x1Shift{};
  Kokkos::View<double*> m_x2Shift{};
  Kokkos::View<double*> m_x3Shift{};
  Kokkos::View<double****> m_v1Shift{};
  Kokkos::View<double****> m_v2Shift{};
  Kokkos::View<double***> m_v3Shift{};
};

}  // namespace bsl6d

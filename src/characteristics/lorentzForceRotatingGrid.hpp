#pragma once

#include <Kokkos_Core.hpp>

#include <grid.hpp>
#include <vectorField.hpp>

#include <baseCharacteristic.hpp>

namespace bsl6d {
/*
 \brief Implement Vlasov-Poisson Model with constant B field in
        x3 dimension using a logical grid for the distribution function
        rotating with gyrofrequency. The grid follows the gyromotion frequency
        in velocity space (v1,v2) dimensions.
 \literature Kormann,Reuter,Rampp; High Performance Computing Applications;
 2019; Sec.2.3
*/
class LorentzForceRotatingGrid : public Characteristic {
  public:
  LorentzForceRotatingGrid() = default;
  LorentzForceRotatingGrid(DistributionFunction const &f6d);

  double update(AxisType axis) const override;
  KOKKOS_INLINE_FUNCTION
  double shift_x1(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x1Shift(iv1, iv2);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x2(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x2Shift(iv1, iv2);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_x3(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_x3Shift(iv3);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v1(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v1Shift(ix1, ix2, ix3);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v2(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v2Shift(ix1, ix2, ix3);
  }
  KOKKOS_INLINE_FUNCTION
  double shift_v3(size_t const ix1,
                  size_t const ix2,
                  size_t const ix3,
                  size_t const iv1,
                  size_t const iv2,
                  size_t const iv3) const {
    return m_v3Shift(ix1, ix2, ix3);
  }

  private:
  Kokkos::View<double **> m_x1Shift{};
  Kokkos::View<double **> m_x2Shift{};
  Kokkos::View<double *> m_x3Shift{};
  Kokkos::View<double ***> m_v1Shift{};
  Kokkos::View<double ***> m_v2Shift{};
  Kokkos::View<double ***> m_v3Shift{};
};

}  // namespace bsl6d

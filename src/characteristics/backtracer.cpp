#include <characteristicHelpers.hpp>
#include <linearAlgebra.hpp>

#include "backtracer.hpp"

namespace bsl6d {
AnalyticalSolution::AnalyticalSolution(
    GridDistribution<3, 3> const &gridDist,
    AnalyticalSolution::InitialConditionFunctor const &initialCondition,
    AnalyticalSolution::BacktracingFunctor const &backtracer)
    : m_gridDist{gridDist}
    , m_initialCondition{initialCondition}
    , m_backtracer{backtracer} {};
double AnalyticalSolution::operator()(size_t ix1,
                                      size_t ix2,
                                      size_t ix3,
                                      size_t iv1,
                                      size_t iv2,
                                      size_t iv3,
                                      double t) const {
  std::array<double, 6> characteristicRoots{};
  characteristicRoots =
      m_backtracer(m_grid(AxisType::x1)(ix1), m_grid(AxisType::x2)(ix2),
                   m_grid(AxisType::x3)(ix3), m_grid(AxisType::v1)(iv1),
                   m_grid(AxisType::v2)(iv2), m_grid(AxisType::v3)(iv3), t);
  return m_initialCondition(
      characteristicRoots[AxisType::x1], characteristicRoots[AxisType::x2],
      characteristicRoots[AxisType::x3], characteristicRoots[AxisType::v1],
      characteristicRoots[AxisType::v2], characteristicRoots[AxisType::v3]);
};

FullKineticVlasovBacktracer::FullKineticVlasovBacktracer(
    double wc,
    double Ex,
    bool rotatingVelocityGrid)
    : m_wc{wc}
    , m_Ex{Ex}
    , m_rotatingVelocityGrid{rotatingVelocityGrid} {};

std::array<double, 6> FullKineticVlasovBacktracer::operator()(double x1,
                                                              double x2,
                                                              double x3,
                                                              double v1,
                                                              double v2,
                                                              double v3,
                                                              double t) const {
  Vector x{x1, x2, x3};
  Vector v{v1, v2, v3};

  if (m_rotatingVelocityGrid != 0.0) {
    v = Impl::RotationMatrix::R_x3_inv(m_wc * t) * v;
  }
  v(1) = v(1) + m_Ex / m_wc;
  x(0) = x(0) + (Impl::RotationMatrix::integrated_R_x3(t, -t, m_wc) * v)(0);
  x(1) = x(1) + (Impl::RotationMatrix::integrated_R_x3(t, -t, m_wc) * v)(1);
  x(1) = x(1) - m_Ex / m_wc * t;

  v    = Impl::RotationMatrix::R_x3_inv(-m_wc * t) * v;
  v(1) = v(1) - m_Ex / m_wc;

  return std::array<double, 6>{x(AxisType::x1),     x(AxisType::x2),
                               x(AxisType::x3),     v(AxisType::v1 - 3),
                               v(AxisType::v2 - 3), v(AxisType::v3 - 3)};
}

std::array<double, 6> FreeStreamingBacktracer::operator()(double x1,
                                                          double x2,
                                                          double x3,
                                                          double v1,
                                                          double v2,
                                                          double v3,
                                                          double t) const {
  std::array<double, 6> xv{};
  xv[AxisType::x1] = x1 - v1 * t;
  xv[AxisType::x2] = x2 - v2 * t;
  xv[AxisType::x3] = x3 - v3 * t;
  xv[AxisType::v1] = v1;
  xv[AxisType::v2] = v2;
  xv[AxisType::v3] = v3;

  return xv;
};

}  // namespace bsl6d

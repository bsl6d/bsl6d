#include <Kokkos_Core.hpp>

#include <characteristicHelpers.hpp>

namespace bsl6d {
namespace Impl {
template <typename ViewType>
double abs_max(ViewType shifts) {
  double max{0};
  Kokkos::parallel_reduce(
      "cfl-condition", shifts.size(),
      KOKKOS_LAMBDA(size_t i0, double& lmax) {
        double* shifts_ptr = shifts.data();
        lmax               = std::max(std::abs(shifts_ptr[i0]), lmax);
      },
      Kokkos::Max<double>(max));

  return max;
}

template double abs_max<Kokkos::View<double*>>(Kokkos::View<double*>);
template double abs_max<Kokkos::View<double**>>(Kokkos::View<double**>);
template double abs_max<Kokkos::View<double***>>(Kokkos::View<double***>);
template double abs_max<Kokkos::View<double****>>(Kokkos::View<double****>);
template double abs_max<Kokkos::View<double*****>>(Kokkos::View<double*****>);
template double abs_max<Kokkos::View<double******>>(Kokkos::View<double******>);
}  // namespace Impl
}  // namespace bsl6d

/*****************************************************************/
/*

\brief Collectiv header of all characteristics

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#include <map>

#include <characteristics.hpp>

namespace bsl6d {

void CharacteristicBuilder::set_shift(double const shift) {
  Constant0DShift characteristic{shift};
  characteristicVariant = std::make_shared<Constant0DShift>(characteristic);
}

void CharacteristicBuilder::set_Nxv(std::array<size_t, 6> _Nxv,
                                    size_t dimension) {
  switch (dimension) {
    case (5):
      characteristicVariant =
          std::make_shared<Constant5DShift>(Constant5DShift{_Nxv});
      break;
    // case (6):
    //  characteristicVariant =
    //  std::make_shared<Constant6DShift>(Constant6DShift{_Nxv}); break;
    default:
      std::runtime_error(
          "Dimension given to CharacteristicBuilder::set_Nxv not implemented");
      break;
  }
}

CharacteristicVariants CharacteristicBuilder::create_characteristic(
    CharacteristicId const characteristicId) const {
  switch (characteristicId) {
    case CharacteristicId::Constant0DShift:
      std::get<std::shared_ptr<Constant0DShift>>(characteristicVariant);
      break;
    case CharacteristicId::Constant5DShift:
      std::get<std::shared_ptr<Constant5DShift>>(characteristicVariant);
      break;
    // case CharacteristicId::Constant6DShift:
    //  std::get<std::shared_ptr<Constant6DShift>>(characteristicVariant);
    //  break;
    case CharacteristicId::FreeStreaming:
      std::get<std::shared_ptr<FreeStreaming>>(characteristicVariant);
      break;
    default: throw std::invalid_argument("Not implemented characteristic.");
  }

  return characteristicVariant;
}

} /* namespace bsl6d */

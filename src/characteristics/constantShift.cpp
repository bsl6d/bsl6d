/*****************************************************************/
/*

\brief Return a constant shift during all iterations

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   23.02.2021

*/
/*****************************************************************/

#include <random>

#include <characteristicHelpers.hpp>
#include <constantShift.hpp>

namespace bsl6d {

namespace Impl {
template <typename ViewType>
void fill_random(ViewType randView,
                 typename ViewType::HostMirror h_randView,
                 size_t const seed,
                 double const boundary) {
  double* shifts_ptr = h_randView.data();
  std::mt19937 generator{static_cast<unsigned int>(seed)};
  // mt19937.operator() is not const and cannot be used in a Lambda
  for (size_t i = 0; i < randView.size(); i++)
    shifts_ptr[i] = (double)(generator() - generator.max() / 2.) * boundary /
                    generator.max();

  Kokkos::deep_copy(randView, h_randView);
}
}  // namespace Impl

Constant0DShift::Constant0DShift(double dxv)
    : Characteristic(CharacteristicId::Constant0DShift, DistributionFunction{})
    , m_dxv{dxv}

{}

Constant5DShift::Constant5DShift(std::array<size_t, 6> const Nxv,
                                 size_t const seed,
                                 double const boundary)
    : Characteristic(CharacteristicId::Constant5DShift, DistributionFunction{})
    , m_shifts{}
    , m_h_shifts{} {
  size_t maxSize = 0;
  for (size_t i = 0; i < 6; i++) {
    size_t tmp = 1;
    for (size_t j = 0; j < 6; j++)
      if (i != j) tmp *= Nxv[j];
    maxSize = std::max(tmp, maxSize);
  }
  m_baseShift   = Kokkos::View<double*>{"Constant5DShift", maxSize};
  m_h_baseShift = Kokkos::create_mirror_view(m_baseShift);
  Impl::fill_random(m_baseShift, m_h_baseShift, seed, boundary);

  m_shifts[0]   = ShiftsViewType(m_baseShift.data(), Nxv[1], Nxv[2], Nxv[3],
                                 Nxv[4], Nxv[5]);
  m_h_shifts[0] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[1],
                                             Nxv[2], Nxv[3], Nxv[4], Nxv[5]);
  m_shifts[1]   = ShiftsViewType(m_baseShift.data(), Nxv[0], Nxv[2], Nxv[3],
                                 Nxv[4], Nxv[5]);
  m_h_shifts[1] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[0],
                                             Nxv[2], Nxv[3], Nxv[4], Nxv[5]);
  m_shifts[2]   = ShiftsViewType(m_baseShift.data(), Nxv[0], Nxv[1], Nxv[3],
                                 Nxv[4], Nxv[5]);
  m_h_shifts[2] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[0],
                                             Nxv[1], Nxv[3], Nxv[4], Nxv[5]);
  m_shifts[3]   = ShiftsViewType(m_baseShift.data(), Nxv[0], Nxv[1], Nxv[2],
                                 Nxv[4], Nxv[5]);
  m_h_shifts[3] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[0],
                                             Nxv[1], Nxv[2], Nxv[4], Nxv[5]);
  m_shifts[4]   = ShiftsViewType(m_baseShift.data(), Nxv[0], Nxv[1], Nxv[2],
                                 Nxv[3], Nxv[5]);
  m_h_shifts[4] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[0],
                                             Nxv[1], Nxv[2], Nxv[3], Nxv[5]);
  m_shifts[5]   = ShiftsViewType(m_baseShift.data(), Nxv[0], Nxv[1], Nxv[2],
                                 Nxv[3], Nxv[4]);
  m_h_shifts[5] = ShiftsViewType::HostMirror(m_h_baseShift.data(), Nxv[0],
                                             Nxv[1], Nxv[2], Nxv[3], Nxv[4]);
}

double Constant5DShift::update(AxisType axis) const {
  size_t axisInt{static_cast<size_t>(axis)};
  Kokkos::Array<size_t, 5> startIdx{0, 0, 0, 0, 0};
  Kokkos::Array<size_t, 5> stopIdx{};
  for (size_t i = 0; i < 5; i++) { stopIdx[i] = m_shifts[axisInt].extent(i); }
  double maxShift{};
  Kokkos::MDRangePolicy<Kokkos::Rank<5>> mdRangePolicy{startIdx, stopIdx};
  Kokkos::View<double*****> shifts{m_shifts[axisInt]};
  maxShift = Impl::abs_max(shifts);
  return maxShift;
}

Constant6DShift::Constant6DShift(std::array<size_t, 6> const _Nxv,
                                 size_t const seed,
                                 double const boundary)
    : Characteristic(CharacteristicId::Constant6DShift, DistributionFunction{})
    , m_shifts{"Shifts", _Nxv[0], _Nxv[1], _Nxv[2], _Nxv[3], _Nxv[4], _Nxv[5]}
    , m_h_shifts{Kokkos::create_mirror_view(m_shifts)}
    , m_maxShift{} {
  Impl::fill_random(m_shifts, m_h_shifts, seed, boundary);

  m_maxShift = Impl::abs_max(m_shifts);
}
} /* namespace bsl6d */

#pragma once

#include <Kokkos_Core.hpp>

#include <linearAlgebra.hpp>

namespace bsl6d {
namespace Impl {

template <typename ViewType>
double abs_max(ViewType shifts);

/*
 Rotates 3d-std::array in predefined plane using D^-1(angle)

 R(angle) = /  cos(angle)  -sin(angle) \
            \  sin(angle)   cos(angle) /
 R^-1(angle) = /  cos(angle)   sin(angle) \
               \ -sin(angle)   cos(angle) /
 R^-1(angle) = R(-angle)
*/
class RotationMatrix {
  public:
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x1(double angle) {
    std::array<std::array<double, 3>, 3> R{};
    R[0][0] = 1.0;
    R[1][1] = R2d(angle)[0][0];
    R[1][2] = R2d(angle)[0][1];
    R[2][1] = R2d(angle)[1][0];
    R[2][2] = R2d(angle)[1][1];
    return Matrix{R};
  }
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x2(double angle) {
    std::array<std::array<double, 3>, 3> R{};
    R[0][0] = R2d(angle)[0][0];
    R[0][2] = R2d(angle)[0][1];
    R[1][1] = 1.0;
    R[2][0] = R2d(angle)[1][0];
    R[2][2] = R2d(angle)[1][1];
    return Matrix{R};
  }
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x3(double angle) {
    std::array<std::array<double, 3>, 3> R{};
    R[0][0] = R2d(angle)[0][0];
    R[0][1] = R2d(angle)[0][1];
    R[1][0] = R2d(angle)[1][0];
    R[1][1] = R2d(angle)[1][1];
    R[2][2] = 1.0;
    return Matrix{R};
  }
  /*
   Integrated rotation matrix R_x3 around x3 axis:
   int_{t0}^{t0+h} R_x3(w_c*t) dt

                      /  cos(w_c*t)  -sin(w_c*t)  0   \
   = int_{t0}^{t0+h}  |  sin(w_c*t)   cos(w_c*t)  0   | dt
                      \       0             0     1   /

           /-sin(w_c*t0)+sin(w_c*(t0+h)) -cos(w_c*t0)+cos(w_c*(t0+h)) 0    \
   = 1/w_c | cos(w_c*t0)-cos(w_c*(t0+h)) -sin(w_c*t0)+sin(w_c*(t0+h)) 0    |
           \           0                              0             w_c*h  /
  */
  KOKKOS_INLINE_FUNCTION
  static Matrix integrated_R_x3(double t0, double dt, double scalingFactor) {
    std::array<std::array<double, 3>, 3> R{};
    R[0][0] =
        (-std::sin(scalingFactor * t0) + std::sin(scalingFactor * (t0 + dt))) /
        scalingFactor;
    R[0][1] =
        (-std::cos(scalingFactor * t0) + std::cos(scalingFactor * (t0 + dt))) /
        scalingFactor;
    R[1][0] =
        (std::cos(scalingFactor * t0) - std::cos(scalingFactor * (t0 + dt))) /
        scalingFactor;
    R[1][1] =
        (-std::sin(scalingFactor * t0) + std::sin(scalingFactor * (t0 + dt))) /
        scalingFactor;
    R[2][2] = dt;
    return Matrix{R};
  }
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x1_inv(double angle) { return R_x1(-angle); }
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x2_inv(double angle) { return R_x2(-angle); }
  KOKKOS_INLINE_FUNCTION
  static Matrix R_x3_inv(double angle) { return R_x3(-angle); }
  /*
   Integrated rotation matrix R_x3_inv around x3 axis:
   int_{t0}^{t0+h} R_x3_inv(w_c*t) dt

                      /  cos(w_c*t)  -sin(w_c*t)  0   \
   = int_{t0}^{t0+h}  |  sin(w_c*t)   cos(w_c*t)  0   | dt
                      \       0             0     1   /

           /-sin(w_c*t0)+sin(w_c*(t0+h))  cos(w_c*t0)-cos(w_c*(t0+h)) 0    \
   = 1/w_c |-cos(w_c*t0)+cos(w_c*(t0+h)) -sin(w_c*t0)+sin(w_c*(t0+h)) 0    |
           \           0                              0             w_c*h  /

  */
  KOKKOS_INLINE_FUNCTION
  static Matrix integrated_R_x3_inv(double t0,
                                    double dt,
                                    double scalingFactor) {
    Matrix R{integrated_R_x3(t0, dt, scalingFactor)};
    double tmp = R(0, 1);
    R(0, 1)    = R(1, 0);
    R(1, 0)    = tmp;
    return R;
  }

  private:
  KOKKOS_INLINE_FUNCTION
  static std::array<std::array<double, 2>, 2> R2d(double angle) {
    return std::array<std::array<double, 2>, 2>{
        std::array<double, 2>{std::cos(angle), -std::sin(angle)},
        std::array<double, 2>{std::sin(angle), std::cos(angle)}};
  }
};

}  // namespace Impl

}  // namespace bsl6d

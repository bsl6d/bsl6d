#include <linearAlgebra.hpp>
#include <vectorAnalysis.hpp>

#include "poisson.hpp"

namespace bsl6d {

MaxwellFields Poisson::solve_self_consistent_fields(
    DistributionFunction& f6d,
    SimulationTime& simulationTime,
    double dt) {
  Gradient grad{f6d.gridDistribution().create_xGridDistribution()};
  Laplace laplace{f6d.gridDistribution().create_xGridDistribution()};
  MaxwellFields maxwellFields{};

  ScalarField rho{f6d.calculate_charge_density("n")};
  maxwellFields.phi = laplace(bsl6d::Inverse{}, -1 * rho, "phi");
  maxwellFields.E   = grad(-1 * maxwellFields.phi, "E");

  return maxwellFields;
}

}  // namespace bsl6d

/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
       by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#include <random>

#include <linearAlgebra.hpp>
#include <vectorAnalysis.hpp>

#include <baseFieldSolver.hpp>

namespace bsl6d {

namespace Impl {

MaxwellFields calculate_background_fields(
    GridDistribution<3, 0> const &gridDistribution,
    SimulationTime const &simulationTime,
    FieldSolverConfig::ConfigBackgroundField const &config) {
  MaxwellFields maxwellFields{};
  maxwellFields.phi = bsl6d::ScalarField{gridDistribution, "phi"};
  if (config.type == "randomNoiseResponse" or
      config.type == "randomNoisePush") {
    /* RandomNumberGeneration */
    std::mt19937 generator{};
    generator.seed(
        maxwellFields.phi.gridDistribution().cart_topology().my_cart_rank());
    Kokkos::View<double ***> randomNumber{
        create_grid_compatible_view(maxwellFields.phi.grid(), "WhiteNoise")};
    Kokkos::View<double ***>::HostMirror h_randomNumber{
        Kokkos::create_mirror_view(randomNumber)};

    for (size_t i0 = 0; i0 < maxwellFields.phi.grid()(bsl6d::AxisType::x1).N();
         i0++)
      for (size_t i1 = 0;
           i1 < maxwellFields.phi.grid()(bsl6d::AxisType::x2).N(); i1++)
        for (size_t i2 = 0;
             i2 < maxwellFields.phi.grid()(bsl6d::AxisType::x3).N(); i2++) {
          h_randomNumber(i0, i1, i2) =
              (double)(generator() - (double)generator.max() / 2.) * 2. /
              generator.max();
        }

    Kokkos::deep_copy(randomNumber, h_randomNumber);

    double errorFunction{};
    if (config.type == "randomNoiseResponse") {
      errorFunction = std::erfc(-config.erSlope * simulationTime.current_T() +
                                config.timeOffset) *
                      config.alpha;
    } else if (config.type == "randomNoisePush") {
      errorFunction =
          (exp(-config.gaussianSD *
               ((simulationTime.current_T() - config.timeOffset) *
                (simulationTime.current_T() - config.timeOffset))) *
           std::sin(config.sincSD *
                    (simulationTime.current_T() - config.timeOffset)) /
           (config.sincSD * (simulationTime.current_T() - config.timeOffset))) *
          config.alpha;
    }

    Kokkos::parallel_for(
        "BackgroundFieldRandom", maxwellFields.phi.grid().mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
          maxwellFields.phi(ix1, ix2, ix3) =
              errorFunction * randomNumber(ix1, ix2, ix3);
        });
  } else if (config.type == "cos") {
    std::array<double, 3> k{config.k};
    double alpha{config.alpha};
    Kokkos::parallel_for(
        "BackgroundFieldSin", maxwellFields.phi.grid().mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
          maxwellFields.phi(ix1, ix2, ix3) =
              alpha *
              std::cos(k[0] * maxwellFields.phi.grid()(AxisType::x1)(ix1)) *
              std::cos(k[1] * maxwellFields.phi.grid()(AxisType::x2)(ix2)) *
              std::cos(k[2] * maxwellFields.phi.grid()(AxisType::x3)(ix3));
        });
  } else if (config.type == "const") {
    maxwellFields.E = VectorField{gridDistribution, "E"};
    double alpha{config.alpha};
    Kokkos::parallel_for(
        "BackgroundFieldSin", maxwellFields.phi.grid().mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
          maxwellFields.phi(ix1, ix2, ix3) =
              -alpha * maxwellFields.phi.mpi_local_grid()(AxisType::x1)(ix1);
          maxwellFields.E(AxisType::x1)(ix1, ix2, ix3) = alpha;
          maxwellFields.E(AxisType::x2)(ix1, ix2, ix3) = 0.0;
          maxwellFields.E(AxisType::x3)(ix1, ix2, ix3) = 0.0;
        });
  }

  if (config.type != "const") {
    Gradient grad{maxwellFields.phi.gridDistribution()};
    maxwellFields.E = grad(-1 * maxwellFields.phi, "E");
  }

  maxwellFields.Bx3 = config.Bx3;

  return maxwellFields;
}

void linear_combination_maxwellFields(MaxwellFields &selfConsistent,
                                      MaxwellFields const &backgroundFields) {
  Kokkos::parallel_for(
      "SumMaxwellFields", selfConsistent.phi.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        selfConsistent.phi(ix1, ix2, ix3) +=
            backgroundFields.phi(ix1, ix2, ix3);
        selfConsistent.E(AxisType::x1)(ix1, ix2, ix3) +=
            backgroundFields.E(AxisType::x1)(ix1, ix2, ix3);
        selfConsistent.E(AxisType::x2)(ix1, ix2, ix3) +=
            backgroundFields.E(AxisType::x2)(ix1, ix2, ix3);
        selfConsistent.E(AxisType::x3)(ix1, ix2, ix3) +=
            backgroundFields.E(AxisType::x3)(ix1, ix2, ix3);
      });
  selfConsistent.Bx3 += backgroundFields.Bx3;
}

}  // namespace Impl

FieldSolver::FieldSolver(FieldSolverConfig const &config)
    : m_configBackgroundField{config.configBgField} {};

MaxwellFields FieldSolver::operator()(DistributionFunction &f6d,
                                      SimulationTime &simulationTime,
                                      double dt) {
  m_backgroundFields = Impl::calculate_background_fields(
      f6d.gridDistribution().create_xGridDistribution(), simulationTime,
      m_configBackgroundField);

  MaxwellFields maxwellFields{
      solve_self_consistent_fields(f6d, simulationTime, dt)};

  MaxwellFields backgroundFields{m_backgroundFields};
  Impl::linear_combination_maxwellFields(maxwellFields, m_backgroundFields);
  return maxwellFields;
};

} /* namespace bsl6d */

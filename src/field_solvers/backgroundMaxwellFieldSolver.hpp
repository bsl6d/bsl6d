/*****************************************************************/
/*
\Background electric field solver implementation

\author Ali Sedighi
\department NMPP
\email  Ali.Sedighi@ipp.mpg.de
\date   15.03.2023

    */
/*****************************************************************/
#pragma once

#include <string>

#include <config.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <scalarField.hpp>
#include <vectorAnalysis.hpp>
#include <vectorField.hpp>

#include "baseFieldSolver.hpp"

namespace bsl6d {

/*
This class requires new tests! Old test have been deleted
since these tests did not provide a good validation and
where flacky as well as difficult to maintain.
*/
class BackgroundMaxwellFieldSolver : public FieldSolver {
  public:
  BackgroundMaxwellFieldSolver() = default;
  BackgroundMaxwellFieldSolver(bsl6d::FieldSolverConfig const &);

  protected:
  virtual MaxwellFields solve_self_consistent_fields(
      DistributionFunction &f6d,
      SimulationTime &simulationTime,
      double dt = 0.0) override;
};

}  // namespace bsl6d

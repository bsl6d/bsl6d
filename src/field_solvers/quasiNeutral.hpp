
#pragma once

#include <backgroundMaxwellFieldSolver.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <openPMDInterface.hpp>
#include <scalarField.hpp>
#include <vectorAnalysis.hpp>
#include <vectorField.hpp>

#include "baseFieldSolver.hpp"

namespace bsl6d {

// clang-format off
/**
 * @brief Solver for electrostatic fields assuming a quasineutral plasma and 
 *        adiabatic electrons. Phyiscal background and implementation details
 *        are found in @verbatim embed:rst:inline :ref:`chapter:NetZeroChargeTransportAlgorithm` @endverbatim
 * 
 * @details Two configurations are available:
 *          - Adiabatic electrons and quasineutrality: 
 *            \f$\phi(\textbf{x}) = n(\textbf{x})\f$
 *            @verbatim embed:rst:inline :ref:`sec:AdiabaticElectronsAndQuasiNeutrality` @endverbatim
 *          - Adiabatic electrons, quasineutrality and a constant flux surface average:
 *            \f[
 *            \partial_t \langle n(\textbf{x}) \rangle &= 0\\
 *            \phi(\textbf{x}) - \langle \phi(\textbf{x}) \rangle_{F} &= n(\textbf{x}) - \langle n(\textbf{x}) \rangle
 *            \f]
 *            The first equation can be used to create an equation for
 *            \f$\langle \phi(\textbf{x}) \rangle_{F}\f$ which is implemented by
 *            @verbatim embed:rst:inline :ref:`par:determineFluxAvgPhi` @endverbatim
 * 
 */
// clang-format on
class QuasiNeutral : public FieldSolver {
  public:
  QuasiNeutral() = default;
  QuasiNeutral(FieldSolverConfig const &);

  protected:
  virtual MaxwellFields solve_self_consistent_fields(
      DistributionFunction &f6d,
      SimulationTime &simulationTime,
      double dt = 0.0) override;

  private:
  FieldSolverConfig::ConfigQuasiNeutral m_config{};

  bool m_seriesOpened{false};
  openPMD::Series m_series{};
};

}  // namespace bsl6d

#include <baseFieldSolver.hpp>

namespace bsl6d {

class Poisson : public FieldSolver {
  public:
  Poisson() = default;

  Poisson(FieldSolverConfig const &config) : FieldSolver{config} {};

  protected:
  virtual MaxwellFields solve_self_consistent_fields(
      DistributionFunction &f6d,
      SimulationTime &simulationTime,
      double dt = 0.0) override;
};

}  // namespace bsl6d

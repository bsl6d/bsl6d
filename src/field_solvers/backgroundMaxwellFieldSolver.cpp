/*****************************************************************/
/*
\Background electric field solver implementation

\author Ali Sedighi
\department NMPP
\email  Ali.Sedighi@ipp.mpg.de
\date   15.03.2023

    */
/*****************************************************************/

#include <cmath>

#include <time.hpp>

#include "backgroundMaxwellFieldSolver.hpp"

namespace bsl6d {

BackgroundMaxwellFieldSolver::BackgroundMaxwellFieldSolver(
    const FieldSolverConfig &config)
    : FieldSolver{config} {}

MaxwellFields BackgroundMaxwellFieldSolver::solve_self_consistent_fields(
    DistributionFunction &f6d,
    SimulationTime &simulationTime,
    double dt) {
  GridDistribution<3, 0> gridDistribution{
      f6d.gridDistribution().create_xGridDistribution()};
  MaxwellFields fields{ScalarField{gridDistribution, "phi"},
                       VectorField{gridDistribution, "E"}, 0.0};
  return fields;
};

}  // namespace bsl6d

#include <cmath>
#include <memory>
#include <type_traits>

#include <gtest/gtest.h>

#include <advector.hpp>
#include <characteristicHelpers.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <integrator.hpp>
#include <mpiTopology.hpp>

#include "../quasiNeutral.hpp"

class FieldSolverQuasiNeutral : public ::testing::Test {
  public:
  FieldSolverQuasiNeutral() {
    auto [m_simTime, m_f6d] = bsl6d::initialize(create_config_file());

    simulationTime = m_simTime;
    f6d            = m_f6d;

    integrate = bsl6d::Integrator{bsl6d::config_file(), f6d};
  }
  static nlohmann::json create_config_file() {
    nlohmann::json configFile =
        R"({
      "distributionFunction": {
        "q": 1.0,
        "m": 1.0
      },
      "gridDistribution" : {
        "grid": {
          "axes": ["x1","v1","v2"],
          "NxvPoints": [64, 33, 33],
          "position": ["L", "N", "N"]
        },
        "parallel": {
          "Nprocs": [1, 1, 1]
        }
      },
      "fieldSolver": {
        "quasiNeutral": {
          "constFluxSurfaceAverage": true
        },
        "backgroundField": {
          "Bx3": 1.0
        }
      },
      "operators": {
        "interpolator": {
          "stencilWidth": [8,8,8,8,8,8]
        }
      },
      "initializationDistributionFunction":{
        "initFunction": "zero"
      },
      "integrator": [
            {"token": "advectV", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
            {"token": "advectVAdj", "t0Offset": 0.5, "dtRatio": 0.5}
          ]
    })"_json;
    configFile["simulationTime"]["dt"]      = 0.01;
    configFile["simulationTime"]["T"]       = 4.0 * M_PI;
    configFile["simulationTime"]["restart"] = false;
    std::vector<double> xvMax{8.0, 6.0, 6.0};
    std::vector<double> xvMin{0.0, -xvMax[1], -xvMax[2]};
    configFile["gridDistribution"]["grid"]["xvMax"]          = xvMax;
    configFile["gridDistribution"]["grid"]["xvMin"]          = xvMin;
    configFile["gridDistribution"]["grid"]["rotateGyroFreq"] = true;
    return configFile;
  }

  bsl6d::DistributionFunction f6d{};
  bsl6d::SimulationTime simulationTime{};
  bsl6d::Integrator integrate{};

  std::map<bsl6d::AxisType, bsl6d::Advector> advect{};
  std::shared_ptr<bsl6d::LorentzForceRotatingGrid> characteristicPtr{};
};

double LInf_error(Kokkos::View<double *> const &a,
                  Kokkos::View<double *> const &b) {
  double err{};
  Kokkos::parallel_reduce(
      "LInf Norm", a.size(),
      KOKKOS_LAMBDA(size_t i, double &lerr) {
        lerr = std::max(lerr, std::abs(a(i) - b(i)));
      },
      Kokkos::Max<double>{err});

  return err;
}

double LInf_norm(Kokkos::View<double *> const &a) {
  double norm{};
  Kokkos::parallel_reduce(
      "LInf Norm", a.size(),
      KOKKOS_LAMBDA(size_t i, double &lnorm) {
        lnorm = std::max(lnorm, std::abs(a(i)));
      },
      Kokkos::Max<double>{norm});

  return norm;
}

void fill_particle_density_sin_perturbation(bsl6d::DistributionFunction &f6d,
                                            double const alpha) {
  double k0x1{};
  k0x1 = 2.0 * M_PI /
         f6d.gridDistribution().mpi_global_grid()(bsl6d::AxisType::x1).L();
  auto analytical = KOKKOS_LAMBDA(double x1, double x2, double x3, double v1,
                                  double v2, double v3)
                        ->double {
    double v0{0.0};
    double T{1.0};
    return (1.0 + alpha * std::sin(k0x1 * x1)) *
           (bsl6d::Impl::gauss(v1, v0, T) * bsl6d::Impl::gauss(v2, v0, T));
  };
  bsl6d::Impl::fill(f6d, analytical);
}

TEST_F(FieldSolverQuasiNeutral, ConstantFluxSurfaceAverages) {
  bsl6d::QuasiNeutral solver{bsl6d::FieldSolverConfig{bsl6d::config_file()}};

  fill_particle_density_sin_perturbation(f6d, 0.01);
  double tolerance{7.0e-9};
  Kokkos::deep_copy(
      f6d.electron_flux_surface_average(),
      bsl6d::flux_surface_average(f6d.calculate_velocity_moments().n));

  while (simulationTime.continue_advection()) {
    bsl6d::MaxwellFields maxwellFields =
        solver(f6d, simulationTime, simulationTime.dt() / 2);
    integrate(f6d, maxwellFields, simulationTime);
    simulationTime.advance_current_T_by_dt();
    // A small current is introduced at the start which leads to a
    // density deviation. This part is skipped in a test.
    // See diagrams in docs on this solver for details.
    if (simulationTime.current_step() % 10 == 0 and
        simulationTime.current_step() > 300) {
      EXPECT_LE(LInf_error(f6d.electron_flux_surface_average(),
                           bsl6d::flux_surface_average(
                               f6d.calculate_charge_density("rho"))),
                tolerance)
          << simulationTime.current_step();

      bsl6d::VectorField J{};
      double wc{simulationTime.gyro_frequency()};
      bsl6d::Matrix rotMat{bsl6d::Impl::RotationMatrix::R_x3_inv(
          simulationTime.current_T() * wc)};
      J              = rotMat * f6d.calculate_velocity_moments().particleFlux;
      auto JxFluxAvg = bsl6d::flux_surface_average(J(bsl6d::AxisType::x1));
      // The algorithm in the current stage does not guarantees zero flux
      // average divergence.
      EXPECT_LE(LInf_norm(JxFluxAvg), tolerance)
          << simulationTime.current_step();
    }
  }
}

TEST_F(FieldSolverQuasiNeutral, RemoveFluxAveragePerturbation) {
  bsl6d::QuasiNeutral solver{bsl6d::FieldSolverConfig{bsl6d::config_file()}};

  fill_particle_density_sin_perturbation(f6d, 1.0e-3);
  double toleranceDensity{3.0e-6};
  double toleranceFlux{5.0e-6};
  Kokkos::deep_copy(f6d.electron_flux_surface_average(), 1.0);

  std::vector<double> JxFluxAvgLInfNorm{0};
  while (simulationTime.continue_advection()) {
    bsl6d::MaxwellFields maxwellFields =
        solver(f6d, simulationTime, simulationTime.dt() / 2);
    integrate(f6d, maxwellFields, simulationTime);
    simulationTime.advance_current_T_by_dt();
    // Provide a certain amount of time to adjust the density to
    // an equilibrium density. Further tests for this precision
    // are necessary.
    if (simulationTime.current_step() % 10 == 0 and
        simulationTime.current_step() > 300) {
      EXPECT_LE(LInf_error(f6d.electron_flux_surface_average(),
                           bsl6d::flux_surface_average(
                               f6d.calculate_charge_density("rho"))),
                toleranceDensity)
          << simulationTime.current_step();

      bsl6d::VectorField J{};
      double wc{simulationTime.gyro_frequency()};
      bsl6d::Matrix rotMat{bsl6d::Impl::RotationMatrix::R_x3_inv(
          simulationTime.current_T() * wc)};
      J              = rotMat * f6d.calculate_velocity_moments().particleFlux;
      auto JxFluxAvg = bsl6d::flux_surface_average(J(bsl6d::AxisType::x1));
      // The algorithm in the current stage does not guarantees zero flux
      // average divergence.
      JxFluxAvgLInfNorm.push_back(LInf_norm(JxFluxAvg));
      // The algorithm in the current stage does not guarantees zero flux
      // average divergence. So I test that the flux is below a given tolerance
      // if it is not decreasing. It should peak around the 300th timestep which
      // is approximately have a gyro period.
      size_t size{JxFluxAvgLInfNorm.size()};
      if (JxFluxAvgLInfNorm[size - 2] < JxFluxAvgLInfNorm[size - 1] and
          size > 2) {
        EXPECT_LE(LInf_norm(JxFluxAvg), toleranceFlux)
            << simulationTime.current_step() << "\n"
            << "Current flux:" << JxFluxAvgLInfNorm[size - 1] << "\n"
            << "Previous flux" << JxFluxAvgLInfNorm[size - 2];
      }
    }
  }
}

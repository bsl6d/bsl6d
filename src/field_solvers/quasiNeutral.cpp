#include <characteristicHelpers.hpp>
#include <characteristics.hpp>
#include <fdDerivative.hpp>
#include <io_init.hpp>
#include <openPMDInterface.hpp>

#include "quasiNeutral.hpp"

namespace bsl6d {

namespace Impl {
Kokkos::View<double*> integrate_phi_flux_surface_average(
    Kokkos::View<double*> const& fluxAverageComponents,
    GridDistribution<3, 0> const& gridDistribution) {
  ScalarField rhs{gridDistribution, "rhs"};
  Kokkos::deep_copy(rhs.device(), 0.0);

  Kokkos::parallel_for(
      "SumRightHandSide", fluxAverageComponents.extent(0),
      KOKKOS_LAMBDA(size_t ix1) {
        rhs(ix1, 0, 0) += fluxAverageComponents(ix1);
      });

  bsl6d::Derivative x1Derivative{bsl6d::AxisType::x1,
                                 gridDistribution.create_xGridDistribution()};
  ScalarField result = x1Derivative(Inverse{}, rhs, "YZPhiFluxAvg");
  auto tmp = Kokkos::subview(result.device().view(), Kokkos::ALL(), 0, 0);
  Kokkos::View<double*> yzPhiFluxAvg{"YZPhiFluxAvg",
                                     fluxAverageComponents.extent(0)};
  Kokkos::deep_copy(yzPhiFluxAvg, tmp);
  return yzPhiFluxAvg;
}

ScalarField energy_flux_x(DistributionFunction const& f6d,
                          Matrix const& rotation) {
  ScalarField s{f6d.gridDistribution().create_xGridDistribution(),
                "int v_x*v_x*v_x f(x,v) d^3v"};
  Kokkos::parallel_for(
      "Third order velocity moment tensor", s.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        for (size_t iv1 = 0; iv1 < f6d.grid()(AxisType::v1).N(); iv1++)
          for (size_t iv2 = 0; iv2 < f6d.grid()(AxisType::v2).N(); iv2++)
            for (size_t iv3 = 0; iv3 < f6d.grid()(AxisType::v3).N(); iv3++) {
              Vector v{f6d.grid()(AxisType::v1)(iv1),
                       f6d.grid()(AxisType::v2)(iv2),
                       f6d.grid()(AxisType::v3)(iv3)};
              auto rotV = rotation * v;
              s(ix1, ix2, ix3) += rotV(0) * rotV(0) * rotV(0) *
                                  f6d(ix1, ix2, ix3, iv1, iv2, iv3);
            }
      });
  return s;
}

// Using T=1 for flux surface average condition
Kokkos::View<double*> calculate_flux_surface_avg_components_fixed_grid(
    DistributionFunction& f6d,
    ScalarField& nFreeFluxAvg,
    SimulationTime& simulationTime,
    openPMD::Series series,
    MaxwellFields backgroundFields) {
  Kokkos::View<double*> ne{f6d.electron_flux_surface_average()};
  ScalarField n{f6d.calculate_velocity_moments().n};
  VectorField J{f6d.calculate_velocity_moments().particleFlux};
  MatrixField S{f6d.calculate_velocity_moments().secondMoment};
  Gradient grad{n.gridDistribution()};
  Divergence div{n.gridDistribution()};
  Derivative dx{AxisType::x1, n.gridDistribution()};

  double mass{f6d.m()}, charge{f6d.q()};
  // double wc{simulationTime.gyro_frequency()};
  double dt{simulationTime.dt()};
  Matrix Id{};
  // Formatting a matrix looks better without clang-format.
  // clang-format off
  Id = Matrix{
    std::array<double, 3>{ 1.0 , 0.0 , 0.0 },
    std::array<double, 3>{ 0.0 , 1.0 , 0.0 },
    std::array<double, 3>{ 0.0 , 0.0 , 1.0 }
    };
  // clang-format on

  auto term1 = flux_surface_average(dx(J(AxisType::x1), "d/dx(A^-1*J)_x"));
  auto term2 = flux_surface_average(
      dx((grad(charge / mass * nFreeFluxAvg, ""))(AxisType::x1)*n,
         "d/dx{[A^-1*grad(n-<n_e>_F)]_x*n}"));
  auto term3 = flux_surface_average(
      dx(dx(S(AxisType::x1, AxisType::x1), ""), "d/dx d/dx A^-1*S*(A^{-1})^T"));
  auto divJ = div(J, "");
  auto term4 =
      flux_surface_average(div(divJ * (charge / mass * grad(nFreeFluxAvg, "")),
                               "div((div(J) q/m*grad(n - <n_e>_F)))"));
  auto term5 =
      flux_surface_average(dx(dx(dx(Impl::energy_flux_x(f6d, Id), ""), ""),
                              "d/dx d/dx d/dx [int v_x^3 * f d^3v]"));

  Kokkos::View<double*> nAvg{flux_surface_average(n)};
  Kokkos::View<double*> divJAvg{flux_surface_average(divJ)};

  Kokkos::View<double*> fluxSurfAvgComponents{
      "dxFluxSurfAvgComponents", f6d.mpi_local_grid()(AxisType::x1).N()};
  Kokkos::parallel_for(
      "CalculateFluxSurfaceAverageComponents", fluxSurfAvgComponents.extent(0),
      KOKKOS_LAMBDA(size_t ix1) {
        fluxSurfAvgComponents(ix1) =
            ne(ix1) - nAvg(ix1) + dt * term1(ix1) - dt * dt / 2.0 * term2(ix1) -
            dt * dt / 2.0 * term3(ix1) + dt * dt * dt / 2.0 * term4(ix1) +
            dt * dt * dt / 6.0 * term5(ix1);
      });

  Kokkos::View<double*> dxPhiNFluxAvg{Impl::integrate_phi_flux_surface_average(
      fluxSurfAvgComponents, n.gridDistribution())};
  Kokkos::View<double*> tmp{"fluxAvgComponents",
                            n.mpi_local_grid()(AxisType::x1).N()};
  Kokkos::parallel_for(
      "FluxSurfAvgComponents", dxPhiNFluxAvg.extent(0),
      KOKKOS_LAMBDA(size_t ix1) {
        tmp(ix1) = dxPhiNFluxAvg(ix1) /
                   (dt * dt / 2.0 * charge / mass * nAvg(ix1) -
                    dt * dt * dt / 2.0 * charge / mass * divJAvg(ix1));
      });

  return tmp;
};

Kokkos::View<double*> calculate_flux_surface_avg_components_rotating_grid(
    DistributionFunction& f6d,
    ScalarField& nFreeFluxAvg,
    SimulationTime& simulationTime,
    openPMD::Series series,
    MaxwellFields backgroundFields) {
  Kokkos::View<double*> ne{f6d.electron_flux_surface_average()};
  ScalarField n{f6d.calculate_velocity_moments().n};
  VectorField J{f6d.calculate_velocity_moments().particleFlux};
  MatrixField S{f6d.calculate_velocity_moments().secondMoment};
  Gradient grad{n.gridDistribution()};
  Divergence div{n.gridDistribution()};
  Derivative dx{AxisType::x1, n.gridDistribution()};

  double wc{simulationTime.gyro_frequency()};
  auto Bh05{Impl::RotationMatrix::integrated_R_x3(
      simulationTime.current_T() + simulationTime.dt() / 2.0,
      -simulationTime.dt() / 2.0, wc)};
  auto BT{Impl::RotationMatrix::integrated_R_x3_inv(
      simulationTime.current_T() + simulationTime.dt(), -simulationTime.dt(),
      wc)};
  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) { Bh05(i, j) *= f6d.q() / f6d.m(); }
  }

  Matrix Bh{};
  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) { Bh(i, j) = BT(j, i); }
  }

  auto nAvg = flux_surface_average(n);
  auto term1 =
      flux_surface_average(dx((BT * J)(AxisType::x1), "d/dx (B^T(h)*J)_x"));
  auto term2 = flux_surface_average(
      dx((((BT * Bh05) * (grad(nFreeFluxAvg, "")))(AxisType::x1)) * n,
         "d/dx [B^T(h)B(h/2)*(grad (n-<n>_F))_x*n]"));
  auto term3 = flux_surface_average(
      dx(dx((BT * (S * Bh))(AxisType::x1, AxisType::x1), ""),
         "d/dx d/dx [B^T(h) * S * B(h)]"));
  auto divJ    = div(BT * J, "");
  auto divJAvg = flux_surface_average(divJ);
  auto term4   = flux_surface_average(
      dx((((BT * Bh05) * (grad(nFreeFluxAvg, "")))(AxisType::x1)) * divJ,
           "d/dx [B^T(h)B(h/2)*(-grad(n-<n>_F))_x * "
             "div(B^T(h) J)]"));
  auto term5 =
      flux_surface_average(dx(dx(dx(energy_flux_x(f6d, BT), ""), ""),
                              "d/dx d/dx d/dx [int (B^T(h)v)_x^3 *f d^3v]"));

  Kokkos::View<double*> fluxSurfAvgComponents{
      "dxFluxSurfAvgComponents", f6d.mpi_local_grid()(AxisType::x1).N()};
  Kokkos::parallel_for(
      "CalculateFluxSurfaceAverageComponents", fluxSurfAvgComponents.extent(0),
      KOKKOS_LAMBDA(size_t ix1) {
        fluxSurfAvgComponents(ix1) = ne(ix1) - nAvg(ix1) - (term1(ix1)) -
                                     term2(ix1) - 1.0 / 2.0 * (term3(ix1)) -
                                     term4(ix1) - 1.0 / 6.0 * (term5(ix1));
      });

  Kokkos::View<double*> dxPhiNFluxAvg{Impl::integrate_phi_flux_surface_average(
      fluxSurfAvgComponents, n.gridDistribution())};
  Kokkos::View<double*> tmp{"fluxAvgComponents",
                            n.mpi_local_grid()(AxisType::x1).N()};
  double dtdt{(BT * Bh05)(0, 0) + (BT * Bh05)(0, 1)};
  Kokkos::parallel_for(
      "FluxSurfAvgComponents", dxPhiNFluxAvg.extent(0),
      KOKKOS_LAMBDA(size_t ix1) {
        tmp(ix1) = dxPhiNFluxAvg(ix1) / (nAvg(ix1) + divJAvg(ix1)) / dtdt;
      });

  return tmp;
};

ScalarField calculate_phi(ScalarField const& phiTilde,
                          Kokkos::View<double*> phiFluxAvg) {
  ScalarField phi{phiTilde.gridDistribution(), "phi"};
  Kokkos::parallel_for(
      "CalculatePhi", phi.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        phi(ix1, ix2, ix3) = phiTilde(ix1, ix2, ix3) + phiFluxAvg(ix1);
      });

  return phi;
}

ScalarField substract_flux_surface_average(ScalarField const& rho,
                                           Kokkos::View<double*> fsaverage) {
  ScalarField phi{rho.gridDistribution(), "phi"};
  Kokkos::parallel_for(
      "Substract FS average", rho.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        phi(ix1, ix2, ix3) = rho(ix1, ix2, ix3) - fsaverage(ix1);
      });
  return phi;
}

}  // namespace Impl

QuasiNeutral::QuasiNeutral(FieldSolverConfig const& config)
    : FieldSolver{config}
    , m_config{config.configQN} {}

MaxwellFields QuasiNeutral::solve_self_consistent_fields(
    DistributionFunction& f6d,
    SimulationTime& simulationTime,
    double dtv) {
  MaxwellFields maxwellFields{};
  maxwellFields.phi =
      ScalarField{f6d.gridDistribution().create_xGridDistribution(), "phi"};

  if (m_config.constFluxSurfaceAverage) {
    if (cmd::is_verbose() and m_seriesOpened == false) {
      m_seriesOpened = true;
      m_series       = openPMD::Series{
          cmd::diagnostic_directory() + "%010T_QNNonAdiabaticFieldSolver.h5",
          openPMD::Access::CREATE,
          f6d.gridDistribution().cart_topology().comm(),
          R"({"hdf5": {"dataset": {"chunks": "none"}},
                                     "defer_iteration_parsing": true })"};
    }

    ScalarField n{f6d.calculate_velocity_moments().n};
    ScalarField nFreeFluxAvg{
        Impl::substract_flux_surface_average(n, flux_surface_average(n))};

    Kokkos::View<double*> fluxSurfAvgComponents{};
    if (f6d.mpi_local_grid().rotating_velocity_grid()) {
      fluxSurfAvgComponents =
          Impl::calculate_flux_surface_avg_components_rotating_grid(
              f6d, nFreeFluxAvg, simulationTime, m_series, m_backgroundFields);
    } else {
      std::cerr << "The zero particle flux algorithm on a fixed grid is not "
                   "stable for production runs.\n";
      std::cerr << "Adapt the repository to continue development on the "
                   "constant flux surface average on a fixed grid.\n";
      bsl6d::CHECK_MPI(MPI_Finalize());
      exit(1);
      // fluxSurfAvgComponents =
      //     Impl::calculate_flux_surface_avg_components_fixed_grid(
      //         f6d, nFreeFluxAvg, simulationTime, m_series,
      //         m_backgroundFields);
    }

    Kokkos::View<double*> phiFluxAvg{Impl::integrate_phi_flux_surface_average(
        fluxSurfAvgComponents,
        f6d.gridDistribution().create_xGridDistribution())};
    maxwellFields.phi = Impl::calculate_phi(nFreeFluxAvg, phiFluxAvg);

    if (cmd::is_verbose()) {
      openPMD::Iteration iter{
          m_series.iterations[simulationTime.current_step()]};
      iter.close();
    }
    // ToDo: Automate this comparison
    //  The Unit tests in nonAdiabaticElectrons field solver are validated
    //  against the purely adiabatic electrons potential The difference
    //  between these to should be numerically notable (several orders of
    //  magnitued) to let the test pass.
  } else {
    RightHandSideConfig::Config configRHS{};
    if (m_config.substractFluxSurfaceAverage) {
      Impl::VelocityMoments velocityMoments = f6d.calculate_velocity_moments();
      // ToDo: This is a hidden change and needs restructring
      //       Usually it could be done in the initialization.
      //       But currently not in this specific case which is time dependend.
      if (simulationTime.current_T() < configRHS.rampTime) {
        Kokkos::deep_copy(f6d.electron_flux_surface_average(),
                          flux_surface_average(velocityMoments.n));
      }

      maxwellFields.phi = Impl::substract_flux_surface_average(
          velocityMoments.n, f6d.electron_flux_surface_average());

    } else {
      maxwellFields.phi = f6d.calculate_charge_density("phi");
    }
  }
  Gradient grad{maxwellFields.phi.gridDistribution()};
  maxwellFields.E = grad(-1 * maxwellFields.phi, "E");

  return maxwellFields;
}

}  // namespace bsl6d

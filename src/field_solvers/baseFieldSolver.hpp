/*****************************************************************/
/*

\brief Advector class, functor which advects distribution
       by time step

\author Mario Räth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   30.11.2021

*/
/*****************************************************************/

#pragma once

#include <utility>

#include <Kokkos_Core.hpp>

#include <distributionFunction.hpp>
#include <scalarField.hpp>
#include <time.hpp>
#include <vectorField.hpp>

namespace bsl6d {

struct MaxwellFields {
  ScalarField phi{};
  VectorField E{};
  double Bx3{};
};

class FieldSolver {
  public:
  FieldSolver() = default;
  virtual ~FieldSolver(){};

  // ToDo: dt parameter is only required depending on the integrator
  // Better soltuion than an separate parameter is required
  // ToDo: Use Background field solver always within FieldSolver by
  // adding the fields afterwards
  MaxwellFields operator()(DistributionFunction &f6d,
                           SimulationTime &simulationTime,
                           double dt = 0.0);

  protected:
  FieldSolver(FieldSolverConfig const &config);
  virtual MaxwellFields solve_self_consistent_fields(
      DistributionFunction &f6d,
      SimulationTime &simulationTime,
      double dt = 0.0) = 0;
  MaxwellFields m_backgroundFields{};

  private:
  FieldSolverConfig::ConfigBackgroundField m_configBackgroundField{};
};

} /* namespace bsl6d */

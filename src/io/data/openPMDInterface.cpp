
#include <string>

#include <distributionFunction.hpp>
#include <hdf5Interface.hpp>
#include <io_init.hpp>
#include <mpi_helper.hpp>
#include <time.hpp>

#include "./openPMDInterface.hpp"

namespace bsl6d {

namespace Impl {

openPMD::Iteration get_opmd_iteration(openPMD::Series &series,
                                      SimulationTime const &simulationTime) {
  namespace opmd = openPMD;
  if (series.iterations.contains(simulationTime.current_step())) {
    opmd::Iteration iter{series.iterations.at(simulationTime.current_step())};
    return iter;
  } else {
    opmd::Iteration iter{series.iterations[simulationTime.current_step()]};
    iter.setDt(simulationTime.dt());
    iter.setTime(simulationTime.current_T());
    // BSL6D does not contain any "real" unit system at the moment.
    // Therefore, we set the conversion to seconds to "0".
    iter.setTimeUnitSI(0.0);
    return iter;
  }
}

template <typename GridDistributionType>
openPMD::Mesh get_opmd_mesh(openPMD::Iteration &iter,
                            std::string const &label,
                            GridDistributionType const &gridDist,
                            SimulationTime const &simTime,
                            std::vector<AxisType> const &axes) {
  namespace opmd = openPMD;

  if (iter.meshes.contains(label)) {
    opmd::Mesh mesh{iter.meshes[label]};
    if (mesh.geometry() != opmd::Mesh::Geometry::cartesian) {
      throw std::runtime_error(
          "Only Cartesian Geomentry is supported at the moment but not given "
          "in mesh");
    }
    if (mesh.dataOrder() != opmd::Mesh::DataOrder::C) {
      throw std::runtime_error(
          "Only LayoutRight (C-Layout) is supported to read data with BSL6D "
          "but not given in mesh");
    }
    std::vector<AxisType> storedAxis{};
    for (std::string axis : mesh.axisLabels()) {
      if (std::find(axes.begin(), axes.end(), string_to_axis(axis)) ==
          axes.end()) {
        throw std::runtime_error("Mesh does not contain requested axis " +
                                 axis);
      }
    }
    for (size_t iAxis = 0; iAxis < mesh.axisLabels().size(); iAxis++) {
      AxisType axis = string_to_axis(mesh.axisLabels()[iAxis]);
      if (std::abs(mesh.gridSpacing<double>()[iAxis] -
                   gridDist.mpi_local_grid()(axis).delta()) > 1e-10) {
        throw std::runtime_error(
            "Grid spacing dx does not match with loaded grid!");
      }
      if (std::abs(mesh.gridGlobalOffset()[iAxis] -
                   gridDist.mpi_global_grid()(axis).min()) > 1e-10) {
        throw std::runtime_error(
            "Lower boundary L_min does not match with loaded grid!");
      }
      std::string axisType{mesh.getAttribute("AxisType_" + axis_to_string(axis))
                               .get<std::string>()};
      if (axisType !=
          axis_type_to_string(gridDist.mpi_global_grid()(axis).type())) {
        if (axisType == "C" and
            gridDist.mpi_global_grid()(axis).type() == Axis::Type::N) {
          std::cerr << "WARNING: We have replaced the option \"C\" for the "
                       "Axis type with\n";
          std::cerr << "         \"N\" for more accurate description!\n";
          std::cerr << "         REPLACE \"C\" with \"N\" to avoid problems in "
                       "the future!"
                    << std::endl;
        } else {
          throw std::runtime_error(
              "Axis Types Axis::Type does not match with loaded grid");
        }
      }
    }
    return mesh;
  } else {
    opmd::Mesh mesh{iter.meshes[label]};
    std::vector<std::string> axisLabels{};
    std::vector<double> gridSpacings{};
    std::vector<double> gridGlobalOffset{};
    for (size_t iAxis = 0; iAxis < axes.size(); iAxis++) {
      AxisType axis = axes[iAxis];
      axisLabels.push_back(axis_to_string(axis));
      gridSpacings.push_back(gridDist.mpi_local_grid()(axis).delta());
      gridGlobalOffset.push_back(gridDist.mpi_global_grid()(axis).min());
      mesh.setAttribute(
          "AxisType_" + axis_to_string(axis),
          axis_type_to_string(gridDist.mpi_global_grid()(axis).type()));
    }
    mesh.setAxisLabels(axisLabels);
    mesh.setGridSpacing(gridSpacings);
    mesh.setGridGlobalOffset(gridGlobalOffset);
    mesh.setGeometry(opmd::Mesh::Geometry::cartesian);
    mesh.setDataOrder(opmd::Mesh::DataOrder::C);
    mesh.setTimeOffset(0.0);
    return mesh;
  }
}
template openPMD::Mesh get_opmd_mesh<GridDistribution<3, 3>>(
    openPMD::Iteration &iter,
    std::string const &label,
    GridDistribution<3, 3> const &gridDist,
    SimulationTime const &simTime,
    std::vector<AxisType> const &axes);
template openPMD::Mesh get_opmd_mesh<GridDistribution<3, 0>>(
    openPMD::Iteration &iter,
    std::string const &label,
    GridDistribution<3, 0> const &gridDist,
    SimulationTime const &simTime,
    std::vector<AxisType> const &axes);

template <typename GridDistributionType>
void read_write_mesh_record_component(
    openPMD::Mesh mesh,
    GridDistributionType const &gridDistribution,
    double *data,
    OpenPMDMode const &mode,
    std::string const &label) {
  namespace opmd = openPMD;

  std::unique_ptr<opmd::MeshRecordComponent> meshRecordComponent_ptr{};
  if (mesh.contains(label)) {
    meshRecordComponent_ptr =
        std::make_unique<opmd::MeshRecordComponent>(mesh[label]);
    auto positionVector = meshRecordComponent_ptr->position<double>();
    auto position       = positionVector.begin();
    for (auto axis : mesh.axisLabels()) {
      if (mesh.getAttribute("AxisType_" + axis).get<std::string>() == "C") {
        std::cerr
            << "WARNING: The AxisType \"C\" has been replaced by \"N\"\n"
            << "         If the HDF5-File contains the attribute \"C\"\n"
            << "         replace it by \"N\" to avoid errors in the future.\n"
            << std::endl;
      } else {
        switch (string_to_axis_type(
            mesh.getAttribute("AxisType_" + axis).get<std::string>())) {
          case (Axis::Type::L):
            if (std::abs(*position - 0.0) > 1.0e-10)
              throw std::runtime_error(
                  "Position of MeshRecordComponent with type L in not 0.0");
            break;
          case (Axis::Type::N):
            if (std::abs(*position - 0.0) > 1.0e-10)
              throw std::runtime_error(
                  "Position of MeshRecordComponent with type N in not 0.0");
            break;
        }
      }
      position++;
    }
  } else {
    meshRecordComponent_ptr =
        std::make_unique<opmd::MeshRecordComponent>(mesh[label]);
    std::vector<double> position{};
    for (auto axis : mesh.axisLabels()) {
      switch (string_to_axis_type(
          mesh.getAttribute("AxisType_" + axis).get<std::string>())) {
        case (Axis::Type::L): position.push_back(0.0); break;
        case (Axis::Type::N): position.push_back(0.0); break;
      }
    }
    meshRecordComponent_ptr->setPosition<double>(position);
  }
  if (mode == OpenPMDMode::write) {
    opmd::Extent dataExtent{};
    for (std::string axis : mesh.axisLabels()) {
      dataExtent.push_back(
          gridDistribution.mpi_global_grid()(string_to_axis(axis)).N());
    }
    opmd::Dataset dataset{opmd::determineDatatype(data), dataExtent};
    meshRecordComponent_ptr->resetDataset(dataset);
  }
  opmd::Extent chunkExtent{};
  opmd::Offset chunkOffset{};
  for (std::string axis : mesh.axisLabels()) {
    chunkExtent.push_back(
        gridDistribution.mpi_local_grid()(string_to_axis(axis)).N());
    chunkOffset.push_back(
        gridDistribution.mpi_local_grid()(string_to_axis(axis)).low());
  }
  if (mode == OpenPMDMode::write) {
    meshRecordComponent_ptr->storeChunkRaw(data, chunkOffset, chunkExtent);
  } else {
    meshRecordComponent_ptr->loadChunkRaw(data, chunkOffset, chunkExtent);
  }
}

SimulationTime read_simulation_time(openPMD::Series series, size_t iteration) {
  openPMD::Iteration iter{series.iterations.at(iteration)};
  iter.open();
  if (iter.meshes.contains("f6d") and
      iter.meshes["f6d"].containsAttribute("gridPhase")) {
    openPMD::Mesh mesh{iter.meshes["f6d"]};
    double phase = mesh.getAttribute("gridPhase").get<double>();
    double time  = mesh.getAttribute("absoluteTime").get<double>();

    return SimulationTime{SimulationTimeConfig{config_file()}, iteration, time,
                          phase};
  } else {
    return SimulationTime{SimulationTimeConfig{config_file()}, iteration};
  }
}

}  // namespace Impl

void read_openPMD(DistributionFunction &f6d,
                  openPMD::Series series,
                  SimulationTime simulationTime) {
  OpenPMDInterface reader{f6d.gridDistribution(),
                          f6d.gridDistribution().mpi_local_grid().xvAxes};
  reader.set_series(series);
  reader.set_mesh_label(simulationTime, f6d.device().label());
  Kokkos::View<double ******, Kokkos::HostSpace> readView{};
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    readView = f6d.host();
  } else {
    readView = Kokkos::View<double ******, Kokkos::HostSpace>{
        "readView",           f6d.host().extent(0), f6d.host().extent(1),
        f6d.host().extent(2), f6d.host().extent(3), f6d.host().extent(4),
        f6d.host().extent(5)};
  }
  openPMD::Mesh mesh{reader.read_meshRecordComponent(readView)};
  if (std::abs(mesh.getAttribute("q").get<double>() - f6d.q()) > 1e-16 or
      std::abs(mesh.getAttribute("m").get<double>() - f6d.m()) > 1e-16)
    throw std::runtime_error(
        "DistributionFunction from file in "
        "iteration " +
        std::to_string(simulationTime.current_step()) + "with the mesh " +
        f6d.device().label() +
        " does not match the particle properties"
        "of DistributionFunction in BSL6D code:\n"
        "   |   openPMD  |  BSL6D  \n"
        " q |  " +
        std::to_string(mesh.getAttribute("q").get<double>()) + "  |  " +
        std::to_string(f6d.q()) +
        "\n"
        " m |  " +
        std::to_string(mesh.getAttribute("m").get<double>()) + "  |  " +
        std::to_string(f6d.m()));

  reader.flush();

  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    Kokkos::deep_copy(f6d.host(), readView);
  } else {
    Kokkos::deep_copy(f6d.host(), Impl::from_hard_drive_layout(readView));
  }
  Kokkos::deep_copy(f6d.device(), f6d.host());
  f6d.require_velocity_moments_update();

  reader.set_axes(std::array{AxisType::x1});
  reader.set_mesh_label(simulationTime,
                        f6d.electron_flux_surface_average().label());
  Kokkos::View<double *>::HostMirror h_fluxSurfaceAverage{
      Kokkos::create_mirror_view(f6d.electron_flux_surface_average())};
  reader.read_meshRecordComponent(h_fluxSurfaceAverage);
  reader.flush();
  MPI_Barrier(MPI_COMM_WORLD);
  Kokkos::deep_copy(f6d.electron_flux_surface_average(), h_fluxSurfaceAverage);
};

void write_openPMD(DistributionFunction const &f6d,
                   openPMD::Series series,
                   SimulationTime simulationTime) {
  OpenPMDInterface writer{f6d.gridDistribution(),
                          f6d.gridDistribution().mpi_local_grid().xvAxes};
  writer.set_series(series);
  writer.set_mesh_label(simulationTime, f6d.device().label());

  Kokkos::deep_copy(f6d.host(), f6d.device());
  Kokkos::View<double ******, Kokkos::HostSpace> writeView{};
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    writeView = f6d.host();
  } else {
    writeView = Impl::to_hard_drive_layout(f6d.host());
  }
  openPMD::Mesh mesh{writer.write_meshRecordComponent(writeView)};
  mesh.setAttribute("q", f6d.q());
  mesh.setAttribute("m", f6d.m());
  mesh.setAttribute("gridPhase", simulationTime.current_phase());
  mesh.setAttribute("absoluteTime", simulationTime.current_T());
  mesh.setAttribute("configFile", config_file().dump());
  writer.flush();

  writer.set_axes(std::array{AxisType::x1});
  writer.set_mesh_label(simulationTime,
                        f6d.electron_flux_surface_average().label());
  Kokkos::View<double *>::HostMirror h_fluxSurfaceAverage{
      Kokkos::create_mirror_view(f6d.electron_flux_surface_average())};
  Kokkos::deep_copy(h_fluxSurfaceAverage, f6d.electron_flux_surface_average());
  writer.write_meshRecordComponent(h_fluxSurfaceAverage);
  writer.flush();

  CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
}

void read_openPMD(ScalarField const &scalarField,
                  openPMD::Series series,
                  SimulationTime simulationTime) {
  OpenPMDInterface reader{
      scalarField.gridDistribution(),
      scalarField.gridDistribution().mpi_local_grid().xAxes};
  reader.set_series(series);
  reader.set_mesh_label(simulationTime, scalarField.device().label());
  Kokkos::View<double ***, Kokkos::HostSpace> readView{};
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    readView = create_grid_compatible_view(scalarField.grid(),
                                           scalarField.device().label());
  } else {
    readView = Kokkos::View<double ***, Kokkos::HostSpace>{
        "readView", scalarField.grid()(bsl6d::AxisType::x1).N(),
        scalarField.grid()(bsl6d::AxisType::x2).N(),
        scalarField.grid()(bsl6d::AxisType::x3).N()};
  }
  reader.read_meshRecordComponent(readView);
  reader.flush();
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    Kokkos::deep_copy(scalarField.get_host_subview_of_compute_region(),
                      readView);
  } else {
    Kokkos::deep_copy(scalarField.get_host_subview_of_compute_region(),
                      Impl::from_hard_drive_layout(readView));
  }
  Kokkos::deep_copy(scalarField.device(), scalarField.host());
};

void write_openPMD(ScalarField const &scalarField,
                   openPMD::Series series,
                   SimulationTime simulationTime) {
  Kokkos::deep_copy(scalarField.host(), scalarField.device());
  OpenPMDInterface writer{
      scalarField.gridDistribution(),
      scalarField.gridDistribution().mpi_local_grid().xAxes};
  writer.set_series(series);
  writer.set_mesh_label(simulationTime, scalarField.device().label());
  Kokkos::View<double ***, Kokkos::HostSpace> writeView{};
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    writeView = scalarField.get_contiguous_host_view_of_compute_region();
  } else {
    writeView = Impl::to_hard_drive_layout(
        scalarField.get_contiguous_host_view_of_compute_region());
  }
  writer.write_meshRecordComponent(writeView);
  writer.flush();

  CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
}

void read_openPMD(VectorField const &vectorField,
                  openPMD::Series series,
                  SimulationTime simulationTime) {
  OpenPMDInterface reader{
      vectorField(AxisType::x1).gridDistribution(),
      vectorField(AxisType::x1).gridDistribution().mpi_local_grid().xAxes};
  reader.set_series(series);
  reader.set_mesh_label(simulationTime, vectorField.label());
  for (auto axis : {AxisType::x1, AxisType::x2, AxisType::x3}) {
    Kokkos::View<double ***, Kokkos::HostSpace> readView{};
    if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                 Kokkos::DefaultHostExecutionSpace>) {
      readView = create_grid_compatible_view(
          vectorField(axis).grid(), vectorField(axis).device().label());
    } else {
      readView = Kokkos::View<double ***, Kokkos::HostSpace>{
          "readView", vectorField(axis).grid()(bsl6d::AxisType::x1).N(),
          vectorField(axis).grid()(bsl6d::AxisType::x2).N(),
          vectorField(axis).grid()(bsl6d::AxisType::x3).N()};
    }
    reader.read_meshRecordComponent(readView, axis_to_string(axis));
    reader.flush();
    if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                 Kokkos::DefaultHostExecutionSpace>) {
      Kokkos::deep_copy(vectorField(axis).get_host_subview_of_compute_region(),
                        readView);
    } else {
      Kokkos::deep_copy(vectorField(axis).get_host_subview_of_compute_region(),
                        Impl::from_hard_drive_layout(readView));
    }
    Kokkos::deep_copy(vectorField(axis).device(), vectorField(axis).host());
  }
}
void write_openPMD(VectorField const &vectorField,
                   openPMD::Series series,
                   SimulationTime simulationTime) {
  OpenPMDInterface writer{
      vectorField(AxisType::x1).gridDistribution(),
      vectorField(AxisType::x1).gridDistribution().mpi_local_grid().xAxes};
  writer.set_series(series);
  writer.set_mesh_label(simulationTime, vectorField.label());
  for (auto axis : {AxisType::x1, AxisType::x2, AxisType::x3}) {
    Kokkos::deep_copy(vectorField(axis).host(), vectorField(axis).device());
    Kokkos::View<double ***, Kokkos::HostSpace> writeView{};
    if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                 Kokkos::DefaultHostExecutionSpace>) {
      writeView =
          vectorField(axis).get_contiguous_host_view_of_compute_region();
    } else {
      writeView = Impl::to_hard_drive_layout(
          vectorField(axis).get_contiguous_host_view_of_compute_region());
    }
    writer.write_meshRecordComponent(writeView, axis_to_string(axis));
    // Flushing is required immediately after setting mesh record component
    // since writeView is deallocated due do the end of its scope
    writer.flush();
  }
  CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
};

void read_openPMD(MatrixField const &matrixField,
                  openPMD::Series series,
                  SimulationTime simulationTime) {
  OpenPMDInterface reader{
      matrixField(AxisType::x1, AxisType::x1).gridDistribution(),
      matrixField(AxisType::x1, AxisType::x1)
          .gridDistribution()
          .mpi_local_grid()
          .xAxes};
  reader.set_series(series);
  reader.set_mesh_label(simulationTime, matrixField.label());
  for (auto axisX : {AxisType::x1, AxisType::x2, AxisType::x3})
    for (auto axisY : {AxisType::x1, AxisType::x2, AxisType::x3}) {
      Kokkos::View<double ***, Kokkos::HostSpace> readView{};
      if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                   Kokkos::DefaultHostExecutionSpace>) {
        readView = create_grid_compatible_view(
            matrixField(axisX, axisY).grid(),
            matrixField(axisX, axisY).device().label());
      } else {
        readView = Kokkos::View<double ***, Kokkos::HostSpace>{
            "readView",
            matrixField(axisX, axisY).grid()(bsl6d::AxisType::x1).N(),
            matrixField(axisX, axisY).grid()(bsl6d::AxisType::x2).N(),
            matrixField(axisX, axisY).grid()(bsl6d::AxisType::x3).N()};
      }
      reader.read_meshRecordComponent(
          readView, axis_to_string(axisX) + axis_to_string(axisY));
      reader.flush();
      if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                   Kokkos::DefaultHostExecutionSpace>) {
        Kokkos::deep_copy(
            matrixField(axisX, axisY).get_host_subview_of_compute_region(),
            readView);
      } else {
        Kokkos::deep_copy(
            matrixField(axisX, axisY).get_host_subview_of_compute_region(),
            Impl::from_hard_drive_layout(readView));
      }
      Kokkos::deep_copy(matrixField(axisX, axisY).device(),
                        matrixField(axisX, axisY).host());
    }
};
void write_openPMD(MatrixField const &matrixField,
                   openPMD::Series series,
                   SimulationTime simulationTime) {
  if (matrixField(AxisType::x1, AxisType::x1)
          .gridDistribution()
          .cart_topology()
          .harddrive_write_communicator()) {
    OpenPMDInterface writer{
        matrixField(AxisType::x1, AxisType::x1).gridDistribution(),
        matrixField(AxisType::x1, AxisType::x1)
            .gridDistribution()
            .mpi_local_grid()
            .xAxes};
    writer.set_series(series);
    writer.set_mesh_label(simulationTime, matrixField.label());
    for (auto axisX : {AxisType::x1, AxisType::x2, AxisType::x3})
      for (auto axisY : {AxisType::x1, AxisType::x2, AxisType::x3}) {
        Kokkos::deep_copy(matrixField(axisX, axisY).host(),
                          matrixField(axisX, axisY).device());
        Kokkos::View<double ***, Kokkos::HostSpace> writeView{};
        if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                     Kokkos::DefaultHostExecutionSpace>) {
          writeView = matrixField(axisX, axisY)
                          .get_contiguous_host_view_of_compute_region();
        } else {
          writeView = Impl::to_hard_drive_layout(
              matrixField(axisX, axisY)
                  .get_contiguous_host_view_of_compute_region());
        }
        writer.write_meshRecordComponent(
            writeView, axis_to_string(axisX) + axis_to_string(axisY));
        // Flushing is required immediately after setting mesh record component
        // since writeView is deallocated due do the end of its scope
        writer.flush();
      }
  }
  CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
};
}  // namespace bsl6d

/*****************************************************************/
/*
\brief Initialization of IO of data using HDF5

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.09.2021
*/
/*****************************************************************/
#pragma once

#include <hdf5.h>
#include <Kokkos_Core.hpp>
#include <hdf5Interface.hpp>
#include <stdexcept>
#include <string>

namespace bsl6d {

class SelalibInterface {
  public:
  SelalibInterface() = default;
  void load(std::string filename,
            std::string datasetName,
            GridDistribution<3, 3> const &gridDistribution,
            Kokkos::View<double ******>::HostMirror data);

  private:
  HDF5Interface ioHDF{};
};

} /* namespace bsl6d */

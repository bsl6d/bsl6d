/*****************************************************************/
/*
\brief Initialization of IO of data using HDF5

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.12.2021

*/
/*****************************************************************/

#include <hdf5_espressopp.h>
#include <mpi_helper.hpp>

#include <hdf5Interface.hpp>
#include <iostream>
namespace bsl6d {

void HDF5Interface::create_file(std::string filename, bool overwrite) {
  auto propertyListCreate = H5Pcreate(H5P_FILE_ACCESS);
  CHECK_HDF5(
      H5Pset_fapl_mpio(propertyListCreate, MPI_COMM_WORLD, MPI_INFO_NULL));
  if (overwrite) {
    auto fileID = CHECK_HDF5(H5Fcreate(filename.c_str(), H5F_ACC_TRUNC,
                                       H5P_DEFAULT, propertyListCreate));
    CHECK_HDF5(H5Fclose(fileID));

  } else {
    auto fileID = CHECK_HDF5(H5Fcreate(filename.c_str(), H5F_ACC_EXCL,
                                       H5P_DEFAULT, propertyListCreate));
    CHECK_HDF5(H5Fclose(fileID));
  }
  H5Pclose(propertyListCreate);
};

template <typename OutView_t, typename GridDist_t>
void HDF5Interface::file_to_view(std::string filename,
                                 std::string datasetName,
                                 GridDist_t const &gridDist,
                                 typename OutView_t::HostMirror &h_data,
                                 bool readLayoutRight) {
  using HardDriveView_t =
      Kokkos::View<typename OutView_t::data_type, Kokkos::HostSpace>;
  HardDriveView_t h_data_read{};
  if constexpr (OutView_t::rank == 6) {
    h_data_read =
        Kokkos::View<typename OutView_t::data_type, Kokkos::HostSpace>{
            h_data.label(),   h_data.extent(0), h_data.extent(1),
            h_data.extent(2), h_data.extent(3), h_data.extent(4),
            h_data.extent(5)};
  } else if constexpr (OutView_t::rank == 3) {
    h_data_read =
        Kokkos::View<typename OutView_t::data_type, Kokkos::HostSpace>{
            h_data.label(), h_data.extent(0), h_data.extent(1),
            h_data.extent(2)};
  }

  /* If reading from a dataset written with ll switch chunk definition */
  if (readLayoutRight) {
    layout_right_chunk(gridDist);
  } else {
    layout_left_chunk(gridDist);
  }

  /* Open H5 in parallel mode */
  auto propertyListOpen = CHECK_HDF5(H5Pcreate(H5P_FILE_ACCESS));
  CHECK_HDF5(H5Pset_fapl_mpio(propertyListOpen, gridDist.cart_topology().comm(),
                              MPI_INFO_NULL));
  auto fileID =
      CHECK_HDF5(H5Fopen(filename.c_str(), H5F_ACC_RDONLY, propertyListOpen));
  CHECK_HDF5(H5Pclose(propertyListOpen));

  /* Create Dataset to read from */
  auto chunkSpace =
      CHECK_HDF5(H5Screate_simple(OutView_t::rank, chunkSize.begin(), NULL));
  auto datasetID =
      CHECK_HDF5(H5Dopen2(fileID, datasetName.c_str(), H5P_DEFAULT));
  auto dataSpace = CHECK_HDF5(H5Dget_space(datasetID));
  CHECK_HDF5(H5Sselect_hyperslab(dataSpace, H5S_SELECT_SET, offset.begin(),
                                 stride.begin(), count.begin(),
                                 chunkSize.begin()));

  /* Every MPI proc reads its own Chunk from hdf5 file */
  auto propertyListRead = CHECK_HDF5(H5Pcreate(H5P_DATASET_XFER));
  CHECK_HDF5(H5Pset_dxpl_mpio(propertyListRead, H5FD_MPIO_COLLECTIVE));
  CHECK_HDF5(H5Dread(datasetID, H5T_NATIVE_DOUBLE, chunkSpace, dataSpace,
                     propertyListRead, h_data_read.data()));

  CHECK_HDF5(H5Pclose(propertyListRead));
  CHECK_HDF5(H5Sclose(dataSpace));
  CHECK_HDF5(H5Dclose(datasetID));
  CHECK_HDF5(H5Sclose(chunkSpace));
  CHECK_HDF5(H5Fclose(fileID));

  if constexpr (std::is_same_v<Kokkos::DefaultHostExecutionSpace,
                               Kokkos::DefaultExecutionSpace>) {
    h_data = h_data_read;
  } else {
    h_data = Impl::from_hard_drive_layout(h_data_read);
  }
}

template <typename OutView_t, typename GridDist_t>
void HDF5Interface::view_to_file(std::string filename,
                                 std::string datasetName,
                                 GridDist_t const &gridDist,
                                 typename OutView_t::HostMirror &h_data) {
  if (gridDist.cart_topology().harddrive_write_communicator()) {
    layout_right_chunk(gridDist);
    using HardDriveView_t =
        Kokkos::View<typename OutView_t::data_type, Kokkos::HostSpace>;
    HardDriveView_t h_data_write{};

    if constexpr (std::is_same_v<Kokkos::DefaultHostExecutionSpace,
                                 Kokkos::DefaultExecutionSpace>) {
      h_data_write = h_data;
    } else {
      h_data_write = Impl::to_hard_drive_layout(h_data);
    }

    /* Open H5 in parallel mode */
    auto propertyListOpen = CHECK_HDF5(H5Pcreate(H5P_FILE_ACCESS));
    CHECK_HDF5(H5Pset_fapl_mpio(
        propertyListOpen, gridDist.cart_topology().comm(), MPI_INFO_NULL));
    auto fileID =
        CHECK_HDF5(H5Fopen(filename.c_str(), H5F_ACC_RDWR, propertyListOpen));
    CHECK_HDF5(H5Pclose(propertyListOpen));

    /* Create a new dataspace in hdf5 file to write to.
     * Always new datasets are created during view to file. */
    auto dataSpace =
        CHECK_HDF5(H5Screate_simple(OutView_t::rank, dataSize.begin(), NULL));
    auto propertyListDataSet = CHECK_HDF5(H5Pcreate(H5P_DATASET_CREATE));
    propertyListDataSet      = CHECK_HDF5(
        H5Pset_chunk(propertyListDataSet, OutView_t::rank, chunkSize.begin()));
    auto datasetID = CHECK_HDF5(
        H5Dcreate(fileID, datasetName.c_str(), H5T_NATIVE_DOUBLE, dataSpace,
                  H5P_DEFAULT, propertyListDataSet, H5P_DEFAULT));
    CHECK_HDF5(H5Pclose(propertyListDataSet));
    CHECK_HDF5(H5Sclose(dataSpace));

    /* Create dataset to write a chunk */
    dataSpace = CHECK_HDF5(H5Dget_space(datasetID));
    CHECK_HDF5(H5Sselect_hyperslab(dataSpace, H5S_SELECT_SET, offset.begin(),
                                   stride.begin(), count.begin(),
                                   chunkSize.begin()));
    auto chunkSpace =
        CHECK_HDF5(H5Screate_simple(OutView_t::rank, chunkSize.begin(), NULL));

    /* Every MPI procs writes its own chunk to dataspace */
    auto propertyListWrite = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(propertyListWrite, H5FD_MPIO_COLLECTIVE);
    CHECK_HDF5(H5Dwrite(datasetID, H5T_NATIVE_DOUBLE, chunkSpace, dataSpace,
                        propertyListWrite, h_data_write.data()));
    CHECK_HDF5(H5Pclose(propertyListWrite));

    CHECK_HDF5(H5Dclose(datasetID));
    CHECK_HDF5(H5Sclose(dataSpace));
    CHECK_HDF5(H5Sclose(chunkSpace));
    CHECK_HDF5(H5Fclose(fileID));
  }
  CHECK_MPI(MPI_Barrier(MPI_COMM_WORLD));
}

namespace Impl {
Kokkos::View<double ******, Kokkos::HostSpace> to_hard_drive_layout(
    Kokkos::View<double ******>::HostMirror in) {
  Kokkos::Array<size_t, 6> startIdx{};
  Kokkos::Array<size_t, 6> stopIdx{};
  for (size_t i = 0; i < 6; i++) stopIdx[i] = in.extent(i);

  Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>
      mdRngPol{startIdx, stopIdx};

  Kokkos::View<double ******, Kokkos::HostSpace> out{
      in.label(),   in.extent(0), in.extent(1), in.extent(2),
      in.extent(3), in.extent(4), in.extent(5)};
  Kokkos::parallel_for("transposeDistribution", mdRngPol,
                       [=](int i0, int i1, int i2, int i3, int i4, int i5) {
                         out(i0, i1, i2, i3, i4, i5) =
                             in(i0, i1, i2, i3, i4, i5);
                       });
  return out;
}

Kokkos::View<double ***, Kokkos::HostSpace> to_hard_drive_layout(
    Kokkos::View<double ***>::HostMirror in) {
  Kokkos::Array<size_t, 3> startIdx{};
  Kokkos::Array<size_t, 3> stopIdx{};
  for (size_t i = 0; i < 3; i++) stopIdx[i] = in.extent(i);

  Kokkos::MDRangePolicy<Kokkos::Rank<3>, Kokkos::DefaultHostExecutionSpace>
      mdRngPol{startIdx, stopIdx};

  Kokkos::View<double ***, Kokkos::HostSpace> out{in.label(), in.extent(0),
                                                  in.extent(1), in.extent(2)};
  Kokkos::parallel_for(
      "transposeDistribution", mdRngPol,
      [=](int i0, int i1, int i2) { out(i0, i1, i2) = in(i0, i1, i2); });
  return out;
}

Kokkos::View<double ******>::HostMirror from_hard_drive_layout(
    Kokkos::View<double ******, Kokkos::HostSpace> in) {
  Kokkos::Array<size_t, 6> startIdx{};
  Kokkos::Array<size_t, 6> stopIdx{};
  for (size_t i = 0; i < 6; i++) stopIdx[i] = in.extent(i);
  Kokkos::View<double ******>::HostMirror out{
      in.label(),   in.extent(0), in.extent(1), in.extent(2),
      in.extent(3), in.extent(4), in.extent(5)};

  Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>
      mdRngPol(startIdx, stopIdx);

  Kokkos::parallel_for("transposeDistribution", mdRngPol,
                       [=](int i0, int i1, int i2, int i3, int i4, int i5) {
                         out(i0, i1, i2, i3, i4, i5) =
                             in(i0, i1, i2, i3, i4, i5);
                       });
  return out;
}

Kokkos::View<double ***>::HostMirror from_hard_drive_layout(
    Kokkos::View<double ***, Kokkos::HostSpace> in) {
  Kokkos::Array<size_t, 3> startIdx{};
  Kokkos::Array<size_t, 3> stopIdx{};
  for (size_t i = 0; i < 3; i++) stopIdx[i] = in.extent(i);
  Kokkos::View<double ***>::HostMirror out{in.label(), in.extent(0),
                                           in.extent(1), in.extent(2)};

  Kokkos::MDRangePolicy<Kokkos::Rank<3>, Kokkos::DefaultHostExecutionSpace>
      mdRngPol(startIdx, stopIdx);
  Kokkos::parallel_for(
      "transposeDistribution", mdRngPol,
      [=](int i0, int i1, int i2) { out(i0, i1, i2) = in(i0, i1, i2); });
  return out;
}
}  // namespace Impl

template <typename GridDist_t>
void HDF5Interface::layout_left_chunk(GridDist_t const &gridDist) {
  for (auto axis : gridDist.my_local_grid().xvAxes) {
    dataSize[5 - static_cast<size_t>(axis)] =
        gridDist.mpi_global_grid()(axis).N();
    chunkSize[5 - static_cast<size_t>(axis)] =
        gridDist.my_local_grid()(axis).N();
    offset[5 - static_cast<size_t>(axis)] =
        gridDist.my_local_grid()(axis).low();
    stride[5 - static_cast<size_t>(axis)] = 1;
    count[5 - static_cast<size_t>(axis)]  = 1;
  }
}

template <typename GridDist_t>
void HDF5Interface::layout_right_chunk(GridDist_t const &gridDist) {
  for (auto axis : gridDist.my_local_grid().xvAxes) {
    dataSize[static_cast<size_t>(axis)]  = gridDist.mpi_global_grid()(axis).N();
    chunkSize[static_cast<size_t>(axis)] = gridDist.my_local_grid()(axis).N();
    offset[static_cast<size_t>(axis)]    = gridDist.my_local_grid()(axis).low();
    stride[static_cast<size_t>(axis)]    = 1;
    count[static_cast<size_t>(axis)]     = 1;
  }
}

/* Template initializations */
template void HDF5Interface::file_to_view<Kokkos::View<double ******>,
                                          GridDistribution<3, 3>>(
    std::string,
    std::string,
    GridDistribution<3, 3> const &,
    Kokkos::View<double ******>::HostMirror &,
    bool);
template void HDF5Interface::file_to_view<Kokkos::View<double ***>,
                                          GridDistribution<3, 0>>(
    std::string,
    std::string,
    GridDistribution<3, 0> const &,
    Kokkos::View<double ***>::HostMirror &,
    bool);
template void HDF5Interface::view_to_file<Kokkos::View<double ******>,
                                          GridDistribution<3, 3>>(
    std::string,
    std::string,
    GridDistribution<3, 3> const &,
    Kokkos::View<double ******>::HostMirror &);
template void HDF5Interface::view_to_file<Kokkos::View<double ***>,
                                          GridDistribution<3, 0>>(
    std::string,
    std::string,
    GridDistribution<3, 0> const &,
    Kokkos::View<double ***>::HostMirror &);
}  // namespace bsl6d

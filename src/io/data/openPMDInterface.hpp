#pragma once

#include <string>

#include <Kokkos_Core.hpp>
#include <openPMD/openPMD.hpp>

#include <distributionFunction.hpp>
#include <gridDistribution.hpp>
#include <time.hpp>

namespace bsl6d {

namespace Impl {

openPMD::Iteration get_opmd_iteration(openPMD::Series &series,
                                      SimulationTime const &simulationTime);
template <typename GridDistributionType>
openPMD::Mesh get_opmd_mesh(openPMD::Iteration &iter,
                            std::string const &label,
                            GridDistributionType const &gridDist,
                            SimulationTime const &simTime,
                            std::vector<AxisType> const &axes);
enum class OpenPMDMode { read, write };
template <typename GridDistributionType>
void read_write_mesh_record_component(
    openPMD::Mesh mesh,
    GridDistributionType const &gridDistribution,
    double *data,
    OpenPMDMode const &mode,
    std::string const &label = openPMD::MeshRecordComponent::SCALAR);

SimulationTime read_simulation_time(openPMD::Series series, size_t iteration);

}  // namespace Impl

template <typename GridDistributionType, size_t numberOfOutDim>
class OpenPMDInterface {
  public:
  OpenPMDInterface(GridDistributionType const &gridDist,
                   std::array<AxisType, numberOfOutDim> const &axes)
      : m_gridDist{gridDist} {
    this->set_axes(axes);
  }
  template <size_t N>
  void set_axes(std::array<AxisType, N> const &axes) {
    m_axes.clear();
    for (auto axis : axes) {
      if (not axis_is_in_grid(axis)) {
        throw std::runtime_error(
            "OpenPMDInterface received an axis that is "
            "not stored in provided GridDistribution!");
      }
      m_axes.push_back(axis);
    }
  }
  void set_series(openPMD::Series const &series) {
    m_seriesWasSet = true;
    m_series       = series;
  }
  void set_mesh_label(SimulationTime const &simTime, std::string const &label) {
    m_simTime         = simTime;
    m_meshLabel       = label;
    m_meshLabelWasSet = true;
  }

  template <typename ViewType>
  openPMD::Mesh read_meshRecordComponent(
      ViewType const &view,
      std::string const &meshRecordConmponentLabel =
          openPMD::MeshRecordComponent::SCALAR) {
    if (not m_seriesWasSet)
      throw std::runtime_error(
          "OpenPMDInterface.set_series(...) needs to be called before "
          "write_meshRecordComponent is called");
    if (not m_meshLabelWasSet)
      throw std::runtime_error(
          "OpenPMDInterface.set_mesh_label(...) needs to be called before "
          "read_meshRecordComponent is called");
    openPMD::Iteration iter{Impl::get_opmd_iteration(m_series, m_simTime)};
    iter.open();
    openPMD::Mesh mesh{
        Impl::get_opmd_mesh(iter, m_meshLabel, m_gridDist, m_simTime, m_axes)};
    Impl::read_write_mesh_record_component(mesh, m_gridDist, view.data(),
                                           Impl::OpenPMDMode::read,
                                           meshRecordConmponentLabel);
    return mesh;
  }
  template <typename ViewType>
  openPMD::Mesh write_meshRecordComponent(
      ViewType const &view,
      std::string const &meshRecordConmponentLabel =
          openPMD::MeshRecordComponent::SCALAR) {
    if (not m_seriesWasSet)
      throw std::runtime_error(
          "OpenPMDInterface.set_series(...) needs to be called before "
          "write_meshRecordComponent is called");
    if (not m_meshLabelWasSet)
      throw std::runtime_error(
          "OpenPMDInterface.set_mesh_label(...) needs to be called before "
          "write_meshRecordComponent is called");
    openPMD::Iteration iter{Impl::get_opmd_iteration(m_series, m_simTime)};
    iter.open();
    openPMD::Mesh mesh{
        Impl::get_opmd_mesh(iter, m_meshLabel, m_gridDist, m_simTime, m_axes)};
    Impl::read_write_mesh_record_component(mesh, m_gridDist, view.data(),
                                           Impl::OpenPMDMode::write,
                                           meshRecordConmponentLabel);
    return mesh;
  }
  void flush() { m_series.flush(); }

  private:
  OpenPMDInterface() = default;
  GridDistributionType const m_gridDist{};
  std::vector<AxisType> m_axes{};
  SimulationTime m_simTime{};
  std::string m_meshLabel{};
  bool m_meshLabelWasSet{false};
  bool m_seriesWasSet;
  openPMD::Series m_series;

  bool axis_is_in_grid(AxisType axis) {
    std::array axes{m_gridDist.mpi_local_grid().xvAxes};
    if (std::find(axes.begin(), axes.end(), axis) == axes.end()) {
      return false;
    } else {
      return true;
    }
  };
};

void read_openPMD(DistributionFunction &f6d,
                  openPMD::Series series,
                  SimulationTime simulationTime);
void write_openPMD(DistributionFunction const &f6d,
                   openPMD::Series series,
                   SimulationTime simulationTime);

void read_openPMD(ScalarField const &scalarField,
                  openPMD::Series series,
                  SimulationTime simulationTime);
void write_openPMD(ScalarField const &scalarField,
                   openPMD::Series series,
                   SimulationTime simulationTime);

void read_openPMD(VectorField const &vectorField,
                  openPMD::Series series,
                  SimulationTime simulationTime);
void write_openPMD(VectorField const &vectorField,
                   openPMD::Series series,
                   SimulationTime simulationTime);

void read_openPMD(MatrixField const &matrixField,
                  openPMD::Series series,
                  SimulationTime simulationTime);
void write_openPMD(MatrixField const &matrixField,
                   openPMD::Series series,
                   SimulationTime simulationTime);

} /* namespace bsl6d */

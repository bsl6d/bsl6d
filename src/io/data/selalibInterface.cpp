/*****************************************************************/
/*
\brief Initialization of IO of data using HDF5

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.12.2021

*/
/*****************************************************************/

#include <hdf5_espressopp.h>

#include <iostream>
#include <selalibInterface.hpp>
namespace bsl6d {

void SelalibInterface::load(std::string filename,
                            std::string datasetName,
                            GridDistribution<3, 3> const &gridDist,
                            Kokkos::View<double ******>::HostMirror data) {
  Kokkos::View<double ******>::HostMirror data_read{
      "readData",     data.extent(5), data.extent(4), data.extent(3),
      data.extent(2), data.extent(1), data.extent(0)};

  ioHDF
      .file_to_view<Kokkos::View<double ******>, bsl6d::GridDistribution<3, 3>>(
          filename, datasetName, gridDist, data_read, false);

  Kokkos::parallel_for(
      "InitLayoutTest",
      Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>{
          {0, 0, 0, 0, 0, 0},
          {data.extent(0), data.extent(1), data.extent(2), data.extent(3),
           data.extent(4), data.extent(5)}},
      KOKKOS_LAMBDA(size_t i, size_t j, size_t k, size_t l, size_t m,
                    size_t n) {
        data(i, j, k, l, m, n) = data_read(n, m, l, k, j, i);
      });
}

}  // namespace bsl6d

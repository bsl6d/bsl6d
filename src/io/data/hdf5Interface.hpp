/*****************************************************************/
/*
\brief Initialization of IO of data using HDF5

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.09.2021
*/
/*****************************************************************/
#pragma once

#include <hdf5.h>

#include <gridDistribution.hpp>

#include <Kokkos_Core.hpp>
#include <stdexcept>
#include <string>

namespace bsl6d {

struct Parallel {};

class HDF5Interface {
  public:
  HDF5Interface() = default;

  void create_file(std::string filename, bool overwrite = false);

  template <typename OutView_t, typename GridDist_t>
  void view_to_file(std::string filename,
                    std::string datasetName,
                    GridDist_t const &gridDist,
                    typename OutView_t::HostMirror &data);

  template <typename OutView_t, typename GridDist_t>
  void file_to_view(std::string filename,
                    std::string datasetName,
                    GridDist_t const &gridDist,
                    typename OutView_t::HostMirror &data,
                    bool readLayoutRight = true);

  private:
  template <typename GridDist_t>
  void layout_right_chunk(GridDist_t const &);
  template <typename GridDist_t>
  void layout_left_chunk(GridDist_t const &);

  // std::vector<size_t> should be more general and
  // no performance critical section in output.
  std::array<hsize_t, 6> dataSize{};
  std::array<hsize_t, 6> chunkSize{};
  std::array<hsize_t, 6> offset{};
  std::array<hsize_t, 6> stride{};
  std::array<hsize_t, 6> count{};
};

namespace Impl {
Kokkos::View<double ***, Kokkos::HostSpace> to_hard_drive_layout(
    Kokkos::View<double ***>::HostMirror);
Kokkos::View<double ******, Kokkos::HostSpace> to_hard_drive_layout(
    Kokkos::View<double ******>::HostMirror);
Kokkos::View<double ***>::HostMirror from_hard_drive_layout(
    Kokkos::View<double ***, Kokkos::HostSpace>);
Kokkos::View<double ******>::HostMirror from_hard_drive_layout(
    Kokkos::View<double ******, Kokkos::HostSpace>);

}  // namespace Impl
} /* namespace bsl6d */

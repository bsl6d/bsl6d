#!/usr/bin/python3.6
import numpy as np
import openpmd_api as opmd

series = opmd.Series("./%06T_TestopenPMDIO.h5", opmd.Access.create)
series.set_author("Nils Schild")
i = series.iterations[0]
i.set_time(0.0)
i.set_dt(0.1)

data_mesh = np.array(np.arange(0.0, 64.0, 0.125).reshape(4, 4, 4, 2, 2, 2))
mesh = i.meshes['f6d']

mesh.time_offset = 0.05
mesh.set_geometry(opmd.Geometry.cartesian)
mesh.set_axis_labels(np.array(("x1","x2","x3","v1","v2","v3")))
mesh.set_grid_spacing(np.array((5.5,4.75,5.0,20.0,18.0,22.0)))
mesh.set_grid_global_offset(np.array((0.0,0.0,0.0,-10.0,-9.0,-11.0)))
mesh.set_attribute("AxisType_x1","L")
mesh.set_attribute("AxisType_x2","L")
mesh.set_attribute("AxisType_x3","L")
mesh.set_attribute("AxisType_v1","N")
mesh.set_attribute("AxisType_v2","N")
mesh.set_attribute("AxisType_v3","N")
mesh.set_attribute("q",2.0)
mesh.set_attribute("m",3.0)

dist = mesh[opmd.Record_Component.SCALAR]
dist.position = np.array((0.0,0.0,0.0,0.0,0.0,0.0))

dist.reset_dataset(opmd.Dataset(data_mesh.dtype, data_mesh.shape))
dist.store_chunk(data_mesh)
series.flush()

mesh = i.meshes["initialFluxSurfaceAverage"]
mesh.set_geometry(opmd.Geometry.cartesian)
mesh.set_axis_labels(["x1"])
mesh.set_grid_spacing([5.5])
mesh.set_grid_global_offset([0.0])
mesh.set_attribute("AxisType_x1", "L")

fluxSurfaceAverage = mesh[opmd.Record_Component.SCALAR]
fs = np.array([0.0,0.125,0.25,0.375],dtype=np.float64)
fluxSurfaceAverage.reset_dataset(opmd.Dataset(fs.dtype, fs.shape))
fluxSurfaceAverage.store_chunk(fs)
series.flush()

i = series.iterations[1]
i.set_time(0.1)
i.set_dt(0.1)
data_mesh = np.array(np.arange(0.0, 8.0, 0.125).reshape(4, 4, 4))
mesh = i.meshes['rho']
mesh.time_offset = 0.0
mesh.set_geometry(opmd.Geometry.cartesian)
mesh.set_axis_labels(np.array(("x1","x2","x3")))
mesh.set_grid_spacing(np.array((5.5,4.75,5.0)))
mesh.set_grid_global_offset(np.array((0.0,0.0,0.0)))
mesh.set_attribute("AxisType_x1","L")
mesh.set_attribute("AxisType_x2","L")
mesh.set_attribute("AxisType_x3","L")

scalarField = mesh[opmd.Record_Component.SCALAR]
scalarField.position = np.array((0.0,0.0,0.0))


scalarField.reset_dataset(opmd.Dataset(data_mesh.dtype, data_mesh.shape))
scalarField.store_chunk(data_mesh)
series.flush()

data_mesh = dict([('x1',None),('x2',None),('x3',None)])
mesh = i.meshes['E']
mesh.time_offset = 0.0
mesh.set_geometry(opmd.Geometry.cartesian)
mesh.set_axis_labels(np.array(("x1","x2","x3")))
mesh.set_grid_spacing(np.array((5.5,4.75,5.0)))
mesh.set_grid_global_offset(np.array((0.0,0.0,0.0)))
mesh.set_attribute("AxisType_x1","L")
mesh.set_attribute("AxisType_x2","L")
mesh.set_attribute("AxisType_x3","L")

for dim in data_mesh:
  data_mesh[dim] = np.array(np.arange(0.0, 8.0, 0.125).reshape(4, 4, 4))
  vectorField = mesh[dim]
  vectorField.position = np.array((0.0,0.0,0.0))

  vectorField.reset_dataset(opmd.Dataset(data_mesh[dim].dtype, data_mesh[dim].shape))
  vectorField.store_chunk(data_mesh[dim])
series.flush()
del series

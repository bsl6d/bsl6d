/*****************************************************************/
/*
\brief Testing the io using the hdf5 package

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.12.2021
*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <hdf5.h>

#include <Kokkos_Core.hpp>
#include <filesystem>
#include <stdexcept>
#include <string>

#include "../hdf5Interface.hpp"

template <typename ViewType6D>
void init_view(Kokkos::View<double ******>, ViewType6D const &data) {
  Kokkos::Array<size_t, 6> startIdx{};
  Kokkos::Array<size_t, 6> stopIdx{};
  for (size_t i = 0; i < 6; i++) stopIdx[i] = data.extent(i);
  Kokkos::parallel_for(
      "InitLayoutTest",
      Kokkos::MDRangePolicy<Kokkos::Rank<6>,
                            typename ViewType6D::execution_space>{startIdx,
                                                                  stopIdx},
      KOKKOS_LAMBDA(size_t i, size_t j, size_t k, size_t l, size_t m,
                    size_t n) {
        data(i, j, k, l, m, n) = i * j * k * l * m * n + 1.1;
      });
}
template <typename ViewType3D>
void init_view(Kokkos::View<double ***>, ViewType3D const &data) {
  Kokkos::Array<size_t, 3> startIdx{};
  Kokkos::Array<size_t, 3> stopIdx{};
  for (size_t i = 0; i < 3; i++) stopIdx[i] = data.extent(i);
  Kokkos::parallel_for(
      "InitLayoutTest",
      Kokkos::MDRangePolicy<Kokkos::Rank<3>,
                            typename ViewType3D::execution_space>{startIdx,
                                                                  stopIdx},
      KOKKOS_LAMBDA(size_t i, size_t j, size_t k) {
        data(i, j, k) = i * j * k + 1.1;
      });
}

void is_same(
    Kokkos::View<double ******>::HostMirror const &h_data,
    Kokkos::View<double ******, Kokkos::HostSpace> const &hardwareLayoutData) {
  Kokkos::Array<size_t, 6> startIdx{};
  Kokkos::Array<size_t, 6> stopIdx{};
  for (size_t i = 0; i < 6; i++) stopIdx[i] = h_data.extent(i);

  Kokkos::parallel_for(
      "InitLayoutTest",
      Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>{
          startIdx, stopIdx},
      [=](size_t i, size_t j, size_t k, size_t l, size_t m, size_t n) {
        EXPECT_DOUBLE_EQ(hardwareLayoutData(i, j, k, l, m, n),
                         h_data(i, j, k, l, m, n));
      });
}

void is_same(
    Kokkos::View<double ***>::HostMirror const &h_data,
    Kokkos::View<double ***, Kokkos::HostSpace> const &hardwareLayoutData) {
  Kokkos::Array<size_t, 3> startIdx{};
  Kokkos::Array<size_t, 3> stopIdx{};
  for (size_t i = 0; i < 3; i++) stopIdx[i] = h_data.extent(i);

  Kokkos::parallel_for(
      "InitLayoutTest",
      Kokkos::MDRangePolicy<Kokkos::Rank<3>, Kokkos::DefaultHostExecutionSpace>{
          startIdx, stopIdx},
      [=](size_t i, size_t j, size_t k) {
        EXPECT_DOUBLE_EQ(hardwareLayoutData(i, j, k), h_data(i, j, k));
      });
}

template <typename ViewType>
class IODataHDF5Interface : public ::testing::Test {
  public:
  bsl6d::CartTopology<6> topology{bsl6d::GridDistributionConfig{}};
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDist{grid, topology};

  bsl6d::HDF5Interface io_hdf5{};

  using View_t = ViewType;
  ViewType data_w{};
  ViewType data_r{};
  Kokkos::
      View<typename ViewType::data_type, Kokkos::LayoutRight, Kokkos::HostSpace>
          hardwareLayoutData{};
  Kokkos::Array<size_t, ViewType::rank> startIdx{};
  Kokkos::Array<size_t, ViewType::rank> stopIdx{};

  typename ViewType::HostMirror h_data_r{};
  typename ViewType::HostMirror h_data_w{};

  IODataHDF5Interface() {
    init_class_on_ViewType(ViewType{});
    h_data_r = Kokkos::create_mirror_view(data_r);
    h_data_w = Kokkos::create_mirror_view(data_w);

    init_view(ViewType{}, h_data_w);

    Kokkos::deep_copy(h_data_r, 0.0);
    Kokkos::deep_copy(hardwareLayoutData, 0.0);

    for (size_t i = 0; i < ViewType::rank; i++) stopIdx[i] = data_w.extent(i);
  }
  void init_class_on_ViewType(Kokkos::View<double ******>) {
    data_w             = ViewType{"WriteData",
                      grid(bsl6d::AxisType::x1).N(),
                      grid(bsl6d::AxisType::x2).N(),
                      grid(bsl6d::AxisType::x3).N(),
                      grid(bsl6d::AxisType::v1).N(),
                      grid(bsl6d::AxisType::v2).N(),
                      grid(bsl6d::AxisType::v3).N()};
    data_r             = ViewType{"ReadData",
                      grid(bsl6d::AxisType::x1).N(),
                      grid(bsl6d::AxisType::x2).N(),
                      grid(bsl6d::AxisType::x3).N(),
                      grid(bsl6d::AxisType::v1).N(),
                      grid(bsl6d::AxisType::v2).N(),
                      grid(bsl6d::AxisType::v3).N()};
    hardwareLayoutData = Kokkos::View<typename ViewType::data_type,
                                      Kokkos::LayoutRight, Kokkos::HostSpace>{
        "hardwareLayoutData",          grid(bsl6d::AxisType::x1).N(),
        grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N(),
        grid(bsl6d::AxisType::v1).N(), grid(bsl6d::AxisType::v2).N(),
        grid(bsl6d::AxisType::v3).N()};
  };
  void init_class_on_ViewType(Kokkos::View<double ***>) {
    data_w =
        ViewType{"WriteData", grid(bsl6d::AxisType::x1).N(),
                 grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()};
    data_r =
        ViewType{"ReadData", grid(bsl6d::AxisType::x1).N(),
                 grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()};
    hardwareLayoutData = Kokkos::View<typename ViewType::data_type,
                                      Kokkos::LayoutRight, Kokkos::HostSpace>{
        "hardwareLayoutData", grid(bsl6d::AxisType::x1).N(),
        grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()};
  }
};

using IOViewTypes =
    ::testing::Types<Kokkos::View<double ******>, Kokkos::View<double ***>>;
// using IOViewTypes = ::testing::Types<Kokkos::View<double***>>;
TYPED_TEST_SUITE(IODataHDF5Interface, IOViewTypes);

TYPED_TEST(IODataHDF5Interface, CreateFile) {
  TestFixture::io_hdf5.create_file("CreateFile.h5");

  EXPECT_THROW(TestFixture::io_hdf5.create_file("CreateFile.h5"),
               std::runtime_error);

  EXPECT_NO_THROW(TestFixture::io_hdf5.create_file("CreateFile.h5", true));

  std::filesystem::remove("CreateFile.h5");
}

TYPED_TEST(IODataHDF5Interface, INwTransposition) {
  init_view(TypeParam{}, TestFixture::hardwareLayoutData);

  auto file_id =
      H5Fcreate("INwTransposition.h5", H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
  hsize_t dims[TypeParam::rank]{};
  for (size_t i = 0; i < TypeParam::rank; i++)
    dims[i] = TestFixture::hardwareLayoutData.extent(i);

  auto dataspace_id = H5Screate_simple(TypeParam::rank, dims, NULL);
  auto dataset_id   = H5Dcreate2(file_id, "f", H5T_NATIVE_DOUBLE, dataspace_id,
                                 H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
           TestFixture::hardwareLayoutData.data());

  H5Sclose(dataspace_id);
  H5Dclose(dataset_id);
  H5Fclose(file_id);

  if constexpr (TypeParam::rank == 6) {
    TestFixture::io_hdf5
        .template file_to_view<TypeParam, bsl6d::GridDistribution<3, 3>>(
            "INwTransposition.h5", "f", TestFixture::gridDist,
            TestFixture::h_data_r);
  } else if constexpr (TypeParam::rank == 3) {
    TestFixture::io_hdf5
        .template file_to_view<TypeParam, bsl6d::GridDistribution<3, 0>>(
            "INwTransposition.h5", "f",
            TestFixture::gridDist.create_xGridDistribution(),
            TestFixture::h_data_r);
  }
  is_same(TestFixture::h_data_r, TestFixture::hardwareLayoutData);

  std::filesystem::remove("INwTransposition.h5");
}

TYPED_TEST(IODataHDF5Interface, OUTwTransposition) {
  TestFixture::io_hdf5.create_file("OUTwTransposition.h5");

  if constexpr (TestFixture::View_t::rank == 6) {
    TestFixture::io_hdf5
        .template view_to_file<TypeParam, bsl6d::GridDistribution<3, 3>>(
            "OUTwTransposition.h5", "f", TestFixture::gridDist,
            TestFixture::h_data_w);
  } else if constexpr (TestFixture::View_t::rank == 3) {
    TestFixture::io_hdf5
        .template view_to_file<TypeParam, bsl6d::GridDistribution<3, 0>>(
            "OUTwTransposition.h5", "f",
            TestFixture::gridDist.create_xGridDistribution(),
            TestFixture::h_data_w);
  }

  auto file_id = H5Fopen("OUTwTransposition.h5", H5F_ACC_RDWR, H5P_DEFAULT);

  auto dataset_id = H5Dopen2(file_id, "f", H5P_DEFAULT);

  H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
          TestFixture::hardwareLayoutData.data());

  H5Dclose(dataset_id);
  H5Fclose(file_id);

  is_same(TestFixture::h_data_w, TestFixture::hardwareLayoutData);

  std::filesystem::remove("OUTwTransposition.h5");
}

/*****************************************************************/
/*
\brief Testing the io using the hdf5 package with multiple procs

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.12.2021
*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <hdf5.h>
#include <hdf5_espressopp.h>

#include <Kokkos_Core.hpp>
#include <filesystem>
#include <string>

#include "../hdf5Interface.hpp"

double LInfErr(double *const val, double comparison, size_t size) {
  double err{};
  Kokkos::parallel_reduce(
      "CompareReadWrite",
      Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, size),
      [=](size_t i, double &lerr) {
        lerr = std::max(std::abs(val[i] - comparison), lerr);
      },
      Kokkos::Max<double>(err));
  return err;
};

nlohmann::json configFileHDF5MPI = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [10.0, 12.0, 14.0, 2.0, 2.5, 3.0],
      "xvMin": [ 0.0,  0.0,  0.0,-2.0,-2.5,-3.0],
      "NxvPoints": [10, 12, 14, 15, 17, 19],
      "position": ["L","L","L","N","N","N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [2,2,1,3,1,1]
    }
  }
})"_json;

template <typename ViewType>
class IODataHDF5Parallel : public ::testing::Test {
  public:
  bsl6d::CartTopology<6> topology{
      bsl6d::GridDistributionConfig{configFileHDF5MPI}};
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{configFileHDF5MPI}};
  bsl6d::GridDistribution<3, 3> gridDist{grid, topology};
  ViewType data{};
  typename ViewType::HostMirror h_data{};

  bsl6d::HDF5Interface hdf5_parallel{};
  using View_t = ViewType;

  IODataHDF5Parallel() { init_class_on_ViewType(ViewType{}); }

  private:
  void init_class_on_ViewType(Kokkos::View<double ******>) {
    data   = ViewType{"data",
                    gridDist.my_local_grid()(bsl6d::AxisType::x1).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::x2).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::x3).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::v1).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::v2).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::v3).N()};
    h_data = Kokkos::create_mirror_view(data);
    Kokkos::deep_copy(h_data, topology.my_cart_rank());
  };
  void init_class_on_ViewType(Kokkos::View<double ***>) {
    data   = ViewType{"data", gridDist.my_local_grid()(bsl6d::AxisType::x1).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::x2).N(),
                    gridDist.my_local_grid()(bsl6d::AxisType::x3).N()};
    h_data = Kokkos::create_mirror_view(data);
    Kokkos::deep_copy(h_data,
                      topology.split<3>({true, true, true, false, false, false})
                          .my_cart_rank());
  };
};

using IOViewTypes =
    ::testing::Types<Kokkos::View<double ******>, Kokkos::View<double ***>>;
TYPED_TEST_SUITE(IODataHDF5Parallel, IOViewTypes);

TYPED_TEST(IODataHDF5Parallel, WriteReadProcessRank) {
  ASSERT_NO_THROW(TestFixture::hdf5_parallel.create_file("MyFile.h5", true));
  ASSERT_THROW(TestFixture::hdf5_parallel.create_file("MyFile.h5"),
               std::runtime_error);
  size_t size{1};
  /*
   * Write a file and read it directly afterwards. Every process writes its
   * rank to the HDF5 File and reads it back into the same view.
   * It is not verified that every process writes and reads the correct chunk
   * in this test.
   */
  if constexpr (TypeParam::rank == 6) {
    TestFixture::hdf5_parallel
        .template view_to_file<TypeParam, bsl6d::GridDistribution<3, 3>>(
            "MyFile.h5", "data", TestFixture::gridDist, TestFixture::h_data);
    Kokkos::deep_copy(TestFixture::h_data, -1.);
    TestFixture::hdf5_parallel
        .template file_to_view<TypeParam, bsl6d::GridDistribution<3, 3>>(
            "MyFile.h5", "data", TestFixture::gridDist, TestFixture::h_data);
    for (auto axis : TestFixture::gridDist.my_local_grid().xvAxes) {
      size *= TestFixture::gridDist.my_local_grid()(axis).N();
    }
  } else if constexpr (TypeParam::rank == 3) {
    TestFixture::hdf5_parallel
        .template view_to_file<TypeParam, bsl6d::GridDistribution<3, 0>>(
            "MyFile.h5", "data",
            TestFixture::gridDist.create_xGridDistribution(),
            TestFixture::h_data);
    Kokkos::deep_copy(TestFixture::h_data, -1.);
    TestFixture::hdf5_parallel
        .template file_to_view<TypeParam, bsl6d::GridDistribution<3, 0>>(
            "MyFile.h5", "data",
            TestFixture::gridDist.create_xGridDistribution(),
            TestFixture::h_data);
    for (auto axis : TestFixture::gridDist.create_xGridDistribution()
                         .my_local_grid()
                         .xvAxes) {
      size *= TestFixture::gridDist.my_local_grid()(axis).N();
    }
  }
  EXPECT_EQ(size, TestFixture::h_data.size());

  std::array<int, 6> keepMPI{true, true, true, false, false, false};
  bsl6d::CartTopology<3> topology3d{
      TestFixture::topology.template split<3>(keepMPI)};
  if constexpr (TypeParam::rank == 6) {
    EXPECT_EQ(LInfErr(TestFixture::h_data.data(),
                      TestFixture::topology.my_cart_rank(),
                      TestFixture::h_data.size()),
              0);
  } else if constexpr (TypeParam::rank == 3) {
    EXPECT_EQ(LInfErr(TestFixture::h_data.data(), topology3d.my_cart_rank(),
                      TestFixture::h_data.size()),
              0);
  }
  std::filesystem::remove("MyFile.h5");
}

#include <filesystem>
#include <random>
#include <stdexcept>
#include <string>

#include <gtest/gtest.h>

#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <mpiTopology.hpp>
#include <time.hpp>

#include "../openPMDInterface.hpp"

void init_comparison(Kokkos::View<double *> &view, bsl6d::Grid<3, 3> grid) {
  view = Kokkos::View<double *>{bsl6d::create_grid_compatible_view(
      grid, "fluxSurfaceAvgComp", std::array{bsl6d::AxisType::x1})};
  Kokkos::parallel_for(
      "initFluxSurfaceAverage", grid(bsl6d::AxisType::x1).N(),
      KOKKOS_LAMBDA(size_t ix1) { view(ix1) = ix1 * 0.125; });
}

void init_comparison(Kokkos::View<double ***> &view, bsl6d::Grid<3, 0> grid) {
  std::array<size_t, 3> stride{};
  view = Kokkos::View<double ***>{
      create_grid_compatible_view(grid, "scalarFieldComp")};
  stride[2] = 1;
  stride[1] = grid(bsl6d::AxisType::x3).N();
  stride[0] = stride[1] * grid(bsl6d::AxisType::x2).N();
  Kokkos::parallel_for(
      "initScalarField", grid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        view(ix1, ix2, ix3) =
            (stride[0] * ix1 + stride[1] * ix2 + stride[2] * ix3) * 0.125;
      });
}

void init_comparison(Kokkos::View<double ******> &view,
                     bsl6d::Grid<3, 3> grid) {
  std::array<size_t, 6> stride{};
  view = Kokkos::View<double ******>{
      create_grid_compatible_view(grid, "scalarFieldComp")};
  stride[5] = 1;
  stride[4] = grid(bsl6d::AxisType::v3).N();
  stride[3] = stride[4] * grid(bsl6d::AxisType::v2).N();
  stride[2] = stride[3] * grid(bsl6d::AxisType::v1).N();
  stride[1] = stride[2] * grid(bsl6d::AxisType::x3).N();
  stride[0] = stride[1] * grid(bsl6d::AxisType::x2).N();
  Kokkos::parallel_for(
      "initScalarField", grid.mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        view(ix1, ix2, ix3, iv1, iv2, iv3) =
            (stride[0] * ix1 + stride[1] * ix2 + stride[2] * ix3 +
             stride[3] * iv1 + stride[4] * iv2 + stride[5] * iv3) *
            0.125;
      });
}

class IODataopenPMDInterfaceMPI : public ::testing::Test {
  public:
  IODataopenPMDInterfaceMPI() {
    auto [T, f]    = bsl6d::initialize(create_config_file());
    simulationTime = T;
    f6d            = f;
    rho = bsl6d::ScalarField{f6d.gridDistribution().create_xGridDistribution(),
                             "rho"};
    E   = bsl6d::VectorField{f6d.gridDistribution().create_xGridDistribution(),
                           "E"};
    init_comparison(elFsAvgComp, f6d.gridDistribution().mpi_global_grid());
    init_comparison(distFuncFieldComp,
                    f6d.gridDistribution().mpi_global_grid());
    for (auto axis : bsl6d::xAxes()) {
      rho.set_halo_width(axis, static_cast<size_t>(axis));
    };
    init_comparison(scalarFieldComp,
                    f6d.gridDistribution().mpi_global_grid().create_xSpace());
  };
  ~IODataopenPMDInterfaceMPI() {
    std::filesystem::remove("000000_" + writeFilename);
    std::filesystem::remove("000001_" + writeFilename);
  }

  static nlohmann::json create_config_file() {
    nlohmann::json configFile = R"(
        {
          "distributionFunction": {
            "q": 2.0,
            "m": 3.0
          },
          "simulationTime": {
            "dt": 0.1,
            "T": 1.0,
          "restart": false
          },
          "initializationDistributionFunction": {
            "initFunction": "zero"
          },
          "gridDistribution" : {
            "grid": {
              "axes": ["x1","x2","x3","v1","v2","v3"],
              "xvMax": [22.0, 19.0, 20.0, 10.0, 9.0, 11.0],
              "xvMin": [ 0.0,  0.0,  0.0,-10.0,-9.0,-11.0],
              "NxvPoints": [4, 4, 4, 2, 2, 2],
              "position": ["L", "L", "L", "N", "N", "N"],
              "rotateGyroFreq": false
            },
            "parallel": {
              "Nprocs": [2, 1, 1, 2, 1, 1]
            }
          }
        })"_json;
    return configFile;
  }

  std::string writeFilename{"TestDistributionFunction.h5"};
  openPMD::Series readSeries{"%06T_TestopenPMDIO.h5",
                             openPMD::Access::READ_WRITE, MPI_COMM_WORLD};
  openPMD::Series writeSeries{"%06T_TestDistributionFunction.h5",
                              openPMD::Access::CREATE, MPI_COMM_WORLD};
  bsl6d::DistributionFunction f6d{};
  bsl6d::SimulationTime simulationTime{};
  bsl6d::ScalarField rho{};
  bsl6d::VectorField E{};
  Kokkos::View<double ***> scalarFieldComp{};
  Kokkos::View<double *> elFsAvgComp{};
  Kokkos::View<double ******> distFuncFieldComp{};
};

double Linf_error(bsl6d::DistributionFunction const &dist,
                  Kokkos::View<double ******> const &comparisonGlobalGrid) {
  std::array<size_t, 6> offset{
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::x1).low(),
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::x2).low(),
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::x3).low(),
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::v1).low(),
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::v2).low(),
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::v3).low()};
  double err{};
  Kokkos::parallel_reduce(
      "Linf_error", dist.gridDistribution().mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3, double &lerr) {
        double diff{
            std::abs(dist(ix1, ix2, ix3, iv1, iv2, iv3) -
                     comparisonGlobalGrid(offset[0] + ix1, offset[1] + ix2,
                                          offset[2] + ix3, offset[3] + iv1,
                                          offset[4] + iv2, offset[5] + iv3))};
        lerr = std::max(lerr, diff);
      },
      Kokkos::Max<double>(err));
  return err;
}

double Linf_error(bsl6d::DistributionFunction const &dist,
                  Kokkos::View<double *> const &comparisonGlobalGrid) {
  std::array<size_t, 1> offset{
      dist.gridDistribution().mpi_local_grid()(bsl6d::AxisType::x1).low()};
  Kokkos::View<double *> view{dist.electron_flux_surface_average()};
  double err{};
  Kokkos::parallel_reduce(
      "Linf_error", dist.mpi_local_grid()(bsl6d::AxisType::x1).N(),
      KOKKOS_LAMBDA(size_t ix1, double &lerr) {
        double diff{
            std::abs(view(ix1) - comparisonGlobalGrid(offset[0] + ix1))};
        lerr = std::max(lerr, diff);
      },
      Kokkos::Max<double>(err));
  return err;
}

TEST_F(IODataopenPMDInterfaceMPI, DistributionFunctionReadWrite) {
  ASSERT_NO_THROW(bsl6d::read_openPMD(f6d, readSeries, simulationTime));
  ASSERT_LT(Linf_error(f6d, distFuncFieldComp), 1e-10);
  ASSERT_LT(Linf_error(f6d, elFsAvgComp), 1e-10);
  ASSERT_NO_THROW(bsl6d::write_openPMD(f6d, writeSeries, simulationTime));
  ASSERT_NO_THROW(bsl6d::read_openPMD(f6d, writeSeries, simulationTime));
  ASSERT_LT(Linf_error(f6d, distFuncFieldComp), 1e-10);
  ASSERT_LT(Linf_error(f6d, elFsAvgComp), 1e-10);
}

// The Assert tests have been flacky due to SegFaults.
// Currently there is not time to debug and they are not of high
// priority for the moment
// Gitlab: #94
// TEST_F(IODataopenPMDInterfaceMPI, DistributionFunctionAssertGrid) {
//  // ASSERT_THROW(bsl6d::read_openPMD(f6d, "UnknownFile.h5", simulationTime),
//  //             openPMD::no_such_file_error);
//  bsl6d::DistributionFunction f6dWrongGrid{
//      bsl6d::GridDistribution{
//          bsl6d::Grid<3, 3>::get_3d3v_grid(),
//          f6d.gridDistribution().cart_topology().create_single_proc_topology()},
//      bsl6d::DistributionFunctionConfig{create_config_file()}};
//  ASSERT_THROW(bsl6d::read_openPMD(f6dWrongGrid, readSeries, simulationTime),
//               std::runtime_error);
//}
//
// TEST_F(IODataopenPMDInterfaceMPI,
//       DistributionFunctionAssertParticleProperties) {
//  bsl6d::DistributionFunction f6dWrongParticle{
//      f6d.gridDistribution(), bsl6d::DistributionFunctionConfig{}};
//  ASSERT_THROW(
//      bsl6d::read_openPMD(f6dWrongParticle, readSeries, simulationTime),
//      std::runtime_error);
//}

double Linf_error(bsl6d::ScalarField const &scalarField,
                  Kokkos::View<double ***> comparisonGlobalGrid) {
  std::array<size_t, 3> offset{scalarField.gridDistribution()
                                   .mpi_local_grid()(bsl6d::AxisType::x1)
                                   .low(),
                               scalarField.gridDistribution()
                                   .mpi_local_grid()(bsl6d::AxisType::x2)
                                   .low(),
                               scalarField.gridDistribution()
                                   .mpi_local_grid()(bsl6d::AxisType::x3)
                                   .low()};
  double err{};
  Kokkos::parallel_reduce(
      "Linf_error",
      scalarField.gridDistribution().mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, double &lerr) {
        double diff{
            std::abs(scalarField(ix1, ix2, ix3) -
                     comparisonGlobalGrid(offset[0] + ix1, offset[1] + ix2,
                                          offset[2] + ix3))};
        lerr = std::max(lerr, diff);
      },
      Kokkos::Max<double>(err));
  return err;
}

double Linf_error(Kokkos::View<double *> view1, Kokkos::View<double *> view2) {
  double err{};
  Kokkos::parallel_reduce(
      "Linf_error",
      Kokkos::RangePolicy<Kokkos::DefaultExecutionSpace>{0, view1.extent(0)},
      KOKKOS_LAMBDA(size_t i, double &lerr) {
        double diff{std::abs(view1(i) - view2(i))};
        lerr = std::max(lerr, diff);
      },
      Kokkos::Max<double>(err));
  return err;
}

TEST_F(IODataopenPMDInterfaceMPI, ScalarFieldReadWrite) {
  simulationTime.advance_current_T_by_dt();
  ASSERT_NO_THROW(bsl6d::read_openPMD(rho, readSeries, simulationTime));
  ASSERT_LT(Linf_error(rho, scalarFieldComp), 1e-10);
  ASSERT_NO_THROW(bsl6d::write_openPMD(rho, writeSeries, simulationTime));
  ASSERT_NO_THROW(bsl6d::read_openPMD(rho, writeSeries, simulationTime));
  ASSERT_LT(Linf_error(rho, scalarFieldComp), 1e-10);
}

TEST_F(IODataopenPMDInterfaceMPI, ScalarFieldAssertGrid) {
  simulationTime.advance_current_T_by_dt();
  bsl6d::GridDistribution wrongGrid{
      bsl6d::Grid<3, 3>{bsl6d::GridDistributionConfig{}},
      f6d.gridDistribution().cart_topology().create_single_proc_topology()};
  bsl6d::ScalarField rhoWrongGrid{wrongGrid.create_xGridDistribution(), "rho"};
  ASSERT_THROW(bsl6d::read_openPMD(rhoWrongGrid, readSeries, simulationTime),
               std::runtime_error);
}

TEST_F(IODataopenPMDInterfaceMPI, VectorFieldReadWrite) {
  simulationTime.advance_current_T_by_dt();
  ASSERT_NO_THROW(bsl6d::read_openPMD(E, readSeries, simulationTime));
  for (auto axis : bsl6d::xAxes()) {
    ASSERT_LT(Linf_error(E(axis), scalarFieldComp), 1e-10);
  }
  ASSERT_NO_THROW(bsl6d::write_openPMD(E, writeSeries, simulationTime));
  ASSERT_NO_THROW(bsl6d::read_openPMD(E, writeSeries, simulationTime));
  for (auto axis : bsl6d::xAxes()) {
    ASSERT_LT(Linf_error(E(axis), scalarFieldComp), 1e-10);
  }
}

#pragma once

#include <nlohmann/json.hpp>

namespace bsl6d {

nlohmann::json get_test_case_config_file(std::string const& testCase);
void create_test_case_config_file(std::string const& testCase);
std::string create_config_file_help_message();
}  // namespace bsl6d
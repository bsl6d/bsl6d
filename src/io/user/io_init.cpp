/*****************************************************************/
/*
\brief Initialize all io streams to communicate with a user

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   03.09.2021
*/
/*****************************************************************/
#include <filesystem>
#include <iostream>

#include <git.h>
#include <nlohmann/json.hpp>

#include <config.hpp>
#include <diagnostics.hpp>

#include <inputFileDatabase.hpp>

#include <config.hpp>
#include "./io_init.hpp"

namespace bsl6d {
static std::ofstream bsl6dLogStream{};
static nlohmann::json bsl6dConfigFile{};

nlohmann::json const& config_file() { return bsl6dConfigFile; };
std::ofstream& bsl6d_log_stream() { return bsl6dLogStream; };

/*****************************************************************/
/*

\brief Initialize main output from master process

\param[in]    fileName - file to write output of master to

ToDo Replace based on #85

*/
/*****************************************************************/
namespace Impl {
void validate_and_complete_config_file_structure(nlohmann::json configFile) {
  // Use contain statements in the constructors of the config files
  // Check issue #98 for more information
  std::vector<std::vector<std::string>> optionalSections{
      std::vector<std::string>{"gridDistribution", "grid", "parallel"},
      std::vector<std::string>{"distributionFunction"},
      std::vector<std::string>{"operators", "interpolator"},
      std::vector<std::string>{"initializationDistributionFunction"},
      std::vector<std::string>{"diagnostics"},
      std::vector<std::string>{"rightHandSide"},
      std::vector<std::string>{"integrator"},
      std::vector<std::string>{"fieldSolver"},
      std::vector<std::string>{"simulationTime"}};

  for (auto section : optionalSections) {
    if (not configFile.contains(section[0])) {
      configFile.emplace(section[0], nlohmann::json::value_t::object);
    }
    if (section.size() > 1) {
      auto configStructure = section.begin();
      configStructure++;
      while (configStructure != section.end()) {
        if (not configFile[section[0]].contains(*configStructure)) {
          configFile[section[0]].emplace(*configStructure,
                                         nlohmann::json::value_t::object);
        }
        configStructure++;
      }
    }
  }

  bsl6dConfigFile = configFile;
}

}  // namespace Impl

std::ofstream& initMasterOut(std::string const& fileName) {
  if (bsl6d::cmd::is_restart()) {
    bsl6dLogStream = std::ofstream{fileName, std::ios_base::app};
  } else {
    bsl6dLogStream = std::ofstream{fileName};
  }
  if (!bsl6dLogStream)
    throw std::ios_base::failure("Could not open file " + fileName);

  bsl6dLogStream.setf(std::ios::scientific);
  bsl6dLogStream.precision(5);
  bsl6dLogStream << "/***************************************************/\n";
  bsl6dLogStream << "[Welcome to your bsl6d calculation]\n";
  bsl6dLogStream << "/***************************************************/\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "/***************************/\n";
  bsl6dLogStream << "[Contact information]\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "  - Name:        Nils Schild\n";
  bsl6dLogStream << "  - E-Mail:      nils.schild@ipp.mpg.de\n";
  bsl6dLogStream << "  - Institution: Max-Plank-Institut for Plasma Physics\n";
  bsl6dLogStream << "  - Departement: NMPP\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "/***************************/\n";
  bsl6dLogStream << "[Version informations]\n";
  if (git_IsPopulated()) {
    bsl6dLogStream
        << "  - Git Tag: /************ToBeAdded*******************/\n";
    bsl6dLogStream << "  - Git Hash: " << git_CommitSHA1() << "\n";
    bsl6dLogStream << "  - Git dirty branch: " << git_AnyUncommittedChanges()
                   << "\n";
  } else {
    bsl6dLogStream << "  NO GIT INFORMATION AVAILABLE!\n";
  }
  bsl6dLogStream << "\n";
  bsl6dLogStream << "/***************************/\n";
  bsl6dLogStream << " Compile-Time information \n";
  bsl6dLogStream
      << "    - Compiler ID: /***********To Be Added**************/\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "/***************************/\n";
  bsl6dLogStream << " Runtime information \n";
  bsl6dLogStream << "/***********************To Be Added*****************/\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream << "\n";
  bsl6dLogStream.flush();

  return bsl6dLogStream;
}

namespace cmd {

template <typename T>
struct IoFlag {
  std::string description;
  T value;
  std::string abriviatedFlag{};
  bool isSet{false};
};

using FlagTypes = std::variant<IoFlag<bool>, IoFlag<std::string>>;

static std::map<std::string, FlagTypes> flags{
    {"--input_file",
     IoFlag<std::string>{"Path to the BSL6D input configuration file.",
                         "bsl6dConfig.json", "-i"}},
    {"--output_file",
     IoFlag<std::string>{"Path to the BSL6D output diagnostics file.",
                         "bsl6dDiagnostics", "-o"}},
    {"--restart",
     IoFlag<std::string>{"Restart the BSL6D process from restart file",
                         "<restart_file>", "-r"}},
    {"--get_config",
     IoFlag<std::string>{"Retrieve BSL6D simulation configuration.",
                         "<test_case>", "noAbreviation"}},
    {"--config_help",
     IoFlag<std::string>{"Show help for passed config section.",
                         "<default-string>", "noAbreviation"}},
    {"--test", IoFlag<std::string>{"Run BSL6D in test mode.", "<test_case>",
                                   "noAbreviation"}},
    {"--help", IoFlag<bool>{"Display help information.", false, "-h"}},
    {"--delete",
     IoFlag<bool>{"Deletes output folder before starting simulation", false,
                  "-d"}},
    {"--verbose",
     IoFlag<bool>{"Enable verbose logging for BSL6D.", false, "-v"}}};

static std::map<std::string, FlagTypes> const defaultFlags{flags};

struct SetFlag {
  SetFlag(const std::string& val) : m_val{val} {};
  void operator()(IoFlag<bool>& flag) {
    flag.value = true;
    flag.isSet = true;
  }

  void operator()(IoFlag<std::string>& flag) {
    flag.value = m_val;
    flag.isSet = true;
  }

  private:
  std::string m_val;
};

struct GetValueAsString {
  std::string operator()(IoFlag<bool> flag) {
    return flag.value ? "true" : "false";
  }

  std::string operator()(IoFlag<std::string> flag) { return flag.value; }
};

void set_flag(std::string val, FlagTypes& flag) {
  std::visit(SetFlag{val}, flag);
}

std::string get_value_as_string(FlagTypes flag) {
  return std::visit(GetValueAsString{}, flag);
}

std::string get_abriviated_flag(FlagTypes flag) {
  std::string shortFlag =
      std::visit([&](const auto& flag) { return flag.abriviatedFlag; }, flag);
  if (shortFlag == "noAbreviation") {
    return "";
  } else {
    return shortFlag;
  }
}

std::string get_description(FlagTypes flag) {
  return std::visit([&](const auto& flag) { return flag.description; }, flag);
}

std::map<std::string, std::string> abriviated_flags() {
  std::map<std::string, std::string> shortToLongMap;
  for (const auto& [longFlag, flag] : flags)
    shortToLongMap[get_abriviated_flag(flag)] = longFlag;
  return shortToLongMap;
}

struct Colors {
  static constexpr std::string_view reset{"\033[0m"};
  static constexpr std::string_view green{"\033[32m"};
  static constexpr std::string_view yellow{"\033[33m"};
};

void print_flags_help_message() {
  std::cout << Colors::green;
  std::cout << "Usage:\n" << Colors::reset << Colors::yellow;
  std::cout << "./bsl6d_integrator --test <test_case>" << Colors::reset;
  std::cout << Colors::green;
  std::cout << "        : Run the specified test case\n" << Colors::reset;
  std::cout << Colors::yellow;
  std::cout << "./bsl6d_integrator --get_config <test_case>" << Colors::reset;
  std::cout << Colors::green;
  std::cout << "  : Get example configuration for the test case\n"
            << Colors::reset;
  std::cout << Colors::yellow;
  std::cout << "./bsl6d_integrator" << Colors::reset;
  std::cout << Colors::green;
  std::cout << "                           : Run the integrator with default "
               "arguments\n\n"
            << Colors::reset;

  size_t maxFlagNameWidth     = 0;
  size_t maxDescriptionWidth  = 0;
  size_t maxDefaultValueWidth = 0;

  for (const auto& [flagName, flag] : flags) {
    maxFlagNameWidth = std::max(
        maxFlagNameWidth, (flagName + ", " + get_abriviated_flag(flag)).size());
    maxDescriptionWidth =
        std::max(maxDescriptionWidth, get_description(flag).size());
    maxDefaultValueWidth =
        std::max(maxDefaultValueWidth, get_value_as_string(flag).size());
  }

  std::cout << std::left << std::setw(maxFlagNameWidth + 4) << "Flag Name"
            << std::setw(maxDescriptionWidth + 4) << "Description"
            << "Value"
            << "\n ";

  std::cout << std::string(maxFlagNameWidth + maxDescriptionWidth +
                               maxDefaultValueWidth + 10,
                           '-')
            << '\n';

  for (const auto& [flagName, flag] : flags) {
    std::cout << std::left << std::setw(maxFlagNameWidth + 4)
              << flagName + ", " + get_abriviated_flag(flag)
              << std::setw(maxDescriptionWidth + 4) << get_description(flag)
              << get_value_as_string(flag) << '\n';
  };

  std::cout << "\n" << create_config_file_help_message() << "\n";

  std::cout << "Full documentation can be found under: \n";
  std::cout << Colors::yellow << "https://bsl6d.pages.mpcdf.de/bsl6d\n\n";
  std::cout << std::flush;
}

std::string input_file() { return get_value_as_string(flags["--input_file"]); }

std::string diagnostic_directory() {
  return get_value_as_string(flags["--output_file"]) + "/";
}

std::string output_file() {
  return get_value_as_string(flags["--output_file"]) + ".out";
}

std::string test_type() { return get_value_as_string(flags["--test"]); }

std::string init_type() { return get_value_as_string(flags["--get_config"]); }

std::filesystem::path restart_file() {
  std::filesystem::path restartFile{get_value_as_string(flags["--restart"])};
  if (not std::filesystem::is_regular_file(restartFile)) {
    throw std::runtime_error("Provided restart file '" +
                             get_value_as_string(flags["--restart"]) +
                             "' does not exist or is not a regular file!");
  }
  return restartFile;
}

bool is_test() {
  return std::get<IoFlag<std::string>>(flags.at("--test")).isSet;
}

bool init_only() {
  return std::get<IoFlag<std::string>>(flags.at("--get_config")).isSet;
}

bool is_verbose() { return std::get<IoFlag<bool>>(flags["--verbose"]).value; }

bool is_restart() {
  return std::get<IoFlag<std::string>>(flags.at("--restart")).isSet;
}

bool delete_flag_value() {
  return std::get<IoFlag<bool>>(flags.at("--delete")).isSet;
}

bool input_file_passed() {
  return std::get<IoFlag<std::string>>(flags.at("--input_file")).isSet;
}

bool output_file_passed() {
  return std::get<IoFlag<std::string>>(flags.at("--output_file")).isSet;
}

void parse(int argc, char* argv[]) {
  for (int i = 1; i < argc; i += 1) {
    std::string flag = argv[i];
    if (flags.find(abriviated_flags()[flag]) != flags.end())
      flag = abriviated_flags()[flag];

    if (flags.find(flag) != flags.end()) {
      if (i + 1 < argc && flags.find(argv[i + 1]) == flags.end() &&
          flags.find(abriviated_flags()[argv[i + 1]]) == flags.end()) {
        set_flag(argv[i + 1], flags[flag]);
        i++;
      } else {
        set_flag("", flags[flag]);
      }
    }
  }
  if (std::get<IoFlag<bool>>(flags["--help"]).value) {
    print_flags_help_message();
    exit(EXIT_SUCCESS);
  }
  std::string configLabel =
      std::get<IoFlag<std::string>>(flags["--config_help"]).value;
  if (configLabel !=
      std::get<IoFlag<std::string>>(defaultFlags.at("--config_help")).value) {
    print_config_help(configLabel);
    exit(EXIT_SUCCESS);
  }
}

}  // namespace cmd
}  // namespace bsl6d

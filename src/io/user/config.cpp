/*****************************************************************/
/*
\brief Create configuration structures for different components
       of the bsl6d-Framework.

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   01.09.2021
*/
/*****************************************************************/

#include <iostream>

#include <initializationFunctions.hpp>
#include <inputFileDatabase.hpp>
#include <integrator.hpp>
#include <io_init.hpp>
#include <mpi_helper.hpp>

#include "./config.hpp"

namespace bsl6d {

GridDistributionConfig::GridDistributionConfig() {
  if (configMPICartTopology.Nprocs.size() == 0) {
    configMPICartTopology.Nprocs = {};
    for (auto label : configGrid.axes)
      configMPICartTopology.Nprocs.push_back(1);
  }
  if (configMPICartTopology.periodicity.size() == 0) {
    configMPICartTopology.periodicity = {};
    for (auto label : configGrid.axes)
      configMPICartTopology.periodicity.push_back(true);
  }
}

GridDistributionConfig::GridDistributionConfig(nlohmann::json const &jsonConfig)
    : configGrid{jsonConfig["gridDistribution"]["grid"]
                     .get<GridDistributionConfig::ConfigGrid>()}
    , configMPICartTopology{
          jsonConfig["gridDistribution"]["parallel"]
              .get<GridDistributionConfig::ConfigMPICartTopology>()} {
  if (configMPICartTopology.Nprocs.size() == 0) {
    configMPICartTopology.Nprocs = {};
    for (auto label : configGrid.axes)
      configMPICartTopology.Nprocs.push_back(1);
  }
  if (configMPICartTopology.periodicity.size() == 0) {
    configMPICartTopology.periodicity = {};
    for (auto label : configGrid.axes)
      configMPICartTopology.periodicity.push_back(true);
  }
}

void GridDistributionConfig::validate(std::ofstream &ofs,
                                      size_t &warnings,
                                      size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Grid]\n";
  ofs << "\n";
  std::vector<std::string> axes{"x1", "x2", "x3", "v1", "v2", "v3"};
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->configGrid}
      << "\n";

  if (configGrid.xvMax.size() > 6 or
      configGrid.xvMax.size() != configGrid.axes.size() or
      configGrid.xvMax.size() != configGrid.xvMin.size() or
      configGrid.xvMax.size() != configGrid.NxvPoints.size() or
      configGrid.xvMax.size() != configGrid.position.size()) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Given number of values in \"grid\" configuration does not "
           "match!\n";
    ofs << "  \"xvMax\", \"xvMin\", \"Nxv\" and \"position\" require the same "
           "amount of "
           "values.\n";
  }
  for (size_t iAxis = 0; iAxis < configGrid.xvMax.size(); iAxis++) {
    if (configGrid.xvMax[iAxis] <= configGrid.xvMin[iAxis]) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  \"xvMax\">\"xvMin\" is required for every dimension of the "
             "grid!\n";
    }
    if (configGrid.NxvPoints[iAxis] <= 0) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  \"NxvPoints\">0 is required for every dimension of the grid!\n";
    }
    if (configGrid.position[iAxis] != "N" and
        configGrid.position[iAxis] != "L") {
      if (configGrid.position[iAxis] == "C") {
        warnings++;
        ofs << "  WARNING:\n";
        ofs << "  \"position\" received argument \"C\".\n";
        ofs << "  The option \"C\" is replaced by \"N\" for node which is a "
               "more\n";
        ofs << "  accurate description than \"C\" for center.\n";
        ofs << "  UPDATE YOUR CONFIGURATION \"C\"->\"N\". The option to avoid "
               "deprication errors.\n";
      } else {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  \"position\" can only take to arguments:\n";
        ofs << "  \"L\"(left)\n";
        ofs << "  \"N\"(node)\n";
        ofs << "  Capital letters are required!\n";
      }
    }
  }
  if (configGrid.rotateGyroFreq) {
    nlohmann::json configFile = config_file();
    if (configFile.contains("simulationTime")) {
      SimulationTimeConfig simTimeConfig{config_file()};
      if (simTimeConfig.config.gyroFreq == 0.0) {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  Using a rotating grid requires a non zero gyro frequency.";
      }
    }
  }
  ofs << "\n" << std::endl;

  ofs << "\n";
  ofs << "[Configure MPICartTopology]\n";
  ofs << "\n";
  ofs << std::setw(1) << std::setfill('\t')
      << nlohmann::json{configMPICartTopology} << "\n";
  if (configMPICartTopology.Nprocs.size() != configGrid.axes.size() or
      configMPICartTopology.periodicity.size() !=
          configMPICartTopology.Nprocs.size()) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  \"mpiCartTopology\" requires same amount of entries as axes are";
    ofs << "  given in the \"grid\" section!\n";
    ofs << "  axes.size(): " << configGrid.axes.size() << "\n";
    ofs << "  Nprocs.size(): " << configMPICartTopology.Nprocs.size() << "\n";
    ofs << "  periodicity.size(): " << configMPICartTopology.periodicity.size()
        << "\n";
  }

  int numProcesses{};
  CHECK_MPI(MPI_Comm_size(MPI_COMM_WORLD, &numProcesses));
  double Ntotal = std::accumulate(configMPICartTopology.Nprocs.begin(),
                                  configMPICartTopology.Nprocs.end(), 1.0,
                                  std::multiplies<double>());
  if (numProcesses != Ntotal) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Number of domain devisions does not match number of MPI "
           "processes";
  }

  if (Ntotal > 4) {
    warnings++;
    ofs << "  WARNING:\n";
    ofs << "  Total number of processes exceeds compute resources of the "
           "usual\n";
    ofs << "  compute nodes (Raven, Cobra, M100).\n";
    ofs << "  BSL6D does not scales well. If possible try to use only a "
           "single\n";
    ofs << "  node!\n";
  }

  for (bool period : configMPICartTopology.periodicity) {
    if (period == false) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  \"periodicity\"=false given but only true is allowed in the\n";
      ofs << "  implementation\n";
    }
  }
  ofs << "\n" << std::endl;

  for (size_t iAxis = 0; iAxis < configGrid.NxvPoints.size(); iAxis++) {
    // ToDo if axes are added to the options above use string_to_axis_type()
    if (configGrid.NxvPoints[iAxis] % configMPICartTopology.Nprocs[iAxis] !=
        0) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  Nprocs must be an integer divisor of Nxv but it is not!";
    }
  }
}

void GridDistributionConfig::help() const {
  std::cout << "Grid input parameters:\n";
  std::cout << "\n";
  std::cout << "  xvMax:          Maximum value represented on the grid\n";
  std::cout << "  xvMin:          Minimum value represented on the grid\n";
  std::cout << "  NxvPoints:      Amount of points used in this dimension.\n";
  std::cout << "  position:       Position of node within a grid cell\n";
  std::cout << "                  Options: \"L\"(Left) of \"N\"(Node)\n";
  std::cout << "                  Options: \"C\"(Center) is deprecated\n";
  std::cout << "  rotateGyroFreq: Rotate the vx-vy velocity plane with the\n";
  std::cout << "                  gyro frequency q*Bx3/m.\n";
  std::cout
      << "  Further information on the input parameters for a grid will\n";
  std::cout << "   be provided in the upcoming documentation.";
  std::cout << "\n" << std::endl;

  std::cout << "MPICartTopology input parameters:\n";
  std::cout << "\n";
  std::cout << "  Nprocs:      6-D Array with the number of processes per\n";
  std::cout << "               dimension.\n";
  std::cout << "               Default: [1,1,1,1,1,1]\n";
  std::cout << "  periodicity: Use periodic boundary conditions for\n";
  std::cout << "               cartesian MPI topology.\n";
  std::cout << "               Default: [true,true,true,true,true,true]\n";
  std::cout << "\n" << std::endl;
};

DistributionFunctionConfig::DistributionFunctionConfig(
    nlohmann::json const &jsonConfig)
    : config{jsonConfig["distributionFunction"]
                 .get<DistributionFunctionConfig::Config>()} {}
void DistributionFunctionConfig::validate(std::ofstream &ofs,
                                          size_t &warnings,
                                          size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure DistributionFunction]\n";
  ofs << "\n";
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->config}
      << "\n";
  if (config.m <= 0) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  m>0 is required but m<=0 given!\n";
  }
  ofs << "\n" << std::endl;
}
void DistributionFunctionConfig::help() const {
  std::cout << "DistributionFunction input parameters:\n";
  std::cout << "\n";
  std::cout << "  q: Charge of particles described by distribution function\n";
  std::cout << "     Default: q=1\n";
  std::cout << "  m: Mass of particles described by distribution function\n";
  std::cout << "     Default: m=1\n";
  std::cout << "\n" << std::endl;
}

SimulationTimeConfig::SimulationTimeConfig(nlohmann::json const &jsonConfig)
    : config{jsonConfig["simulationTime"].get<SimulationTimeConfig::Config>()} {
  DistributionFunctionConfig::Config f6dConf{};
  FieldSolverConfig::ConfigBackgroundField backgroundField{};

  if (jsonConfig.contains("distributionFunction")) {
    f6dConf = jsonConfig["distributionFunction"]
                  .get<DistributionFunctionConfig::Config>();
  }
  if (jsonConfig.contains("fieldSolver") &&
      jsonConfig["fieldSolver"].contains("backgroundField")) {
    backgroundField = jsonConfig["fieldSolver"]["backgroundField"]
                          .get<FieldSolverConfig::ConfigBackgroundField>();
  }
  config.gyroFreq = f6dConf.q * backgroundField.Bx3 / f6dConf.m;
}
void SimulationTimeConfig::validate(std::ofstream &ofs,
                                    size_t &warnings,
                                    size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure SimulationTime]\n";
  ofs << "\n";
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->config}
      << "\n";
  if (config.dt <= 0) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  dt<=0. dt has to be larger than zero!\n";
  }
  if (config.T < config.dt) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  T<dt. T has to be larger than or equal to dt.\n";
  }

  ofs << "\n" << std::endl;
}

void SimulationTimeConfig::help() const {
  std::cout << "SimulationTime input parameters:\n";
  std::cout << "\n";
  std::cout << "  dt:      Timestep used for a single iteration step.\n";
  std::cout << "  T:       Final time of the simulation. At t+dt>=T ist the\n";
  std::cout << "           last step\n";
  std::cout << "\n" << std::endl;
}

OperatorsConfig::OperatorsConfig(nlohmann::json const &jsonConfig)
    : configLagrangeInterpolator{
          jsonConfig["operators"]["interpolator"]
              .get<OperatorsConfig::ConfigLagrangeInterpolator>()} {};
void OperatorsConfig::validate(std::ofstream &ofs,
                               size_t &warnings,
                               size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Operators]\n";
  ofs << "\n";
  ofs << "[Configure Interpolator]\n";
  ofs << std::setw(1) << std::setfill('\t')
      << nlohmann::json{this->configLagrangeInterpolator} << "\n";
  if ((configLagrangeInterpolator.type.size() != 6) or
      (configLagrangeInterpolator.stencilWidth.size() != 6) or
      (configLagrangeInterpolator.stencilWidth.size() != 6) or
      (configLagrangeInterpolator.stencilWidth.size() != 6)) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  The number configuration values given to lagrange interpolator\n";
    ofs << "  is incorrect. Six values need to be provided if not the "
           "default\n";
    ofs << "  is used.\n";
  }

  for (size_t i = 0; i < configLagrangeInterpolator.stencilWidth.size(); i++) {
    if (configLagrangeInterpolator.type[i] != "Lag" and
        configLagrangeInterpolator.type[i] != "Trig") {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  Wrong interpolator type given. Implemented are\n";
      ofs << "  Lagrange (Lag) and Trigonometric (Trig) \n";
      ofs << "  interpolators.\n";
    }
    if (configLagrangeInterpolator.type[i] == "Lag") {
      if ((configLagrangeInterpolator.stencilWidth[i] < 3 or
           configLagrangeInterpolator.stencilWidth[i] > 12)) {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  Wrong stencil given to lagrange interpolator "
               "configuration.\n";
        ofs << "  Only stencils of width 3 to 12 are implemented.\n";
      }
      if (configLagrangeInterpolator.teamSize[i] < 0) {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  \"teamSize\" has to be larger or equal to zero.\n";
      }
      if (configLagrangeInterpolator.vectorSize[i] < 0) {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  \"vectorSize\" has to be larger or equal to zero.\n";
      }
    }
  }
  ofs << "\n" << std::endl;
};
void OperatorsConfig::help() const {
  std::cout << "Operator input parameters:\n";
  std::cout << "\n";
  std::cout << "Lagrange Interpolation:";
  std::cout
      << "  interpolatorType: Interpolator that is used in the advection\n";
  std::cout << "                    Lagrange (Lag)\n";
  std::cout << "                    Trigonometric (Trig)\n";
  std::cout << "  stencilWidth: Number of points used within the lagrange\n";
  std::cout << "                interpolation stencil\n";
  std::cout << "  teamSize:     See team_size argument of Kokkos TeamPolicy\n";
  std::cout
      << "  vectorSize:   See vector_size argument of Kokkos TeamPolicy\n";
  std::cout << "\n" << std::endl;
};

DistributionInitializationConfig::DistributionInitializationConfig(
    nlohmann::json const &jsonConfig)
    : config{jsonConfig["initializationDistributionFunction"]
                 .get<DistributionInitializationConfig::Config>()} {
  if (jsonConfig.contains("rightHandSide")) {
    bsl6d::RightHandSideConfig rhsConf{jsonConfig};
    if (rhsConf.config.gradientType == "nonlinear")
      this->config.initFunction = "zero";
  }
};
void DistributionInitializationConfig::validate(std::ofstream &ofs,
                                                size_t &warnings,
                                                size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure DistributionInitialization]\n";
  ofs << "\n";
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->config}
      << "\n";
  if (not bsl6d::Impl::available_init_keyword(config.initFunction) and
      not(config.initFunction == "external")) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Unkown function type for initialization given.\n";
  }
  bsl6d::RightHandSideConfig::Config rhsConf{
      config_file()["rightHandSide"].get<bsl6d::RightHandSideConfig::Config>()};

  if (rhsConf.gradientType == "nonlinear") {
    ofs << "  WARNING:\n";
    ofs << "  'initializationFunction','type' is set to 'zero' internally\n";
    ofs << "  because f6d is initialized over time using a source term\n!";
    warnings++;
  }

  if (std::abs(config.alpha) > 1.0) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Alpha is perturbation, needs to be |alpha|<1, alpha="
        << config.alpha << "\n";
  }
  ofs << "\n" << std::endl;
};

void DistributionInitializationConfig::help() const {
  std::cout << "DistributionInitialization input parameters:\n";
  std::cout << "  1. planewave_dens_perturbation:\n";
  std::cout << "     (1 + "
               "alpha*sin(mk*2pi/L*(x-x0))*f_Maxwellian(v-v0,temperature)\n";
  std::cout << "  2. rand_dens_perturbation:\n";
  std::cout << "     (1+alpha*rand(x))*f_Maxwellian(v-v0,temperature)\n";
  std::cout << "  3. gauss:\n";
  std::cout
      << "     f_Maxwellian(x-x0,sigma2)*f_Maxwellian(v-v0,temperature))\n";
  std::cout << "  4. polynom:\n";
  std::cout
      << "     ((x1-xv0[0])^polynomOrder[0] + (x2-xv0[1])^polynomOrder[1] + "
         "... + (v3-xv0[5])^polynomOrder[5]\n";
  std::cout << "\n" << std::endl;
};

RightHandSideConfig::RightHandSideConfig(nlohmann::json const &jsonConfig)
    : config{jsonConfig["rightHandSide"].get<RightHandSideConfig::Config>()} {};

void RightHandSideConfig::validate(std::ofstream &ofs,
                                   size_t &warnings,
                                   size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Gradients and other RHSs]\n";
  if (config.gradientType != "boussinesq" and
      config.gradientType != "nonlinear" and config.gradientType != "none") {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Unknown gradientType given.\n";
  }

  if (std::abs(config.kappaN) > 1.0 or std::abs(config.kappaT) > 1.0) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Gradient values in `rightHandSide` are to large.\n";
    ofs << "  Requirement abs(kappaN)<1 and abs(kappaT)<1\n";
  }

  ofs << "\n";
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->config}
      << "\n";

  ofs << "\n" << std::endl;
};

void RightHandSideConfig::help() const {
  std::cout << "Right hand side input parameters:\n";
  std::cout << "kappaT : Temperature gradient in x-direction \n";
  std::cout << "kappaN : Density gradient in x-direction \n";
  std::cout << "gradientType : Type of temperature gradient \n";
  std::cout << "    1. boussinesq - local gradients (df/dt = kappa_T (m v^2/2 "
               "- 3/2) f_M) \n";
  std::cout << "    2. nonlinear  - nonlinear treatment of gradients \n";
  std::cout << "\n" << std::endl;
};

FieldSolverConfig::FieldSolverConfig(nlohmann::json const &jsonConfig) {
  nlohmann::json fieldSolver = jsonConfig.at("fieldSolver");
  if (fieldSolver.size() > 2) {
    throw std::runtime_error(
        "Competing field solvers have been encountered. Only backgroundField "
        "can be used together with one other field solver.");
  } else if (fieldSolver.size() == 2 and
             (not fieldSolver.contains("backgroundField"))) {
    throw std::runtime_error(
        "If two field solver configurations are given to the configuration "
        "file one of them has to be the solver 'background'.");
  } else {
    for (auto &[key, value] : fieldSolver.items()) {
      if ((key != "backgroundField") or fieldSolver.size() == 1) {
        m_type = key;
      }
    }
  }
  if (jsonConfig["fieldSolver"].contains("backgroundField")) {
    configBgField = jsonConfig["fieldSolver"]["backgroundField"]
                        .get<FieldSolverConfig::ConfigBackgroundField>();
  }
  if (jsonConfig["fieldSolver"].contains("quasiNeutral")) {
    configQN = jsonConfig["fieldSolver"]["quasiNeutral"];
  }
};

void FieldSolverConfig::validate(std::ofstream &ofs,
                                 size_t &warnings,
                                 size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Field Solver]\n";
  ofs << "\n";
  ofs << "[Configure Background Field]\n";
  ofs << std::setw(1) << std::setfill('\t')
      << nlohmann::json{this->configBgField} << "\n";

  // These keywords are not meaningful enough
  if (configBgField.type != "randomNoiseResponse" and
      configBgField.type != "randomNoisePush" and
      configBgField.type != "cos" and configBgField.type != "const") {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "  Unknown function type for initialization given.\n";
  }
  ofs << "\n";

  ofs << "[Configure Quasi Neutral]\n";
  ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{this->configQN}
      << "\n";
  bsl6d::RightHandSideConfig::Config rhsConf{
      config_file()["rightHandSide"].get<bsl6d::RightHandSideConfig::Config>()};
  if (rhsConf.gradientType == "nonlinear" &&
      configQN.substractFluxSurfaceAverage == false) {
    errors++;
    ofs << "  ERROR:\n";
    ofs << "substractFluxSurfaceAverage has to be set to true\n";
    ofs << "because grdientType === nonlinear\n";
  }
  ofs << "\n" << std::endl;
};

void FieldSolverConfig::help() const {
  std::cout << "FieldSolver input parameters:\n";
  std::cout << "\n";
  std::cout << "BackgroundFieldSolver:\n";
  std::cout << "  type: -\"randomNoiseResponse\"\n";
  std::cout << "          alpha * erfc(-slope * t - offset) * rand(x)\n";
  std::cout << "        -\"randomNoisePush\"\n";
  std::cout << "          alpha * e^[-gaussSD*(t - offset)^2] * sinc(sincSD*(t "
               "- offset)) * rand(x)\n";
  std::cout << "        -\"cos\"\n";
  std::cout << "          alpha * cos(k*x)\n";
  std::cout << "        -\"const\"\n";
  std::cout << "          Ex = alpha; Ey = 0; Ez = 0\n";
  std::cout << "  alpha: Amplitude of the distribution.\n";
  std::cout << "  erSlope: Slope of the error function.\n";
  std::cout << "  gaussianSD: Standard deviation of the Gaussian function.\n";
  std::cout << "  sincSD: Standard deviation of the Sinc function.\n";
  std::cout << "\n" << std::endl;
  std::cout << "QuasiNeutral:\n";
  std::cout << "  modelType: - noB\n";
  std::cout << "             - rotatingGridB\n";
  std::cout << "  constFluxSurfaceAverage: bool maintain a constant "
               "flux surface average\n";
  std::cout << "\n";
  std::cout << "\n" << std::endl;
};

IntegratorConfig::IntegratorConfig(nlohmann::json const &jsonConfig) {
  nlohmann::json configTemp{};
  // ToDo replace by a .contains("integrator") call.
  if (jsonConfig["integrator"].empty()) {
    configTemp = R"([
            {"token": "advectV", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "startLoop", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "rightHandSide", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
            {"token": "solveMaxwellFields","t0Offset": 0.0,"dtRatio": 0.0},
            {"token": "rightHandSide", "t0Offset": 0.5, "dtRatio": 0.5},
            {"token": "advectV","t0Offset": 0.5,"dtRatio": 1.0},
            {"token": "advanceTbydt","t0Offset": 0.0,"dtRatio": 0.0},
            {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "endLoop", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "rightHandSide", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
            {"token": "solveMaxwellFields","t0Offset": 0.0,"dtRatio": 0.0},
            {"token": "rightHandSide", "t0Offset": 0.5, "dtRatio": 0.5},
            {"token": "advectV", "t0Offset": 0.5, "dtRatio": 0.5}
          ])"_json;
  } else {
    configTemp = jsonConfig["integrator"];
  }
  for (auto integratorStep : configTemp) {
    IntegratorStepConfig step{
        integratorStep.get<IntegratorConfig::IntegratorStepConfig>()};
    integratorSteps.push_back(step);
  }
};

void IntegratorConfig::validate(std::ofstream &ofs,
                                size_t &warnings,
                                size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Integrator]\n";
  ofs << "\n";
  std::string solver{};
  bool diagnosticWasSet{false};
  std::map<std::string, bool> tokenWasSet{
      {"advanceTbydt", false}, {"startLoop", false}, {"stopLoop", false}};

  for (auto step : integratorSteps) {
    ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{step} << "\n";
    if (not Integrator::token_exists(step.token)) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  The token " + step.token + " given to integrator\n";
      ofs << "  does not exists. Check help message for existing \n";
      ofs << "  tokens.\n";
    }

    if (tokenWasSet.find(step.token) != std::end(tokenWasSet)) {
      if (tokenWasSet[step.token]) {
        errors++;
        ofs << "  ERROR:\n";
        ofs << "  The token " + step.token + " can only be set once.\n";
      }
      tokenWasSet[step.token] = true;
    }

    if (step.token == "solveMaxwellFields") {
      // No exceptions are necessary at the moment
    }
    if (step.token.find("advect") != std::string::npos) {
      // No exceptions are necessary at the moment
    }
    ofs << "\n" << std::endl;
  }
  if (integratorSteps.size() > 0) {
    if (not tokenWasSet["advanceTbydt"]) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  No token to advance the simulation time has been given.\n";
    }
    if (not diagnosticWasSet) {
      warnings++;
      ofs << "  WARNING:\n";
      ofs << "  No token for the diagnostic output has been given.\n";
    }
  }
  ofs << "\n" << std::endl;
};

void IntegratorConfig::help() const {
  std::cout << "Integrator Input Parameters:\n";
  std::cout << "  advectX,advectV:  Advect spatial or velocity domain\n";
  std::cout
      << "                    by dt*dtRatio starting from t0+t0Offset*dt\n";
  std::cout
      << "                    where dt is the timestep from simulationTime\n";
  std::cout
      << "                    and t0 simulation time in the advected step.\n";
  std::cout
      << "  solveQN, solveVP: Solving the quasi neutral or poisson field \n";
  std::cout
      << "                    equation always including the background field\n";
  std::cout << "  solveOnlyBG:      Calculate maxwell fields only based on the "
               "background\n";
  std::cout << "                    field\n.";
  std::cout << "  advanceTbydt:     t0 = t0 + dt\n";
  std::cout << "  diagnostic:       Output diagnostics\n";
  std::cout << "  rhs:              Solve the right hand side.\n";
}

DiagnosticConfig::DiagnosticConfig(const nlohmann::json &jsonConfig)
    : diagOutConf{} {
  for (auto singleDiagnosticConfig : jsonConfig["diagnostics"]) {
    ConfigDiagnosticOutput diagnosticStruct =
        singleDiagnosticConfig.get<DiagnosticConfig::ConfigDiagnosticOutput>();
    diagOutConf.push_back(diagnosticStruct);
  }
}

void DiagnosticConfig::validate(std::ofstream &ofs,
                                size_t &warnings,
                                size_t &errors) const {
  ofs << "\n";
  ofs << "[Configure Diagnostics]\n";
  ofs << "\n";

  for (auto conf : diagOutConf) {
    ofs << std::setw(1) << std::setfill('\t') << nlohmann::json{conf} << "\n";
    auto found = std::find(availableDiagnostics.begin(),
                           availableDiagnostics.end(), conf.diagnostic);
    if (found == availableDiagnostics.end()) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  Unknown diagnostic given in config file: " << conf.diagnostic
          << "!\n";
    }
    if (conf.iteration <= 0) {
      errors++;
      ofs << "  ERROR:\n";
      ofs << "  The output frequency of " << conf.diagnostic
          << " has to be larger than zero!\n";
    }
  }
}

void DiagnosticConfig::help() const {
  std::cout << "Diagnostic input parameters:\n";
  std::cout << "  diagnostic: Type of diagnostic to output.\n";
  std::cout << "              Available:\n";
  for (auto keywords : availableDiagnostics) {
    std::cout << "                          - " << keywords << "\n";
  }
  std::cout << "  iteration: Steps between two outputs of the corresponding\n";
  std::cout << "             diagnostic. (default: 10)\n";
  std::cout << "\n" << std::endl;
}

void print_config_help(const std::string section) {
  nlohmann::json configFile =
      get_test_case_config_file("itg_non_linear_gradients");
  bsl6d::Impl::validate_and_complete_config_file_structure(configFile);

  const std::string green_start = "\033[1;32m";
  const std::string color_reset = "\033[0m";

  std::map<std::string, std::function<void()>> help_functions = {
      {"GridDistribution",
       [&]() { GridDistributionConfig{configFile}.help(); }},
      {"DistributionFunction",
       [&]() { DistributionFunctionConfig{configFile}.help(); }},
      {"SimulationTime", [&]() { SimulationTimeConfig{configFile}.help(); }},
      {"Operators", [&]() { OperatorsConfig{configFile}.help(); }},
      {"DistributionInitialization",
       [&]() { DistributionInitializationConfig{configFile}.help(); }},
      {"RightHandSide", [&]() { RightHandSideConfig{configFile}.help(); }},
      {"FieldSolver", [&]() { FieldSolverConfig{configFile}.help(); }},
      {"Diagnostic", [&]() { DiagnosticConfig{configFile}.help(); }},
      {"Integrator", [&]() { IntegratorConfig{configFile}.help(); }}};

  if (section.empty()) {
    for (const auto &[name, func] : help_functions) {
      std::cout << green_start << name << " Config Help" << color_reset
                << std::endl;
      func();
    }
  } else {
    auto it = help_functions.find(section);
    if (it != help_functions.end()) {
      std::cout << green_start << section << " Config Help" << color_reset
                << std::endl;  // Call the corresponding help function
      help_functions[section]();

    } else {
      std::cout << "Error: Unknown configuration section: " << section
                << std::endl;
      std::cout << "Available sections are:" << std::endl;
      for (const auto &[name, _] : help_functions) {
        std::cout << " - " << name << std::endl;
      }
    }
  }
}

namespace Impl {
void validate_file_data(nlohmann::json const &configFile, std::ofstream &ofs) {
  // @todo Can this be automated by iterating on the nodes of the json file,
  //       creating a specific object and finally validating this object?
  size_t err{};
  size_t warn{};
  GridDistributionConfig{configFile}.validate(ofs, warn, err);
  SimulationTimeConfig{configFile}.validate(ofs, warn, err);
  DistributionFunctionConfig{configFile}.validate(ofs, warn, err);
  DiagnosticConfig{configFile}.validate(ofs, warn, err);
  OperatorsConfig{configFile}.validate(ofs, warn, err);
  DistributionInitializationConfig{configFile}.validate(ofs, warn, err);
  FieldSolverConfig{configFile}.validate(ofs, warn, err);
  RightHandSideConfig{configFile}.validate(ofs, warn, err);
  IntegratorConfig{configFile}.validate(ofs, warn, err);

  if (warn > 0) {
    ofs << warn << " warnings have been found in the provided config file.\n";
  }
  if (err > 0) {
    ofs << err << " errors have been found in the provided config file.\n"
        << std::endl;
    throw std::runtime_error(
        "Errors in the configuration occurred!\n"
        "Further information in simulation output file.");
  }
}
}  // namespace Impl
}  // namespace bsl6d

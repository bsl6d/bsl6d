#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <list>
#include <string>

#include <nlohmann/json.hpp>

namespace bsl6d {

class GridDistributionConfig {
  public:
  GridDistributionConfig();
  GridDistributionConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  template <size_t xDim, size_t vDim>
  friend class Grid;
  template <size_t dim>
  friend class CartTopology;
  friend int main(int argc, char **argv);
  struct ConfigGrid {
    std::vector<std::string> axes{"x1", "x2", "x3", "v1", "v2", "v3"};
    std::vector<double> xvMin{0.0, 0.0, 0.0, -2.0, -2.5, -3.0};
    std::vector<double> xvMax{1.5, 2.0, 3.0, 2.0, 2.5, 3.0};
    std::vector<size_t> NxvPoints{10, 12, 14, 15, 17, 19};
    std::vector<std::string> position{"L", "L", "L", "N", "N", "N"};
    bool rotateGyroFreq{false};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(ConfigGrid,
                                              axes,
                                              xvMax,
                                              xvMin,
                                              NxvPoints,
                                              position,
                                              rotateGyroFreq);
  ConfigGrid configGrid{};

  private:
  struct ConfigMPICartTopology {
    std::vector<int> Nprocs{};
    std::vector<bool> periodicity{};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(ConfigMPICartTopology,
                                              Nprocs,
                                              periodicity);

  ConfigMPICartTopology configMPICartTopology{};
};

class DistributionFunctionConfig {
  public:
  DistributionFunctionConfig() = default;
  DistributionFunctionConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  private:
  friend class DistributionFunction;
  friend class GridDistributionConfig;
  friend class SimulationTimeConfig;
  template <size_t xDim, size_t vDim>
  friend class Grid;
  friend class Sources;

  struct Config {
    double q{1.0};
    double m{1.0};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(Config, q, m);
  Config const config{};
};

class SimulationTimeConfig {
  public:
  SimulationTimeConfig() = default;
  SimulationTimeConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  friend class SimulationTime;
  struct Config {
    double dt{0.1};
    double T{1};
    double gyroFreq{};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(Config, dt, T);
  Config config{};
};

class OperatorsConfig {
  public:
  OperatorsConfig() = default;
  OperatorsConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  private:
  friend class LagrangeBuilder;
  struct ConfigLagrangeInterpolator {
    std::vector<std::string> type{"Lag", "Lag", "Lag", "Lag", "Lag", "Lag"};
    std::vector<int> stencilWidth{8, 8, 8, 7, 7, 7};
    std::vector<int> teamSize{0, 0, 0, 0, 0, 0};
    std::vector<int> vectorSize{0, 0, 0, 0, 0, 0};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(ConfigLagrangeInterpolator,
                                              type,
                                              stencilWidth,
                                              teamSize,
                                              vectorSize);
  ConfigLagrangeInterpolator const configLagrangeInterpolator{};
};

class DistributionInitializationConfig {
  public:
  DistributionInitializationConfig() = default;
  DistributionInitializationConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  // Has to be public to be captured within a __host__ __device__ LAMBDA
  struct Config {
    std::string initFunction{};
    std::array<double, 6> xv0{};
    std::array<double, 3> sigma2{};
    double temperature{1.0};
    std::array<double, 3> mk{};
    double alpha{};
    size_t seed{};
    std::array<size_t, 6> polynomOrder{};
  };

  friend class Sources;
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(Config,
                                              initFunction,
                                              xv0,
                                              sigma2,
                                              temperature,
                                              mk,
                                              alpha,
                                              seed,
                                              polynomOrder);
  Config config{};
};

class RightHandSideConfig {
  public:
  RightHandSideConfig() = default;
  RightHandSideConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  // Has to be public to be captured within a __host__ __device__ LAMBDA
  struct Config {
    double kappaN{0.0};
    double kappaT{0.0};
    std::string gradientType{"none"};
    double rampTime{30.0};
    double rampSTD{3.0};
  };

  friend class Sources;
  friend class QuasiNeutral;
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(Config,
                                              kappaN,
                                              kappaT,
                                              gradientType);
  Config const config{};
};

class FieldSolverConfig {
  public:
  FieldSolverConfig() = default;
  FieldSolverConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  // Has to be public to be captured within a __host__ __device__ LAMBDA
  struct ConfigBackgroundField {
    double Bx3{};
    std::string type{"const"};
    double alpha{};
    std::array<double, 3> k{0.0, 0.0, 0.0};
    double timeOffset{};
    double erSlope{};
    double gaussianSD{};
    double sincSD{};
  };
  struct ConfigQuasiNeutral {
    bool constFluxSurfaceAverage{false};
    bool substractFluxSurfaceAverage{false};
  };

  std::string type() { return m_type; };

  private:
  std::string m_type{"None"};
  friend class FieldSolver;

  friend class GridDistributionConfig;
  template <size_t xDim, size_t vDim>
  friend class Grid;
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(ConfigBackgroundField,
                                              Bx3,
                                              type,
                                              alpha,
                                              k,
                                              timeOffset,
                                              erSlope,
                                              gaussianSD,
                                              sincSD);
  ConfigBackgroundField configBgField{};

  friend class QuasiNeutral;
  NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(ConfigQuasiNeutral,
                                              constFluxSurfaceAverage,
                                              substractFluxSurfaceAverage);
  ConfigQuasiNeutral configQN{};
};

class DiagnosticConfig {
  public:
  DiagnosticConfig() = default;
  DiagnosticConfig(nlohmann::json const &);
  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  friend class Diagnostic;
  struct ConfigDiagnosticOutput {
    std::string diagnostic{};
    size_t iteration{10};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE(ConfigDiagnosticOutput, diagnostic, iteration);
  // ToDo Should be const. Ensure that this can not be changed
  //  outside the class by making it const.
  std::list<ConfigDiagnosticOutput> diagOutConf{};

  private:
  // ToDo fill this vector through the keys of the map in diagnositcs
  std::vector<std::string> availableDiagnostics{"f6d",
                                                "n",
                                                "particleFlux",
                                                "kineticEnergy",
                                                "secondMoment",
                                                "reducedFourthMoment",
                                                "energyFlux",
                                                "labframeParticleFlux",
                                                "labframeEnergyFlux",
                                                "phi",
                                                "E"};
};

class IntegratorConfig {
  public:
  IntegratorConfig() = delete;
  IntegratorConfig(nlohmann::json const &);

  void validate(std::ofstream &ofs, size_t &warnings, size_t &errors) const;
  void help() const;

  private:
  friend class Integrator;
  struct IntegratorStepConfig {
    std::string token{};
    double t0Offset{};
    double dtRatio{};
  };
  NLOHMANN_DEFINE_TYPE_INTRUSIVE(IntegratorStepConfig,
                                 token,
                                 t0Offset,
                                 dtRatio);
  std::list<IntegratorStepConfig> integratorSteps{};
};

void print_config_help(std::string section);

namespace Impl {
void validate_file_data(nlohmann::json const &configFile, std::ofstream &ofs);
}  // namespace Impl

} /* namespace bsl6d */

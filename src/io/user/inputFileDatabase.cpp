#include <filesystem>
#include <fstream>
#include <map>
#include <string>

#include <io_init.hpp>

#include <inputFileDatabase.hpp>

namespace bsl6d {
std::map<std::string, nlohmann::json> const get_test_case_config_map() {
  std::map<std::string, nlohmann::json> examples{};
  nlohmann::json config{};
  {
    config =
        R"({
          "description" : "Simulation of ion Bernstein waves reproducing the analytical dispersion relation",
          "distributionFunction": {
            "q": 1.0,
            "m": 1.0
          },
          "simulationTime": {
            "dt": 0.019,
            "T": 0.10,
            "restart": false
          },
          "initializationDistributionFunction": {
            "initFunction": "ibw_perturbation",
            "mk": [15,15,0],
            "polynomOrder": [0,0,0,5,5,0],
            "alpha": 0.0001,
            "temperature": 1.0
          },
          "gridDistribution": {
            "grid":{
              "axes": ["x1","x2","v1","v2"],
              "NxvPoints": [32, 32, 33, 33],
              "position": ["L", "L", "N", "N"],
              "rotateGyroFreq": true
            }
          },
          "fieldSolver": {
            "quasiNeutral": {
              "constFluxSurfaceAverage": true
            },
            "backgroundField": {
              "Bx3": 1.0
            }
          },
          "integrator": [
            {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "solveMaxwellFields", "t0Offset": 0.0, "dtRatio": 0.0},
            {"token": "advectV", "t0Offset": 0.0, "dtRatio": 0.5},
            {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
            {"token": "advectVAdj", "t0Offset": 0.5, "dtRatio": 0.5},
            {"token": "advanceTbydt", "t0Offset": 0.0, "dtRatio": 0.0}
          ],
          "operators": {
            "interpolator": {
              "stencilWidth": [10,10,10,10,10,10]
            }
          },
          "diagnostics": [
            {
              "diagnostic": "f6d",
              "iteration": 1000
            },  
            {   
              "diagnostic": "n",
              "iteration": 1
            },
            {
              "diagnostic": "phi",
              "iteration": 1
            }
          ]
        })"_json;
    std::vector<double> xvMax{20.0 / 3.0 * M_PI, 20.0 / 3.0 * M_PI, 6.0, 6.0};
    std::vector<double> xvMin{0.0, 0.0, -xvMax[2], -xvMax[3]};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["ion_bernstein_wave"]              = config;
  }
  {
    config =
        R"({"description" : "Simulation ion temperature gradient (ITG) driven turbulence using the Boussinesq approximation",
            "distributionFunction": {
              "q": 1.0,
              "m": 1.0 
            },  
            "simulationTime": {
              "dt": 0.05,
              "T": 2.0,
              "restart": false
            },  
            "gridDistribution" : { 
              "grid": {
                "axes": ["x2","x3","v1","v2","v3"],
                "NxvPoints": [16,16,17,17,17],
                "position": ["L","L", "N", "N","N"],
                "rotateGyroFreq": true
              },  
              "parallel": {
                "Nprocs": [1, 1, 1, 1, 1]
              }   
            },  
            "operators": {
              "lagrangeInterpolator": {
                "stencilWidth": [7,7,7,7,7,7],
                "teamSize": [0,0,0,0,0,0],
                "vectorSize": [0,0,0,0,0,0]
              }   
            },  
            "initializationDistributionFunction":{
              "initFunction": "rand_dens_perturbation",
              "alpha": 0.0000001,
              "mk": [1,1,1],
              "temperature": 1.0 
            },   
            "fieldSolver": {
              "backgroundField": {
                "Bx3": 1.0
              }
            },
            "rightHandSide": {
              "kappaT": 0.1,
              "kappaN": 0.0,
              "gradientType": "boussinesq"
            },
            "diagnostics": [
              {   
                "diagnostic": "f6d",
                "iteration": 1000
              },  
              {   
                "diagnostic": "n",
                "iteration": 10
              }   
            ]
          })"_json;
    std::vector<double> xvMax{M_PI / 0.4, 240 * M_PI, 4.0, 4.0, 4.0};
    std::vector<double> xvMin{0.0, 0.0, -xvMax[2], -xvMax[3], -xvMax[4]};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["slab_itg_boussinesq"]             = config;
  }
  {
    config =
        R"({"description" : "Simulation of ion sound wave with quasi-neutral electrons in parallel direction",
            "distributionFunction": {
              "q": 1.0,
              "m": 1.0
            },
            "simulationTime": {
              "dt": 0.001,
              "T": 1.0,
              "restart": false
            },
            "gridDistribution" : { 
               "grid": {
                 "axes": ["x1","v1"],
                 "NxvPoints": [32,127],
                 "position": ["L", "N"],
                 "rotateGyroFreq": false
               }
            },  
            "rightHandSide": {
              "gradientType": "none"
            },
            "fieldSolver": {
              "quasiNeutral":{}
            },
            "integrator": [
              {"token": "advectV","t0Offset": 0.0,"dtRatio": 0.5},
              {"token": "advectX", "t0Offset": 0.0, "dtRatio": 1.0},
              {"token": "solveMaxwellFields","t0Offset": 0.0,"dtRatio": 0.0},
              {"token": "advectV","t0Offset": 0.5,"dtRatio": 0.5},
              {"token": "advanceTbydt","t0Offset": 0.0,"dtRatio": 0.0},
              {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.0}
            ],
            "operators": {
              "lagrangeInterpolator": {
                "stencilWidth": [8,8,8,8,8,8]
              }
            },
            "initializationDistributionFunction":{
              "initFunction": "rand_dens_perturbation",
              "alpha": 0.000001
            },
            "diagnostics": [
              {   
                "diagnostic": "f6d",
                "iteration": 1000
              },  
              {   
                "diagnostic": "n",
                "iteration": 1
              }   
            ]
          })"_json;
    std::vector<double> xvMax{2.0 * M_PI, 8.0};
    std::vector<double> xvMin{0.0, -xvMax[1]};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["ion_sound_wave"]                  = config;
  }
  {
    config =
        R"({"description" : "Simulation of drift-kinetic equation in a 2D configuration",
            "distributionFunction": {
              "q": 1.0,
              "m": 1.0
            },
            "gridDistribution" : {
              "grid": {
                "axes": ["x1","x2"],
                "NxvPoints": [64, 64],
                "position": ["L", "L"],
                "rotateGyroFreq": true
              }
            },
            "fieldSolver": {
              "backgroundField": {
                "Bx3": 1.0, 
                "type": "cos",
                "alpha" : 0.1,
                "k" : [0.0,1.0,0.0]
              }
            },
            "operators": {
              "interpolator": {
                "stencilWidth": [8,8,8,8,8,8]
              }
            },
            "initializationDistributionFunction":{
              "initFunction": "planewave_dens_perturbation",
              "alpha": 0.01,
              "mk": [1,0,0],
              "temperature": 1.0
            },
            "simulationTime": {
              "dt": 0.05,
              "T": 10.0,
              "restart": false
            },
            "integrator": [
                  {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.5},
                  {"token": "startLoop", "t0Offset": 0.0, "dtRatio": 0.5},
                  {"token": "solveMaxwellFields","t0Offset": 0.0,"dtRatio": 0.0},
                  {"token": "advectXDrift","t0Offset": 0.0,"dtRatio": 1.0},
                  {"token": "advanceTbydt","t0Offset": 0.0,"dtRatio": 0.0},
                  {"token": "diagnostic", "t0Offset": 0.0, "dtRatio": 0.5}
                ],
            "diagnostics": [
              {   
                "diagnostic": "f6d",
                "iteration": 1
              }
            ]
          })"_json;
    std::vector<double> xvMax{2.0 * M_PI, 2.0 * M_PI};
    std::vector<double> xvMin{0.0, 0.0};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["2D_drift_kinetic"]                = config;
  }
  {
    config =
        R"({"description" : "Simulation of ITG instablity with nonlinear gradients",
            "distributionFunction": {
              "m": 1.0,
              "q": 1.0
            },
            "simulationTime": {
              "T": 10000,
              "dt": 0.02
            },
            "fieldSolver": {
              "backgroundField": {
                "Bx3": 1.0
              },  
              "quasiNeutral": {
                "substractFluxSurfaceAverage": true
              }
            },
            "gridDistribution": {
              "grid": {
                "NxvPoints": [128, 32, 16, 33, 33, 33],
                "axes": ["x1", "x2", "x3", "v1", "v2", "v3"],
                "position": ["L", "L", "L", "N", "N", "N"],
                "rotateGyroFreq": true
              },
              "parallel": {
                "Nprocs": [4, 1, 1, 1, 1, 1]
              }
            },
            "initializationDistributionFunction": {
              "alpha": 0.0000001,
              "initFunction": "rand_dens_perturbation",
              "temperature": 1.0
            },
            "operators": {
              "interpolator": {
                "stencilWidth": [8,8,8,7,7,7]
              }
            },
            "rightHandSide": {
              "gradientType": "nonlinear",
              "kappaN": 0.0,
              "kappaT": 0.5
            },
            "diagnostics": [
              {
                "diagnostic": "f6d",
                "iteration": 1000
              },
              {
                "diagnostic": "n",
                "iteration": 10
              }
            ]
            })"_json;
    std::vector<double> xvMax{10.0 * M_PI, 2.5 * M_PI, 240.0 * M_PI,
                              4.0,         4.0,        4.0};
    std::vector<double> xvMin{0.0, 0.0, 0.0, -xvMax[3], -xvMax[4], -xvMax[5]};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["itg_non_linear_gradients"]        = config;
  }
  {
    config =
        R"({"description" : "Simulation of IBW instablity with nonlinear gradients",
            "distributionFunction": {
              "m": 1.0,
              "q": 1.0
            },
            "fieldSolver": {
              "backgroundField": {
                "Bx3": 1.0
              },  
              "quasiNeutral": {
                "substractFluxSurfaceAverage": true
              }
            },
            "gridDistribution": {
              "grid": {
                "NxvPoints": [64, 32, 16, 33, 33, 17],
                "axes": ["x1", "x2", "x3", "v1", "v2", "v3"],
                "position": ["L", "L", "L", "N", "N", "N"],
                "rotateGyroFreq": true
              },
              "parallel": {
                "Nprocs": [4, 1, 1, 1, 1, 1]
              }
            },
            "initializationDistributionFunction": {
              "alpha": 0.000001,
              "initFunction": "rand_dens_perturbation"
            },
            "operators": {
              "interpolator": {
                "stencilWidth": [8,8,8,7,7,7]
              }
            },
            "rightHandSide": {
              "gradientType": "nonlinear",
              "kappaN": 0.49,
              "kappaT": 0.6
            },
            "simulationTime": {
              "T": 10.000,
              "dt": 0.008
            },
            "diagnostics": [
              {
                "diagnostic": "f6d",
                "iteration": 1000
              },
              {
                "diagnostic": "n",
                "iteration": 10
              }
            ]
            })"_json;
    std::vector<double> xvMax{4 * M_PI, 2.0 * M_PI, 80.0 * M_PI, 4.0, 4.0, 4.0};
    std::vector<double> xvMin{0.0, 0.0, 0.0, -xvMax[3], -xvMax[4], -xvMax[5]};
    config["gridDistribution"]["grid"]["xvMax"] = xvMax;
    config["gridDistribution"]["grid"]["xvMin"] = xvMin;
    examples["ibw_non_linear_gradients"]        = config;
  }
  return examples;
};

nlohmann::json get_test_case_config_file(std::string const& testCase) {
  auto testCases = get_test_case_config_map();
  if (testCases.find(testCase) == testCases.end()) {
    throw std::runtime_error("BSL6D Error: Unknown test case given to --test " +
                             testCase);
  }
  return testCases[testCase];
}

void create_test_case_config_file(std::string const& testCase) {
  nlohmann::json config = get_test_case_config_file(testCase);
  std::filesystem::remove(cmd::input_file());
  std::ofstream ofs{cmd::input_file()};
  ofs << std::setw(1) << std::setfill('\t') << config << std::flush;
}

std::string create_config_file_help_message() {
  auto testCases = get_test_case_config_map();
  std::ostringstream oss;

  size_t maxTestCaseLength    = 0;
  size_t maxDescriptionLength = 0;

  for (const auto& [key, value] : testCases) {
    maxTestCaseLength       = std::max(maxTestCaseLength, key.size());
    std::string explanation = value.at("description");
    maxDescriptionLength = std::max(maxDescriptionLength, explanation.size());
  }

  oss << std::left << std::setw(maxTestCaseLength + 4) << "Test Case"
      << "Description \n ";

  oss << std::left
      << std::string(maxTestCaseLength + maxDescriptionLength + 4, '-') << '\n';

  for (const auto& [key, value] : testCases) {
    std::string explanation = value.at("description");

    oss << std::left << std::setw(maxTestCaseLength + 4) << key << std::left
        << std::setw(maxDescriptionLength) << explanation << "\n";
  }

  return oss.str();
}

}  // namespace bsl6d

#include <filesystem>

#include <gtest/gtest.h>

#include "../config.hpp"
#include "../io_init.hpp"

TEST(IOUserConfig, JsonParseGridDistributionConfig) {
  nlohmann::json gridJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  ASSERT_NO_THROW(bsl6d::GridDistributionConfig{gridJson});

  nlohmann::json jsonConfig = R"(
  {
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  bsl6d::GridDistributionConfig wrongNumberOfValues{jsonConfig};
  size_t err{};
  size_t warn{};
  std::ofstream ofs{"ConfigGridDistributionTestOutput.out"};
  wrongNumberOfValues.validate(ofs, warn, err);
  ASSERT_EQ(err, 1);

  jsonConfig = R"(
  {
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, -4.0, -4.0, -4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  bsl6d::GridDistributionConfig wrongXVMin{jsonConfig};
  err = 0;
  wrongXVMin.validate(ofs, warn, err);
  ASSERT_EQ(err, 3);

  jsonConfig = R"(
  {
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [0, 0, 0, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  bsl6d::GridDistributionConfig wrongNxvPoints{jsonConfig};
  err = 0;
  wrongNxvPoints.validate(ofs, warn, err);
  ASSERT_EQ(err, 3);

  jsonConfig = R"(
  {
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["V", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  bsl6d::GridDistributionConfig wrongPosition{jsonConfig};
  err = 0;
  wrongPosition.validate(ofs, warn, err);
  ASSERT_EQ(err, 1);

  nlohmann::json mpiCartTopologyJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 1, 1, 1, 1],
      "periodicity": [true,true,true,true,true,true]
    }
  }
  })"_json;
  ASSERT_NO_THROW(bsl6d::GridDistributionConfig{mpiCartTopologyJson});

  // The missing value creates an undefined behavior in Nvx%Nprocs
  // Therefore just expect an error larger than zero
  mpiCartTopologyJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 1, 1, 1]
    }
  }
  })"_json;
  // bsl6d::GridDistributionConfig wrongAmountOfValues{mpiCartTopologyJson};
  // err  = 0;
  // warn = 0;
  // EXPECT_ANY_THROW(wrongAmountOfValues.validate(ofs, warn, err));
  // EXPECT_GT(err, 0);

  mpiCartTopologyJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 1, 1, 5, 2]
    }
  }
  })"_json;
  bsl6d::GridDistributionConfig warnToMuchParallel{mpiCartTopologyJson};
  err  = 0;
  warn = 0;
  warnToMuchParallel.validate(ofs, warn, err);
  EXPECT_EQ(warn, 1);

  mpiCartTopologyJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 1, 1, 1, 1],
      "periodicity": [false, true, true, true, true, true]
    }
  }
  })"_json;
  bsl6d::GridDistributionConfig wrongPeriodicity{mpiCartTopologyJson};
  err  = 0;
  warn = 0;
  wrongPeriodicity.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  mpiCartTopologyJson =
      R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
      "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
      "NxvPoints": [32, 32, 32, 33, 33, 33],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 1, 1, 2, 2]
    }
  }
  })"_json;
  bsl6d::GridDistributionConfig procsNotMultipleOfPoints{mpiCartTopologyJson};
  err  = 0;
  warn = 0;
  procsNotMultipleOfPoints.validate(ofs, warn, err);
  EXPECT_EQ(err, 3);

  std::filesystem::remove("ConfigGridDistributionTestOutput.out");
}

TEST(IOUserConfig, JsonParseDistributionFunction) {
  nlohmann::json distFuncJson =
      R"({
    "distributionFunction": {
      "q": 1.0,
      "m": 1.0
    }
  })"_json;
  ASSERT_NO_THROW(bsl6d::DistributionFunctionConfig{distFuncJson});

  distFuncJson =
      R"({
    "distributionFunction": {
      "m": 0.0
    }
  })"_json;
  bsl6d::DistributionFunctionConfig wrongM{distFuncJson};
  size_t err{};
  size_t warn{};
  std::ofstream ofs{"ConfigDistributionFunctionTestOutput.out"};
  wrongM.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);
  std::filesystem::remove("ConfigDistributionFunctionTestOutput.out");
}

TEST(IOUserConfig, JsonParseSimulationTime) {
  nlohmann::json simTimeJson =
      R"({
    "simulationTime": {
      "dt": 0.1,
      "T": 1.0    }
  })"_json;
  ASSERT_NO_THROW(bsl6d::SimulationTimeConfig{simTimeJson});

  simTimeJson =
      R"({
    "simulationTime": {
      "dt": 0.0,
      "T": 1.0    }
  })"_json;
  bsl6d::SimulationTimeConfig wrongDT{simTimeJson};
  size_t err{};
  size_t warn{};
  std::ofstream ofs{"ConfigSimTimeTestOutput.out"};
  wrongDT.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  simTimeJson =
      R"({
    "simulationTime": {
      "dt": 1.1,
      "T": 1.0    }
  })"_json;
  bsl6d::SimulationTimeConfig wrongdtT{simTimeJson};
  err  = 0;
  warn = 0;
  wrongdtT.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  std::filesystem::remove("ConfigSimTimeTestOutput.out");
}

TEST(IOUserConfig, JsonParseOperatorLagrange) {
  nlohmann::json lagrangeOperatorConfig =
      R"({
    "operators": {
      "interpolator": {
        "stencilWidth": [3,4,5,6,7,8],
        "teamSize": [0,0,0,0,0,0],
        "vectorSize": [0,0,0,0,0,0]
      }
    }
  })"_json;
  ASSERT_NO_THROW(bsl6d::OperatorsConfig{lagrangeOperatorConfig});

  lagrangeOperatorConfig =
      R"({
    "operators": {
      "interpolator": {
        "stencilWidth": [3,4,5,6,7],
        "teamSize": [0,0,0,0,0,0],
        "vectorSize": [0,0,0,0,0,0]
      }  
    }
  })"_json;
  bsl6d::OperatorsConfig wrongArrayLength{lagrangeOperatorConfig};
  size_t err{};
  size_t warn{};
  std::ofstream ofs{"ConfigLagrangeInterpolator.out"};
  wrongArrayLength.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  lagrangeOperatorConfig =
      R"({
    "operators": {
      "interpolator": {
        "stencilWidth": [3,4,5,6,7,14],
        "teamSize": [0,0,0,0,0,0],
        "vectorSize": [0,0,0,0,0,0]
      }
    }
  })"_json;
  bsl6d::OperatorsConfig wrongStencil{lagrangeOperatorConfig};
  err  = 0;
  warn = 0;
  wrongStencil.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  lagrangeOperatorConfig =
      R"({
    "operators": {
      "interpolator": {
        "stencilWidth": [3,4,5,6,7,9],
        "teamSize": [0,0,0,0,0,-1],
        "vectorSize": [0,0,0,0,0,0]
      }
    }
  })"_json;
  bsl6d::OperatorsConfig wrongTeamSize{lagrangeOperatorConfig};
  err  = 0;
  warn = 0;
  wrongTeamSize.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  lagrangeOperatorConfig =
      R"({
    "operators": {
      "interpolator": {
        "stencilWidth": [3,4,5,6,7,9],
        "teamSize": [0,0,0,0,0,0],
        "vectorSize": [0,0,0,0,0,-1]
      }
    }
  })"_json;
  bsl6d::OperatorsConfig wrongVectorSize{lagrangeOperatorConfig};
  err  = 0;
  warn = 0;
  wrongVectorSize.validate(ofs, warn, err);
  EXPECT_EQ(err, 1);

  std::filesystem::remove("ConfigLagrangeInterpolator.out");
}

TEST(IOUserConfig, JsonParseDiagnostic) {
  nlohmann::json diagnosticConfig = R"({"diagnostics":{}})"_json;
  bsl6d::DiagnosticConfig empty{diagnosticConfig};
  size_t err{};
  size_t warn{};
  std::ofstream ofs{"ConfigDiagnostics.out"};
  empty.validate(ofs, warn, err);
  EXPECT_EQ(err, 0);

  diagnosticConfig =
      R"({"diagnostics": [
          {
          "diagnostic": "f6d",
          "iteration": 10
          },
          {
          "diagnostic": "E",
          "iteration": 5
          }
        ]
  })"_json;
  bsl6d::DiagnosticConfig correctInput{diagnosticConfig};
  err  = 0;
  warn = 0;
  correctInput.validate(ofs, warn, err);
  EXPECT_EQ(err, 0);

  diagnosticConfig =
      R"({"diagnostics": [
          {
          "diagnostic": "wrongDiagnostic",
          "iteration": 0
          }
        ]

  })"_json;
  bsl6d::DiagnosticConfig allWrongConfig{diagnosticConfig};
  err  = 0;
  warn = 0;
  allWrongConfig.validate(ofs, warn, err);
  EXPECT_EQ(err, 2);

  std::filesystem::remove("ConfigDiagnostics.out");
}

TEST(IOUserConfig, ValidateConfigFileStructure) {
  nlohmann::json configFile =
      R"({
  "gridDistribution": {"grid": {}},
  "simulationTime": {},
  "fieldSolver": {"backgroundField": {}}
  })"_json;
  EXPECT_NO_THROW(
      bsl6d::Impl::validate_and_complete_config_file_structure(configFile));

  EXPECT_TRUE(bsl6d::config_file().contains("distributionFunction"));
  EXPECT_TRUE(bsl6d::config_file()["gridDistribution"].contains("parallel"));
  ASSERT_TRUE(bsl6d::config_file().contains("operators"));
  EXPECT_TRUE(bsl6d::config_file()["operators"].contains("interpolator"));
}
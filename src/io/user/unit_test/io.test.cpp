#include <filesystem>
#include <iostream>

#include <gtest/gtest.h>

#include <config.hpp>
#include <diagnostics.hpp>

TEST(IOUserIO, ReadConfigFile) {
  bsl6d::initMasterOut("bsl6dDiagnostics.out");
  ASSERT_NO_THROW(bsl6d::read_config_file("bsl6dConfig.json"););
  EXPECT_TRUE(std::filesystem::remove("bsl6dDiagnostics.out"));
}

TEST(IOUserIO, DefaultCMDArguments) {
  int argc{1};
  char *argv[1] = {(char *)("/Full/Path/Of/Executable")};

  EXPECT_NO_THROW(bsl6d::cmd::parse(argc, argv));
  EXPECT_NO_THROW(bsl6d::cmd::input_file());
  EXPECT_EQ(bsl6d::cmd::input_file(), "bsl6dConfig.json");
  EXPECT_EQ(bsl6d::cmd::is_test(), false);
  EXPECT_EQ(bsl6d::cmd::is_verbose(), false);
  EXPECT_EQ(bsl6d::cmd::is_restart(), false);
  EXPECT_THROW(bsl6d::cmd::restart_file(), std::runtime_error);
}

TEST(IOUserIO, ParseCMDArguments) {
  int argc{8};
  char *argv[8] = {(char *)("/Full/Path/Of/Executable"),
                   (char *)("--input_file"),
                   (char *)("config.json"),
                   (char *)("--output_file"),
                   (char *)("diagnostics"),
                   (char *)("--test"),
                   (char *)("bernstein_waves"),
                   (char *)("--verbose")};
  bsl6d::cmd::parse(argc, argv);
  EXPECT_EQ(bsl6d::cmd::input_file(), "config.json");
  EXPECT_EQ(bsl6d::cmd::is_test(), true);
  EXPECT_EQ(bsl6d::cmd::test_type(), "bernstein_waves");
  EXPECT_EQ(bsl6d::cmd::is_verbose(), true);
}

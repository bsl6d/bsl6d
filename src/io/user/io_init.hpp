/*****************************************************************/
/*
\brief Initialize all io streams to communicate with a user

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   03.09.2021
*/
/*****************************************************************/
#pragma once

#include <filesystem>
#include <fstream>
#include <list>
#include <string>

#include <nlohmann/json.hpp>

namespace bsl6d {

namespace Impl {
void validate_and_complete_config_file_structure(nlohmann::json configFile);
}

std::ofstream &initMasterOut(std::string const &filename);
void read_config_file(std::string const &filename);
nlohmann::json const &config_file();
std::ofstream &bsl6d_log_stream();

namespace cmd {
std::string diagnostic_directory();
void print_flags_help_message();
std::string output_file();
std::string input_file();
std::string test_type();
std::string init_type();
std::filesystem::path restart_file();
bool is_test();
bool init_only();
bool is_verbose();
bool is_restart();
bool delete_flag_value();
bool input_file_passed();
bool output_file_passed();

void parse(int argc, char **argv);

} /* namespace cmd  */

} /* namespace bsl6d  */

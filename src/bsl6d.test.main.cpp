#include <gtest/gtest.h>
#include <mpi.h>
#include <iostream>
#include <mpi_helper.hpp>

#include <Kokkos_Core.hpp>

int main(int argc, char **argv) {
  bsl6d::CHECK_MPI(MPI_Init(&argc, &argv));
  // ToDo how to set this errorhandler as default in MPI program?
  // This is done by somehow setting the MPI_INFO keyword
  // "mpi_initial_errhandler" to "mpi_errors_return"
  // MPI Default: "mpi_errors_are_fatal"
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  int result{};
  {
    Kokkos::ScopeGuard scope_guard(argc, argv);
    ::testing::InitGoogleTest(&argc, argv);
    result = RUN_ALL_TESTS();
  }
  bsl6d::CHECK_MPI(MPI_Finalize());
  return result;
}

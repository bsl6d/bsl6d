#pragma once

#include <baseFieldSolver.hpp>
#include <distributionFunction.hpp>
#include <time.hpp>

namespace bsl6d {
// clang-format off
/**
 * @brief Compare the simulation results of the ion sound wave in inputFileDatabase.cpp with
 *        the analytical results.
 *
 * This function compares the the simulation results of \f$\phi_\text{Sim}(k_x,t)\f$ to the 
 * analytical results of \f$\rho_\text{Analytical}(k_x,t)\f$. Both are equal due to quasi neutrality.
 * The analytical solution is known in the frequency and wave domain (fourier space). The 
 * comparison uses a strongly simplified transformation to transform from \f$\omega\f$ to \f$ t\f$ 
 * and compares only the real part of the solution
 * 
 * @f[
 * \frac{4\alpha}{\sqrt{\pi}} \text{Re}(\int \rho_\text{Analytical}(\omega,k)e^{it\omega}\mathrm{d}\omega)
 * @f]
 * 
 * with
 * 
 * @f[
 * \rho(\omega) = \frac{1}{\pi \omega^2 e^{-\omega^2} + 4 e^{\omega^2}(-1.0 + \omega D_+(\omega))^2}
 * @f]
 * 
 * where \f$D_+(x)\f$ is the Dawson function and \f$\omega/k \rightarrow \omega\f$
 * using \f$k=1\f$. Higher modes just travel faster such that \f$k>1\f$ are represented
 * through a shift in time.
 * 
 */
// clang-format on
bool compare_ion_sound_wave(DistributionFunction const &f6d,
                            MaxwellFields const &maxwellFields,
                            SimulationTime const &simTime);
}  // namespace bsl6d

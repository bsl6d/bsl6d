/*****************************************************************/
/*
\brief Solve the full kinetic 6d-Vlasov equation

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   02.09.2021
*/
/*****************************************************************/
#include <gtest/gtest.h>

#include <fstream>
#include <map>
#include <string>

#include <config.hpp>
#include <io_init.hpp>

#include <advector.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <lagrangeBuilder.hpp>
#include <linearAlgebra.hpp>
#include <mpiTopology.hpp>
#include <mpi_helper.hpp>
#include <scalarField.hpp>
#include <selalibInterface.hpp>
#include <time.hpp>
#include <vectorAnalysis.hpp>
#include <vectorField.hpp>

nlohmann::json configFilePVP =
    R"({
  "distributionFunction": {
    "q": -1.0,
    "m": 1.0
  },
  "simulationTime": {
    "dt": 0.1,
    "T": 1.0,
    "restart": false
  },
  "initializationDistributionFunction": {
    "initFunction": "planewave_dens_perturbation",
    "alpha": 0.1,
    "k": [0.5,0.5,0.5]
  },
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [12.566370614359172, 12.566370614359172, 12.566370614359172, 6.0, 6.0, 6.0],
      "xvMin": [ 0.0,  0.0,  0.0,-6.0,-6.0,-6.0],
      "NxvPoints": [8, 8, 12, 13, 15, 17],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {
      "Nprocs": [1, 1, 2, 1, 3, 1]
    }
  },
  "operators": {
    "interpolator": {
      "stencilWidth": [9,9,9,9,9,9],
      "teamSize": [0,0,0,0,0,0],
      "vectorSize": [0,0,0,0,0,0]
    }
  }
})"_json;

bsl6d::DistributionFunction landauSimulation(
    bsl6d::CartTopology<6> const &topology) {
  /* Create grid for bsl6d */
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{configFilePVP}};
  bsl6d::GridDistribution<3, 3> gridDistrib{grid, topology};
  /* Create Data structures */
  bsl6d::DistributionFunction f6d{
      gridDistrib, bsl6d::DistributionFunctionConfig(configFilePVP)};
  bsl6d::MaxwellFields maxwellFields{};

  bsl6d::plane_wave_dens_pertubation(f6d, configFilePVP);

  bsl6d::ScalarField rho{gridDistrib.create_xGridDistribution(), "rho"};

  /* Create Characteristic and Operators */
  bsl6d::LagrangeBuilder lagrangeBuilder{};
  lagrangeBuilder.setLagrangeConf(bsl6d::OperatorsConfig{configFilePVP});
  lagrangeBuilder.set_gridDistribution(f6d.gridDistribution());
  bsl6d::SimulationTime simTime{bsl6d::SimulationTimeConfig{configFilePVP}};

  std::shared_ptr<bsl6d::LorentzForceRotatingGrid> characteristicPtr{
      std::make_shared<bsl6d::LorentzForceRotatingGrid>(
          bsl6d::LorentzForceRotatingGrid{f6d})};

  bsl6d::Gradient grad{f6d.gridDistribution().create_xGridDistribution()};
  bsl6d::Laplace laplace{f6d.gridDistribution().create_xGridDistribution()};

  std::map<bsl6d::AxisType, bsl6d::Advector> advector{};

  for (auto axis : bsl6d::xvAxes())
    advector[axis] = bsl6d::Advector{
        characteristicPtr, bsl6d::CharacteristicVariants{characteristicPtr},
        axis, lagrangeBuilder};

  for (auto axis : bsl6d::xAxes()) {
    advector[axis](f6d, simTime.current_T(), simTime.dt());
  }

  rho               = f6d.calculate_charge_density("rho");
  maxwellFields.phi = laplace(bsl6d::Inverse{}, -1 * rho, "phi");
  maxwellFields.E   = grad(maxwellFields.phi, "E");
  characteristicPtr->set_maxwellFields(maxwellFields);

  for (auto axis : bsl6d::vAxes()) {
    advector[axis](f6d, simTime.current_T(), simTime.dt());
  }

  return f6d;
}

TEST(Integration, ParallelLorentzForce) {
  /***************************************************************/
  /* Create configuration structures needed to init bsl6d classes */
  bsl6d::CartTopology<6> topology{bsl6d::GridDistributionConfig{configFilePVP}};

  bsl6d::DistributionFunction f6d_parallel{landauSimulation(topology)};
  Kokkos::deep_copy(f6d_parallel.host(), f6d_parallel.device());
  bsl6d::DistributionFunction f6d_single{
      landauSimulation(topology.create_single_proc_topology())};
  Kokkos::deep_copy(f6d_single.host(), f6d_single.device());

  Kokkos::Array<size_t, 6> lowIdx{};
  Kokkos::Array<size_t, 6> upIdx{};
  for (auto axis : bsl6d::xvAxes()) {
    lowIdx[static_cast<size_t>(axis)] = f6d_parallel.grid()(axis).low();
    upIdx[static_cast<size_t>(axis)]  = f6d_parallel.grid()(axis).up() + 1;
  }
  Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>
      resRange{lowIdx, upIdx};

  double err{};
  Kokkos::parallel_reduce(
      "Compare Parallel and Single Run", resRange,
      [=](size_t i0, size_t i1, size_t i2, size_t i3, size_t i4, size_t i5,
          double &lerr) {
        lerr = std::max(
            lerr,
            std::abs(
                (f6d_single.host()(i0, i1, i2, i3, i4, i5) -
                 f6d_parallel.host()(
                     i0 - f6d_parallel.grid()(bsl6d::AxisType::x1).low(),
                     i1 - f6d_parallel.grid()(bsl6d::AxisType::x2).low(),
                     i2 - f6d_parallel.grid()(bsl6d::AxisType::x3).low(),
                     i3 - f6d_parallel.grid()(bsl6d::AxisType::v1).low(),
                     i4 - f6d_parallel.grid()(bsl6d::AxisType::v2).low(),
                     i5 - f6d_parallel.grid()(bsl6d::AxisType::v3).low())) /
                f6d_single.host()(i0, i1, i2, i3, i4, i5)));
      },
      Kokkos::Max<double>(err));

  ASSERT_LT(err, 5e-14);
}

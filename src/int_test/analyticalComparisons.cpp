#include "./analyticalComparisons.hpp"
#include <cmath>
#include <complex>
#include <diagnostics.hpp>
#include <heffte.hpp>
#include <initializationFunctions.hpp>
#include <io_init.hpp>

namespace bsl6d {

/**
 * @brief The Dawson function below is approximated by a simple
 *        Riemann sum which has an accuracy almost to machine
 *        precision for the given configuration in the interval
 *        (0,0.008)
 */
double dawson_function(double x) {
  int const steps{3000};
  double xPrime{};
  double dxPrime{x / (steps - 1)};
  double dawson{};

  while (xPrime < x) {
    dawson += std::exp(xPrime * xPrime);
    xPrime += dxPrime;
  }

  dawson *= std::exp(-x * x) * dxPrime;
  return dawson;
}

/**
 * @brief Solution of the ion sound wave in Fourier space (omega,k)
 *        for k=1.
 */
double phi_analytical_ion_sound_wave_k0(double w) {
  return 1.0 / (M_PI * w * w * std::exp(-w * w) +
                4.0 * std::exp(w * w) * (-1.0 + w * dawson_function(w)) *
                    (-1.0 + w * dawson_function(w)));
}

/**
 * @brief Fourier backtransform of the frequency domain omega to
 *        the time domain t of the analytical solution for the ion
 *        sound wave. Higher modes (k>1) travel faster in time
 *        compared to k=1. Modes (k>1) can be expressed through the
 *        mode k=1 shifted in time (t*k).
 *        This solution only consideres the real part of the fourier
 *        transform.
 */
double phi_analytical_ion_sound_wave(double k,
                                     double alpha,
                                     SimulationTime const &simTime) {
  size_t const steps{8000};
  double const wMax{6.0};
  double dw{wMax / (steps - 1)};

  double phi{};
  double fftPrefactor{4 * alpha / (std::sqrt(M_PI))};
  double w{};
  while (w < wMax) {
    double t{simTime.current_T() * k};
    phi += fftPrefactor * phi_analytical_ion_sound_wave_k0(w + 0.5 * dw) *
           std::cos(std::sqrt(2.0) * (w + 0.5 * dw) * t);
    w += dw;
  }

  phi *= dw;
  return phi;
}

bool compare_ion_sound_wave(DistributionFunction const &f6d,
                            MaxwellFields const &maxwellFields,
                            SimulationTime const &simTime) {
  nlohmann::json configFile = bsl6d::config_file();

  auto [simTime1, f6dAnalytical] = initialize();

  FastFourierTransform3d fft{f6d.gridDistribution().create_xGridDistribution()};

  Kokkos::View<std::complex<double> ***> rhoAnalytical{};
  rhoAnalytical =
      fft(f6dAnalytical.calculate_charge_density("charge density @ t=0"),
          "charge density @ t=0 FFT");
  Kokkos::View<std::complex<double> ***>::HostMirror h_rhoAnalytical{
      Kokkos::create_mirror_view(rhoAnalytical)};
  Kokkos::deep_copy(h_rhoAnalytical, rhoAnalytical);

  Kokkos::View<std::complex<double> ***> phiFourier{};
  phiFourier = fft(maxwellFields.phi, "electric potential @ t=simTime");
  Kokkos::View<std::complex<double> ***>::HostMirror h_phiFourier{
      Kokkos::create_mirror_view(phiFourier)};
  Kokkos::deep_copy(h_phiFourier, phiFourier);

  // The Simulation is not valid for large mk due to damping effects of
  // the lagrange interpolation at large modes. Therefore the simulation output
  // is only compared up to five modes.
  bool hasPassed{true};
  for (size_t mk = 1; mk < 5; mk++) {
    double relErr{};
    relErr = std::abs((phi_analytical_ion_sound_wave(
                           mk, h_rhoAnalytical(mk, 0, 0).real(), simTime) -
                       h_phiFourier(mk, 0, 0).real()) /
                      h_phiFourier(mk, 0, 0).real());
    // The threshold for the relative error is set based on experience
    // and comparison to the evaluation using mathematica
    double threshold{5.0e-3};
    if (relErr > threshold) {
      hasPassed = false;
      std::cerr << "Ion sound wave test failed at mode m_k" << mk << std::endl;
      std::cerr << "Relative difference: " << relErr << std::endl;
      std::cerr << "Threshold: " << threshold << std::endl;
    }
  }

  return hasPassed;
}
}  // namespace bsl6d

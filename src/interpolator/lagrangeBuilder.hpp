/*****************************************************************/
/*

\brief Builder to generate Interpolation class

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#pragma once

#include <gridDistribution.hpp>
#include <halo.hpp>

#include <lagrangePrefetchingIterator.hpp>
#include <trigonometricInterpolator.hpp>

namespace bsl6d {

/*************************************************************/
/*

\brief Build class that builds up interpolation class for
       distribution function.

\description Abstract the creation of a concrete interpolation
             class using the build pattern and std::variant to
             select different type save objects at runtime.

\literature 2004; Design Patterns - Elements of Reusable Object-
              Oriented Software; Gamma, Helm, Johnson, Vlissides;
              p.97ff
            2020; A Tour of C++; Bjarne Stroustrup; p.175f

*/
/*************************************************************/
class LagrangeBuilder {
  public:
  LagrangeBuilder() = default;

  void setAxis(AxisType _axis) {
    axis = _axis;
    dim  = static_cast<size_t>(axis);
  };

  void setLagrangeConf(OperatorsConfig conf) {
    m_conf     = conf.configLagrangeInterpolator;
    confWasSet = true;
  };

  void setCharacteristic(CharacteristicVariants const &_characteristic) {
    characteristic = _characteristic;
  }
  void set_gridDistribution(GridDistribution<3, 3> const &gridDist) {
    if (confWasSet) {
      for (auto axis : gridDist.my_local_grid().xvAxes) {
        Nxv[static_cast<size_t>(axis)] = gridDist.my_local_grid()(axis).N();
      }
      m_gridDist = gridDist;
      std::map<AxisType, size_t> boundaryWidth{};
      for (auto axis : gridDist.mpi_global_grid().xvAxes) {
        boundaryWidth[axis] =
            std::floor(m_conf.stencilWidth[static_cast<size_t>(axis)] / 2.0);
      }
      m_halo = HaloComm(m_gridDist.my_local_grid(), boundaryWidth);

    } else {
      throw std::invalid_argument(
          "Set configuration of lagrange interpolator before setting "
          "GridDistribution");
    }
  }

  std::shared_ptr<Base1dInterpolator> getInterpolator() {
    if (m_conf.type[static_cast<size_t>(axis)] == "Trig") {
      return chooseTrigonometricInterpolator();
    } else if (m_conf.type[static_cast<size_t>(axis)] == "Lag") {
      return chooseLagrangeInterpolator();
    }
    throw std::runtime_error("Unknown interpolator " +
                             m_conf.type[static_cast<size_t>(axis)] +
                             " type given to class lagrangeBuilder for axis " +
                             axis_to_string(axis) + "!");
  };

  private:
  AxisType axis{};
  GridDistribution<3, 3> m_gridDist{};
  size_t dim{static_cast<size_t>(axis)};
  LagrangeInterpolationStencilVariants stencil{};
  std::array<size_t, 6> Nxv{};
  HaloComm m_halo{};
  OperatorsConfig::ConfigLagrangeInterpolator m_conf{};
  bool confWasSet{false};
  CharacteristicVariants characteristic{};

  /*************************************************************/
  /*

  \brief Methods to choose correct template class for lagrange
         prefetching interpolator

  \description The nested 'chooseType()'-Funktions recreate the
               following for loop as a compile time construct:
               for(int i=M; M>=0 ; M--){
                 if(std::get_if<i>(&stencil)){
                   for(int i=N; N>=0 ; N--){
                     if constexpr(N==dim){
                       return std::make_shared<...>(...)
                     }
                   }
                 }
               }
               (C++ does not have compile time for loops...)


  */
  /*************************************************************/
  std::shared_ptr<Base1dInterpolator> chooseLagrangeInterpolator();
  std::shared_ptr<Base1dInterpolator> chooseTrigonometricInterpolator();

  template <size_t characIdx, size_t stencilIdx, size_t axisIdx>
  std::shared_ptr<Base1dInterpolator> chooseLagrangeDim() {
    if (axisIdx == dim) {
      using StencilType =
          std::variant_alternative_t<stencilIdx,
                                     LagrangeInterpolationStencilVariants>;
      using CharacteristicType =
          std::variant_alternative_t<characIdx, CharacteristicVariants>;
      size_t stencilWidth = m_conf.stencilWidth[static_cast<size_t>(axis)];
      size_t teamSize     = m_conf.teamSize[static_cast<size_t>(axis)];
      size_t vectorSize   = m_conf.vectorSize[static_cast<size_t>(axis)];
      LagrangeInterpolationPrefetching<CharacteristicType, StencilType,
                                       static_cast<AxisType>(axisIdx)>
          chosenInterpolator{
              Nxv,      std::get<CharacteristicType>(characteristic),
              m_halo,   stencilWidth,
              teamSize, vectorSize};
      return std::make_shared<LagrangeInterpolationPrefetching<
          CharacteristicType, StencilType, static_cast<AxisType>(axisIdx)>>(
          chosenInterpolator);
    } else if constexpr (axisIdx > 0) {
      return chooseLagrangeDim<characIdx, stencilIdx, axisIdx - 1>();
    }
    throw std::invalid_argument("Invalid dimension given to builder: " +
                                std::to_string(dim));
  }

  template <size_t characIdx, size_t stencilIdx, size_t axisIdx>
  std::shared_ptr<Base1dInterpolator> chooseLagrangeStencil() {
    stencil = create_lagrange_interpolation_stencil(
        m_conf.stencilWidth[static_cast<size_t>(axis)]);
    if (std::get_if<stencilIdx>(&stencil)) {
      return chooseLagrangeDim<characIdx, stencilIdx, axisIdx>();
    } else if constexpr (stencilIdx > 0) {
      return chooseLagrangeStencil<characIdx, stencilIdx - 1, axisIdx>();
    }
    throw std::invalid_argument(
        "Invalid Stencil given to builder: " +
        std::to_string(m_conf.stencilWidth[static_cast<size_t>(axis)]));
  }

  template <size_t characIdx, size_t stencilIdx, size_t axisIdx>
  std::shared_ptr<Base1dInterpolator> chooseLagrangeCharacteristic() {
    if (std::get_if<characIdx>(&characteristic)) {
      return chooseLagrangeStencil<characIdx, stencilIdx, axisIdx>();
    } else if constexpr (characIdx > 0) {
      return chooseLagrangeCharacteristic<characIdx - 1, stencilIdx, axisIdx>();
    }
    throw std::invalid_argument("Invalid Characteristic given to builder!");
  }

  template <size_t characIdx, size_t axisIdx>
  std::shared_ptr<Base1dInterpolator> chooseTrigonometicAxis() {
    if (axisIdx == static_cast<size_t>(axis)) {
      using CharacteristicType =
          std::variant_alternative_t<characIdx, CharacteristicVariants>;
      TrigonometricInterpolator<CharacteristicType,
                                static_cast<AxisType>(axisIdx)>
          interpolator{std::get<CharacteristicType>(characteristic)};
      return std::make_shared<TrigonometricInterpolator<
          CharacteristicType, static_cast<AxisType>(axisIdx)>>(interpolator);
    } else if constexpr (axisIdx > 0) {
      return chooseTrigonometicAxis<characIdx, axisIdx - 1>();
    }
    throw std::invalid_argument(
        "Invalid interpolation axis given to builder: " + axis_to_string(axis));
  }

  template <size_t characIdx, size_t axisIdx>
  std::shared_ptr<Base1dInterpolator> chooseTrigonometicCharacteristic() {
    if (std::get_if<characIdx>(&characteristic)) {
      return chooseTrigonometicAxis<characIdx, axisIdx>();
    } else if constexpr (characIdx > 0) {
      return chooseTrigonometicCharacteristic<characIdx - 1, axisIdx>();
    }
    throw std::invalid_argument("Invalid Characteristic given to builder!");
  }
};

} /* namespace bsl6d */

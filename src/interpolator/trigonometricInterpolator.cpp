/*****************************************************************/
/*
\brief Using prefetching algorithm to interpolate in 1 dimension

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include <lagrangeBuilder.hpp>
#include <lagrangePrefetchingIterator.hpp>

#include <trigonometricInterpolator.hpp>

#if defined(LIKWID_PERFMON)
  #include <likwid-marker.h>
  #include <omp.h>
#elif defined(ROCM_PERFMON)
  #include <roctx.h>
#endif

namespace bsl6d {
template <typename CharacteristicPtrType, AxisType axis>
TrigonometricInterpolator<CharacteristicPtrType, axis>::
    TrigonometricInterpolator(CharacteristicPtrType characteristicPtr)
    : m_characteristicPtr{characteristicPtr}
    , m_characteristic{*characteristicPtr} {}

// Throw an error if boundary condition of f6d is not periodic
// Throw an error if one of the dimensions interpolated is MPI-Parallelized
template <typename CharacteristicPtrType, AxisType axis>
void TrigonometricInterpolator<CharacteristicPtrType, axis>::interpolate(
    DistributionFunction &f6d) {
  m_f     = f6d.device();
  m_grid  = f6d.mpi_local_grid();
  f6d_res = bsl6d::create_grid_compatible_view(f6d.mpi_local_grid(), "res");
  double cfl{m_characteristicPtr->update(axis)};
  if (cfl > 2 * f6d.mpi_local_grid()(axis).N() - 1) {
    throw std::runtime_error(
        "Interpolated axis " + axis_to_string(axis) +
        " can not cross the domain twice in class "
        "TrigonometricInterpolator which would happen for maxShift=" +
        std::to_string(cfl));
  }
  m_characteristic = *m_characteristicPtr;
  if (not f6d.gridDistribution().cart_topology().periodic(axis)) {
    throw std::runtime_error("Interpolated axis " + axis_to_string(axis) +
                             " requires periodic boundary conditions for class "
                             "TrigonometricInterpolator!");
  }
  if (f6d.gridDistribution().cart_topology().cart_dim_size(axis) > 1) {
    throw std::runtime_error(
        "Interpolated axis " + axis_to_string(axis) +
        " can not be distributed in the cartesian MPI topology "
        "to use class TrigonometricInterpolator!");
  }
  // WARNING: The trigonometric interpolation might not be stable
  // for shifts close to grid points
  std::string label{"InterpolateTrigonometric" + axis_to_string(axis)};
  Kokkos::parallel_for(label, f6d.mpi_local_grid().mdRangePolicy(), *this);
  Kokkos::deep_copy(f6d.device(), f6d_res);
  f6d.require_velocity_moments_update();
}

template <typename CharacteristicPtrType, AxisType axis>
KOKKOS_INLINE_FUNCTION double
    TrigonometricInterpolator<CharacteristicPtrType, axis>::trig_cardinal(
        double x,
        size_t N) const {
  double tauEven{Kokkos::sin(N * M_PI * x / 2.0) /
                 (N * Kokkos::tan(M_PI * x / 2.0))};
  double tauOdd{Kokkos::sin(N * M_PI * x / 2.0) /
                (N * Kokkos::sin(M_PI * x / 2.0))};
  return (N % 2 == 0) ? tauEven : tauOdd;
};

template <typename CharacteristicPtrType, AxisType axis>
KOKKOS_INLINE_FUNCTION constexpr size_t
    TrigonometricInterpolator<CharacteristicPtrType, axis>::index(
        std::array<size_t, 6> const &idx) const {
  if constexpr (m_axis == AxisType::x1) {
    return idx[0];
  } else if constexpr (m_axis == AxisType::x2) {
    return idx[1];
  } else if constexpr (m_axis == AxisType::x3) {
    return idx[2];
  } else if constexpr (m_axis == AxisType::v1) {
    return idx[3];
  } else if constexpr (m_axis == AxisType::v2) {
    return idx[4];
  }
  // AxisType::v3
  return idx[5];
}

template <typename CharacteristicTypePtr, AxisType axis>
KOKKOS_INLINE_FUNCTION constexpr double
    TrigonometricInterpolator<CharacteristicTypePtr, axis>::shift(
        size_t const &ix1,
        size_t const &ix2,
        size_t const &ix3,
        size_t const &iv1,
        size_t const &iv2,
        size_t const &iv3) const {
  if constexpr (AxisType::x1 == m_axis) {
    return m_characteristic.shift_x1(ix1, ix2, ix3, iv1, iv2, iv3);
  } else if constexpr (AxisType::x2 == m_axis) {
    return m_characteristic.shift_x2(ix1, ix2, ix3, iv1, iv2, iv3);
  } else if constexpr (AxisType::x3 == m_axis) {
    return m_characteristic.shift_x3(ix1, ix2, ix3, iv1, iv2, iv3);
  } else if constexpr (AxisType::v1 == m_axis) {
    return m_characteristic.shift_v1(ix1, ix2, ix3, iv1, iv2, iv3);
  } else if constexpr (AxisType::v2 == m_axis) {
    return m_characteristic.shift_v2(ix1, ix2, ix3, iv1, iv2, iv3);
  }
  return m_characteristic.shift_v3(ix1, ix2, ix3, iv1, iv2, iv3);
}

std::shared_ptr<Base1dInterpolator>
    LagrangeBuilder::chooseTrigonometricInterpolator() {
  return chooseTrigonometicCharacteristic<
      std::variant_size_v<CharacteristicVariants> - 1, 5>();
};

} /* namespace bsl6d */

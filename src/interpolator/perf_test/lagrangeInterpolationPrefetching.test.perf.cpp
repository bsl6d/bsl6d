/*****************************************************************/
/*
\brief Solve the full kinetic 6d-Vlasov equation

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   02.09.2021
*/
/*****************************************************************/
#include <fstream>
#include <map>
#include <string>

#include <config.hpp>
#include <distributionFunction.hpp>
#include <gradient.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initDistribution.hpp>
#include <lagrangeBuilder.hpp>
#include <laplace.hpp>
#include <mpiTopology.hpp>
#include <mpi_helper.hpp>
#include <scalarField.hpp>
#include <selalibInterface.hpp>
#include <time.hpp>
#include <vectorField.hpp>

#if defined(LIKWID_PERFMON)
  #include <likwid-marker.h>
  #include <omp.h>
#elif defined(ROCM_PERFMON)
  #include <roctracer_ext.h>
#endif

nlohmann::json configFile = R"({
"simulationTime": {
  "dt": 0.0,
  "T": 1.0,
  "restart": false
},
"gridDistribution": {
  "grid": {
    "xvMax": [1.0, 1.0, 1.0, 4.0, 4.0, 4.0],
    "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
    "NxvPoints": [32, 32, 32, 33, 33, 33],
    "position": ["L", "L", "L", "N", "N", "N"]
  },
  "parallel": {
    "Nprocs": [1,1,1,1,1,1]
  }
},
"operators": {
  "interpolator": {
    "stencilWidth": [7,7,7,8,8,8],
    "teamSize": [0,0,0,0,0,0],
    "vectorSize": [0,0,0,0,0,0]
  }
}
})"_json;

int main(int argc, char* argv[]) {
  bsl6d::CharacteristicId characteristicId{
      bsl6d::CharacteristicId::Constant0DShift};
  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-m") == 0) {
      ++i;
      characteristicId = static_cast<bsl6d::CharacteristicId>(atoi(argv[i]));
      std::cout << argv[i] << "\n";
      switch (atoi(argv[i])) {
        case (0):
          characteristicId = bsl6d::CharacteristicId::Constant0DShift;
          std::cout << "CharacteristicID via -m "
                    << static_cast<size_t>(characteristicId) << "\n";
          break;
        case (5):
          characteristicId = bsl6d::CharacteristicId::Constant5DShift;
          std::cout << "CharacteristicID via -m "
                    << static_cast<size_t>(characteristicId) << "\n";
          break;
        default:
          throw std::invalid_argument(
              "Given Characteristic of -m is not available. Use --help for "
              "more "
              "information");
      }
    } else if (strcmp(argv[i], "--Characteristic") == 0) {
      ++i;
      std::cout << argv[i] << "\n";
      if (strcmp(argv[i], "0D_Shift") == 0) {
        characteristicId = bsl6d::CharacteristicId::Constant0DShift;
        std::cout << "CharacteristicID via --Characteristic "
                  << static_cast<size_t>(characteristicId) << "\n";
      } else if (strcmp(argv[i], "5D_Shift") == 0) {
        characteristicId = bsl6d::CharacteristicId::Constant5DShift;
        std::cout << "CharacteristicID via --Characteristic "
                  << static_cast<size_t>(characteristicId) << "\n";
      } else {
        throw std::invalid_argument(
            "Given Characteristic of --Characteristic is not available. Use "
            "--help for more "
            "information");
      }
    } else if ((strcmp(argv[i], "-h") == 0) ||
               (strcmp(argv[i], "--help") == 0)) {
      std::cout << "  Lagrange Interpolation Performance Test:\n";
      std::cout << "  -m <int>:              CharacteristicId as integer\n";
      std::cout << "                         0 -> 0D_Shift\n";
      std::cout << "                         5 -> 5D_Shift\n";
      std::cout
          << "  --Characteristic <str>:         CharacteristicId as string\n";
      std::cout << "                         0D_Shift\n";
      std::cout << "                         5D_Shift\n";
      std::cout << "  --help (-h):           print this message\n\n";
      std::cout << std::flush;
      exit(1);
    }
  }

  bsl6d::CHECK_MPI(MPI_Init(&argc, &argv));
#if defined(LIKWID_PERFMON)
  LIKWID_MARKER_INIT;
#elif defined(ROCM_PERFMON)
  roctracer_start();
#endif
  Kokkos::initialize(argc, argv);
  {
#if defined(LIKWID_PERFMON)
  #pragma omp parallel
    { LIKWID_MARKER_THREADINIT; }
#endif
    /***************************************************************/
    /* Create grid for bsl6d */
    bsl6d::SimulationTime simTime{
        bsl6d::SimulationTimeConfig{configFile["simulationTime"]}};
    bsl6d::CartTopology<6> topology{bsl6d::GridDistributionConfig{
        bsl6d::GridDistributionConfig{configFile}}};
    bsl6d::Grid<3, 3> grid{bsl6d::Grid<3, 3>::get_3d3v_grid(
        bsl6d::GridDistributionConfig{configFile})};
    bsl6d::GridDistribution<3, 3> gridDistrib{grid, topology};
    bsl6d::Grid<3, 3> myGrid{gridDistrib.mpi_local_grid()};

    /* Create Interpolators */
    std::map<bsl6d::AxisType, std::shared_ptr<bsl6d::Base1dInterpolator>>
        intrp_map;
    bsl6d::LagrangeBuilder lagrangeBuilder{};
    lagrangeBuilder.setLagrangeConf(
        bsl6d::OperatorsConfig{configFile["operators"]});
    lagrangeBuilder.set_gridDistribution(gridDistrib);

    switch (characteristicId) {
      case (bsl6d::CharacteristicId::Constant0DShift): {
        bsl6d::CharacteristicBuilder characteristicBuilder{};
        characteristicBuilder.set_shift(.01);
        lagrangeBuilder.setCharacteristic(
            characteristicBuilder.create_characteristic(characteristicId));
        break;
      }
      case (bsl6d::CharacteristicId::Constant5DShift): {
        bsl6d::CharacteristicBuilder characteristicBuilder{};
        characteristicBuilder.set_Nxv(
            {myGrid(bsl6d::AxisType::x1).N(), myGrid(bsl6d::AxisType::x2).N(),
             myGrid(bsl6d::AxisType::x3).N(), myGrid(bsl6d::AxisType::v1).N(),
             myGrid(bsl6d::AxisType::v2).N(), myGrid(bsl6d::AxisType::v3).N()},
            5);
        lagrangeBuilder.setCharacteristic(
            characteristicBuilder.create_characteristic(characteristicId));
        break;
      }
      default:
        throw std::runtime_error(
            "Performance test only implements Constant0DShift Characteristic "
            "and 5D "
            "Shift Characteristic");
    }

    for (auto axis : bsl6d::xvAxes()) {
      lagrangeBuilder.setAxis(axis);
      intrp_map[axis] = lagrangeBuilder.getInterpolator();
    }

    /* Get storage needed for interpolation */
    bsl6d::DistributionFunction f6d(gridDistrib,
                                    bsl6d::DistributionFunctionConfig{});

    double* f_ptr = f6d().data();
    Kokkos::parallel_for(
        "Init baseF/resF", f6d().size(),
        KOKKOS_LAMBDA(size_t i) { f_ptr[i] = 1; });

    while (simTime.continue_advection()) {
      for (auto axis : bsl6d::xvAxes()) {
        intrp_map[axis]->interpolate(f6d);
        Kokkos::fence();
      }
      simTime.advance_current_T_by_dt();
    }
  }
  Kokkos::finalize();
#if defined(LIKWID_PERFMON)
  LIKWID_MARKER_CLOSE;
#elif defined(ROCM_PERFMON)
  roctracer_stop();
#endif
  bsl6d::CHECK_MPI(MPI_Finalize());

  return EXIT_SUCCESS;
}

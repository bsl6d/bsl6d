#include <Kokkos_Core.hpp>
#include <simd.hpp>

#include <cstdio>
#include <cstdlib>
#include <iostream>

#ifdef LIKWID_PERFMON
  #include <likwid-marker.h>
  #include <omp.h>
#endif

using simd_t    = simd::simd<double, simd::simd_abi::native>;
using f_simd_t  = Kokkos::View<simd_t ******, Kokkos::LayoutRight>;
using fs_simd_t = Kokkos::View<simd_t **,
                               Kokkos::LayoutRight,
                               Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
using f_t       = Kokkos::View<double ******, Kokkos::LayoutRight>;
using fs_t      = Kokkos::View<double **,
                          Kokkos::LayoutRight,
                          Kokkos::MemoryTraits<Kokkos::Unmanaged>>;

struct MaxFlexibility {};
struct MinFlexibility {};

struct Prefetching {
  size_t N0, N1, N2, N3, N4, N5, N1N2, N1N2N4, N2N4, N2N4N5;
  f_t f{};
  f_t low{};
  f_t up{};
  f_simd_t E{};
  std::array<double, 7> pp{};
  static constexpr size_t bw{3};
  static constexpr double inv_720{1. / 720.}, inv_120{1. / 120.},
      inv_48{1. / 48.}, inv_36{1. / 36.};

  Prefetching(size_t _N0,
              size_t _N1,
              size_t _N2,
              size_t _N3,
              size_t _N4,
              size_t _N5,
              f_t _f,
              f_t _low,
              f_t _up,
              f_simd_t _E)
      : N0(_N0)
      , N1(_N1)
      , N2(_N2)
      , N3(_N3)
      , N4(_N4)
      , N5(_N5)
      , N1N2(_N1 * _N2)
      , N2N4(_N2 * _N4)
      , N1N2N4(_N1 * _N2 * _N4)
      , f(_f)
      , low{_low}
      , up{_up}
      , E{_E} {};

  void runTeam(int TS, int VS) {
#if BSL6D_ENABLE_GPU
    Kokkos::TeamPolicy<> p(N5 * N1N2N4, TS, VS);
    int shmem_size =
        fs_t::shmem_size(N0 / simd_t::size(), (N3 + 2 * bw) / simd_t::size()) +
        fs_t::shmem_size(N0, N3);
#else
    Kokkos::TeamPolicy<MaxFlexibility> pMax(N0 * N1N2N4, TS, VS);
    Kokkos::TeamPolicy<MinFlexibility> pMin(N0 * N1N2N4, TS, VS);
    int shmem_size =
        fs_t::shmem_size(N3 + 2 * bw, N5) + fs_t::shmem_size(N3, N5);
#endif

#ifdef LIKWID_PERFMON
  #pragma omp parallel
    { LIKWID_MARKER_START("MaxFlexibility"); }
#endif
    Kokkos::parallel_for("AdvTeam",
                         pMax.set_scratch_size(0, Kokkos::PerTeam(shmem_size)),
                         *this);
#ifdef LIKWID_PERFMON
  #pragma omp parallel
    { LIKWID_MARKER_STOP("MaxFlexibility"); }
#endif
    double dx   = static_cast<double>(rand() % 100);
    double dxdx = dx * dx;
    pp[0]       = dx * (dx - 3.) * (dxdx - 4.) * (dxdx - 1.) * inv_720;
    pp[1]       = -dx * (dx - 2.) * (dxdx - 9.) * (dxdx - 1.) * inv_120;
    pp[2]       = dx * (dx - 1.) * (dxdx - 9.) * (dxdx - 4.) * inv_48;
    pp[3]       = -(dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_36;
    pp[4]       = (dx + 1.) * dx * (dxdx - 9.) * (dxdx - 4.) * inv_48;
    pp[5]       = -(dx + 2.) * dx * (dxdx - 9.) * (dxdx - 1.) * inv_120;
    pp[6]       = (dx + 3.) * dx * (dxdx - 4.) * (dxdx - 1.) * inv_720;
#ifdef LIKWID_PERFMON
  #pragma omp parallel
    { LIKWID_MARKER_START("MinFlexibility"); }
#endif
    Kokkos::parallel_for("AdvTeam",
                         pMin.set_scratch_size(0, Kokkos::PerTeam(shmem_size)),
                         *this);
#ifdef LIKWID_PERFMON
  #pragma omp parallel
    { LIKWID_MARKER_STOP("MinFlexibility"); }
#endif
  }

#if BSL6D_ENABLE_GPU
#else
  KOKKOS_INLINE_FUNCTION
  void operator()(
      MinFlexibility,
      const typename Kokkos::TeamPolicy<>::member_type &team) const {
    // league_size = N0N1N2N4
    size_t N5_simd{N5 / simd_t::size()};
    size_t i0 = team.league_rank() / N1N2N4;
    size_t i1 = (team.league_rank() % N1N2N4) / N2N4;
    size_t i2 = ((team.league_rank() % N1N2N4) % N2N4) / N4;
    size_t i4 = ((team.league_rank() % N1N2N4) % N2N4) % N4;
    fs_t f_in(team.team_scratch(0), N3 + 2 * bw, N5);
    fs_t f_out(team.team_scratch(0), N3, N5);
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, bw), [&](const size_t &i3) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, N5), [&](const size_t &i5) {
                f_in(i3, i5) = low(i0, i1, i2, i3, i4, i5);
                f_in(i3 + bw + N3, i5) = up(i0, i1, i2, i3, i4, i5);
              });
        });
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, N5),
                               [&](const size_t &i5) {
                                 f_in(i3 + bw, i5) = f(i0, i1, i2, i3, i4, i5);
                               });
        });
    team.team_barrier();

    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, N5), [&](const size_t &i5) {
                f_out(i3 + bw, i5) = pp[0] * f_in(i3 + bw - 3, i5) +
                                     pp[1] * f_in(i3 + bw - 2, i5) +
                                     pp[2] * f_in(i3 + bw - 1, i5) +
                                     pp[3] * f_in(i3 + bw, i5) +
                                     pp[4] * f_in(i3 + bw + 1, i5) +
                                     pp[5] * f_in(i3 + bw + 2, i5) +
                                     pp[6] * f_in(i3 + bw + 3, i5);
              });
        });
    team.team_barrier();

    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, N5),
                               [&](const size_t &i5) {
                                 f(i0, i1, i2, i3, i4, i5) = f_out(i3, i5);
                               });
        });
  }

  KOKKOS_INLINE_FUNCTION
  void operator()(
      MaxFlexibility,
      const typename Kokkos::TeamPolicy<>::member_type &team) const {
    // league_size = N0N1N2N4
    size_t N5_simd{N5 / simd_t::size()};
    size_t i0 = team.league_rank() / N1N2N4;
    size_t i1 = (team.league_rank() % N1N2N4) / N2N4;
    size_t i2 = ((team.league_rank() % N1N2N4) % N2N4) / N4;
    size_t i4 = ((team.league_rank() % N1N2N4) % N2N4) % N4;
    fs_simd_t f_in_simd(team.team_scratch(0), (N3 + 2 * bw), N5_simd);
    fs_simd_t f_out_simd(team.team_scratch(0), N3, N5_simd);
    fs_t f_in((double *)f_in_simd.data(), N3 + 2 * bw, N5);
    fs_t f_out((double *)f_out_simd.data(), N3, N5);
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, bw), [&](const size_t &i3) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, N5), [&](const size_t &i5) {
                f_in(i3, i5) = low(i0, i1, i2, i3, i4, i5);
                f_in(i3 + bw + N3, i5) = up(i0, i1, i2, i3, i4, i5);
              });
        });
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, N5),
                               [&](const size_t &i5) {
                                 f_in(i3 + bw, i5) = f(i0, i1, i2, i3, i4, i5);
                               });
        });
    team.team_barrier();

    std::array<simd_t, 7> pp_in{};
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, N5_simd), [&](const size_t &i5) {
                simd_t dx = E(i0, i1, i2, i3, i4, i5);
                simd_t dxdx = dx * dx;
                pp_in[0] = dx * (dx - 3.) * (dxdx - 4.) * (dxdx - 1.) * inv_720;
                pp_in[1] =
                    -dx * (dx - 2.) * (dxdx - 9.) * (dxdx - 1.) * inv_120;
                pp_in[2] = dx * (dx - 1.) * (dxdx - 9.) * (dxdx - 4.) * inv_48;
                pp_in[3] = -(dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_36;
                pp_in[4] = (dx + 1.) * dx * (dxdx - 9.) * (dxdx - 4.) * inv_48;
                pp_in[5] =
                    -(dx + 2.) * dx * (dxdx - 9.) * (dxdx - 1.) * inv_120;
                pp_in[6] = (dx + 3.) * dx * (dxdx - 4.) * (dxdx - 1.) * inv_720;
                f_out_simd(i3 + bw, i5) =
                    pp_in[0] * f_in_simd(i3 + bw - 3, i5) +
                    pp_in[1] * f_in_simd(i3 + bw - 2, i5) +
                    pp_in[2] * f_in_simd(i3 + bw - 1, i5) +
                    pp_in[3] * f_in_simd(i3 + bw, i5) +
                    pp_in[4] * f_in_simd(i3 + bw + 1, i5) +
                    pp_in[5] * f_in_simd(i3 + bw + 2, i5) +
                    pp_in[6] * f_in_simd(i3 + bw + 3, i5);
              });
        });
    team.team_barrier();

    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, N3), [&](const size_t &i3) {
          Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, N5),
                               [&](const size_t &i5) {
                                 f(i0, i1, i2, i3, i4, i5) = f_out(i3, i5);
                               });
        });
  }
};
#endif

  int main(int argc, char *argv[]) {
#ifdef LIKWID_PERFMON
    LIKWID_MARKER_INIT;
#endif
    Kokkos::initialize(argc, argv);
    {
      size_t N = (argc > 1) ? atoi(argv[1]) : 16;
      size_t R = (argc > 2) ? atoi(argv[2]) : 100;
#if BSL6D_ENABLE_GPU
      size_t TS = (argc > 3) ? atoi(argv[3]) : 8;
      size_t VS = (argc > 4) ? atoi(argv[4]) : 8;
#else
    size_t TS = (argc > 3) ? atoi(argv[3]) : 1;
    size_t VS = (argc > 4) ? atoi(argv[4]) : 1;
#endif

      size_t N0, N1, N2, N3, N4, N5;
      N0 = N1 = N2 = N3 = N4 = N5 = N;

      f_t f("F", N0, N1, N2, N3, N4, N5);
      f_t low("low", N0, N1, N2, Prefetching::bw, N4, N5);
      f_t up("up", N0, N1, N2, Prefetching::bw, N4, N5);
      f_simd_t E("E", N0, N1, N2, N3, N4, N5 / simd_t::size());
      Kokkos::deep_copy(E, simd_t{static_cast<double>(rand() % 100)});

      Prefetching F(N0, N1, N2, N3, N4, N5, f, low, up, E);

      // F.run(TS,VS);
      // Kokkos::fence();
#ifdef LIKWID_PERFMON
  #pragma omp parallel
      {
        LIKWID_MARKER_THREADINIT;
        LIKWID_MARKER_REGISTER("PREFETCHING_TEAMPOL");
      }
#endif
      Kokkos::Timer timer;
      for (size_t r = 0; r < R; r++) F.runTeam(TS, VS);
      Kokkos::fence();
      double time = timer.seconds();
      printf("Team:%e\n", time);
      Kokkos::fence();
    }
    Kokkos::finalize();
#ifdef LIKWID_PERFMON
    LIKWID_MARKER_CLOSE;
#endif
  }

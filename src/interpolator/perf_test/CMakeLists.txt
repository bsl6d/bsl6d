ADD_EXECUTABLE(LagrangeInterpolationPrefetching.Perf_Test lagrangeInterpolationPrefetching.test.perf.cpp)

TARGET_LINK_LIBRARIES(LagrangeInterpolationPrefetching.Perf_Test PRIVATE 
                        bsl6d)

IF(rocprofiler_FOUND)
  target_link_libraries(LagrangeInterpolationPrefetching.Perf_Test PUBLIC
                          bsl6d::roctracer bsl6d::roctx)
  #TARGET_COMPILE_DEFINITIONS(SingleDimensionLagrangeInterpolation.Perf_Test
  #                            PRIVATE
  #                            -DBSL6D_ENABLE_GPU=1)
ELSEIF(likwid_FOUND)
  #ADD_EXECUTABLE(SingleDimensionLagrangeInterpolation.Perf_Test singleDimensionLagrangeInterpolation.test.perf.cpp)
  #TARGET_LINK_LIBRARIES(SingleDimensionLagrangeInterpolation.Perf_Test PUBLIC
  #                       Kokkos::kokkos)
  TARGET_LINK_LIBRARIES(LagrangeInterpolationPrefetching.Perf_Test PUBLIC
                          OpenMP::OpenMP_CXX
                          bsl6d::likwid)
  #TARGET_LINK_LIBRARIES(SingleDimensionLagrangeInterpolation.Perf_Test PUBLIC
  #                       OpenMP::OpenMP_CXX
  #                       likwid::likwid)
ENDIF()

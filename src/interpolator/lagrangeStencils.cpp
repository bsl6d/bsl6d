/*****************************************************************/
/*

\brief Lagrange Stenicls that can be used for interpolation

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   12.01.2021

*/
/*****************************************************************/

#include <lagrangeStencils.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief FactoryMethod to return different stencil classes inside a
       variant.

*/
/*****************************************************************/
LagrangeInterpolationStencilVariants create_lagrange_interpolation_stencil(
    size_t const width) {
  switch (width) {
    case (LagrangeInterpolationStencil2::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil2{}};
    case (LagrangeInterpolationStencil3::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil3{}};
    case (LagrangeInterpolationStencil4::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil4{}};
    case (LagrangeInterpolationStencil5::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil5{}};
    case (LagrangeInterpolationStencil6::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil6{}};
    case (LagrangeInterpolationStencil7::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil7{}};
    case (LagrangeInterpolationStencil8::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil8{}};
    case (LagrangeInterpolationStencil9::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil9{}};
    case (LagrangeInterpolationStencil10::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil10{}};
    case (LagrangeInterpolationStencil11::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil11{}};
    case (LagrangeInterpolationStencil12::width):
      return LagrangeInterpolationStencilVariants{
          LagrangeInterpolationStencil12{}};
    default:
      throw std::invalid_argument("Not implemented Stencil width given: " +
                                  std::to_string(width));
  }
};

} /* namespace bsl6d */

/*****************************************************************/
/*
\brief Using prefetching algorithm to interpolate in 1 dimension

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#include <lagrangePrefetchingIterator.hpp>

#include <lagrangeBuilder.hpp>

#if defined(LIKWID_PERFMON)
  #include <likwid-marker.h>
  #include <omp.h>
#elif defined(ROCM_PERFMON)
  #include <roctx.h>
#endif

namespace bsl6d {

/*****************************************************************/
/*

\brief Constructor that gets data from config file

\param[in] _Nxv - Array dimesnions of f6d.f that will be interpolated
\param[in] _intrpAxis - Dimension in which data will be interpolated
\param[in] _intrpStencil - Size of stencil used for interpolation

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
LagrangeInterpolationPrefetching<CharacteristicTypePtr,
                                 StencilType,
                                 _intrpAxis>::
    LagrangeInterpolationPrefetching(
        std::array<size_t, 6> const Nxv,
        CharacteristicTypePtr const _characteristic_ptr,
        HaloComm const _halo,
        size_t stencilWidth,
        size_t teamSize,
        size_t vectorSize)
    : Base1dInterpolator{_intrpAxis}
    , characteristic_ptr{_characteristic_ptr}
    , characteristic{*_characteristic_ptr}
    , halo{_halo} {
  size_t bw = std::floor(stencilWidth / 2.0);
  size_t shmem_size{};

  if (teamSize == 0) {
    if (std::is_same_v<Kokkos::DefaultExecutionSpace,
                       Kokkos::DefaultHostExecutionSpace>) {
      teamSize = 1;
    } else {
      teamSize = 8;
    }
  }
  if (vectorSize == 0) {
    if (std::is_same_v<Kokkos::DefaultExecutionSpace,
                       Kokkos::DefaultHostExecutionSpace>) {
      vectorSize = 1;
    } else {
      vectorSize = 8;
    }
  }
  using SharedView =
      Kokkos::View<double **,
                   Kokkos::DefaultExecutionSpace::scratch_memory_space,
                   Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
  if (get_intrpDim() != contiguousDimension<BaseView>()) {
    teamPolNonCont = Kokkos::TeamPolicy<TeamIdxType, NonCont>(
        Nxv[leagueRankDim(get_intrpDim())[3]] *
            Nxv[leagueRankDim(get_intrpDim())[2]] *
            Nxv[leagueRankDim(get_intrpDim())[1]] *
            Nxv[leagueRankDim(get_intrpDim())[0]],
        teamSize, vectorSize);

    shmem_size = SharedView::shmem_size(Nxv[contiguousDimension<BaseView>()],
                                        Nxv[get_intrpDim()] + 2 * bw);
    teamPolNonCont.set_scratch_size(0, Kokkos::PerTeam(shmem_size));
  } else {
    teamPolCont = Kokkos::TeamPolicy<TeamIdxType, Cont>(
        Nxv[leagueRankDim(get_intrpDim())[3]] *
            Nxv[leagueRankDim(get_intrpDim())[2]] *
            Nxv[leagueRankDim(get_intrpDim())[1]] *
            Nxv[leagueRankDim(get_intrpDim())[0]],
        teamSize, vectorSize);
    if (std::is_same_v<Kokkos::DefaultExecutionSpace,
                       Kokkos::DefaultHostExecutionSpace>) {
      shmem_size =
          SharedView::shmem_size(Nxv[get_outerIterationDim<BaseView>()],
                                 Nxv[contiguousDimension<BaseView>()] + 2 * bw);
    } else {
      shmem_size =
          SharedView::shmem_size(Nxv[contiguousDimension<BaseView>()] + 2 * bw,
                                 Nxv[get_outerIterationDim<BaseView>()]);
    }
    teamPolCont.set_scratch_size(0, Kokkos::PerTeam(shmem_size));
  }
  std::string label{"Interpolate" + axis_to_string(intrpAxis)};
#if defined(LIKWID_PERFMON)
  #pragma omp parallel
  { LIKWID_MARKER_REGISTER(label.c_str()); }
#elif defined(ROCM_PERFMON)
  roctxMark(label.c_str());
#endif
}

/*****************************************************************/
/*

\brief Interpolate all points of f6d into f using Kokkos

\param[in] f6d - datastrucutre containing distribution function and
                 boundary points

\description Calculate:

     f(i0,i1,i2,i3,i4,i5) =
            sum_(i=-1)^1 f(i0,i1,i2,i3+i,i4,i5) p[i]

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
void LagrangeInterpolationPrefetching<
    CharacteristicTypePtr,
    StencilType,
    _intrpAxis>::interpolate(DistributionFunction &f6d) {
  // Copy meta data of Views to be interpolated
  BoundaryHypercube boundary{};
  if (not skipThisHaloExchange) {
    boundary             = halo.exchange(f6d, intrpAxis);
    skipThisHaloExchange = false;
  } else {
    boundary = _boundary;
  }
  f   = f6d.device();
  low = boundary.low(get_intrpAxis());
  up  = boundary.up(get_intrpAxis());
  double cfl{characteristic_ptr->update(_intrpAxis)};
  Kokkos::fence();
  if (cfl >= 1.0) {
    throw std::runtime_error(
        "CFL condition is not met for lagrange "
        "interpolation with maxShift=" +
        std::to_string(cfl) + ">1.0 for axis " + axis_to_string(_intrpAxis));
  }
  characteristic = *characteristic_ptr;
  std::string label{"InterpolateLagrange" + axis_to_string(intrpAxis)};
#if defined(LIKWID_PERFMON)
  #pragma omp parallel
  { LIKWID_MARKER_START(label.c_str()); }
#elif defined(ROCM_PERFMON)
  roctxRangePush(label.c_str());
#endif
  if (get_intrpDim() != contiguousDimension<BaseView>()) {
    Kokkos::parallel_for(label.c_str(), teamPolNonCont, *this);
  } else {
    Kokkos::parallel_for(label.c_str(), teamPolCont, *this);
  }
  Kokkos::fence();
#if LIKWID_PERFMON
  #pragma omp parallel
  { LIKWID_MARKER_STOP(label.c_str()); }
#elif defined(ROCM_PERFMON)
  roctxRangePop();
#endif
  f6d.require_velocity_moments_update();
};

// For some reason clang can not declares this class in the builder pattern.
// Therefore it is declared explicitly in at this point.
template class bsl6d::LagrangeInterpolationPrefetching<
    std::shared_ptr<bsl6d::Constant0DShift>,
    bsl6d::LagrangeInterpolationStencil9,
    static_cast<bsl6d::AxisType>(3)>;

// To instantiate all classes by default call the main builder function
// in here.
std::shared_ptr<Base1dInterpolator>
    LagrangeBuilder::chooseLagrangeInterpolator() {
  return chooseLagrangeCharacteristic<
      std::variant_size_v<CharacteristicVariants> - 1,
      std::variant_size_v<LagrangeInterpolationStencilVariants> - 1, 5>();
}

/*****************************************************************/
/*****************************************************************/
/*

\brief Private and free helper functions

*/
/*****************************************************************/
/*****************************************************************/

/*****************************************************************/
/*

\brief Get displacement of a specific point in distribution
       function

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
KOKKOS_INLINE_FUNCTION double LagrangeInterpolationPrefetching<
    CharacteristicTypePtr,
    StencilType,
    _intrpAxis>::get_shift(size_t const innerIdx,
                           size_t const outerIdx,
                           std::array<size_t, 4> const &idx) const {
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    if constexpr (AxisType::x1 == intrpAxis) {
      return characteristic.shift_x1(innerIdx, idx[3], idx[2], idx[1], idx[0],
                                     outerIdx);
    } else if constexpr (AxisType::x2 == intrpAxis) {
      return characteristic.shift_x2(idx[3], innerIdx, idx[2], idx[1], idx[0],
                                     outerIdx);
    } else if constexpr (AxisType::x3 == intrpAxis) {
      return characteristic.shift_x3(idx[3], idx[2], innerIdx, idx[1], idx[0],
                                     outerIdx);
    } else if constexpr (AxisType::v1 == intrpAxis) {
      return characteristic.shift_v1(idx[3], idx[2], idx[1], innerIdx, idx[0],
                                     outerIdx);
    } else if constexpr (AxisType::v2 == intrpAxis) {
      return characteristic.shift_v2(idx[3], idx[2], idx[1], idx[0], innerIdx,
                                     outerIdx);
    } else if constexpr (AxisType::v3 == intrpAxis) {
      return characteristic.shift_v3(idx[3], idx[2], idx[1], idx[0], outerIdx,
                                     innerIdx);
    }

  } else {
    if constexpr (AxisType::x1 == intrpAxis) {
      return characteristic.shift_x1(innerIdx, outerIdx, idx[0], idx[1], idx[2],
                                     idx[3]);
    } else if constexpr (AxisType::x2 == intrpAxis) {
      return characteristic.shift_x2(outerIdx, innerIdx, idx[0], idx[1], idx[2],
                                     idx[3]);
    } else if constexpr (AxisType::x3 == intrpAxis) {
      return characteristic.shift_x3(outerIdx, idx[0], innerIdx, idx[1], idx[2],
                                     idx[3]);
    } else if constexpr (AxisType::v1 == intrpAxis) {
      return characteristic.shift_v1(outerIdx, idx[0], idx[1], innerIdx, idx[2],
                                     idx[3]);
    } else if constexpr (AxisType::v2 == intrpAxis) {
      return characteristic.shift_v2(outerIdx, idx[0], idx[1], idx[2], innerIdx,
                                     idx[3]);
    } else if constexpr (AxisType::v3 == intrpAxis) {
      return characteristic.shift_v3(outerIdx, idx[0], idx[1], idx[2], idx[3],
                                     innerIdx);
    }
  }

  return 0.0;
}

/*****************************************************************/
/*

\brief Calculate four non interpolation, non contiguous index from
       league_rank

\description Using div-mod operations to calculate 4 indexes
             which are neither contiguous nor interpolation
             (View strides can not be used since league_rank strides
              and view strides are not the same for all dimensions.)

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
KOKKOS_INLINE_FUNCTION std::array<size_t, 4> LagrangeInterpolationPrefetching<
    CharacteristicTypePtr,
    StencilType,
    _intrpAxis>::calc_nonCont_nonIntrp_Idxs(size_t const league_rank) const {
  std::array<size_t, 4> idx;
  size_t residuum;

  idx[3]   = league_rank / (f.extent(leagueRankDim(get_intrpDim())[2]) *
                          f.extent(leagueRankDim(get_intrpDim())[1]) *
                          f.extent(leagueRankDim(get_intrpDim())[0]));
  residuum = league_rank % (f.extent(leagueRankDim(get_intrpDim())[2]) *
                            f.extent(leagueRankDim(get_intrpDim())[1]) *
                            f.extent(leagueRankDim(get_intrpDim())[0]));
  idx[2]   = residuum / (f.extent(leagueRankDim(get_intrpDim())[1]) *
                       f.extent(leagueRankDim(get_intrpDim())[0]));
  residuum = residuum % (f.extent(leagueRankDim(get_intrpDim())[1]) *
                         f.extent(leagueRankDim(get_intrpDim())[0]));
  idx[1]   = residuum / f.extent(leagueRankDim(get_intrpDim())[0]);
  idx[0]   = residuum % f.extent(leagueRankDim(get_intrpDim())[0]);

  return idx;
}

} /* namespace bsl6d */

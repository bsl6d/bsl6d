/*****************************************************************/
/*

\brief Using prefetching algorithm to interpolate in 1 dimension

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#pragma once

#include <fstream>
#include <iostream>

#include <halo.hpp>
#include <interpolatorBase.hpp>
#include <lagrangeStencils.hpp>

#include <Kokkos_Core.hpp>

namespace bsl6d {

// Tags for contiguous and non contiguous interpolation dimensions
struct NonCont {};
struct Cont {};

/*****************************************************************/
// Helper functions defined below in section private an free helper
// functions.
template <typename ViewType>
constexpr size_t contiguousDimension();
template <typename ViewType>
static constexpr size_t get_outerIterationDim();
constexpr std::array<size_t, 4> leagueRankDim(size_t intrpDim);

template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
class LagrangeInterpolationPrefetching : public Base1dInterpolator {
  public:
  void interpolate(DistributionFunction &_f6d) override;

  /*************************************************************/
  using TeamIdxType  = Kokkos::IndexType<size_t>;
  using TeamMembType = Kokkos::TeamPolicy<TeamIdxType>::member_type;
  KOKKOS_INLINE_FUNCTION
  void operator()(NonCont, TeamMembType const &team) const;
  KOKKOS_INLINE_FUNCTION
  void operator()(Cont, TeamMembType const &team) const;

  constexpr AxisType get_intrpAxis() const { return intrpAxis; };
  CharacteristicId get_characteristicId() const override {
    return characteristic_ptr->id();
  };

  protected:
  LagrangeInterpolationPrefetching() = default;
  LagrangeInterpolationPrefetching(
      std::array<size_t, 6> const _Nxv,
      CharacteristicTypePtr const _characteristic_ptr,
      HaloComm const _halo,
      size_t stencilWidth,
      size_t teamSize,
      size_t vectorSize);
  friend class LagrangeBuilder;

  private:
  using BaseView = Kokkos::View<double ******>;
  static constexpr AxisType intrpAxis{_intrpAxis};
  // Internally used as an integer
  constexpr size_t get_intrpDim() const {
    return static_cast<size_t>(intrpAxis);
  };
  CharacteristicTypePtr characteristic_ptr{};
  typename CharacteristicTypePtr::element_type characteristic{};
  StencilType stencil{};
  HaloComm halo{};

  // Data to be interpolated
  Kokkos::View<double ******> f{};
  // Boundary arrays for interpolation
  Kokkos::View<double ******> low{}, up{};

  // Kokkos execution policy
  Kokkos::TeamPolicy<TeamIdxType, Cont> teamPolCont{};
  Kokkos::TeamPolicy<TeamIdxType, NonCont> teamPolNonCont{};

  KOKKOS_INLINE_FUNCTION
  double get_shift(size_t const innerIdx,
                   size_t const outerIdx,
                   std::array<size_t, 4> const &idx) const;
  KOKKOS_INLINE_FUNCTION
  std::array<size_t, 4> calc_nonCont_nonIntrp_Idxs(
      size_t const league_rank) const;
  KOKKOS_INLINE_FUNCTION
  auto get_subview(Kokkos::View<double ******> view,
                   std::array<size_t, 4> const &idx) const {
    if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                 Kokkos::DefaultHostExecutionSpace>) {
      if constexpr (AxisType::x1 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, idx[3], idx[2], idx[1],
                               idx[0], Kokkos::ALL);
      } else if constexpr (AxisType::x2 == intrpAxis) {
        return Kokkos::subview(view, idx[3], Kokkos::ALL, idx[2], idx[1],
                               idx[0], Kokkos::ALL);
      } else if constexpr (AxisType::x3 == intrpAxis) {
        return Kokkos::subview(view, idx[3], idx[2], Kokkos::ALL, idx[1],
                               idx[0], Kokkos::ALL);
      } else if constexpr (AxisType::v1 == intrpAxis) {
        return Kokkos::subview(view, idx[3], idx[2], idx[1], Kokkos::ALL,
                               idx[0], Kokkos::ALL);
      } else if constexpr (AxisType::v2 == intrpAxis) {
        return Kokkos::subview(view, idx[3], idx[2], idx[1], idx[0],
                               Kokkos::ALL, Kokkos::ALL);
      } else if constexpr (AxisType::v3 == intrpAxis) {
        return Kokkos::subview(view, idx[3], idx[2], idx[1], idx[0],
                               Kokkos::ALL, Kokkos::ALL);
      }
    } else {
      if constexpr (AxisType::x1 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, Kokkos::ALL, idx[0], idx[1],
                               idx[2], idx[3]);
      } else if constexpr (AxisType::x2 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, Kokkos::ALL, idx[0], idx[1],
                               idx[2], idx[3]);
      } else if constexpr (AxisType::x3 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, idx[0], Kokkos::ALL, idx[1],
                               idx[2], idx[3]);
      } else if constexpr (AxisType::v1 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, idx[0], idx[1], Kokkos::ALL,
                               idx[2], idx[3]);
      } else if constexpr (AxisType::v2 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, idx[0], idx[1], idx[2],
                               Kokkos::ALL, idx[3]);
      } else if constexpr (AxisType::v3 == intrpAxis) {
        return Kokkos::subview(view, Kokkos::ALL, idx[0], idx[1], idx[2],
                               idx[3], Kokkos::ALL);
      }
    }
  }
};

/*****************************************************************/
/*

\brief Interpolate one plane x1-intrpDim of the 6d distribution
       function

\param[in] team - Current team that executes interpolation on a
                  certain plane.

\description Interpolation in i3 dimension:

     f(i0,i1,i2,i3,i4,i5) =
            sum_(i=-1)^1 f(i0,i1,i2,i3+i,i4,i5) p[i]

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
KOKKOS_INLINE_FUNCTION void LagrangeInterpolationPrefetching<
    CharacteristicTypePtr,
    StencilType,
    _intrpAxis>::operator()(NonCont, TeamMembType const &team) const {
  using SharedView =
      Kokkos::View<double **,
                   Kokkos::DefaultExecutionSpace::scratch_memory_space,
                   Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
  SharedView f_s{};
  size_t intrpDimSize_s = f.extent(get_intrpDim()) + up.extent(get_intrpDim()) +
                          low.extent(get_intrpDim());
  f_s = SharedView{team.team_scratch(0),
                   f.extent(contiguousDimension<BaseView>()), intrpDimSize_s};

  std::array<size_t, 4> const idxNonContNonIntrp{
      calc_nonCont_nonIntrp_Idxs(team.league_rank())};
  auto low_sub = get_subview(low, idxNonContNonIntrp);
  auto f_sub   = get_subview(f, idxNonContNonIntrp);
  auto up_sub  = get_subview(up, idxNonContNonIntrp);

  /***************************************************************/
  Kokkos::parallel_for(
      Kokkos::TeamThreadRange(team, low.extent(get_intrpDim())),
      [&](size_t const &intrpIdx) {
        Kokkos::parallel_for(
            Kokkos::ThreadVectorRange(
                team, low.extent(contiguousDimension<BaseView>())),
            [&](size_t const &contIdx) {
              size_t const upIdx{intrpIdx + f.extent(get_intrpDim()) +
                                 low.extent(get_intrpDim())};
              if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                           Kokkos::DefaultHostExecutionSpace>) {
                f_s(contIdx, intrpIdx) = low_sub(intrpIdx, contIdx);
                f_s(contIdx, upIdx)    = up_sub(intrpIdx, contIdx);
              } else {
                f_s(contIdx, intrpIdx) = low_sub(contIdx, intrpIdx);
                f_s(contIdx, upIdx)    = up_sub(contIdx, intrpIdx);
              }
            });
      });

  Kokkos::parallel_for(
      Kokkos::TeamThreadRange(team, f.extent(get_intrpDim())),
      [&](size_t const &intrpIdx) {
        Kokkos::parallel_for(
            Kokkos::ThreadVectorRange(
                team, f.extent(contiguousDimension<BaseView>())),
            [&](size_t const &contIdx) {
              size_t const fIdx{intrpIdx + low.extent(get_intrpDim())};
              if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                           Kokkos::DefaultHostExecutionSpace>) {
                f_s(contIdx, fIdx) = f_sub(intrpIdx, contIdx);
              } else {
                f_s(contIdx, fIdx) = f_sub(contIdx, intrpIdx);
              }
            });
      });
  team.team_barrier();

  /***************************************************************/
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team, f.extent(get_intrpDim())),
        [&](size_t const &intrpIdx) {
          Kokkos::parallel_for(
              Kokkos::TeamVectorRange(
                  team, f.extent(contiguousDimension<BaseView>())),
              [&](size_t const &contIdx) {
                size_t const fIdx{intrpIdx + low.extent(get_intrpDim())};
                double shift{get_shift(intrpIdx, contIdx, idxNonContNonIntrp)};
                f_sub(intrpIdx, contIdx) =
                    stencil.interpolateIdx(&f_s(contIdx, fIdx), shift, 1);
              });
        });
  } else {
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team,
                                f.extent(contiguousDimension<BaseView>())),
        [&](size_t const &contIdx) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, f.extent(get_intrpDim())),
              [&](size_t const &intrpIdx) {
                size_t const fIdx{intrpIdx + low.extent(get_intrpDim())};
                double shift{get_shift(intrpIdx, contIdx, idxNonContNonIntrp)};
                f_sub(contIdx, intrpIdx) = stencil.interpolateIdx(
                    &f_s(contIdx, fIdx), shift, f_s.stride_1());
              });
        });
  }
}

/*****************************************************************/
/*

\brief Interpolate one plane x1-intrpDim of the 6d distribution
       function

\param[in] team - Current team that executes interpolation on a
                  certain plane.

\description Interpolation in i3 dimension:

     fres(i0,i1,i2,i3,i4,i5) =
            sum_(i=-1)^1 f(i0,i1,i2,i3+i,i4,i5) p[i]

*/
/*****************************************************************/
template <typename CharacteristicTypePtr,
          typename StencilType,
          AxisType _intrpAxis>
KOKKOS_INLINE_FUNCTION void LagrangeInterpolationPrefetching<
    CharacteristicTypePtr,
    StencilType,
    _intrpAxis>::operator()(Cont, TeamMembType const &team) const {
  using SharedView =
      Kokkos::View<double **,
                   Kokkos::DefaultExecutionSpace::scratch_memory_space,
                   Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
  SharedView f_s{};
  size_t intrpDimSize_s = f.extent(get_intrpDim()) + up.extent(get_intrpDim()) +
                          low.extent(get_intrpDim());
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    f_s =
        SharedView{team.team_scratch(0),
                   f.extent(get_outerIterationDim<BaseView>()), intrpDimSize_s};
  } else {
    f_s = SharedView{team.team_scratch(0), intrpDimSize_s,
                     f.extent(get_outerIterationDim<BaseView>())};
  }

  std::array<size_t, 4> const idxNonContNonIntrp{
      calc_nonCont_nonIntrp_Idxs(team.league_rank())};
  auto low_sub = get_subview(low, idxNonContNonIntrp);
  auto f_sub   = get_subview(f, idxNonContNonIntrp);
  auto up_sub  = get_subview(up, idxNonContNonIntrp);

  Kokkos::parallel_for(
      Kokkos::TeamThreadRange(team,
                              low.extent(get_outerIterationDim<BaseView>())),
      [&](size_t const &nonContIdx) {
        Kokkos::parallel_for(
            Kokkos::ThreadVectorRange(team, low.extent(get_intrpDim())),
            [&](size_t const &contIdx) {
              size_t const upIdx{contIdx + f.extent(get_intrpDim()) +
                                 low.extent(get_intrpDim())};
              if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                           Kokkos::DefaultHostExecutionSpace>) {
                f_s(nonContIdx, contIdx) = low_sub(nonContIdx, contIdx);
                f_s(nonContIdx, upIdx)   = up_sub(nonContIdx, contIdx);
              } else {
                f_s(contIdx, nonContIdx) = low_sub(contIdx, nonContIdx);
                f_s(upIdx, nonContIdx)   = up_sub(contIdx, nonContIdx);
              }
            });
      });

  Kokkos::parallel_for(
      Kokkos::TeamThreadRange(team,
                              f.extent(get_outerIterationDim<BaseView>())),
      [&](size_t const &nonContIdx) {
        Kokkos::parallel_for(
            Kokkos::ThreadVectorRange(team, f.extent(get_intrpDim())),
            [&](size_t const &contIdx) {
              size_t const fIdx{contIdx + low.extent(get_intrpDim())};
              if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                                           Kokkos::DefaultHostExecutionSpace>) {
                f_s(nonContIdx, fIdx) = f_sub(nonContIdx, contIdx);
              } else {
                f_s(fIdx, nonContIdx) = f_sub(contIdx, nonContIdx);
              }
            });
      });
  team.team_barrier();

  /***************************************************************/
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team,
                                f.extent(get_outerIterationDim<BaseView>())),
        [&](size_t const &nonContIdx) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, f.extent(get_intrpDim())),
              [&](size_t const &contIdx) {
                size_t const fIdx{contIdx + low.extent(get_intrpDim())};
                double shift{
                    get_shift(contIdx, nonContIdx, idxNonContNonIntrp)};
                f_sub(nonContIdx, contIdx) =
                    stencil.interpolateIdx(&f_s(nonContIdx, fIdx), shift, 1);
              });
        });
  } else {
    Kokkos::parallel_for(
        Kokkos::TeamThreadRange(team,
                                f.extent(get_outerIterationDim<BaseView>())),
        [&](size_t const &nonContIdx) {
          Kokkos::parallel_for(
              Kokkos::ThreadVectorRange(team, f.extent(get_intrpDim())),
              [&](size_t const &contIdx) {
                size_t const fIdx{contIdx + low.extent(get_intrpDim())};
                double shift{
                    get_shift(contIdx, nonContIdx, idxNonContNonIntrp)};
                f_sub(contIdx, nonContIdx) =
                    stencil.interpolateIdx(&f_s(fIdx, nonContIdx), shift, 1);
              });
        });
  }
}

/*****************************************************************/
/*****************************************************************/
/*

\brief Private and free helper functions

*/
/*****************************************************************/
/*****************************************************************/

/*****************************************************************/
/*

\brief Get the dimension of View that is contiguous in memory

\param[in] ViewType - View to get contiguous dimension for

*/
/*****************************************************************/
template <typename ViewType>
constexpr size_t contiguousDimension() {
  using layout = typename ViewType::array_layout;
  static_assert(std::is_same<layout, Kokkos::LayoutLeft>::value ||
                    std::is_same<layout, Kokkos::LayoutRight>::value,
                "Continuous dimension could not be determined.\n -> Layout is "
                "neither LayoutRight nor LayoutLeft");

  if constexpr (std::is_same<layout, Kokkos::LayoutLeft>::value) { return 0; }

  if constexpr (std::is_same<layout, Kokkos::LayoutRight>::value) {
    return ViewType::rank - 1;
  }
}

/*****************************************************************/
/*

\brief The outer dimension needed in the team policies can be determined
       with this procedure.
       (Needed specifically if contiguous dimension is interpolated)

\param[in] ViewType - View to get contiguous dimension for

*/
/*****************************************************************/
template <typename ViewType>
static constexpr size_t get_outerIterationDim() {
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    return contiguousDimension<ViewType>() - 1;
  }
  // else
  return contiguousDimension<ViewType>() + 1;
}

/*****************************************************************/
/*

\brief Get dimensions not needed for prefetching interpolation

\param[in] intrpDim - Interpolation dimension

\description The returned array contains non contiguous dimension
             and non interpolation dimension starting with starting
             from lowest dimension
             e.g.
              -> contDim: 0; intrpDim: 3
             return {1,2,4,5}
              -> contDim: 5; intrpDim: 3
             return {4,2,1,0}

*/
/*****************************************************************/
constexpr std::array<size_t, 4> leagueRankDim(size_t intrpDim) {
  std::array<size_t, 4> a{};
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    size_t dim{0};
    for (size_t i = 0; i < 4; i++) {
      if (intrpDim == dim) dim++;
      a[3 - i] = dim++;
    }
  } else {
    size_t dim{5};
    for (size_t i = 0; i < 4; i++) {
      if (intrpDim == dim) dim--;
      a[3 - i] = dim--;
    }
  }

  return a;
}

}  // namespace bsl6d

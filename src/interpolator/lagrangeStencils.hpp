/*****************************************************************/
/*

\brief Lagrange Stenicls that can be used for interpolation

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   12.01.2021

\description
  To add a new stencil:
    1. Define the new stencil-class at the top of this file
    2. Add the new stencil class to the stencilVariants std::variant.
    3. Create the new stencil class with the SAME interface as the
       currently available stencil classes using TDD.
    4. Add the new stencil to the factory method.

*/
/*****************************************************************/

#pragma once

#include <variant>

#include <Kokkos_Core.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief Stencil classes that are available and variant to store any
       of the available classes.

*/
/*****************************************************************/
class LagrangeInterpolationStencil2;
class LagrangeInterpolationStencil3;
class LagrangeInterpolationStencil4;
class LagrangeInterpolationStencil5;
class LagrangeInterpolationStencil6;
class LagrangeInterpolationStencil7;
class LagrangeInterpolationStencil8;
class LagrangeInterpolationStencil9;
class LagrangeInterpolationStencil10;
class LagrangeInterpolationStencil11;
class LagrangeInterpolationStencil12;

// Only use high order stencils to reduce compile time in
// lagrange interpolation
using LagrangeInterpolationStencilVariants =
    std::variant<LagrangeInterpolationStencil2,
                 LagrangeInterpolationStencil3,
                 LagrangeInterpolationStencil4,
                 LagrangeInterpolationStencil5,
                 LagrangeInterpolationStencil6,
                 LagrangeInterpolationStencil7,
                 LagrangeInterpolationStencil8,
                 LagrangeInterpolationStencil9,
                 LagrangeInterpolationStencil10,
                 LagrangeInterpolationStencil11,
                 LagrangeInterpolationStencil12>;
/*****************************************************************/
/*

\brief FactoryMethod to return different stencil classes inside a
       variant.

*/
/*****************************************************************/
LagrangeInterpolationStencilVariants create_lagrange_interpolation_stencil(
    size_t const width);

/*****************************************************************/
/*

\brief Two point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil2 {
  public:
  LagrangeInterpolationStencil2() = default;

  static constexpr size_t width{2};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const { return 1.0 - dx; };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const { return dx; };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first =
        f_ptr[0 - 1 * stride] * poly0(dx_neg) + f_ptr[0] * poly1(dx_neg);
    res.second = f_ptr[0] * poly0(dx) + f_ptr[0 + 1 * stride] * poly1(dx);

    return dx < 0 ? res.first : res.second;
  };
};

/*****************************************************************/
/*

\brief Three point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil3 {
  public:
  LagrangeInterpolationStencil3() = default;

  static constexpr size_t width{3};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const { return dx * (dx - 1) * 0.5; };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const { return 1. - dx * dx; };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const { return dx * (dx + 1) * 0.5; };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - stride] * poly0(dx) + f_ptr[0] * poly1(dx) +
           f_ptr[0 + stride] * poly2(dx);
  };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        std::array<double, width> const& poly,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - stride] * poly[0] + f_ptr[0 + stride] * poly[2] +
           f_ptr[0] * poly[1];
  };
};

/*****************************************************************/
/*

\brief Four point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil4 {
  public:
  LagrangeInterpolationStencil4() = default;

  static constexpr size_t width{4};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    return -dx * (dx - 1.0) * (dx - 2.0) * inv_6;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    return (dx * dx - 1.0) * (dx - 2.0) * inv_2;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    return -dx * (dx + 1.0) * (dx - 2.0) * inv_2;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const { return dx * (dx * dx - 1.0) * inv_6; };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first = f_ptr[0 - 2 * stride] * poly0(dx_neg) +
                f_ptr[0 - 1 * stride] * poly1(dx_neg) +
                f_ptr[0] * poly2(dx_neg) +
                f_ptr[0 + 1 * stride] * poly3(dx_neg);
    res.second = f_ptr[0 - 1 * stride] * poly0(dx) + f_ptr[0] * poly1(dx) +
                 f_ptr[0 + 1 * stride] * poly2(dx) +
                 f_ptr[0 + 2 * stride] * poly3(dx);
    return dx < 0 ? res.first : res.second;
  };

  private:
  static constexpr double inv_2{1. / 2.};
  static constexpr double inv_6{1. / 6.};
};

/*****************************************************************/
/*

\brief Five point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil5 {
  public:
  LagrangeInterpolationStencil5() = default;

  static constexpr size_t width{5};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return dx * (dx - 2.) * (dxdx - 1.) * inv_24;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return -dx * (dx - 1.) * (dxdx - 4.) * inv_6;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 4.) * (dxdx - 1.) * inv_4;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return -(dx + 1.) * dx * (dxdx - 4.) * inv_6;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return (dx + 2.) * dx * (dxdx - 1.) * inv_24;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx), poly4(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 2 * stride] * poly0(dx) +
           f_ptr[0 + 2 * stride] * poly4(dx) +
           f_ptr[0 - 1 * stride] * poly1(dx) +
           f_ptr[0 + 1 * stride] * poly3(dx) + f_ptr[0] * poly2(dx);
  };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        std::array<double, width> const& poly,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 2 * stride] * poly[0] + f_ptr[0 + 2 * stride] * poly[4] +
           f_ptr[0 - 1 * stride] * poly[1] + f_ptr[0 + 1 * stride] * poly[3] +
           f_ptr[0] * poly[2];
  };

  private:
  static constexpr double inv_24{1. / 24.};
  static constexpr double inv_6{1. / 6.};
  static constexpr double inv_4{1. / 4.};
};

/*****************************************************************/
/*

\brief Six point lagrange interpolation stencil

        */
/*****************************************************************/
class LagrangeInterpolationStencil6 {
  public:
  LagrangeInterpolationStencil6() = default;

  static constexpr size_t width{6};
  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    return -dx * (dx * dx - 1.0) * (dx - 2.0) * (dx - 3.0) * inv_120;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    return dx * (dx - 1.0) * (dx * dx - 4.0) * (dx - 3.0) * inv_24;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx{dx * dx};
    return -(dxdx - 1.0) * (dxdx - 4.0) * (dx - 3.0) * inv_12;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    return dx * (dx + 1.0) * (dx * dx - 4.0) * (dx - 3.0) * inv_12;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    return -dx * (dx * dx - 1.0) * (dx + 2.0) * (dx - 3.0) * inv_24;
  }
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx{dx * dx};
    return dx * (dxdx - 1.0) * (dxdx - 4.0) * inv_120;
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first = f_ptr[0 - 3 * stride] * poly0(dx_neg) +
                f_ptr[0 - 2 * stride] * poly1(dx_neg) +
                f_ptr[0 - 1 * stride] * poly2(dx_neg) +
                f_ptr[0] * poly3(dx_neg) +
                f_ptr[0 + 1 * stride] * poly4(dx_neg) +
                f_ptr[0 + 2 * stride] * poly5(dx_neg);
    res.second =
        f_ptr[0 - 2 * stride] * poly0(dx) + f_ptr[0 - 1 * stride] * poly1(dx) +
        f_ptr[0] * poly2(dx) + f_ptr[0 + 1 * stride] * poly3(dx) +
        f_ptr[0 + 2 * stride] * poly4(dx) + f_ptr[0 + 3 * stride] * poly5(dx);
    return dx < 0 ? res.first : res.second;
  };

  private:
  static constexpr double inv_12{1. / 12.};
  static constexpr double inv_24{1. / 24.};
  static constexpr double inv_120{1. / 120.};
};

/*****************************************************************/
/*

\brief Seven point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil7 {
  public:
  LagrangeInterpolationStencil7() = default;

  static constexpr size_t width{7};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return dx * (dx - 3.) * (dxdx - 4.) * (dxdx - 1.) * inv_720;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return -dx * (dx - 2.) * (dxdx - 9.) * (dxdx - 1.) * inv_120;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return dx * (dx - 1.) * (dxdx - 9.) * (dxdx - 4.) * inv_48;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_36;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return (dx + 1.) * dx * (dxdx - 9.) * (dxdx - 4.) * inv_48;
  };
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx = dx * dx;
    return -(dx + 2.) * dx * (dxdx - 9.) * (dxdx - 1.) * inv_120;
  };
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx = dx * dx;
    return (dx + 3.) * dx * (dxdx - 4.) * (dxdx - 1.) * inv_720;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx),
            poly4(dx), poly5(dx), poly6(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 3 * stride] * poly0(dx) +
           f_ptr[0 + 3 * stride] * poly6(dx) +
           f_ptr[0 - 2 * stride] * poly1(dx) +
           f_ptr[0 + 2 * stride] * poly5(dx) +
           f_ptr[0 - 1 * stride] * poly2(dx) +
           f_ptr[0 + 1 * stride] * poly4(dx) + f_ptr[0] * poly3(dx);
  };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        std::array<double, width> const& poly,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 3 * stride] * poly[0] + f_ptr[0 + 3 * stride] * poly[6] +
           f_ptr[0 - 2 * stride] * poly[1] + f_ptr[0 + 2 * stride] * poly[5] +
           f_ptr[0 - 1 * stride] * poly[2] + f_ptr[0 + 1 * stride] * poly[4] +
           f_ptr[0] * poly[3];
  }

  private:
  static constexpr double inv_720{1. / 720.};
  static constexpr double inv_120{1. / 120.};
  static constexpr double inv_48{1. / 48.};
  static constexpr double inv_36{1. / 36.};
};

/*****************************************************************/
/*

\brief Six point lagrange interpolation stencil

        */
/*****************************************************************/
class LagrangeInterpolationStencil8 {
  public:
  LagrangeInterpolationStencil8() = default;

  static constexpr size_t width{8};
  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx{dx * dx};
    return -dx * (dx - 3.0) * (dx - 4.0) * (dxdx - 4.0) * (dxdx - 1.0) *
           inv_5040;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx{dx * dx};
    return dx * (dx - 2.0) * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 1.0) * inv_720;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx{dx * dx};
    return -dx * (dx - 1.0) * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           inv_240;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx{dx * dx};
    return (dx - 4.0) * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * inv_144;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx{dx * dx};
    return -(dx + 1.0) * dx * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           inv_144;
    ;
  }
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx{dx * dx};
    return (dx + 2.0) * dx * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 1.0) * inv_240;
  }
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx{dx * dx};
    return -(dx + 3.0) * dx * (dx - 4.0) * (dxdx - 4.0) * (dxdx - 1.0) *
           inv_720;
  }
  KOKKOS_INLINE_FUNCTION
  double poly7(double const dx) const {
    double dxdx{dx * dx};
    return dx * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * inv_5040;
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first = f_ptr[0 - 4 * stride] * poly0(dx_neg) +
                f_ptr[0 - 3 * stride] * poly1(dx_neg) +
                f_ptr[0 - 2 * stride] * poly2(dx_neg) +
                f_ptr[0 - 1 * stride] * poly3(dx_neg) +
                f_ptr[0] * poly4(dx_neg) +
                f_ptr[0 + 1 * stride] * poly5(dx_neg) +
                f_ptr[0 + 2 * stride] * poly6(dx_neg) +
                f_ptr[0 + 3 * stride] * poly7(dx_neg);
    res.second =
        f_ptr[0 - 3 * stride] * poly0(dx) + f_ptr[0 - 2 * stride] * poly1(dx) +
        f_ptr[0 - 1 * stride] * poly2(dx) + f_ptr[0] * poly3(dx) +
        f_ptr[0 + 1 * stride] * poly4(dx) + f_ptr[0 + 2 * stride] * poly5(dx) +
        f_ptr[0 + 3 * stride] * poly6(dx) + f_ptr[0 + 4 * stride] * poly7(dx);
    return dx < 0 ? res.first : res.second;
  };

  private:
  static constexpr double inv_144{1.0 / 144.0};
  static constexpr double inv_240{1.0 / 240.0};
  static constexpr double inv_720{1.0 / 720.0};
  static constexpr double inv_5040{1.0 / 5040.0};
};

/*****************************************************************/
/*

\brief Nine point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil9 {
  public:
  LagrangeInterpolationStencil9() = default;

  static constexpr size_t width{9};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return dx * (dx - 4.) * (dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_40320;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return -dx * (dx - 3.) * (dxdx - 16.) * (dxdx - 4.) * (dxdx - 1.) *
           inv_5040;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return dx * (dx - 2.) * (dxdx - 16.) * (dxdx - 9.) * (dxdx - 1.) * inv_1440;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return -dx * (dx - 1.) * (dxdx - 16.) * (dxdx - 9.) * (dxdx - 4.) * inv_720;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 16.) * (dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_576;
  };
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx = dx * dx;
    return -(dx + 1.) * dx * (dxdx - 16.) * (dxdx - 9.) * (dxdx - 4.) * inv_720;
  };
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx = dx * dx;
    return (dx + 2.) * dx * (dxdx - 16.) * (dxdx - 9.) * (dxdx - 1.) * inv_1440;
  };
  KOKKOS_INLINE_FUNCTION
  double poly7(double const dx) const {
    double dxdx = dx * dx;
    return -(dx + 3) * dx * (dxdx - 16.) * (dxdx - 4.) * (dxdx - 1.) * inv_5040;
  };
  KOKKOS_INLINE_FUNCTION
  double poly8(double const dx) const {
    double dxdx = dx * dx;
    return (dx + 4) * dx * (dxdx - 9.) * (dxdx - 4.) * (dxdx - 1.) * inv_40320;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx), poly4(dx),
            poly5(dx), poly6(dx), poly7(dx), poly8(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 4 * stride] * poly0(dx) +
           f_ptr[0 + 4 * stride] * poly8(dx) +
           f_ptr[0 - 3 * stride] * poly1(dx) +
           f_ptr[0 + 3 * stride] * poly7(dx) +
           f_ptr[0 - 2 * stride] * poly2(dx) +
           f_ptr[0 + 2 * stride] * poly6(dx) +
           f_ptr[0 + 1 * stride] * poly5(dx) +
           f_ptr[0 - 1 * stride] * poly3(dx) + f_ptr[0] * poly4(dx);
  };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        std::array<double, width> const& poly,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 4 * stride] * poly[0] + f_ptr[0 + 4 * stride] * poly[8] +
           f_ptr[0 - 3 * stride] * poly[1] + f_ptr[0 + 3 * stride] * poly[7] +
           f_ptr[0 - 2 * stride] * poly[2] + f_ptr[0 + 2 * stride] * poly[6] +
           f_ptr[0 + 1 * stride] * poly[5] + f_ptr[0 - 1 * stride] * poly[3] +
           f_ptr[0] * poly[4];
  };

  private:
  static constexpr double inv_40320{1. / 40320.};
  static constexpr double inv_5040{1. / 5040.};
  static constexpr double inv_1440{1. / 1440.};
  static constexpr double inv_720{1. / 720.};
  static constexpr double inv_576{1. / 576.};
};

/*****************************************************************/
/*

\brief Ten point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil10 {
  public:
  LagrangeInterpolationStencil10() = default;

  static constexpr size_t width{10};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 5.0) * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_362880;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 5.0) * (dxdx - 16.0) * (dx - 3.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_40320;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dx - 2.0) *
           (dxdx - 1.0) * dx * m_inv_10080;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dx - 1.0) * dx * m_inv_4320;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * m_inv_2880;
  };
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) * dx *
           (dx + 1.0) * m_inv_2880;
  };
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 1.0) * dx *
           (dx + 2.0) * m_inv_4320;
  };
  KOKKOS_INLINE_FUNCTION
  double poly7(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 5.0) * (dxdx - 16.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           (dx + 3.0) * m_inv_10080;
  };
  KOKKOS_INLINE_FUNCTION
  double poly8(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 5.0) * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           (dx + 4.0) * m_inv_40320;
  };
  KOKKOS_INLINE_FUNCTION
  double poly9(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           m_inv_362880;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx), poly4(dx),
            poly5(dx), poly6(dx), poly7(dx), poly8(dx), poly9(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first = f_ptr[0 - 5 * stride] * poly0(dx_neg) +
                f_ptr[0 - 4 * stride] * poly1(dx_neg) +
                f_ptr[0 - 3 * stride] * poly2(dx_neg) +
                f_ptr[0 - 2 * stride] * poly3(dx_neg) +
                f_ptr[0 - 1 * stride] * poly4(dx_neg) +
                f_ptr[0] * poly5(dx_neg) +
                f_ptr[0 + 1 * stride] * poly6(dx_neg) +
                f_ptr[0 + 2 * stride] * poly7(dx_neg) +
                f_ptr[0 + 3 * stride] * poly8(dx_neg) +
                f_ptr[0 + 4 * stride] * poly9(dx_neg);
    res.second =
        f_ptr[0 - 4 * stride] * poly0(dx) + f_ptr[0 - 3 * stride] * poly1(dx) +
        f_ptr[0 - 2 * stride] * poly2(dx) + f_ptr[0 - 1 * stride] * poly3(dx) +
        f_ptr[0] * poly4(dx) + f_ptr[0 + 1 * stride] * poly5(dx) +
        f_ptr[0 + 2 * stride] * poly6(dx) + f_ptr[0 + 3 * stride] * poly7(dx) +
        f_ptr[0 + 4 * stride] * poly8(dx) + f_ptr[0 + 5 * stride] * poly9(dx);
    return dx < 0 ? res.first : res.second;
  };

  private:
  static constexpr double m_inv_362880{1.0 / 362880.0};
  static constexpr double m_inv_40320{1.0 / 40320.0};
  static constexpr double m_inv_10080{1.0 / 10080.0};
  static constexpr double m_inv_4320{1.0 / 4320.0};
  static constexpr double m_inv_2880{1.0 / 2880.0};
};

/*****************************************************************/
/*

\brief Eleven point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil11 {
  public:
  LagrangeInterpolationStencil11() = default;

  static constexpr size_t width{11};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_3628800;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 25.0) * (dx - 4.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_362880;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 25.0) * (dxdx - 16.0) * (dx - 3.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_80640;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dx - 2.0) *
           (dxdx - 1.0) * dx * m_inv_30240;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dx - 1.0) * dx * m_inv_17280;
  };
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * m_inv_14400;
  };
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) * dx *
           (dx + 1.0) * m_inv_17280;
  };
  KOKKOS_INLINE_FUNCTION
  double poly7(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 1.0) * dx *
           (dx + 2.0) * m_inv_30240;
  };
  KOKKOS_INLINE_FUNCTION
  double poly8(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           (dx + 3.0) * m_inv_80640;
  };
  KOKKOS_INLINE_FUNCTION
  double poly9(double const dx) const {
    double dxdx = dx * dx;
    return -(dxdx - 25.0) * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           (dx + 4.0) * m_inv_362880;
  };
  KOKKOS_INLINE_FUNCTION
  double poly10(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) * (dxdx - 1.0) * dx *
           (dx + 5.0) * m_inv_3628800;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx), poly4(dx), poly5(dx),
            poly6(dx), poly7(dx), poly8(dx), poly9(dx), poly10(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 5 * stride] * poly0(dx) +
           f_ptr[0 + 5 * stride] * poly10(dx) +
           f_ptr[0 - 4 * stride] * poly1(dx) +
           f_ptr[0 + 4 * stride] * poly9(dx) +
           f_ptr[0 - 3 * stride] * poly2(dx) +
           f_ptr[0 + 3 * stride] * poly8(dx) +
           f_ptr[0 - 2 * stride] * poly3(dx) +
           f_ptr[0 + 2 * stride] * poly7(dx) +
           f_ptr[0 - 1 * stride] * poly4(dx) +
           f_ptr[0 + 1 * stride] * poly6(dx) + f_ptr[0] * poly5(dx);
  };
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        std::array<double, width> const& poly,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    return f_ptr[0 - 5 * stride] * poly[0] + f_ptr[0 + 5 * stride] * poly[10] +
           f_ptr[0 - 4 * stride] * poly[1] + f_ptr[0 + 4 * stride] * poly[9] +
           f_ptr[0 - 3 * stride] * poly[2] + f_ptr[0 + 3 * stride] * poly[8] +
           f_ptr[0 - 2 * stride] * poly[3] + f_ptr[0 + 2 * stride] * poly[7] +
           f_ptr[0 - 1 * stride] * poly[4] + f_ptr[0 + 1 * stride] * poly[6] +
           f_ptr[0] * poly[5];
  };

  private:
  static constexpr double m_inv_3628800{1.0 / 3628800.0};
  static constexpr double m_inv_362880{1.0 / 362880.0};
  static constexpr double m_inv_80640{1.0 / 80640.0};
  static constexpr double m_inv_30240{1.0 / 30240.0};
  static constexpr double m_inv_17280{1.0 / 17280.0};
  static constexpr double m_inv_14400{1.0 / 14400.0};
};

/*****************************************************************/
/*

\brief Twelve point lagrange interpolation stencil

*/
/*****************************************************************/
class LagrangeInterpolationStencil12 {
  public:
  LagrangeInterpolationStencil12() = default;

  static constexpr size_t width{12};

  KOKKOS_INLINE_FUNCTION
  double poly0(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dx - 5.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dxdx - 4.0) * (dxdx - 1.0) * dx * m_inv_39916800;
  };
  KOKKOS_INLINE_FUNCTION
  double poly1(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 6.0) * (dxdx - 25.0) * (dx - 4.0) * (dxdx - 9.0) *
           (dxdx - 4.0) * (dxdx - 1.0) * dx * m_inv_3628800;
  };
  KOKKOS_INLINE_FUNCTION
  double poly2(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dx - 3.0) *
           (dxdx - 4.0) * (dxdx - 1.0) * dx * m_inv_725760;
  };
  KOKKOS_INLINE_FUNCTION
  double poly3(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dx - 2.0) * (dxdx - 1.0) * dx * m_inv_241920;
  };
  KOKKOS_INLINE_FUNCTION
  double poly4(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dxdx - 4.0) * (dx - 1.0) * dx * m_inv_120960;
  };
  KOKKOS_INLINE_FUNCTION
  double poly5(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dxdx - 4.0) * (dxdx - 1.0) * m_inv_86400;
  };
  KOKKOS_INLINE_FUNCTION
  double poly6(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dxdx - 4.0) * dx * (dx + 1.0) * m_inv_86400;
  };
  KOKKOS_INLINE_FUNCTION
  double poly7(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) *
           (dxdx - 1.0) * dx * (dx + 2.0) * m_inv_120960;
  };
  KOKKOS_INLINE_FUNCTION
  double poly8(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * (dx + 3.0) * m_inv_241920;
  };
  KOKKOS_INLINE_FUNCTION
  double poly9(double const dx) const {
    double dxdx = dx * dx;
    return (dx - 6.0) * (dxdx - 25.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * (dx + 4.0) * m_inv_725760;
  };
  KOKKOS_INLINE_FUNCTION
  double poly10(double const dx) const {
    double dxdx = dx * dx;
    return -(dx - 6.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * (dx + 5.0) * m_inv_3628800;
  };
  KOKKOS_INLINE_FUNCTION
  double poly11(double const dx) const {
    double dxdx = dx * dx;
    return (dxdx - 25.0) * (dxdx - 16.0) * (dxdx - 9.0) * (dxdx - 4.0) *
           (dxdx - 1.0) * dx * m_inv_39916800;
  };
  KOKKOS_INLINE_FUNCTION
  std::array<double, width> lagrange_basis_poly(double const dx) const {
    return {poly0(dx), poly1(dx), poly2(dx), poly3(dx), poly4(dx),  poly5(dx),
            poly6(dx), poly7(dx), poly8(dx), poly9(dx), poly10(dx), poly11(dx)};
  }
  KOKKOS_INLINE_FUNCTION
  double interpolateIdx(double const* f_ptr,
                        double const dx,
                        size_t const stride) const {
    // const double* f_ptr = &f;
    std::pair<double, double> res{};
    double dx_neg{1.0 + dx};
    res.first = f_ptr[0 - 6 * stride] * poly0(dx_neg) +
                f_ptr[0 - 5 * stride] * poly1(dx_neg) +
                f_ptr[0 - 4 * stride] * poly2(dx_neg) +
                f_ptr[0 - 3 * stride] * poly3(dx_neg) +
                f_ptr[0 - 2 * stride] * poly4(dx_neg) +
                f_ptr[0 - 1 * stride] * poly5(dx_neg) +
                f_ptr[0] * poly6(dx_neg) +
                f_ptr[0 + 1 * stride] * poly7(dx_neg) +
                f_ptr[0 + 2 * stride] * poly8(dx_neg) +
                f_ptr[0 + 3 * stride] * poly9(dx_neg) +
                f_ptr[0 + 4 * stride] * poly10(dx_neg) +
                f_ptr[0 + 5 * stride] * poly11(dx_neg);
    res.second =
        f_ptr[0 - 5 * stride] * poly0(dx) + f_ptr[0 - 4 * stride] * poly1(dx) +
        f_ptr[0 - 3 * stride] * poly2(dx) + f_ptr[0 - 2 * stride] * poly3(dx) +
        f_ptr[0 - 1 * stride] * poly4(dx) + f_ptr[0] * poly5(dx) +
        f_ptr[0 + 1 * stride] * poly6(dx) + f_ptr[0 + 2 * stride] * poly7(dx) +
        f_ptr[0 + 3 * stride] * poly8(dx) + f_ptr[0 + 4 * stride] * poly9(dx) +
        f_ptr[0 + 5 * stride] * poly10(dx) + f_ptr[0 + 6 * stride] * poly11(dx);
    return dx < 0 ? res.first : res.second;
  };

  private:
  static constexpr double m_inv_39916800{1.0 / 39916800.0};
  static constexpr double m_inv_3628800{1.0 / 3628800.0};
  static constexpr double m_inv_725760{1.0 / 725760.0};
  static constexpr double m_inv_241920{1.0 / 241920.0};
  static constexpr double m_inv_120960{1.0 / 120960.0};
  static constexpr double m_inv_86400{1.0 / 86400.0};
};

} /* namespace bsl6d */


#include <array>
#include <cmath>
#include <utility>
#include <vector>

#include <gtest/gtest.h>
#include <Kokkos_Core.hpp>

#include <baseCharacteristic.hpp>
#include <characteristics.hpp>
#include <config.hpp>
#include <constantShift.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <halo.hpp>
#include <initializationFunctions.hpp>
#include <lagrangeBuilder.hpp>

#include "../trigonometricInterpolator.hpp"

class TrigonometricPolynomial {
  public:
  TrigonometricPolynomial(bsl6d::AxisType axis, bsl6d::Grid<3, 3> const& grid)
      : m_axis{axis}
      , m_grid{grid} {};

  KOKKOS_INLINE_FUNCTION
  double operator()(double x) const {
    return std::sin(4.0 * 2.0 * M_PI /
                    (m_grid(m_axis).N() * m_grid(m_axis).delta()) * x);
  }
  bsl6d::AxisType m_axis{};
  bsl6d::Grid<3, 3> m_grid{};
};

void init_trigonometric_polynomial(bsl6d::AxisType axis,
                                   bsl6d::DistributionFunction& f6d) {
  TrigonometricPolynomial trigPol(axis, f6d.mpi_local_grid());
  Kokkos::parallel_for(
      "Init f6d with trigonometric polynomial",
      f6d.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        bsl6d::Grid<3, 3> grid{f6d.mpi_local_grid()};
        std::array<double, 6> xv{
            grid(bsl6d::AxisType::x1)(ix1), grid(bsl6d::AxisType::x2)(ix2),
            grid(bsl6d::AxisType::x3)(ix3), grid(bsl6d::AxisType::v1)(iv1),
            grid(bsl6d::AxisType::v2)(iv2), grid(bsl6d::AxisType::v3)(iv3)};
        f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
            trigPol(xv[static_cast<size_t>(axis)]);
      });
}

class InterpolatorTrigonometric
    : public ::testing::TestWithParam<bsl6d::AxisType> {
  public:
  InterpolatorTrigonometric() {
    init_trigonometric_polynomial(axis, f6d);
    characteristic =
        std::make_shared<bsl6d::Constant5DShift>(bsl6d::Constant5DShift{
            std::array<size_t, 6>{
                grid(bsl6d::AxisType::x1).N(), grid(bsl6d::AxisType::x2).N(),
                grid(bsl6d::AxisType::x3).N(), grid(bsl6d::AxisType::v1).N(),
                grid(bsl6d::AxisType::v2).N(), grid(bsl6d::AxisType::v3).N()},
            0, 8.0});
    interpBuilder.setLagrangeConf(bsl6d::OperatorsConfig{config_file()});
    interpBuilder.set_gridDistribution(gridDistribution);
    interpBuilder.setAxis(axis);
    interpBuilder.setCharacteristic(
        bsl6d::CharacteristicVariants{characteristic});
    trigonometricInterpolator = interpBuilder.getInterpolator();
    auto view{characteristic->host(axis)};
    // Add specific shifts for testing which are close to a grid point.
    // To test the indexing and behavior close to zero.
    view(0, 0, 0, 0, 0) = 3.0 - 1.0e-15;
    view(1, 0, 0, 0, 0) = 3.0 + 1.0e-15;
    view(2, 0, 0, 0, 0) = 3.0;
    view(3, 0, 0, 0, 0) = -3.0;
    Kokkos::deep_copy(characteristic->device(axis), characteristic->host(axis));
  };
  bsl6d::AxisType axis{GetParam()};
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::CartTopology<6> topo{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topo};
  bsl6d::DistributionFunction f6d{gridDistribution,
                                  bsl6d::DistributionFunctionConfig{}};
  std::shared_ptr<bsl6d::Constant5DShift> characteristic{};
  bsl6d::LagrangeBuilder interpBuilder{};

  std::shared_ptr<bsl6d::Base1dInterpolator> trigonometricInterpolator{};

  static nlohmann::json config_file() {
    nlohmann::json configFile = R"({
      "operators": {
        "interpolator": {
          "type": ["Trig","Trig","Trig","Trig","Trig","Trig"],
          "stencilWidth": [0,0,0,0,0,0],
          "teamSize": [0,0,0,0,0,0],
          "vectorSize": [0,0,0,0,0,0]
        }
      }
    })"_json;
    return configFile;
  };

  double interpolatedPoint(size_t ix1,
                           size_t ix2,
                           size_t ix3,
                           size_t iv1,
                           size_t iv2,
                           size_t iv3) const {
    if (axis == bsl6d::AxisType::x1) {
      return grid(axis)(ix1) +
             grid(axis).delta() *
                 characteristic->host(axis)(ix2, ix3, iv1, iv2, iv3);
    } else if (axis == bsl6d::AxisType::x2) {
      return grid(axis)(ix2) +
             grid(axis).delta() *
                 characteristic->host(axis)(ix1, ix3, iv1, iv2, iv3);
    } else if (axis == bsl6d::AxisType::x3) {
      return grid(axis)(ix3) +
             grid(axis).delta() *
                 characteristic->host(axis)(ix1, ix2, iv1, iv2, iv3);
    } else if (axis == bsl6d::AxisType::v1) {
      return grid(axis)(iv1) +
             grid(axis).delta() *
                 characteristic->host(axis)(ix1, ix2, ix3, iv2, iv3);
    } else if (axis == bsl6d::AxisType::v2) {
      return grid(axis)(iv2) +
             grid(axis).delta() *
                 characteristic->host(axis)(ix1, ix2, ix3, iv1, iv3);
    }
    return grid(axis)(iv3) + grid(axis).delta() * characteristic->host(axis)(
                                                      ix1, ix2, ix3, iv1, iv2);
  };
};

TEST_P(InterpolatorTrigonometric, Interpolate) {
  TrigonometricPolynomial trigPol{GetParam(), grid};
  trigonometricInterpolator->interpolate(f6d);
  Kokkos::deep_copy(f6d.host(), f6d.device());
  double l1err{};
  Kokkos::parallel_reduce(
      "Compare Analytical and Numerical Trigonometric Interpolation",
      grid.mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double& l_l1err) {
        double x = interpolatedPoint(ix1, ix2, ix3, iv1, iv2, iv3);
        l_l1err  = std::max(
            std::abs(f6d.host()(ix1, ix2, ix3, iv1, iv2, iv3) - trigPol(x)),
            l_l1err);
        EXPECT_NEAR(f6d.host()(ix1, ix2, ix3, iv1, iv2, iv3), trigPol(x),
                    5.5e-9)
            << "Idx: " << ix1 << " " << ix2 << " " << ix3 << " " << iv1 << " "
            << iv2 << " " << iv3 << ";\n "
            << characteristic->host(axis)(ix2, ix3, iv1, iv2, iv3);
      },
      Kokkos::Sum<double>(l1err));
  EXPECT_LT(l1err, 5.5e-9);
}

INSTANTIATE_TEST_SUITE_P(
    InterpolatorTrigonometric_allDim,
    InterpolatorTrigonometric,
    ::testing::Values(bsl6d::AxisType::x1,
                      bsl6d::AxisType::x2,
                      bsl6d::AxisType::x3,
                      bsl6d::AxisType::v1,
                      bsl6d::AxisType::v2,
                      bsl6d::AxisType::v3),
    [](const ::testing::TestParamInfo<InterpolatorTrigonometric::ParamType>&
           info) {
      bsl6d::AxisType axis{info.param};
      std::string name{"Axis" + bsl6d::axis_to_string(axis)};
      return name;
    });

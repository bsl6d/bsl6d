/*****************************************************************/
/*

\brief Testing the settings and functionalities of the lagrange
       interpolation of the bsl6d project using gtest (Prefetching)

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#include <array>
#include <utility>
#include <vector>

#include <gtest/gtest.h>
#include <Kokkos_Core.hpp>

#include <baseCharacteristic.hpp>
#include <characteristics.hpp>
#include <config.hpp>
#include <constantShift.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <halo.hpp>
#include <initializationFunctions.hpp>

#include "../lagrangeBuilder.hpp"
#include "../lagrangePrefetchingIterator.hpp"
/*****************************************************************/
/*

\brief Test setup of interpolation object

*/
/*****************************************************************/
TEST(LagrangePrefetching, HelperFunctions) {
  if constexpr (std::is_same_v<Kokkos::DefaultExecutionSpace,
                               Kokkos::DefaultHostExecutionSpace>) {
    EXPECT_EQ(bsl6d::contiguousDimension<Kokkos::View<double******>>(), 5);
    EXPECT_EQ(bsl6d::get_outerIterationDim<Kokkos::View<double******>>(), 4);

    EXPECT_EQ(bsl6d::contiguousDimension<Kokkos::View<double**>>(), 1);
    EXPECT_EQ(bsl6d::get_outerIterationDim<Kokkos::View<double**>>(), 0);

    std::array<size_t, 4> nonIntrpDim0{4, 3, 2, 1};
    EXPECT_EQ(nonIntrpDim0, bsl6d::leagueRankDim(0));

    std::array<size_t, 4> nonIntrpDim1{4, 3, 2, 0};
    EXPECT_EQ(nonIntrpDim1, bsl6d::leagueRankDim(1));

    std::array<size_t, 4> nonIntrpDim2{4, 3, 1, 0};
    EXPECT_EQ(nonIntrpDim2, bsl6d::leagueRankDim(2));

    std::array<size_t, 4> nonIntrpDim3{4, 2, 1, 0};
    EXPECT_EQ(nonIntrpDim3, bsl6d::leagueRankDim(3));

    std::array<size_t, 4> nonIntrpDim4{3, 2, 1, 0};
    EXPECT_EQ(nonIntrpDim4, bsl6d::leagueRankDim(4));

    std::array<size_t, 4> nonIntrpDim5{3, 2, 1, 0};
    EXPECT_EQ(nonIntrpDim5, bsl6d::leagueRankDim(5));

  } else {
    EXPECT_EQ(bsl6d::contiguousDimension<Kokkos::View<double******>>(), 0);
    EXPECT_EQ(bsl6d::get_outerIterationDim<Kokkos::View<double******>>(), 1);

    EXPECT_EQ(bsl6d::contiguousDimension<Kokkos::View<double**>>(), 0);
    EXPECT_EQ(bsl6d::get_outerIterationDim<Kokkos::View<double**>>(), 1);

    std::array<size_t, 4> nonIntrpDim0{2, 3, 4, 5};
    EXPECT_EQ(nonIntrpDim0, bsl6d::leagueRankDim(0));

    std::array<size_t, 4> nonIntrpDim1{2, 3, 4, 5};
    EXPECT_EQ(nonIntrpDim1, bsl6d::leagueRankDim(1));

    std::array<size_t, 4> nonIntrpDim2{1, 3, 4, 5};
    EXPECT_EQ(nonIntrpDim2, bsl6d::leagueRankDim(2));

    std::array<size_t, 4> nonIntrpDim3{1, 2, 4, 5};
    EXPECT_EQ(nonIntrpDim3, bsl6d::leagueRankDim(3));

    std::array<size_t, 4> nonIntrpDim4{1, 2, 3, 5};
    EXPECT_EQ(nonIntrpDim4, bsl6d::leagueRankDim(4));

    std::array<size_t, 4> nonIntrpDim5{1, 2, 3, 4};
    EXPECT_EQ(nonIntrpDim5, bsl6d::leagueRankDim(5));
  }
}

/*****************************************************************/
/*

\brief Test interpolation of one full dimension of distribution
            function

\description Shift all values by 0.5 to the right using
             interpolation
             The dimension which is to be interpolated is initialized
             as a parabel. Every other direction is constant.
 arbitrary
 other
 dimension
 (not intrpDim)
   --------------
  5| 0  1  4  9 |
  4| 0  1  4  9 |   2d example of initialization
  3| 0  1  4  9 |   -> function values are given in the plane
  2| 0  1  4  9 |
  1| 0  1  4  9 |
   --------------
     0  1  2  3  intrpDim

*/
/*****************************************************************/

/*****************************************************************/
/*

\brief Configuration class for test

\description
  - Configuration for interpolator
  - Interpolation dimension (set by parameter test)
  - Initialization of parabel in interpolation dimension
  - Initialization of comparison values

*/
/*****************************************************************/
nlohmann::json configFileLagrPrefetchIter = R"({
"gridDistribution": {
  "grid":{
    "axes": ["x1","x2","x3","v1","v2","v3"],
    "xvMax": [22.0, 19.0, 20.0, 10.0, 9.0, 11.0],
    "xvMin": [ 0.0,  0.0,  0.0,-10.0,-9.0,-11.0],
    "NxvPoints": [22, 19, 20, 21, 19, 23],
    "position": ["L", "L", "L", "N", "N", "N"],
    "rotateGyroFreq": false
  },
  "parallel": {}
}
})"_json;

class Monomial {
  public:
  Monomial(bsl6d::AxisType const& axis, size_t const& polynomOrder)
      : m_axis{axis}
      , m_polynomOrder{polynomOrder} {}
  KOKKOS_INLINE_FUNCTION
  double operator()(double x1,
                    double x2,
                    double x3,
                    double v1,
                    double v2,
                    double v3) const {
    switch (m_axis) {
      case bsl6d::AxisType::x1: return Kokkos::pow(x1, m_polynomOrder);
      case bsl6d::AxisType::x2: return Kokkos::pow(x2, m_polynomOrder);
      case bsl6d::AxisType::x3: return Kokkos::pow(x3, m_polynomOrder);
      case bsl6d::AxisType::v1: return Kokkos::pow(v1, m_polynomOrder);
      case bsl6d::AxisType::v2: return Kokkos::pow(v2, m_polynomOrder);
      case bsl6d::AxisType::v3: return Kokkos::pow(v3, m_polynomOrder);
      default: return 0.0;
    }
  };

  private:
  bsl6d::AxisType m_axis{};
  size_t m_polynomOrder{};
};

enum class Domain { lower, main, upper };
class InterpolatorLagrange
    : public ::testing::TestWithParam<
          std::tuple<bsl6d::AxisType, size_t, Domain>> {
  public:
  /*************************************************************/
  /* All parameter sets that are tested:
     - First parameter: dimension
     - Second parameter: stencil width
     - Third parameter: Test points that are effected by lower, non,
                        upper boundaries
  */
  /*************************************************************/

  bsl6d::AxisType intrpAxis{std::get<bsl6d::AxisType>(GetParam())};
  bsl6d::CharacteristicVariants characteristicVariant{};
  std::shared_ptr<bsl6d::Constant5DShift> characteristic{};
  bsl6d::LagrangeBuilder builder{};
  size_t boundaryWidth = std::floor(std::get<size_t>(GetParam()) / 2);
  bsl6d::Grid<3, 3> globalGrid{
      bsl6d::GridDistributionConfig{configFileLagrPrefetchIter}};
  // Upper and lower index are chosen such that the stencil fits exactly twice
  // in on the main grid: (upperIdx-lowerIdx)%stencilWidth=2
  std::array<size_t, 6> lowerIdx{boundaryWidth, boundaryWidth, boundaryWidth,
                                 boundaryWidth, boundaryWidth, boundaryWidth};
  std::array<size_t, 6> upperIdx{
      boundaryWidth + std::get<size_t>(GetParam()) + 1,
      boundaryWidth + std::get<size_t>(GetParam()) + 1,
      boundaryWidth + std::get<size_t>(GetParam()) + 1,
      boundaryWidth + std::get<size_t>(GetParam()) + 1,
      boundaryWidth + std::get<size_t>(GetParam()) + 1,
      boundaryWidth + std::get<size_t>(GetParam()) + 1};
  bsl6d::Grid<3, 3> grid{globalGrid.partition(lowerIdx, upperIdx)};
  bsl6d::CartTopology<6> topo{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topo};
  bsl6d::DistributionFunction f6d{gridDistribution,
                                  bsl6d::DistributionFunctionConfig{}};
  std::array<size_t, 6> Nxv{
      grid(bsl6d::AxisType::x1).N(), grid(bsl6d::AxisType::x2).N(),
      grid(bsl6d::AxisType::x3).N(), grid(bsl6d::AxisType::v1).N(),
      grid(bsl6d::AxisType::v2).N(), grid(bsl6d::AxisType::v3).N()};
  bsl6d::BoundaryHypercube boundary{};

  /*************************************************************/
  InterpolatorLagrange() {
    std::map<bsl6d::AxisType, size_t> Nxv_local, boundaryWidth_local;
    for (auto axis : bsl6d::xvAxes()) {
      Nxv_local[axis]           = grid(axis).N();
      boundaryWidth_local[axis] = std::floor(std::get<size_t>(GetParam()) / 2);
    }
    boundary = bsl6d::BoundaryHypercube{Nxv_local, boundaryWidth_local};
    boundary.set_my_axis(intrpAxis);
    nlohmann::json configFile               = R"({
      "operators": {
        "interpolator": {
          "stencilWidth": [0,0,0,0,0,0],
          "teamSize": [0,0,0,0,0,0],
          "vectorSize": [0,0,0,0,0,0]
        }
      }
    })"_json;
    std::vector<size_t> stencil             = {0, 0, 0, 0, 0, 0};
    stencil[static_cast<size_t>(intrpAxis)] = std::get<size_t>(GetParam());
    configFile["operators"]["interpolator"]["stencilWidth"] = stencil;

    bsl6d::CharacteristicBuilder characteristicBuilder{};

    characteristicBuilder.set_Nxv(get_Nxv<std::array<size_t, 6>>(), 5);
    characteristicVariant = characteristicBuilder.create_characteristic(
        bsl6d::CharacteristicId::Constant5DShift);
    characteristic = std::get<std::shared_ptr<bsl6d::Constant5DShift>>(
        characteristicVariant);
    builder.setAxis(intrpAxis);
    builder.setCharacteristic(characteristicVariant);
    builder.setLagrangeConf(bsl6d::OperatorsConfig{configFile});
    builder.set_gridDistribution(gridDistribution);

    initParabel(f6d);
  };

  /*************************************************************/
  void initParabel(bsl6d::DistributionFunction f6d) {
    nlohmann::json configFile{};
    size_t polynomOrder = std::get<size_t>(GetParam()) - 1;
    bsl6d::Impl::fill(f6d, Monomial{intrpAxis, polynomOrder});

    lowerIdx[static_cast<size_t>(intrpAxis)] =
        grid(intrpAxis).low() - boundaryWidth;
    upperIdx[static_cast<size_t>(intrpAxis)] = grid(intrpAxis).low() - 1;
    bsl6d::DistributionFunction low{
        bsl6d::GridDistribution{globalGrid.partition(lowerIdx, upperIdx), topo},
        bsl6d::DistributionFunctionConfig{}};
    bsl6d::Impl::fill(low, Monomial{intrpAxis, polynomOrder});
    Kokkos::deep_copy(boundary.low(intrpAxis), low.device());

    lowerIdx[static_cast<size_t>(intrpAxis)] = grid(intrpAxis).up() + 1;
    upperIdx[static_cast<size_t>(intrpAxis)] =
        grid(intrpAxis).up() + boundaryWidth;
    bsl6d::DistributionFunction up{
        bsl6d::GridDistribution{globalGrid.partition(lowerIdx, upperIdx), topo},
        bsl6d::DistributionFunctionConfig{}};
    bsl6d::Impl::fill(up, Monomial{intrpAxis, polynomOrder});
    Kokkos::deep_copy(boundary.up(intrpAxis), up.device());
  }

  inline double f_comp(size_t const i0,
                       size_t const i1,
                       size_t const i2,
                       size_t const i3,
                       size_t const i4,
                       size_t const i5) {
    std::array<double, 6> parabolic{};
    parabolic[0] = f6d.mpi_local_grid()(bsl6d::AxisType::x1)(i0) +
                   characteristic->host(intrpAxis)(i1, i2, i3, i4, i5);
    parabolic[1] = f6d.mpi_local_grid()(bsl6d::AxisType::x2)(i1) +
                   characteristic->host(intrpAxis)(i0, i2, i3, i4, i5);
    parabolic[2] = f6d.mpi_local_grid()(bsl6d::AxisType::x3)(i2) +
                   characteristic->host(intrpAxis)(i0, i1, i3, i4, i5);
    parabolic[3] = f6d.mpi_local_grid()(bsl6d::AxisType::v1)(i3) +
                   characteristic->host(intrpAxis)(i0, i1, i2, i4, i5);
    parabolic[4] = f6d.mpi_local_grid()(bsl6d::AxisType::v2)(i4) +
                   characteristic->host(intrpAxis)(i0, i1, i2, i3, i5);
    parabolic[5] = f6d.mpi_local_grid()(bsl6d::AxisType::v3)(i5) +
                   characteristic->host(intrpAxis)(i0, i1, i2, i3, i4);
    // These functions work and can be tested but they the commented version can
    // not be accessed through host if GPU is available
    // parabolic[0] =
    // grid(bsl6d::AxisType::x1)(i0)+characteristic->shift_x1(i1,i2,i3,i4,i5);
    // parabolic[1] =
    // grid(bsl6d::AxisType::x2)(i1)+characteristic->shift_x2(i0,i2,i3,i4,i5);
    // parabolic[2] =
    // grid(bsl6d::AxisType::x3)(i2)+characteristic->shift_x3(i0,i1,i3,i4,i5);
    // parabolic[3] =
    // grid(bsl6d::AxisType::v1)(i3)+characteristic->shift_v1(i0,i1,i2,i4,i5);
    // parabolic[4] =
    // grid(bsl6d::AxisType::v2)(i4)+characteristic->shift_v2(i0,i1,i2,i3,i5);
    // parabolic[5] =
    // grid(bsl6d::AxisType::v3)(i5)+characteristic->shift_v3(i0,i1,i2,i3,i4);

    for (size_t i = 0; i < 6; i++)
      parabolic[i] = std::pow(parabolic[i], std::get<size_t>(GetParam()) - 1);
    return parabolic[static_cast<size_t>(
        std::get<bsl6d::AxisType>(GetParam()))];
  };

  /*************************************************************/
  template <typename T>
  T get_Nxv() {
    T Nxv{{grid(bsl6d::AxisType::x1).N(), grid(bsl6d::AxisType::x2).N(),
           grid(bsl6d::AxisType::x3).N(), grid(bsl6d::AxisType::v1).N(),
           grid(bsl6d::AxisType::v2).N(), grid(bsl6d::AxisType::v3).N()}};
    return Nxv;
  }

  /*************************************************************/
  template <size_t dim>
  Kokkos::Array<size_t, dim> startIdx(bsl6d::AxisType const intrpAxis,
                                      size_t const intrpDimVal) {
    Kokkos::Array<size_t, dim> array;
    for (size_t i = 0; i < 6; i++) array[i] = 0;
    array[static_cast<size_t>(intrpAxis)] = intrpDimVal;
    return array;
  }

  /*************************************************************/
  template <size_t dim>
  Kokkos::Array<size_t, dim> stopIdx(std::array<size_t, dim> const Nxv,
                                     bsl6d::AxisType const intrpAxis,
                                     size_t const intrpDimVal) {
    Kokkos::Array<size_t, dim> array;
    for (size_t i = 0; i < 6; i++) array[i] = Nxv[i];
    array[static_cast<size_t>(intrpAxis)] = intrpDimVal;
    return array;
  }

  Kokkos::MDRangePolicy<Kokkos::Rank<6>, Kokkos::DefaultHostExecutionSpace>
      getMDRng() {
    std::array<size_t, 6> Nxv{get_Nxv<std::array<size_t, 6>>()};
    Kokkos::Array<size_t, 6> sIdx{};
    Kokkos::Array<size_t, 6> fIdx{};
    std::string domainName{};
    switch (std::get<Domain>(GetParam())) {
      case Domain::lower:
        domainName = "LowerDomain";
        sIdx       = startIdx<6>(intrpAxis, 0);
        fIdx       = stopIdx<6>(Nxv, intrpAxis, boundaryWidth);
        break;
      case Domain::main:
        domainName = "MainDomain";
        sIdx       = {startIdx<6>(intrpAxis, boundaryWidth)};
        fIdx       = {
            stopIdx<6>(Nxv, intrpAxis,
                       Nxv[static_cast<size_t>(intrpAxis)] - boundaryWidth)};
        break;
      case Domain::upper:
        domainName = "UpperDomain";
        sIdx       = startIdx<6>(intrpAxis,
                           Nxv[static_cast<size_t>(intrpAxis)] - boundaryWidth);
        fIdx = stopIdx<6>(Nxv, intrpAxis, Nxv[static_cast<size_t>(intrpAxis)]);
        break;
    }
    return Kokkos::MDRangePolicy<Kokkos::Rank<6>,
                                 Kokkos::DefaultHostExecutionSpace>{sIdx, fIdx};
  }
};

/*****************************************************************/
TEST_P(InterpolatorLagrange, Interpolate) {
  std::shared_ptr<bsl6d::Base1dInterpolator> lagIntrp =
      builder.getInterpolator();
  ASSERT_EQ(intrpAxis, lagIntrp->get_intrpAxis());

  /***************************************************************/
  // Change the characteristic used in interpolation such that interpolation
  //   fails if characteristic = *characteristic_ptr is removed from
  //   LagrangeInterpolationPrefetching<CharacteristicTypePtr,StencilType,_intrpAxis>::interpolate
  *std::shared_ptr<bsl6d::Constant5DShift>(characteristic) =
      bsl6d::Constant5DShift{get_Nxv<std::array<size_t, 6>>(), 1};

  std::string domainName{};
  switch (std::get<2>(GetParam())) {
    case Domain::lower: domainName = "LowerDomain"; break;
    case Domain::main: domainName = "MainDomain"; break;
    case Domain::upper: domainName = "UpperDomain"; break;
  }
  /***************************************************************/
  // Carry out interpolation
  lagIntrp->skip_next_halo_exchange(boundary);
  lagIntrp->interpolate(f6d);
  Kokkos::deep_copy(f6d.host(), f6d.device());

  /***************************************************************/
  // Check if interpolation succeeded

  double err{};
  Kokkos::parallel_reduce(
      "LInf" + domainName, getMDRng(),
      [=](size_t i0, size_t i1, size_t i2, size_t i3, size_t i4, size_t i5,
          double& lerr) {
        lerr = std::max(lerr, std::abs((f_comp(i0, i1, i2, i3, i4, i5) -
                                        f6d.host()(i0, i1, i2, i3, i4, i5)) /
                                       (f_comp(i0, i1, i2, i3, i4, i5) + 1)));
        // Double EQ is not possible since larger stencils lead to Floating
        // Point
        //  errors, such that floating point error is larger than google Double
        //  EQ.
        // EXPECT_DOUBLE_EQ(f_comp(i0,i1,i2,i3,i4,i5),f6d.host()(i0,i1,i2,i3,i4,i5));
      },
      Kokkos::Max<double>(err));
  ASSERT_GT(err, 0.) << "Linf is never to be expected to be zero if any points "
                        "are interpolated. If err is exactly zero it is assumed"
                        "that nothing has been interpolated.";
  ASSERT_LE(err, 5e-13)
      << " |L(x+dx) - (x+dx)^(stencilWidth-1) |\n"
         " |----------------------------------|\n"
         " |    (x+dx)^(stencilWidth-1)+1)    |_inf\n";  // 5e-13 is analyzed
                                                         // and discussed in the
                                                         // doku of Nils Schild
                                                         // for his Ph.D.
}

INSTANTIATE_TEST_SUITE_P(
    InterpolatorLagrange_allDim,
    InterpolatorLagrange,
    ::testing::Combine(
        ::testing::Values(bsl6d::AxisType::x1,
                          bsl6d::AxisType::x2,
                          bsl6d::AxisType::x3,
                          bsl6d::AxisType::v1,
                          bsl6d::AxisType::v2,
                          bsl6d::AxisType::v3),
        ::testing::Values(3, 4),
        //::testing::Values(6, 7), // This parameter set has an error 2e-11
        ::testing::Values(Domain::lower, Domain::main, Domain::upper)),
    [](const ::testing::TestParamInfo<InterpolatorLagrange::ParamType>& info) {
      bsl6d::AxisType axis{std::get<bsl6d::AxisType>(info.param)};
      size_t stencil{std::get<size_t>(info.param)};
      Domain domain{std::get<Domain>(info.param)};
      std::string domainName{};
      switch (domain) {
        case Domain::lower: domainName = "LowerDomain"; break;
        case Domain::main: domainName = "MainDomain"; break;
        case Domain::upper: domainName = "UpperDomain"; break;
      }
      std::string name{"Axis" + bsl6d::axis_to_string(axis) + "Stencil" +
                       std::to_string(stencil) + domainName};
      return name;
    });

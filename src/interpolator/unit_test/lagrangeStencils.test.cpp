/*****************************************************************/
/*

\brief Testing the settings and functionalities of the lagrange
       interpolation of the bsl6d project using gtest (Stencil)

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   07.10.2021

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>
#include "../lagrangeStencils.hpp"

template <typename StencilType>
void stencilApply(StencilType stencil,
                  std::pair<double, double> shifts,
                  Kokkos::View<double**>& f,
                  Kokkos::View<double[2]>& fres) {
  size_t intrpIdx = std::floor(stencil.width / 2.0);
  Kokkos::parallel_for(
      "Interpolation Test", 1, KOKKOS_LAMBDA(size_t i) {
        fres(0) =
            stencil.interpolateIdx(&f(0, intrpIdx), shifts.first, f.stride(1));
        fres(1) = stencil.interpolateIdx(&f(intrpIdx, f.extent(1) - 1),
                                         shifts.second, f.stride(0));
      });
}

int start_idx(int stencilWidth) {
  // sum_i=-(n-1)/2^(n-1)/2 li(dx)f_i
  // sum_i=-n/2-1^n/2 li(dx)f_i
  if (stencilWidth % 2 == 0) { return -std::floor(stencilWidth / 2.0) + 1; }
  return -std::floor(stencilWidth / 2.0);
}

template <typename StencilType>
void test_LagBasisPoly_0To1(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width)) {
      EXPECT_DOUBLE_EQ(lagStencil.poly0(i), 1)
          << "Width: " << lagStencil.width << "; Index: " << i;
    } else {
      EXPECT_NEAR(lagStencil.poly0(i), 0.0, 5e-13);
    }
  }
  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 1) {
      EXPECT_DOUBLE_EQ(lagStencil.poly1(i), 1)
          << "Width: " << lagStencil.width << "; Index: " << i;
    } else {
      EXPECT_NEAR(lagStencil.poly1(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_2(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 2) {
      EXPECT_DOUBLE_EQ(lagStencil.poly2(i), 1)
          << "Width: " << lagStencil.width << "; Index: " << i;
    } else {
      EXPECT_NEAR(lagStencil.poly2(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_3(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth; i++) {
    if (i == start_idx(lagStencil.width) + 3) {
      EXPECT_DOUBLE_EQ(lagStencil.poly3(i), 1);
    } else {
      EXPECT_NEAR(lagStencil.poly3(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_4(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 4) {
      EXPECT_DOUBLE_EQ(lagStencil.poly4(i), 1);
    } else {
      EXPECT_NEAR(lagStencil.poly4(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_5(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 5) {
      EXPECT_DOUBLE_EQ(lagStencil.poly5(i), 1);
    } else {
      EXPECT_NEAR(lagStencil.poly5(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_6(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 6) {
      EXPECT_DOUBLE_EQ(lagStencil.poly6(i), 1);
    } else {
      EXPECT_NEAR(lagStencil.poly6(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_7(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 7) {
      EXPECT_DOUBLE_EQ(lagStencil.poly7(i), 1);
    } else {
      EXPECT_NEAR(lagStencil.poly7(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_8(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 8) {
      EXPECT_DOUBLE_EQ(lagStencil.poly8(i), 1.0)
          << "Width: " << lagStencil.width << "; Index: " << i;
    } else {
      EXPECT_NEAR(lagStencil.poly8(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_9(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 9) {
      EXPECT_DOUBLE_EQ(lagStencil.poly9(i), 1.0);
    } else {
      EXPECT_NEAR(lagStencil.poly9(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_10(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 10) {
      EXPECT_DOUBLE_EQ(lagStencil.poly10(i), 1.0);
    } else {
      EXPECT_NEAR(lagStencil.poly10(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_LagBasisPoly_11(StencilType lagStencil) {
  int halfWidth = std::floor(lagStencil.width / 2.0);

  for (int i = start_idx(lagStencil.width); i < halfWidth + 1; i++) {
    if (i == start_idx(lagStencil.width) + 11) {
      EXPECT_DOUBLE_EQ(lagStencil.poly11(i), 1.0);
    } else {
      EXPECT_NEAR(lagStencil.poly11(i), 0.0, 5e-13);
    }
  }
}

template <typename StencilType>
void test_stencil_to_monomial(StencilType lagStencil) {
  std::pair<double, double> shifts{-0.25, 0.25};
  size_t domainWidth{};
  if (lagStencil.width % 2 == 0) {
    domainWidth = lagStencil.width + 1;
  } else {
    domainWidth = lagStencil.width;
  }
  Kokkos::View<double**> f{"IntrpStart", domainWidth, domainWidth};
  Kokkos::View<double[2]> fres{"IntrpRes"};

  Kokkos::View<double**>::HostMirror h_f{};
  Kokkos::View<double[2]>::HostMirror h_fres{};
  h_f    = Kokkos::create_mirror_view(f);
  h_fres = Kokkos::create_mirror_view(fres);

  Kokkos::deep_copy(h_f, 1.);
  for (size_t i = 0; i < f.extent(0); i++)
    for (size_t j = 0; j < f.extent(1); j++) {
      h_f(i, j) = std::pow(i + j, lagStencil.width - 1);
    }

  Kokkos::deep_copy(f, h_f);
  stencilApply(lagStencil, shifts, f, fres);
  Kokkos::deep_copy(h_fres, fres);

  double interpolationPointX = std::floor(f.extent(0) / 2.0);
  EXPECT_DOUBLE_EQ(h_fres(0), std::pow(interpolationPointX + shifts.first,
                                       lagStencil.width - 1))
      << "Width: " << lagStencil.width;
  double interpolationPointY =
      std::floor(f.extent(1) / 2.0) + (f.extent(0) - 1);
  EXPECT_DOUBLE_EQ(h_fres(1), std::pow(interpolationPointY + shifts.second,
                                       lagStencil.width - 1))
      << "Width: " << lagStencil.width;
}

TEST(LagrangeStencils, InterpolateSinglePoint) {
  bsl6d::LagrangeInterpolationStencil2 lagStencil2{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil2>(lagStencil2);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil2>(lagStencil2);

  bsl6d::LagrangeInterpolationStencil3 lagStencil3{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil3>(lagStencil3);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil3>(lagStencil3);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil3>(lagStencil3);

  bsl6d::LagrangeInterpolationStencil4 lagStencil4{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil4>(lagStencil4);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil4>(lagStencil4);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil4>(lagStencil4);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil4>(lagStencil4);

  bsl6d::LagrangeInterpolationStencil5 lagStencil5{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil5>(lagStencil5);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil5>(lagStencil5);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil5>(lagStencil5);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil5>(lagStencil5);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil5>(lagStencil5);

  bsl6d::LagrangeInterpolationStencil6 lagStencil6{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil6>(lagStencil6);

  bsl6d::LagrangeInterpolationStencil7 lagStencil7{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil7>(lagStencil7);

  bsl6d::LagrangeInterpolationStencil8 lagStencil8{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_LagBasisPoly_7<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil8>(lagStencil8);

  bsl6d::LagrangeInterpolationStencil9 lagStencil9{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_7<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_LagBasisPoly_8<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil9>(lagStencil9);

  bsl6d::LagrangeInterpolationStencil10 lagStencil10{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_7<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_8<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_LagBasisPoly_9<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil10>(lagStencil10);

  bsl6d::LagrangeInterpolationStencil11 lagStencil11{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_7<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_8<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_9<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_LagBasisPoly_10<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil11>(lagStencil11);

  bsl6d::LagrangeInterpolationStencil12 lagStencil12{};
  test_LagBasisPoly_0To1<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_2<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_3<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_4<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_5<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_6<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_7<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_8<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_9<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_10<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_LagBasisPoly_11<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
  test_stencil_to_monomial<bsl6d::LagrangeInterpolationStencil12>(lagStencil12);
}

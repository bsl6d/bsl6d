/*****************************************************************/
/*

\brief Using prefetching algorithm to interpolate in 1 dimension

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   10.12.2021

*/
/*****************************************************************/

#pragma once

#include <fstream>
#include <iostream>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include <interpolatorBase.hpp>

namespace bsl6d {

template <typename CharacteristicPtrType, AxisType axis>
class TrigonometricInterpolator : public Base1dInterpolator {
  public:
  TrigonometricInterpolator(CharacteristicPtrType characteristicPtr);
  void interpolate(DistributionFunction &f6d) override;

  CharacteristicId get_characteristicId() const override {
    return m_characteristicPtr->id();
  };

  KOKKOS_INLINE_FUNCTION
  void operator()(size_t ix1,
                  size_t ix2,
                  size_t ix3,
                  size_t iv1,
                  size_t iv2,
                  size_t iv3) const {
    std::array<size_t, 6> idx{ix1, ix2, ix3, iv1, iv2, iv3};
    double x{m_grid(m_axis)(index(idx))};
    // If x is again a grid point the interpolation fails!
    // Therefore in a small range around each grid point no
    // interpolation is executed
    bool shiftFullCell{false};
    shiftFullCell =
        (Kokkos::abs(Kokkos::round(shift(ix1, ix2, ix3, iv1, iv2, iv3)) -
                     shift(ix1, ix2, ix3, iv1, iv2, iv3)) < 1e-10);
    int shiftIdx{
        static_cast<int>(Kokkos::round(shift(ix1, ix2, ix3, iv1, iv2, iv3))) +
        static_cast<int>(index(idx))};

    x += m_grid(m_axis).delta() * shift(ix1, ix2, ix3, iv1, iv2, iv3);
    // If the shift is a multiple of dx just add a random value such
    // that it is non a multiple of dx and the calculation down below
    // does not return nans (slow). The interpolated value is not used
    // anyway in this setup
    if (shiftFullCell) x += m_grid(m_axis).delta() * 0.5;
    double h{2.0 / m_f.extent(intrpDim())};
    double scale{m_grid(m_axis).delta() / h};
    // Here we have a race condition as soon as f6d_res and m_f
    // are the same views due to the shared memory concept of
    // Views. Currently this are two separate Views.
    auto f{get_subview(m_f, idx)};
    f6d_res(ix1, ix2, ix3, iv1, iv2, iv3) = 0.0;
    for (size_t k = 0; k < m_f.extent(intrpDim()); k++) {
      f6d_res(ix1, ix2, ix3, iv1, iv2, iv3) +=
          f(k) * trig_cardinal((x - m_grid(m_axis)(k)) / scale,
                               m_f.extent(intrpDim()));
    }
    // Periodic boundary conditions if index is outside of the
    // computational domain I assume the particle does not corss
    // the whole domain twice or more.
    int gridPoints = static_cast<int>(m_grid(m_axis).N());
    shiftIdx = shiftIdx >= gridPoints ? (shiftIdx - gridPoints) : shiftIdx;
    shiftIdx = shiftIdx < 0 ? (shiftIdx + gridPoints) : shiftIdx;
    f6d_res(ix1, ix2, ix3, iv1, iv2, iv3) =
        shiftFullCell ? f(shiftIdx) : f6d_res(ix1, ix2, ix3, iv1, iv2, iv3);
  };

  KOKKOS_INLINE_FUNCTION
  double trig_cardinal(double x, size_t N) const;

  KOKKOS_INLINE_FUNCTION
  constexpr size_t index(std::array<size_t, 6> const &idx) const;

  KOKKOS_INLINE_FUNCTION
  constexpr double shift(size_t const &ix1,
                         size_t const &ix2,
                         size_t const &ix3,
                         size_t const &iv1,
                         size_t const &iv2,
                         size_t const &iv3) const;
  KOKKOS_INLINE_FUNCTION
  auto get_subview(Kokkos::View<double ******> const f,
                   std::array<size_t, 6> const &idx) const {
    if constexpr (m_axis == AxisType::x1) {
      return Kokkos::subview(f, Kokkos::ALL(), idx[1], idx[2], idx[3], idx[4],
                             idx[5]);
    } else if constexpr (m_axis == AxisType::x2) {
      return Kokkos::subview(f, idx[0], Kokkos::ALL(), idx[2], idx[3], idx[4],
                             idx[5]);
    } else if constexpr (m_axis == AxisType::x3) {
      return Kokkos::subview(f, idx[0], idx[1], Kokkos::ALL(), idx[3], idx[4],
                             idx[5]);
    } else if constexpr (axis == AxisType::v1) {
      return Kokkos::subview(f, idx[0], idx[1], idx[2], Kokkos::ALL(), idx[4],
                             idx[5]);
    } else if constexpr (m_axis == AxisType::v2) {
      return Kokkos::subview(f, idx[0], idx[1], idx[2], idx[3], Kokkos::ALL(),
                             idx[5]);
    } else if constexpr (m_axis == AxisType::v3) {
      return Kokkos::subview(f, idx[0], idx[1], idx[2], idx[3], idx[4],
                             Kokkos::ALL());
    }
  }

  private:
  // Internally used as an integer
  constexpr size_t intrpDim() const { return static_cast<size_t>(m_axis); };

  // Data to be interpolated
  Kokkos::View<double ******> m_f{};
  Kokkos::View<double ******> f6d_res{};
  static constexpr AxisType m_axis{axis};

  CharacteristicPtrType m_characteristicPtr{};
  typename CharacteristicPtrType::element_type m_characteristic{};

  Grid<3, 3> m_grid{};
};

}  // namespace bsl6d

/*****************************************************************/
/*

\brief Base abstract interface for 1d lagrange interpolators

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   21.12.2021

*/
/*****************************************************************/

#pragma once

#include <boundaryHypercube.hpp>
#include <characteristics.hpp>
#include <distributionFunction.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief Abstract interface for interpolation class

*/
/*****************************************************************/
class Base1dInterpolator {
  public:
  AxisType get_intrpAxis() const { return intrpAxis_base; };
  virtual CharacteristicId get_characteristicId() const = 0;

  virtual void interpolate(DistributionFunction &_f6d) = 0;

  // Only for testing and performance usage:
  // Boundary arrays have to be given externally in this case
  void skip_next_halo_exchange(BoundaryHypercube const &bd) {
    skipThisHaloExchange = true;
    _boundary            = bd;
  };

  protected:
  Base1dInterpolator() = default;
  Base1dInterpolator(AxisType intrpAxis) : intrpAxis_base(intrpAxis){};
  virtual ~Base1dInterpolator(){};
  bool skipThisHaloExchange{false};
  BoundaryHypercube _boundary{};

  private:
  AxisType const intrpAxis_base{};
};

} /* namespace bsl6d */

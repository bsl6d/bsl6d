#include <distributionFunction.hpp>
#include <scalarField.hpp>

namespace bsl6d {
/**
 * @brief \f$ \int | f(\textbf{x},\textbf{v}) | \mathrm{d}^3 x\mathrm{d}^3 v\f$
 */
double L1_norm(DistributionFunction const&);
/**
 * @brief \f$ \int f^2(\textbf{x},\textbf{v}) \mathrm{d}^3 x\mathrm{d}^3 v\f$
 */
double L2_norm(DistributionFunction const&);
/**
 * @brief \f$ \text{max}(| f(\textbf{x},\textbf{v}) |)\f$
 */
double LInf_norm(DistributionFunction const&);
/**
 * @brief \f$ \int | f(\textbf{x}) | \mathrm{d}^3 x\f$
 */
double L1_norm(ScalarField const&);
/**
 * @brief \f$ \int f^2(\textbf{x}) \mathrm{d}^3 x\f$
 */
double L2_norm(ScalarField const&);
/**
 * @brief \f$ \text{max}(| f(\textbf{x}) |\f$
 */
double LInf_norm(ScalarField const&);
/**
 * @brief \f$ \int | f(\textbf{x},\textbf{v}) - g(\textbf{x},\textbf{v}) |
 * \mathrm{d}^3 x\mathrm{d}^3 v\f$
 */
double L1_error(DistributionFunction const&, DistributionFunction const&);
/**
 * @brief \f$ \int (f(\textbf{x},\textbf{v}) - g(\textbf{x},\textbf{v}))^2
 * \mathrm{d}^3 x\mathrm{d}^3 v\f$
 */
double L2_error(DistributionFunction const&, DistributionFunction const&);
/**
 * @brief \f$ \text{max}(| f(\textbf{x},\textbf{v}) -
 * g(\textbf{x},\textbf{v})|)\f$
 */
double LInf_error(DistributionFunction const&, DistributionFunction const&);
/**
 * @brief \f$ \int | f(\textbf{x}) - g(\textbf{x}) | \mathrm{d}^3 x\f$
 */
double L1_error(ScalarField const&, ScalarField const&);
/**
 * @brief \f$ \int (f(\textbf{x}) - g(\textbf{x}))^2 \mathrm{d}^3 x\f$
 */
double L2_error(ScalarField const&, ScalarField const&);
/**
 * @brief \f$ \text{max}(| f(\textbf{x}) - g(\textbf{x})|)\f$
 */
double LInf_error(ScalarField const&, ScalarField const&);

namespace Impl {
template <typename ViewType>
std::map<std::string, typename ViewType::value_type> L_errors(
    ViewType const& a,
    ViewType const& b) {
  if (not a.span_is_contiguous() or not b.span_is_contiguous()) {
    throw std::runtime_error("L_errors(...) requires View " + a.label() +
                             " and " + b.label() + " to be contiguous");
  }
  std::map<std::string, typename ViewType::value_type> res{};
  res["L1_err"]   = 0.0;
  res["L2_err^2"] = 0.0;
  res["LInf_err"] = 0.0;
  typename ViewType::value_type* a_ptr{a.data()};
  typename ViewType::value_type* b_ptr{b.data()};
  Kokkos::parallel_reduce(
      "L_errors",
      Kokkos::RangePolicy<typename ViewType::execution_space>(0, a.size()),
      KOKKOS_LAMBDA(typename ViewType::size_type i,
                    typename ViewType::value_type & l_l1,
                    typename ViewType::value_type & l_l2,
                    typename ViewType::value_type & l_lInf) {
        l_l1 += std::abs(a_ptr[i] - b_ptr[i]);
        l_l2 += std::abs((a_ptr[i] - b_ptr[i]) * (a_ptr[i] - b_ptr[i]));
        l_lInf = std::max(l_lInf, std::abs(a_ptr[i] - b_ptr[i]));
      },
      Kokkos::Sum<typename ViewType::value_type>(res["L1_err"]),
      Kokkos::Sum<typename ViewType::value_type>(res["L2_err^2"]),
      Kokkos::Max<typename ViewType::value_type>(res["LInf_err"]));
  return res;
}

template <typename ViewType>
std::map<std::string, typename ViewType::value_type> L_norms(
    ViewType const& view) {
  ViewType zeroView{"zeroViewForNorm", view.layout()};
  Kokkos::deep_copy(zeroView, 0.0);
  std::map<std::string, typename ViewType::value_type> err{
      L_errors(view, zeroView)};
  std::map<std::string, typename ViewType::value_type> norm{};
  norm["L1"]   = err["L1_err"];
  norm["L2^2"] = err["L2_err^2"];
  norm["LInf"] = err["LInf_err"];
  return norm;
}
template <typename ViewType>
typename ViewType::value_type LInf_norm(ViewType const& view) {
  return L_norms(view)["LInf"];
};
template <typename ViewType>
typename ViewType::value_type L1_norm(ViewType const& view) {
  return L_norms(view)["L1"];
};
template <typename ViewType>
typename ViewType::value_type L2_norm(ViewType const& view) {
  return L_norms(view)["L2^2"];
};
template <typename ViewType>
typename ViewType::value_type LInf_error(ViewType const& a, ViewType const& b) {
  return L_errors(a, b)["LInf_err"];
};
template <typename ViewType>
typename ViewType::value_type L1_error(ViewType const& a, ViewType const& b) {
  return L_errors(a, b)["L1_err"];
};
template <typename ViewType>
typename ViewType::value_type L2_error(ViewType const& a, ViewType const& b) {
  return L_errors(a, b)["L2_err^2"];
};
}  // namespace Impl

}  // namespace bsl6d
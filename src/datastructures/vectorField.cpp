#include <vectorField.hpp>

namespace bsl6d {

VectorField::VectorField(GridDistribution<3, 0> const &gridDistribution,
                         std::string const &label)
    : m_field{ScalarField{gridDistribution, label + "_x1"},
              ScalarField{gridDistribution, label + "_x2"},
              ScalarField{gridDistribution, label + "_x3"}}
    , m_label{Kokkos::View<char *>{label, 0}} {}
void VectorField::replace_scalarField(AxisType axis,
                                      ScalarField const &scalarField) {
  m_field[static_cast<size_t>(axis)] = scalarField;
}

MatrixField::MatrixField(const GridDistribution<3, 0> &gridDistribution,
                         const std::string &label)
    : m_field{std::array<ScalarField, 3>{
                  ScalarField{gridDistribution, label + "_x1x1"},
                  ScalarField{gridDistribution, label + "_x1x2"},
                  ScalarField{gridDistribution, label + "_x1x3"}},
              std::array<ScalarField, 3>{
                  ScalarField{gridDistribution, label + "_x2x1"},
                  ScalarField{gridDistribution, label + "_x2x2"},
                  ScalarField{gridDistribution, label + "_x2x3"}},
              std::array<ScalarField, 3>{
                  ScalarField{gridDistribution, label + "_x3x1"},
                  ScalarField{gridDistribution, label + "_x3x2"},
                  ScalarField{gridDistribution, label + "_x3x3"}}}
    , m_label{Kokkos::View<char *>{label, 0}} {};

void MatrixField::replace_scalarField(AxisType axisRow,
                                      AxisType axisColumn,
                                      ScalarField const &scalarField) {
  m_field[static_cast<size_t>(axisRow)][static_cast<size_t>(axisColumn)] =
      scalarField;
};

} /* namespace bsl6d */

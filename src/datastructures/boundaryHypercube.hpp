/*****************************************************************/
/*

\brief Boundary for specific dimension of hypercube

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   02.11.2021

*/
/*****************************************************************/
#pragma once

#include <Kokkos_Core.hpp>
#include <array>
#include <fstream>
#include <map>
#include <string>

#include <config.hpp>
#include <grid.hpp>

namespace bsl6d {

/*****************************************************************/
/*

\brief Class to define a boundary for data represented on a
       3d3v hypercube

\description Only boundarys in one dimension are needed at a time.
             The memory needed for boundaries in different dimensions
             is shared between all dimensions.

*/
/*****************************************************************/
class BoundaryHypercube {
  public:
  BoundaryHypercube() = default;
  BoundaryHypercube(std::map<bsl6d::AxisType, size_t> Nxv,
                    std::map<bsl6d::AxisType, size_t> boundaryWidth);

  KOKKOS_INLINE_FUNCTION
  Kokkos::View<double******> low(AxisType axis) const {
    return m_low[static_cast<size_t>(axis)];
  };

  KOKKOS_INLINE_FUNCTION
  Kokkos::View<double******> up(AxisType axis) const {
    return m_up[static_cast<size_t>(axis)];
  };

  Kokkos::View<double******>::HostMirror h_low(AxisType axis) const {
    if (axis != m_myAxis)
      throw std::invalid_argument(
          "Lower axis requested is currently not stored in "
          "class BoundaryHypercube.");
    return m_h_low[static_cast<size_t>(axis)];
  };

  Kokkos::View<double******>::HostMirror h_up(AxisType axis) const {
    if (axis != m_myAxis)
      throw std::invalid_argument(
          "Upper axis requested is currently not stored in "
          "class BoundaryHypercube.");
    return m_h_up[static_cast<size_t>(axis)];
  };

  void set_my_axis(AxisType axis) { m_myAxis = axis; };

  AxisType my_axis() { return m_myAxis; };

  size_t shared_buffer_size(std::string type) const {
    return m_baseMem.at(type).size();
  };

  private:
  using BoundaryViewType =
      Kokkos::View<double******, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
  // Kokkos View containing base data for boundaries
  std::map<std::string, Kokkos::View<double*>> m_baseMem{};
  std::map<std::string, Kokkos::View<double*>::HostMirror> m_h_baseMem{};
  AxisType m_myAxis;
  // Lower and upper indexes for memory that is not part of the
  // grid (solution domain)
  std::array<BoundaryViewType, 6> m_low{}, m_up{};
  std::array<BoundaryViewType::HostMirror, 6> m_h_low{}, m_h_up{};
};

} /* namespace bsl6d */
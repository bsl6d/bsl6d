/*****************************************************************/
/*
\brief Initializations for distribution functions

\author Nils Schild; Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021

*/
/*****************************************************************/
#pragma once

#include <array>
#include <iostream>

#include <Kokkos_Core.hpp>

#include <advector.hpp>
#include <baseFieldSolver.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <io_init.hpp>
#include <openPMDInterface.hpp>
#include <time.hpp>

namespace bsl6d {

namespace Impl {
KOKKOS_INLINE_FUNCTION
double gauss(double const x, double const mu, double const sigma2) {
  double x2 = (x - mu) * (x - mu);
  return 1. / std::sqrt(2. * M_PI * sigma2) * exp(-x2 / (2. * sigma2));
};

bool is_positive_definite(DistributionFunction const &);
bool has_nan_values(DistributionFunction const &);

enum class FuncTypes {
  pw_dens_pert,
  gauss,
  poly,
  rand_dens_pert,
  ibw_pert,
  restart,
  zero
};

bool available_init_keyword(std::string key);

}  // namespace Impl

/**
 * @brief Initialization of the distribution function on the local grid with a
 * plane wave density perturbation. The wavenumber must be chosen such that
 * periodic boundary conditions are satisfied.
 * @f[
 * f(\textbf{x},\textbf{v}) = (1+\alpha \sin(\textbf{k}\textbf{x}))
 * \frac{1}{(2 \pi T)^{\frac{3}{2}}}e^{-\frac{(\textbf{v} -
 * \textbf{v}_0)^2}{2T}}
 * @f]
 */
void plane_wave_dens_pertubation(DistributionFunction &f6d,
                                 DistributionInitializationConfig config);

/**
 * @brief Initialization of the distribution function on the local grid with a
 * 3D gaussian density perturbation.
 * @f[
 * f(\textbf{x},\textbf{v}) = \prod_{i=x_1}^{x_3} \frac{1}{\sqrt{2 \pi
 * \sigma_i}} e^{-\frac{(x_i - x_{0,i})^2}{2 \sigma_i}} \frac{1}{(2 \pi
 * T)^{\frac{3}{2}}} e^{-\frac{(\textbf{v} - \textbf{v}_0)^2}{2 T}}
 * @f]
 */
void gauss(DistributionFunction &f6d, DistributionInitializationConfig config);

/**
 * @brief Initialization of the distribution function with white noise density
 * perturbation
 *
 * @f[
 * f(\textbf{x},\textbf{v}) = (1 + \alpha\;
 * \text{rand}(\textbf{x}))\frac{1}{(2 \pi T)^{\frac{3}{2}}}
 * e^{-\frac{(\textbf{v} - \textbf{v}_0)^2}{2 T}}
 * @f]
 */
void rand_dens_perturbation(DistributionFunction &f6d,
                            DistributionInitializationConfig config);

// clang-format off
/**
 * @brief Initialization of the distribution function using a perturbation
 *        in the velocity space based on Bessel functions to excite 
 *        specifically Bernstein waves.
 *
 * @f[
 * f(\textbf{x},\textbf{v}) &= g_0(v) 
 *                             \left[1 + \alpha 
 *                               \sum_{m_{k_x}=1}^{m_{k_x,\text{max}}} \sum_{m_{k_y}=1}^{m_{k_y,\text{max}}}
 *                               \sum_{p=0}^{\text{max}(p_{v_x,\text{max}},p_{v_y,\text{max}})}
 *                                 J_p(k_\perp v_\perp) \text{Re}(e^{\text{i}(v_\perp k_\perp \sin(\gamma) - p\gamma + \textbf{k}\cdot\textbf{x})})
 *                             \right]\\
 * g_0(\textbf{v}) &= \frac{1}{(2 \pi T)^{\frac{3}{2}}} e^{-\frac{(\textbf{v} - \textbf{v}_0)^2}{2 T}}
 * @f]
 *
 * where \f$ J_p(\cdot) \f$ is the cylindrical Bessel function of the first kind and \f$\gamma\f$
 * is the angle \f$\sphericalangle(\textbf{k}_\perp,\textbf{v}_\perp)\f$. Perpendicular quantities are
 * perpendicular with respect to the magnetic field \f$ \textbf{v}_\perp\cdot\textbf{B}_0=\textbf{k}_\perp\cdot\textbf{B}_0=0 \f$.
 * 
 * \f$ p_{v_x,\text{max}}\f$,\f$ p_{v_y,\text{max}}\f$ and \f$ m_{k_x,\text{max}}\f$,\f$ m_{k_y,\text{max}}\f$ are set 
 * in the input file using the parameter `polynomOrder` and `mk`.
 * 
 */
// clang-format on
void ibw_perturbation(DistributionFunction const &f6d,
                      DistributionInitializationConfig config);

// clang-format off
/**
 * @brief Initializes the simulation with the provided configuration.
 * 
 * This function initializes the simulation with the provided configuration
 * file or command line arguments. It constructs the simulation time and 
 * distribution function based on the specified parameters. Depending on 
 * whether the simulation is a restart or a new run, different initialization
 * procedures are followed.
 * 
 * @param configFile Should only be used for testing purposes!
 *                    An optional JSON configuration file containing simulation parameters.
 *                    If provided, the function uses this file for initialization. 
 *                    If not provided, the function falls back to command line arguments.
 * 
 * @return A pair containing the initialized simulationTime and distributionFunction Objects.
 *         
 * @throws std::runtime_error If the distribution function contains negative phase space values after initialization, which is not permitted.
 */
// clang-format on
std::pair<SimulationTime, DistributionFunction> initialize(
    std::optional<nlohmann::json> configFile = std::nullopt);

std::pair<std::shared_ptr<Characteristic>, std::map<AxisType, Advector>>
    initialize_advector(DistributionFunction const &f6d);

std::unique_ptr<FieldSolver> initialize_fieldSolver();

} /*namespace bsl6d*/

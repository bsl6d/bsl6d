/*****************************************************************/
/*
\brief Base structure to define 6d distribution function

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/
#pragma once

#include <Kokkos_Core.hpp>

#include <boundaryHypercube.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <scalarField.hpp>
#include <vectorField.hpp>

namespace bsl6d {

namespace Impl {

struct VelocityMoments {
  ScalarField n{};
  VectorField particleFlux{};
  ScalarField energyDensity{};
  MatrixField secondMoment{};
  MatrixField reducedFourthMoment{};
  VectorField energyFlux{};
};

}  // namespace Impl

class DistributionFunction {
  public:
  DistributionFunction() = default;
  DistributionFunction(GridDistribution<3, 3>, DistributionFunctionConfig);
  DistributionFunction(DistributionFunction const&) = default;
  DistributionFunction(DistributionFunction&&)      = default;
  DistributionFunction& operator=(DistributionFunction const&);
  DistributionFunction& operator=(DistributionFunction&&);
  ~DistributionFunction() = default;

  KOKKOS_INLINE_FUNCTION
  double& operator()(size_t const ix1,
                     size_t const ix2,
                     size_t const ix3,
                     size_t const iv1,
                     size_t const iv2,
                     size_t const iv3) const {
    return data(ix1, ix2, ix3, iv1, iv2, iv3);
  };
  Kokkos::View<double******> device() const { return data; };
  Kokkos::View<double******>::HostMirror host() const { return h_data; };
  Kokkos::View<double*> electron_flux_surface_average() const {
    return m_initialFSAverage;
  }
  // To be replaced by mpi_local_grid();
  KOKKOS_INLINE_FUNCTION
  Grid<3, 3> grid() const { return m_gridDistribution.my_local_grid(); };
  KOKKOS_INLINE_FUNCTION
  Grid<3, 3> mpi_local_grid() const {
    return m_gridDistribution.my_local_grid();
  };
  GridDistribution<3, 3> gridDistribution() const {
    return m_gridDistribution;
  };
  KOKKOS_INLINE_FUNCTION
  double q() const { return m_q; };
  KOKKOS_INLINE_FUNCTION
  double m() const { return m_m; };
  Impl::VelocityMoments calculate_velocity_moments();
  ScalarField calculate_charge_density(std::string label);

  void require_velocity_moments_update() {
    velocityMomentStatus = VelocityMomentStatusType::RequireUpdate;
  }
  // Here needs to be a definition how the boundaries are filled;
  // Currently only periodic implemented for halo exchange
  // Maybe definition if section of distribution function really is part of
  // physical domain boundary (not compulational domain boundary of grid)

  private:
  GridDistribution<3, 3> m_gridDistribution;

  Kokkos::View<double******> data{};
  Kokkos::View<double******>::HostMirror h_data{};
  Kokkos::View<double*> m_initialFSAverage{};

  double m_q{};
  double m_m{};

  enum class VelocityMomentStatusType { UpToDate, RequireUpdate };
  VelocityMomentStatusType velocityMomentStatus{
      VelocityMomentStatusType::RequireUpdate};
  Impl::VelocityMoments m_velocityMoments{};
};

namespace Impl {
template <typename AnalyticalExpression>
void fill(DistributionFunction& f6d,
          AnalyticalExpression const& analyticalExpression) {
  Kokkos::parallel_for(
      "DistributionFunctionFiller", f6d.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        double x1{f6d.mpi_local_grid()(AxisType::x1)(ix1)};
        double x2{f6d.mpi_local_grid()(AxisType::x2)(ix2)};
        double x3{f6d.mpi_local_grid()(AxisType::x3)(ix3)};
        double v1{f6d.mpi_local_grid()(AxisType::v1)(iv1)};
        double v2{f6d.mpi_local_grid()(AxisType::v2)(iv2)};
        double v3{f6d.mpi_local_grid()(AxisType::v3)(iv3)};
        f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
            analyticalExpression(x1, x2, x3, v1, v2, v3);
      });
  f6d.require_velocity_moments_update();
}
}  // namespace Impl

}  // namespace bsl6d

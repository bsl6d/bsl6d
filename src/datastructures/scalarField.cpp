/*****************************************************************/
/*
\brief Base structure to define to define 3D scalar field

\author Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth.mpg.de
\date   10.02.2022

*/
/*****************************************************************/

#include <scalarField.hpp>

#include <iostream>

namespace bsl6d {

/*****************************************************************/
/*

\brief Create scalarfield object  with grid
\param[in] _grid - grid object

*/
/*****************************************************************/
ScalarField::ScalarField(GridDistribution<3, 0> const &gridDistribution,
                         std::string const &label)
    : m_gridDistribution{gridDistribution}
    , values{create_grid_compatible_view(grid(), label), {0, 0, 0}}
    , h_values{Kokkos::create_mirror_view(values)} {}

ScalarField &ScalarField::operator=(ScalarField const &field) {
  if ((grid() == field.grid())) {
    if (this->values.label() == field.values.label()) {
      this->values      = field.values;
      this->h_values    = field.h_values;
      this->m_haloWidth = field.m_haloWidth;
      return *this;
    } else {
      throw std::runtime_error("Views do not have same label in ScalarField " +
                               field.values.label() + " in " +
                               this->values.label());
    }
  } else if (grid() == Grid<3, 0>{}) {
    this->m_gridDistribution = field.m_gridDistribution;
    this->values             = field.values;
    this->h_values           = field.h_values;
    this->m_haloWidth        = field.m_haloWidth;
    return *this;
  } else {
    throw std::runtime_error(
        "Grids are not identical in copy assignment of ScalarField");
  }
}

ScalarField &ScalarField::operator=(ScalarField &&field) {
  if (grid() == grid()) {
    if (this->values.label() == field.values.label()) {
      this->values   = std::move(field.values);
      this->h_values = std::move(field.h_values);
      return *this;
    } else if (grid() == Grid<3, 0>{}) {
      this->m_gridDistribution = std::move(field.m_gridDistribution);
      this->values             = std::move(field.values);
      this->h_values           = std::move(field.h_values);
      return *this;
    } else {
      throw std::runtime_error("Views do not have same label in ScalarField");
    }
  } else {
    throw std::runtime_error(
        "Grids are not identical in copy assignment of ScalarField");
  }
}

void ScalarField::set_halo_width(AxisType axis, int haloWidth) {
  std::map<AxisType, int> haloWidthOld{m_haloWidth};
  if (m_haloWidth[axis] < haloWidth) {
    m_haloWidth[axis] = haloWidth;
    Kokkos::Experimental::OffsetView<double ***> extendedOffsetView(
        values.label(),
        std::make_pair(-m_haloWidth[AxisType::x1],
                       static_cast<int>(grid()(AxisType::x1).N() +
                                        m_haloWidth[AxisType::x1] - 1)),
        std::make_pair(-m_haloWidth[AxisType::x2],
                       static_cast<int>(grid()(AxisType::x2).N() +
                                        m_haloWidth[AxisType::x2] - 1)),
        std::make_pair(-m_haloWidth[AxisType::x3],
                       static_cast<int>(grid()(AxisType::x3).N() +
                                        m_haloWidth[AxisType::x3] - 1)));
    auto oldComputeView = Impl::create_compute_range_subview(device(), grid());
    auto newComputeView =
        Impl::create_compute_range_subview(extendedOffsetView, grid());
    Kokkos::deep_copy(newComputeView, oldComputeView);
    values   = extendedOffsetView;
    h_values = Kokkos::create_mirror_view(values);
  }
};

Kokkos::View<double *> flux_surface_average(ScalarField const &scalarField) {
  Kokkos::View<double *> fluxSurfAvg{
      "FluxSurfaceAverage" + scalarField.device().label(),
      scalarField.grid()(AxisType::x1).N()};
  Kokkos::deep_copy(fluxSurfAvg, 0.0);
  CartTopology<2> x2x3Topology{
      scalarField.gridDistribution().cart_topology().split<2>(
          {false, true, true})};

  double dx2dx3 =
      scalarField.gridDistribution().mpi_global_grid()(AxisType::x2).delta() *
      scalarField.gridDistribution().mpi_global_grid()(AxisType::x3).delta();
  double L2L3 =
      scalarField.gridDistribution().mpi_global_grid()(AxisType::x2).L() *
      scalarField.gridDistribution().mpi_global_grid()(AxisType::x3).L();
  Kokkos::parallel_for(
      "YZAverage", scalarField.grid()(AxisType::x1).N(),
      KOKKOS_LAMBDA(size_t ix1) {
        for (size_t ix2 = 0; ix2 < scalarField.grid()(AxisType::x2).N(); ix2++)
          for (size_t ix3 = 0; ix3 < scalarField.grid()(AxisType::x3).N();
               ix3++) {
            fluxSurfAvg(ix1) += scalarField(ix1, ix2, ix3) * dx2dx3 / L2L3;
          }
      });
#if BSL6D_ENABLE_CUDA_AWARE_MPI
  Kokkos::fence();
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, fluxSurfAvg.data(),
                                 fluxSurfAvg.size(), MPI_DOUBLE, MPI_SUM,
                                 x2x3Topology.cart_comm()));
#else
  Kokkos::View<double *>::HostMirror h_fluxSurfAvg{
      Kokkos::create_mirror_view(fluxSurfAvg)};
  Kokkos::deep_copy(h_fluxSurfAvg, fluxSurfAvg);
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, h_fluxSurfAvg.data(),
                                 fluxSurfAvg.size(), MPI_DOUBLE, MPI_SUM,
                                 x2x3Topology.cart_comm()));
  Kokkos::deep_copy(fluxSurfAvg, h_fluxSurfAvg);
#endif

  return fluxSurfAvg;
}

}  // namespace bsl6d

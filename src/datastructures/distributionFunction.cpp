/*****************************************************************/
/*
\brief Base structure to define 6d distribution function

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/

#include <distributionFunction.hpp>
#include <velocityMomements.hpp>

#include <iostream>

namespace bsl6d {

/*****************************************************************/
/*

\brief Create distribution function

\param gridDistribution - Grid Distribution on which DistributionFunction
                          is based.

*/
/*****************************************************************/
DistributionFunction::DistributionFunction(
    GridDistribution<3, 3> gridDistribution,
    DistributionFunctionConfig conf)
    : m_gridDistribution{gridDistribution}
    , data{create_grid_compatible_view(grid(), "f6d")}
    , h_data{Kokkos::create_mirror_view(data)}
    , m_initialFSAverage{create_grid_compatible_view(
          grid(),
          "initialFluxSurfaceAverage",
          std::array{AxisType::x1})}
    , m_q{conf.config.q}
    , m_m{conf.config.m} {}
DistributionFunction& DistributionFunction::operator=(
    DistributionFunction const& other) {
  if ((grid() == other.grid())) {
    if ((std::abs(m_m - other.m_m) > 1e-15) or
        (std::abs(m_q - other.m_q) > 1e-15)) {
      throw std::runtime_error(
          std::string{"Particle properties do not match\n"} + " m: " +
          std::to_string(m_m) + " other.m: " + std::to_string(other.m_m) +
          "\n" + " q: " + std::to_string(m_q) +
          " other.q: " + std::to_string(other.m_q) + "\n");
    }
    if (this->data.label() == other.data.label()) {
      this->data               = other.data;
      this->h_data             = other.h_data;
      this->m_initialFSAverage = other.m_initialFSAverage;

      return *this;
    } else {
      throw std::runtime_error(
          "Views do not have same label in DistributionFunction " +
          other.data.label() + " in " + this->data.label());
    }
  } else if (grid() == Grid<3, 3>{}) {
    this->m_gridDistribution = other.m_gridDistribution;
    this->data               = other.data;
    this->m_initialFSAverage = other.m_initialFSAverage;
    this->h_data             = other.h_data;
    this->m_m                = other.m_m;
    this->m_q                = other.m_q;
    return *this;
  } else {
    throw std::runtime_error(
        "Grids are not identical in copy assignment of DistributionFunction " +
        other.data.label() + " in " + this->data.label());
  }
};

DistributionFunction& DistributionFunction::operator=(
    DistributionFunction&& other) {
  if ((grid() == other.grid())) {
    if ((std::abs(m_m - other.m_m) > 1e-15) or
        (std::abs(m_q - other.m_q) > 1e-15)) {
      throw std::runtime_error(
          std::string{"Particle properties do not match\n"} + " m: " +
          std::to_string(m_m) + " other.m: " + std::to_string(other.m_m) +
          "\n" + " q: " + std::to_string(m_q) +
          " other.q: " + std::to_string(other.m_q) + "\n");
    }
    if (this->data.label() == other.data.label()) {
      this->data               = std::move(other.data);
      this->h_data             = std::move(other.h_data);
      this->m_initialFSAverage = std::move(other.m_initialFSAverage);
      return *this;
    } else {
      throw std::runtime_error(
          "Views do not have same label in DistributionFunction " +
          other.data.label() + " in " + this->data.label());
    }
  } else if (grid() == Grid<3, 3>{}) {
    this->m_gridDistribution = std::move(other.m_gridDistribution);
    this->data               = std::move(other.data);
    this->m_initialFSAverage = std::move(other.m_initialFSAverage);
    this->h_data             = std::move(other.h_data);
    this->m_m                = std::move(other.m_m);
    this->m_q                = std::move(other.m_q);
    return *this;
  } else {
    throw std::runtime_error(
        "Grids are not identical in copy assignment of DistributionFunction " +
        other.data.label() + " in " + this->data.label());
  }
};

Impl::VelocityMoments DistributionFunction::calculate_velocity_moments() {
  if (velocityMomentStatus == VelocityMomentStatusType::RequireUpdate) {
    m_velocityMoments    = Impl::calculate_velocity_moments(*this);
    velocityMomentStatus = VelocityMomentStatusType::UpToDate;
  }
  return m_velocityMoments;
};

ScalarField DistributionFunction::calculate_charge_density(std::string label) {
  ScalarField rho{gridDistribution().create_xGridDistribution(), label};
  Kokkos::View<double******> _f{this->data};
  Grid<3, 3> _grid{m_gridDistribution.mpi_local_grid()};
  double q{this->q()};

  Kokkos::Profiling::pushRegion("CalculateChargeDensity");
  Kokkos::parallel_for(
      "sum3dv", rho.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        double sum_single = 0;

        for (size_t iv1 = 0; iv1 < _grid(AxisType::v1).N(); iv1++)
          for (size_t iv2 = 0; iv2 < _grid(AxisType::v2).N(); iv2++)
            for (size_t iv3 = 0; iv3 < _grid(AxisType::v3).N(); iv3++)
              sum_single += _f(ix1, ix2, ix3, iv1, iv2, iv3);

        rho(ix1, ix2, ix3) = q * sum_single * _grid.cell_vol_v();
      });

  Kokkos::fence("Calculate Charge Density");
  bsl6d::CartTopology<3> topologyV{m_gridDistribution.cart_topology().split<3>(
      {false, false, false, true, true, true})};
#if BSL6D_ENABLE_CUDA_AWARE_MPI
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, rho.device().data(),
                                 rho.device().size(), MPI_DOUBLE, MPI_SUM,
                                 topologyV.cart_comm()));
#else
  Kokkos::deep_copy(rho.host(), rho.device());
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, rho.device().data(),
                                 rho.device().size(), MPI_DOUBLE, MPI_SUM,
                                 topologyV.cart_comm()));
  Kokkos::deep_copy(rho.device(), rho.host());
#endif
  Kokkos::Profiling::popRegion();
  // Kokkos::deep_copy(rho.device(),
  //                   (q() * calculate_velocity_moments().n)());

  return rho;
};

}  // namespace bsl6d

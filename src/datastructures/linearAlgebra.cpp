#include <Kokkos_Core.hpp>

#include <scalarField.hpp>
#include <vectorField.hpp>

#include "linearAlgebra.hpp"

namespace bsl6d {

ScalarField operator+=(ScalarField &a, const ScalarField &b) {
  Kokkos::parallel_for(
      "Add " + b.device().label() + " to " + a.device().label(),
      a.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        a(ix1, ix2, ix3) += b(ix1, ix2, ix3);
      });
  return a;
}

ScalarField operator*(const ScalarField &sf, const double &a) {
  ScalarField res{
      sf.gridDistribution(),
      "Undefined label after Multiplication of scalar with ScalarField" +
          sf.device().label()};
  auto res_view   = res.device();
  auto field_view = sf.device();
  Kokkos::parallel_for(
      "ScalarField*Scalar", sf.device().size(), KOKKOS_LAMBDA(size_t i) {
        res_view.data()[i] = field_view.data()[i] * a;
      });
  return res;
}
ScalarField operator*(const double &a, const ScalarField &sf) { return sf * a; }

ScalarField operator*(const ScalarField &a, const ScalarField &b) {
  ScalarField res{a.gridDistribution(),
                  "Undefined label after Multiplication of ScalarField" +
                      a.device().label() + " with ScalarField " +
                      b.device().label()};
  Kokkos::parallel_for(
      "ScalarField*ScalarField", a.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        res(ix1, ix2, ix3) = a(ix1, ix2, ix3) * b(ix1, ix2, ix3);
      });
  return res;
}

VectorField operator*(const VectorField &vf, const double &a) {
  VectorField res{};
  for (auto axis : vf(AxisType::x1).mpi_local_grid().xAxes)
    res.replace_scalarField(axis, vf(axis) * a);
  return res;
};

VectorField operator*(const double &a, const VectorField &vf) { return vf * a; }

VectorField operator*(const ScalarField &sf, const VectorField &vf) {
  VectorField res{};
  for (auto axis : vf(AxisType::x1).mpi_local_grid().xAxes)
    res.replace_scalarField(axis, vf(axis) * sf);
  return res;
}

VectorField operator*(const VectorField &vf, const ScalarField &sf) {
  return sf * vf;
}

ScalarField operator*(const VectorField &vf1, const VectorField &vf2) {
  ScalarField res{vf1(AxisType::x1).gridDistribution(),
                  "Undefined label after ScalarProduct of VectorField" +
                      vf1.label() + " with VectorField " + vf2.label()};
  for (auto axis : res.mpi_local_grid().xAxes) { res += vf1(axis) * vf2(axis); }
  return res;
};

VectorField operator*(const Matrix &matrix, const VectorField &vf) {
  VectorField res{
      vf(AxisType::x1).gridDistribution(),
      "Undefined label after Multiplication of Matrix with VectorField" +
          vf.label()};
  Kokkos::parallel_for(
      "Matrix*VectorField Multiplication",
      vf(AxisType::x1).mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        auto vec{matrix * vf(ix1, ix2, ix3)};
        for (auto axis : vf(AxisType::x1).mpi_local_grid().xAxes) {
          res(axis)(ix1, ix2, ix3) = vec(axis);
        }
      });
  return res;
};

MatrixField operator*(const Matrix &matrix, const MatrixField &mf) {
  MatrixField res{
      mf(AxisType::x1, AxisType::x1).gridDistribution(),
      "Undefined label after Multiplication of Matrix with MatrixField " +
          mf.label()};
  Kokkos::parallel_for(
      "Matrix*MatrixField Multiplication",
      mf(AxisType::x1, AxisType::x1).mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        auto mat{matrix * mf(ix1, ix2, ix3)};
        for (auto axis1 : mf(AxisType::x1, AxisType::x1).mpi_local_grid().xAxes)
          for (auto axis2 :
               mf(AxisType::x1, AxisType::x1).mpi_local_grid().xAxes)
            res(axis1, axis2)(ix1, ix2, ix3) = mat(axis1, axis2);
      });
  return res;
};

MatrixField operator*(const MatrixField &mf, const Matrix &matrix) {
  MatrixField res{
      mf(AxisType::x1, AxisType::x1).gridDistribution(),
      "Undefined label after Multiplication of MatrixField with Matrix " +
          mf.label()};
  Kokkos::parallel_for(
      "Matrix*MatrixField Multiplication",
      mf(AxisType::x1, AxisType::x1).mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        auto mat{mf(ix1, ix2, ix3) * matrix};
        for (auto axis1 : mf(AxisType::x1, AxisType::x1).mpi_local_grid().xAxes)
          for (auto axis2 :
               mf(AxisType::x1, AxisType::x1).mpi_local_grid().xAxes)
            res(axis1, axis2)(ix1, ix2, ix3) = mat(axis1, axis2);
      });
  return res;
};

}  // namespace bsl6d
#include <mpi.h>

#include <distributionFunction.hpp>
#include <scalarField.hpp>

#include "mathNorms.hpp"

namespace bsl6d {

std::map<std::string, double> L_errors(DistributionFunction const& f,
                                       DistributionFunction const& g) {
  std::map<std::string, double> res{};
  res = Impl::L_errors(f.device(), g.device());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("L1_err"), 1, MPI_DOUBLE, MPI_SUM,
                f.gridDistribution().cart_topology().comm());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("L2_err^2"), 1, MPI_DOUBLE, MPI_SUM,
                f.gridDistribution().cart_topology().comm());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("LInf_err"), 1, MPI_DOUBLE, MPI_MAX,
                f.gridDistribution().cart_topology().comm());
  res.at("L1_err") *= f.gridDistribution().mpi_global_grid().cell_vol_xv();
  res.at("L2_err^2") *= f.gridDistribution().mpi_global_grid().cell_vol_xv();
  return res;
};
std::map<std::string, double> L_errors(ScalarField const& f,
                                       ScalarField const& g) {
  std::map<std::string, double> res{};
  res = Impl::L_errors(f.get_contiguous_view_of_compute_region(),
                       g.get_contiguous_view_of_compute_region());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("L1_err"), 1, MPI_DOUBLE, MPI_SUM,
                f.gridDistribution().cart_topology().comm());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("L2_err^2"), 1, MPI_DOUBLE, MPI_SUM,
                f.gridDistribution().cart_topology().comm());
  MPI_Allreduce(MPI_IN_PLACE, &res.at("LInf_err"), 1, MPI_DOUBLE, MPI_MAX,
                f.gridDistribution().cart_topology().comm());
  res.at("L1_err") *= f.gridDistribution().mpi_global_grid().cell_vol_xv();
  res.at("L2_err^2") *= f.gridDistribution().mpi_global_grid().cell_vol_xv();
  return res;
};

std::map<std::string, double> L_norms(DistributionFunction const& f) {
  DistributionFunction g{f.gridDistribution(),
                         bsl6d::DistributionFunctionConfig{}};
  Kokkos::deep_copy(g.device(), 0.0);
  std::map<std::string, double> err{L_errors(f, g)};
  std::map<std::string, double> norm{};
  norm["L1"]   = err["L1_err"];
  norm["L2^2"] = err["L2_err^2"];
  norm["LInf"] = err["LInf_err"];
  return norm;
};

std::map<std::string, double> L_norms(ScalarField const& f) {
  ScalarField g{f.gridDistribution(), "zeroScalarField"};
  Kokkos::deep_copy(g.device(), 0.0);
  std::map<std::string, double> err{L_errors(f, g)};
  std::map<std::string, double> norm{};
  norm["L1"]   = err["L1_err"];
  norm["L2^2"] = err["L2_err^2"];
  norm["LInf"] = err["LInf_err"];
  return norm;
};

double L1_norm(DistributionFunction const& f) { return L_norms(f).at("L1"); };
double L2_norm(DistributionFunction const& f) {
  return std::sqrt(L_norms(f).at("L2^2"));
};
double LInf_norm(DistributionFunction const& f) {
  return L_norms(f).at("LInf");
};
double L1_norm(ScalarField const& f) { return L_norms(f).at("L1"); };
double L2_norm(ScalarField const& f) {
  return std::sqrt(L_norms(f).at("L2^2"));
};
double LInf_norm(ScalarField const& f) { return L_norms(f).at("LInf"); };

double L1_error(DistributionFunction const& f, DistributionFunction const& g) {
  return L_errors(f, g).at("L1_err");
};
double L2_error(DistributionFunction const& f, DistributionFunction const& g) {
  return std::sqrt(L_errors(f, g).at("L2_err^2"));
};
double LInf_error(DistributionFunction const& f,
                  DistributionFunction const& g) {
  return L_errors(f, g).at("LInf_err");
};
double L1_error(ScalarField const& f, ScalarField const& g) {
  return L_errors(f, g).at("L1_err");
};
double L2_error(ScalarField const& f, ScalarField const& g) {
  return std::sqrt(L_errors(f, g).at("L2_err^2"));
};
double LInf_error(ScalarField const& f, ScalarField const& g) {
  return L_errors(f, g).at("LInf_err");
};

}  // namespace bsl6d

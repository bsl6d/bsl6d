/*****************************************************************/
/*
\brief Testing of datastructure containing 6d distribution
       function.

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <grid.hpp>

#include <gridDistribution.hpp>
#include <mpiTopology.hpp>
#include "../distributionFunction.hpp"
#include "config.hpp"
#include "initializationFunctions.hpp"

nlohmann::json configFileDistFunc = R"(
  {
    "distributionFunction": {
      "q": 2.0,
      "m": 3.0
    },
    "gridDistribution": {
      "grid" : {},
      "parallel" : {}
    }
  })"_json;

class DatastructuresDistFunc : public ::testing::Test {
  public:
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::CartTopology<6> topo{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topo};
  bsl6d::DistributionFunction f6d{
      gridDistribution, bsl6d::DistributionFunctionConfig{configFileDistFunc}};

  DatastructuresDistFunc() = default;
};

/*****************************************************************/
/*

\brief Test initialization of distribution function

*/
/*****************************************************************/
TEST_F(DatastructuresDistFunc, Init) {
  bsl6d::DistributionFunction{};

  EXPECT_EQ(f6d.q(), 2);
  EXPECT_EQ(f6d.m(), 3);

  ASSERT_EQ(f6d.device().size(), 8139600);
  ASSERT_EQ(f6d.host().size(), f6d.device().size());
  for (auto axis : bsl6d::xvAxes())
    EXPECT_EQ(f6d.device().extent(static_cast<size_t>(axis)), grid(axis).N());
}

TEST_F(DatastructuresDistFunc, CopyAssignment) {
  bsl6d::DistributionFunction empty{};
  EXPECT_NO_THROW(empty = f6d);
  bsl6d::DistributionFunction wrongParticleProperties{
      gridDistribution, bsl6d::DistributionFunctionConfig{}};
  EXPECT_THROW(f6d = wrongParticleProperties, std::runtime_error);
  configFileDistFunc["gridDistribution"]["grid"] =
      R"({
    "axes": ["x1", "x2", "x3", "v1", "v2", "v3"],
    "xvMax": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    "xvMin": [0.0, 0.0, 0.0, -4.0, -4.0, -4.0],
    "NxvPoints": [2, 3, 4, 5, 6, 7],
    "position": ["L", "L", "L", "N", "N", "N"],
    "rotateGyroFreq": false
    })"_json;
  bsl6d::Grid<3, 3> wrongGrid{
      bsl6d::GridDistributionConfig{configFileDistFunc}};
  bsl6d::GridDistribution<3, 3> gridDistribution{wrongGrid, topo};
  bsl6d::DistributionFunction wrongGridDistFunc{
      gridDistribution, bsl6d::DistributionFunctionConfig{configFileDistFunc}};
  EXPECT_THROW(f6d = wrongGridDistFunc, std::runtime_error);
}

TEST_F(DatastructuresDistFunc, VelocityMoments) {
  Kokkos::deep_copy(f6d.device(), 0.0);
  bsl6d::Impl::VelocityMoments velocityMoments{
      f6d.calculate_velocity_moments()};
  Kokkos::deep_copy(velocityMoments.n.host(), velocityMoments.n.device());
  EXPECT_EQ(velocityMoments.n.host()(0, 0, 0), 0.0);
  Kokkos::deep_copy(f6d.device(), 1.0);
  velocityMoments = f6d.calculate_velocity_moments();
  Kokkos::deep_copy(velocityMoments.n.host(), velocityMoments.n.device());
  EXPECT_DOUBLE_EQ(velocityMoments.n.host()(0, 0, 0), 0.0);

  f6d.require_velocity_moments_update();
  velocityMoments = f6d.calculate_velocity_moments();
  Kokkos::deep_copy(velocityMoments.n.host(), velocityMoments.n.device());
  EXPECT_GT(velocityMoments.n.host()(0, 0, 0), 0.0);
}

TEST_F(DatastructuresDistFunc, ChargeDensity) {
  Kokkos::deep_copy(f6d.device(), 1.0);
  bsl6d::ScalarField rho{f6d.gridDistribution().create_xGridDistribution(),
                         "rho"};
  rho = f6d.calculate_charge_density("rho");
  double chargeDensity{1};
  for (auto axis : bsl6d::vAxes()) chargeDensity *= grid(axis).N();
  chargeDensity *= grid.cell_vol_v();
  chargeDensity *= f6d.q();
  Kokkos::deep_copy(rho.host(), rho.device());
  EXPECT_DOUBLE_EQ(rho.host()(0, 0, 0), chargeDensity);
}

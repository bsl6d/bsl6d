#include <gtest/gtest.h>

#include <Kokkos_Core.hpp>

#include <linearAlgebra.hpp>
#include <mathNorms.hpp>
#include <vectorField.hpp>

class DatastructuresLinearAlgebra : public ::testing::Test {
  public:
  bsl6d::GridDistribution<3, 3> gridDistribution{
      bsl6d::GridDistributionConfig{}};
  bsl6d::ScalarField sf1{gridDistribution.create_xGridDistribution(), ""},
      sf2{gridDistribution.create_xGridDistribution(), ""},
      sf3{gridDistribution.create_xGridDistribution(), ""};
  bsl6d::VectorField vf1{gridDistribution.create_xGridDistribution(), ""},
      vf2{gridDistribution.create_xGridDistribution(), ""};
  bsl6d::MatrixField mf{gridDistribution.create_xGridDistribution(), ""};

  double LInf_error(bsl6d::ScalarField const& a, double const& b) {
    double err{};
    Kokkos::parallel_reduce(
        "Test Multiplication", a.mpi_local_grid().mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, double& l_err) {
          l_err = std::max(std::abs(a(ix1, ix2, ix3) - b), l_err);
        },
        Kokkos::Max<double>(err));
    return err;
  }

  double LInf_error(bsl6d::VectorField const& a, bsl6d::Vector b) {
    std::array<double, 3> err{};
    Kokkos::parallel_reduce(
        "Test Multiplication", a(bsl6d::AxisType::x1).grid().mdRangePolicy(),
        KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, double& l_errX1,
                      double& l_errX2, double& l_errX3) {
          l_errX1 = std::max(std::abs(a(bsl6d::AxisType::x1)(ix1, ix2, ix3) -
                                      b(bsl6d::AxisType::x1)),
                             l_errX1);
          l_errX2 = std::max(std::abs(a(bsl6d::AxisType::x2)(ix1, ix2, ix3) -
                                      b(bsl6d::AxisType::x2)),
                             l_errX2);
          l_errX3 = std::max(std::abs(a(bsl6d::AxisType::x3)(ix1, ix2, ix3) -
                                      b(bsl6d::AxisType::x3)),
                             l_errX3);
        },
        Kokkos::Max<double>(err[0]), Kokkos::Max<double>(err[1]),
        Kokkos::Max<double>(err[2]));
    auto maxErr{std::max_element(err.begin(), err.end())};
    return *maxErr;
  }

  double LInf_error(bsl6d::MatrixField const& a, bsl6d::Matrix const& b) {
    std::array<double, 9> err{};
    bsl6d::Grid<3, 0> grid{
        a(bsl6d::AxisType::x1, bsl6d::AxisType::x1).mpi_local_grid()};
    for (auto axis1 : grid.xAxes) {
      for (auto axis2 : grid.xAxes) {
        Kokkos::parallel_reduce(
            "Test Multiplication", grid.mdRangePolicy(),
            KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, double& l_err) {
              l_err = std::max(
                  std::abs(a(axis1, axis2)(ix1, ix2, ix3) - b(axis1, axis2)),
                  l_err);
            },
            Kokkos::Max<double>(err[axis1 + 3 * axis2]));
      }
    }

    auto maxErr{std::max_element(err.begin(), err.end())};
    return *maxErr;
  }
};

TEST_F(DatastructuresLinearAlgebra, MatrixVectorMultiplication) {
  bsl6d::Matrix matrix{std::array{1.0, 1.0, 1.0}, std::array{2.0, 2.0, 2.0},
                       std::array{3.0, 3.0, 3.0}};
  bsl6d::Vector vector{1.0, 1.0, 1.0};
  bsl6d::Vector resultVector{3.0, 6.0, 9.0};
  EXPECT_EQ((matrix * vector), resultVector);
}

TEST_F(DatastructuresLinearAlgebra, MatrixMatrixMultiplication) {
  bsl6d::Matrix A{std::array<double, 3>{1.0, 2.0, 3.0},
                  std::array<double, 3>{4.0, 5.0, 6.0},
                  std::array<double, 3>{7.0, 8.0, 9.0}};
  bsl6d::Matrix B{std::array<double, 3>{1.0, 2.0, 3.0},
                  std::array<double, 3>{1.0, 2.0, 3.0},
                  std::array<double, 3>{1.0, 2.0, 3.0}};
  bsl6d::Matrix resultMatrix{std::array<double, 3>{6.0, 12.0, 18.0},
                             std::array<double, 3>{15.0, 30.0, 45.0},
                             std::array<double, 3>{24.0, 48.0, 72.0}};
  EXPECT_EQ((A * B), resultMatrix);
}

TEST_F(DatastructuresLinearAlgebra, AddAndAssignScalarField) {
  Kokkos::deep_copy(sf1.device(), 1.0);
  Kokkos::deep_copy(sf2.device(), 2.0);
  sf1 += sf2;
  EXPECT_EQ(LInf_error(sf1, 3), 0.0);
  EXPECT_EQ(sf1.device().label(), "");
}

TEST_F(DatastructuresLinearAlgebra, ScalarMultiplicationScalarField) {
  Kokkos::deep_copy(sf1.device(), 1.0);

  EXPECT_EQ(LInf_error(sf1 * 2, 2), 0);
  EXPECT_EQ(LInf_error(4 * sf1, 4), 0);
}

TEST_F(DatastructuresLinearAlgebra, ScalarFieldMultiplicationScalarField) {
  Kokkos::deep_copy(sf1.device(), 2.0);
  Kokkos::deep_copy(sf2.device(), -3.0);

  EXPECT_EQ(LInf_error(sf1 * sf2, -6.0), 0);
}

TEST_F(DatastructuresLinearAlgebra, ScalarValueVectorFieldMultiplication) {
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x1).device(), 1.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x2).device(), 2.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x3).device(), 3.0);

  EXPECT_EQ(LInf_error(vf1 * (-2), {-2, -4, -6}), 0.0);

  EXPECT_EQ(LInf_error(2 * vf1, {2, 4, 6}), 0.0);
}

TEST_F(DatastructuresLinearAlgebra, ScalarFieldVectorFieldMultiplication) {
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x1).device(), 1.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x2).device(), 2.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x3).device(), 3.0);
  Kokkos::deep_copy(sf1.device(), 3.0);

  EXPECT_EQ(LInf_error(vf1 * sf1, {3, 6, 9}), 0.0);

  EXPECT_EQ(LInf_error(sf1 * vf1, {3, 6, 9}), 0.0);
}

TEST_F(DatastructuresLinearAlgebra, VectorFieldScalarProduct) {
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x1).device(), 1.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x2).device(), 2.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x3).device(), 3.0);
  Kokkos::deep_copy(vf2(bsl6d::AxisType::x1).device(), 3.0);
  Kokkos::deep_copy(vf2(bsl6d::AxisType::x2).device(), 4.0);
  Kokkos::deep_copy(vf2(bsl6d::AxisType::x3).device(), 5.0);

  EXPECT_EQ(LInf_error(vf1 * vf2, 26), 0.0);
}

TEST_F(DatastructuresLinearAlgebra, MatrixVectorFieldMultiplication) {
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x1).device(), 1.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x2).device(), 2.0);
  Kokkos::deep_copy(vf1(bsl6d::AxisType::x3).device(), 3.0);

  bsl6d::Matrix matrix{std::array{1.0, 2.0, 3.0}, std::array{4.0, 5.0, 6.0},
                       std::array{7.0, 8.0, 9.0}};

  EXPECT_EQ(LInf_error(matrix * vf1, {14.0, 32.0, 50.0}), 0.0);
}

TEST_F(DatastructuresLinearAlgebra, MatrixMatrixFieldMultiplication) {
  bsl6d::Grid<3, 0> grid{
      gridDistribution.create_xGridDistribution().mpi_local_grid()};
  bsl6d::Matrix A{std::array{1.0, 3.0, 5.0}, std::array{2.0, 4.0, 6.0},
                  std::array{5.0, 5.0, 7.0}};
  bsl6d::Matrix B{std::array{1.0, 2.0, 3.0}, std::array{4.0, 5.0, 6.0},
                  std::array{7.0, 8.0, 9.0}};
  bsl6d::Matrix AB{A * B};
  bsl6d::Matrix BA{B * A};

  for (auto axis1 : grid.xAxes)
    for (auto axis2 : grid.xAxes) {
      Kokkos::deep_copy(mf(axis1, axis2).device(), A(axis1, axis2));
    }
  EXPECT_EQ(LInf_error(mf * B, AB), 0.0);
  EXPECT_GT(LInf_error(mf * B, BA), 0.0);

  for (auto axis1 : grid.xAxes)
    for (auto axis2 : grid.xAxes) {
      Kokkos::deep_copy(mf(axis1, axis2).device(), B(axis1, axis2));
    }
  EXPECT_EQ(LInf_error(A * mf, AB), 0.0);
  EXPECT_GT(LInf_error(A * mf, BA), 0.0);
}

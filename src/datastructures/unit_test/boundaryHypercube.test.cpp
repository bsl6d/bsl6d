/*****************************************************************/
/*
\brief Testing of boundaries used for hypercube

\author Nils Schild, Mario Raeth
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021
*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <array>
#include <grid.hpp>

#include "../boundaryHypercube.hpp"

class DatastructuresBoundaryHypercube : public ::testing::Test {
  public:
  DatastructuresBoundaryHypercube() = default;
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  std::map<bsl6d::AxisType, size_t> Nxv{
      {bsl6d::AxisType::x1, 3}, {bsl6d::AxisType::x2, 4},
      {bsl6d::AxisType::x3, 5}, {bsl6d::AxisType::v1, 6},
      {bsl6d::AxisType::v2, 7}, {bsl6d::AxisType::v3, 8}};
  std::map<bsl6d::AxisType, size_t> boundaryWidth{
      {bsl6d::AxisType::x1, 1}, {bsl6d::AxisType::x2, 2},
      {bsl6d::AxisType::x3, 3}, {bsl6d::AxisType::v1, 4},
      {bsl6d::AxisType::v2, 5}, {bsl6d::AxisType::v3, 6}};
  bsl6d::BoundaryHypercube boundary{Nxv, boundaryWidth};
};

TEST_F(DatastructuresBoundaryHypercube, SharedMemSize) {
  ASSERT_EQ(boundary.shared_buffer_size("low"), 15120);
  ASSERT_EQ(boundary.shared_buffer_size("up"), 15120);
}

TEST_F(DatastructuresBoundaryHypercube, SharedBoundaryViews) {
  for (auto boundaryAxis : bsl6d::xvAxes()) {
    boundary.set_my_axis(boundaryAxis);

    for (auto axis : bsl6d::xvAxes()) {
      if (axis != boundaryAxis) {
        EXPECT_EQ(boundary.low(boundaryAxis).extent(static_cast<size_t>(axis)),
                  Nxv[axis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
        EXPECT_EQ(
            boundary.h_low(boundaryAxis).extent(static_cast<size_t>(axis)),
            Nxv[axis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
      } else {
        EXPECT_EQ(boundary.low(boundaryAxis).extent(static_cast<size_t>(axis)),
                  boundaryWidth[boundaryAxis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
        EXPECT_EQ(
            boundary.h_low(boundaryAxis).extent(static_cast<size_t>(axis)),
            boundaryWidth[boundaryAxis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
      }
    }
    ASSERT_LE(boundary.low(boundaryAxis).size(),
              boundary.shared_buffer_size("low"));
    for (auto axis : bsl6d::xvAxes()) {
      if (axis != boundaryAxis) {
        EXPECT_EQ(boundary.up(boundaryAxis).extent(static_cast<size_t>(axis)),
                  Nxv[axis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
        EXPECT_EQ(boundary.h_up(boundaryAxis).extent(static_cast<size_t>(axis)),
                  Nxv[axis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
      } else {
        EXPECT_EQ(boundary.up(boundaryAxis).extent(static_cast<size_t>(axis)),
                  boundaryWidth[boundaryAxis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
        EXPECT_EQ(boundary.h_up(boundaryAxis).extent(static_cast<size_t>(axis)),
                  boundaryWidth[boundaryAxis])
            << "Axis: " << bsl6d::axis_to_string(axis)
            << "Boundary: " << bsl6d::axis_to_string(boundaryAxis);
      }
    }
    ASSERT_LE(boundary.up(boundaryAxis).size(),
              boundary.shared_buffer_size("up"));
  }
}

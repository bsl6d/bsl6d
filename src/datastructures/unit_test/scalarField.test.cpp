/*****************************************************************/
/*
\brief Testing of datastructure containing a 3D scalar field

\author Mario Raeth
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   10.02.2022

*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <config.hpp>
#include <grid.hpp>
#include <mathNorms.hpp>

#include "../scalarField.hpp"

class DatastructuresScalarField : public ::testing::Test {
  public:
  bsl6d::GridDistribution<3, 3> gridDistribution{
      bsl6d::GridDistributionConfig{}};
  bsl6d::ScalarField s1{gridDistribution.create_xGridDistribution(),
                        "ScalarField1"};
  bsl6d::ScalarField s2{gridDistribution.create_xGridDistribution(),
                        "ScalarField2"};
  bsl6d::ScalarField s3{gridDistribution.create_xGridDistribution(),
                        "ScalarField3"};
};

/*****************************************************************/
/*

\brief Test initialization of scalar field

*/
/*****************************************************************/
TEST_F(DatastructuresScalarField, Init) {
  bsl6d::ScalarField scalarEmpty{};

  EXPECT_NO_THROW(scalarEmpty = s1);
}

TEST_F(DatastructuresScalarField, AssignemetObject) {
  bsl6d::ScalarField wrongLabel{gridDistribution.create_xGridDistribution(),
                                "3sdfhgshgfs"};
  EXPECT_THROW(s1 = wrongLabel, std::runtime_error);
  nlohmann::json configWrongGrid = R"({
  "gridDistribution": {
    "grid": {
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [22.0, 19.0, 20.0, 10.0, 9.0, 11.0],
      "xvMin": [ 0.0,  0.0,  0.0,-10.0,-9.0,-11.0],
      "NxvPoints": [4, 4, 4, 2, 2, 2],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
  })"_json;
  bsl6d::Grid<3, 3> wrongGrid{bsl6d::GridDistributionConfig{configWrongGrid}};
  bsl6d::GridDistribution<3, 3> wrongGridDistribution{
      wrongGrid, bsl6d::GridDistributionConfig{}};
  bsl6d::ScalarField wrongGridScalarField{
      wrongGridDistribution.create_xGridDistribution(), "ScalarField1"};
  EXPECT_THROW(s1 = wrongGridScalarField, std::runtime_error);

  bsl6d::ScalarField matchingScalarField{
      gridDistribution.create_xGridDistribution(), "ScalarField1"};
  EXPECT_NO_THROW(s1 = matchingScalarField);
}

TEST_F(DatastructuresScalarField, HaloRegions) {
  double comparisonValue1                                  = 1.0;
  double comparisonValue2                                  = 2.0;
  s1.host()(0, 0, 0)                                       = comparisonValue1;
  s1.host()(s1.mpi_local_grid()(bsl6d::AxisType::x1).up(),
            s1.mpi_local_grid()(bsl6d::AxisType::x2).up(),
            s1.mpi_local_grid()(bsl6d::AxisType::x3).up()) = comparisonValue2;
  Kokkos::deep_copy(s1.device(), s1.host());
  for (auto axis : bsl6d::xAxes()) {
    EXPECT_EQ(s1.device().begin(static_cast<size_t>(axis)), 0);
    EXPECT_EQ(s1.host().begin(static_cast<size_t>(axis)), 0);
    EXPECT_EQ(s1.device().end(static_cast<size_t>(axis)),
              s1.mpi_local_grid()(axis).N());
    EXPECT_EQ(s1.host().end(static_cast<size_t>(axis)),
              s1.mpi_local_grid()(axis).N());
    int haloWidth{static_cast<int>(axis) + 1};
    EXPECT_NO_THROW(s1.set_halo_width(axis, haloWidth));
    EXPECT_EQ(s1.device().begin(static_cast<size_t>(axis)), -haloWidth)
        << bsl6d::axis_to_string(axis);
    EXPECT_EQ(s1.host().begin(static_cast<size_t>(axis)), -haloWidth)
        << bsl6d::axis_to_string(axis);
    EXPECT_EQ(s1.device().end(static_cast<size_t>(axis)),
              s1.mpi_local_grid()(axis).N() + haloWidth)
        << bsl6d::axis_to_string(axis);
    EXPECT_EQ(s1.host().end(static_cast<size_t>(axis)),
              s1.mpi_local_grid()(axis).N() + haloWidth)
        << bsl6d::axis_to_string(axis);
    Kokkos::deep_copy(s1.host(), s1.device());
    EXPECT_DOUBLE_EQ(s1.host()(0, 0, 0), comparisonValue1)
        << bsl6d::axis_to_string(axis);
    EXPECT_DOUBLE_EQ(s1.host()(s1.mpi_local_grid()(bsl6d::AxisType::x1).up(),
                               s1.mpi_local_grid()(bsl6d::AxisType::x2).up(),
                               s1.mpi_local_grid()(bsl6d::AxisType::x3).up()),
                     comparisonValue2)
        << bsl6d::axis_to_string(axis);
    // halo region should not change if requested region is smaller than
    // existing region
    haloWidth = static_cast<size_t>(axis);

    EXPECT_NO_THROW(s1.set_halo_width(axis, haloWidth));
    EXPECT_LT(s1.device().begin(static_cast<size_t>(axis)), -haloWidth)
        << bsl6d::axis_to_string(axis);
    EXPECT_GT(s1.device().end(static_cast<size_t>(axis)),
              s1.mpi_local_grid()(axis).N() + haloWidth)
        << bsl6d::axis_to_string(axis);
  }
  bsl6d::AxisType axis = bsl6d::AxisType::x1;
  int haloWidth{static_cast<int>(axis) + 1};
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x1>().first.begin(0), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x1>().first.end(0), haloWidth);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x1>().second.begin(0), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x1>().second.end(0), haloWidth);
  axis      = bsl6d::AxisType::x2;
  haloWidth = static_cast<size_t>(axis) + 1;
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x2>().first.begin(1), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x2>().first.end(1), haloWidth);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x2>().second.begin(1), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x2>().second.end(1), haloWidth);
  axis      = bsl6d::AxisType::x3;
  haloWidth = static_cast<size_t>(axis) + 1;
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x3>().first.begin(2), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x3>().first.end(2), haloWidth);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x3>().second.begin(2), 0);
  EXPECT_EQ(s1.get_halo_views<bsl6d::AxisType::x3>().second.end(2), haloWidth);
}

TEST_F(DatastructuresScalarField, GetViewsOfComputeRegion) {
  for (auto axis : bsl6d::xAxes()) {
    int haloWidth{static_cast<int>(axis) + 1};
    EXPECT_NO_THROW(s1.set_halo_width(axis, haloWidth));
  }
  double comparisonValue1 = 1.0;
  double comparisonValue2 = 2.0;

  s1.host()(0, 0, 0)                                       = comparisonValue1;
  s1.host()(s1.mpi_local_grid()(bsl6d::AxisType::x1).up(),
            s1.mpi_local_grid()(bsl6d::AxisType::x2).up(),
            s1.mpi_local_grid()(bsl6d::AxisType::x3).up()) = comparisonValue2;
  Kokkos::deep_copy(s1.device(), s1.host());
  Kokkos::View<double***>::HostMirror h_contiguousComputeRegionView{
      s1.get_contiguous_host_view_of_compute_region()};
  EXPECT_DOUBLE_EQ(h_contiguousComputeRegionView(0, 0, 0), comparisonValue1);
  EXPECT_DOUBLE_EQ(h_contiguousComputeRegionView(
                       s1.mpi_local_grid()(bsl6d::AxisType::x1).up(),
                       s1.mpi_local_grid()(bsl6d::AxisType::x2).up(),
                       s1.mpi_local_grid()(bsl6d::AxisType::x3).up()),
                   comparisonValue2);
  EXPECT_EQ(h_contiguousComputeRegionView.extent(0),
            s1.mpi_local_grid()(bsl6d::AxisType::x1).N());
  EXPECT_EQ(h_contiguousComputeRegionView.extent(1),
            s1.mpi_local_grid()(bsl6d::AxisType::x2).N());
  EXPECT_EQ(h_contiguousComputeRegionView.extent(2),
            s1.mpi_local_grid()(bsl6d::AxisType::x3).N());
  EXPECT_EQ(h_contiguousComputeRegionView.span_is_contiguous(), true);
}

double LInf_err(bsl6d::ScalarField const& sf, double const& res) {
  double err{};
  Kokkos::parallel_reduce(
      "Test Multiplication", sf.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, double& lerr) {
        lerr = std::max(std::abs(sf(ix1, ix2, ix3) - res), lerr);
      },
      Kokkos::Max<double>(err));
  return err;
}

TEST_F(DatastructuresScalarField, FluxSurfaceAverage) {
  Kokkos::deep_copy(s1.device(), 1.0);
  Kokkos::View<double*> fluxSurfaceAverage{flux_surface_average(s1)};
  Kokkos::View<double*>::HostMirror h_fluxSurfaceAverage{
      Kokkos::create_mirror_view(fluxSurfaceAverage)};
  Kokkos::deep_copy(h_fluxSurfaceAverage, fluxSurfaceAverage);

  double yzAverage{1.0};
  for (auto axis : {bsl6d::AxisType::x2, bsl6d::AxisType::x3}) {
    yzAverage *= s1.gridDistribution().mpi_global_grid()(axis).N() *
                 s1.gridDistribution().mpi_global_grid()(axis).delta() /
                 s1.gridDistribution().mpi_global_grid()(axis).L();
  }
  for (size_t i = 0; i < h_fluxSurfaceAverage.extent(0); i++) {
    EXPECT_NEAR(h_fluxSurfaceAverage(i), yzAverage, 1e-14);
  }
}

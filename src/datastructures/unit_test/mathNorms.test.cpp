#include <gtest/gtest.h>

#include <distributionFunction.hpp>
#include <scalarField.hpp>

#include "../mathNorms.hpp"

class DatastructuresMathNorms : public ::testing::Test {
  public:
  bsl6d::DistributionFunction f1{bsl6d::GridDistributionConfig{},
                                 bsl6d::DistributionFunctionConfig{}};
  bsl6d::DistributionFunction f2{bsl6d::GridDistributionConfig{},
                                 bsl6d::DistributionFunctionConfig{}};
  bsl6d::ScalarField g1{f1.gridDistribution().create_xGridDistribution(), "g1"};
  bsl6d::ScalarField g2{f2.gridDistribution().create_xGridDistribution(), "g2"};
};

// Comparison to symbolic calculations would be better
// But it seems not symbolic math libraries for C++ are available
TEST_F(DatastructuresMathNorms, Norms) {
  Kokkos::deep_copy(f1.device(), 1.0);
  Kokkos::deep_copy(g1.device(), 1.0);
  EXPECT_EQ(bsl6d::L1_norm(f1),
            f1.device().size() *
                f1.gridDistribution().mpi_global_grid().cell_vol_xv());
  EXPECT_EQ(bsl6d::L1_norm(g1),
            g1.device().size() *
                g1.gridDistribution().mpi_global_grid().cell_vol_xv());

  Kokkos::deep_copy(f1.device(), 2.0);
  Kokkos::deep_copy(g1.device(), 2.0);
  EXPECT_EQ(
      bsl6d::L2_norm(f1),
      2 * std::sqrt(f1.device().size() *
                    f1.gridDistribution().mpi_global_grid().cell_vol_xv()));
  EXPECT_EQ(
      bsl6d::L2_norm(g1),
      2 * std::sqrt(g1.device().size() *
                    g1.gridDistribution().mpi_global_grid().cell_vol_xv()));

  Kokkos::deep_copy(f1.host(), 0.0);
  Kokkos::deep_copy(g1.host(), 0.0);
  f1.host()(0, 0, 0, 0, 0, 0) = 1.0;
  g1.host()(0, 0, 0)          = 1.0;
  Kokkos::deep_copy(f1.device(), f1.host());
  Kokkos::deep_copy(g1.device(), g1.host());
  EXPECT_EQ(bsl6d::LInf_norm(f1), 1.0);
  EXPECT_EQ(bsl6d::LInf_norm(g1), 1.0);
}

TEST_F(DatastructuresMathNorms, Errors) {
  Kokkos::deep_copy(f1.device(), 1.0);
  Kokkos::deep_copy(g1.device(), 1.0);
  Kokkos::deep_copy(f2.device(), 2.0);
  Kokkos::deep_copy(g2.device(), 2.0);
  EXPECT_EQ(bsl6d::L1_error(f1, f2),
            f1.device().size() *
                f1.gridDistribution().mpi_global_grid().cell_vol_xv());
  EXPECT_EQ(bsl6d::L1_error(g1, g2),
            g1.device().size() *
                g1.gridDistribution().mpi_global_grid().cell_vol_xv());

  Kokkos::deep_copy(f1.device(), 2.0);
  Kokkos::deep_copy(g1.device(), 2.0);
  Kokkos::deep_copy(f2.device(), 4.0);
  Kokkos::deep_copy(g2.device(), 4.0);
  EXPECT_EQ(
      bsl6d::L2_error(f1, f2),
      2 * std::sqrt(f1.device().size() *
                    f1.gridDistribution().mpi_global_grid().cell_vol_xv()));
  EXPECT_EQ(
      bsl6d::L2_error(g1, g2),
      2 * std::sqrt(g1.device().size() *
                    g1.gridDistribution().mpi_global_grid().cell_vol_xv()));

  Kokkos::deep_copy(f1.device(), 1.0);
  Kokkos::deep_copy(g1.device(), 1.0);
  Kokkos::deep_copy(f2.device(), 0.0);
  Kokkos::deep_copy(g2.device(), 0.0);
  EXPECT_EQ(bsl6d::LInf_error(f1, f2), 1.0);
  EXPECT_EQ(bsl6d::LInf_error(g1, g2), 1.0);
}
/*****************************************************************/
/*
\brief Testing of datastructure containing 6d distribution
       function.

\author Nils Schild
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/

#include <gtest/gtest.h>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <mpiTopology.hpp>
#include "../velocityMomements.hpp"

nlohmann::json configFileVelMom = R"({
  "gridDistribution": {
    "grid":{
      "axes": ["x1","x2","x3","v1","v2","v3"],
      "xvMax": [1.5, 2.0, 3.0, 2.0, 2.5, 3.0],
      "xvMin": [0.0, 0.0, 0.0,-2.0,-2.5,-3.0],
      "NxvPoints": [2, 4, 6, 91, 93, 95],
      "position": ["L", "L", "L", "N", "N", "N"],
      "rotateGyroFreq": false
    },
    "parallel": {}
  }
})"_json;

class DatastructuresVelMom : public ::testing::Test {
  public:
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{configFileVelMom}};
  bsl6d::CartTopology<6> topo{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topo};
  bsl6d::DistributionFunction f6d{gridDistribution,
                                  bsl6d::DistributionFunctionConfig{}};
  bsl6d::Impl::VelocityMoments velocityMoments{};

  DatastructuresVelMom() {
    Kokkos::deep_copy(f6d.device(), 1.0);
    auto f6d_partition = Kokkos::subview(
        f6d.device(), Kokkos::ALL(), Kokkos::ALL(), Kokkos::ALL(),
        std::pair<size_t, size_t>{0,
                                  f6d.grid()(bsl6d::AxisType::v1).N() / 2 + 1},
        std::pair<size_t, size_t>{0,
                                  f6d.grid()(bsl6d::AxisType::v2).N() / 2 + 1},
        std::pair<size_t, size_t>{0,
                                  f6d.grid()(bsl6d::AxisType::v3).N() / 2 + 1});

    Kokkos::deep_copy(f6d_partition, 2.0);
  };
};

TEST_F(DatastructuresVelMom, VelocityMomentV0) {
  velocityMoments = bsl6d::Impl::calculate_velocity_moments(f6d);
  Kokkos::deep_copy(velocityMoments.n.host(), velocityMoments.n.device());
  EXPECT_EQ(velocityMoments.n.grid(), grid.create_xSpace());

  EXPECT_NEAR(velocityMoments.n.host()(1, 2, 3), 139.9569842738205, 1e-7);
}
TEST_F(DatastructuresVelMom, VelocityMomentV2Energy) {
  velocityMoments = bsl6d::Impl::calculate_velocity_moments(f6d);
  Kokkos::deep_copy(velocityMoments.energyDensity.host(),
                    velocityMoments.energyDensity.device());
  EXPECT_EQ(velocityMoments.energyDensity.grid(), grid.create_xSpace());

  EXPECT_NEAR(velocityMoments.energyDensity.host()(1, 2, 3),
              916.4712567386071695 / 2, 0.10500548677612187);
}
TEST_F(DatastructuresVelMom, VeclocityMomentV1Momentum) {
  for (auto axis : bsl6d::vAxes()) {
    EXPECT_NEAR(f6d.grid()(axis)(f6d.grid()(axis).N() / 2), 0.0, 1.0e-9);
  }

  velocityMoments = bsl6d::Impl::calculate_velocity_moments(f6d);

  for (auto axis : bsl6d::xAxes()) {
    Kokkos::deep_copy(velocityMoments.particleFlux(axis).host(),
                      velocityMoments.particleFlux(axis).device());
    EXPECT_EQ(velocityMoments.particleFlux(axis).grid(), grid.create_xSpace());
  }

  std::map<bsl6d::AxisType, double> particleFlux{{bsl6d::AxisType::x1, -16.0},
                                                 {bsl6d::AxisType::x2, -20.0},
                                                 {bsl6d::AxisType::x3, -24.0}};

  for (auto axis : bsl6d::xAxes()) {
    EXPECT_NEAR(velocityMoments.particleFlux(axis).host()(1, 2, 3),
                particleFlux[axis], 1e-10);
  }
}
TEST_F(DatastructuresVelMom, VelocityMomentV3energyFlux) {
  for (auto axis : bsl6d::vAxes()) {
    EXPECT_NEAR(f6d.grid()(axis)(f6d.grid()(axis).N() / 2), 0.0, 1.0e-9);
  }

  velocityMoments = bsl6d::Impl::calculate_velocity_moments(f6d);

  for (auto axis : bsl6d::xAxes()) {
    Kokkos::deep_copy(velocityMoments.energyFlux(axis).host(),
                      velocityMoments.energyFlux(axis).device());
    EXPECT_EQ(velocityMoments.energyFlux(axis).grid(), grid.create_xSpace());
  }

  std::map<bsl6d::AxisType, double> momentV1Heat{
      {bsl6d::AxisType::x1, -114.93467338804933035 / 2},
      {bsl6d::AxisType::x2, -151.48480750244634428 / 2},
      {bsl6d::AxisType::x3, -193.23120959696538182 / 2}};
  for (auto axis : bsl6d::xAxes()) {
    EXPECT_NEAR(velocityMoments.energyFlux(axis).host()(1, 2, 3),
                momentV1Heat[axis], 1e-1);
  }
}
TEST_F(DatastructuresVelMom, VelocityMomentV2Stress) {
  velocityMoments = bsl6d::Impl::calculate_velocity_moments(f6d);

  for (auto axis1 : bsl6d::xAxes()) {
    for (auto axis2 : bsl6d::xAxes()) {
      Kokkos::deep_copy(velocityMoments.secondMoment(axis1, axis2).host(),
                        velocityMoments.secondMoment(axis1, axis2).device());
      EXPECT_EQ(velocityMoments.secondMoment(axis1, axis2).grid(),
                grid.create_xSpace());
    }
  }

  std::map<bsl6d::AxisType, std::map<bsl6d::AxisType, double>> momentsV2Stress;
  momentsV2Stress[bsl6d::AxisType::x1][bsl6d::AxisType::x1] =
      190.54218712288191384;
  momentsV2Stress[bsl6d::AxisType::x1][bsl6d::AxisType::x2] =
      19.999999999999994998;
  momentsV2Stress[bsl6d::AxisType::x1][bsl6d::AxisType::x3] =
      23.999999999999995974;
  momentsV2Stress[bsl6d::AxisType::x2][bsl6d::AxisType::x1] =
      19.999999999999994998;
  momentsV2Stress[bsl6d::AxisType::x2][bsl6d::AxisType::x2] =
      297.58781234844475247;
  momentsV2Stress[bsl6d::AxisType::x2][bsl6d::AxisType::x3] =
      29.999999999999993625;
  momentsV2Stress[bsl6d::AxisType::x3][bsl6d::AxisType::x1] =
      23.999999999999995974;
  momentsV2Stress[bsl6d::AxisType::x3][bsl6d::AxisType::x2] =
      29.999999999999993625;
  momentsV2Stress[bsl6d::AxisType::x3][bsl6d::AxisType::x3] =
      428.34125726728050319;

  for (auto axis1 : bsl6d::xAxes()) {
    for (auto axis2 : bsl6d::xAxes()) {
      EXPECT_NEAR(velocityMoments.secondMoment(axis1, axis2).host()(1, 2, 3),
                  momentsV2Stress[axis1][axis2], 1.0e-1);
    }
  }
}

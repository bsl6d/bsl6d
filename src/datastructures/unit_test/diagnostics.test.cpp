/*****************************************************************/
/*
\brief Testing of initialisation functions for distribution

\author Nils Schild, Mario Raeth
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021
*/
/*****************************************************************/

#include <filesystem>
#include <map>

#include <gtest/gtest.h>

#include <config.hpp>
#include <diagnostics.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <initializationFunctions.hpp>
#include <scalarField.hpp>
#include <time.hpp>
#include <vectorField.hpp>

class DatastructuresDiagnostic : public ::testing::Test {
  public:
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::CartTopology<6> topology{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topology};

  DatastructuresDiagnostic() = default;
};

// ToDo test if the output acutally works for the given configuration
TEST_F(DatastructuresDiagnostic, Setup) {
  std::filesystem::remove_all("./diagnostics/");
  nlohmann::json configFile = R"({
    "initializationDistributionFunction":{
      "initFunction": "gauss",
      "xv0": [0.1,0.2,0.3,0.15,0.16,0.17],
      "temperature": 1.0,
      "sigma2": [1.0,1.0,1.0]
    },
    "diagnostics": [
        {
          "diagnostic": "f6d",
          "iteration": 10
        },
        {
          "diagnostic": "labframeEnergyFlux",
          "iteration": 5
        },
        {
          "diagnostic": "labframeParticleFlux",
          "iteration": 5
        }
        ]
    })"_json;

  int argc{3};
  char *argv[3]       = {(char *)("/Full/Path/Of/Executable"),
                         (char *)("--output_file"), (char *)("diagnostics")};
  auto [simTime, f6d] = bsl6d::initialize(configFile);
  bsl6d::cmd::parse(argc, argv);

  bsl6d::Diagnostic diagnostic{bsl6d::DiagnosticConfig{configFile}, topology};

  EXPECT_NO_THROW(diagnostic(simTime, f6d, bsl6d::MaxwellFields{}));

  EXPECT_THROW(bsl6d::Diagnostic(bsl6d::DiagnosticConfig{configFile}, topology),
               std::runtime_error);

  EXPECT_TRUE(std::filesystem::remove_all("./diagnostics"));
}

TEST_F(DatastructuresDiagnostic, PhiMean) {
  nlohmann::json configFile = R"(
  {
  "simulationTime": {
    "dt": 0.1,
    "T": 1.0,
    "restart": false
  }
  })"_json;
  bsl6d::SimulationTime simTime{bsl6d::SimulationTimeConfig{configFile}};

  bsl6d::ScalarField phi{gridDistribution.create_xGridDistribution(), "phi"};

  bsl6d::Impl::Storage storage{Kokkos::View<double ****>{
      "phiMean", 10, grid(bsl6d::AxisType::x1).N(),
      grid(bsl6d::AxisType::x2).N(), grid(bsl6d::AxisType::x3).N()}};
  Kokkos::View<double ****>::HostMirror h_phiMean{
      Kokkos::create_mirror_view(storage.phiMean)};

  Kokkos::deep_copy(phi.device(), 1.0);

  bsl6d::Impl::gyroCollectMeanPhi(simTime, phi, storage);

  Kokkos::deep_copy(h_phiMean, storage.phiMean);
  EXPECT_EQ(h_phiMean(0, 0, 0, 0), 1.);
  EXPECT_EQ(h_phiMean(1, 0, 0, 0), 0.);
  bsl6d::ScalarField rho{gridDistribution.create_xGridDistribution(), "rho"};
  bsl6d::VectorField E{gridDistribution.create_xGridDistribution(), "E"};
  bsl6d::DistributionFunction f6d{gridDistribution,
                                  bsl6d::DistributionFunctionConfig{}};

  bsl6d::ScalarField phiMean{gridDistribution.create_xGridDistribution(),
                             "phiMean"};
  phiMean = bsl6d::Impl::DiagnosticPhiMean(f6d, rho, phi, E, storage);
  Kokkos::deep_copy(phiMean.host(), phiMean.device());

  EXPECT_DOUBLE_EQ(phiMean.host()(1, 2, 3), 0.1);

  Kokkos::deep_copy(storage.phiMean, 0.0);

  while (simTime.continue_advection()) {
    bsl6d::Impl::gyroCollectMeanPhi(simTime, phi, storage);
    phiMean = bsl6d::Impl::DiagnosticPhiMean(f6d, rho, phi, E, storage);
    Kokkos::deep_copy(phiMean.host(), phiMean.device());
    EXPECT_DOUBLE_EQ(phiMean.host()(1, 2, 3),
                     (simTime.current_step() + 1) * 0.1);
    simTime.advance_current_T_by_dt();
  }

  bsl6d::SimulationTime simTime2{bsl6d::SimulationTimeConfig{configFile}};

  while (simTime2.continue_advection()) {
    bsl6d::Impl::gyroCollectMeanPhi(simTime2, phi, storage);
    phiMean = bsl6d::Impl::DiagnosticPhiMean(f6d, rho, phi, E, storage);
    Kokkos::deep_copy(phiMean.host(), phiMean.device());
    EXPECT_DOUBLE_EQ(phiMean.host()(1, 2, 3), 1.0);
    simTime2.advance_current_T_by_dt();
  }
  std::filesystem::remove_all("diagnostics");
}

/*****************************************************************/
/*
\brief Testing of initialisation functions for distribution

\author Nils Schild, Mario Raeth
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021
*/
/*****************************************************************/

#include <gtest/gtest.h>

#include <config.hpp>
#include <diagnostics.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <initializationFunctions.hpp>
#include <io_init.hpp>

class DatastructuresInitializationFunctions : public ::testing::Test {
  public:
  bsl6d::Grid<3, 3> grid{bsl6d::GridDistributionConfig{}};
  bsl6d::CartTopology<6> topology{bsl6d::GridDistributionConfig{}};
  bsl6d::GridDistribution<3, 3> gridDistribution{grid, topology};
  bsl6d::DistributionFunction f6d{gridDistribution,
                                  bsl6d::DistributionFunctionConfig{}};

  DatastructuresInitializationFunctions() = default;
};

/*****************************************************************/
template <typename F>
double Linf_error(bsl6d::DistributionFunction const &f6d, F f) {
  double err{};
  bsl6d::Grid<3, 3> grid{f6d.mpi_local_grid()};
  Kokkos::deep_copy(f6d.host(), f6d.device());
  Kokkos::parallel_reduce(
      "Linf_error",
      f6d.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, double &lerr) {
        double val{
            f(grid(bsl6d::AxisType::x1)(ix1), grid(bsl6d::AxisType::x2)(ix2),
              grid(bsl6d::AxisType::x3)(ix3), grid(bsl6d::AxisType::v1)(iv1),
              grid(bsl6d::AxisType::v2)(iv2), grid(bsl6d::AxisType::v3)(iv3))};
        lerr = std::max(
            std::abs(f6d.host()(ix1, ix2, ix3, iv1, iv2, iv3) - val), lerr);
      },
      Kokkos::Max<double>(err));

  return err;
}

double average(Kokkos::View<double ***> rho) {
  double avg{};
  Kokkos::parallel_reduce(
      "Test Random X space average",
      Kokkos::MDRangePolicy<Kokkos::Rank<3>>{
          {0, 0, 0}, {rho.extent(0), rho.extent(1), rho.extent(2)}},
      KOKKOS_LAMBDA(size_t i0, size_t i1, size_t i2, double &_avg) {
        _avg += rho(i0, i1, i2);
      },
      avg);

  avg = avg / (rho.extent(0) * rho.extent(1) * rho.extent(2));
  return avg;
}

double varianz(Kokkos::View<double ***> rho) {
  double avg = average(rho);
  double var{};
  Kokkos::parallel_reduce(
      "Test Random Xspace variation",
      Kokkos::MDRangePolicy<Kokkos::Rank<3>>{
          {0, 0, 0}, {rho.extent(0), rho.extent(1), rho.extent(2)}},
      KOKKOS_LAMBDA(size_t i0, size_t i1, size_t i2, double &_var) {
        _var += (rho(i0, i1, i2) - avg) * (rho(i0, i1, i2) - avg);
      },
      var);

  var = var / (rho.extent(0) * rho.extent(1) * rho.extent(2));
  return var;
}

TEST_F(DatastructuresInitializationFunctions, RandomNoiseDensityPerturbation) {
  nlohmann::json configFile =
      R"({
    "initializationDistributionFunction":{
      "initFunction": "rand_dens_perturbation",
      "seed": 1,
      "alpha": 0.1,
      "temperature": 1.0
    }
  })"_json;
  double alpha = configFile["initializationDistributionFunction"]["alpha"];

  EXPECT_EQ(
      true,
      bsl6d::Impl::available_init_keyword(
          configFile["initializationDistributionFunction"]["initFunction"]));
  auto [simTime, f6d] = bsl6d::initialize(configFile);
  bsl6d::ScalarField rho{f6d.calculate_charge_density("rho")};

  EXPECT_NEAR(average(rho.device().view()), 1.0, .05);
  EXPECT_NEAR(varianz(rho.device().view()), 1. / 3. * (alpha * alpha), 0.08);
}

double average_gauss(bsl6d::Grid<3, 3> grid, double mu, double invSigma2) {
  double avg{};
  Kokkos::parallel_reduce(
      "Test", grid(bsl6d::AxisType::v3).N(),
      KOKKOS_LAMBDA(size_t i5, double &_avg) {
        _avg +=
            bsl6d::Impl::gauss(grid(bsl6d::AxisType::v3)(i5), mu, invSigma2) *
            grid(bsl6d::AxisType::v3)(i5) * grid(bsl6d::AxisType::v3).delta();
      },
      avg);
  return avg;
}

double varianz_gauss(bsl6d::Grid<3, 3> grid, double mu, double invSigma2) {
  double var{};
  double avg{average_gauss(grid, mu, invSigma2)};
  Kokkos::parallel_reduce(
      "Test", grid(bsl6d::AxisType::v3).N(),
      KOKKOS_LAMBDA(size_t i5, double &_var) {
        _var +=
            (grid(bsl6d::AxisType::v3)(i5) - avg) *
            (grid(bsl6d::AxisType::v3)(i5) - avg) *
            bsl6d::Impl::gauss(grid(bsl6d::AxisType::v3)(i5), mu, invSigma2) *
            grid(bsl6d::AxisType::v3).delta();
      },
      var);
  return var;
}

double norm_gauss(bsl6d::Grid<3, 3> grid, double mu, double invSigma2) {
  double norm{};
  Kokkos::parallel_reduce(
      "Test", grid(bsl6d::AxisType::v3).N(),
      KOKKOS_LAMBDA(size_t i5, double &_norm) {
        _norm +=
            bsl6d::Impl::gauss(grid(bsl6d::AxisType::v3)(i5), mu, invSigma2) *
            grid(bsl6d::AxisType::v3).delta();
      },
      norm);
  return norm;
}

TEST_F(DatastructuresInitializationFunctions, Gauss1D) {
  double mu{.33333333};
  double sigma2{0.2};
  EXPECT_NEAR(average_gauss(grid, mu, sigma2), mu, 0.0001);
  EXPECT_NEAR(varianz_gauss(grid, mu, sigma2), sigma2, 0.0001);
  EXPECT_NEAR(norm_gauss(grid, mu, sigma2), 1., 0.0001);
}

TEST_F(DatastructuresInitializationFunctions, PositivDefinitness) {
  Kokkos::deep_copy(f6d.device(), 1.0);
  EXPECT_TRUE(bsl6d::Impl::is_positive_definite(f6d));
  f6d.host()(0, 0, 5, 0, 0, 0) = -1.0;
  Kokkos::deep_copy(f6d.device(), f6d.host());
  EXPECT_FALSE(bsl6d::Impl::is_positive_definite(f6d));
  f6d.host()(0, 0, 5, 0, 0, 0) = 1.0;
  f6d.host()(0, 0, 0, 1, 0, 0) = 0.0;
  Kokkos::deep_copy(f6d.device(), f6d.host());
  EXPECT_FALSE(bsl6d::Impl::is_positive_definite(f6d));
}

TEST_F(DatastructuresInitializationFunctions, DistributionFunctionNotNan) {
  Kokkos::deep_copy(f6d.device(), 1.0);
  EXPECT_FALSE(bsl6d::Impl::has_nan_values(f6d));
  f6d.host()(0, 0, 5, 0, 0, 0) = std::numeric_limits<double>::quiet_NaN();
  Kokkos::deep_copy(f6d.device(), f6d.host());
  EXPECT_TRUE(bsl6d::Impl::has_nan_values(f6d));
}

TEST(DatastructuresInitializationFunctionsRestart, RunRestart) {
  std::filesystem::remove_all("./bsl6dDiagnostics/");
  nlohmann::json configFile = R"({
      "distributionFunction": {
        "q": 2.0,
        "m": 3.0
      },
      "simulationTime": {
        "dt": 0.1,
        "T": 1.5,
      "restart": false
      },
      "initializationDistributionFunction":{
          "initFunction": "planewave_dens_perturbation",
          "xv0": [0.1,0.2,0.3,0.4,0.5,0.6],
          "temperature": 1.0,
          "k": [1.0,2.0,3.0],
          "sigma2": [1.0,1.0,1.0],
          "polynomOrder":  [2, 3, 1, 0, 2, 1]
      },
      "diagnostics": [
        {   
          "diagnostic": "f6d",
          "iteration": 2
        }
      ]})"_json;

  auto [simTime, f6d] = bsl6d::initialize(configFile);
  bsl6d::Diagnostic diagnostic{bsl6d::DiagnosticConfig{bsl6d::config_file()},
                               f6d.gridDistribution().cart_topology()};
  bsl6d::MaxwellFields maxwellFields{};

  while (simTime.continue_advection()) {
    diagnostic(simTime, f6d, maxwellFields);
    simTime.advance_current_T_by_dt();
  };

  int argc{3};
  char *argv[3] = {(char *)("/Full/Path/Of/Executable"), (char *)("--restart"),
                   (char *)("bsl6dDiagnostics/0000000010_diagnostics.h5")};
  bsl6d::cmd::parse(argc, argv);

  std::pair<bsl6d::SimulationTime, bsl6d::DistributionFunction> restartData{};
  ASSERT_NO_THROW(restartData = bsl6d::initialize());
  diagnostic = bsl6d::Diagnostic{bsl6d::DiagnosticConfig{configFile},
                                 f6d.gridDistribution().cart_topology()};

  EXPECT_DOUBLE_EQ(restartData.first.current_T(), 1.0);
  EXPECT_EQ(restartData.first.final_T(), 2.5);
  while (restartData.first.continue_advection()) {
    restartData.first.advance_current_T_by_dt();
    std::filesystem::path deletedFile{
        bsl6d::cmd::diagnostic_directory() + "00000000" +
        std::to_string(restartData.first.current_step()) +
        bsl6d::Diagnostic::s_outputSuffix};
    EXPECT_FALSE(std::filesystem::exists(deletedFile));
    EXPECT_NO_THROW(
        diagnostic(restartData.first, restartData.second, maxwellFields));
  };

  std::filesystem::remove_all("./bsl6dDiagnostics/");
}

/*****************************************************************/
/*
\brief Base structure to define 6d distribution function

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/
#pragma once

#include <distributionFunction.hpp>
#include <scalarField.hpp>
#include <vectorField.hpp>

#include <Kokkos_Core.hpp>

namespace bsl6d {

namespace Impl {
// clang-format off
/**
 * @brief Implementes a Riemann-Integral for the velocity moments defined below
 *
 * @details This function calculates the following moments of \f$ f(x,v) \f$:
 * 
 * - n(x): particle density \f$ n = \int f \mathrm d^3 v \f$
 * - particleFlux(x): particle flux \f$ J_i = \int v_i f \mathrm d^3v \f$
 * - energyDensity(x): energy density \f$ \epsilon = \sum_{i=1}^{3}\int \frac{v_i^2}{2} f \mathrm d^3v \f$ 
 * - secondMoment(x): second moment \f$ \Pi_{ij} = \int v_i v_j f \mathrm d^3v \f$ 
 * - energyFlux(x): energy flux \f$ Q_j = \sum_{i=1}^{3}\int \frac{v_i^2}{2} v_j f \mathrm d^3v \f$
 * - reducedFourthMoment(x): reduced fourth moment \f$ \Pi_{ij}^{*} =\frac{1}{2} \sum_{k,l} \int \delta_{k,l} v_i v_j v_k v_l f \mathrm d^3v \f$ 
 * 
 * \f$\delta_{k,l}\f$ is the Kronecker-Delta.
 */
// clang-format on
VelocityMoments calculate_velocity_moments(DistributionFunction const& f6d);
}  // namespace Impl

}  // namespace bsl6d

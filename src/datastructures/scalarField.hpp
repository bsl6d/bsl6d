/*****************************************************************/
/*
\brief Base structure to define 3D scalar field

\author Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  mario.raeth@ipp.mpg.de
\date   10.02.2022

*/
/*****************************************************************/
#pragma once

#include <boundaryHypercube.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>

#include <Kokkos_Core.hpp>
#include <Kokkos_OffsetView.hpp>

namespace bsl6d {

namespace Impl {

inline auto create_compute_range_subview(
    Kokkos::Experimental::OffsetView<double ***> const &view,
    Grid<3, 0> const &grid) {
  auto computeRegion = Kokkos::Experimental::subview(
      view, Kokkos::make_pair<int, int>(0, grid(AxisType::x1).N()),
      Kokkos::make_pair<int, int>(0, grid(AxisType::x2).N()),
      Kokkos::make_pair<int, int>(0, grid(AxisType::x3).N()));
  return computeRegion;
}
}  // namespace Impl

class ScalarField {
  public:
  ScalarField() = default;
  ScalarField(GridDistribution<3, 0> const &gridDistribution,
              std::string const &label);

  ScalarField(ScalarField const &field) = default;
  ScalarField(ScalarField &&field)      = default;
  ScalarField &operator=(ScalarField const &field);
  ScalarField &operator=(ScalarField &&field);

  Kokkos::Experimental::OffsetView<double ***> device() const {
    return values;
  };
  Kokkos::Experimental::OffsetView<double ***>::HostMirror host() const {
    return h_values;
  };

  void set_halo_width(AxisType axis, int haloWidth);

  auto get_subview_of_compute_region() const {
    auto computeRegionView =
        Impl::create_compute_range_subview(device(), grid());
    return computeRegionView;
  };
  auto get_host_subview_of_compute_region() const {
    auto h_computeRegionView = Kokkos::Experimental::subview(
        h_values, Kokkos::make_pair<int, int>(0, grid()(AxisType::x1).N()),
        Kokkos::make_pair<int, int>(0, grid()(AxisType::x2).N()),
        Kokkos::make_pair<int, int>(0, grid()(AxisType::x3).N()));
    return h_computeRegionView;
  };

  Kokkos::View<double ***> get_contiguous_view_of_compute_region() const {
    auto computeRegionView = get_subview_of_compute_region();
    Kokkos::View<double ***> contiguousComputeRegionView{
        create_grid_compatible_view(grid(), values.label())};
    Kokkos::deep_copy(contiguousComputeRegionView, computeRegionView);
    return contiguousComputeRegionView;
  };
  Kokkos::View<double ***>::HostMirror
      get_contiguous_host_view_of_compute_region() const {
    Kokkos::View<double ***> contiguousComputeRegionView =
        get_contiguous_view_of_compute_region();
    Kokkos::View<double ***>::HostMirror h_continuousComputeRegionView{
        Kokkos::create_mirror_view(contiguousComputeRegionView)};
    Kokkos::deep_copy(h_continuousComputeRegionView,
                      contiguousComputeRegionView);
    return h_continuousComputeRegionView;
  };
  template <AxisType axis>
  auto get_halo_views() const {
    if constexpr (axis == AxisType::x1) {
      return std::make_pair(
          Kokkos::Experimental::subview(
              values, Kokkos::make_pair<int, int>(-m_haloWidth.at(axis), 0),
              Kokkos::ALL(), Kokkos::ALL()),
          Kokkos::Experimental::subview(
              values,
              Kokkos::make_pair<int, int>(
                  grid()(axis).N(), grid()(axis).N() + m_haloWidth.at(axis)),
              Kokkos::ALL(), Kokkos::ALL()));
    } else if constexpr (axis == AxisType::x2) {
      return std::make_pair(
          Kokkos::Experimental::subview(
              values, Kokkos::ALL(),
              Kokkos::make_pair<int, int>(-m_haloWidth.at(axis), 0),
              Kokkos::ALL()),
          Kokkos::Experimental::subview(
              values, Kokkos::ALL(),
              Kokkos::make_pair<int, int>(
                  grid()(axis).N(), grid()(axis).N() + m_haloWidth.at(axis)),
              Kokkos::ALL()));
    } else if constexpr (axis == AxisType::x3) {
      return std::make_pair(
          Kokkos::Experimental::subview(
              values, Kokkos::ALL(), Kokkos::ALL(),
              Kokkos::make_pair<int, int>(-m_haloWidth.at(axis), 0)),
          Kokkos::Experimental::subview(
              values, Kokkos::ALL(), Kokkos::ALL(),
              Kokkos::make_pair<int, int>(
                  grid()(axis).N(), grid()(axis).N() + m_haloWidth.at(axis))));
    }
  }

  // To be replaced by mpi_local_grid();
  KOKKOS_INLINE_FUNCTION
  Grid<3, 0> grid() const { return m_gridDistribution.my_local_grid(); };
  KOKKOS_INLINE_FUNCTION
  Grid<3, 0> mpi_local_grid() const {
    return m_gridDistribution.my_local_grid();
  };
  KOKKOS_INLINE_FUNCTION
  GridDistribution<3, 0> gridDistribution() const {
    return m_gridDistribution;
  };

  KOKKOS_INLINE_FUNCTION
  double &operator()(int const i0, int const i1, int const i2) const {
    return values(i0, i1, i2);
  };

  private:
  GridDistribution<3, 0> m_gridDistribution{};
  std::map<AxisType, int> m_haloWidth{{AxisType::x1, 0},
                                      {AxisType::x2, 0},
                                      {AxisType::x3, 0}};
  Kokkos::Experimental::OffsetView<double ***> values{};
  Kokkos::Experimental::OffsetView<double ***>::HostMirror h_values{};
};

// clang-format off
/**
  * @brief Calculate the flux-surface (y-z-plane) average of a given scalar field
  * 
  * @details Implicit assumtion: The magnetic field is given by \f$\textbf{B}_0 = B_0 \hat{e}_z\f$ 
  *          and the flux surface is spanned by \f$\text{SPAN}\{\hat{e}_y¸\hat{e}_z\}\f$.
  *          Flux surface average is defined by:
  *          @f[
  *          <\cdot>_F = \dfrac{1}{L_y L_z} \int f(x,y,z) \mathrm{d}y \mathrm{dz}
  *          @f]
  */
// clang-format on
Kokkos::View<double *> flux_surface_average(ScalarField const &);

namespace Impl {
template <typename AnalyticalExpression>
void fill(ScalarField const &scalarField,
          AnalyticalExpression const &analyticalExpression) {
  Kokkos::parallel_for(
      "ScalarFieldFiller", scalarField.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        double x1{scalarField.mpi_local_grid()(AxisType::x1)(ix1)};
        double x2{scalarField.mpi_local_grid()(AxisType::x2)(ix2)};
        double x3{scalarField.mpi_local_grid()(AxisType::x3)(ix3)};
        scalarField(ix1, ix2, ix3) = analyticalExpression(x1, x2, x3);
      });
}
}  // namespace Impl

}  // namespace bsl6d

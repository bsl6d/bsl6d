#pragma once

#include <array>

#include <Kokkos_Core.hpp>

#include <scalarField.hpp>
#include <vectorField.hpp>

namespace bsl6d {
/**
 * @brief Scalar product \f$\sum_{j=1}^3 a_j b_j\f$
 */
KOKKOS_INLINE_FUNCTION double operator*(Vector const &a, Vector const &b) {
  double result{};
  for (size_t i = 0; i < 3; i++) { result += a(i) * b(i); }
  return result;
}

/**
 * @brief Matrix Vector Multiplication \f$a_i = \sum_{j=1}^3 M_{i,j} b_j\f$
 */
KOKKOS_INLINE_FUNCTION
Vector operator*(const Matrix &matrix, const Vector &vector) {
  Vector result{};
  for (size_t i = 0; i < 3; i++) {
    for (size_t j = 0; j < 3; j++) { result(i) += matrix(i, j) * vector(j); }
  }
  return result;
}

/**
 * @brief Matrix Matrix Multiplication \f$C_{i,j} = \sum_{j=1}^3 A_{i,j}
 * B_{j,k}\f$
 */
KOKKOS_INLINE_FUNCTION
Matrix operator*(const Matrix &A, const Matrix &B) {
  Matrix result{};
  for (size_t i = 0; i < 3; i++)
    for (size_t j = 0; j < 3; j++)
      for (size_t k = 0; k < 3; k++) { result(i, j) += A(i, k) * B(k, j); };

  return result;
}

/**
 * @brief Pointwise adding a ScalarField to another ScalarField \f$f(\textbf{x})
 * = f(\textbf{x})+g(\textbf{x})\f$
 */
ScalarField operator+=(ScalarField &, ScalarField const &);

/**
 * @brief Pointwise multiplication of a scalar with a ScalarField
 * \f$f(\textbf{x}) = ag(\textbf{x})\f$
 */
ScalarField operator*(const ScalarField &, const double &);
/**
 * @brief Commutative of @ref operator*(const ScalarField &, const double &)
 * "operator*"
 */
ScalarField operator*(const double &, const ScalarField &);

/**
 * @brief Pointwise multiplication of two ScalarField \f$h(\textbf{x}) =
 * f(\textbf{x})g(\textbf{x})\f$
 */
ScalarField operator*(const ScalarField &, const ScalarField &);

/**
 * @brief Pointwise multiplication of a scalar with a VectorField
 * \f$\textbf{v}(\textbf{x}) = a \textbf{u}(\textbf{x})\f$
 */
VectorField operator*(const VectorField &, const double &);
/**
 * @brief Commutative of @ref operator*(const VectorField &, const double &)
 * "operator*"
 */
VectorField operator*(const double &, const VectorField &);

/**
 * @brief Pointwise multiplication of a ScalarField with a VectorField
 * \f$\textbf{v}(\textbf{x}) = \textbf{u}(\textbf{x}) g(\textbf{x})\f$
 */
VectorField operator*(const VectorField &, const ScalarField &);
/**
 * @brief Commutative of @ref operator*(const VectorField &, const ScalarField
 * &) "operator*"
 */
VectorField operator*(const ScalarField &, const VectorField &);

/**
 * @brief Pointwise scalarproduct of two VectorField \f$f(\textbf{x}) =
 * \textbf{u}(\textbf{x})\cdot\textbf{v}(\textbf{x}) = \sum_{i=x_1,x_2,x_3}
 * u_i(\textbf{x})v_i(\textbf{x})\f$
 */
ScalarField operator*(const VectorField &, const VectorField &);

/**
 * @brief Pointwise multiplication of a Matrix with a VectorField
 * \f$\textbf{v}(\textbf{x}) = \textbf{M}\cdot \textbf{u}(\textbf{x}) =
 * \sum_{j=x_1,x_2,x_3} M_{i,j} u_j(\textbf{x})\f$
 */
VectorField operator*(const Matrix &, const VectorField &);

/**
 * @brief Pointwise multiplication of a Matrix with a MatrixField
 * \f$\textbf{A}(\textbf{x}) = \textbf{M}\textbf{B}(\textbf{x}) =
 * \sum_{j=x_1,x_2,x_3} M_{i,j} B_{j,k}(\textbf{x})\f$
 */
MatrixField operator*(const Matrix &, const MatrixField &);
/**
 * @brief Pointwise multiplication of a Matrix with a MatrixField
 * \f$\textbf{A}(\textbf{x}) = \textbf{B}(\textbf{x})\textbf{M} =
 * \sum_{j=x_1,x_2,x_3} B_{i,j}(\textbf{x})M_{j,k} \f$
 */
MatrixField operator*(const MatrixField &, const Matrix &);

}  // namespace bsl6d
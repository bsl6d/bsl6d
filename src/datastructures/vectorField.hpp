#pragma once

#include <Kokkos_Core.hpp>

#include <scalarField.hpp>

namespace bsl6d {

struct Vector {
  using VectorType = std::array<double, 3>;
  VectorType data{};
  KOKKOS_INLINE_FUNCTION
  VectorType::reference operator()(VectorType::size_type pos) {
    return data[pos];
  }
  KOKKOS_INLINE_FUNCTION
  VectorType::const_reference operator()(VectorType::size_type pos) const {
    return data[pos];
  }
  friend bool operator==(Vector const& a, Vector const& b) {
    bool equal{true};
    for (size_t i = 0; i < a.data.size(); i++) {
      if (a(i) != b(i)) equal = false;
    }
    return equal;
  };
  friend std::ostream& operator<<(std::ostream& output, const Vector& v) {
    for (size_t i = 0; i < v.data.size(); i++) { output << v(i) << " "; }
    output << "\n";
    return output;
  }
};
struct Matrix {
  using MatrixType = std::array<std::array<double, 3>, 3>;
  MatrixType data{};
  KOKKOS_INLINE_FUNCTION
  MatrixType::value_type::reference operator()(MatrixType::size_type i,
                                               MatrixType::size_type j) {
    return data[i][j];
  }
  KOKKOS_INLINE_FUNCTION
  MatrixType::value_type::const_reference operator()(
      MatrixType::size_type i,
      MatrixType::size_type j) const {
    return data[i][j];
  }
  friend bool operator==(Matrix const& a, Matrix const& b) {
    bool equal{true};
    for (size_t i = 0; i < a.data.size(); i++)
      for (size_t j = 0; j < a.data[0].size(); j++) {
        if (a(i, j) != b(i, j)) equal = false;
      }
    return equal;
  };
  friend std::ostream& operator<<(std::ostream& output, const Matrix& m) {
    for (size_t i = 0; i < m.data.size(); i++) {
      for (size_t j = 0; j < m.data[0].size(); j++) {
        output << m(i, j) << " ";
      }
      output << "\n";
    }
    return output;
  }
};

class VectorField {
  public:
  VectorField() = default;
  VectorField(GridDistribution<3, 0> const& gridDistribution,
              std::string const& label);

  KOKKOS_INLINE_FUNCTION
  ScalarField const& operator()(AxisType axis) const {
    return m_field[static_cast<size_t>(axis)];
  }
  KOKKOS_INLINE_FUNCTION
  Vector const operator()(size_t ix1, size_t ix2, size_t ix3) const {
    return Vector{m_field[0](ix1, ix2, ix3), m_field[1](ix1, ix2, ix3),
                  m_field[2](ix1, ix2, ix3)};
  }
  void replace_scalarField(AxisType axis, ScalarField const& scalarField);
  std::string label() const { return m_label.label(); }

  private:
  std::array<ScalarField, 3> m_field{};
  // Use only the label of the View
  // If the string is used a warning is given due to the
  // destructor which is not a host device function
  Kokkos::View<char*> m_label{};
};

class MatrixField {
  public:
  MatrixField() = default;
  MatrixField(GridDistribution<3, 0> const& gridDistribution,
              std::string const& label);

  KOKKOS_INLINE_FUNCTION
  ScalarField const& operator()(AxisType axisRow, AxisType axisColumn) const {
    return m_field[static_cast<size_t>(axisRow)]
                  [static_cast<size_t>(axisColumn)];
  };
  KOKKOS_INLINE_FUNCTION
  Matrix const operator()(size_t ix1, size_t ix2, size_t ix3) const {
    return Matrix{
        std::array{m_field[0][0](ix1, ix2, ix3), m_field[0][1](ix1, ix2, ix3),
                   m_field[0][2](ix1, ix2, ix3)},
        std::array{m_field[1][0](ix1, ix2, ix3), m_field[1][1](ix1, ix2, ix3),
                   m_field[1][2](ix1, ix2, ix3)},
        std::array{m_field[2][0](ix1, ix2, ix3), m_field[2][1](ix1, ix2, ix3),
                   m_field[2][2](ix1, ix2, ix3)}};
  }
  void replace_scalarField(AxisType axisRow,
                           AxisType axisColumn,
                           ScalarField const& scalarField);
  std::string label() const { return m_label.label(); }

  private:
  std::array<std::array<ScalarField, 3>, 3> m_field{};
  // Use only the label of the View
  // If the string is used a warning is given due to the
  // destructor which is not a host device function
  Kokkos::View<char*> m_label{};
};

} /* namespace bsl6d */

/*****************************************************************/
/*

\brief Boundary for specific dimension of hypercube

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   02.11.2021

*/
/*****************************************************************/
#include <grid.hpp>

#include <boundaryHypercube.hpp>

namespace bsl6d {

BoundaryHypercube::BoundaryHypercube(
    std::map<bsl6d::AxisType, size_t> Nxv,
    std::map<bsl6d::AxisType, size_t> boundaryWidth) {
  size_t maxMemSize{0};
  for (auto boundaryAxis : bsl6d::xvAxes()) {
    size_t dimMemSize{1};
    for (auto axis : bsl6d::xvAxes()) {
      if (axis != boundaryAxis) {
        dimMemSize *= Nxv.at(axis);
      } else {
        dimMemSize *= boundaryWidth.at(boundaryAxis);
      }
    }
    maxMemSize = std::max(dimMemSize, maxMemSize);
  }

  m_baseMem["low"]   = Kokkos::View<double*>("BoundaryBaseMem", maxMemSize);
  m_baseMem["up"]    = Kokkos::View<double*>("BoundaryBaseMem", maxMemSize);
  m_h_baseMem["low"] = Kokkos::create_mirror_view(m_baseMem["low"]);
  m_h_baseMem["up"]  = Kokkos::create_mirror_view(m_baseMem["up"]);

  for (auto boundaryAxis : bsl6d::xvAxes()) {
    size_t boundaryAxisIdx{static_cast<size_t>(boundaryAxis)};
    std::array<size_t, 6> Nxv_tmp{1, 1, 1, 1, 1, 1};
    for (auto axis : bsl6d::xvAxes())
      Nxv_tmp[static_cast<size_t>(axis)] = Nxv.at(axis);
    Nxv_tmp[boundaryAxisIdx] = boundaryWidth.at(boundaryAxis);
    m_low[boundaryAxisIdx]   = BoundaryViewType{m_baseMem["low"].data(),
                                              Nxv_tmp[0],
                                              Nxv_tmp[1],
                                              Nxv_tmp[2],
                                              Nxv_tmp[3],
                                              Nxv_tmp[4],
                                              Nxv_tmp[5]};
    m_h_low[boundaryAxisIdx] =
        BoundaryViewType::HostMirror{m_h_baseMem["low"].data(),
                                     Nxv_tmp[0],
                                     Nxv_tmp[1],
                                     Nxv_tmp[2],
                                     Nxv_tmp[3],
                                     Nxv_tmp[4],
                                     Nxv_tmp[5]};
    m_up[boundaryAxisIdx] = BoundaryViewType{m_baseMem["up"].data(),
                                             Nxv_tmp[0],
                                             Nxv_tmp[1],
                                             Nxv_tmp[2],
                                             Nxv_tmp[3],
                                             Nxv_tmp[4],
                                             Nxv_tmp[5]};
    m_h_up[boundaryAxisIdx] =
        BoundaryViewType::HostMirror{m_h_baseMem["up"].data(),
                                     Nxv_tmp[0],
                                     Nxv_tmp[1],
                                     Nxv_tmp[2],
                                     Nxv_tmp[3],
                                     Nxv_tmp[4],
                                     Nxv_tmp[5]};
  }
};

} /* namespace bsl6d */

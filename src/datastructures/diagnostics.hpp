/*****************************************************************/
/*
\brief Diagnostics for distrbution function

\author Nils Schild; Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021

*/
/*****************************************************************/
#pragma once

#include <array>
#include <filesystem>
#include <functional>
#include <iostream>
#include <list>

#include <Kokkos_Core.hpp>

#include <baseFieldSolver.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <gridDistribution.hpp>
#include <io_init.hpp>
#include <openPMDInterface.hpp>
#include <scalarField.hpp>
#include <time.hpp>
#include <vectorField.hpp>

namespace bsl6d {

enum class MPIReduce { Call, Omit };
double L2Norm(ScalarField const &field,
              MPIReduce callMPIReduce = MPIReduce::Call);
std::array<double, 3> L2Norm(VectorField const &field);

namespace Impl {

/*ToDo refacture or depricate the diagnostics of this section*/
/*Start Section*/
struct Storage {
  Kokkos::View<double ****> phiMean;
};
ScalarField DiagnosticPhiMean(DistributionFunction f6d,
                              ScalarField rho,
                              ScalarField phi,
                              VectorField E,
                              Storage storage);

void gyroCollectMeanPhi(SimulationTime simTime,
                        ScalarField phi,
                        Storage storage);
/*Stop Section*/
size_t get_iteration_from_restart_file(std::filesystem::path restartFile);

} /*namespace Impl*/

class Diagnostic {
  public:
  Diagnostic() = default;
  Diagnostic(DiagnosticConfig const &conf, CartTopology<6> const &);
  void operator()(SimulationTime const &simTime,
                  DistributionFunction &f6d,
                  MaxwellFields const &maxwellFields);

  static std::filesystem::path series_name() {
    return "%0" + std::to_string(Diagnostic::s_outputIterationDigits) + "T" +
           Diagnostic::s_outputSuffix;
  }
  inline static const std::string s_outputSuffix{"_diagnostics.h5"};
  static constexpr size_t s_outputIterationDigits{10};

  using diagnostic_function = std::function<void(SimulationTime const &,
                                                 DistributionFunction &,
                                                 MaxwellFields const &,
                                                 openPMD::Series const &)>;

  private:
  openPMD::Series m_series{};
  std::list<DiagnosticConfig::ConfigDiagnosticOutput> config{};
  // ToDo Use the keys in this map to check whether provided characteristics are
  //  allowed as input or not to remove duplicated responsibility
  static const std::map<std::string, diagnostic_function> s_diagnostics;
};

} /*namespace bsl6d*/


#include <cmath>
#include <random>
#include <string>

#include <advector.hpp>
#include <backgroundMaxwellFieldSolver.hpp>
#include <config.hpp>
#include <diagnostics.hpp>
#include <initializationFunctions.hpp>
#include <io_init.hpp>
#include <lagrangeBuilder.hpp>
#include <openPMDInterface.hpp>
#include <poisson.hpp>
#include <quasiNeutral.hpp>
#include <time.hpp>

namespace bsl6d {

namespace Impl {
bool is_positive_definite(DistributionFunction const &f6d) {
  double min{};
  Kokkos::parallel_reduce(
      "TestNegativPhasespacevalues", f6d.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3, double &lMin) {
        lMin = std::min(lMin, f6d(ix1, ix2, ix3, iv1, iv2, iv3));
      },
      Kokkos::Min<double>(min));
  if (min <= 0.0) { return false; }
  return true;
}

bool has_nan_values(DistributionFunction const &f6d) {
  bool hasNan{false};

  Kokkos::deep_copy(f6d.host(), f6d.device());

  Kokkos::LOr<bool, Kokkos::HostSpace> reducer{hasNan};
  Kokkos::parallel_reduce(
      "TestNaNValuesf6d",
      f6d.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [&](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3, bool &lhasNan) {
        if (lhasNan or
            Kokkos::isnan(f6d.host()(ix1, ix2, ix3, iv1, iv2, iv3))) {
          lhasNan = true;
        }
      },
      reducer);
  return hasNan;
}

std::map<std::string, FuncTypes> initFunctionMap{
    {"planewave_dens_perturbation", FuncTypes::pw_dens_pert},
    {"gauss", FuncTypes::gauss},
    {"rand_dens_perturbation", FuncTypes::rand_dens_pert},
    {"zero", FuncTypes::zero},
    {"ibw_perturbation", FuncTypes::ibw_pert}};

bool available_init_keyword(std::string key) {
  auto search = Impl::initFunctionMap.find(key);
  if (search != Impl::initFunctionMap.end()) {
    return true;
  } else {
    return false;
  }
}

}  // namespace Impl

void plane_wave_dens_pertubation(DistributionFunction &f6d,
                                 DistributionInitializationConfig config) {
  std::array<double, 6> xv0{config.config.xv0};
  double temperature{config.config.temperature};
  std::array<double, 3> mk{config.config.mk};
  double alpha{config.config.alpha};

  Grid<3, 3> globalGrid{f6d.gridDistribution().mpi_global_grid()};
  auto planewavePerturbation = KOKKOS_LAMBDA(double x1, double x2, double x3,
                                             double v1, double v2, double v3)
                                   ->double {
    double k0x1{2.0 * M_PI / globalGrid(AxisType::x1).L()};
    double k0x2{2.0 * M_PI / globalGrid(AxisType::x2).L()};
    double k0x3{2.0 * M_PI / globalGrid(AxisType::x3).L()};
    double fx{};
    fx = 1 +
         alpha * std::sin(mk[AxisType::x1] * k0x1 * (x1 - xv0[AxisType::x1]) +
                          mk[AxisType::x2] * k0x2 * (x2 - xv0[AxisType::x2]) +
                          mk[AxisType::x3] * k0x3 * (x3 - xv0[AxisType::x3]));
    double fv{1.0};
    // Maybe solve this using an iterator on the axis of a grid.
    if (globalGrid(AxisType::v1).N() != 1)
      fv *= Impl::gauss(v1, xv0[AxisType::v1], temperature);
    if (globalGrid(AxisType::v2).N() != 1)
      fv *= Impl::gauss(v2, xv0[AxisType::v2], temperature);
    if (globalGrid(AxisType::v3).N() != 1)
      fv *= Impl::gauss(v3, xv0[AxisType::v3], temperature);
    return fx * fv;
  };
  Impl::fill(f6d, planewavePerturbation);
}

void gauss(DistributionFunction &f6d, DistributionInitializationConfig config) {
  std::array<double, 6> xv0{config.config.xv0};
  std::array<double, 3> sigma2{config.config.sigma2};
  double temperature{config.config.temperature};

  Grid<3, 3> grid{f6d.mpi_local_grid()};
  auto gaussDist = KOKKOS_LAMBDA(double x1, double x2, double x3, double v1,
                                 double v2, double v3)
                       ->double {
    double fxv{1.0};
    if (grid(AxisType::x1).N() != 1)
      fxv *= Impl::gauss(x1, xv0[AxisType::x1], sigma2[AxisType::x1]);
    if (grid(AxisType::x2).N() != 1)
      fxv *= Impl::gauss(x2, xv0[AxisType::x2], sigma2[AxisType::x2]);
    if (grid(AxisType::x3).N() != 1)
      fxv *= Impl::gauss(x3, xv0[AxisType::x3], sigma2[AxisType::x3]);
    if (grid(AxisType::v1).N() != 1)
      fxv *= Impl::gauss(v1, xv0[AxisType::v1], temperature);
    if (grid(AxisType::v2).N() != 1)
      fxv *= Impl::gauss(v2, xv0[AxisType::v2], temperature);
    if (grid(AxisType::v3).N() != 1)
      fxv *= Impl::gauss(v3, xv0[AxisType::v3], temperature);
    return fxv;
  };
  Impl::fill(f6d, gaussDist);
}

void rand_dens_perturbation(DistributionFunction &f6d,
                            DistributionInitializationConfig config) {
  std::array<double, 6> xv0{config.config.xv0};
  double temperature{config.config.temperature};
  double alpha{config.config.alpha};
  size_t seed{config.config.seed + f6d.gridDistribution()
                                       .create_xGridDistribution()
                                       .cart_topology()
                                       .my_cart_rank()};

  Grid<3, 3> grid{f6d.mpi_local_grid()};
  std::mt19937 generator{static_cast<size_t>(seed)};

  Kokkos::View<double ***> perturbation{
      create_grid_compatible_view(grid.create_xSpace(), "RandValues")};
  Kokkos::View<double ***>::HostMirror h_perturbation{
      Kokkos::create_mirror_view(perturbation)};

  // mt19937.operator() is not const and cannot be used in a Lambda
  for (size_t ix1 = 0; ix1 < grid(AxisType::x1).N(); ix1++)
    for (size_t ix2 = 0; ix2 < grid(AxisType::x2).N(); ix2++)
      for (size_t ix3 = 0; ix3 < grid(AxisType::x3).N(); ix3++) {
        h_perturbation(ix1, ix2, ix3) =
            (double)(generator() - (double)generator.max() / 2.0) * 2.0 /
            generator.max();
      }

  Kokkos::deep_copy(perturbation, h_perturbation);
  Kokkos::parallel_for(
      "Init density distribution", f6d.mpi_local_grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
                    size_t iv3) {
        std::array<size_t, 6> ixv{ix1, ix2, ix3, iv1, iv2, iv3};

        f6d(ix1, ix2, ix3, iv1, iv2, iv3) =
            1.0 + alpha * perturbation(ix1, ix2, ix3);
        for (auto axis : grid.vAxes) {
          size_t axisIdx{static_cast<size_t>(axis)};
          if (grid(axis).N() != 1) {
            f6d(ix1, ix2, ix3, iv1, iv2, iv3) *= Impl::gauss(
                grid(axis)(ixv[axisIdx]), xv0[axisIdx], temperature);
          }
        }
      });
  f6d.require_velocity_moments_update();
}

void ibw_perturbation(DistributionFunction &f6d,
                      DistributionInitializationConfig config) {
  std::array<double, 6> xv0{config.config.xv0};
  double temperature{config.config.temperature};
  double alpha{config.config.alpha};
  std::array<size_t, 6> polynomOrder{config.config.polynomOrder};
  std::array<double, 3> mkMax{config.config.mk};

  double k0x1{2 * M_PI /
              f6d.gridDistribution().mpi_global_grid()(AxisType::x1).L()};
  double k0x2{2 * M_PI /
              f6d.gridDistribution().mpi_global_grid()(AxisType::x2).L()};

  Grid<3, 3> globalGrid{f6d.gridDistribution().mpi_global_grid()};

  auto analyticalExpression = [=](double x1, double x2, double x3, double v1,
                                  double v2, double v3) -> double {
    double maxwellian{1.0};
    if (globalGrid(AxisType::v1).N() != 1)
      maxwellian *= Impl::gauss(v1, xv0[AxisType::v1], temperature);
    if (globalGrid(AxisType::v2).N() != 1)
      maxwellian *= Impl::gauss(v2, xv0[AxisType::v2], temperature);
    if (globalGrid(AxisType::v3).N() != 1)
      maxwellian *= Impl::gauss(v3, xv0[AxisType::v3], temperature);

    double vPerb{std::sqrt(v1 * v1 + v2 * v2)};
    double perturbation{};
    if (vPerb != 0.0) {
      for (size_t mkx1 = 0; mkx1 < mkMax[AxisType::x1]; mkx1++) {
        for (size_t mkx2 = 0; mkx2 < mkMax[AxisType::x2]; mkx2++) {
          double kx1{mkx1 * k0x1};
          double kx2{mkx2 * k0x2};
          double kPerb{std::sqrt(kx1 * kx1 + kx2 + kx2)};
          if (kPerb != 0.0) {
            double vPerbKxAngle{std::atan2(v1, v2) - std::atan2(kx1, kx2)};
            for (size_t p = 0; p <= std::max(polynomOrder[AxisType::v1],
                                             polynomOrder[AxisType::v2]);
                 p++) {
              perturbation += alpha * std::cyl_bessel_j(p, vPerb * kPerb) *
                              std::cos(vPerb * kPerb * std::sin(vPerbKxAngle) -
                                       p * vPerbKxAngle + kx1 * x1 + kx2 * x2);
            }
          }
        }
      }
    }
    return maxwellian * (1.0 + perturbation);
  };

  Kokkos::parallel_for(
      "Init density distribution",
      f6d.mpi_local_grid().mdRangePolicy<Kokkos::DefaultHostExecutionSpace>(),
      [=](size_t ix1, size_t ix2, size_t ix3, size_t iv1, size_t iv2,
          size_t iv3) {
        double x1{f6d.mpi_local_grid()(AxisType::x1)(ix1)};
        double x2{f6d.mpi_local_grid()(AxisType::x2)(ix2)};
        double x3{f6d.mpi_local_grid()(AxisType::x3)(ix3)};
        double v1{f6d.mpi_local_grid()(AxisType::v1)(iv1)};
        double v2{f6d.mpi_local_grid()(AxisType::v2)(iv2)};
        double v3{f6d.mpi_local_grid()(AxisType::v3)(iv3)};
        f6d.host()(ix1, ix2, ix3, iv1, iv2, iv3) =
            analyticalExpression(x1, x2, x3, v1, v2, v3);
      });
  Kokkos::deep_copy(f6d.device(), f6d.host());
  f6d.require_velocity_moments_update();
}

void read_config_from_restart_file(openPMD::Series series, int iteration) {
  openPMD::Iteration iter{series.iterations.at(iteration)};
  iter.open();
  if (iter.meshes.contains("f6d")) {
    openPMD::Mesh mesh{iter.meshes["f6d"]};
    nlohmann::json configFile = nlohmann::json::parse(
        mesh.getAttribute("configFile").get<std::string>());
    Impl::validate_and_complete_config_file_structure(configFile);
  } else {
    throw std::runtime_error("File does not contain a distribution function!");
  }
}

void read_config_file(std::string const &filename) {
  std::ifstream ifs(filename);
  nlohmann::json configFile{};
  if (std::filesystem::exists(filename)) {
    configFile = nlohmann::json::parse(std::istreambuf_iterator<char>{ifs},
                                       std::istreambuf_iterator<char>{});
  } else {
    std::cerr << "Warning: File does not exist: " << filename << std::endl;
    cmd::print_flags_help_message();
    exit(EXIT_FAILURE);
  }
  Impl::validate_and_complete_config_file_structure(configFile);
  Impl::validate_file_data(config_file(), bsl6d_log_stream());
}

void conduct_restart(DistributionFunction &f6d,
                     SimulationTime simTime,
                     openPMD::Series series) {
  read_openPMD(f6d, series, simTime);

  DiagnosticConfig diagnosticConfig{config_file()};
  size_t smallestDiagFrequency{std::numeric_limits<size_t>::max()};
  for (auto diag : diagnosticConfig.diagOutConf) {
    if (diag.iteration < smallestDiagFrequency) {
      smallestDiagFrequency = diag.iteration;
    }
  }

  size_t number{simTime.current_step()};
  std::string paddedNumber{};

  bool fileRemoved{true};
  size_t iterationsWithoutRemoving{};
  while (fileRemoved) {
    number++;
    std::stringstream currentDeletedDiagnostic{};
    currentDeletedDiagnostic << std::setw(Diagnostic::s_outputIterationDigits)
                             << std::setfill('0') << number;
    paddedNumber = currentDeletedDiagnostic.str();
    fileRemoved =
        std::filesystem::remove(cmd::diagnostic_directory() + paddedNumber +
                                Diagnostic::s_outputSuffix);
    if (not fileRemoved) {
      iterationsWithoutRemoving++;
    } else {
      iterationsWithoutRemoving = 0;
    }
    if (iterationsWithoutRemoving < smallestDiagFrequency) {
      fileRemoved = true;
    }
  }
}

std::pair<SimulationTime, DistributionFunction> initialize(
    std::optional<nlohmann::json> configFile) {
  SimulationTime simTime{};
  openPMD::Series series{};
  Impl::FuncTypes type{};

  if (cmd::is_restart()) {
    size_t iteration{
        Impl::get_iteration_from_restart_file(cmd::restart_file())};
    std::filesystem::path seriesPath{};
    seriesPath = cmd::restart_file().parent_path() / Diagnostic::series_name();
    series = openPMD::Series{seriesPath.string(), openPMD::Access::READ_ONLY,
                             R"({"hdf5": {"dataset": {"chunks": "none"}},
                                 "defer_iteration_parsing": true})"};
    if (cmd::input_file_passed()) {
      read_config_file(cmd::input_file());
    } else {
      read_config_from_restart_file(series, iteration);
    }
    simTime = Impl::read_simulation_time(series, iteration);
    type    = Impl::FuncTypes::restart;
  } else {
    if (configFile.has_value()) {
      Impl::validate_and_complete_config_file_structure(configFile.value());
    } else {
      read_config_file(cmd::input_file());
    }
    simTime = SimulationTime{SimulationTimeConfig{config_file()}};
    type    = Impl::initFunctionMap.at(
        DistributionInitializationConfig{config_file()}.config.initFunction);
  }

  CartTopology<6> topology{GridDistributionConfig{config_file()}};
  Grid<3, 3> grid{GridDistributionConfig{config_file()}};
  GridDistribution<3, 3> gridDistrib{grid, topology};

  DistributionFunction f6d{gridDistrib,
                           DistributionFunctionConfig{config_file()}};

  DistributionInitializationConfig config{config_file()};
  switch (type) {
    case Impl::FuncTypes::restart: conduct_restart(f6d, simTime, series); break;
    case Impl::FuncTypes::zero: Kokkos::deep_copy(f6d.device(), 0.0); break;
    case Impl::FuncTypes::pw_dens_pert:
      plane_wave_dens_pertubation(f6d, config);
      break;
    case Impl::FuncTypes::gauss: gauss(f6d, config); break;
    case Impl::FuncTypes::rand_dens_pert:
      rand_dens_perturbation(f6d, config);
      break;
    case Impl::FuncTypes::ibw_pert: ibw_perturbation(f6d, config); break;
    default:
      std::runtime_error("Unknown function type for initialization of 6d-View");
  };
  if ((not Impl::is_positive_definite(f6d)) and
      (type != Impl::FuncTypes::zero) and (type != Impl::FuncTypes::restart)) {
    throw std::runtime_error("The distribution function " +
                             f6d.device().label() +
                             " contains negativ phasespace values after "
                             "initialization which is not permitted.");
  } else if (Impl::has_nan_values(f6d)) {
    throw std::runtime_error("The distribution function " +
                             f6d.device().label() +
                             " contains nan values after initialization!");
  }

  if (not cmd::is_restart()) {
    Kokkos::deep_copy(f6d.electron_flux_surface_average(),
                      flux_surface_average(f6d.calculate_velocity_moments().n));
  }

  return std::pair<SimulationTime, DistributionFunction>{simTime, f6d};
}

std::pair<std::shared_ptr<Characteristic>, std::map<AxisType, Advector>>
    initialize_advector(DistributionFunction const &f6d) {
  LagrangeBuilder lagrangeBuilder{};
  lagrangeBuilder.setLagrangeConf(OperatorsConfig{config_file()});
  lagrangeBuilder.set_gridDistribution(f6d.gridDistribution());
  std::map<AxisType, Advector> advector{};
  std::shared_ptr<Characteristic> characteristicPtr{};
  CharacteristicVariants characteristicVariant{};

  {
    bool rotateGrid{
        GridDistributionConfig{config_file()}.configGrid.rotateGyroFreq};
    if (rotateGrid) {
      std::shared_ptr<LorentzForceRotatingGrid> characteristicLFRGPtr{
          std::make_shared<LorentzForceRotatingGrid>(
              LorentzForceRotatingGrid{f6d})};
      characteristicVariant = CharacteristicVariants{characteristicLFRGPtr};
      characteristicPtr     = characteristicLFRGPtr;
    } else {
      std::shared_ptr<LorentzForce> characteristicLFPtr{
          std::make_shared<LorentzForce>(LorentzForce{f6d})};
      characteristicVariant = CharacteristicVariants{characteristicLFPtr};
      characteristicPtr     = characteristicLFPtr;
    }
    for (auto axis : xvAxes())
      advector[axis] = Advector{characteristicPtr, characteristicVariant, axis,
                                lagrangeBuilder};
  }
  return std::pair<std::shared_ptr<Characteristic>,
                   std::map<AxisType, Advector>>{characteristicPtr, advector};
}

std::unique_ptr<FieldSolver> initialize_fieldSolver() {
  std::unique_ptr<FieldSolver> fieldSolver_ptr{};
  FieldSolverConfig config{config_file()};
  if (config.type() == "quasiNeutral") {
    return std::make_unique<QuasiNeutral>(QuasiNeutral{config_file()});
  } else if (config.type() == "poisson") {
    return std::make_unique<Poisson>(Poisson{});
  } else if (config.type() == "backgroundField") {
    return std::make_unique<BackgroundMaxwellFieldSolver>(
        BackgroundMaxwellFieldSolver{config_file()});
  } else if (config.type() == "None") {
    return NULL;
  }
  throw std::runtime_error("Tried to create an undefined field solver!");
}
} /*namespace bsl6d*/

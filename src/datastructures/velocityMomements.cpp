/*****************************************************************/
/*
\brief Base structure to define 6d distribution function

\author Nils Schild
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   05.11.2021

*/
/*****************************************************************/

#include <distributionFunction.hpp>
#include <iostream>
#include <velocityMomements.hpp>

namespace bsl6d {
namespace Impl {
VelocityMoments calculate_velocity_moments(DistributionFunction const& f6d) {
  Kokkos::Profiling::pushRegion("CalculateVelocityMoments");
  VelocityMoments velocityMoments{};
  velocityMoments.n =
      ScalarField{f6d.gridDistribution().create_xGridDistribution(), "n"};

  velocityMoments.particleFlux = VectorField{
      f6d.gridDistribution().create_xGridDistribution(), "particleFlux"};
  velocityMoments.energyDensity = ScalarField{
      f6d.gridDistribution().create_xGridDistribution(), "energyDensity"};
  velocityMoments.secondMoment = MatrixField{
      f6d.gridDistribution().create_xGridDistribution(), "secondMoment"};
  velocityMoments.reducedFourthMoment = MatrixField{
      f6d.gridDistribution().create_xGridDistribution(), "reducedFourthMoment"};
  velocityMoments.energyFlux = VectorField{
      f6d.gridDistribution().create_xGridDistribution(), "energyFlux"};
  Kokkos::deep_copy(velocityMoments.n.device(), 0.0);
  Kokkos::deep_copy(velocityMoments.energyDensity.device(), 0.0);
  for (auto axis1 : f6d.grid().xAxes) {
    Kokkos::deep_copy(velocityMoments.particleFlux(axis1).device(), 0.0);
    for (auto axis2 : f6d.grid().xAxes) {
      Kokkos::deep_copy(velocityMoments.secondMoment(axis1, axis2).device(),
                        0.0);
      Kokkos::deep_copy(
          velocityMoments.reducedFourthMoment(axis1, axis2).device(), 0.0);
    }
    Kokkos::deep_copy(velocityMoments.energyFlux(axis1).device(), 0.0);
  }

  Kokkos::parallel_for(
      "CalculateVelocityMoments", velocityMoments.n.grid().mdRangePolicy(),
      KOKKOS_LAMBDA(size_t ix1, size_t ix2, size_t ix3) {
        for (size_t iv1 = 0; iv1 < f6d.grid()(AxisType::v1).N(); iv1++)
          for (size_t iv2 = 0; iv2 < f6d.grid()(AxisType::v2).N(); iv2++)
            for (size_t iv3 = 0; iv3 < f6d.grid()(AxisType::v3).N(); iv3++) {
              double f6d_val = f6d(ix1, ix2, ix3, iv1, iv2, iv3);
              double v1      = f6d.grid()(AxisType::v1)(iv1);
              double v2      = f6d.grid()(AxisType::v2)(iv2);
              double v3      = f6d.grid()(AxisType::v3)(iv3);
              double vv      = v1 * v1 + v2 * v2 + v3 * v3;

              velocityMoments.n(ix1, ix2, ix3) += f6d_val;

              velocityMoments.particleFlux(AxisType::x1)(ix1, ix2, ix3) +=
                  f6d_val * v1;
              velocityMoments.particleFlux(AxisType::x2)(ix1, ix2, ix3) +=
                  f6d_val * v2;
              velocityMoments.particleFlux(AxisType::x3)(ix1, ix2, ix3) +=
                  f6d_val * v3;

              velocityMoments.energyDensity(ix1, ix2, ix3) += f6d_val * vv / 2;

              velocityMoments.secondMoment(AxisType::x1, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v1 * v1;
              velocityMoments.secondMoment(AxisType::x1, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v1 * v2;
              velocityMoments.secondMoment(AxisType::x1, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v1 * v3;
              velocityMoments.secondMoment(AxisType::x2, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v2 * v1;
              velocityMoments.secondMoment(AxisType::x2, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v2 * v2;
              velocityMoments.secondMoment(AxisType::x2, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v2 * v3;
              velocityMoments.secondMoment(AxisType::x3, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v3 * v1;
              velocityMoments.secondMoment(AxisType::x3, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v3 * v2;
              velocityMoments.secondMoment(AxisType::x3, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v3 * v3;

              velocityMoments.reducedFourthMoment(AxisType::x1, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v1 * v1 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x1, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v1 * v2 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x1, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v1 * v3 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x2, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v2 * v1 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x2, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v2 * v2 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x2, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v2 * v3 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x3, AxisType::x1)(
                  ix1, ix2, ix3) += f6d_val * v3 * v1 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x3, AxisType::x2)(
                  ix1, ix2, ix3) += f6d_val * v3 * v2 * vv / 2.0;
              velocityMoments.reducedFourthMoment(AxisType::x3, AxisType::x3)(
                  ix1, ix2, ix3) += f6d_val * v3 * v3 * vv / 2.0;

              velocityMoments.energyFlux(AxisType::x1)(ix1, ix2, ix3) +=
                  f6d_val * v1 * vv / 2;
              velocityMoments.energyFlux(AxisType::x2)(ix1, ix2, ix3) +=
                  f6d_val * v2 * vv / 2;
              velocityMoments.energyFlux(AxisType::x3)(ix1, ix2, ix3) +=
                  f6d_val * v3 * vv / 2;
            }
        // ToDo what is the impact of cell volume for a distributed grid?
        // gird is my_grid; global_grid might have a slightly different cell
        // volume
        velocityMoments.n(ix1, ix2, ix3) *= f6d.grid().cell_vol_v();
        velocityMoments.energyDensity(ix1, ix2, ix3) *= f6d.grid().cell_vol_v();
        for (auto axis1 : f6d.grid().xAxes) {
          velocityMoments.particleFlux(axis1)(ix1, ix2, ix3) *=
              f6d.grid().cell_vol_v();
          for (auto axis2 : f6d.grid().xAxes) {
            velocityMoments.secondMoment(axis1, axis2)(ix1, ix2, ix3) *=
                f6d.grid().cell_vol_v();
            velocityMoments.reducedFourthMoment(axis1, axis2)(ix1, ix2, ix3) *=
                f6d.grid().cell_vol_v();
          }
          velocityMoments.energyFlux(axis1)(ix1, ix2, ix3) *=
              f6d.grid().cell_vol_v();
        }
      });

  Kokkos::fence("Reduce Veloity Moments");
  bsl6d::CartTopology<3> topologyV{
      f6d.gridDistribution().cart_topology().split<3>(
          {false, false, false, true, true, true})};
#if BSL6D_ENABLE_CUDA_AWARE_MPI
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE,
                                 velocityMoments.n.device().data(),
                                 velocityMoments.n.device().size(), MPI_DOUBLE,
                                 MPI_SUM, topologyV.cart_comm()));
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE,
                                 velocityMoments.energyDensity.device().data(),
                                 velocityMoments.energyDensity.device().size(),
                                 MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
  for (auto axis1 : f6d.grid().xAxes) {
    bsl6d::CHECK_MPI(MPI_Allreduce(
        MPI_IN_PLACE, velocityMoments.particleFlux(axis1).device().data(),
        velocityMoments.particleFlux(axis1).device().size(), MPI_DOUBLE,
        MPI_SUM, topologyV.cart_comm()));
    for (auto axis2 : f6d.grid().xAxes) {
      bsl6d::CHECK_MPI(MPI_Allreduce(
          MPI_IN_PLACE,
          velocityMoments.secondMoment(axis1, axis2).device().data(),
          velocityMoments.secondMoment(axis1, axis2).device().size(),
          MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
      bsl6d::CHECK_MPI(MPI_Allreduce(
          MPI_IN_PLACE,
          velocityMoments.reducedFourthMoment(axis1, axis2).device().data(),
          velocityMoments.reducedFourthMoment(axis1, axis2).device().size(),
          MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
    }
    bsl6d::CHECK_MPI(MPI_Allreduce(
        MPI_IN_PLACE, velocityMoments.energyFlux(axis1).device().data(),
        velocityMoments.energyFlux(axis1).device().size(), MPI_DOUBLE, MPI_SUM,
        topologyV.cart_comm()));
  }
#else
  Kokkos::deep_copy(velocityMoments.n.host(), velocityMoments.n.device());
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE, velocityMoments.n.host().data(),
                                 velocityMoments.n.host().size(), MPI_DOUBLE,
                                 MPI_SUM, topologyV.cart_comm()));
  Kokkos::deep_copy(velocityMoments.n.device(), velocityMoments.n.host());
  Kokkos::deep_copy(velocityMoments.energyDensity.host(),
                    velocityMoments.energyDensity.device());
  bsl6d::CHECK_MPI(MPI_Allreduce(MPI_IN_PLACE,
                                 velocityMoments.energyDensity.host().data(),
                                 velocityMoments.energyDensity.host().size(),
                                 MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
  Kokkos::deep_copy(velocityMoments.energyDensity.device(),
                    velocityMoments.energyDensity.host());
  for (auto axis1 : f6d.grid().xAxes) {
    Kokkos::deep_copy(velocityMoments.particleFlux(axis1).host(),
                      velocityMoments.particleFlux(axis1).device());
    bsl6d::CHECK_MPI(MPI_Allreduce(
        MPI_IN_PLACE, velocityMoments.particleFlux(axis1).host().data(),
        velocityMoments.particleFlux(axis1).device().size(), MPI_DOUBLE,
        MPI_SUM, topologyV.cart_comm()));
    Kokkos::deep_copy(velocityMoments.particleFlux(axis1).device(),
                      velocityMoments.particleFlux(axis1).host());
    for (auto axis2 : f6d.grid().xAxes) {
      Kokkos::deep_copy(velocityMoments.secondMoment(axis1, axis2).host(),
                        velocityMoments.secondMoment(axis1, axis2).device());
      Kokkos::deep_copy(
          velocityMoments.reducedFourthMoment(axis1, axis2).host(),
          velocityMoments.reducedFourthMoment(axis1, axis2).device());
      bsl6d::CHECK_MPI(MPI_Allreduce(
          MPI_IN_PLACE,
          velocityMoments.secondMoment(axis1, axis2).host().data(),
          velocityMoments.secondMoment(axis1, axis2).device().size(),
          MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
      bsl6d::CHECK_MPI(MPI_Allreduce(
          MPI_IN_PLACE,
          velocityMoments.reducedFourthMoment(axis1, axis2).host().data(),
          velocityMoments.reducedFourthMoment(axis1, axis2).device().size(),
          MPI_DOUBLE, MPI_SUM, topologyV.cart_comm()));
      Kokkos::deep_copy(velocityMoments.secondMoment(axis1, axis2).device(),
                        velocityMoments.secondMoment(axis1, axis2).host());
      Kokkos::deep_copy(
          velocityMoments.reducedFourthMoment(axis1, axis2).device(),
          velocityMoments.reducedFourthMoment(axis1, axis2).host());
    }
    Kokkos::deep_copy(velocityMoments.energyFlux(axis1).host(),
                      velocityMoments.energyFlux(axis1).device());
    bsl6d::CHECK_MPI(MPI_Allreduce(
        MPI_IN_PLACE, velocityMoments.energyFlux(axis1).host().data(),
        velocityMoments.energyFlux(axis1).device().size(), MPI_DOUBLE, MPI_SUM,
        topologyV.cart_comm()));
    Kokkos::deep_copy(velocityMoments.energyFlux(axis1).device(),
                      velocityMoments.energyFlux(axis1).host());
  }
#endif
  MPI_Barrier(topologyV.cart_comm());
  Kokkos::Profiling::popRegion();

  return velocityMoments;
}
}  // namespace Impl
}  // namespace bsl6d

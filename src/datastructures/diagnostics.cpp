/*****************************************************************/
/*
\brief Initializations for distribution functions

\author Nils Schild; Mario Raeth
\institution Max-Plank-Institute for Plasma Physics
\department NMPP
\email  nils.schild@ipp.mpg.de
\date   28.10.2021

*/
/*****************************************************************/
#include <filesystem>

#include <characteristicHelpers.hpp>
#include <diagnostics.hpp>
#include <linearAlgebra.hpp>

#include <mpi_helper.hpp>

namespace bsl6d {

namespace Impl {

void gyroCollectMeanPhi(SimulationTime simTime,
                        ScalarField phi,
                        Storage storage) {
  auto phiMean_sub = Kokkos::subview(
      storage.phiMean, simTime.current_step() % storage.phiMean.extent(0),
      Kokkos::ALL(), Kokkos::ALL(), Kokkos::ALL());

  deep_copy(phiMean_sub, phi.device());
}

ScalarField DiagnosticPhiMean(DistributionFunction f6d,
                              ScalarField rho,
                              ScalarField phi,
                              VectorField E,
                              Storage storage) {
  ScalarField phiMean{phi.gridDistribution(), "phiMean"};

  Kokkos::MDRangePolicy<Kokkos::Rank<3>> mdRangePolicy{
      {0, 0, 0},
      {phi.grid()(AxisType::x1).N(), phi.grid()(AxisType::x2).N(),
       phi.grid()(AxisType::x3).N()}};
  Kokkos::parallel_for(
      "MeanPhi", mdRangePolicy, KOKKOS_LAMBDA(size_t i0, size_t i1, size_t i2) {
        for (size_t iSum = 0; iSum < storage.phiMean.extent(0); iSum++)
          phiMean(i0, i1, i2) =
              phiMean(i0, i1, i2) +
              storage.phiMean(iSum, i0, i1, i2) / storage.phiMean.extent(0);
      });
  return phiMean;
}
void diagnostic_distribution_function(SimulationTime const &simTime,
                                      DistributionFunction &f6d,
                                      MaxwellFields const &maxwellFields,
                                      openPMD::Series const &series) {
  write_openPMD(f6d, series, simTime);
}
void diagnostic_particle_density(SimulationTime const &simTime,
                                 DistributionFunction &f6d,
                                 MaxwellFields const &maxwellFields,
                                 openPMD::Series const &series) {
  ScalarField rho{f6d.calculate_velocity_moments().n};
  write_openPMD(rho, series, simTime);
}
void diagnostic_particle_flux(SimulationTime const &simTime,
                              DistributionFunction &f6d,
                              MaxwellFields const &maxwellFields,
                              openPMD::Series const &series) {
  VectorField flux{f6d.calculate_velocity_moments().particleFlux};
  write_openPMD(flux, series, simTime);
}
void diagnostic_kinetic_energy(SimulationTime const &simTime,
                               DistributionFunction &f6d,
                               MaxwellFields const &maxwellFields,
                               openPMD::Series const &series) {
  ScalarField eKin{f6d.calculate_velocity_moments().energyDensity};
  write_openPMD(eKin, series, simTime);
}
void diagnostic_second_moment_tensor(SimulationTime const &simTime,
                                     DistributionFunction &f6d,
                                     MaxwellFields const &maxwellFields,
                                     openPMD::Series const &series) {
  MatrixField stress{f6d.calculate_velocity_moments().secondMoment};
  write_openPMD(stress, series, simTime);
}

void diagnostic_reduced_fourth_moment_tensor(SimulationTime const &simTime,
                                             DistributionFunction &f6d,
                                             MaxwellFields const &maxwellFields,
                                             openPMD::Series const &series) {
  MatrixField stress{f6d.calculate_velocity_moments().reducedFourthMoment};
  write_openPMD(stress, series, simTime);
}

void diagnostic_energy_flux(const SimulationTime &simTime,
                            DistributionFunction &f6d,
                            const MaxwellFields &maxwellFields,
                            const openPMD::Series &series) {
  VectorField energyFlux{f6d.calculate_velocity_moments().energyFlux};
  write_openPMD(energyFlux, series, simTime);
}
void diagnostic_labframe_particle_flux(SimulationTime const &simTime,
                                       DistributionFunction &f6d,
                                       MaxwellFields const &maxwellFields,
                                       openPMD::Series const &series) {
  double w_c{f6d.q() / f6d.m() * maxwellFields.Bx3};
  Matrix rotMat{Impl::RotationMatrix::R_x3_inv(simTime.current_T() * w_c)};
  VectorField flux{rotMat * f6d.calculate_velocity_moments().particleFlux};
  VectorField labFlux{f6d.gridDistribution().create_xGridDistribution(),
                      "labframeParticleFlux"};
  for (auto axis : f6d.grid().xAxes) {
    Kokkos::deep_copy(labFlux(axis).device(), flux(axis).device());
  };
  write_openPMD(labFlux, series, simTime);
}
void diagnostic_labframe_energy_flux(SimulationTime const &simTime,
                                     DistributionFunction &f6d,
                                     MaxwellFields const &maxwellFields,
                                     openPMD::Series const &series) {
  double w_c{f6d.q() / f6d.m() * maxwellFields.Bx3};
  Matrix rotMat{Impl::RotationMatrix::R_x3_inv(simTime.current_T() * w_c)};
  VectorField energyflux{rotMat * f6d.calculate_velocity_moments().energyFlux};
  VectorField labEnergyFlux{f6d.gridDistribution().create_xGridDistribution(),
                            "labframeEnergyFlux"};
  for (auto axis : f6d.grid().xAxes) {
    Kokkos::deep_copy(labEnergyFlux(axis).device(), energyflux(axis).device());
  };
  write_openPMD(labEnergyFlux, series, simTime);
}
void diagnostic_electric_potential(SimulationTime const &simTime,
                                   DistributionFunction &f6d,
                                   MaxwellFields const &maxwellFields,
                                   openPMD::Series const &series) {
  write_openPMD(maxwellFields.phi, series, simTime);
}
void diagnostic_electric_field(SimulationTime const &simTime,
                               DistributionFunction &f6d,
                               MaxwellFields const &maxwellFields,
                               openPMD::Series const &series) {
  write_openPMD(maxwellFields.E, series, simTime);
}

size_t get_iteration_from_restart_file(std::filesystem::path restartFile) {
  std::string filename{restartFile.filename().string()};

  size_t iteration =
      std::stoi(filename.substr(0, Diagnostic::s_outputIterationDigits));

  return iteration;
}

} /*namespace Impl*/

Diagnostic::Diagnostic(DiagnosticConfig const &conf,
                       CartTopology<6> const &topo)
    : config{conf.diagOutConf} {
  if (topo.my_cart_rank() == 0 && !cmd::is_restart()) {
    const std::string &diagnostic_dir = cmd::diagnostic_directory();

    if (std::filesystem::exists(diagnostic_dir)) {
      if (cmd::delete_flag_value()) {
        std::filesystem::remove_all(diagnostic_dir);
      } else {
        throw std::runtime_error("The directory '" + diagnostic_dir +
                                 "' already exists! Use '--delete/-d' to "
                                 "overwrite old diagnostics folder.");
      }
    }
    bool is_created{std::filesystem::create_directory(diagnostic_dir)};
    if (!is_created) {
      throw std::runtime_error("Failed to create the directory '" +
                               diagnostic_dir + "'!");
    }
  }
  CHECK_MPI(MPI_Barrier(topo.comm()));
  openPMD::Access accessType{};
  accessType = openPMD::Access::CREATE;

  m_series = openPMD::Series{
      std::filesystem::path(cmd::diagnostic_directory()) / this->series_name(),
      accessType, topo.comm(), R"({"hdf5": {"dataset": {"chunks": "none"}},
                                 "defer_iteration_parsing": true })"};
}

void Diagnostic::operator()(const SimulationTime &simTime,
                            DistributionFunction &f6d,
                            const MaxwellFields &maxwellFields) {
  for (auto output : config) {
    if (simTime.current_step() % output.iteration == 0) {
      s_diagnostics.at(output.diagnostic)(simTime, f6d, maxwellFields,
                                          m_series);
    }
  }
  openPMD::Iteration iter{m_series.iterations[simTime.current_step()]};
  iter.close();
}

const std::map<std::string, Diagnostic::diagnostic_function>
    Diagnostic::s_diagnostics = {
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "f6d", Impl::diagnostic_distribution_function},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "n", Impl::diagnostic_particle_density},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "particleFlux", Impl::diagnostic_particle_flux},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "kineticEnergy", Impl::diagnostic_kinetic_energy},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "secondMoment", Impl::diagnostic_second_moment_tensor},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "reducedFourthMoment",
            Impl::diagnostic_reduced_fourth_moment_tensor},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "energyFlux", Impl::diagnostic_energy_flux},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "labframeParticleFlux", Impl::diagnostic_labframe_particle_flux},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "labframeEnergyFlux", Impl::diagnostic_labframe_energy_flux},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "phi", Impl::diagnostic_electric_potential},
        std::pair<std::string, Diagnostic::diagnostic_function>{
            "E", Impl::diagnostic_electric_field}};

} /*namespace bsl6d*/

# Authors

BSL6D development is coordinated by a group of *principal developers*, who are also its main contributors and who can be contacted in case of questions about BSL6D. In addition, there are *contributors* who have provided substantial additions or modifications. Together, these two groups form "The BSL6D Authors" as mentioned in the [LICENSE](LICENSE) file.

## Principal Developers

* [Nils Schild](nils.schild@ipp.mpg.de),
  Max Planck Institute for Plasma Physics, Garching, Germany

## Contributors

The following people contributed to BSL6D and are listed in alphabetical order:

* Sebastian Eibl (Max Planck Computing and Data Facility, Garching, Germany)
* Katharina Kormann (Ruhr University Bochum, Bochum, Germany)
* Mario Räth (Max Planck Institute for Plasma Physics, Garching, Germany)
* Klaus Reuter (Max Planck Computing and Data Facility, Garching, Germany)
* Nils Schild (Max Planck Institute for Plasma Physics, Garching, Germany)
* Ali Sedighi (Max Planck Institute for Plasma Physics, Garching, Germany)

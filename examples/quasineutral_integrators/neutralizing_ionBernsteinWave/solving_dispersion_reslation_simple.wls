#!/usr/bin/env wolframscript
(* ::Package:: *)

(* ::Subsection:: *)
(*Preliminaries*)


(* ::Text:: *)
(*Can't assume p\[Element]Integers because then solve doesn't work due to Mathematica bug. -->assumptions.nb*)
(*EI1[__]>0 is not sufficient, it is only used, if also all the arguments are real and EI1[__] is real.*)


(* ::Input::Initialization:: *)
$Assumptions={kz>0,ko>0,Te>0,ky\[Element]Reals,EI1[__]\[Element]Reals,EI1[__]>0,\!\(\*SuperscriptBox[\(EI1\), 
TagBox[
RowBox[{"(", 
RowBox[{"0", ",", "1"}], ")"}],
Derivative],
MultilineFunction->None]\)[__]\[Element]Reals,gT\[Element]Reals,gn\[Element]Reals,gu\[Element]Reals,p\[Element]Reals}


(* ::Input:: *)
(*Z[x_]=I Sqrt[\[Pi]]Exp[-x^2](1+ Erf[I x])*)


(* ::Input::Initialization:: *)
Zr[x_]=-2DawsonF[x]


(* ::Input::Initialization:: *)
Z[x_]=I Sqrt[\[Pi]]Exp[-x^2]+Zr[x]


(* ::Text:: *)
(*^^Much better numerically.*)


(* ::Text:: *)
(*Series of Z for x with positive imaginary part :*)


(* ::Input::Initialization:: *)
SZ[n_]:=Evaluate[(Series[Z[x],{x,Infinity,n}]/.Sqrt[\[Pi]]->0//Normal)/.x->#]&


(* ::Input::Initialization:: *)
EI[p_,x_]=Exp[-x]BesselI[p,x]


(* ::Text:: *)
(*EI is symmetric in p.*)


(* ::Input::Initialization:: *)
EI1[-p_,x_]=EI1[p,x]


(* ::Input::Initialization:: *)
EI1[p_/;p<0,x_]:=EI1[-p,x]


(* ::Input::Initialization:: *)
SEI[n_]:=Evaluate[(Series[EI[p,x],{x,0,n}]//Normal)/.{p->(#1//Abs),x->#2}]&


(* ::Input::Initialization:: *)
fr={EI1->EI,Z1->Z}


(* ::Input::Initialization:: *)
frr={EI1->EI,Z1->Zr}


(* ::Text:: *)
(*^^ for the case that the imaginary contribution in the Z-function is unimportant despite finite kz. This is suggested in the Brunner script. However this is unlikely, as there wouldn't be a zero in the imaginary part.*)


(* ::Input:: *)
(*f1r={BesselI[p_,x_]->EI1[p,x]Exp[x],Solve[Z[x]==Z1[x],Erfi[x]][[1]][[1]]/.Erfi[x]->Erfi[x_]}*)


(* ::Input::Initialization:: *)
f1r={BesselI[p_,x_]->EI1[p,x]Exp[x],Solve[Z[x]==Z1[x],DawsonF[x]][[1]][[1]]/.DawsonF[x]->DawsonF[x_]}


(* ::Input::Initialization:: *)
derZ={Z1'[x_]->Z'[x]}/.f1r//FullSimplify


(* ::Input::Initialization:: *)
der={D[EI1[p_,x_],x_]->D[EI[p,x],x],Z1'[x_]->Z'[x]}/.f1r//FullSimplify


(* ::Input::Initialization::Italic:: *)
ei1[EI_,z_]:=1/z (1-EI[0,z]);


(* ::Input::Initialization:: *)
eirec2[p_/;p<0,EI_,x_]:=-eirec2[-p,EI,x]


(* ::Input::Initialization:: *)
eirec2[m1_,EI_,z_]:=RecurrenceTable[{b[m]==b[m-2] +2/z (1-EI[m-1,z]-(m-1)b[m-1]),b[0]==0,b[1]==ei1[EI,z]},b,{m,m1,m1}][[1]]


(* ::Text:: *)
(*^^this corresponds to the s_n from eq. 567 in localcoord.pdf*)


(* ::Text:: *)
(*To get the values for kz<0 just make the replacement u->-u kz->-kz.*)


(* ::Input::Initialization:: *)
rhs[Z_,EI_,n_]:=Sqrt[\[Xi]/2]/kz Sum[(Z[(\[Omega]+p-u kz)/kz Sqrt[\[Xi]/2]]+Z[(\[Omega]-p-u kz)/kz Sqrt[\[Xi]/2]])EI[p,ko^2/\[Xi]]If[p==0,1/2,1],{p,0,n}]


(* ::Input::Initialization:: *)
rhsn[Z_,EI_,p_]:=Sqrt[\[Xi]/2]/kz Z[(\[Omega]-p-u kz)/kz Sqrt[\[Xi]/2]]EI[p,ko^2/\[Xi]]


(* ::Text:: *)
(*ion response:*)


(* ::Input::Initialization:: *)
iresp[rhs_]:=-((\[Omega]-u kz)rhs-ky(gT D[rhs,\[Xi]]-gn rhs+gu D[rhs,u]))-1 /.{\[Xi]->1,\[Omega]->\[Omega]1+u kz}


(* ::Text:: *)
(*Total ion response, with Z and Gamma functions still unevaluated: (advantageous for symbolic calculations and evaluation):*)


(* ::Text:: *)
(*(Block is necessary due to "der" "derZ")*)


(* ::Input::Initialization:: *)
irespsy[n_]:=Block[{p,x},Collect[iresp[rhs[Z1,EI1,n]]/.der,{Z1[_],EI1[_,_],gn,gT,gu},FullSimplify]]


(* ::Input::Initialization:: *)
irespsyn[n_]:=Block[{p,x},Collect[iresp[rhsn[Z1,EI1,n]]/.derZ,{Z1[_],EI1[_,_],gn,gT,gu},FullSimplify]]


(* ::Input::Initialization:: *)
simbern=Collect[Solve[FullSimplify[Im[irespsyn[p]]==0/.Z1->Z,\[Omega]1\[Element]Reals],{gn},Assumptions->{}][[1]],{gT,kz},FullSimplify]


(* ::Input::Initialization:: *)
simObern={\[Omega]1->((gu ky-kz) kz)/(gT ky)+p-kz \[Sqrt](1-(2 gn)/gT+(-gu ky+kz)^2/(gT^2 ky^2)-(2 p)/(gT ky)-(2 ko^2 \!\(\*SuperscriptBox[\(EI1\), 
TagBox[
RowBox[{"(", 
RowBox[{"0", ",", "1"}], ")"}],
Derivative],
MultilineFunction->None]\)[p,ko^2])/EI1[p,ko^2])};


(* ::Input::Initialization:: *)
rutei={Te->1/(x-1),EI1[p,ko^2]->x y,gu->gukz/kz};


(* ::Input::Initialization:: *)
rutei1={y->EI1[p,ko^2]/(1+1/Te),x->1+1/Te,gukz->gu kz};


(* ::Input::Initialization:: *)
srebern=Collect[Solve[FullSimplify[irespsyn[p]-1/Te==0/.simbern/.Z1->Z],{\[Omega]1}][[1]]/.rutei,kz,FullSimplify]/.rutei1


(* ::Input::Initialization:: *)
gnlimbern=Collect[gn/.simbern/.srebern/.rutei,{gT},FullSimplify]/.rutei1


(* ::Input::Initialization:: *)
rhsz0[EI_,n_]:=-Sum[EI[p,ko^2/(\[Xi] B^2 )]/(\[Omega]+p),{p,-n,n}];
irespsyz0[n_]:=Collect[iresp[rhsz0[EI1,n]]/.der/.kz->0,{EI1[_,_],gn,gT,gu},FullSimplify]


(* ::Input:: *)
(*f =irespsyz0[1]/.fr /.gT->0/.B->1/.gn->0/.\[Omega]1->0//Evaluate *)


(* ::Input:: *)
(*Plot[f,{ko,0,10.3923},PlotRange->All]*)


(* ::Input:: *)
(*(* *)
(*	Neutralizing ion Bernstein waves: irespsyz0[modes]-1*)
(*Pure ion Bernstein waves: irespsyz0[modes]*)
(**)*)
(*With[{modes=7},s=Evaluate[x/. Solve[Evaluate[irespsyz0[modes]-1/. {EI1->EI}/. {\[Omega]1->x+I y}/. {gn->0,gT->0.,y->0,B->1,ky->ko}]==0,x]];*)
(*ListLinePlot[Table[Table[s[[i]],{ko,0.1,10,0.1}],{i,2,2 modes-1}]]]*)
(*Export["dispersionRelation_NonAdiabatic.csv",Table[Table[s[[i]],{ko,0.1,10,0.1}],{i,2,2 modes-1}]]*)


(* ::Input:: *)
(*\[AliasDelimiter]\:f3b5dispersionRelation_NonAdiabatic . csv"*)


(* ::Message:: *)
(*Table::iterb*)


(* ::Input:: *)
(*Clear[a]*)
(*Table[Show@Table[Block[{gu=0 ,gT=0.34,gn=0.21,ko=Sqrt[ky^2+kx^2  ],kx={0.5,4}[[ip]],kz=1/40,\[Omega]1,Te=1,f=1,p={0,-1}[[ip]]},*)
(*p=Show[ta=Table[{ky,\[Omega]1-p}/.Quiet@FindRoot[irespsy[7]-1/Te/.fr,{\[Omega]1,\[Omega]1/.simObern/.fr}//Evaluate,WorkingPrecision->40],{ky,0.5,12,.1}];*)
(*ListPlot[ta/.{a_,b_}->{a,{Re,Im}[[part]][b]},Joined->True,PlotRange->All,Quiet@texstyle["k_y","\\gamma"],PlotStyle->ColorData["Rainbow"][ip/2],ImageSize->400]]],{ip,2}],{part,2}]*)
(**)
(**)

import json
import openpmd_api as opmd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

cm2inch = 0.3937007874

startTime = 100
stopTime = 400
step = 1

model = "neutralizingWaves_dispersionRelation"

try: 
  data = np.load('spectrum_'+ str(startTime)+'_'+str(stopTime)+'.npy')
  K = data[0,:,:]
  W = data[1,:,:]
  dispRel_wkx = data[2,:,:]
except:
  diagnosticDir = 'bsl6dDiagnostics/'
  with open('bsl6dConfig.json') as file:
    conf = json.load(file)
  startStep = startTime / conf["simulationTime"]["dt"]
  stopStep = stopTime / conf["simulationTime"]["dt"]
  diagnostics = opmd.Series(diagnosticDir + '%010T_diagnostics.h5', opmd.Access.read_only,"{\"defer_iteration_parsing\": true}")
  rho_kx = []
  import h5py as h5
  for i in range(int(startStep),int(stopStep),step):
    print( str(i) + '/' + str(stopStep) )
    #iteration = diagnostics.iterations[i]
    #iteration.open()
    #rho = iteration.meshes["n"][opmd.Mesh_Record_Component.SCALAR]
    #rhoData = rho.load_chunk()
    #iteration.close()
    #diagnostics.flush()
    #rhoData = np.array(rhoData)
    #rho_kx.append(np.fft.fftn(rhoData)[:,0,0])
    diagnostics = h5.File(diagnosticDir+'{:010d}_diagnostics.h5'.format(i),'r')
    rho = diagnostics['/data/{:d}/meshes/n'.format(i)]
    rho = np.array(rho)
    rho_kx.append(np.fft.fftn(rho)[:,0,0])
  rho_tkx = np.array(rho_kx)
  del diagnostics
  
  Nx1 = conf['gridDistribution']['grid']['NxvPoints'][0]
  Lx1 = conf['gridDistribution']['grid']['xvMax'][0] - conf['gridDistribution']['grid']['xvMin'][0]
  w = np.fft.fftfreq(len(rho_tkx[:,0]), d = conf['simulationTime']['dt']*step)*2*np.pi
  kx = np.fft.fftfreq(Nx1,Lx1/Nx1)*2*np.pi
  dispRel_wkx = np.fft.fft(rho_tkx*np.kaiser(len(rho_tkx),40)[:,None], axis = 0)
  # consider only positive halfspace
  w  = w[:int(len(rho_tkx)/2)]
  kx = kx[:int(Nx1/2)]
  dispRel_wkx = dispRel_wkx[:int(len(rho_tkx)/2),:int(Nx1/2)]
  
  dispRel_wkx = abs(dispRel_wkx)
  
  K,W = np.meshgrid(kx,w)
  np.save('spectrum_'+ str(startTime)+'_'+str(stopTime)+'.npy', np.array([K,W,dispRel_wkx]))

fig, ax = plt.subplots(1,1, figsize = (12*cm2inch, 6*cm2inch)) 
img = ax.pcolormesh(K,W,dispRel_wkx, norm=LogNorm(vmin=dispRel_wkx.min(), vmax=dispRel_wkx.max()))
analytisch = pd.read_csv(model+".dat")
for col in analytisch.columns[2:]:
  ax.plot(analytisch['k'],analytisch[col],"k--",linewidth=0.5)

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)


ax.set_xlabel(r'$k_x$') 
ax.set_ylabel(r'$\omega/\omega_{ci}$') 
ax.set_ylim([0.5,8.5])
ax.set_xlim([0,9])

plt.colorbar(img, cax=cax)
plt.tight_layout()
plt.savefig(model+".png",dpi=150)
plt.show()

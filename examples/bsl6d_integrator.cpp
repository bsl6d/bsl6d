#include <fstream>
#include <map>
#include <string>

#include <analyticalComparisons.hpp>
#include <config.hpp>
#include <distributionFunction.hpp>
#include <grid.hpp>
#include <gridDistribution.hpp>
#include <initializationFunctions.hpp>
#include <inputFileDatabase.hpp>
#include <integrator.hpp>
#include <io_init.hpp>
#include <mpiTopology.hpp>
#include <mpi_helper.hpp>
#include <scalarField.hpp>
#include <time.hpp>
#include <vectorField.hpp>

int main(int argc, char *argv[]) {
  {
    bsl6d::cmd::parse(argc, argv);
    Kokkos::ScopeGuard scopeGuard{argc, argv};
    bsl6d::CHECK_MPI(MPI_Init(&argc, &argv));

    std::ofstream &ofs{bsl6d::initMasterOut(bsl6d::cmd::output_file())};

    nlohmann::json configFile{};
    if (bsl6d::cmd::is_test()) {
      bsl6d::create_test_case_config_file(bsl6d::cmd::test_type());
    }
    if (bsl6d::cmd::init_only()) {
      ofs << "Create InitFile: " << bsl6d::cmd::init_type() << "\n";
      bsl6d::create_test_case_config_file(bsl6d::cmd::init_type());
      exit(EXIT_SUCCESS);
    }

    /* Create Datastructures */
    auto [simTime, f6d] = bsl6d::initialize();
    bsl6d::MaxwellFields maxwellFields{};

    /* Create Characteristic and Operators */
    bsl6d::Integrator integrate{bsl6d::IntegratorConfig{bsl6d::config_file()},
                                f6d};

    if (not bsl6d::cmd::is_restart())
      integrate(f6d, maxwellFields, simTime, bsl6d::Integrator::Stage::pre);

    Kokkos::Timer timer{};
    ofs << "T;N; t per step [s]\n" << std::flush;

    while (simTime.continue_advection()) {
      double start{timer.seconds()};

      integrate(f6d, maxwellFields, simTime, bsl6d::Integrator::Stage::loop);

      double stop{timer.seconds()};
      ofs << std::fixed << simTime.current_T() << "; " << std::fixed
          << simTime.current_step() << "; " << (stop - start) << "; "
          << "\n"
          << std::flush;
    }

    integrate(f6d, maxwellFields, simTime, bsl6d::Integrator::Stage::post);

    if (bsl6d::cmd::is_test()) {
      bool testPassed{true};
      if (bsl6d::cmd::test_type() == "ion_sound_wave") {
        testPassed = bsl6d::compare_ion_sound_wave(f6d, maxwellFields, simTime);
      }
      std::filesystem::remove_all(bsl6d::cmd::diagnostic_directory());
      if (not testPassed) {
        bsl6d::CHECK_MPI(MPI_Finalize());
        return EXIT_FAILURE;
      }
    }
  }

  bsl6d::CHECK_MPI(MPI_Finalize());
  return EXIT_SUCCESS;
}

Test Charged Particle Trajectory
================================

.. _Lorentz Force: https://de.wikipedia.org/wiki/Larmor-Radius

To test the correctness of the advection in the model the code solves the
system

.. math::
  \partial_t f + \vec v \cdot \nabla f + \frac q m ( -\nabla \phi + \vec v \times B_0 \hat{\vec{z}})\cdot \nabla_v f &= 0

with the charge :math:`q` equal to 1, mass :math:`m` equal to 1 and the amplitude of the background magnetic field
:math:`B_0` in z-direction such that :math:`\vec B = \hat{\vec e}_z`.

In order to test the correctness of the implementation we first test the computation of an electric field from a distribution
function. Secondly we test the propagation of the Vlasov-Equation by using fixed background fields :math:`\vec B` and
:math:`\vec E`. Initializing the distribution function with a gauss should propagate the 6-D distribution function such
that the center of mass of the charge density :math:`\rho` computed from the distribution function follows a charged
point particle trajectory.

Here we test classical trajectories for a free streaming model, :math:`\vec E \times \vec B` drift and gyro motion of a
charged particle in a magnetic field. The distribution function of a point particle can be described by the ansatz

.. math::
  f(\vec r, \vec v) &= f_r(\vec r) \cdot f_v(\vec v) \\
                    &=\dfrac{e^{-\frac{1}{2} \left((x-L_x/2)^2+(y-L_y/2)^2+(z-L_z/2)^2\right)}}{(2\pi)^{3/2}} \cdot f_v(\vec v).

The distribution function is centered in the domain :math:`L_x\times L_y \times L_z`. For the test, a relativly large
spatial domain (:math:`L = 10\pi`) is chosen, such that the spreading distribution does not effect the result.
The point particle trajectory is calculated from the distribution function through the center of mass.

.. math::
  \langle\vec r\rangle(t) = \int \vec r \left(\int f(\vec r, \vec v, t) \mathrm d \vec v\right) \mathrm d \vec r.

(ToDo give literature or link for the correct equation of motion.)

Computation of electric field
-----------------------------
In order to test the signs involved in the computation of the electric field, a distribution
function with a density perturbation is initialized

.. math::
  f(\vec r, \vec v) = \frac 1 {(2\pi)^{3/2}}  \sin ( x + y +z) e^{-\frac {|v|^2}{2}}\\
  \rho(\vec r) = q \cdot \int f(\vec r, \vec v) \mathrm d \vec r = \sin ( x + y +z).
  
and the electric field should be

.. math::
  \vec E(\vec r) = \left(\begin{array}{c}
  1\\1\\1
  \end{array}\right) \cos(x+y+z).

To test, confirm that :math:`E_x(0,0,0) = 1`, :math:`E_y(0,0,0) = 1` and :math:`E_z(0,0,0) = 1`.\
(This test does not really well fits into this test suit. Move it to a different test suit.)

Advection direction
-------------------
In the next test, we want to confirm the signs in the velocity advection. For this purpose a distribution function with a
initial mean velocity unequal zero is initialized

.. math::
    f(\vec r, \vec v) = f_r(\vec r) \cdot \dfrac {e^{-\frac{1}{2}\left((v_x-1)^2+(v_y-2)^2+(v_z-3)^2\right)} } {(2\pi)^{3/2}} ,

and compute the advection in absence of an electric or magnetic field

.. math::
   \vec B = \vec E = \left(\begin{array}{c} 0 \\ 0 \\ 0 \end{array} \right).

This corresponds to a free streaming example.\
The point particle should follow the trajectory

.. math::
    \vec r(t) = t \cdot \vec v_{\text{mean}} = t \cdot \left(\begin{array}{c} 1 \\ 2 \\ 3 \end{array}\right)


and after one time unit (:math:`t=1`), we expect

.. math::
  \langle \vec r \rangle = (L_x/2+1,L_y/2+2,L_z/2+3).
  

:math:`\vec E \times \vec B` - Drift velocity
----------------------------------------------
In order to test the direction of the :math:`\vec E \times \vec B` velocity, the distrubution is initialized using the
following Gauss

.. math::
  f(\vec r, \vec v) = f_r(\vec r) \cdot \frac {e^{-\dfrac{1}{2} \left(v_x^2+v_y^2+v_z^2\right)} } {(2\pi)^{3/2}}.

For this condition, the trajectory of the point particle is given by

.. math::
  X(t) &= L_x/2 \cdot \sin(\omega_{ci} t),\\
  Y(t) &= L_y/2 \cdot (1-\cos(\omega_{ci} t)) - t \\
  Z(t) &= L_z/2

with the gyro frequency :math:`\omega_{ci}= \dfrac{q B_0}{m}`.

For the test, the simulation if performed till :math:`t=2\pi` and the center of mass should be at

.. math::
  \langle \vec r \rangle = (L_x/2,L_y+2\pi,L_z/2).

  

Gyromotion of Particles
-----------------------
In the last test, the distribution function is initialized with a mean velocity in x-direction

.. math::
  f(\vec r, \vec v) = f_r(\vec r) \cdot  \frac {e^{-\dfrac{1}{2} \left((v_x-1)^2+v_y^2+v_z^2\right)} } {(2\pi)^{3/2}}.

The particle trajectory is calculated through the `Lorentz Force`_ and given by

.. math::
  X(t) &= L_x/2 \cdot \sin(\omega_{ci} t) \\
  Y(t) &= L_y/2 \cdot (1-\cos(\omega_{ci} t)) \\
  Z(t) &= L_z/2

with the gyro frequency :math:`\omega_{ci}= \dfrac{q B_0}{m}`
For the test, the expectation value is computed at :math:`T = \pi`. At this time, the particles have performed a half
oscillation and we expect

.. math::
  \langle \vec r \rangle = (L_x/2,L_y/2-2,L_z/2).
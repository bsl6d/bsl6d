"""
Download and extract the bsl6dDocStorage which contains pictures
of the documentation.

https://datashare.mpcdf.mpg.de/s/XQmHImLvI0wobSf

If the directory for the bsl6dDocStorage already exists it is 
deleted and newly created.
If a link to the directoy exists, the data is overwritten but
not skipped.
"""
import os
import shutil
import subprocess
import warnings

def doc_storage_dir_name():
    return "./bsl6dDocStorage"

def link_to_bsl6dDocStorage_exists():
    return (os.path.islink(doc_storage_dir_name()))

def download_bsl6dDocStorage_directory():
    subprocess.run(r'wget --no-check-certificate --content-disposition '\
                   r'-O bsl6dDocStorage.zip "https://datashare.mpcdf.mpg.de/s/XQmHImLvI0wobSf/download"',\
                    shell=True, check=True)

def extract_bsl6dDocStorage_directory():
    if(os.path.exists(doc_storage_dir_name())):
        shutil.rmtree(doc_storage_dir_name())
    subprocess.run("unzip -n "+doc_storage_dir_name()+".zip",shell=True, check=True)
        
def main(sourceDir):
    currentDir = os.getcwd()
    os.chdir(sourceDir)
    if(not os.path.exists('./_static/')):
        os.makedirs("./_static/")
    os.chdir("./_static")
    if(not link_to_bsl6dDocStorage_exists()):
        download_bsl6dDocStorage_directory()
        extract_bsl6dDocStorage_directory()
    else:
        warnings.warn("Download and extraction of documentation storage directory is skipped"\
                      "because a link '"+doc_storage_dir_name()+"' already exists!")
    os.chdir(currentDir)
    
    
if __name__ == 'main':
    main("./source")
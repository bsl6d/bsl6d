"""
Convert all tex-files into rst-files 

Some files of the documentation are written using Latex. 
This script:
- searches for all tex-files
- converts them to rst-files using pandoc
- ensures that the created rst-file is added to the toctree
  of the index.rst in the directory where the tex-file is
  located
"""
import os
import subprocess
import re

def convert_latex_to_rst_using_pandoc(texFile):
    subprocess.check_call("pandoc --from=latex --to=rst --bibliography ./Bibliographie.bib --citeproc -o " + texFile[:-4] + ".rst " + texFile, shell=True)

def check_if_toctree_contains_rst_file(dir, texFile):
    with open(dir+"index.rst") as indexFile:
        index = indexFile.readlines()
    fileFound = False
    for line in index:
        if(re.match((r"^\s*"+texFile[:-4]+"\.rst"), line)):
            fileFound = True
    if(not fileFound):
        raise KeyError("The toctree of file " + dir + "/index.rst does not "
                       "refrences the file " + texFile + ". Add "+ texFile[:-4] +
                       ".rst to the toctree.")
        
def main(sourceAbsPath):
    cwd = os.getcwd()
    os.chdir(sourceAbsPath)
    for root, dirs, files in os.walk(sourceAbsPath):
        if('_static' in dirs):
            dirs.remove('_static')
        for texFile in files:
            if(texFile.endswith(".tex")):
                convert_latex_to_rst_using_pandoc(root + "/" + texFile)
                check_if_toctree_contains_rst_file(root + "/",texFile)
    os.chdir(cwd)

if __name__ == '__main__':
    main(os.getcwd()+'/')


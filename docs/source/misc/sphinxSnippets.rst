Sphinx Environment Snippets
===========================

.. _Sphinx Homepage: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _list-table: https://docutils.sourceforge.io/docs/ref/rst/directives.html#list-table

Mathematical formulas are pure LaTex code in the Sphinx documentation. Therefore formulas
can be written using standard math packages.

.. math:: 
  F(\omega) &= \frac 1 {2\pi} \int f(t) e ^{i \omega t} \mathrm d t\\
  a &= \left (
  \begin{array}{c}
    4 \\4
  \end{array}\right)

\

Tables can either be implemented through grid table syntax\

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | ...        | ...      |          |
+------------------------+------------+----------+----------+

or through `list-table`_ syntax.
\

Example Code can be written using the following environment

.. code-block:: cpp
    :linenos:

    double my_function(int const a, std::vector<double> const &b){
        std::for_each(b.begin(),b.end(),[&](double bi){
            bi += a;
            });
    }


.. code-block:: python
   :emphasize-lines: 3,5

   def some_function():
       interesting = False
       print 'This line is highlighted.'
       print 'This one is not...'
       print '...but this one is.'

More details on how to write documentation using reStructuredText markup language can
be found on the `Sphinx Homepage`_ or on the official Documentation on `reStructuredText`_
markup syntax.

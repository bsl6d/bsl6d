.. _`chapter:NetZeroChargeTransportAlgorithm`:

An algorithm for net-zero charge transport between flux surfaces
================================================================

This document motivates a field equation to determine the electric field
:math:`\textbf{E}(\textbf{x},t)` in the Vlasov equation and derives a
numerical algorithm to maintain a constant flux surface average combined
with a quasineutrality condition and adiabatic electrons. Following Chen
(2016, sec. 2.1)

*A plasma is a quasineutral gas of charged and neutral particles which
exhibits collective behavior.*

It is important to note that a plasma must consist of at least two
particle species with opposite charges to enable the quasineutrality
property.

The Vlasov equation describes the evolution of particle distribution in
an up to 6-D phase space build-up from the configuration and velocity
space. The forces in a Vlasov equation are determined through
electromagnetic fields. A full two-species Vlasov–Maxwell simulation
would require the solution of two 6-D distribution functions described
by the Vlasov equation and the solution of Maxwell’s equation. Solving
such a model introduces multiple numerical and computational challenges.
We can reduce the complexity of the model by considering only one
particle species in the Vlasov equation. Maxwell’s equation can be
reduced by implicitly taking the electrostatic limit and embedding the
second species’ behavior to the electrostatic field. In this chapter, we
discuss such a reduced model.

The Vlasov equation simulates one particle species’ (ions) behavior, and
the second species’ behavior (electrons) is simulated implicitly.
`1.1 <#sec:AdiabaticElectronsAndQuasiNeutrality>`__ introduces a model
to simulate kinetic ions with adiabatic electrons, assuming
quasineutrality. We then extend the model by restricting the transport
properties of ions within the plasma. The dispersion relation of this
model is derived in `1.5 <#sec:IonBernsteinWavesDispersionRelation>`__.

After analytical considerations, we develop a numerical algorithm, which
introduces the new transport properties into the integrators for the
Vlasov equation described in (ToDo: Add and link documentation on
integrators). The algorithm will be verified against an equilibrium
state and the previously derived dispersion relation.

The considered Vlasov equation is given by

.. math::

   \begin{aligned}
       \label{num:equ:VlasovEquation}
       \partial_t f(\textbf{x},\textbf{v},t) + \textbf{v}\cdot \nabla_\textbf{x}f(\textbf{x},\textbf{v},t) + \dfrac{q}{m}(\textbf{E}(\textbf{x},t) + \textbf{v}\times \textbf{B}_0)\cdot\nabla_\textbf{v}f(\textbf{x},\textbf{v},t) = 0.
   \end{aligned}

and in the rotating frame the Vlasov equation has the form

.. math::

   \begin{aligned}
       \label{num:equ:VlasovEquationRotatingFrame}
       \partial_\tau f(\textbf{x},\tilde{\textbf{v}},\tau) + (\textbf{D}_{\omega_c}^{-1}(\tau)\tilde{\textbf{v}})\cdot \nabla_\textbf{x}f(\textbf{x},\tilde{\textbf{v}},\tau) + \left(\textbf{D}_{\omega_c}(\tau)\dfrac{q}{m}\textbf{E}(\textbf{x},\tau)\right)\cdot \nabla_{\tilde{\textbf{v}}} f(\textbf{x},\tilde{\textbf{v}},\tau) = 0.
   \end{aligned}

The notation will be simplified as this document continues. The tilde
and :math:`\tau` will no longer be used to highlight the moving velocity
mesh.

.. _`sec:AdiabaticElectronsAndQuasiNeutrality`:

Adiabatic electron models and quasineutrality
---------------------------------------------

This section derives a plasma model for kinetic ions with adiabatic
electrons, assuming quasineutrality. The model is derived by determining
the electrostatic potential through an equilibrium solution for the
Vlasov equation of electrons. The quasineutrality assumption will couple
this potential to the solution of the Vlasov equation describing the
evolution of the ion distribution function. Our model considers an
electrostatic setup with a constant background magnetic field.

We start from the assumption that electrons are arranged such that they
provide a solution for the stationary Vlasov equation. This can be
justified with the assumption that electrons move faster compared to
ions due to the mass ratio

.. math::

   \begin{aligned}
       \label{phy:equ:StationatyVlasovEquation}
       \textbf{v}\cdot \nabla_\textbf{x}f_e(\textbf{x},\textbf{v}) - \dfrac{e}{m_e}(-\nabla_\textbf{x}\phi(\textbf{x}) + \textbf{v}\times\textbf{B}_0)\cdot\nabla_\textbf{v}f_e(\textbf{x},\textbf{v}) = 0
   \end{aligned}

where :math:`e` is the elementary charge, :math:`m_e` the electron mass.
We solve the equation by separating variables for :math:`\textbf{x}` and
:math:`\textbf{v}`. The velocity distribution is assumed to be
Maxwellian normalized with the factor :math:`\mathcal{N}`

.. math::

   \begin{aligned}
       f_e(\textbf{x},\textbf{v}) &= f_{e,x}(\textbf{x})f_{e,M}(v)\\
                   &= f_{e,x}(\textbf{x}) \dfrac{1}{\mathcal{N}}\exp\left(-\dfrac{v^2}{2v_{e,\text{th}}^2}\right)
   \end{aligned}

Inserting this ansatz into
`[phy:equ:StationatyVlasovEquation] <#phy:equ:StationatyVlasovEquation>`__
the rotational term vanishes since
:math:`\nabla_\textbf{v}f_{e,M} \perp \textbf{v}`, and we obtain

.. math::

   \begin{aligned}
       \left[\textbf{v}\cdot \nabla_\textbf{x}f_e(\textbf{x}) - \dfrac{e}{m_e}(-\nabla_\textbf{x}\phi(\textbf{x}))\cdot \dfrac{\textbf{v}}{v_{e,\text{th}}^2} f_{e,x}(\textbf{x})\right]f_{e,M}(v) = 0.
   \end{aligned}

For :math:`f_{e,x}(\textbf{x})` we use the Boltzmann
distribution(Lifshitz, Pitaevskii, and Landau 1981, sec. 36), which
completes the equilibrium electron distribution function

.. math::

   \begin{aligned}
       f_e(\textbf{x},\textbf{v}) = n_{e,0} e^{-e\phi(\textbf{x})/(m_e v_{e,\text{th}}^2)} f_{e,M}(v).
   \end{aligned}

From the particle distribution function, we calculate the particle
density. In the second line, we expanded the exponential

.. math::

   \begin{aligned}
       n_e(\textbf{x}) &= \int f_e(\textbf{x},\textbf{v}) \mathrm{d}^3 v = n_{e,0} e^{-e\phi(\textbf{x})/(m_e v_{e,\text{th}}^2)}\\
       \label{phy:equ:AdiabaticElectronDensity}
       &= n_{e,0} \left(1 - \dfrac{e}{m_e v_{e,\text{th}}^2} \phi(\textbf{x})\right) + O\left(\left(\dfrac{e \Vert \phi(\textbf{x}) \Vert}{m_e v_{e,\text{th}}^2}\right)^2\right).
   \end{aligned}

Secondly, we have to solve the electrostatic problem. We start with the
Poisson equation. The quasineutrality assumption is equivalent to the
limit :math:`\lim \epsilon_0 \rightarrow 0` for the vacuum permittivity
shown by Scott (2021, sec. 6.4). We can use this assumption to simplify
the Poisson equation. This results in the quasineutrality assumption,
which requires ion and electron particle density to be equivalent [1]_

.. math::

   \begin{aligned}
       \label{phy:equ:QuasiNeutralityAssumption}
       \Delta \phi(\textbf{x}) = -\dfrac{\rho(\textbf{x})}{\epsilon_0} = -\dfrac{e(n_i(\textbf{x}) - n_e(\textbf{x}))}{\epsilon_0} \underset{\lim \epsilon_0 \rightarrow 0}{\Rightarrow} n_i(\textbf{x}) = n_e(\textbf{x}).
   \end{aligned}

We combine
`[phy:equ:AdiabaticElectronDensity,phy:equ:QuasiNeutralityAssumption] <#phy:equ:AdiabaticElectronDensity,phy:equ:QuasiNeutralityAssumption>`__
to determine the electrostatic potential.

We start with a quasineutrality assumption

.. math::

   \begin{aligned}
       n_i(\textbf{x}) &= n_e(\textbf{x}).
   \end{aligned}

The ion particle density :math:`n_i(\textbf{x})` is the quantity that is
explicitly simulated in our setup and a known quantity. We split
:math:`n_i(\textbf{x})` into a background and a perturbation
:math:`n_i(\textbf{x}) = n_{i,0} (1 - \delta n_i(\textbf{x}))`.
Inserting the ion density ansatz and the adiabatic electron assumption
of
`[phy:equ:AdiabaticElectronDensity] <#phy:equ:AdiabaticElectronDensity>`__
for the electron particle density into the previous equation returns

.. math::

   \begin{aligned}
       n_{i,0} (1 - \delta n_i(\textbf{x})) &= n_{e,0}\left(1 - \dfrac{e}{m_e v_{e,\text{th}}^2} \phi(\textbf{x})\right).
   \end{aligned}

The quasineutrality assumption can be solved for the electrostatic
potential

.. math::

   \begin{aligned}
       \label{phy:equ:QuasineutralElectrostaticPotential}
       \delta n_i(\textbf{x}) &= \dfrac{e}{m_e v_{e,\text{th}}^2} \phi(\textbf{x}).
   \end{aligned}

Finally, the system for fully kinetic, singly charged ions with a
constant magnetic background field and assuming an electrostatic
potential that is determined by adiabatic electrons and quasineutrality
with
`[phy:equ:QuasineutralElectrostaticPotential] <#phy:equ:QuasineutralElectrostaticPotential>`__,
is given by

.. math::

   \begin{aligned}
       \label{phy:equ:FullKineticIonsAdiabElectronsQuasiNeutral}
       \begin{cases}
           \frac{\partial}{\partial t} f(\textbf{x},\textbf{v}, t) + \textbf{v}\cdot \nabla_\textbf{x}f(\textbf{x},\textbf{v}, t) + \dfrac{e}{m} (-\nabla_\textbf{x}\phi(\textbf{x}, t) + \textbf{v}\times \textbf{B}_0)\cdot \nabla_\textbf{v}f(\textbf{x},\textbf{v},t)  = 0\\
           n(\textbf{x}, t) = \dfrac{e}{m_e v_{e,\text{th}}^2} \phi(\textbf{x}, t)
       \end{cases}.
   \end{aligned}

In this last formula, we dropped the subscript :math:`i` for the ions.
Furthermore, we used :math:`n(\textbf{x})` instead of
:math:`\delta n(\textbf{x})` to determine the electrostatic potential.
Since we assumed that the background :math:`n_{i,0}` is constant in
space, it does not change the electrostatic force
:math:`-\nabla_\textbf{x}\phi(\textbf{x},t)`. This notation will be kept
in the following sections.

.. _`sec:NetZeroChargeTransport`:

Net-zero charge transport between flux surfaces
-----------------------------------------------

After we have presented a well-known reduced model for plasma
simulations, we add a small extension to the model in
`1.1 <#sec:AdiabaticElectronsAndQuasiNeutrality>`__.
Eq. `[phy:equ:FullKineticIonsAdiabElectronsQuasiNeutral] <#phy:equ:FullKineticIonsAdiabElectronsQuasiNeutral>`__
has a rotational symmetry in the :math:`x`–:math:`y` plane, which does
not appropriately replicate the behavior of a magnetically confined
plasma. In a magnetically confined quasineutral plasma, the net charge
transport should be zero between flux surfaces as described by Scott
(2021, 16–22). That means the average particle density on this kind of
surface needs to be constant over time, which is expressed by

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxSurfaceAverageCondition}
       \frac{\partial}{\partial t} \langle n(\textbf{x}, t) \rangle_\text{F} = 0.
   \end{aligned}

This property is not included in
`[phy:equ:FullKineticIonsAdiabElectronsQuasiNeutral] <#phy:equ:FullKineticIonsAdiabElectronsQuasiNeutral>`__
and is the central new behavior introduced in this section. We motivate
the new equations, which will add the new property to the system.
Additionally, we can derive the necessary conditions that have to be met
when developing an algorithm for a constant flux surface average.

define a magnetic flux surface as a surface traced out by magnetic field
lines. With our magnetic background field
:math:`\textbf{B}_0 = B_0 \hat{e}_z`, all surfaces parallel to the
:math:`z` dimension are flux surfaces. In a fusion reactor device, flux
surfaces are distinct. We now define the flux surfaces parallel to the
:math:`y`–:math:`z` plane to extend our model. Accordingly, the flux
surface average is given by

.. math::

   \begin{aligned}
       \label{phy:equ:FluxSurfaceAverage}
       \langle \cdot \rangle_\text{F} = \dfrac{1}{L_y L_z} \int \cdot~ \mathrm{d}y\mathrm{d}z.
   \end{aligned}

We maintain a net-zero particle transport between flux surfaces,
redefining the force introduced by the electrostatic potential. The new
electrostatic potential cancels all forces which would change
:math:`\langle n(\textbf{x},t) \rangle_\text{F}`. The electrostatic
potential is split into a potential free of the flux surface average and
the flux surface average

.. math::

   \begin{aligned}
       \phi(\textbf{x}) = \tilde{\phi}(\textbf{x}) + \langle \phi \rangle_\text{F}(x)
   \end{aligned}

with

.. math::

   \begin{aligned}
       \langle \tilde{\phi}(\textbf{x}) \rangle_\text{F} = 0.
   \end{aligned}

Only :math:`\tilde{\phi}(\textbf{x})` is determined with the
quasineutrality condition with adiabatic electrons described in the last
section. This potential does not change the flux surface average of the
density. We can subtract the flux surface average from the density in
`[phy:equ:QuasineutralElectrostaticPotential] <#phy:equ:QuasineutralElectrostaticPotential>`__
to determine :math:`\tilde{\phi}(\textbf{x})`

.. math::

   \begin{aligned}
       \label{phy:equ:FieldEquationFluxSurfaceAverage}
       \dfrac{e}{m_e v^2_{e,\text{th}}}\tilde{\phi}(\textbf{x}) &= n(\textbf{x}) - \langle n \rangle_\text{F}(x)\\
                                                           &= \dfrac{e}{m_e v^2_{e,\text{th}}}(\phi(\textbf{x}) - \langle \phi \rangle_\text{F}(x))
   \end{aligned}

The second part of the potential, namely its flux surface average
:math:`\langle \phi \rangle_\text{F}(x)`, shall remove changes of
:math:`\langle n(\textbf{x},t) \rangle_\text{F}` over time. The latter
force may only act on the flux surface average of the particle density
:math:`\langle n(\textbf{x},t) \rangle_\text{F}`.

Using the electrostatic potential from
`[phy:equ:FieldEquationFluxSurfaceAverage] <#phy:equ:FieldEquationFluxSurfaceAverage>`__
and the net-zero charge transport condition from
`[phy:equ:ConstantFluxSurfaceAverageCondition] <#phy:equ:ConstantFluxSurfaceAverageCondition>`__,
we can define the system we are interested in

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgSystemOfEqu}
       \begin{cases}
           &\frac{\partial}{\partial t} f(\textbf{x},\textbf{v},t) + \textbf{v}\cdot \nabla_\textbf{x}f(\textbf{x},\textbf{v},t) + \dfrac{q}{m} \left((-\nabla_\textbf{x}\phi(\textbf{x}, t)) + \textbf{v}\times \textbf{B}_0\right) \cdot \nabla_\textbf{v}f(\textbf{x},\textbf{v},t) = 0\\
           &\dfrac{e}{m_e v^2_{e,\text{th}}} \left(\phi(\textbf{x}, t) - \langle \phi(\textbf{x}, t) \rangle_\text{F}\right) = n(\textbf{x},t) - \langle n \rangle_\text{F}(x)\\
           &\frac{\partial}{\partial t}\langle n(\textbf{x},t) \rangle_\text{F} = 0
       \end{cases}.
   \end{aligned}

Suppose we want to guarantee a long-term net-zero charge transport
between flux surfaces. In that case, we must fulfill the continuity
equation obtained by integrating the Vlasov equation in
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__
on :math:`\textbf{v}`. The force term vanishes using Gauss’s theorem,
which gives us the continuity equation

.. math::

   \begin{aligned}
       \frac{\partial}{\partial t} n(\textbf{x}, t) + \nabla_\textbf{x}\cdot \textbf{j}(\textbf{x},t) = 0.
   \end{aligned}

Taking the flux surface average of the continuity equation, we obtain

.. math::

   \begin{aligned}
       \frac{\partial}{\partial t} \langle n(\textbf{x}, t) \rangle_\text{F} + \langle \nabla_\textbf{x}\cdot \textbf{j}(\textbf{x},t) \rangle_\text{F} = 0.
   \end{aligned}

If we maintain a constant flux surface average according to
`[phy:equ:ConstantFluxSurfaceAverageCondition] <#phy:equ:ConstantFluxSurfaceAverageCondition>`__,
the flux surface average of the current density has to be zero to
fulfill the continuity equation

.. math::

   \begin{aligned}
       \label{phy:equ:ZeroParticleFluxDivergence}
       \langle \nabla_\textbf{x}\cdot \textbf{j}(\textbf{x}, t) \rangle_\text{F} = 0,
   \end{aligned}

with the particle flux
:math:`\textbf{j}(\textbf{x},t)=\int \textbf{v}f(\textbf{x},\textbf{v},t) \mathrm{d}^3v`.
This condition is intuitive if we consider that a particle current would
immediately drive a change in the particle density. The two properties
in
`[phy:equ:ConstantFluxSurfaceAverageCondition,phy:equ:ZeroParticleFluxDivergence] <#phy:equ:ConstantFluxSurfaceAverageCondition,phy:equ:ZeroParticleFluxDivergence>`__
will provide insights into the quality of the algorithm derived in
`1.3 <#sec:NumericalAlgorithmWithNetZeroChargeTransportBetweenFluxSurfaceAverages>`__.

.. _`sec:NumericalAlgorithmWithNetZeroChargeTransportBetweenFluxSurfaceAverages`:

A splitting with net-zero charge transport between flux surfaces
----------------------------------------------------------------

In this section, we develop an algorithm to solve
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__,
which was motivated in `1.2 <#sec:NetZeroChargeTransport>`__. The system
simulates kinetic ions with adiabatic electrons while omitting any
charge transport between flux surfaces.

The numerical integration for the kinetic ions is based on a three-step
Strang Splitting described (ToDo: add and link documentation on
splitting schemes), which separates the spatial and velocity domain
integration for the distribution function
:math:`f(\textbf{x},\textbf{v},t)`. A single split step of the
integrator is based on the semi-Lagrangian method in (ToDo: add and link
documentation on rotating grid), which integrates the distribution
function by interpolating based on a shift given through the
characteristic equations. We derive the algorithm with a semi-discrete
approach, which only discretizes the time step and not the phase space.
We denote the integrator with the following three steps from time
:math:`t_0` to :math:`t_0 + h` while the asterisks denote intermediate
steps of the splitting. The interpolation shifts for the semi-Lagrangian
method are given by
:math:`\textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h\right)` and
:math:`\textbf{c}_{\textbf{x}}\left(\textbf{v},h\right)` for the
:math:`\textbf{v}` and :math:`\textbf{x}` advection step respectively.
The integrator for kinetic ions reads

.. math::

   \begin{aligned}
       f^{*}(\textbf{x},\textbf{v})   &= f(\textbf{x},\textbf{v}+ \textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h / 2\right), t_0)\\
       \label{phy:equ:SplittingScheme}
       f^{**}(\textbf{x},\textbf{v})  &= f^{*}(\textbf{x}+ \textbf{c}_{\textbf{x}}(\textbf{v},h),\textbf{v})\\
       f(\textbf{x},\textbf{v}, t_0 + h) &= f^{**}(\textbf{x},\textbf{v}+ \textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h / 2\right)).
   \end{aligned}

In the remainder of the section, we will determine the electrostatic
potential according to
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.
The electrostatic potential is given by

.. math::

   \begin{aligned}
       \label{phy:equ:PotentialSplit}
       \phi(\textbf{x}) = \tilde{\phi}(\textbf{x}) + \langle \phi \rangle_\text{F}(x).
   \end{aligned}

Adiabatic electrons and quasineutrality are used to determine
:math:`\tilde{\phi}(\textbf{x})` according to
`[phy:equ:FieldEquationFluxSurfaceAverage] <#phy:equ:FieldEquationFluxSurfaceAverage>`__
which has zero flux surface average
:math:`\langle \tilde{\phi(\textbf{x})} \rangle_\text{F}=0`.

We now describe an approach that allows us to determine
:math:`\langle \phi(\textbf{x}) \rangle_\text{F}` such that the flux
surface average of the particle density :math:`n(\textbf{x},t)` is the
same as the electron flux surface average after applying the Strang
splitting of `[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__
once to the distribution function

.. math::

   \begin{aligned}
       \label{phy:equ:DiscreteConstantFluxSurfaceAverageCondition}
       \langle n(\textbf{x},t_0+h) \rangle_\text{F} = \langle n_e \rangle_\text{F}(x)
   \end{aligned}

where the electron flux surface average is given by
:math:`\langle n_e \rangle_\text{F}(x)`. We fix the electron flux
surface average at the beginning of a simulation using the initial
condition :math:`f(\textbf{x},\textbf{v},0)`

.. math::

   \begin{aligned}
       \langle n_e \rangle_\text{F}(x) = \langle \int f(\textbf{x},\textbf{v},0) \mathrm{d}^3 v \rangle_\text{F},
   \end{aligned}

such that the quasineutrality condition is fulfilled at the beginning of
every simulation.

The change in the density is traced back through the splitting steps,
and the flux surface average of the potential is determined such that
the flux surface average of the density does not change. We start with
the last step of the splitting scheme, which consists of an advection in
velocity space. A pure change of the distribution function does not
change the particle density

.. math::

   \begin{aligned}
       0 &= \langle n_e \rangle_\text{F} - \langle \int f(\textbf{x},\textbf{v},t_0+h)\mathrm{d}^3v \rangle_\text{F}\\
         &= \langle n_e \rangle_\text{F} - \langle \int f^{**}(\textbf{x},\textbf{v}+ \textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h / 2\right)) \mathrm{d}^3 v \rangle_\text{F}\\
         &= \langle n_e \rangle_\text{F} - \langle \int f^{**}(\textbf{x},\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}.
   \end{aligned}

It is, therefore, sufficient to consider the first :math:`\textbf{v}`
advection and the :math:`\textbf{x}` advection to maintain a constant
flux surface average.

In the next step, we trace back the :math:`\textbf{x}`-advection of the
integrator and expand the result in a Taylor series using multi-index
notation for :math:`\textbf{i} = (i_1,i_2,i_3)` as, e.g., described by
Duistermaat and Kolk (2010, chap. 6) assuming that the shift is still
small. The derivative :math:`\partial_\textbf{x}^{\textbf{i}}` expands
to :math:`\partial_x^{i_1}\partial_y^{i_2}\partial_z^{i_3}`

.. math::

   \begin{aligned}
       0 &= \langle n_e \rangle_\text{F} - \langle \int f^{**}(\textbf{x},\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}\\
         &= \langle n_e \rangle_\text{F} - \langle \int f^{*}(\textbf{x}+ \textbf{c}_{\textbf{x}}\left(\textbf{v},h\right),\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}\\
         \label{phy:equ:BacktracedDensityFluxSurfaceAverageAfterX}
         &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\textbf{c}_{\textbf{x}}\left(\textbf{v},h\right)^{\textbf{i}}}{\textbf{i}!} \partial_\textbf{x}^{\textbf{i}} f^{*}(\textbf{x},\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}.
   \end{aligned}

The last step traces the change of the first :math:`\textbf{v}`
advection. This step is traced back to
:math:`f(\textbf{x},\textbf{v},t)` using an integral transformation
described by, e.g., Zorič (2015, Subsec. 11.5.1, Theorem I) which states
for a mapping :math:`\varphi : \Omega_x \rightarrow \Omega_u`

.. math::

   \begin{aligned}
       \int_{\Omega_x = \varphi(\Omega_u)} f(x) \mathrm{d}x = \int_{\Omega_u} f\circ \varphi(u) \text{det}(D\varphi(u))\mathrm{d}u.
   \end{aligned}

We denote with :math:`\text{det}(D \varphi(\textbf{u}))` the Jacobian
determinant. Applying the integral transformation, we obtain the
condition

.. math::

   \begin{aligned}
       0 &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\textbf{c}_{\textbf{x}}\left(\textbf{v},h\right)^{\textbf{i}}}{\textbf{i}!} \partial_\textbf{x}^{\textbf{i}} f^{*}(\textbf{x},\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}\\
         &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\textbf{c}_{\textbf{x}}\left(\textbf{v},h\right)^{\textbf{i}}}{\textbf{i}!} \partial_\textbf{x}^{\textbf{i}} f(\textbf{x},\textbf{v}+ \textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h/2\right),t_0) \mathrm{d}^3 u \rangle_\text{F}\\
         \label{phy:equ:BacktracedDensityFluxSurfaceAverage}
         &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\textbf{c}_{\textbf{x}}\left(\varphi(\textbf{u}),h/2\right)^{\textbf{i}}}{\textbf{i}!} \partial_\textbf{x}^{\textbf{i}} f(\textbf{x},\textbf{u},t_0) \text{det}(D \varphi(\textbf{u}))\mathrm{d}^3 u \rangle_\text{F}.
   \end{aligned}

The mapping :math:`\varphi(\textbf{u})` has to be chosen such that it
transforms
:math:`f(\textbf{x}, \textbf{v}+ \textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h/2\right),t)`
into :math:`f(\textbf{x},\textbf{u},t)`. The definition of
:math:`\varphi(\textbf{u})` depends on the characteristics which define
:math:`\textbf{c}_{\textbf{v}}\left(\phi(\textbf{x}),h/2\right)`.

In the following two subsections we utilize
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
to determine :math:`\langle \phi(\textbf{x}) \rangle_\text{F}` such that
we fulfill the constant flux surface average of
`[phy:equ:DiscreteConstantFluxSurfaceAverageCondition] <#phy:equ:DiscreteConstantFluxSurfaceAverageCondition>`__
in the splitting
`[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__. Furthermore,
we will utilize periodic boundary conditions in all spatial dimensions.
This removes many terms since the derivative of a periodic function
:math:`f(x) = f(x + L)` vanishes in the integral on the periodic domain

.. math::

   \begin{aligned}
       \label{phy:equ:VanishingPeriodicIntegral}
       \int_0^L \partial_x f(x) \mathrm{d}x = f(L) - f(0) = 0.
   \end{aligned}

The constant flux surface average shall be numerically accurate globally
with :math:`O(h^2)`. Therefore, we need an accuracy :math:`O(h^3)` for
the flux surface average in every step

.. math::

   \begin{aligned}
       O(h^3) = \langle n_e \rangle_\text{F} - \langle n(\textbf{x}, t_0+h) \rangle_\text{F}.
   \end{aligned}

Since :math:`\textbf{c}_{\textbf{x}}` and
:math:`\textbf{c}_{\textbf{v}}` are both :math:`O(h)` we have to
consider the following list of multi-indices :math:`\textbf{i}` to
collect all terms up to :math:`O(h^3)` in
`[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__

.. container:: footnotesize

   .. math::

      \begin{aligned}
          \label{phy:equ:MultiIndexList}
          \textbf{i} \in \left\{
              \begin{pmatrix}
              0 \\
              0 \\
              0 \\
              \end{pmatrix},
              \begin{pmatrix}
              1 \\
              0 \\
              0 \\
              \end{pmatrix}
              \cdots
              \begin{pmatrix}
              0 \\
              0 \\
              1 \\
              \end{pmatrix},
              \begin{pmatrix}
              1 \\
              1 \\
              0 \\
              \end{pmatrix}
              \cdots
              \begin{pmatrix}
              0 \\
              1 \\
              1 \\
              \end{pmatrix},
              \begin{pmatrix}
              2 \\
              0 \\
              0 \\
              \end{pmatrix}
              \cdots
              \begin{pmatrix}
              0 \\
              0 \\
              2 \\
              \end{pmatrix},
              \begin{pmatrix}
              1 \\
              1 \\
              1 \\
              \end{pmatrix},
              \begin{pmatrix}
              2 \\
              1 \\
              0 \\
              \end{pmatrix}
              \cdots
              \begin{pmatrix}
              0 \\
              1 \\
              2 \\
              \end{pmatrix},
              \begin{pmatrix}
              3 \\
              0 \\
              0 \\
              \end{pmatrix}
              \cdots
              \begin{pmatrix}
              0 \\
              0 \\
              3 \\
              \end{pmatrix}
          \right\}
      \end{aligned}

.. _`subsec:NetZeroChargeTransportPhiFluxAvgRotGrid`:

Net-zero charge transport on the rotating grid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this subsection, we use the same approach as in the last subsection
to derive an equation for :math:`\langle \phi \rangle_\text{F}(x,t_0)`
to fulfill
`[phy:equ:DiscreteConstantFluxSurfaceAverageCondition] <#phy:equ:DiscreteConstantFluxSurfaceAverageCondition>`__.
We solve
`[phy:equ:BacktracedDensityFluxSurfaceAverageAfterX,phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverageAfterX,phy:equ:BacktracedDensityFluxSurfaceAverage>`__
explicitly for the integrator, which integrates the distribution
function in phase space as described in (ToDo: Add and link
documentation on the integrator on a rotating grid). According to (ToDo:
Add and Link documentation on characteristics for the Vlasov equation on
the rotating grid), the shifts from the characteristics in the
semi-Lagrangian method are given by

.. math::

   \begin{aligned}
       \textbf{c}_\textbf{x}(\textbf{v}, h) &= \int_{t_0 + h}^{t_0} \textbf{D}_{\omega_c}^{-1}(t) \textbf{v}\mathrm{d}t =: \textbf{B}^{T}(h) \textbf{v}\\
       \textbf{c}_\textbf{v}(\phi(\textbf{x}), h) &= \int_{t_0 + h/2}^{t_0} \textbf{D}_{\omega_c}(t) \dfrac{q}{m} (-\nabla\phi(\textbf{x})) \mathrm{d}s =: \textbf{B}(h/2)\dfrac{q}{m} (-\nabla\phi(\textbf{x}))
   \end{aligned}

where :math:`\textbf{D}_{\omega_c}(t)` is a matrix which rotates the
velocity domain according to (ToDo: Add and link documentation on
rotating frame.) and :math:`T` indicates the transposed matrix. Both
shifts result in a linear translation of the distribution function. We
first insert :math:`\textbf{c}_\textbf{x}(\textbf{v}, h)` into the
expansion
`[phy:equ:BacktracedDensityFluxSurfaceAverageAfterX] <#phy:equ:BacktracedDensityFluxSurfaceAverageAfterX>`__
which gives

.. math::

   \begin{aligned}
       0 &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\left(\textbf{B}^{T}(h) \textbf{v}\right)^{\textbf{i}}}{\textbf{i}!}\partial^{\textbf{i}}_\textbf{x}f\left(\textbf{x}, \textbf{v}+ \textbf{B}(h/2)\dfrac{q}{m}(-\nabla\phi(\textbf{x})), t_0\right) \mathrm{d}^3 v \rangle_\text{F}
   \end{aligned}

In the second step, we transform the integral to finally express the
particle density at :math:`t_0 + h` with the distribution function
:math:`f(\textbf{x},\textbf{v},t_0)`. The mapping
:math:`\varphi(\textbf{u})` in
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
to transform the integral is given by

.. math::

   \begin{aligned}
       \begin{cases}
           \varphi &: \mathbb{R}^3 \rightarrow \mathbb{R}^3\\
           \varphi(\textbf{u}) &= \textbf{u} - \textbf{B}(h/2) \dfrac{q}{m} (-\nabla \phi(\textbf{x}))
       \end{cases}
   \end{aligned}

such that the expansion
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
on the rotating grid reads as

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgExpansionRotatingDomain}
       0 &=\langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\left(\textbf{B}^{T}(h) (\textbf{u} - \textbf{B}(h/2)\dfrac{q}{m}(-\nabla \phi(\textbf{x})))\right)^{\textbf{i}}}{\textbf{i}!} \partial^{\textbf{i}}_\textbf{x}f(\textbf{x}, \textbf{u}, t_0) \mathrm{d}^3 u \rangle_\text{F}
   \end{aligned}

The following estimates will turn out useful to simplify the expansion

.. math::

   \begin{aligned}
       \label{phy:equ:ApproxBByH}
       \textbf{B}(h) = O(h)
   \end{aligned}

and

.. math::

   \begin{aligned}
       \label{phy:equ:ApproxBInvByH}
       \textbf{B}^{T}(h) = O(h)
   \end{aligned}

As in the last subsection, this expression is evaluated to determine
:math:`\langle \phi \rangle_\text{F}(\textbf{x}, t_0)` up to
:math:`O(h^3)` using all multi-indices given in
`[phy:equ:MultiIndexList] <#phy:equ:MultiIndexList>`__.

Zero order terms :math:`\vert \textbf{i} \vert = 0`
'''''''''''''''''''''''''''''''''''''''''''''''''''

The term for :math:`\textbf{i} = (0,0,0)` only considers the particle
density. It is given by

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder0}
       \langle \int f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} = \langle n_i(\textbf{x},t_0) \rangle_\text{F}
   \end{aligned}

which is the same term as for the last subsection.

First order terms :math:`\vert \textbf{i} \vert = 1`
''''''''''''''''''''''''''''''''''''''''''''''''''''

The terms :math:`((1,0,0);(0,1,0);(0,0,1))` of the expansion contain
:math:`O(h)` and :math:`O(h^2)` orders according to
`[phy:equ:ApproxBByH,phy:equ:ApproxBInvByH] <#phy:equ:ApproxBByH,phy:equ:ApproxBInvByH>`__.
Therefore, we have to take the whole term into account. In the second
line, we evaluate the distribution function for the velocity moments
according to (ToDo: Add and link documentation on velocity moments). We
split the potential according to
`[phy:equ:PotentialSplit] <#phy:equ:PotentialSplit>`__ in the third step

.. math::

   \begin{aligned}
       &  &&\langle \int \textbf{B}^{T}(h)\left(\textbf{u} - \textbf{B}(h/2)\dfrac{q}{m}(-\nabla\phi(\textbf{x}))\right) \cdot \nabla f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F}\\
       &= &&\langle \nabla \cdot \textbf{B}^{T}(h)\textbf{j}(\textbf{x}, t_0) \rangle_\text{F} + \langle \textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\phi(\textbf{x})\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F}\\
       &= &&\langle \nabla \cdot \textbf{B}^{T}(h)\textbf{j}(\textbf{x}, t_0) \rangle_\text{F} + \langle \textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\tilde{\phi}(\textbf{x})\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F}\\
       \label{phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder1}
       &  &&+ \textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\langle \phi \rangle_\text{F}(x)\right] \cdot \nabla \langle n(\textbf{x},t_0) \rangle_\text{F}.
   \end{aligned}

Second order terms :math:`\vert \textbf{i} \vert = 2`
'''''''''''''''''''''''''''''''''''''''''''''''''''''

The second order terms of the expansion are given by the multi-indices
:math:`(1,1,0),\cdots,(0,1,1),(2,0,0),\cdots,(0,0,2)` contain
:math:`O(h^2)` to :math:`O(h^4)` orders. The second-order terms can be
represented using Matrix-Vector multiplications. The matrix
:math:`\textbf{H}_f(\textbf{x},t_0) = (\partial_{x_i}\partial_{x_j} f(\textbf{x},\textbf{v},t_0))_{i,j}`
is the Hessian matrix. We omit orders higher than :math:`O(h^3)` in the
second line using
`[phy:equ:ApproxBByH,phy:equ:ApproxBInvByH] <#phy:equ:ApproxBByH,phy:equ:ApproxBInvByH>`__.
Next, we split the potential according to
`[phy:equ:PotentialSplit] <#phy:equ:PotentialSplit>`__. Finally, we
evaluate velocity moments according to (ToDo: Add and link documentation
on velocity moments).

.. math::

   \begin{aligned}
       &  & \dfrac{1}{2} \langle\int &\left\{\textbf{B}^{T}(h)\left[\textbf{u} - \textbf{B}(h/2)\dfrac{q}{m}(-\nabla\phi(\textbf{x}))\right]\right\}^T \textbf{H}_f(\textbf{x}) \\
       &  &  &\left\{\textbf{B}^{T}(h)\left[\textbf{u} - \textbf{B}(h/2)\dfrac{q}{m}(-\nabla\phi(\textbf{x}))\right]\right\} \mathrm{d}^3v\rangle_\text{F}\\
       &=  & \dfrac{1}{2} \langle \int &(\textbf{B}^{T}(h)\textbf{u})^T \textbf{H}_f(\textbf{x})(\textbf{B}^{T}(h)\textbf{u}) \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &(\textbf{B}^{T}(h)\textbf{u})^T \textbf{H}_f(\textbf{x}) \left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\phi(\textbf{x})\right] \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &\left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\phi(\textbf{x})\right]^T \textbf{H}_f(\textbf{x}) (\textbf{B}^{T}(h)\textbf{u}) \mathrm{d}^3v \rangle_\text{F} + O(h^4)\\
       &=  & \dfrac{1}{2} \langle \int &(\textbf{B}^{T}(h)\textbf{u})^T \textbf{H}_f(\textbf{x})(\textbf{B}^{T}(h)\textbf{u}) \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &(\textbf{B}^{T}(h)\textbf{u})^T \textbf{H}_f(\textbf{x}) \left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\tilde{\phi}(\textbf{x})\right] \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &\left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\tilde{\phi}(\textbf{x})\right]^T \textbf{H}_f(\textbf{x}) (\textbf{B}^{T}(h)\textbf{u}) \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &(\textbf{B}^{T}(h)\textbf{u})^T \textbf{H}_f(\textbf{x}) \left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\langle \phi \rangle_\text{F}(x)\right] \mathrm{d}^3v \rangle_\text{F}\\
       &  &  + \dfrac{1}{2}\langle \int &\left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}\nabla\langle \phi \rangle_\text{F}(x)\right]^T \textbf{H}_f(\textbf{x}) (\textbf{B}^{T}(h)\textbf{u}) \mathrm{d}^3v \rangle_\text{F} + O(h^4)\\
       &= & \dfrac{1}{2}\langle \partial_x^2&(\textbf{B}^{T}(h) \textbf{S}(\textbf{x},t_0) \textbf{B}^{T}(h))_{1,1} \rangle_\text{F} + \langle \nabla(\nabla\cdot\textbf{B}^{T} \textbf{j}(\textbf{x},t_0)) \cdot (\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\tilde{\phi}(\textbf{x})) \rangle_\text{F}\\
       \label{phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder2}
       &  &  + \nabla&(\nabla\cdot\textbf{B}^{T}(h) \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F}) \cdot (\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\langle \phi \rangle_\text{F}(x)) + O(h^4)
   \end{aligned}

Third order terms :math:`\vert \textbf{i} \vert = 3`
''''''''''''''''''''''''''''''''''''''''''''''''''''

Lastly, we collect the third order terms of the expansion. This
corresponds to the multi-indices
:math:`((1,1,1),(2,1,0),...,(0,1,2),(3,0,0),...(0,0,3))`.

We first consider the multi-index :math:`(1,1,1)`. In the first step, we
omit all contributions :math:`O(h^4)` and higher using
`[phy:equ:ApproxBByH,phy:equ:ApproxBInvByH] <#phy:equ:ApproxBByH,phy:equ:ApproxBInvByH>`__.
The remaining integral vanishes on a periodic domain using
`[phy:equ:VanishingPeriodicIntegral] <#phy:equ:VanishingPeriodicIntegral>`__
in the second step

.. math::

   \begin{aligned}
   {4}
       & & \langle\int &\left\{\textbf{B}^{T}(h)\left[\textbf{u}-\textbf{B}(h/2)\dfrac{q}{m}(-\nabla \phi(\textbf{x}))\right]\right\}_x\left\{\textbf{B}^{T}(h)\left[\textbf{u}-\textbf{B}(h/2)\dfrac{q}{m}(-\nabla \phi(\textbf{x}))\right]\right\}_y \\
       & &  &\left\{\textbf{B}^{T}(h)\left[\textbf{u}-\textbf{B}(h/2)\dfrac{q}{m}(-\nabla \phi(\textbf{x}))\right]\right\}_z\partial_x\partial_y\partial_z f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u\rangle_{\text{F}}\\
       &= &\langle \int &(\textbf{B}^{T}(h)\textbf{u})_x (\textbf{B}^{T}(h)\textbf{u})_y (\textbf{B}^{T}(h)\textbf{u})_z \partial_x\partial_y\partial_z f(\textbf{x}, \textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} + O(h^4)\\
       &= 0& + O(&h^4).
   \end{aligned}

We can remove the multi-indices
:math:`(2,1,0),...,(0,1,2),(0,3,0),(0,0,3)` using the same arguments.

The remaining the multi-index is :math:`(3,0,0)`

.. math::

   \begin{aligned}
         &\dfrac{1}{6} \langle \int \left\{\textbf{B}^{T}(h)\left[\textbf{u}-\textbf{B}(h/2)\dfrac{q}{m}(-\nabla \phi(\textbf{x}))\right]\right\}_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t)\mathrm{d}^3 u \rangle_\text{F}\\
       \label{phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder3}
       = &\dfrac{1}{6} \langle \int (\textbf{B}^{T}(h)\textbf{u})_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} + O(h^4).
   \end{aligned}

.. _`par:determineFluxAvgPhi`:

Determining electrostatic potential
'''''''''''''''''''''''''''''''''''

Finally, after determining all contributions up to :math:`O(h^3)` we
obtain an equation for the flux surface average
:math:`\langle \phi \rangle_\text{F}` by inserting
`[phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder0,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder1,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder2,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder3] <#phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder0,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder1,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder2,phy:equ:ConstantFluxAvgExpansionRotatingDomainOrder3>`__
into
`[phy:equ:ConstantFluxAvgExpansionRotatingDomain] <#phy:equ:ConstantFluxAvgExpansionRotatingDomain>`__
which reads

.. math::

   \begin{aligned}
       0 = &\langle n_e \rangle_\text{F}(x) - \langle n(\textbf{x}, t_0) \rangle_\text{F}\\
           &-\langle \nabla \cdot \textbf{B}^{T}(h)\textbf{j}(\textbf{x}, t_0) \rangle_\text{F} - \langle \textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\tilde{\phi}(\textbf{x})\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F}\\
           &-\textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\langle \phi \rangle_\text{F}(x)\right] \cdot \nabla \langle n(\textbf{x},t_0) \rangle_\text{F}\\
           &-\dfrac{1}{2}\langle \partial_x^2(\textbf{B}^{T}(h) \textbf{S}(\textbf{x},t_0) \textbf{B}^{T}(h))_{1,1} \rangle_\text{F} - \langle \nabla(\nabla\cdot\textbf{B}^{T}(h) \textbf{j}(\textbf{x},t_0)) \cdot (\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\tilde{\phi}(\textbf{x})) \rangle_\text{F}\\
           &-\nabla(\nabla\cdot\textbf{B}^{T}(h) \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F}) \cdot (\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\langle \phi \rangle_\text{F}(x))\\
           &-\dfrac{1}{6} \langle \int (\textbf{B}^{T}(h)\textbf{u})_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t)\mathrm{d}^3 u \rangle_\text{F}\\
           \label{phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvgIllConditioned}
           &+O(h^4).
   \end{aligned}

As in the last paragraph of the previous subsection, our equation is
ill-conditioned such that we again use
`[phy:equ:RewriteIllConditionedDensity] <#phy:equ:RewriteIllConditionedDensity>`__
to rewrite
`[phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvgIllConditioned] <#phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvgIllConditioned>`__
to

.. math::

   \begin{aligned}
       0 = &\langle n_e \rangle_\text{F}(x) - \langle n(\textbf{x}, t_0) \rangle_\text{F}\\
           &-\langle \nabla \cdot \textbf{B}^{T}(h)\textbf{j}(\textbf{x}, t_0) \rangle_\text{F} - \nabla\cdot\langle \textbf{B}^{T}(h)\textbf{B}(h/2)\left[\dfrac{q}{m}\nabla\tilde{\phi}(\textbf{x})\right]n(\textbf{x},t_0) \rangle_\text{F}\\
           &-\nabla\cdot\left[\textbf{B}^{T}(h)\textbf{B}(h/2)\dfrac{q}{m}(\nabla\langle \phi \rangle_\text{F}(x))\langle n(\textbf{x},t_0) \rangle_\text{F}\right]\\
           &-\dfrac{1}{2}\langle \partial_x^2(\textbf{B}^{T}(h) \textbf{S}(\textbf{x},t_0) \textbf{B}^{T}(h))_{1,1} \rangle_\text{F} - \nabla \cdot \left[\langle (\nabla\cdot\textbf{B}^{T}(h) \textbf{j}(\textbf{x},t_0))(\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\tilde{\phi}(\textbf{x})) \rangle_\text{F}\right]\\
           &-\nabla\cdot\left[(\nabla\cdot\textbf{B}^{T}(h) \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F})(\textbf{B}^{T}(h)\textbf{B}(h/2)\nabla\langle \phi \rangle_\text{F}(x))\right]\\
           &-\dfrac{1}{6} \langle \int (\textbf{B}^{T}(h)\textbf{u})_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t)\mathrm{d}^3 u \rangle_\text{F}\\
           \label{phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg}
           &+O(h^4).
   \end{aligned}

We can use this equation to maintain a constant flux surface average in
the physical domain. Numerical examples are provided in
`1.4 <#sec:NumericalExamplesWithNetZeroChargeTransport>`__.

.. _`subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid`:

Net-zero charge transport on the physical domain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this subsection, we solve
`[phy:equ:BacktracedDensityFluxSurfaceAverageAfterX,phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverageAfterX,phy:equ:BacktracedDensityFluxSurfaceAverage>`__
explicitly for the integrator, which integrates the distribution
function in phase space as described in (ToDo: Add and link
documentation of Integrators using a simple strang splitting). According
to (ToDo: Add and link documentation on characteristics for the Vlasov
equation) the shifts from the characteristics in the semi-Lagrangian
method are given by

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomainShifts}
       \begin{cases}
           \textbf{c}_{\textbf{x}}(\textbf{v},h) &= -h \textbf{v}\\
           \textbf{c}_{\textbf{v}}(\phi(\textbf{x}),h/2) &= -\dfrac{h}{2}\dfrac{q}{m}((-\nabla \phi(\textbf{x})) + \textbf{v}\times \textbf{B}_0)
       \end{cases}.
   \end{aligned}

We first insert :math:`\textbf{c}_{\textbf{x}}(\textbf{v},h)` into
`[phy:equ:BacktracedDensityFluxSurfaceAverageAfterX] <#phy:equ:BacktracedDensityFluxSurfaceAverageAfterX>`__
and obtain

.. math::

   \begin{aligned}
       0 = \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{(-h \textbf{v})^{\textbf{i}}}{\textbf{i}!} \partial_\textbf{x}^{\textbf{i}} f^{*}(\textbf{x},\textbf{v}) \mathrm{d}^3 v \rangle_\text{F}
   \end{aligned}

Inserting :math:`\textbf{c}_{\textbf{v}}(\phi(\textbf{x}),h/2)` into
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
requires a bit more effort. We rewrite the cross-product using a
matrix-vector multiplication

.. math::

   \begin{aligned}
       \textbf{v}\times \textbf{B}_0 = 
       \begin{pmatrix}
         0 &B_0& 0\\
       -B_0& 0 & 0\\
         0 & 0 & 0
       \end{pmatrix} \textbf{v},
   \end{aligned}

which can be used to write
:math:`\textbf{v}+ \textbf{c}_{\textbf{v}}(\phi(\textbf{x}),h/2)` as an
affine mapping

.. math::

   \begin{aligned}
       \label{num:equ:ConstantFluxAvgExpansionAffineMapping}
       \textbf{v}+ \textbf{c}_{\textbf{v}}(\phi(\textbf{x}),h/2) =
       \begin{pmatrix}
           1 & -h\omega_c/2 & 0\\
           h\omega_c/2 & 1 & 0\\
           0 & 0 & 1
       \end{pmatrix} \textbf{v}- \dfrac{h}{2} \dfrac{q}{m} (-\nabla \phi(\textbf{x})) =: \textbf{A} \cdot \textbf{v}+ \textbf{b} 
   \end{aligned}

with :math:`\omega_c=\vert q \vert B_0/m` as cyclotron frequency. The
distribution function of a :math:`\textbf{v}` advection step can be
reformulated with the affine mapping

.. math::

   \begin{aligned}
       f(\textbf{x},\textbf{v}+ \textbf{c}_{\textbf{v}}(\phi(\textbf{x}),h/2),t_0) = f(\textbf{x}, \textbf{A} \textbf{v}+ \textbf{b}, t_0)
   \end{aligned}

In the second step, we transform the integral in
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
using

.. math::

   \begin{aligned}
       \begin{cases}
           \varphi &: \mathbb{R}^3 \rightarrow \mathbb{R}^3\\
           \varphi(\textbf{u}) &= \textbf{A}^{-1} \cdot (\textbf{u} - \textbf{b})
       \end{cases}.
   \end{aligned}

We remove the shift of the :math:`\textbf{v}` advection step from the
distribution function by transforming
`[phy:equ:BacktracedDensityFluxSurfaceAverage] <#phy:equ:BacktracedDensityFluxSurfaceAverage>`__
using :math:`\varphi(\textbf{u})` into

.. math::

   \begin{aligned}
       0 &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{(-h \textbf{v})^{\textbf{i}}}{\textbf{i}!} \partial_{\textbf{x}}^{\textbf{i}}f(\textbf{x}, \textbf{A} \textbf{v}+ \textbf{b}, t_0) \mathrm{d}^3v \rangle_\text{F}\\
         &= \langle n_e \rangle_\text{F} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\left(-h \textbf{A}^{-1}(\textbf{u} - \textbf{b})\right)^{\textbf{i}}}{\textbf{i}!} \partial_{\textbf{x}}^{\textbf{i}}f(\textbf{x}, \textbf{u}, t_0) \text{det}(\textbf{A}^{-1}) \mathrm{d}^3u \rangle_\text{F}\\
         \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomain}
         &= \dfrac{\langle n_e \rangle_\text{F}}{\text{det}(\textbf{A}^{-1})} - \langle \int \sum_{\textbf{i} \in \mathbb{N}^3_0} \dfrac{\left(-h \textbf{A}^{-1}(\textbf{u} - \textbf{b})\right)^{\textbf{i}}}{\textbf{i}!} \partial_{\textbf{x}}^{\textbf{i}}f(\textbf{x}, \textbf{u}, t_0) \mathrm{d}^3u \rangle_\text{F}
   \end{aligned}

This expression needs to be evaluated to determine
:math:`\langle \phi \rangle_\text{F}(x)`.

The following three order conditions are useful to simplify the
expansion terms of the following paragraphs

.. math::

   \begin{aligned}
       \label{phy:equ:ApproxAInvByH}
       \textbf{A}^{-1} &= 
       \begin{pmatrix}
           \dfrac{1}{1 + (-h\omega_c/2)^2} & \dfrac{h\omega_c/2}{1 + (-h\omega_c/2)^2} & 0\\
           -\dfrac{h\omega_c/2}{1 + (-h\omega_c/2)^2} & \dfrac{1}{1 + (-h\omega_c/2)^2} & 0\\
           0 & 0 & 1       
       \end{pmatrix} = \mathbb{Id} + O(h)
   \end{aligned}

and

.. math::

   \begin{aligned}
       \label{phy:equ:ApproxbByH}
       \textbf{b} &= \dfrac{h}{2} \dfrac{q}{m}(\nabla \phi(\textbf{x})) = O(h)
   \end{aligned}

In the following paragraphs, we will evaluate
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomain] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomain>`__
for all multi-indices of
`[phy:equ:MultiIndexList] <#phy:equ:MultiIndexList>`__ up to
:math:`O(h^3)`. Furthermore, we approximate :math:`\textbf{A}` by
`[phy:equ:ApproxAInvByH] <#phy:equ:ApproxAInvByH>`__ since we have not
treated this part appropriately to the splitting that is used in the
simulations. Any approximations with errors up to :math:`O(h^2)` are
called the neglected matrix :math:`\textbf{A}`.

.. _zero-order-terms-vert-textbfi-vert-0-1:

Zero order terms :math:`\vert \textbf{i} \vert = 0`
'''''''''''''''''''''''''''''''''''''''''''''''''''

The term for :math:`\textbf{i} = (0,0,0)` in
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomain] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomain>`__
only considers the particle density. It is given by

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder0}
       \langle \int f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} = \langle n_i(\textbf{x},t_0) \rangle_\text{F}
   \end{aligned}

.. _first-order-terms-vert-textbfi-vert-1-1:

First order terms :math:`\vert \textbf{i} \vert = 1`
''''''''''''''''''''''''''''''''''''''''''''''''''''

The terms :math:`((1,0,0);(0,1,0);(0,0,1))` of the expansion in
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomain] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomain>`__
contain :math:`O(h)` and :math:`O(h^2)` orders. Therefore, we neglect
the :math:`O(h)` components of :math:`\textbf{A}^{-1}` according to
`[phy:equ:ApproxAInvByH] <#phy:equ:ApproxAInvByH>`__. We evaluated the
velocity moments in the second line according to (ToDo: Add and link to
documentation of velocity moments). The third line inserts
`[phy:equ:ApproxbByH] <#phy:equ:ApproxbByH>`__ to include the
electrostatic potential. We split the potential in the fourth line using
`[phy:equ:PotentialSplit] <#phy:equ:PotentialSplit>`__ which we can then
solve for :math:`\langle \phi \rangle_\text{F}(x)`

.. math::

   \begin{aligned}
        &\langle \int -h \textbf{A}^{-1}[\textbf{u} - \textbf{b}] \cdot \nabla f(\textbf{x},\textbf{u}, t_0) \mathrm{d}^3 u \rangle_\text{F}\\
       =&\langle -h \nabla \cdot \textbf{j}(\textbf{x},t_0) + h \textbf{b}\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F} + O(h^2)\\
       =&\langle -h \nabla \cdot \textbf{j}(\textbf{x},t_0) + h \left[\dfrac{h}{2}\dfrac{q}{m}(\nabla \tilde{\phi}(\textbf{x},t_0) + \nabla \langle \phi \rangle_\text{F}(x,t_0))\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F} + O(h^2)\\
       =&-h\langle \nabla \cdot  \textbf{j}(\textbf{x},t_0) \rangle_\text{F} + \dfrac{h^2}{2} \langle  \left[\dfrac{q}{m}\nabla \tilde{\phi}(\textbf{x},t_0)\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F} \\
       \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder1}
        &+ \dfrac{h^2}{2} \left[\dfrac{q}{m}\nabla \langle \phi \rangle_\text{F}(x,t_0)\right]\cdot \langle \nabla n(\textbf{x},t_0) \rangle_\text{F} + O(h^2)
   \end{aligned}

.. _second-order-terms-vert-textbfi-vert-2-1:

Second order terms :math:`\vert \textbf{i} \vert = 2`
'''''''''''''''''''''''''''''''''''''''''''''''''''''

The second order terms of the expansion in
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomain] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomain>`__
are given by the multi-indices
:math:`(1,1,0),\cdots,(0,1,1),(2,0,0),\cdots,(0,0,2)` contain
:math:`O(h^2)` to :math:`O(h^4)` orders. We can express these terms
using the Hessian matrix
:math:`\textbf{H}_f(\textbf{x},t_0) = (\partial_{x_i}\partial_{x_j} f(\textbf{x},\textbf{v},t_0))_{i,j}`.
We omit orders higher than :math:`O(h^3)` in the second step using
`[phy:equ:ApproxbByH] <#phy:equ:ApproxbByH>`__ and orders :math:`O(h^3)`
using `[phy:equ:ApproxAInvByH] <#phy:equ:ApproxAInvByH>`__. In the third
line, we evaluate the velocity moments according to (ToDo: Add and link
to documentation of velocity moments) and insert
`[phy:equ:ApproxbByH] <#phy:equ:ApproxbByH>`__ to include the
electrostatic potential. Finally, we split the potential according to
`[phy:equ:PotentialSplit] <#phy:equ:PotentialSplit>`__ which can again
be solved for :math:`\langle \phi \rangle_\text{F}(x)`

.. math::

   \begin{aligned}
        & \langle \int \dfrac{1}{2} \{-h \textbf{A}^{-1}(\textbf{u} - \textbf{b})\}^T \textbf{H}_f(\textbf{x},t_0) \{-h \textbf{A}^{-1}(\textbf{u} - \textbf{b})\} \mathrm{d}^3u \rangle_\text{F}\\
       =& \langle \dfrac{h^2}{2} \int \textbf{u}^T \textbf{H}_f(\textbf{x},t_0) \textbf{u} - \textbf{u}^T \textbf{H}_f(\textbf{x},t_0) \textbf{b} - \textbf{b}^T \textbf{H}_f(\textbf{x},t_0) \textbf{u}\mathrm{d}^3u \rangle_\text{F} + O(h^3)\\
       =& \langle \dfrac{h^2}{2} \left[ \partial_x^2 \textbf{S}_{1,1}(\textbf{x},t_0) - 2 \nabla (\nabla\cdot \textbf{j}(\textbf{x},t_0)) \cdot \dfrac{h}{2} \dfrac{q}{m} (\nabla \phi(\textbf{x},t_0))\right] \rangle_\text{F} + O(h^3)\\
       =& \langle \dfrac{h^2}{2} \partial_x^2 \textbf{S}_{1,1}(\textbf{x},t_0) \rangle_\text{F} - \langle \dfrac{h^3}{2} \nabla (\nabla\cdot \textbf{j}(\textbf{x},t_0) ) \cdot \dfrac{q}{m} \nabla \tilde{\phi}(\textbf{x},t_0) \rangle_\text{F}\\
       \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder2}
        &-\dfrac{h^3}{2} \nabla (\nabla\cdot \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F}) \cdot \dfrac{q}{m} \nabla \langle \phi \rangle_\text{F}(x,t_0) + O(h^3)
   \end{aligned}

Third order terms in :math:`\vert \textbf{i} \vert = 3`
'''''''''''''''''''''''''''''''''''''''''''''''''''''''

Lastly, we collect the third order terms of the expansion. This
corresponds to the multi-indices
:math:`((1,1,1),(2,1,0),...,(0,1,2),(3,0,0),...(0,0,3))`.

We first consider the multi-index :math:`(1,1,1)`. Due to
`[phy:equ:ApproxAInvByH,phy:equ:ApproxbByH] <#phy:equ:ApproxAInvByH,phy:equ:ApproxbByH>`__,
we can omit all contributions except one which is :math:`O(h^4)` and
higher order in the second line. Furthermore, we utilize the vanishing
integral on a periodic domain from
`[phy:equ:VanishingPeriodicIntegral] <#phy:equ:VanishingPeriodicIntegral>`__
in the third line such that we do not get a contribution from the
multi-index :math:`(1,1,1)`

.. math::

   \begin{aligned}
        & \langle \int (-h \textbf{A}^{-1}(\textbf{u}-\textbf{b}))_x (-h \textbf{A}^{-1}(\textbf{u}-\textbf{b}))_y (-h \textbf{A}^{-1}(\textbf{u}-\textbf{b}))_z \partial_x\partial_y\partial_z f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} \nonumber\\
       =& \langle - h^3 \int (\textbf{u})_x (\textbf{u})_y (\textbf{u})_z \partial_x\partial_y\partial_z f(\textbf{x}, \textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} + O(h^4) \nonumber\\
       =& 0 + O(h^4)
   \end{aligned}

The multi-indices :math:`(2,1,0),...,(0,1,2),(0,3,0),(0,0,3)` vanish
using the same arguments from the last subsection.

The remaining the multi-index is :math:`(3,0,0)`

.. math::

   \begin{aligned}
       &\dfrac{1}{6} \langle \int (-h \textbf{A}^{-1}(\textbf{u}-\textbf{b}))_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} \nonumber\\
       \label{phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder3}
       = &\dfrac{-h^3}{6} \langle \int (\textbf{u})_x^3 \partial_x^3 f(\textbf{x}, \textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F} + O(h^4).
   \end{aligned}

Determining electrostatic potential
'''''''''''''''''''''''''''''''''''

Finally, after determining all contributions up to :math:`O(h^3)` we
obtain an equation for the flux surface average
:math:`\langle \phi \rangle_\text{F}` by inserting
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder0,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder1,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder2,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder3] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder0,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder1,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder2,phy:equ:ConstantFluxAvgExpansionPhysicalDomainOrder3>`__
into
`[phy:equ:ConstantFluxAvgExpansionPhysicalDomain] <#phy:equ:ConstantFluxAvgExpansionPhysicalDomain>`__
which reads

.. math::

   \begin{aligned}
       0 = &\langle n_e \rangle_\text{F}(x) - \langle n(\textbf{x},t_0) \rangle_\text{F}\\
           &+ h\langle \nabla \cdot [ \textbf{j}(\textbf{x},t_0)] \rangle_\text{F} - \dfrac{h^2}{2}\langle  \left[\dfrac{q}{m}\nabla \tilde{\phi}(\textbf{x},t_0)\right]\cdot \nabla n(\textbf{x},t_0) \rangle_\text{F} \\
           &- \dfrac{h^2}{2} \left[\dfrac{q}{m}\nabla \langle \phi \rangle_\text{F}(x,t_0)\right]\cdot \langle \nabla n(\textbf{x},t_0) \rangle_\text{F}\\
           &- \dfrac{h^2}{2} \langle \partial_x^2 \textbf{S}_{1,1}(\textbf{x},t_0) \rangle_\text{F} + \langle \dfrac{h^3}{2} \nabla (\nabla\cdot \textbf{j}(\textbf{x},t_0)) \cdot \dfrac{q}{m} \nabla \tilde{\phi}(\textbf{x},t_0) \rangle_\text{F}\\
           &+ \dfrac{h^3}{2} \nabla (\nabla\cdot \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F}) \cdot \dfrac{q}{m} \nabla \langle \phi \rangle_\text{F}(x,t_0)\\
           &+ \dfrac{h^3}{6} \langle \int (\textbf{u})_x^3 \partial_x^3 f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F}\\
           \label{phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvgIllConditioned}
           &+ O(h^2).
   \end{aligned}

If we try to solve this equation for
:math:`\langle \phi(\textbf{x}, t_0) \rangle_\text{F}` we have to divide
by :math:`\nabla n(\textbf{x}, t_0)` and
:math:`\nabla\cdot\textbf{j}(\textbf{x}, t_0)` which might be a division
by zero and is therefore ill-conditioned. This is why we reformulate the
equation using the quasineutrality assumption
:math:`\Delta \phi(\textbf{x}, t_0) = 0`

.. math::

   \begin{aligned}
       \label{phy:equ:RewriteIllConditionedDensity}
       \nabla \phi(\textbf{x}, t_0) \cdot \nabla n(\textbf{x}, t_0) = \nabla\cdot(\nabla \phi(\textbf{x}, t_0) n(\textbf{x}, t_0)) + (\Delta \phi(\textbf{x}, t))n(\textbf{x}, t) = \nabla\cdot(\nabla \phi(\textbf{x}, t) n(\textbf{x}, t_0))
   \end{aligned}

and rewrite
`[phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvgIllConditioned] <#phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvgIllConditioned>`__
to

.. math::

   \begin{aligned}
       0 = &\langle n_e \rangle_\text{F}(x) - \langle n(\textbf{x},t_0) \rangle_\text{F}\\
           &+ h\langle \nabla \cdot \textbf{j}(\textbf{x},t_0) \rangle_\text{F} - \dfrac{h^2}{2}\nabla \cdot \langle  \left[\dfrac{q}{m}\nabla \tilde{\phi}(\textbf{x},t_0)\right] n(\textbf{x},t_0) \rangle_\text{F} \\
           &- \dfrac{h^2}{2} \nabla \cdot \left[\dfrac{q}{m}\left(\nabla \langle \phi \rangle_\text{F}(x,t_0)\right) \langle n(\textbf{x},t_0) \rangle_\text{F}\right]\\
           &- \dfrac{h^2}{2} \langle \partial_x^2 \textbf{S}_{1,1}(\textbf{x},t_0) \rangle_\text{F} + \dfrac{h^3}{2}\nabla \cdot \left[\langle (\nabla\cdot \textbf{j}(\textbf{x},t_0)) \cdot \dfrac{q}{m} \nabla \tilde{\phi}(\textbf{x},t_0) \rangle_\text{F}\right]\\
           &+ \dfrac{h^3}{2} \nabla \cdot \left[(\nabla\cdot \langle \textbf{j}(\textbf{x},t_0) \rangle_\text{F}) \dfrac{q}{m} \nabla \langle \phi \rangle_\text{F}(x,t_0)\right]\\
           &+ \dfrac{h^3}{6} \langle \int (\textbf{u})_x^3 \partial_x^3 f(\textbf{x},\textbf{u}, t_0)\mathrm{d}^3 u \rangle_\text{F}\\
           \label{phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvg}
           &+ O(h^2).
   \end{aligned}

This equation can be solved for
:math:`\langle \phi \rangle_\text{F}(x, t_0)` by integrating twice for
:math:`x`.

We can use this equation to maintain a constant flux surface average in
the physical domain. Numerical examples are provided in
`1.4 <#sec:NumericalExamplesWithNetZeroChargeTransport>`__. Any orders
higher than :math:`O(h^4)` are due to the approximation in
`[phy:equ:ApproxAInvByH] <#phy:equ:ApproxAInvByH>`__, which does not
treat the velocity splitting accurately.

.. _`subsec:ASemiLagrangianStrangSplittingWithNetZeroChargeTransport`:

A semi-Lagrangian Strang splitting with net-zero charge transport
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We can now combine the results of the previous
`[subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid,subsec:NetZeroChargeTransportPhiFluxAvgRotGrid] <#subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid,subsec:NetZeroChargeTransportPhiFluxAvgRotGrid>`__
using
`[phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvg,phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg] <#phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvg,phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg>`__
respectively with the Strang splitting in
`[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__ and the
electrostatic potential from
`[phy:equ:PotentialSplit] <#phy:equ:PotentialSplit>`__ to write an
algorithm that maintains a constant flux surface average for the
particle density :math:`n(\textbf{x},t)`.

In
`[phy:alg:ConstantFluxSurfaceAverageFieldSolver] <#phy:alg:ConstantFluxSurfaceAverageFieldSolver>`__,
we provide the algorithm that calculates the electrostatic potential and
the electrostatic field, which is used in the Strang splitting to
integrate the distribution function for a single time step.

The actual splitting algorithm is given in
`[phy:alg:SplittingWithConstantFluxSurfAvg] <#phy:alg:SplittingWithConstantFluxSurfAvg>`__.
In this section, we always used a 6-D phase space to determine the
algorithm. It is also possible to apply the algorithm to a
lower-dimensional problem. We only need the direction into which the
charge transport should be net-zero for the spatial domain and a 2-D
velocity domain, which can include the rotational term of the magnetic
field. We omit integration steps in
`[phy:alg:SplittingWithConstantFluxSurfAvg] <#phy:alg:SplittingWithConstantFluxSurfAvg>`__
for lower dimensional problems. In the next section, which considers
numerical examples for the algorithm derived in this subsection, we
consider a 3-D phase space and a 4-D phase space, reducing the
dimensionality as described.

.. _`sec:NumericalExamplesWithNetZeroChargeTransport`:

Numerical evaluation of the net-zero charge transport
-----------------------------------------------------

This section discusses two numerical experiments to evaluate the
algorithm described in
`1.3.3 <#subsec:ASemiLagrangianStrangSplittingWithNetZeroChargeTransport>`__.
We will evaluate the conserving properties described in both examples in
`1.2 <#sec:NetZeroChargeTransport>`__. The flux surface average of the
particle density shall be constant over time

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxSurfaceAveragePreservation}
       \langle n(\textbf{x},t) \rangle_\text{F} = \langle n_0(\textbf{x}) \rangle_\text{F} = \langle \int f(\textbf{x},\textbf{v},0) \mathrm{d}^3v \rangle_\text{F},
   \end{aligned}

and the particle flux perpendicular to the flux surfaces shall be zero

.. math::

   \begin{aligned}
       \label{phy:equ:NetZeroParticleFluxPreservation}
       j_x(\textbf{x},t) = 0.
   \end{aligned}

The last condition is stronger than the condition for the particle flux
described in `1.2 <#sec:NetZeroChargeTransport>`__ in a general setup.
Assuming the :math:`x`-dimension in our slab geometry is the radial
direction of a Tokamak both conditions would be equivalent.

The first experiment focuses on a simple setup in which the electric
potential can be determined analytically as an equilibrium solution.
This can be used to measure convergence properties and verify the above
conditions.

The second example will reproduce the dispersion relation of ion
Bernstein waves from `1.5 <#sec:IonBernsteinWavesDispersionRelation>`__.
In the direction parallel to the flux surfaces, we expect
`[phy:equ:NeutralizingIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves>`__
called neutralizing ion Bernstein waves. In the direction perpendicular
to the flux surfaces, we expect
`[phy:equ:PureIonBernsteinWaves] <#phy:equ:PureIonBernsteinWaves>`__
called pure ion Bernstein waves. To our knowledge, the latter has not
yet been reproduced in a numerical simulation.

Net-zero charge transport equilibrium solution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We consider a reduced Vlasov equation using dimensions
:math:`x`,\ :math:`v_x` and :math:`v_y`. The Vlasov system of
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__
reduces to

.. math::

   \begin{aligned}
       \label{phy:equ:LowDimVlasovEquConstFluxSurfaceAverage}
       \begin{cases}
       \frac{\partial}{\partial t} f + v_x \partial_x f + \dfrac{q}{m}(-\partial_x \phi)\partial_{v_x}f + \dfrac{q}{m}(\textbf{v}\times \textbf{B}_0) \cdot \nabla_\textbf{v}f = 0\\
       \frac{\partial}{\partial t} n(x, t) = 0     
       \end{cases}
   \end{aligned}

with :math:`f=f(x,v_x,v_y,t)` and the particle density
:math:`n(x,t) = \int f(x,v_x,v_y,t)\mathrm{d}^2 v`. Applying the flux
surface average to any quantity in this simulation is an identity
operation. Therefore, electrostatic potential :math:`\phi(x)` is fully
determined by
`[phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvg,phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg] <#phy:equ:FixedGridDeterminingElectricPotentialFluxSurfAvg,phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg>`__
derived in
`[subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid,subsec:NetZeroChargeTransportPhiFluxAvgRotGrid] <#subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid,subsec:NetZeroChargeTransportPhiFluxAvgRotGrid>`__,
respectively

.. math::

   \begin{aligned}
       \langle f(x, v_x, v_y) \rangle_\text{F} &= f(x, v_x, v_y)\\
       \langle n(x, t) \rangle_\text{F} &= n(x, t)\\
       \langle \phi(x, t) \rangle_\text{F} &= \phi(x, t).
   \end{aligned}

The particle density in this setup is an equilibrium, and the
electrostatic potential for this equilibrium can be determined using the
approach in `1.1 <#sec:AdiabaticElectronsAndQuasiNeutrality>`__ for
adiabatic electrons. The electrostatic potential that maintains a
constant density flux surface average is then given by

.. math::

   \begin{aligned}
       \phi(x) = n(x) - n_0 = \int f(x,v_x,v_y) \mathrm{d}^2v - n_0,
   \end{aligned}

where :math:`n_0` is a constant background ion particle density. All
units of measure are normalized
:math:`q`,\ :math:`T`,\ :math:`m`,\ :math:`n_0` to one. We use
`[phy:alg:SplittingWithConstantFluxSurfAvg] <#phy:alg:SplittingWithConstantFluxSurfAvg>`__
to solve
`[phy:equ:LowDimVlasovEquConstFluxSurfaceAverage] <#phy:equ:LowDimVlasovEquConstFluxSurfaceAverage>`__.
Quasineutrality is fulfilled in the initial step using
:math:`\langle n_e \rangle_\text{F}(x) = \int f_0(x,v_x,v_y) \mathrm{d}^2v`.

We initialize the distribution function with a plane wave perturbation
and use periodic boundary conditions in all dimensions

.. math::

   \begin{aligned}
       f(x,v_x,v_y)\vert_{t=0} = (1 + 0.01 \sin(m_k x)) f_M(v)
   \end{aligned}

where :math:`m_k=m L_x/2\pi` and :math:`m\in\mathbb{N}`. In the
remainder of this subsection, :math:`m` does not correspond to the mass
of the kinetic particles but to the mode of the
:math:`\sin`-perturbation.

All results are used on a grid with :math:`128\times61\times61` degrees
of freedom for :math:`t\in[0,20\pi\omega_c^{-1})`. We vary the time step
and the mode :math:`j`.

The results in `1.1 <#phy:fig:ConvergenceRatesRotatingGrid>`__ are given
for the rotating grid. The left column shows the conservation property
for
`[phy:equ:ConstantFluxSurfaceAveragePreservation] <#phy:equ:ConstantFluxSurfaceAveragePreservation>`__.
In the upper plot, we observe neglectable fluctuations in the density
after an initial jump. We observe a convergence rate of :math:`O(h^2)`
to :math:`O(h^3)` in the plot. The electrostatic potential calculated
through
`[phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg] <#phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg>`__
should maintain a constant flux surface average with
:math:`n(x,t) = n(x,0) + O(h^2)`. For the mode :math:`k_0`, we can
observe this convergence rate quite well. For higher modes, the
convergence rate rises. A closer investigation of the Taylor expansion
factor used to derive
`[phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg] <#phy:equ:RotatingGridDeterminingElectricPotentialFluxSurfAvg>`__
could provide further insights. Even if the convergence rate does not
precisely match the expected behavior, we get a stable conservation for
the flux surface average, as is observed in the upper plot.

The right column shows the behavior of the particle flux. Here, the
convergence rates match the :math:`O(h^2)` behavior well. The particle
flux is initially zero and shows a steep increase at the start of the
simulation. This is damped after a few time steps, and we conserve the
zero flux property given in
`[phy:equ:NetZeroParticleFluxPreservation] <#phy:equ:NetZeroParticleFluxPreservation>`__.

.. container:: float
   :name: phy:fig:ConvergenceRatesRotatingGrid

   .. container:: center

The results in `1.2 <#phy:fig:ConvergenceRatesFixedGrid>`__ provide the
same insights as the upper row in
`1.1 <#phy:fig:ConvergenceRatesRotatingGrid>`__. Both conditions
`[phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation] <#phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation>`__
are not conserved as well as for the rotating grid. In
`1.3.2 <#subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid>`__, we mixed
different orders of accuracy. This leads to no stable convergence rates,
which, therefore, have been omitted in
`1.2 <#phy:fig:ConvergenceRatesFixedGrid>`__. Furthermore, we neglected
the impact of the rotational term, which would not have been
appropriately represented in the splitting. This is especially visible
in `1.2 <#phy:fig:ConvergenceRatesFixedGrid>`__ b). The particle flux
has minima on multiples of half the gyro period :math:`\omega_c/2`.
Taking the rotation into account and following the density change more
accurately in the splitting of
`[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__ for the fixed
grid would give better results.

.. container:: float
   :name: phy:fig:ConvergenceRatesFixedGrid

   .. container:: center

      |image1|

.. _`subsec:IonBernsteinWaves`:

Validation against ion Bernstein waves dispersion relation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this subsection we verify the behavior of the equation derived in
`1.3 <#sec:NumericalAlgorithmWithNetZeroChargeTransportBetweenFluxSurfaceAverages>`__
by reproducing the dispersion relation for neutralizing and pure ion
Bernstein waves of
`[phy:equ:NeutralizingIonBernsteinWaves,phy:equ:PureIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves,phy:equ:PureIonBernsteinWaves>`__
of in `1.5 <#sec:IonBernsteinWavesDispersionRelation>`__. The system
given by
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__
where we omit the :math:`z`,\ :math:`v_z` dimensions

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgSystemOfEqu4D}
       \begin{cases}
           &\frac{\partial}{\partial t} f(\textbf{x},\textbf{v},t) + \textbf{v}\cdot \nabla_\textbf{x}f(\textbf{x},\textbf{v},t) + \dfrac{q}{m} \left((-\nabla_\textbf{x}\phi(\textbf{x}, t)) + \textbf{v}\times \textbf{B}_0\right) \cdot \nabla_\textbf{v}f(\textbf{x},\textbf{v},t) = 0\\
           &\dfrac{e}{m_e v^2_{e,\text{th}}} \left(\phi(\textbf{x}, t) - \langle \phi(\textbf{x}, t) \rangle_\text{F}\right) = n(\textbf{x},t) - \langle n \rangle_\text{F}(x)\\
           &\frac{\partial}{\partial t}\langle n(\textbf{x},t) \rangle_\text{F} = 0
       \end{cases}
   \end{aligned}

with :math:`\textbf{x}= (x,y)` and :math:`\textbf{v}= (v_x,v_y)`.

We explicitly excite ion Bernstein waves by initializing a perturbation
based on the solution of :math:`\hat{f}_1` from
`[phy:equ:DispersionRelationDistributionFunctionExpression] <#phy:equ:DispersionRelationDistributionFunctionExpression>`__

.. math::

   \begin{aligned}
       f(\textbf{x},\textbf{v},t)\vert_{t=0} = \left(1 + 10^{-5}\sum_{k<k_\text{max}}\sum_{p<p_\text{max}} J_p(v_\perp k_\perp) \cos(v_\perp k_\perp \sin(\gamma) - p\gamma + \textbf{k} \cdot \textbf{x})\right) f_M(v),
   \end{aligned}

where :math:`k_\text{max}`,\ :math:`p_\text{max}`, and the perturbation
amplitude is small to not excite nonlinear couplings. The angle
:math:`\gamma\measuredangle(v_\perp,k_\perp)` is between the
perpendicular velocity dimension and the perpendicular wave number.
Perpendicular dimensions are perpendicular to :math:`\textbf{B}_0`, in
our setup :math:`\hat{e}_z`.

The results are obtained on a grid with
:math:`128\times128\times61\times61` degrees of freedom for
:math:`t\in[0,1000\omega_c^{-1})`. Both stopped early due to an MPI
error in a communicator. Therefore, not the entire time domain could be
utilized. We obtain the dispersion relation with a Fourier transform
applied to the electrostatic potential in space and time
:math:`\phi(\textbf{x},t)`. The dispersion relation can be compared
against the analytical solution calculated in
`[phy:equ:NeutralizingIonBernsteinWaves,phy:equ:PureIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves,phy:equ:PureIonBernsteinWaves>`__.
This direction reproduces neutralizing ion Bernstein waves, which are
given for :math:`\omega_c(0,k_y,0)`. Therefore, we can reproduce the
behavior of adiabatic electrons within a flux surface. In
`1.2 <#sec:NetZeroChargeTransport>`__, we broke the symmetry of the
:math:`x-y` plane. No charge transport should take place between flux
surfaces. This should produce the dispersion relation of pure ion
Bernstein waves, which are given for :math:`\omega(k_x,0,0)`.

`1.3 <#phy:fig:ReproducingRotatingIBWDispersionRelation>`__ shows the
dispersion relation for IBWs on the rotating frame, which was introduced
in (ToDo: Link and add documentation on the rotating frame). The black
lines indicate the analytical solution of :math:`\omega(k_x,0,0)` in
`[phy:equ:PureIonBernsteinWaves] <#phy:equ:PureIonBernsteinWaves>`__ in
`1.3 <#phy:fig:ReproducingRotatingIBWDispersionRelation>`__ a). In
`1.3 <#phy:fig:ReproducingRotatingIBWDispersionRelation>`__ b) the black
lines indicate the analytical solution of :math:`\omega(0,k_y,0)` in
`[phy:equ:NeutralizingIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves>`__.
Both are qualitatively in good agreement. The neutralizing IBWs show a
higher amplitude than the pure IBWs. We observe in both dispersion
relations noise at multiples of the gyrofrequencies :math:`\omega_c`.

.. container:: float
   :name: phy:fig:ReproducingRotatingIBWDispersionRelation

   .. container:: center

      |image2|

In `1.4 <#phy:fig:ReproducingRotatingIBWPreservedQuantities>`__, we
observe the behavior of the preserved quantities from
`[phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation] <#phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation>`__.
The flux surface average of the particle density drops initially and
then remains constant. The Lagrange interpolation has damping effects,
which increase with the mode number. Since this setup uses :math:`30k_0`
for :math:`k_\text{max}`, which is almost half the Nyquist frequency of
the system, the damping is an expected behavior at the beginning of the
simulation. We can observe in the right plot that we start from an
illegal state with the initial condition of this subsection, which has a
non-zero :math:`\langle \textbf{j}(\textbf{x},t) \rangle_\text{F}`.
Therefore, our initial condition does not fulfill
`[phy:equ:NetZeroParticleFluxPreservation] <#phy:equ:NetZeroParticleFluxPreservation>`__.

.. container:: float
   :name: phy:fig:ReproducingRotatingIBWPreservedQuantities

   .. container:: center

      |image3|

We conclude this subsection with the same simulation on the physical
domain without the rotating grid. The simulation ran only until
:math:`t=250\omega_c^{-1}`, so the dispersion relation has significantly
stronger diffusive results in :math:`\omega`. We can only simulate
significantly fewer modes compared to the rotating grid in
:math:`\omega` as well as :math:`k`. The observed modes match the
theoretical values quite well. We omit a plot for the conserved
quantities
`[phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation] <#phy:equ:ConstantFluxSurfaceAveragePreservation,phy:equ:NetZeroParticleFluxPreservation>`__.
The results are the same as where already visible in
`1.2 <#phy:fig:ConvergenceRatesFixedGrid>`__ just for an extended
simulation. The particle flux
:math:`\langle j_x(\textbf{x},t) \rangle_\text{F}` saturates during the
simulation while :math:`\langle n(\textbf{x},t) \rangle_\text{F}`
deviates further from the initial value. This can be explained by the
mixed orders used in
`1.3.2 <#subsec:NetZeroChargeTransportPhiFluxAvgPhyGrid>`__.
Furthermore, we did not precisely trace back the
:math:`\textbf{v}\times \textbf{B}_0` term in the splitting
`[phy:equ:SplittingScheme] <#phy:equ:SplittingScheme>`__.

.. container:: float
   :name: phy:fig:ReproducingFixedIBWDispersionRelation

   .. container:: center

      |image4|

.. _`sec:IonBernsteinWavesDispersionRelation`:

Ion Bernstein waves dispersion relation
---------------------------------------

In this section, we derive the dispersion relation for
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__,
which we utilize to verify the behavior of the numerical algorithm in
`1.3 <#sec:NumericalAlgorithmWithNetZeroChargeTransportBetweenFluxSurfaceAverages>`__.
The waves which will be derived have first been analyzed by Bernstein
(1958) and were experimentally found by Schmitt (1973). Our solution
strategy follows the calculation of Kennel and Engelmann (1966, sec. 2)
and was recently applied by Lafitte and Maj (2018, sec. 5).

We start by separating the distribution function and the potential into
a background and a perturbation with the perturbation amplitude
:math:`\epsilon\ll1`

.. math::

   \begin{aligned}
       \label{phy:equ:ConstantFluxAvgPerturbedVariables}
       \begin{cases}
           f &= f_0 + \epsilon f_1\\
           \phi &= \phi_0 + \epsilon \phi_1
       \end{cases}.
   \end{aligned}

The background is a Maxwell distribution
:math:`f_0(\textbf{x},\textbf{v}) = f_M(v) = \dfrac{1}{\mathcal{N}}\exp\left(-\dfrac{v^2}{2{v_\text{th}}^2}\right)`
while the background potential :math:`\phi_0(\textbf{x})` is assumed to
be zero. The background is an equilibrium solution of
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.

Solving for the solution of the dispersion relation starts by inserting
`[phy:equ:ConstantFluxAvgPerturbedVariables] <#phy:equ:ConstantFluxAvgPerturbedVariables>`__
into
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__
and keeping only the terms linear in :math:`f_1` and :math:`\phi_1`. The
linearized form of
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__
is given by

.. math::

   \begin{aligned}
       \begin{cases}
           \frac{\partial}{\partial t}f_1 + \textbf{v}\cdot \nabla_\textbf{x}f_1 + \left(\textbf{v}\times \boldsymbol{\omega}_c\right) \cdot \nabla_\textbf{v}f_1 = \dfrac{q}{m}\nabla_\textbf{x}\phi_1 \cdot \nabla_\textbf{v}f_M\\
           \dfrac{e}{m_e v^2_{e,\text{th}}}(\phi_1 - \langle \phi_1 \rangle_\text{F}) = n_1 - \langle n_1 \rangle_\text{F}\\
           \frac{\partial}{\partial t}\langle n_1 \rangle_\text{F} = 0
       \end{cases},
   \end{aligned}

with the gyrofrequency
:math:`\boldsymbol{\omega}_c = \dfrac{q}{m}\textbf{B}_0`, and its
abolute value :math:`\omega_c= qB_0/m`. Now, we consider Fourier
components for the perturbed quantities

.. math::

   \begin{aligned}
       \label{phy:equ:FourierAnsatzDispersionRelation}
       \begin{cases}
       f_1(\textbf{x},\textbf{v},t) &= \hat{f}_1(\textbf{k}, \textbf{v}, \omega) e^{-{i\mkern 1mu}\omega t + {i\mkern 1mu}\textbf{k}\textbf{x}}\\
       \phi_1(\textbf{x},t) &= \hat{\phi}_1(\textbf{k}, \omega) e^{-{i\mkern 1mu}\omega t + {i\mkern 1mu}\textbf{k}\textbf{x}}\\
       \langle \phi_1(\textbf{x},t) \rangle_\text{F} &= \hat{\phi}_1(\textbf{k}, \omega) e^{-{i\mkern 1mu}\omega t + {i\mkern 1mu}k_x x} \delta_{k_y,0}\delta_{k_z,0}    
       \end{cases}
   \end{aligned}

using :math:`\textbf{k} = (k_x, k_y,k_z)` and assuming discrete
:math:`k_{y}`,\ :math:`k_{z}` in the Kronecker delta. We can insert the
Fourier components into the linearized system of equations. Applying
:math:`\partial_t`, :math:`\langle \cdot \rangle_\text{F}` and
:math:`\nabla_\textbf{x}` to the Fourier components of the linearized
system results in

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationFourierSystem}
       \begin{cases}
           -{i\mkern 1mu}\omega \hat{f}_1 + i\textbf{k} \cdot \textbf{v}\hat{f}_1 + &(\textbf{v}\times \boldsymbol{\omega}_c) \cdot \nabla_\textbf{v}\hat{f}_1 = {i\mkern 1mu}\dfrac{q}{m}\hat{\phi}_1\textbf{k}\cdot \nabla_\textbf{v}f_M\\
           \dfrac{e}{m_e v^2_{e,\text{th}}} \hat{\phi}_1 = \hat{n}_1 \qquad &\text{if } (k_y,k_z)\neq(0,0)\\
           -{i\mkern 1mu}\omega \hat{n}_1 = 0 \qquad &\text{if } (k_y,k_z)=(0,0)
       \end{cases}
   \end{aligned}

In the three following paragraphs, we calculate the dispersion relation
of
`[phy:equ:DispersionRelationFourierSystem] <#phy:equ:DispersionRelationFourierSystem>`__.
First, a solution for :math:`\hat{f}_1` is determined. With the Fourier
component of the distribution function, we can calculate
:math:`\hat{n}_1`. The dispersion relation is obtained by plugging
:math:`\hat{n}_1` into the latter two equations of
`[phy:equ:FourierAnsatzDispersionRelation] <#phy:equ:FourierAnsatzDispersionRelation>`__.

Solving :math:`\hat{f}_1`
'''''''''''''''''''''''''

In this paragraph the first equation of
`[phy:equ:DispersionRelationFourierSystem] <#phy:equ:DispersionRelationFourierSystem>`__

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDistributionFunction}
       -{i\mkern 1mu}\omega \hat{f}_1 + i\textbf{k} \cdot \textbf{v}\hat{f}_1 + (\textbf{v}\times \boldsymbol{\omega}_c) \cdot \nabla_\textbf{v}\hat{f}_1 = {i\mkern 1mu}\dfrac{q}{m}\hat{\phi}_1\textbf{k}\cdot \nabla_\textbf{v}f_M
   \end{aligned}

will be solved for :math:`\hat{f}_1`. We briefly outline the approach to
calculate :math:`\hat{f_1}` starting from
`[phy:equ:DispersionRelationDistributionFunction] <#phy:equ:DispersionRelationDistributionFunction>`__.
First, cylindrical coordinates in velocity space are introduced. This
will enable us to transform the PDE of
`[phy:equ:DispersionRelationDistributionFunction] <#phy:equ:DispersionRelationDistributionFunction>`__
into an ODE. The ODE can be integrated to obtain an expression for
:math:`\hat{f}_1`.

The polar plane of the cylindrical coordinate system is chosen to be
parallel to
:math:`\hat{e}_z = \hat{e}_\parallel = \dfrac{\boldsymbol{\omega}_c}{\omega_c}`.
The velocity vector is written as

.. math::

   \begin{aligned}
       \textbf{v}&= v_z \hat{e}_{z} + v_1 \hat{e}_{v_1} + v_2 \hat{e}_{v_2}\\
       \label{phy:equ:DispersionRelationCylindircalV}
       &= v_{\parallel} \hat{e}_{\parallel} + v_{\perp}(\cos(\varphi) \hat{e}_{v_1} + \sin(\varphi) \hat{e}_{v_2}),
   \end{aligned}

where :math:`\textbf{B}_0 \parallel \hat{e}_\parallel`,
:math:`\textbf{B}_0 \perp \hat{e}_{1,2}` and
:math:`\hat{e}_1 \perp \hat{e_2}`. We explicitly do not assign
:math:`\hat{e}_x` or :math:`\hat{e}_y` to the basis vectors
:math:`\hat{e}_{v_1}` and :math:`\hat{e}_{v_2}` in polar plane. This
decision will be useful in the last paragraph, where we calculate the
dispersion relation. With the cylindrical coordinates for
`[phy:equ:DispersionRelationDistributionFunction] <#phy:equ:DispersionRelationDistributionFunction>`__
we can replace the rotational term with a derivative acting on the polar
angle :math:`\varphi` if we consider
:math:`\boldsymbol{\omega}_c = (0,0,\omega_c)`

.. math::

   \begin{aligned}
       (\textbf{v}\times\boldsymbol{\omega}_c)\cdot\nabla_\textbf{v}g(\textbf{v}(v_\parallel,v_\perp,\varphi)) &=  \omega_c\left(v_2\frac{\partial}{\partial v_1} - v_1\frac{\partial}{\partial v_2}\right) g(\textbf{v}(v_\parallel,v_\perp,\varphi))\\
       \label{phy:equ:DispersionRelationCoordinateTransformation}
       &= -\omega_c\frac{\partial}{\partial \varphi} g(\textbf{v}(v_\parallel, v_\perp, \varphi)).
   \end{aligned}

We can now insert
`[phy:equ:DispersionRelationCylindircalV,phy:equ:DispersionRelationCoordinateTransformation] <#phy:equ:DispersionRelationCylindircalV,phy:equ:DispersionRelationCoordinateTransformation>`__
into
`[phy:equ:DispersionRelationDistributionFunction] <#phy:equ:DispersionRelationDistributionFunction>`__
obtaining

.. math::

   \begin{aligned}
       \frac{\partial}{\partial \varphi} \hat{f}_1 + {i\mkern 1mu}\dfrac{\omega}{\omega_c} \hat{f}_1 - \dfrac{{i\mkern 1mu}}{\omega_c} \left[k_\parallel v_\parallel + v_\perp (k_1 \cos(\varphi) + k_2 \sin(\varphi))\right] \hat{f}_1 = - {i\mkern 1mu}\dfrac{q}{m \omega_c} \hat{\phi}_1 \textbf{k} \cdot \nabla_\textbf{v}f_M.
   \end{aligned}

For convenience, we rearrange it to

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationPolarCoodrinates}
       \frac{\partial}{\partial \varphi} \hat{f}_1 + {i\mkern 1mu}\left[\dfrac{\omega - k_\parallel v_\parallel}{\omega_c} - \dfrac{v_\perp k_1}{\omega_c} \cos(\varphi) - \dfrac{v_\perp k_2}{\omega_c} \sin(\varphi)\right] \hat{f}_1 = - {i\mkern 1mu}\dfrac{q}{m \omega_c} \hat{\phi}_1 \textbf{k} \cdot \nabla_\textbf{v}f_M.
   \end{aligned}

This ODE can be solved using the ansatz

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationSolutionAnsatz}
       \begin{cases}
           e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\hat{f}_1\\
           \vartheta(\textbf{k}, \textbf{v}, \omega) := \dfrac{\omega - k_\parallel v_\parallel}{\omega_c}\varphi - \dfrac{v_\perp k_1}{\omega_c}\sin(\varphi) + \dfrac{v_\perp k_2}{\omega_c} (\cos(\varphi) - 1)
       \end{cases},
   \end{aligned}

where we have chosen the phase :math:`\vartheta\vert_{\varphi=0}=0`.
With this ansatz we can rewrite
`[phy:equ:DispersionRelationPolarCoodrinates] <#phy:equ:DispersionRelationPolarCoodrinates>`__
to

.. math::

   \begin{aligned}
       \frac{\partial}{\partial \varphi} \left[e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\hat{f}_1\right] = - {i\mkern 1mu}\dfrac{q}{m \omega_c} \hat{\phi}_1 \textbf{k} \cdot \left(\nabla_\textbf{v}f_M\right) e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}.
   \end{aligned}

We can further simplify the right-hand side. The gradient
:math:`\nabla_\textbf{v}` can be substituted using the spherical
symmetry. It was replaced with a derivative acting on :math:`v` in the
first line. In the second line we explicitly evaluate the derivative
:math:`v` applied to
:math:`f_M(v) = \mathcal{N}e^{-v^2/(2{v_\text{th}}^2)}`

.. math::

   \begin{aligned}
       \frac{\partial}{\partial \varphi} \left[e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\hat{f}_1\right] &= - {i\mkern 1mu}\dfrac{q}{m \omega_c} \hat{\phi}_1 \textbf{k} \cdot \textbf{v}\left(\dfrac{1}{v}\frac{\partial}{\partial v} f_M\right) e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\\
                                                                                       &= - {i\mkern 1mu}\dfrac{q}{m \omega_c} \hat{\phi}_1 \textbf{k} \cdot \textbf{v}\left(-\dfrac{f_M}{{v_\text{th}}^2}\right) e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}.
   \end{aligned}

Now, we identify the property
:math:`\frac{\partial}{\partial \varphi}\vartheta(\textbf{k},\textbf{v},\omega) = \dfrac{\omega}{\omega_c} - \dfrac{\textbf{k} \cdot \textbf{v}}{\omega_c}`
for
`[phy:equ:DispersionRelationSolutionAnsatz] <#phy:equ:DispersionRelationSolutionAnsatz>`__
to substitute :math:`\textbf{k}\cdot \textbf{v}/\omega_c` in the
previous equation

.. math::

   \begin{aligned}
       \frac{\partial}{\partial \varphi} \left[e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\hat{f}_1\right] &= - {i\mkern 1mu}\dfrac{q}{m} \hat{\phi}_1 \left(-\dfrac{f_M}{{v_\text{th}}^2} \right)\left(\dfrac{\omega}{\omega_c} - \frac{\partial}{\partial \varphi}\vartheta(\textbf{k},\textbf{v},\omega)\right) e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\\
                                                                                       &= \dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2} \left({i\mkern 1mu}\dfrac{\omega}{\omega_c}e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)} - \frac{\partial}{\partial \varphi}e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\right).
   \end{aligned}

The expression is integrated with respect to :math:`\varphi`

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationIntegralEquationFull}
       \int_0^\varphi \frac{\partial}{\partial \varphi'} \left[e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\hat{f}_1\right] \mathrm{d}\varphi' = \dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\int_0^\varphi \left({i\mkern 1mu}\dfrac{\omega}{\omega_c}e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)} - \frac{\partial}{\partial \varphi'}e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\right) \mathrm{d}\varphi',
   \end{aligned}

which is evaluated to

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationIntegralEquation}
       e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)} \hat{f}_1 - \hat{f}_1\vert_{\varphi=0} = \dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\left[\int_0^\varphi {i\mkern 1mu}\dfrac{\omega}{\omega_c}e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)}\mathrm{d}\varphi' - \left(e^{{i\mkern 1mu}\vartheta(\textbf{k}, \textbf{v}, \omega)} - 1\right)\right].
   \end{aligned}

In the remainder of the section, we focus on the modes :math:`k_1\neq0`
using :math:`k_2=k_\parallel=0`. The reduced phase will be sufficient to
calculate the dispersion relation of interest. The phase
:math:`\vartheta` in
`[phy:equ:DispersionRelationSolutionAnsatz] <#phy:equ:DispersionRelationSolutionAnsatz>`__
reduces to

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationReducedPhase}
       \vartheta(k_1, v_\perp, \varphi, \omega) = \dfrac{\omega}{\omega_c}\varphi - \dfrac{k_1 v_\perp}{\omega_c}\sin(\varphi).
   \end{aligned}

Furthermore, we utilize the Jacobi–Anger identity on several occasions
in the remaining section

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationJacobiAngerIdentity}
       e^{-ix\sin(\varphi)} = \sum_{p=-\infty}^{\infty} J(x) e^{-ip\varphi}
   \end{aligned}

First, we solve the remaining integral in
`[phy:equ:DispersionRelationIntegralEquation,] <#phy:equ:DispersionRelationIntegralEquation,>`__
using the reduced phase :math:`\vartheta` of
`[phy:equ:DispersionRelationReducedPhase] <#phy:equ:DispersionRelationReducedPhase>`__
together with the Jacobi–Anger identity of
`[phy:equ:DispersionRelationJacobiAngerIdentity] <#phy:equ:DispersionRelationJacobiAngerIdentity>`__
in the second line. In the third line, we evaluate the integral

.. math::

   \begin{aligned}
       {i\mkern 1mu}\int_0^\varphi \dfrac{\omega}{\omega_c}e^{{i\mkern 1mu}\vartheta(k_1, v_\perp, \varphi, \omega)}\mathrm{d}\varphi' &={i\mkern 1mu}\int_0^\varphi \dfrac{\omega}{\omega_c}\exp\left[{i\mkern 1mu}\left(\dfrac{\omega}{\omega_c}\varphi' - \dfrac{v_\perp k_1}{\omega_c} \sin(\varphi')\right)\right]\mathrm{d}\varphi'\\
                                                                                                       &={i\mkern 1mu}\int_0^\varphi \dfrac{\omega}{\omega_c} \sum_{p=-\infty}^{\infty} e^{{i\mkern 1mu}(\omega/\omega_c- p)\varphi'}J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right)\mathrm{d}\varphi'\\
                                                                                                       \label{phy:equ:DispersionRelationIntegralEvaluated}
                                                                                                       &= \sum_{p=-\infty}^{\infty} \dfrac{\omega/\omega_c}{\omega/\omega_c- p} \left(e^{{i\mkern 1mu}(\omega/\omega_c- p)\varphi}-1\right)J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right).
   \end{aligned}

Eg. `[phy:equ:DispersionRelationIntegralEvaluated] <#phy:equ:DispersionRelationIntegralEvaluated>`__
is insert into
`[phy:equ:DispersionRelationIntegralEquation] <#phy:equ:DispersionRelationIntegralEquation>`__
together with
`[phy:equ:DispersionRelationReducedPhase] <#phy:equ:DispersionRelationReducedPhase>`__

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationIntegralEquationEvaluated}
       &e^{{i\mkern 1mu}(\omega/\omega_c\;\varphi - k_1 v_\perp/\omega_c\;\sin(\varphi))} \hat{f}_1 - \hat{f}_1\vert_{\varphi=0} \\
       = &\dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\left[\sum_{p=-\infty}^{\infty} \dfrac{\omega/\omega_c}{\omega/\omega_c- p} \left(e^{{i\mkern 1mu}(\omega/\omega_c- p)\varphi}-1\right)J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right) - \left(e^{{i\mkern 1mu}(\omega/\omega_c\;\varphi - k_1 v_\perp/\omega_c\;\sin(\varphi))} - 1\right)\right].
   \end{aligned}

The component :math:`\hat{f}_1\vert_{\varphi=0}` is determined by
integrating
`[phy:equ:DispersionRelationIntegralEquationFull] <#phy:equ:DispersionRelationIntegralEquationFull>`__
for :math:`\varphi'` from :math:`0` to :math:`2\pi` and assuming that
:math:`\hat{f}_1` is periodic in the polar angle
:math:`\hat{f}_1\vert_{\varphi=0} = \hat{f}_1\vert_{\varphi=2\pi}`. We
insert :math:`\varphi=2\pi` in
`[phy:equ:DispersionRelationIntegralEquation,phy:equ:DispersionRelationReducedPhase,phy:equ:DispersionRelationReducedPhase,phy:equ:DispersionRelationIntegralEvaluated] <#phy:equ:DispersionRelationIntegralEquation,phy:equ:DispersionRelationReducedPhase,phy:equ:DispersionRelationReducedPhase,phy:equ:DispersionRelationIntegralEvaluated>`__
and obtain

.. math::

   \begin{aligned}
       &\left(e^{{i\mkern 1mu}\omega/\omega_c2\pi} - 1\right) \hat{f}_1\vert_{\varphi=0} \\
       = &\dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\left[\sum_{p=-\infty}^{\infty} \dfrac{\omega/\omega_c}{\omega/\omega_c- p} \left(e^{{i\mkern 1mu}(\omega/\omega_c- p) 2\pi}-1\right)J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right) - \left(e^{{i\mkern 1mu}\omega/\omega_c2\pi} - 1\right)\right].
   \end{aligned}

Using :math:`e^{{i\mkern 1mu}p 2\pi} = 1` with :math:`p\in\mathbb{Z}`,
we can simplify the last expression to

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationZeroPhaseDistributionFunction}
       \hat{f}_1\vert_{\varphi=0} = \dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\left[\sum_{p=-\infty}^{\infty} \dfrac{\omega/\omega_c}{\omega/\omega_c- p} J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right) - 1\right].
   \end{aligned}

Inserting
`[phy:equ:DispersionRelationZeroPhaseDistributionFunction] <#phy:equ:DispersionRelationZeroPhaseDistributionFunction>`__
into
`[phy:equ:DispersionRelationIntegralEquationEvaluated] <#phy:equ:DispersionRelationIntegralEquationEvaluated>`__
the expression for :math:`\hat{f}_1` becomes

.. math::

   \begin{aligned}
       &e^{{i\mkern 1mu}(\omega/\omega_c\;\varphi - k_1 v_\perp/\omega_c\;\sin(\varphi))} \hat{f}_1\\
       = &\dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2}\left[\sum_{p=-\infty}^{\infty} \dfrac{\omega/\omega_c}{(\omega/\omega_c- p)} e^{{i\mkern 1mu}(\omega/\omega_c- p)\varphi}J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right) - e^{{i\mkern 1mu}(\omega/\omega_c\;\varphi - k_1 v_\perp/\omega_c\;\sin(\varphi))}\right].
   \end{aligned}

The expression can further be simplified by canceling the phase
:math:`e^{{i\mkern 1mu}\omega/\omega_c\;\varphi}` and applying the
Jacobi–Anger identity of
`[phy:equ:DispersionRelationJacobiAngerIdentity] <#phy:equ:DispersionRelationJacobiAngerIdentity>`__
twice. The final expression for :math:`\hat{f}_1` is then given by

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDistributionFunctionExpression}
       \hat{f}_1 &=\dfrac{q}{m} \hat{\phi}_1 \dfrac{f_M}{{v_\text{th}}^2} \sum_{p',p=-\infty}^{\infty} \dfrac{p}{\omega/\omega_c- p} e^{{i\mkern 1mu}(p' - p)\varphi} J_{p'}\left(\dfrac{v_\perp k_1}{\omega_c}\right)J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right).
   \end{aligned}

Eq. `[phy:equ:DispersionRelationDistributionFunctionExpression] <#phy:equ:DispersionRelationDistributionFunctionExpression>`__
is the solution for :math:`\hat{f}_1` that has been derived in this
paragraph and will be used to calculate :math:`\hat{n}_1` in the next
paragraph.

Solving for :math:`\hat{n}_1`
'''''''''''''''''''''''''''''

In this paragraph we use
`[phy:equ:DispersionRelationDistributionFunctionExpression] <#phy:equ:DispersionRelationDistributionFunctionExpression>`__
to calculate the Fourier component of the density :math:`\hat{n}_1` by
integration on the velocity domain

.. math::

   \begin{aligned}
       \hat{n}_1(\textbf{k}, \omega) =& \iiint_{\mathbb{R}^3} \hat{f}_1(\textbf{k}, \textbf{v}, \omega) \mathrm{d}^3 v = \int_0^{2\pi}\int_0^{\infty}\int_{0}^{\infty} v_\perp \hat{f}_1(\textbf{k}, v_\parallel, v_\perp, \varphi, \omega) \mathrm{d}v_\parallel \mathrm{d}v_\perp \mathrm{d}\varphi\\
                                 =& \dfrac{q}{m} \hat{\phi}_1 \dfrac{1}{{v_\text{th}}^2}\\
                                 &\int_0^{2\pi}\int_0^{\infty}\int_{0}^{\infty} f_M(v_\perp,v_\parallel) v_\perp \sum_{p',p=-\infty}^{\infty} \dfrac{p}{\omega/\omega_c- p} e^{{i\mkern 1mu}(p' - p)\varphi} J_{p'}\left(\dfrac{v_\perp k_1}{\omega_c}\right)J_p\left(\dfrac{v_\perp k_1}{\omega_c} \right) \mathrm{d}v_\parallel \mathrm{d}v_\perp \mathrm{d}\varphi.
   \end{aligned}

We start with evaluating the integral on :math:`\varphi` which is given
by
:math:`\int_0^{2\pi} e^{{i\mkern 1mu}(p'-p)\varphi} \mathrm{d}\varphi = \delta_{p,p'}`
and will remove the double sum of :math:`p` and :math:`p'` due to the
Kronecker delta

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDensityIntegralPhi}
       \hat{n}_1(\textbf{k}, \omega) = \dfrac{q}{m} \hat{\phi}_1 \dfrac{1}{{v_\text{th}}^2} \int_0^{\infty}\int_{0}^{\infty} f_M(v_\perp,v_\parallel) v_\perp \sum_{p=-\infty}^{\infty} \dfrac{p}{\omega/\omega_c- p} J_p^2\left(\dfrac{v_\perp k_1}{\omega_c} \right) \mathrm{d}v_\parallel \mathrm{d}v_\perp.
   \end{aligned}

The remaining two integrals can be split and evaluated separately. The
Maxwellian is given by

.. math::

   \begin{aligned}
       f_M(v_\perp,v_\parallel) = \dfrac{n_0}{\pi^{3/2}{v_\text{th}}^3}e^{-v^2_\perp/(2{v_\text{th}}^2)}e^{-v^2_\parallel/(2{v_\text{th}}^2)}.
   \end{aligned}

We integrate separately

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDensityIntegralVParallel}
       \int_0^{\infty} e^{-v^2_\parallel/(2{v_\text{th}}^2)} \mathrm{d}v_\parallel = \sqrt{\pi}{v_\text{th}}
   \end{aligned}

and the perpendicular integral results in

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDensityIntegralVPerp}
       \int_{0}^{\infty} e^{-v^2_\perp/(2{v_\text{th}}^2)} v_\perp J_p^2\left(\dfrac{v_\perp k_1}{\omega_c} \right) \mathrm{d}v_\perp = e^{-(k_1{v_\text{th}}/\omega_c)^2}{v_\text{th}}^2 I_p\left(\left(\dfrac{k_1{v_\text{th}}}{\omega_c}\right)^2\right)
   \end{aligned}

We insert
`[phy:equ:DispersionRelationDensityIntegralVParallel,phy:equ:DispersionRelationDensityIntegralVPerp] <#phy:equ:DispersionRelationDensityIntegralVParallel,phy:equ:DispersionRelationDensityIntegralVPerp>`__
into
`[phy:equ:DispersionRelationDensityIntegralPhi] <#phy:equ:DispersionRelationDensityIntegralPhi>`__
and obtain the final result for the Fourier representation of the
density

.. math::

   \begin{aligned}
       \label{phy:equ:DispersionRelationDensityExpression}
       \hat{n}_1 = \dfrac{q n_0}{m {v_\text{th}}^2}\hat{\phi}_1 e^{-(k_1{v_\text{th}}/\omega_c)^2} \sum_{p=-\infty}^{\infty} \dfrac{p}{\omega/\omega_c- p} I_p\left(\left(\dfrac{k_1{v_\text{th}}}{\omega_c}\right)^2\right)
   \end{aligned}

with the modified Bessel function of the first kind :math:`I_p(z)`.

Dispersion relation
'''''''''''''''''''

In this last paragraph, we finally partially derive the dispersion
relation for
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.
We focus on the waves discussed by Schmitt (1973), namely pure and
neutralizing ion Bernstein waves. The last two paragraphs fully utilized
the symmetry in the polar plane, which is perpendicular to
:math:`\textbf{B}_0`. Therefore, we can now use the result from
`[phy:equ:DispersionRelationDistributionFunctionExpression,phy:equ:DispersionRelationDensityExpression] <#phy:equ:DispersionRelationDistributionFunctionExpression,phy:equ:DispersionRelationDensityExpression>`__
to calculate the dispersion relation in the direction perpendicular to
the magnetic field but parallel (:math:`k_y`) and perpendicular
(:math:`k_x`) to the flux surfaces. In this last paragraph, we normalize
all units of measure, which is also done in our simulations namely
:math:`T,q,m,\omega_c` are considered to be one.

We start with the waves already investigated by Raeth and Hallatschek
(2023) and are called neutralizing ion Bernstein waves by Schmitt
(1973). For these waves, we need to assume adiabatic electrons, which is
the second equation of
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.
Here we need to use :math:`k_1` from the last paragraphs in the
direction :math:`k_y`. The dispersion relation will describe the modes
:math:`\textbf{k} = (0,k_y,0)`. The dispersion relation is retrieved
from the second equation of the linearized Fourier system in
`[phy:equ:DispersionRelationFourierSystem] <#phy:equ:DispersionRelationFourierSystem>`__
with normalized units which reads

.. math::

   \begin{aligned}
           \hat{\phi}_1 = \hat{n}_1.
   \end{aligned}

We insert the Fourier component of the density from
`[phy:equ:DispersionRelationDensityExpression] <#phy:equ:DispersionRelationDensityExpression>`__
and use the identity. The dispersion relation for neutralizing ion
Bernstein waves is then given by

.. math::

   \begin{aligned}
       \hat{\phi}_1 &= \hat{\phi}_1 e^{-k_y^2} \sum_{p=-\infty}^{\infty} \dfrac{p}{\omega - p} I_p\left(k_y^2\right)
   \end{aligned}

which can be simplified using the identity
:math:`\sum_{p=-\infty}^{\infty}I_p(x) = 1` to

.. math::

   \begin{aligned}
       \label{phy:equ:NeutralizingIonBernsteinWaves}
       0 &= \hat{\phi}_1 e^{-k_y^2} \sum_{p=-\infty}^{\infty} \dfrac{\omega}{\omega - p} I_p\left(k_y^2\right)
   \end{aligned}

Secondly, we calculate the dispersion relation for modes perpendicular
to flux surfaces. This behavior is introduced with the extension
motivated in `1.2 <#sec:NetZeroChargeTransport>`__. Namely, we remove
the net charge transport between flux surfaces such that the particle
density on a flux surface is constant. This is achieved by the third
equation in
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.
If we remove the net charge transport between flux surfaces we obtain
the dispersion relation for waves called pure ion Bernstein waves by
Schmitt (1973). To our knowledge, these waves have not been reproduced
in a numerical setup. The modes :math:`k_1` from the last two paragraphs
must be taken into the direction :math:`k_x`. The dispersion relation
will describe modes :math:`\textbf{k}=(k_x,0,0)`. The dispersion
relation is calculated from the last equation of the linearized Fourier
system in
`[phy:equ:DispersionRelationFourierSystem] <#phy:equ:DispersionRelationFourierSystem>`__

.. math::

   \begin{aligned}
       -{i\mkern 1mu}\omega\hat{n}_1 = 0.
   \end{aligned}

We calculate the dispersion relation of this system by inserting the
Fourier representation of the particle density in
`[phy:equ:DispersionRelationDensityExpression] <#phy:equ:DispersionRelationDensityExpression>`__
into the previous equation

.. math::

   \begin{aligned}
       \label{phy:equ:PureIonBernsteinWaves}
       -{i\mkern 1mu}\omega \hat{\phi}_1 \left[e^{-k_x^2} \sum_{p=-\infty}^{\infty} \dfrac{p}{\omega - p} I_p\left(k_x^2\right)\right] = 0
   \end{aligned}

Eqs. `[phy:equ:NeutralizingIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves>`__
and `[phy:equ:PureIonBernsteinWaves] <#phy:equ:PureIonBernsteinWaves>`__
contain two dispersion relations for decoupled modes :math:`(k_x,0,0)`
and :math:`(0,k_y,0)` from the system of kinetic ions with adiabatic
electrons and constant flux surface averages given in
`[phy:equ:ConstantFluxAvgSystemOfEqu] <#phy:equ:ConstantFluxAvgSystemOfEqu>`__.
In the next section, these results will be used to verify the behavior
of our algorithm. Within a single simulation we should observe the
dispersion relation :math:`\omega(0,k_y,0)` from
`[phy:equ:NeutralizingIonBernsteinWaves] <#phy:equ:NeutralizingIonBernsteinWaves>`__
and :math:`\omega(k_x,0,0)` from
`[phy:equ:PureIonBernsteinWaves] <#phy:equ:PureIonBernsteinWaves>`__. We
omit the behavior of coupled modes.

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-bernstein_waves_1958

      Bernstein, Ira B. 1958. “Waves in a Plasma in a Magnetic Field.”
      *Phys. Rev.* 109: 10–21. https://doi.org/10.1103/PhysRev.109.10.

   .. container:: csl-entry
      :name: ref-chen_introduction_2016

      Chen, F. F. 2016. *Introduction to Plasma Physics and Controlled
      Fusion*. 3rd ed. 2016. Cham: Springer.

   .. container:: csl-entry
      :name: ref-duistermaat_distributions_2010

      Duistermaat, J. J., and J. A. C. Kolk. 2010. *Distributions:
      Theory and Applications*. Boston: Birkhäuser Boston.

   .. container:: csl-entry
      :name: ref-kennel_velocity_1966

      Kennel, C. F., and F. Engelmann. 1966. “Velocity Space Diffusion
      from Weak Plasma Turbulence in a Magnetic Field.” *The Physics of
      Fluids* 9: 2377–88. https://doi.org/10.1063/1.1761629.

   .. container:: csl-entry
      :name: ref-lafitte_uniqueness_2018

      Lafitte, Olivier, and Omar Maj. 2018. “Uniqueness of the Cauchy
      Datum for the Tempered-in-Time Response and Conductivity Operator
      of a Plasma.” arXiv. https://doi.org/10.48550/ARXIV.1805.08733.

   .. container:: csl-entry
      :name: ref-lifshits_physical_1981

      Lifshitz, E. M., L. P. Pitaevskii, and L. D. Landau. 1981.
      *Physical Kinetics*. Course of Theoretical Physics, v. 10. Oxford
      ; New York: Pergamon Press.

   .. container:: csl-entry
      :name: ref-raeth_high_2023

      Raeth, Mario, and Klaus Hallatschek. 2023. “High Frequency
      Non-Gyrokinetic Turbulence at Tokamak Edge Parameters.” *Physical
      Review Letters* [submitted]. https://arxiv.org/pdf/2310.15981.

   .. container:: csl-entry
      :name: ref-schmitt_dispersion_1973

      Schmitt, J. P. M. 1973. “Dispersion and Cyclotron Damping of Pure
      Ion Bernstein Waves.” *Phys. Rev. Lett.* 31: 982–86.
      https://doi.org/10.1103/PhysRevLett.31.982.

   .. container:: csl-entry
      :name: ref-scott_fluid_2021

      Scott, Bruce. 2021. *Fluid Drift Turbulence*. volume 1.
      Bristol,UK: IOP Publishing.

   .. container:: csl-entry
      :name: ref-zoric_mathematical_2016

      Zorič, Vladimir A. 2015. *Mathematical Analysis. 1*. Second
      Edition. Berlin Heidelberg: Springer.

.. [1]
   As long as :math:`\vert q_e\vert = \vert q_i\vert`, which we have
   assumed.

.. |image1| image:: ../_static/bsl6dDocStorage/constantFluxAvgFieldSolver/ConstFluxAvgConvergenceFixedGrid.pdf
   :width: 100.0%
.. |image2| image:: ../_static/bsl6dDocStorage/constantFluxAvgFieldSolver/IBWRotatingDispersionRelation.pdf
.. |image3| image:: ../_static/bsl6dDocStorage/constantFluxAvgFieldSolver/IBWRotatingPreservedQuantities.pdf
.. |image4| image:: ../_static/bsl6dDocStorage/constantFluxAvgFieldSolver/IBWFixedDispersionRelation.pdf

.. _field-solvers:

Field solvers
=============

This section documents field solvers which are implemented in BSL6D.

The main field solver currently focuses on quasi neutrality condition with adiabatic electrons in different flavors.

.. doxygenfile:: baseFieldSolver.hpp
.. doxygenfile:: quasiNeutral.hpp


.. toctree::
  :maxdepth: 1

  ConstantFluxSurfaceAverage.rst


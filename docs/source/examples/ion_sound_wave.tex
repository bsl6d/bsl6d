\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{caption}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{bm}
\usepackage[backend=bibtex,style=alphabetic,natbib=true]{biblatex}
\addbibresource{../Bibliographie.bib}

\newcommand{\kperp}{\ensuremath{k_{\perp}}}
\renewcommand{\vec}[1]{\ensuremath{\bm{#1}}}
\newcommand{\ub}{\ensuremath{\hat{\vec z}}}
\newcommand{\at}[2]{\ensuremath{\left (  #1 \right ) _{#2}}}
\newcommand{\parac}[1]{\ensuremath{#1_{z}}}
\newcommand{\perpc}[1]{\ensuremath{#1_{\perp}}}
\newcommand{\mat}[1]{\ensuremath{\bm{\mathrm{#1}}}}
\newcommand{\intv}[1]{\ensuremath{\int #1 \mathrm d ^3 v}}
\newcommand{\dn}[1]{\ensuremath{\mathrm d^{#1}}}

\begin{document}

\section{Landau Damping: Ion Sound Wave}

Ion Landau damping, colloquially known as the ion sound wave, is an instance of
Landau damping. In contrast to the standart example of Landau damping, in which
electrons oscillate around a fixed ion background, the ion sound wave occurs in
a completely self-consistent Vlasov-Maxwell setup in the absence of magnetic
fields. This damped ion density wave and the "standard" electron damped wave
coexist, however, the electron wave is "invisible" in the ion sound wave limit
due to its much higher frequency. In other words, the electron wave is damped
very quickly on ion timescales; in the case of adiabatic electrons, which we
will assume in the following, the damping is even infinitely fast.

\subsection{Theoretical Background}

We work in the linear regime, i.e. $f(x,v,t) = f_0(v) + f_1(x,v,t)$, and solve
the linearised, self-consistent Vlasov-Maxwell system. For simplicity, we work
in one dimension (s = species):
\begin{align}
     & \partial_t f^s_1 + v \nabla f^s_1 - \tfrac{q}{m} \nabla \phi \nabla_v f^s_0 = 0 \\
     & - \Delta \phi = \frac{e}{\epsilon_0} \int d^3v(f_1^i-f_1^e)
\end{align}

During the calculation one finds the plasma dielectric function:

\begin{align}
    \epsilon(k,\omega) = k^2 - \frac{1}{2 \lambda_D^2} Z'(\delta_e) - \frac{1}{2 \lambda_D^2}Z'(\delta_i)
\end{align}

Here, $\lambda_D$ is the Debye length, $\delta=\sqrt{\frac{m}{2 k_B T}}\frac{\omega}{k}$ and $Z(\delta)$ the plasma dispersion function:

\begin{align}
    Z(\delta) = \frac{1}{\sqrt{\pi}}\int_{-\infty}^{\infty} \frac{e^{-t^2}}{t - \delta} dt
\end{align}

In fact, the dielectric function is also the plasma dispersion relation, since
its roots determine the complex poles of the plasma's response to an initial
perturbation.  In return, these poles determine the individual modes of the
plasma response in real space after Fourier back-transformation; this can be
partly seen by (\ref{PlasmaResponse}), but requires a more detailed
investigation beyond the scope of this documentation.  Now, part of our
assumption $f(x,v,t) = f_0(v) + f_1(x,v,t)$ is that $f_0(v) = f_M(v)$. For an
initialisation with a pure ion density perturbation, the self-consistent
electric potential can be shown to be:

\begin{align}
    \tilde{\phi}(k, \omega) = \frac{ie}{\epsilon_0}\frac{1}{\epsilon(k, \omega)} \int d^3 v \frac{f_M(v)}{kv - w} \delta(k-k_0)
\end{align}

Taking the quasi-neutral $\lambda_D \rightarrow 0$ and the adiabatic $m_e
\rightarrow 0$ limit as well as $T_e = T_i = 1$ and $m_i = k_B = 1$, one
arrives at the final expression for the plasma response to the initial ion
density perturbation. The calculation can be simplified by anticipating that
the quasi-neutral and adiabatic limit implies $\phi = n_i = n_e$:

\begin{align}
    \label{PlasmaResponse}
    \tilde{\rho}(k, \omega) = i \alpha k \delta(k - k_0) \frac{Z(\delta)}{2 + \delta Z(\delta)}
\end{align}

A Fourier back-transformation of (\ref{PlasmaResponse}) in $\omega$ allows to
follow a specific k-mode of the initial density perturbation in time.  In other
words, if the initial density perturbation is chosen to be a simple sine, one
can directly observe the the damping of the ion density perturbation
oscillation at a given point in real space via (\ref{PlasmaResponse}). It also
opens up the possibility to investigate the accuracy of several k-modes at once
by initialising with a random density perturbation. The Fourier spectrum of
such random initialised data can then be easily compared to the analytical
solution by comparing each mode with (\ref{PlasmaResponse}) individually. 

\subsection{Simulation Configuration and Results}

To analyse the numerical errors of BSL6D in the ion sound wave scenario, we
performed a 1D1V simulation with 1500 $\times$ 1500 points in a domain of
length 2$\pi$, simulation time T = 2 and a random density initialisation on .
Figure \ref{result1} shows a plot of the ion sound wave:

\begin{figure}[h]
    \label{result}
    \begin{center}
        \includegraphics[width =  0.6\textwidth]{../_static/bsl6dDocStorage/ionSoundWave/isw.png}
    \end{center}
\end{figure}

There is no particular reason for the choice of k=3 apart from filling the plot
nicely. Now, as mentioned above, we can analyse both the time splitting error
and the interpolation error qualitatively at the same time; the result is shown
in \ref*{results} and requires some explanation. First, the time splitting
error of BSL6D is expected to be of order $(\Delta t)^2$. A simple integral
transformation of the Fourier back-transformation of (\ref{PlasmaResponse})
shows that any k-dependence of the integrand can be eliminated. In turn, the
usual Fourier exponent of $i \omega t$ turns into $i \omega k t$, which shows
that the plasma response is actually the same for all modes, but with a
transformed time coordinate. For example, the function values of the k=1 mode
appear at time $t \rightarrow 0.5 t$ for the k=2 mode. As a result, one expects
the time splitting error to show up as a $k^2$ dependence in the regime of
dominating time splitting error, which is clearly seen in the plot. As soon as
we move to higher k-modes, which means that we loose spatial resolution, the
interpolation error starts to dominate. If we consider a single k-mode, i.e. a
complex exponential multiplied with a complex amplitude, we easily see from the
Lagrangian remainder formula that a single Lagrange interpolation will result
in an error proportional to $k^{d+1}$ where $d$ is the degree of the lagrange
stencil. In the present case, this implies an error of order $k^8$, since we
used a stencil of length eight. However, for the analysis we used the values of
each mode at the first minimum, which occurs at earlier times $t_{k1}
\rightarrow t_{k1}/k$ for higher k, which implies that higher k-mode values
collect less interpolation error due to the shorter simulation time. The
precise error can be calculated as follows (here, $s$ is the error from the
Lagrangian remainder formula):
\begin{align*}
    e^{ikx} \rightarrow& (1 + s)^N e^{ikx} \\
            =& (1 + Ns + ...) e^{ikx} \\
             =& (1 + \frac{k T}{\Delta t}s + ...)e^{ikx}
\end{align*}
Therefore, the asymptotic error is proportional to $k^7$:
\begin{figure}[h]
    \label{results}
    \begin{center}
        \includegraphics[width =  0.6\textwidth]{../_static/bsl6dDocStorage/ionSoundWave/isw_convergence.png}
    \end{center}
    \end{figure}

\printbibliography[heading=bibintoc]

\end{document}

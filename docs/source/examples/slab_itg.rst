Simulation of ion temperature gradient driven modes (ITG) 
==========================================================

Description of temperature gradient
-----------------------------------

For the simulation of ion temperature gradient (ITG) driven modes, the
gradient is introduced in Boussinesq approximation. For this purpose, we
assume that a background distribution function :math:`f_0` exists, which
fulfills (Raeth, Hallatschek, and Kormann 2024)

.. math::

   \begin{aligned}
   \ensuremath{\bm{v}} \cdot \nabla f_0 + \ensuremath{\bm{v}} \times \hat{\ensuremath{\bm{z}}} \cdot \nabla_v f_0 = 0.
   \end{aligned}

This condition is met, when the background distribution is parameterized
by an arbitrary function :math:`g` with
:math:`f_0(\ensuremath{\bm{r}}, \ensuremath{\bm{v}}) = f_0(\ensuremath{\bm{R}} + \ensuremath{\bm{\rho}}, \ensuremath{\bm{v}}) = g(\ensuremath{\bm{R}}, \ensuremath{v_{\perp}}, \ensuremath{v_{z}})`
(with
:math:`\ensuremath{v_{z}} = \ensuremath{\bm{v}} \cdot \hat{\ensuremath{\bm{z}}}`,
:math:`\ensuremath{v_{\perp}} = |\ensuremath{\bm{v}} \times \ensuremath{\hat{\ensuremath{\bm{z}}}}|`
and
:math:`\ensuremath{\bm{R}} = \ensuremath{\bm{r}} - \ensuremath{\bm{\rho }}= \ensuremath{\bm{r}} + \ensuremath{\bm{v}} \times \ensuremath{\bm{\hat}}{\ensuremath{\bm{z}}}`
is the location of the gyrocenter of a given particle, where
:math:`\ensuremath{\bm{r}}` is the configurations space coordinate and
:math:`\ensuremath{\bm{\rho}}=\ensuremath{\hat{\ensuremath{\bm{z}}}}\times \ensuremath{\bm{v}}`
is the Larmor radius vector). The background distribution is chosen to
resemble a Maxwellian with varying temperature
:math:`T(\ensuremath{\bm{R}})`, and thus reads

.. math::

   \begin{aligned}
   g_0(\ensuremath{\bm{R}}, \ensuremath{v_{\perp}}, \ensuremath{v_{z}}) :=\left( \frac{1}{2\pi T(\ensuremath{\bm{R}})}\right)^{\frac 32} \exp\left(-\frac{ [\ensuremath{v_{\perp}} ^2 + \ensuremath{v_{z}}^2]}{2 T(\ensuremath{\bm{R}})}\right).
   \end{aligned}

When splitting the distribution function in the Vlasov equation into a
perturbation :math:`\delta f = f- g_0` and the background :math:`g_0`,
one obtains

.. math::

   \begin{aligned}
   \partial_t \delta f + \ensuremath{\bm{v}}\cdot \nabla \delta f + \left[-\nabla \phi+(\ensuremath{\bm{v}}\times \hat{\ensuremath{\bm{z}}})\right] \cdot \nabla_v \delta f =  \nabla \phi\cdot \nabla_v g_0,
   \end{aligned}

where the gradient on the right-hand side is given by (for
clarification, we introduce a notation to specify which variable is held
constant for the partial derivative, i.e.
:math:`\ensuremath{\left (  \nabla_{\ensuremath{\bm{v}}} f \right ) _{\ensuremath{\bm{r}}}}`
for a derivative with respect to :math:`\ensuremath{\bm{v}}` at fixed
:math:`\ensuremath{\bm{r}}`)

.. math::

   \begin{aligned}
   \ensuremath{\left (  \nabla_v g_0 \right ) _{\ensuremath{\bm{r}}}} &= \ensuremath{\hat{\ensuremath{\bm{z}}}}\times \ensuremath{\left (  \nabla_{\ensuremath{\bm{R}}} g_0 \right ) _{\ensuremath{\bm{v}}}} + \ensuremath{\left (  \nabla_v g_0 \right ) _{\ensuremath{\bm{R}}}}  =: {\ensuremath{\bm{v}}^*}g_0 + \ensuremath{\left (  \nabla_v g_0 \right ) _{\ensuremath{\bm{R}}}}\notag \\
   &= -g_0 \ensuremath{\hat{\ensuremath{\bm{z}}}}\times \left( \frac{ \nabla T}{T} \frac{3 -  (\ensuremath{v_{\perp}} ^2 + \ensuremath{v_{z}}^2)}{2 }\right) +  \ensuremath{\left (  \nabla_v g_0 \right ) _{\ensuremath{\bm{R}}}} .\label{eq_source_term}
   \end{aligned}

The limit that the gradient length is infinite
:math:`T(\ensuremath{\bm{R}}) \rightarrow T = 1` is taken, such that the
background distribution is independent of :math:`\ensuremath{\bm{R}}`.
Thus, :math:`g_0` is replaced with the homogeneous Maxwellian
distribution
:math:`f_M = \frac{1}{(2\pi)^{\frac 32}}e^{-\frac {v^2}{2}}`. For
simplicity, we assume that the electrons are adiabatic
:math:`f_e = e^{-\phi}f_M \approx f_M - \phi f_M` and the plasma is
quasineutral :math:`n_e=n` (with the electron density :math:`n_e`), the
complete system of equations is given by

.. math::

   \begin{aligned}
   \partial_t f + \ensuremath{\bm{v}} \cdot \nabla f + \left( - \nabla \phi + \ensuremath{\bm{v}} \times \ensuremath{\hat{\ensuremath{\bm{z}}}}\right)\cdot \nabla_v f = 
   \ensuremath{\bm{v}}^* \cdot \nabla \phi f_M, \label{eq_temperature_gradient_system} 
   \end{aligned}

.. math::

   \begin{aligned}
   \phi =  n, \quad \quad \quad\vspace{2cm}  n = \int f \ensuremath{\mathrm d^{3}}v. \label{eq_field_equation}
   \end{aligned}

Dispersion relation
-------------------

The dispersion relation can be written, using the plasma dispersion
function which is defined for :math:`{\rm Im}(x)>0` as

.. math::

   \begin{aligned}
   Z(x) = \frac{1}{\sqrt\pi} \int_{-\infty}^\infty \frac{e^{-t^2}}{(t-x)}\mathrm d t,
   \end{aligned}

and is analytically continued for :math:`{\rm Im}(x)\leq 0`. After
solving the perpendicular integral, we introduce
:math:`\Gamma_n(x^2) = e^{-x^2}I_n(x^2)`. Resulting in the expression
(Brambilla 1998)

.. math::

   \begin{aligned}
   \frac{n_k}{\phi_k} = \left[\omega- k_y\frac{\nabla T}{T}\partial_\xi\right] \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}\sum_{p \in  Z}  Z\left(\frac{\omega+p}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_n\left(\frac{\ensuremath{k_{\perp}}^2}{\xi}\right)-1.\label{eq_density_response}
   \end{aligned}

The density response is introduced in the quasi-neutrality equation
which gives the electrostatic potential form equation
(`[eq_field_equation] <#eq_field_equation>`__). The resulting dispersion
relation, given by solution :math:`\omega(\ensuremath{\bm{k}})` to the
equation

.. math::

   \begin{aligned}
   0=\left[\omega - k_y\frac{\nabla T}{T}\partial_\xi\right]  \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}\sum_{p \in  Z}  Z\left(\frac{\omega+p}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_n\left(\frac{\ensuremath{k_{\perp}}^2}{\xi}\right)-2, \label{eq_dispersion_relation}
   \end{aligned}

describes all modes for the 6D kinetic system. In the gyrokinetic limit
:math:`\omega\ll1` and :math:`k_z\ll 1` all terms of the sum vanish
except the :math:`p=0` contribution, simplifying the dispersion relation
to

.. math::

   \begin{aligned}
   0=\left[\omega - k_y\frac{\nabla T}{T}\partial_\xi\right] \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}Z\left(\frac{\omega}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_0\left(\frac{\ensuremath{k_{\perp}}^2}{\xi}\right)-2. \label{eq_gyro_dispersion_relation}
   \end{aligned}

Simulation Configuration and Information
----------------------------------------

The configuration for the simulation above can be retrieved using

::

       ./bsl6d_integrator --get_config slab_itg_boussinesq

It was run until 2000 time units. The provided config file in this
documentation has a lower resolution and a significantly shorter
simulation time :math:`T` to reduce the computational effort in our CI.
Beside these two differences the config file is the same as was used to
obtain the results below.

The simulation took around 2 h on a GPU node of the `MPCDF Raven
cluster <https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html>`__.
The config file can be retrieved from a bsl6d executable by adding the
flag

::

   ./bsl6d_integrator --bsl6d-get-simulation-config slab_itg_boussinesq

Result
------

The plot presented below is generated using the script
`analysis.py <../../../examples/quasineutral_integrators/slab_itg/analysis.py>`__.
It illustrates the linear growth of the instability on the right and
includes a comparison of the growth rate with the results obtained from
the analytical dispersion relation
`[eq_dispersion_relation] <#eq_dispersion_relation>`__ on the right
side. A reasonable agreement is observed between the analytical and
numerical results. The discrepancies for larger values of :math:`k_y`
are attributed to numerical damping resulting from Lagrange
interpolation.

.. container:: float
   :name: fig:dispersion_relation

   .. container:: center

      |image1|

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-brambilla_kinetic_1998

      Brambilla, Marco. 1998. *Kinetic Theory of Plasma Waves:
      Homogeneous Plasmas*. Oxford University Press.

   .. container:: csl-entry
      :name: ref-raeth_simulation_2024

      Raeth, M., K. Hallatschek, and K. Kormann. 2024. “Simulation of
      ion temperature gradient driven modes with 6D kinetic Vlasov
      code.” *Physics of Plasmas* 31 (4): 042101.
      https://doi.org/10.1063/5.0197970.

.. |image1| image:: ../_static/bsl6dDocStorage/slabITG/growth_rate.png

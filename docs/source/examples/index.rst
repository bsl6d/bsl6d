Simulation examples
===================

The following sections describe several simulations that can be run using
the BSL6D code. In the following we really briefly describe the physical
equations that can be solved with the BSL6D code.

Vlasov Equation
---------------
The main kernel of the BSL6D code uses the semi-Lagrangian method to solve
the 6D Vlasov equation. Lower dimensional versions can also be run but 
optimizations and tests focus on the 6D-Vlasov equation.

.. math::

   \partial_t f(\textbf{x},\textbf{v},t) + \textbf{v}\nabla_x f(\textbf{x},\textbf{v},t) + \frac{q}{m}\left( \textbf{E}(\textbf{x},t) + \textbf{v}\times\textbf{B}(\textbf{x},t)\right)\nabla_v f(\textbf{x},\textbf{v},t) = 0

With an initial condition :math:`f(\textbf{x},\textbf{v},0)` the BSL6D code can 
advect :math:`f` in time until a final time :math:`f(\textbf{x},\textbf{v},T)` 
where :math:`T` is provided in the input parameters. In the current stage of 
the development only a single particle species can be described by :math:`f`.

The fields given in the equation above have to be calculated selfconsistently 
through :math:`f`. So far only a constant background magnetic field has been 
implemented :math:`\textbf{B}(\textbf{x}) = \hat{\textbf{e}}_z` or no magnetic
field at all can be used :math:`\textbf{B}(\textbf{x}) = \textbf{0}`. The 
electric field can be solved using different field equations.

Field Equations
---------------

Quasineutrality and adiabatic electrons
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We can explicitly simulate ions (:math:`q>0`) and implicitly simulate the 
electron behavior by assuming quasineutrality (:math:`n_i=n_e`) and adiabatic 
electrons. With this setup the electric potential can be solved using 

.. math::

   \dfrac{e}{T_e} \phi(\textbf{x},t) &= \delta n_e(\textbf{x},t)

   n_e(\textbf{x},t) &= 1 + \delta n_e(\textbf{x},t) = n_i(\textbf{x},t) = \int f(\textbf{x},\textbf{v},t) \text{d} \textbf{v}

where :math:`n_e` is the electron particle density which is equal to the ion particle 
density :math:`n_i` and :math:`\phi` the electric potential. The electric potential then
determines the electric field through

.. math::

   \textbf{E}(\textbf{x},t) = -\nabla \phi(\textbf{x},t)

ToDo: Add a link to quasineutral field solver and move the description there

Poisson equation with constant ion background
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We can assume a constant ion background particle density :math:`n_i(\textbf{x})=1` and
simulate the movement of electrons (:math:`q<0`) within these fixed ions by solving 
Poissons equation

.. math::

   \Delta \phi &= 1 + q_e n_e(\textbf{x},t)

   n_e(\textbf{x},t) &= 1 + \delta n_e(\textbf{x},t) = \int f(\textbf{x},\textbf{v},t) \text{d}\textbf{v}

where :math:`n_e` is the electron particle density which is equal to the ion particle 
density :math:`n_i` and :math:`\phi` the electric potential. The electric potential then
determines the electric field through

.. math::

   \textbf{E}(\textbf{x},t) = -\nabla \phi(\textbf{x},t)

ToDo: Add a link to quasineutral field solver and move the description there

.. toctree::
   :caption: Contents:
   :hidden:
  
   ion_sound_wave.rst
   slab_itg.rst
   itg_nonlinear_gradient.rst
   stable_neutralizing_ion_bernstein_waves.rst
  

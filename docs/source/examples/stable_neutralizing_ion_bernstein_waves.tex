\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{caption}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage[backend=bibtex,style=alphabetic,natbib=true]{biblatex}
\addbibresource{../Bibliographie.bib}

\begin{document}

\section{Neutralizing Ion Bernstein Waves (IBWs)}
Neutralizing IBWs can be simulated using the quasi neutrality with adiabatic
electrons if the models described in the above section are considered.

\subsection{Theoretical Background}
The existence of high-frequency waves in the Vlasov system can be shown from
the dispersion relation \citep{brambilla_kinetic_1998}

\begin{align}
   \label{equ:stable_ibw:dispersion_relation}
   0 = \left[\omega - k_y\frac{\nabla T}{T}\partial_\xi\right] \frac{1}{|k_z|}\sqrt{\frac \xi 2} e^{-\frac{k_{\perp}^2}{\xi}}\sum_{p \in \mathbb{Z}}  Z\left(\frac{\omega+p}{|k_z|} \sqrt{\frac{\xi}{2}}\right)I_p\left(\frac{k_{\perp}^2}{\xi}\right)-2.
\end{align}

With $\frac{\nabla T}{T}=0$ and $\xi = 1$, the dispersion relation
eq.~\eqref{equ:stable_ibw:dispersion_relation} reads

\begin{align}
   \label{eq. dispersion_relation_stable}
   0 = \frac{\omega}{2|k_z|} \sum_{p \in \mathbb{Z}}  Z\left(\frac{\omega+p}{\sqrt{2}|k_z|}	\right)\Gamma_n-2 ,
\end{align}

with $\Gamma_n = e^{-k_{\perp}^2}I_n(k_{\perp}^2)$.

In case of fast oscillations in the perpendicular plane, any parallel motion
has a small to negligible influence as the parallel transition time is much
larger than the Larmor period $\omega_{\mathrm{c}} \gg k_z v_{\mathrm {th}}$,
and thus, the limit $k_z \rightarrow 0$ can be taken which reduces the plasma
dispersion function $Z(x)$ to:
\begin{align}
   \lim_{{k_z \rightarrow 0}}	\frac{1}{\sqrt{2 |k_z|}}Z\left(\frac{\omega+p}{\sqrt{2|k_z|}} \right) = \frac{1}{\omega + p}.
\end{align}

The dispersion relation results in normalized units of the
BSL6D code is then given by
\begin{align}
   \label{equ:stable_ibw:dispersion_relation_bsl6d}
   0 =  \omega  \sum_{p \in \mathbb{Z}} \frac{\Gamma_n}{\omega + p}-2 =  \Gamma_0+\omega \sum_{\substack{p \in \mathbb{Z} \\ p \neq 1}} \frac{\Gamma_n}{\omega + p}-2.
\end{align}

\subsection{Simulation Configuration and Information}
The simulation was conducted using a high resolution of the spatial domain with
128 points in all directions. Additionally, it was run until 500 time units.
The provided config file in this documentation has a lower resolution and a
significantly shorter simulation time $T$ to reduce the computational effort in
our CI. Beside these two differences the config file is the same as was used to
obtain the results below.

The simulation took around 90~min on a GPU node of the
\href{https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html}{MPCDF Raven cluster}.
The config file can be retrieved from a bsl6d executable by adding the flag
\begin{verbatim}
   ./bsl6d_integrator --get_config stable_neutralizing_ion_bernstein_waves
\end{verbatim}

\subsection{Simulation Results}
The dispersion relation is calculated from the particle density
$n(\textbf{x},t) = \int f(\textbf{x},\textbf{v}, t) \mathrm{d}v$ where the particle
distribution function is integrated in time by the executable obtained by the
source file
\href{https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d/-/blob/master/examples/bsl6d_integrator.cpp}{quasineutral integrators}.

The data is stored in the directory \verb|bsl6dDiagnostics| within the
directory where the simulation is executed. Finally, the obtained particle
density time series is Fourier transformed in time and space. The dispersion
relation below in figure~\ref{fig:dispersion_relation} transforms the timeframe
1.0 to 1000.0 to the frequency space. The visible branches are in agreement
with the theoretical values obtained by
equation~\eqref{equ:stable_ibw:dispersion_relation_bsl6d}. We used the rotating
grid which is presented in \cite{schild_convergence_2024}, and compared it with
a Strang-Splitting approach for a fixed grid. The results in the plot clearly
state that the rotating grid approach is advantageous compared to the fixed
grid.

\begin{figure}
   \begin{center}
      \includegraphics[scale=0.6]{../_static/bsl6dDocStorage/neutralizingIBW/neutralizingWaves_dispersionRelation_fixedGridRotatingGrid.png}
      \caption{\label{fig:dispersion_relation}Comparing the analytical dispersion
         relation from equation~\eqref{equ:stable_ibw:dispersion_relation_bsl6d} with
         the dispersion relation of the particle density obtained using the BSL6D
         code.}
   \end{center}
\end{figure}

\printbibliography[heading=bibintoc]

\end{document}

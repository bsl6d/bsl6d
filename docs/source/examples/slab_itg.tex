\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage{caption}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{bm}
\usepackage[backend=bibtex,style=alphabetic,natbib=true]{biblatex}
\addbibresource{../Bibliographie.bib}



\newcommand{\kperp}{\ensuremath{k_{\perp}}}
\renewcommand{\vec}[1]{\ensuremath{\bm{#1}}}
\newcommand{\ub}{\ensuremath{\hat{\vec z}}}
\newcommand{\at}[2]{\ensuremath{\left (  #1 \right ) _{#2}}}
\newcommand{\parac}[1]{\ensuremath{#1_{z}}}
\newcommand{\perpc}[1]{\ensuremath{#1_{\perp}}}
\newcommand{\mat}[1]{\ensuremath{\bm{\mathrm{#1}}}}
\newcommand{\intv}[1]{\ensuremath{\int #1 \mathrm d ^3 v}}
\newcommand{\dn}[1]{\ensuremath{\mathrm d^{#1}}}


\begin{document}
\section{Simulation of ion temperature gradient driven modes (ITG) }
\subsection{Description of temperature gradient}
For the simulation of ion temperature gradient (ITG) driven modes, the gradient is introduced in Boussinesq approximation. For this purpose, we assume that a background distribution function $f_0$ exists, which fulfills  \cite{raeth_simulation_2024}
\begin{align}
\vec v \cdot \nabla f_0 + \vec v \times \hat{\vec z} \cdot \nabla_v f_0 = 0.
\end{align}
This condition is met, when the background distribution is parameterized by an arbitrary function $g$ with $f_0(\vec r, \vec v) = f_0(\vec R + \vec \rho, \vec v) = g(\vec R, \perpc v, \parac v)$ (with $\parac v = \vec v \cdot \hat{\vec z}$, $\perpc v = |\vec v \times \ub|$ and $\vec R = \vec r - \vec \rho = \vec r + \vec v \times \vec \hat{\vec z} $ is the location of the gyrocenter of a given particle, where $\vec r$ is the configurations space coordinate and $\vec\rho=\ub \times \vec v$ is the Larmor radius vector). The background distribution is chosen to resemble a Maxwellian with varying temperature $T(\vec R)$, and thus reads
\begin{align}
g_0(\vec R, \perpc v, \parac v) :=\left( \frac{1}{2\pi T(\vec R)}\right)^{\frac 32} \exp\left(-\frac{ [\perpc v ^2 + \parac v^2]}{2 T(\vec R)}\right).
\end{align}
When splitting the distribution function in the Vlasov equation into a perturbation $\delta f = f- g_0$ and the background $g_0$, one obtains 
\begin{align}
\partial_t \delta f + \vec v\cdot \nabla \delta f + \left[-\nabla \phi+(\vec v\times \hat{\vec z})\right] \cdot \nabla_v \delta f =  \nabla \phi\cdot \nabla_v g_0,
\end{align}
where the gradient on the right-hand side is given by (for clarification, we introduce a notation to specify which variable is held constant for the partial derivative, i.e. $\at{\nabla_{\vec v} f}{\vec r} $ for a derivative with respect to $\vec v$ at fixed $\vec r$)
\begin{align}
\at{\nabla_v g_0}{\vec r} &= \ub \times \at{\nabla_{\vec R} g_0}{\vec v} + \at{\nabla_v g_0}{\vec R}  =: {\vec v^*}g_0 + \at{\nabla_v g_0}{\vec R}\notag \\
&= -g_0 \ub \times \left( \frac{ \nabla T}{T} \frac{3 -  (\perpc v ^2 + \parac v^2)}{2 }\right) +  \at{\nabla_v g_0}{\vec R} .\label{eq_source_term}
\end{align}
The limit that the gradient length is infinite $T(\vec R) \rightarrow T = 1$ is taken, such that the background distribution is independent of $\vec R$. Thus,  $g_0$ is replaced with the homogeneous Maxwellian distribution $f_M = \frac{1}{(2\pi)^{\frac 32}}e^{-\frac {v^2}{2}}$. 
For simplicity, we assume that the electrons are adiabatic $f_e = e^{-\phi}f_M \approx f_M - \phi f_M$ and the plasma is quasineutral $n_e=n$  (with the electron density $n_e$), the complete system of equations is given by  
\begin{align}
\partial_t f + \vec v \cdot \nabla f + \left( - \nabla \phi + \vec v \times \ub \right)\cdot \nabla_v f = 
\vec v^* \cdot \nabla \phi f_M, \label{eq_temperature_gradient_system} 
\end{align}
\begin{align}
\phi =  n, \quad \quad \quad\vspace{2cm}  n = \int f \dn3v. \label{eq_field_equation}
\end{align}

\subsection{Dispersion relation}
The dispersion relation can be written, using the plasma dispersion function which is defined for $ {\rm Im}(x)>0$ as
\begin{align}
Z(x) = \frac{1}{\sqrt\pi} \int_{-\infty}^\infty \frac{e^{-t^2}}{(t-x)}\mathrm d t,
\end{align}
and is analytically continued for $ {\rm Im}(x)\leq 0$.
After solving the perpendicular integral, we introduce $\Gamma_n(x^2) = e^{-x^2}I_n(x^2)$. Resulting in the expression \cite{brambilla_kinetic_1998}
\begin{align}
\frac{n_k}{\phi_k} = \left[\omega- k_y\frac{\nabla T}{T}\partial_\xi\right] \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}\sum_{p \in  Z}  Z\left(\frac{\omega+p}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_n\left(\frac{\perpc k^2}{\xi}\right)-1.\label{eq_density_response}
\end{align}
The density response is introduced in the quasi-neutrality equation which gives the electrostatic potential form equation (\ref{eq_field_equation}).
The resulting dispersion relation, given by solution $\omega(\vec k)$ to the equation
\begin{align}
0=\left[\omega - k_y\frac{\nabla T}{T}\partial_\xi\right]  \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}\sum_{p \in  Z}  Z\left(\frac{\omega+p}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_n\left(\frac{\perpc k^2}{\xi}\right)-2, \label{eq_dispersion_relation}
\end{align}
describes all modes for the 6D kinetic system. In the gyrokinetic limit $\omega\ll1$ and $k_z\ll 1$ all terms of the sum vanish except the $p=0$ contribution, simplifying the dispersion relation to 
\begin{align}
0=\left[\omega - k_y\frac{\nabla T}{T}\partial_\xi\right] \frac{\sqrt \xi}{|k_z|\sqrt{{2}}}Z\left(\frac{\omega}{|k_z|} \sqrt{\frac{\xi}{2}}\right)\Gamma_0\left(\frac{\perpc k^2}{\xi}\right)-2. \label{eq_gyro_dispersion_relation}
\end{align}


\subsection{Simulation Configuration and Information}
The configuration for the simulation above can be retrieved using 
\begin{verbatim}
	./bsl6d_integrator --get_config slab_itg_boussinesq
\end{verbatim}
It was run until 2000 time units. The
provided config file in this documentation has a lower resolution and a
significantly shorter simulation time $T$ to reduce the computational effort in
our CI. Beside these two differences the config file is the same as was used to
obtain the results below.

The simulation took around 2~h on a GPU node of the
\href{https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html}{MPCDF Raven cluster}.
The config file can be retrieved from a bsl6d executable by adding the flag
\begin{verbatim}
./bsl6d_integrator --bsl6d-get-simulation-config slab_itg_boussinesq
\end{verbatim}

\subsection{Result}
The plot presented below is generated using the script \href{../../../examples/quasineutral_integrators/slab_itg/analysis.py}{analysis.py}. It illustrates the linear growth of the instability on the right and includes a comparison of the growth rate with the results obtained from the analytical dispersion relation \eqref{eq_dispersion_relation} on the right side. A reasonable agreement is observed between the analytical and numerical results. The discrepancies for larger values of $k_y$ are attributed to numerical damping resulting from Lagrange interpolation.
\begin{figure}[h]
\begin{center}
	\includegraphics[width =  \textwidth]{../_static/bsl6dDocStorage/slabITG/growth_rate.png}
	\caption{\label{fig:dispersion_relation} Time evolution of RMS of electrostatic potential (left) and comparison of growth rate with analytical dispersion relation~\eqref{eq_gyro_dispersion_relation}}
\end{center}
\end{figure}

\printbibliography[heading=bibintoc]

\end{document}

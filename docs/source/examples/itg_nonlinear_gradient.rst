Simulation of ion temperature gradient driven modes (ITG) with nonlinear gradients
==================================================================================

Description of temperature gradient
-----------------------------------

The chosen initial condition in the simulation is designed to allow the
distribution function to exhibit a density and temperature profile

.. math::

   \begin{aligned}
       f_0(\ensuremath{\bm{r}}, \ensuremath{\bm{v}}) = \frac{n(\ensuremath{\bm{R}})}{(2\pi T(\ensuremath{\bm{R}}))^{\frac 32}}e^{-\frac{v^2}{2T(\ensuremath{\bm{R}})}}.
   \end{aligned}

The background profiles
:math:`n(\ensuremath{\bm{r}} - \ensuremath{\bm{\rho}})` and
:math:`T(\ensuremath{\bm{r}} - \ensuremath{\bm{\rho}})` are defined in
gyrocenter coordinates
:math:`\ensuremath{\bm{R}} := \ensuremath{\bm{r}} - \ensuremath{\bm{\rho}}`
(where
:math:`\ensuremath{\bm{\rho }}:= \ensuremath{\hat{\ensuremath{\bm{z}}}}\times \ensuremath{\bm{v}}`
is the Larmor radius vector) to establish a background that does not
oscillate with the Larmor frequency. To simplify the treatment of
boundary conditions, the profiles are periodically set up using a
sine-profile in the :math:`x`-direction

.. math::

   \begin{aligned}
       n(\ensuremath{\bm{R}}) &= 1 + \kappa_n \sin( k_0 \left(x -  v_y\right)),\\
       T(\ensuremath{\bm{R}}) &= 1 + \kappa_T \sin( k_0 \left(x -  v_y\right)) \label{eq_temperature_profile}.
   \end{aligned}

To prevent the background density gradient from generating an electric
field, all modes with wave numbers parallel to the gradient are removed
from the electrostatic potential by subtracting the flux surface average
over the :math:`y-z`-plane

.. math::

   \begin{aligned}
       \phi = n - \langle n\rangle_{y,z} = n - \frac{1}{L_y L_z} \int n  \mathrm{d} y  \mathrm{d} z.
   \end{aligned}

Gyrokinetic ITG driven mode
---------------------------

Numerical results
~~~~~~~~~~~~~~~~~

With this configuration, we can perform gyrokinetic ITG simulations
similar to the example `"Slab ITG with Boussinesq
gradient" <slab_itg.html>`__. The simulation results growing modes on
the two flanks of the initial temperature profile.

.. container:: float
   :name: fig:dispersion_relation

   .. container:: center

      |image1|

A more description for this example can be found
`here <https://doi.org/10.1063/5.0197970>`__ (M. Raeth, Hallatschek, and
Kormann 2024).

Simulation Configuration and Information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The simulation is performed with the standard integrator, thus no
specific integration method is given in the configuration file. The
config file can be retrieved from a bsl6d executable by adding the flag

::

      ./bsl6d_integrator --get_config itg_non_linear_gradients

Unstable ion Bernstein waves (IBWs)
-----------------------------------

.. _numerical-results-1:

Numerical results
~~~~~~~~~~~~~~~~~

The figure below show shows the temperature and density gradients,
including the respective logarithmic gradients on top. The bottom 4
panel show :math:`x-y` cross sections of the electrostatic potential,
split into low frequency perturbations (a gyrokinetic ITG mode, left)
and high frequency perturbations (IBW, right) for different times.

.. container:: float
   :name: fig:density_plot

   .. container:: center

      |image2|

A more description for this example can be found
`here <https://arxiv.org/pdf/2310.15981>`__ (Mario Raeth and Hallatschek
2023).

.. _simulation-configuration-and-information-1:

Simulation Configuration and Information
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The simulation is performed with the standard integrator, thus no
specific integration method is given in the configuration file. The
config file can be retrieved from a bsl6d executable by adding the flag

::

      ./bsl6d_integrator --get_config ibw_non_linear_gradients

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-raeth_high_2023

      Raeth, Mario, and Klaus Hallatschek. 2023. “High Frequency
      Non-Gyrokinetic Turbulence at Tokamak Edge Parameters.” *Physical
      Review Letters* [submitted]. https://arxiv.org/pdf/2310.15981.

   .. container:: csl-entry
      :name: ref-raeth_simulation_2024

      Raeth, M., K. Hallatschek, and K. Kormann. 2024. “Simulation of
      ion temperature gradient driven modes with 6D kinetic Vlasov
      code.” *Physics of Plasmas* 31 (4): 042101.
      https://doi.org/10.1063/5.0197970.

.. |image1| image:: ../_static/bsl6dDocStorage/nonlinearGradient/growth_nonlinear_gradient_itg.png
.. |image2| image:: ../_static/bsl6dDocStorage/nonlinearGradient/density_plots_ibw.png

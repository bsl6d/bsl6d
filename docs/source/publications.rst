BSL6D Publications
==================

Physics Results
---------------

**Simulation of ion temperature gradient driven modes with 6D kinetic Vlasov code**
 **Authors:** Mario Raeth, Klaus Hallatschek, Katharina Kormann

 **Journal:** Physics of Plasmas [accepted]
 
 **Year:** 2024
 
 **DOI:** https://arxiv.org/abs/2402.06605


**High frequency non-gyrokinetic turbulence at tokamak edge parameter**
 **Authors:** Mario Raeth, Klaus Hallatschek

 **Journal:** [submitted]

 **Year:** 2024

 **DOI:** https://arxiv.org/abs/2310.15981

Numerical Results
-----------------

**Convergence of splitting methods on rotating grids for the magnetized Vlasov equation**
 **Authors:** Nils Schild, Mario Raeth, Klaus Hallatschek, Katharina Kormann

 **Journal:** [submitted]

 **Year:** 2024

 **DOI:** https://doi.org/10.48550/arXiv.2406.09941

High Performance Computing
--------------------------

**Performance portable semi-Lagrangian algorithm in six dimensions**
 **Authors:** Nils Schild, Mario Räth, Sebastian Eibl, Klaus Hallatschek, Katharina Kormann

 **Journal:** Computer Physics Communications

 **Year:** 2024

 **DOI:** https://doi.org/10.1016/j.cpc.2023.108973

Predecessors
------------

**Massively parallel semi-Lagrangian solver for the six-dimensional Vlasov–Poisson equation**
 **Authors:** Katharina Kormann, Klaus Reuter, Markus Rampp

 **Journal:** The International Journal of High-Performance Computing Applications

 **Year:** 2019

 **DOI:** https://doi.org/10.1177/1094342019834644

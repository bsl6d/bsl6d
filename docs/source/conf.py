# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
import os
import subprocess
import sys
# The source path is the path of the current file (conf.py) removing the name
# of the file
sourcePath = os.path.dirname(os.path.abspath(__file__)) + '/'
sys.path.append(sourcePath)

# -- Project information -----------------------------------------------------

project = 'BSL6D'
copyright = '2022, BSL6D Authors'
author = 'BSL6D Authors'

# The full version, including alpha/beta/rc tags
release = 'Live at HEAD'

# -- Pre setup phase ---------------------------------------------------------

from bsl6dDocSetup import convertLatexToRstFiles, downloadAndExtractDocStorage
convertLatexToRstFiles.main(sourcePath)
downloadAndExtractDocStorage.main(sourcePath)
subprocess.run('doxygen ./Doxyfile', shell=True)

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
#    'sphinx.ext.docutils',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.imgconverter',
    'sphinx.ext.imgmath',
    'breathe',
    'myst_parser',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Breathe configuration -------------------------------------------------

breathe_projects = {
	"BSL6D": "../build_doxygen/xml"
}
breathe_default_project = "BSL6D"
breathe_default_members = ('members', 'undoc-members')
highlight_language = 'c++'

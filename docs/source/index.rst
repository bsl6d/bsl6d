

Welcome to the BSL6D documentation!
===================================

.. image:: ./_static/bsl6dDocStorage/Logo.png


BSL6D: Backwards SemiLagrangian 6Dimensions
===========================================

Welcome to BSL6D, the simulation software application designed for tackling the challenges of simulating the fully kinetic 6D-Vlasov equation. This cutting-edge code is developed to explore physics beyond the conventional gyrokinetic model. Simulating the fully kinetic Vlasov equation demands efficient utilization of compute and storage capabilities, given the high dimensionality of the problem. BSL6D rises to the challenge, providing a performant and extensible solution.


Description
-----------

BSL6D employs a semi-Lagrangian algorithm to simulate the fully kinetic 6D-Vlasov equation. This approach ensures efficient utilization of compute and storage capabilities, making it adaptable to various hardware configurations, including pure CPU, AMD, or Nvidia GPU-accelerated nodes.

Algorithmic Background
~~~~~~~~~~~~~~~~~~~~~~

The algorithmic background of BSL6D lies in the semi-Lagrangian algorithm used to simulate the 6-D Vlasov equation, providing a robust foundation for exploring physics beyond the reduced gyrokinetic model.

Performance and Extensibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance-portable software stack of BSL6D enables production runs on diverse hardware architectures. The software architecture of the main kernel ensures extensibility, achieving remarkable memory bandwidth on both Nvidia GPU and Intel Xeon Gold CPU with a single code base.

.. toctree::
    :maxdepth: 1
    :caption: Contents:
    :hidden:

    self

Contents
~~~~~~~~

.. toctree::
   :maxdepth: 1

   usage/index.rst
   publications.rst
   examples/index.rst
   GitHub Repo <https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d>
   api/index.rst
   field_solvers/index.rst

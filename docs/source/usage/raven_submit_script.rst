.. _raven_submit_script:
Valid SLURM Submit script
=========================

A valid SLURM submit script to execute the `quasineutral_integrators` executable
on the Raven compute cluster using a GPU partition.

.. code-block:: bash

    #!/bin/bash -l
    #SBATCH --nodes=1
    #SBATCH --ntasks=4
    #SBATCH --cpus-per-task=18
    #SBATCH --time=01:00:00
    #SBATCH --constraint="gpu"
    #SBATCH --gres=gpu:a100:4

    #SBATCH --job-name=landau_damping_example
    #SBATCH -o out.%j
    #SBATCH -e err.%j

    module purge
    module load gcc/11 openmpi/4 hdf5-mpi cuda/11.4 openmpi_gpu/4
    module list

    export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
    export OMP_PROC_BIND=spread
    export OMP_PLACES=threads

    srun ./bsl6d_integrator


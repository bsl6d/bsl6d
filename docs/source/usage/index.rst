Using the BSL6D Code
====================

The following sections describe how to build, install and execute the BSL6D
code. The development of the code is in a rather early stage and the workflow
is still converging to a stable version. Therefore, rapid changes of the 
required workflows are still possible

.. toctree::
   :caption: Contents:
   
   build.md
   install.rst
   execution.rst




Execution
=========
.. _SLURM: https://slurm.schedmd.com/overview.html
.. _Raven: https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html#slurm-batch-system
.. _Viper: https://docs.mpcdf.mpg.de/doc/computing/viper-user-guide.html#slurm-example-batch-scripts

General Remarks
---------------

Once the code has been successfully built, the executable can be used to run various 
simulations. To run a simulation, you first need to create an empty folder that will 
later on contain the simulation data. After that, you need to copy the executable 
and the configuration file (on SLURM systems also the batch submission script) to the 
empty directory:

+-------------------------+--------------------------+
| File description        | Default File Name        |
+=========================+==========================+
| Executable              | `bsl6d_integrators`      |
+-------------------------+--------------------------+
| Configuration script    | `bsl6dConfig.json`       |
+-------------------------+--------------------------+

The code can be executed by running

.. code-block:: bash

    ./bsl6d_integrator `--args*`

where `--args*` are a selected set of command line arguments. Passing these arguments 
is optional as they are preset to the following default values which can be retrieved
using the `--help` flag.

.. code-block:: bash

    Flag Name            Description                                    Value
     --------------------------------------------------------------------------------------
    --config_help,       Show help for passed config section.           <default-string>
    --get_config,        Retrieve BSL6D simulation configuration.       <test_case>
    --help, -h           Display help information.                      true
    --input_file, -i     Path to the BSL6D input configuration file.    bsl6dConfig.json
    --output_file, -o    Path to the BSL6D output diagnostics file.     bsl6dDiagnostics
    --restart, -r        Restart the BSL6D process. (Optional flag)     <restart_file>
    --test,              Run BSL6D in test mode.                        <test_case>
    --verbose, -v        Enable verbose logging for BSL6D.              false

On systems with a queuing manager, like `Raven`_ or `Viper`_ of the MPCDF, additionally
a submit-script might be necessary. Then the job is submitted to the queuing system via

.. code-block:: bash

    sbatch submit.sh

.. _MPCDF SLURM documentation: https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html#slurm-batch-system
where `submit.sh` is the submission script containing all relevant job parameters as well as the command

.. code-block:: bash

    srun ./bsl6d_integrator `--args*`

For more information on how to use a batch system see `MPCDF SLURM documentation`_

Executing a 1D1V Landau Damping Example
---------------------------------------

Here we perform a simple `1D1V` example to simulate the Landau damping of a density perturbation parallel to the magnetic field. 
A detailed description of the example is given in :ref:`Landau Damping: Ion Sound Wave`.
As explained above, the following steps must be carried out before running the simulation:

.. code-block:: bash

    mkdir bsl6d_landau_damping
    cd bsl6d_landau_damping
    cp /PATH/TO/BUILD/DIRECTORY/bin/bsl6d_integrator .
    ./bsl6d_integrator --get_config ion_sound_wave

If no ressource management system like `SLURM`_ is used, the simulation can now simply be executed by running

.. code-block:: bash

    ./bsl6d_integrator

In a SLURM-based system, a batch script is required. 
An example batch script can be found in can be found in :ref:`raven_submit_script`.
Note that this batch script will submit the `SLURM`_ workload to the MPCDF `Raven`_ cluster using a GPU partition. 
Submitting the batch script to the queue is done using

.. code-block:: bash

    touch submit.sh
    #Write configuration data using your favorite texteditor 
    sbatch submit.sh

The status of the job can be viewed by running `squeue --user <myUserName>` in the command line.

In both cases the executable creates an output file `bsl6dDiagnostics.out`. The simulation data is 
stored in `bsl6d_landau_damping/bsl6dDiagnostics/` using HDF5 files according to the openPMD standard.
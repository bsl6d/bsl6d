# Build

The following sections describe how to build the BSL6D code.

## Required Third Party Tools and Software

All *required* external dependencies can usually be installed using a package manager on Unix systems 
(e.g. `zypper` on Linux or `brew` on MacOS) or by using a `module` environment on compute clusters. 
More detailed information on installing software using package managers or module environments is given 
in the next section.

Some external dependencies must be installed manually by the user. The remaining ones are shipped 
internally, but can be installed manually if desired. 

List of required external software to be installed by the user:

* A C++17 compatible compiler (recommended: gnu9+)
* [CMake](https://github.com/Kitware/CMake) 
* [git](https://git-scm.com/)
* MPI (e.g.[OpenMPI](https://www.open-mpi.org/))
* [Parallel HDF5](https://github.com/HDFGroup/hdf5)
* A Fast-Fourier Transform library (backend dependent)
    * **CPU**: [fftw](http://fftw.org/)
    * **AMD GPU**: [rocfft](https://rocm.docs.amd.com/projects/rocFFT/en/latest/index.html)
    * **Nvidia GPUs**: [cufft](https://docs.nvidia.com/cuda/cufft/index.html)

List of internally shipped external software:

* [Kokkos](https://github.com/kokkos/kokkos)
    * Tested backends: OpenMP, CUDA, HIP
    * Can be compiled alongside BSL6D for all presets below and for some additional CPU builds
    * Possible to provide the package externally (recommended to achive higher performance)
* [HeFFTe](https://github.com/icl-utk-edu/heffte) 
    * Tested backends: fftw, cufft or rocfft
    * Possible to provide as an external build
* [openPMD](https://github.com/openPMD/openPMD-api)
    * Implemented backends: Parallel-HDF5
    * Possible to provide as an external build
* [googletest](https://github.com/google/googletest.git)
* [cmake-git-version-tracking](https://github.com/andrew-hardin/cmake-git-version-tracking)

List of optional external software:

* [LIKWID](https://github.com/RRZE-HPC/likwid) (performance profiling, optional)
    * Required if `CMAKE_ENABLE_PROFILING=ON` with a CPU backend
    * LIKWID furthermore requires an OpenMP to be found by CMake

## Build Instructions

To get started, you need to clone the BSL6D code from the [BSL6D-MPCDF GitLab] repository 
(https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d) using the following command:
```bash
git clone git@gitlab.mpcdf.mpg.de:bsl6d/bsl6d.git
```

The code can now be built using one of the predefined presets below, or manually using CMake. 
Manual build is recommended for users familiar with CMake. However, before BSL6D can actually 
be built, the external dependencies must be installed.

### Retriving External Tools and Packages

* MPCDF Cluster
If the project is installed on systems with a module environment, such as the 
[MPCDF Clusters](https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html#software), the 
required external software can be loaded using the `module` command. For pure CPU builds on 
MPCDF-Clusters, the required modules are loaded as follows:
```bash
module load cmake git gcc/11 
module load openmpi/4 fftw-mpi hdf5-mpi likwid/5.1
```

Additional modules are required for GPU builds (e.g. CUDA). Detailed information on this can 
be found in the list of available CMake presets in the next subsection. In general, additional 
modules can be loaded as follows:
```bash
module load <ADDITIONAL/MODULES>
```

* Cineca Leonardo Cluster
If the project is installed on systems with a module environment, such as the 
[Cineca Leonardo Clusters](https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2%3A+LEONARDO+UserGuide), the 
required external software can be loaded using the `module` command. For pure GPU build on 
Cineca Leonardo Clusters, the required modules are loaded as follows:
```bash
module load cmake cuda/12.1 gcc/12.2.0 openmpi/4.1.6--gcc--12.2.0 fftw/3.3.10--openmpi--4.1.6--gcc--12.2.0 hdf5/1.14.3--openmpi--4.1.6--gcc--12.2.0
```
On Leonardo we found performance issues if `srun` is used. These need closer investigation.
For now use `mpirun`. Consider [Issue #95](https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d/-/issues/95)
for more details.

Installing external dependencies on local Unix systems (Linux/MacOS) is similar to working on 
compute clusters. In these cases, package managers are used. The installation commands are

* Linux: `zypper`
```bash
zypper install cmake git gcc-c++
zypper install openmpi4-devel hdf5-openmpi4-devel fftw3-openmp-devel fftw3-threads-devel
```
* MacOS: `brew`
```bash
brew install cmake git gcc@12
brew install open-mpi hdf5-mpi fftw
```

### Build BSL6D Using Predefined Presets (Recommended)

To simplify the process of installing BSL6D on different platforms, 
[CMake Presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html) have been defined for 
systems that are regularly used by the BSL6D developers. These presets cover the MPCDF clusters as 
well as the local Unix systems OpenSUSE and MacOS. All available presets are listed in the table below 
or can be viewed using 
```bash
cmake --list-presets
```
To build BSL6D with one of the presets, use the following commands:
```bash
cmake --preset name-of-preset -S /PATH/TO/BSL6D/PROJECT -B /PATH/TO/BSL6D/BUILD/DIRECTORY
cmake --build /PATH/TO/BUILD/DIRECTORY -j 5
```
The `-j <int>` flag can be added to parallelise the build of the BSL6D code using a specified number of processes.
In a parallel build, care should be taken not to oversubscribe the available resources.

The following [CMake presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html) are available:
| Name                            | Description                                  | System / OS     | Additional Modules       |
|---------------------------------|----------------------------------------------|-----------------|--------------------------|
| `mpcdf-gcc-cpu`                 | GCC CPU Settings                             | Raven / Cobra   |                          |
| `mpcdf-gcc-gpu-cuda-aware`      | GCC GPU Settings - Cuda Aware MPI            | Raven           | `cuda/11.4 openmpi_gpu/4`|
| `mpcdf-gcc-gpu-non-cuda-aware ` | GCC GPU Settings - Non Cuda Aware MPI        | Cobra           | `cuda/11.4`              |
| `mpcdf-clang-cpu`               | CLANG CPU Settings                           | Raven / Cobra   | `clang/11.1`             |
| `mpcdf-intel-cpu`               | Intel CPU Settings                           | Raven / Cobra   | `intel/21.5.0`           |
| `opensuse-tumbleweed`           | GCC CPU Settings set up with `zypper`        | OpenSuse        |                          |
| `MacOS`                         | GCC CPU Settings set up with `brew`          | MacOS           |                          |

Personal presets that should be used on a particular system, but are not added to the BSL6D repository, can be stored in the
in `CMakeUserPresets.json'.

**Known issues** 
Dependencies may not be found if they are not installed in the standard Unix file structure.
Running the CMake command to configure the BSL6D project may result in errors such as
```
CMake Error at /usr/share/cmake/Modules/FindPackageHandleStandardArgs.cmake:230 (message):
  Could NOT find HDF5 (missing: HDF5_LIBRARIES HDF5_INCLUDE_DIRS C) (found version "")
```
In this case, CMake could not find one of the external dependencies. Specifying installation paths to 
CMake can be done using [\<PackageName>_ROOT](https://cmake.org/cmake/help/latest/envvar/PackageName_ROOT.html).
This was found for the parallel HDF5 library installed on OpenSUSE. Pointing cmake to the
installation of HDF5 by adding `-D HDF5_ROOT=/PATH/TO/PARALLEL/HDF5` to the cmake
command above (found on OpenSUSE15.3+).

### Build BSL6D Manually

If there is no build preset defined for a system, the cmake command must be specified manually. This is more complex and, 
as mentioned above, only recommended for users familiar with CMake and command line tools.
```bash
cmake -S /PATH/TO/BSL6D/PROJECT -B /PATH/TO/BSL6D/BUILD/DIRECTORY ...
ccmake /PATH/TO/BSL6D/BUILD/DIRECTORY
```
Using `ccmake`, cache variables can be set to configure the BSL6D build. All available options to build the BSL6D code are listed below:

## CMake Keywords for BSL6D

| Name                            | Description                                  | Default | Prerequisite                                          |
|---------------------------------|----------------------------------------------|---------|-------------------------------------------------------|
| ` BSL6D_ENABLE_GPU `            | To build with GPU support                    | `OFF`   | Kokkos build with Hip or CUDA backend                 | 
| ` BSL6D_ENABLE_CUDA_AWARE_MPI ` | To build with GPU direct communication       | `OFF`   | MPI library was build with GPU direct communication   | 
| ` BSL6D_ENABLE_TEST `           | To build unit tests                          | `ON`    | Likwid profiling library if CPU application is build. | 
| ` BSL6D_ENABLE_PROFILING `      | To build performance tests                   | `OFF`   |                                                       | 
| ` BSL6D_CLANG_TIDY `            | To run clang-tidy checks during compilation. | `OFF`   |                                                       | 
| ` BSL6D_BUILD_EXAMPLES `        | Build examples based on BSL6D implementation | `ON`    |                                                       | 

## Testing

The BSL6D project includes a comprehensive suite of unit, integration and execution tests, which can be executed using 
[CTest](https://cmake.org/cmake/help/latest/manual/ctest.1.html):
```bash
cd /PATH/TO/BSL6D/BUILD/DIRECTORY
ctest
```

On MPCDF clusters, no GPUs are available on login nodes or interactive nodes. Therefore, to test a GPU build of 
the BSL6D project, a [SLURM](https://slurm.schedmd.com/overview.html) job needs to be submitted to the queue. 
For all MPCDF clusters, the required batch files are created when the project is configured with CMake. These can 
be run with the following command:
```bash
cd /PATH/TO/BSL6D/BUILD/DIRECTORY
sbatch test.<CLUSTERNAME>.sh
```
After the job has run, run the following command on the job's output file:
```bash
tail -f out.<JOBID>
```

Naming conventions for tests: <DirectoryName>TestName
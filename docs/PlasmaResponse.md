# Waves in magnetized Plasma
Let's imagine we apply a strong and constant background magnetic field in a plasma.
As a result, the particle will follow the helical trajectories around the magnetic field. 
Moreover, the behavior of plasma will change in response to the presence of an electromagnetic
wave. We are interested to see what would be the linear response in the different scenarios. 

To do so, we imagine a wave with a positive growth rate i.e. finite and positive imaginary part of the frequency.
Thus, at $` t = -\infty `$ we have an unperturbed distribution $` f_0 `$, which is isotropic and only a function of velocity. 

The main equation on which the simulation is based is the Vlasov equation. We substitute the distribution into the Vlasov equation and try to derive the response of the system.

we have the Vlasov equation: 
```math
\begin{align} 
\partial_t f + \vec v \cdot \nabla f + ( -\nabla \phi + \vec v \times \vec B) \nabla_v f = 0
\end{align}
```

Assumptions: 

1) $`f_0`$ is the isotropic background distribution which is only a function of velocity $` \frac{\vec v^2}{2} `$
2) We work with a linearized Vlasov equation. 
3) We assume the following reductions in the dimension of the problem. 


```math 
\begin{align}
\vec v = \begin{pmatrix}
v_x\\
v_y\\
c
\end{pmatrix}\\
\vec r = \vec x
\end{align}
```
In equation (2), "c" is a constant value. We assume there is  symmetry in the $` y `$ and $` z `$ and neglect them in position space. In velocity space, due to the motion of the ions as a response to the electric potential we must keep $`v_x`$ and $` v_y `$ and assume we have a constant velocity in the $`z`$ direction. 

Having the mentioned assumptions in mind, we can solve the problem in one dimension. Consequently, we can write the equation (1) as follows : 
```math
\begin{align}
\partial_t f(\vec  x, \vec v, t) + v_x \partial_x f(\vec x, \vec v, t) + \frac{q}{m} (\vec E(\vec x, t) + \vec v \times \vec B_0).( \partial_{v_x}f(\vec x, \vec v, t) \hat x + \partial_{v_y}f(\vec x, \vec v, t) \hat y) = 0 
\end{align}
```
$` B_0 `$ is the constant backgroud magnetic field and $`\vec E(\vec x, t)  `$ is related to the electric potential by the following relation. 

```math
\begin{align}
\vec E(\bold x, t) = - \vec\nabla \phi(\vec  x, t)
\end{align}
```
As an approach, we choose a plane wave with a time-dependent position vector for the electrostatic potential as follows, 

```math
\begin{align} 
\phi =  \phi_k e^{i(k \cdot \vec r - \omega t)}
\end{align}
```

By solving the Vlasov equation concerning the introduced electric potential we can find the general solution of the distribution function in a homogenous plasma with a strong background magnetic field and call it $` h(k) `$. 
 
To find the dispersion relation we need to compute the response of the ion density and substitute in electrostatic potential.

```math
\begin{align} 
\hat n = \int h_k d^3 v
\end{align}
```
As the global response ion density to the introduced electric potential. 
Then we reach following ratio: 

```math
\begin{align}
\frac{\hat n}{\hat \phi } = \Gamma_0(k) = e^{-\vec k^2} I_0(k) - 1
\end{align}
```
The main goal of this simulation is to check whether there is an agreement between the analytical and numerical results. So, if the code works well, it will produce the $` \Gamma_0(K) `$ and fit the analytical plot.

# Dependencies 
GitWatcher.cmake and additional files (git.cpp.in/git.h) where taken from 'https://github.com/andrew-hardin/cmake-git-version-tracking'. \
HDF5 library used for IO of large datasets.\
FFTW3 library used for discrete fourier transformations.\
CUDAToolkit and CUDA libaries are used on GPU using cuFFT libraries and NVTX profiling.\
likwid library used as a vendor independent performance profiling library.\
Kokkos main performance portability library. (provided internally if necessary)\
GTest as testing framework. (provided internally if necessary)\

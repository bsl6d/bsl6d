find_package(openPMD CONFIG)

if(NOT openPMD_FOUND)
  message(STATUS "Fetching openPMD-api from https://github.com/openPMD/openPMD-api.git")
  FetchContent_Declare(
    openPMD
    GIT_REPOSITORY https://github.com/openPMD/openPMD-api.git
    GIT_TAG 2e246cc8be8003071a3aece734ad4e632bd87fd7
    OVERRIDE_FIND_PACKAGE
  )

  set(openPMD_USE_MPI ON CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_HDF5 ON CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_ADIOS1 OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_ADIOS2 OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_PYTHON OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_INTERNAL_JSON OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_INSTALL OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_INSTALL_RPATH OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_BUILD_TESTING OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_BUILD_EXAMPLES OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_BUILD_CLI_TOOLS OFF CACHE BOOL "openPMD CMake option set with BSL6D")
  set(openPMD_USE_CUDA_EXAMPLES OFF CACHE BOOL "openPMD CMake option set with BSL6D")

  FetchContent_MakeAvailable(openPMD)
endif()

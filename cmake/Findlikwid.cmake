#*****************************************************************#
#[[
#\brief CMake installation on MPCDF-Clusters does provide a 
#       likwidConfig.cmake file which may substitude this script at
#       some point.
#
#\author Nils Schild
#\department NMPP
#\email  nils.schild@ipp.mpg.de
#\date   10.10.2021
#]]
#*****************************************************************#
SET(likwid_ROOT $ENV{likwid_ROOT} CACHE STRING "Root Directory of likwid library.")

IF(NOT DEFINED likwid_ROOT AND
   NOT DEFINED ENV{likwid_ROOT})

  MESSAGE(FATAL_ERROR "likwid_ROOT was not defined as CMake Cache or"
                      " environment variable. BSL6D relies on CMake ROOT variable.\n"
                      "Set either export likwid_ROOT=<Path to likwid install directory>"
                      " or cmake -D likwid_ROOT=<Path to likwid install directory> ")

ENDIF()

FIND_LIBRARY(likwid_LIBRARY NAMES likwid
             HINTS ${likwid_ROOT}
             PATH_SUFFIXES lib)
FIND_PATH(likwid_INCLUDE_DIR NAMES likwid-marker.h
          HINTS ${likwid_ROOT}
          PATH_SUFFIXES include)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(likwid DEFAULT_MSG
                                  likwid_LIBRARY
                                  likwid_INCLUDE_DIR)

IF(NOT likwid_FOUND)

  MESSAGE(FATAL_ERROR "likwid could not be found in:\n"
                      "  - likwid_DIR=${likwid_ROOT}:\n"
                      "  - likwid_LIBRARY : ${likwid_LIBRARY}\n"
                      "  - liwkid_INCLUDE_DIR : ${likwid_INCLUDE_DIR}")
  
ENDIF()

MARK_AS_ADVANCED(likwid_LIBRARY likwid_INCLUDE_DIR)
ADD_LIBRARY(bsl6d::likwid SHARED IMPORTED)
SET_TARGET_PROPERTIES(
      bsl6d::likwid
      PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${likwid_INCLUDE_DIR}"
      IMPORTED_LOCATION ${likwid_LIBRARY})
TARGET_COMPILE_OPTIONS(
      bsl6d::likwid 
      INTERFACE -DLIKWID_PERFMON -llikwid)

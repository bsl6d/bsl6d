find_package(nlohmann_json CONFIG)

if(NOT nlohmann_json_FOUND)
  message(STATUS "Fetching JSON from https://github.com/nlohmann/json.git")
  FetchContent_Declare(
    nlohmann_json
    GIT_REPOSITORY https://github.com/nlohmann/json.git
    GIT_TAG 2d42229f4d68c6f86f37468b84ac65e86b815bbb
    OVERRIDE_FIND_PACKAGE
  )

  set(JSON_Install OFF CACHE BOOL "JSON CMake option set with BSL6D")
  set(JSON_BuildTests OFF CACHE BOOL "JSON CMake option set with BSL6D")

  FetchContent_MakeAvailable(nlohmann_json)
endif()

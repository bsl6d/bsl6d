find_package(Heffte CONFIG)

if(NOT Heffte_FOUND)
  message(STATUS "Fetching HeFFTe from https://github.com/icl-utk-edu/heffte.git")
  set(BSL6D_HEFFTE_SET_BY_FETCH_CONTENT TRUE)
  FetchContent_Declare(
    Heffte
    GIT_REPOSITORY https://github.com/icl-utk-edu/heffte.git
    GIT_TAG c61c772f8e6d31d35f5f7e45dfed4fe75973db93
  )
  if(Kokkos_ENABLE_CUDA)
  
    set(Heffte_ENABLE_CUDA ON CACHE BOOL "Heffte CMake option set within BSL6D.")
    set(Heffte_DISABLE_GPU_AWARE_MPI (NOT BSL6D_ENABLE_CUDA_AWARE_MPI) CACHE BOOL "Heffte CMake option set within BSL6D.")
  
  elseif(Kokkos_ENABLE_HIP)
  
    set(Heffte_ENABLE_ROCM ON CACHE BOOL "Heffte CMake option set within BSL6D.")
    set(Heffte_DISABLE_GPU_AWARE_MPI (NOT BSL6D_ENABLE_CUDA_AWARE_MPI) CACHE BOOL "Heffte CMake option set within BSL6D.")
  
  else()
  
    set(Heffte_ENABLE_FFTW ON CACHE BOOL "Heffte CMake option set within BSL6D.")
  
  endif()
  FetchContent_MakeAvailable(Heffte)
endif()

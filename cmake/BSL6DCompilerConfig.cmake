#*****************************************************************#
#[[
# \brief Selects compiler specific flags and other compile options
#
# \author Nils Schild
# \department NMPP
# \email nils.schild@ipp.mpg.de
# \date 20.01.2022
# \note 
#       Most optimization Flags are provided by Kokkos. Therefore
#       if a certain flag is not set check whether it is provided
#       by Kokkos.
#]]
#*****************************************************************#

#GCC
IF(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
  # funroll-all-loops is probably only interesting for the LagrangeIterator.cpp (compiletime option!)
  SET(BSL6D_CXX_FLAGS -Wall -Wextra -funroll-all-loops -Wno-unused-parameter)
  IF(CMAKE_BUILD_TYPE MATCHES "Debug")
    SET(BSL6D_CXX_DEBUG_FLAGS ${BSL6D_CXX_FLAGS} -Wpedantic -Wunused-parameter)
  ENDIF()
  if(BSL6D_ENABLE_GPU)
    # Throw an error if host functions are called from the device 
    # (https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html#werror-kind-werror)
    set(BSL6D_CXX_FLAGS ${BSL6D_CXX_FLAGS} --Werror=cross-execution-space-call)
  endif()
ELSEIF(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  SET(BSL6D_CXX_FLAGS -Wall -Wextra -Wno-unused-parameter)
  IF(CMAKE_BUILD_TYPE MATCHES "Debug")
    SET(BSL6D_CXX_DEBUG_FLAGS ${BSL6D_CXX_FLAGS} -Wpedantic -Wunused-parameter)
  ENDIF()
ELSEIF(CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  SET(BSL6D_CXX_FLAGS -Wall -ffp-model=precise)# -unroll-aggressive) I am not sure how much this flag improves performance really.
  IF(CMAKE_BUILD_TYPE MATCHES "Debug")
    SET(BSL6D_CXX_DEBUG_FLAGS ${BSL6D_CXX_FLAGS} -Weffc++)
  ENDIF()
  IF(BSL6D_ENABLE_PROFILING)
    # Creates optimization reports for intel compiler
    # -help report for help on optimization report options
    # -qopt-report-help for help on different reports
    SET(BSL6D_CXX_FLAGS ${BSL6D_CXX_FLAGS} -qopt-report-phase=vec,ipo -qopt-report=2)
  ENDIF()
ELSE()
  MESSAGE(FATAL_ERROR "Unknown Compiler ID (${CMAKE_CXX_COMPILER_ID}) in BSL6D project. This is mainly a development information. To build the program comment this line out.")
ENDIF()

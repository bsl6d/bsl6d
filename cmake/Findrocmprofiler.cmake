#*****************************************************************#
#[[
#\brief CMake installation on MPCDF-Clusters does provide a 
#       rocprofilerConfig.cmake file which may substitude this script at
#       some point.
#
#\author Nils Schild
#\department NMPP
#\email  nils.schild@ipp.mpg.de
#\date   10.10.2021
#]]
#*****************************************************************#
SET(rocprofiler_ROOT ENV{rocprofiler_ROOT} CACHE PATH "Root Directory of rocprofiler library.")

IF(NOT DEFINED rocprofiler_ROOT AND
        NOT DEFINED ENV{rocprofiler_ROOT})

  MESSAGE(FATAL_ERROR "rocprofiler_ROOT was not defined as CMake Cache or"
                      " environment variable. BSL6D relies on CMake ROOT variable.\n"
                      "Set either export rocprofiler_ROOT=<Path to rocprofiler install directory>"
                      " or cmake -D rocprofiler_ROOT=<Path to rocprofiler install directory> ")

ENDIF()

FIND_LIBRARY(roctracer_LIBRARY NAMES roctracer64
             HINTS ${rocprofiler_ROOT}
             PATH_SUFFIXES lib)
FIND_PATH(roctracer_INCLUDE_DIR NAMES roctracer.h
          HINTS ${rocprofiler_ROOT}
          PATH_SUFFIXES include/roctracer/)

FIND_LIBRARY(roctx_LIBRARY NAMES roctx64
             HINTS ${rocprofiler_ROOT}
             PATH_SUFFIXES lib)
FIND_PATH(roctx_INCLUDE_DIR NAMES roctx.h
          HINTS ${rocprofiler_ROOT}
          PATH_SUFFIXES include/roctracer/)


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(rocprofiler DEFAULT_MSG
                                  roctracer_LIBRARY
                                  roctracer_INCLUDE_DIR
                                  roctx_LIBRARY
                                  roctx_INCLUDE_DIR)

IF(NOT rocprofiler_FOUND)

  MESSAGE(FATAL_ERROR "rocprofiler could not be found in rocprofiler_DIR=${rocprofiler_ROOT}"
                      "  - roctracer_LIBRARY : ${roctx_LIBRARY}\n"
                      "  - roctracer_INCLUDE_DIR : ${roctx_INCLUDE_DIR}\n"
                      "  - roctx_LIBRARY : ${roctx_LIBRARY}\n"
                      "  - roctx_INCLUDE_DIR : ${roctx_INCLUDE_DIR}")
  
ENDIF()

MARK_AS_ADVANCED(roctx_LIBRARY roctx_INCLUDE_DIR 
                 roctracer_LIBRARY roctracer_INCLUDE_DIR)

ADD_LIBRARY(bsl6d::roctracer SHARED IMPORTED)
SET_TARGET_PROPERTIES(
        bsl6d::roctracer
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${roctracer_INCLUDE_DIR}
        INTERFACE_LINK_LIBRARIES ${roctracer_LIBRARY}
        IMPORTED_LOCATION ${roctracer_LIBRARY})
TARGET_COMPILE_OPTIONS(
      bsl6d::roctracer 
      INTERFACE -DROCM_PERFMON)
ADD_LIBRARY(bsl6d::roctx SHARED IMPORTED)
SET_TARGET_PROPERTIES(
        bsl6d::roctx
        PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${roctx_INCLUDE_DIR}
        INTERFACE_LINK_LIBRARIES ${roctx_LIBRARY}
        IMPORTED_LOCATION ${roctx_LIBRARY})
TARGET_COMPILE_OPTIONS(
      bsl6d::roctx
      INTERFACE -DROCM_PERFMON)


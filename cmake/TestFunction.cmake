function(bsl6d_add_mpitest)
    set(options)
    set(oneValueArgs NAME PROCESSES)
    set(multiValueArgs FILES LABELS LIBRARIES)
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if (NOT ARG_NAME)
        message(FATAL_ERROR "No name given for test!")
    endif ()

    if (NOT ARG_FILES)
        message(FATAL_ERROR "No files given for test ${ARG_NAME}!")
    endif ()

    if (NOT ARG_PROCESSES)
        set(num_processes 1)
    else ()
        set(num_processes ${ARG_PROCESSES})
    endif ()

    add_executable(${ARG_NAME}
            ${ARG_FILES}
            )
    target_link_libraries(${ARG_NAME}
            PRIVATE bsl6d_gtest_main 
            bsl6d 
            ${ARG_LIBRARIES}
            )
    add_test(NAME ${ARG_NAME} 
             COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} ${num_processes} 
                                           --oversubscribe ${MPIEXEC_PREFLAGS} ./${ARG_NAME})

    #Can these properties be added to Target gtestMPIKokkosMain
    set_tests_properties(${ARG_NAME} PROPERTIES ENVIRONMENT "OMP_PROC_BIND=spread;OMP_PLACES=threads")
    if (NOT ARG_LABELS)
        set_tests_properties(${ARG_NAME} PROPERTIES LABELS "${ARG_LABELS}")
    endif ()
    set_tests_properties(${ARG_NAME} PROPERTIES PROCESSORS ${num_processes})
    set_tests_properties(${ARG_NAME} PROPERTIES PROCESSOR_AFFINITY ON)
    set_tests_properties(${ARG_NAME} PROPERTIES LABELS MultiProcMPI)
endfunction()

function(bsl6d_create_submit_file)
  if($ENV{HOSTNAME} MATCHES "viper" )
    file(WRITE "${CMAKE_BINARY_DIR}/test.viper.sh"
                "#!/bin/bash -l\n"
                "# Standard output and error:\n"
                "# Initial working directory:\n"
                "# Number of nodes and MPI tasks per node:\n"
                "#SBATCH --nodes=1\n"
                "#SBATCH --ntasks-per-node=2\n"
                "# for OpenMP:\n"
                "#SBATCH --cpus-per-task=64\n"
                "#SBATCH --time=00:15:00\n"
                "\n"
                "#SBATCH --job-name=bsl6d_testing\n"
                "#SBATCH -o ./out.%j\n"
                "#SBATCH -e ./err.%j\n"
                "\n"
                "# Load compiler and MPI modules (must be the same as used for compiling the code)\n"
                "module purge\n"
                "module load cmake gcc/14 openmpi/4.1 hdf5-mpi fftw-mpi\n"
                "module list\n"
                "\n"
                "# For pinning threads correctly:\n"
                "export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n"
                "export OMP_PROC_BIND=spread\n"
                "export OMP_PLACES=cores\n"
                "\n"
                "ctest --output-on-failure --verbose\n")
  elseif($ENV{HOST} MATCHES "raven" )
    file(WRITE "${CMAKE_BINARY_DIR}/test.raven.sh"
                "#!/bin/bash -l\n"
                "#SBATCH --nodes=1\n"
                "#SBATCH --ntasks=4\n"
                "#SBATCH --cpus-per-task=18\n"
                "#SBATCH --time=00:15:00\n"
                "#SBATCH --constraint=\"gpu\"\n"
                "#SBATCH --partition=gpudev\n"
                "#SBATCH --gres=gpu:a100:4\n"
                "\n"
                "#SBATCH --job-name=bsl6d_testing\n"
                "#SBATCH -o out.%j\n"
                "#SBATCH -e err.%j\n"
                "\n"
                "module purge\n"
                "module load cmake gcc/12 cuda/12.2 openmpi/4.1 openmpi_gpu/4.1 hdf5-mpi\n"
                "module list\n"
                "\n"
                "export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n"
                "export OMP_PROC_BIND=spread\n"
                "export OMP_PLACES=threads\n"
                "\n"
                "ctest --output-on-failure --verbose\n")
  elseif($ENV{HOST} MATCHES "leonardo")
    file(WRITE "${CMAKE_BINARY_DIR}/test.leonardo.sh" 
               "#!/bin/bash\n"
               "#SBATCH --account=fual8_logy\n"
               "#SBATCH --partition=boost_fua_dbg\n"
               "#SBATCH --time=00:10:00\n"
               "#SBATCH --nodes=1\n"
               "#SBATCH --ntasks-per-node=4\n"
               "#SBATCH --cpus-per-task=8\n"
               "#SBATCH --gres=gpu:4\n"
               "#SBATCH --mem=256GB\n"
               "#SBATCH --job-name=bsl6d\n"
               "\n"
               "module load cmake cuda/12.1 gcc/12.2.0 openmpi/4.1.6--gcc--12.2.0 hdf5/1.14.3--openmpi--4.1.6--gcc--12.2.0\n"
               "\n"
               "export OMP_NUM_THREADS=\$SLURM_CPUS_PER_TASK\n"
               "export OMP_PROC_BIND=spread\n"
               "export OMP_PLACES=threads\n"
               "\n"
               "ctest --output-on-failure --verbose\n")
  endif()
endfunction()


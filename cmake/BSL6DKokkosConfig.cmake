find_package(Kokkos CONFIG)

if(NOT Kokkos_FOUND)
  message(STATUS "Fetching Kokkos from https://github.com/kokkos/kokkos.git")
  FetchContent_Declare(
    Kokkos
    GIT_REPOSITORY https://github.com/kokkos/kokkos.git
    GIT_TAG 18cb82e5764a68d6f0633d81a50982f3804692e0
  )
  
  set( Kokkos_ENABLE_SERIAL ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
  
  if($ENV{HOSTNAME} MATCHES "viper" )

    if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
      set( Kokkos_ENABLE_OPENMP ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
    set( Kokkos_ARCH_ZEN3 ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    if( BSL6D_ENABLE_GPU )
      message(FATAL_ERROR "BSL6D does not provide appropriate GPU configurations for Kokkos yet.")
    endif()
  
  elseif( $ENV{HOST} MATCHES "raven" )
  
    if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
      set( Kokkos_ENABLE_OPENMP ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
    set( Kokkos_ARCH_ICX ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    if( BSL6D_ENABLE_GPU )
      set( Kokkos_ENABLE_CUDA ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      set( Kokkos_ARCH_AMPERE80 ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
        set( Kokkos_ENABLE_CUDA_RELOCATABLE_DEVICE_CODE ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      endif()
      set( Kokkos_ENABLE_CUDA_CONSTEXPR ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
  
  elseif($ENV{HOST} MATCHES "leonardo")
   
    set( Kokkos_ENABLE_OPENMP ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    if( BSL6D_ENABLE_GPU )
      set( Kokkos_ENABLE_CUDA ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      set( Kokkos_ARCH_AMPERE80 ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
        set( Kokkos_ENABLE_CUDA_RELOCATABLE_DEVICE_CODE ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      endif()
      set( Kokkos_ENABLE_CUDA_CONSTEXPR ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
  
  elseif($ENV{CI_RUNNER_TAGS} MATCHES "cloud")
  
    if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
      set( Kokkos_ENABLE_OPENMP ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
    set( Kokkos_ARCH_SKX ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    if( BSL6D_ENABLE_GPU )
      set( Kokkos_ENABLE_CUDA ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      if(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
        set( Kokkos_ENABLE_CUDA_RELOCATABLE_DEVICE_CODE ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
      endif()
      set( Kokkos_ENABLE_CUDA_CONSTEXPR ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
   
  else()
  
    if( BSL6D_ENABLE_GPU )
      MESSAGE(FATAL_ERROR "No internal configuration for the Kokkos backend of the BSL6D code is given for this host: $ENV{HOST}. Please provide your own installation of Kokkos for this system and provide it to cmake using `-D Kokkos_ROOT=/PATH/TO/KOKKOS`")
    elseif(NOT (CMAKE_CXX_COMPILER_ID MATCHES "Clang") )
      set( Kokkos_ENABLE_OPENMP ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    else()
      set( Kokkos_ENABLE_THREADS ON CACHE BOOL "Kokkos CMake option set within BSL6D.")
    endif()
    MESSAGE(WARNING "The default configuration for the Kokkos backend of the BSL6D code is used for an unknown host: $ENV{HOST}. This has non quantified impact on the performance of the code. It is suggested to install your own installation of Kokkos for this system and provide it to cmake using `-D Kokkos_ROOT=/PATH/TO/KOKKOS`")
  
  endif()
  
  if(BSL6D_ENABLE_GPU AND
     NOT (Kokkos_ENABLE_CUDA OR Kokkos_ENABLE_HIP) )
     MESSAGE(FATAL_ERROR "BSL6D_ENABLE_GPU is set to ${BSL6D_ENABLE_GPU} but "
                        "Kokkos_ENABLE_CUDA and Kokkos_ENABLE_HIP are OFF.")
  endif()
  
  FetchContent_MakeAvailable(Kokkos)

endif()

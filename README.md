# BSL6D
**B**ackwards **S**emi**L**agrangian **6D**imensions

[![Master Pipeline Status](https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d/badges/master/pipeline.svg)](https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d/commits/master)
[![Documentation](https://img.shields.io/static/v1?label=Documentation&message=Online&color=006c66)](https://bsl6d.pages.mpcdf.de/bsl6d/)
[![Version - Live at HEAD](https://img.shields.io/static/v1?label=Version&message=Live_at_HEAD&color=00b1ea)](https://abseil.io/blog/20171004-cppcon-plenary)

Welcome to BSL6D, the simulation software application designed for tackling the challenges of simulating the fully kinetic 6D-Vlasov equation. 
Our code is developed to explore physics beyond the conventional gyrokinetic model. 
Simulating the fully kinetic Vlasov equation demands efficient utilization of compute and storage capabilities, given the high dimensionality of the problem.
BSL6D rises to the challenge, providing a performant and extensible solution.

For further information feel free to consult our (work in progess) documentation:
- [Build](https://bsl6d.pages.mpcdf.de/bsl6d/usage/build.html)
- [Execution](https://bsl6d.pages.mpcdf.de/bsl6d/usage/execution.html)
- [Examples](https://bsl6d.pages.mpcdf.de/bsl6d/examples/index.html)

## Contiguous Integration
Our extensive test suit which is based on unit tests and run tests of the examples listed in our documentation is run every time something is pushed to the repository on a CPU and a GPU.
Unfortunately the GitLab tag only tracks the result of our default branch which is master.
At the time of writing GitLab has not added the feature to print the status of the currently selected branch as discussed in this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/27093).

If you are interested in the CI status of a specific branch you have to check this branch in the [pipeline](https://gitlab.mpcdf.mpg.de/bsl6d/bsl6d/-/pipelines) logs.

## Documentation
Our documentation has been added rather recently and gives you an overview of the capabilities of the code and how to recreate the given simulations.
We work on improvements of the API and design documentations.

## Users and Versioning
If you would like to run a few examples we are happy to support you setting those up and executing them. If you would like to use BSL6D as a C++ framework we plan to set up an installation workflow to allow linking against the library as soon as somebody approaches us with this feature request.

There is a fly in the ointment. We do not guarantee any long term support for any features of our codebase.
This has one simple reason. The project was the first larger codebase designed by the maintainers. 
Therefore, we obviously made some bad design decisions (e.g. the templated grid class.). 
If we find a better solution we want to incorporate this as soon as we find time to do so and not add a maintenance burden by keeping the old design as well.

This brings us to the version management.
All our changes might be breaking changes.
Therefore, we do not use semantic versioning but follow a "Live at HEAD" policy for us and expect the same from any downstream dependency.
"Live at HEAD" for us means that we try to update all our upstream dependencies regularly to the latest version available.
Reproducibility of our simulations is still ensured by storing the exact git hash of a conducted simulation in the output file of every simulation. 
We expect the same version management of downstream dependencies using BSL6D. 
We have reached multiple stable APIs, such that breaking changes should not occur frequently.
If any breaking change occurs we are happy to help fix it in any dependency.
But we do not plan to support deprecated features.

# Citing BSL6D
```
Main article on the performance results of the BSL6D project and its portability between different hardware architectures.
@article{Schild2024,
    title = {A performance portable implementation of the semi-Lagrangian algorithm in six dimensions},
    author = {Nils Schild and Mario Räth and Sebastian Eibl and Klaus Hallatschek and Katharina Kormann},
    journal = {Computer Physics Communications},
    volume = {295},
    pages = {108973},
    year = {2024},
    issn = {0010-4655},
    doi = {https://doi.org/10.1016/j.cpc.2023.108973},
    url = {https://www.sciencedirect.com/science/article/pii/S0010465523003181},
    keywords = {Fully kinetic simulation, Semi-Lagrangian, General purpose computing on graphic processing units (GPGPU), Performance portability, Kokkos, Software design patterns}
```

Main article on the semi-Lagrangian scheme used in the BSL6D implementation.
```
@article{Kormann2019,
    title ={A massively parallel semi-Lagrangian solver for the six-dimensional Vlasov–Poisson equation},
    author = {Katharina Kormann and Klaus Reuter and Markus Rampp},
    journal = {The International Journal of High Performance Computing Applications},
    volume = {33},
    number = {5},
    pages = {924-947},
    year = {2019},
    doi = {10.1177/1094342019834644}
}
```
